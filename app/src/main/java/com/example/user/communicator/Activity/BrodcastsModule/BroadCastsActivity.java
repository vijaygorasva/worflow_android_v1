package com.example.user.communicator.Activity.BrodcastsModule;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.communicator.Activity.MainActivity;
import com.example.user.communicator.Activity.TaskModule.BaseActivity;
import com.example.user.communicator.Activity.TaskModule.SearchUserActivity;
import com.example.user.communicator.Adapter.BrodcastsAdapters.BroadcastsListAdapter;
import com.example.user.communicator.Adapter.BrodcastsAdapters.OptionsAddAdapter;
import com.example.user.communicator.CustomViews.CustomButton;
import com.example.user.communicator.CustomViews.CustomEditText;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.BrodcastsModels.BroadCastItem;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.TaskModels.SelectionItem;
import com.example.user.communicator.Model.TaskModels.SelectionObject;
import com.example.user.communicator.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;


public class BroadCastsActivity extends BaseActivity {


    @BindView(R.id.tvToolbar)
    TextView tvToolbar;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.rvTaskList)
    RecyclerView brodcastListView;

    @BindView(R.id.etAddTask)
    CustomEditText etAddTask;

    @BindView(R.id.tvDone)
    CustomTextView tvDone;


    @BindView(R.id.tvOptions)
    CustomTextView tvOptions;


    @BindView(R.id.usersCount)
    CustomTextView usersCount;

    @BindView(R.id.otherCount)
    CustomTextView otherCount;

    @BindView(R.id.fabtnAdd)
    FloatingActionButton fabtnAdd;

    Context context;
    SelectionObject mObject;
    Datum mUserDetails;

    @BindView(R.id.cvTasks)
    CardView mAddView;

    @BindView(R.id.spinner)
    MaterialSpinner spinner;
    boolean isFromNotification = false;
    private BroadcastsListAdapter broadcastsListAdapter;
    private ArrayList<BroadCastItem> broadCastItems = new ArrayList<>();
    private Realm mRealm;
    private boolean isMyBroadCastItems = false;
    private ArrayList<String> mOptions = new ArrayList<>();
    private String mReplyCHoice;
    private String mReplyTYpe;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brodcasts);
        ButterKnife.bind(this);

        context = this;
        mRealm = Realm.getDefaultInstance();

        mUserDetails = getLoginData();

        isFromNotification = getIntent().getBooleanExtra("isFromNotification", false);

        brodcastListView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        brodcastListView.setLayoutManager(mLayoutManager);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        brodcastListView.setLayoutManager(layoutManager);

        broadcastsListAdapter = new BroadcastsListAdapter(this, broadCastItems);
        brodcastListView.setAdapter(broadcastsListAdapter);

        mObject = getSelectionItem();
        setAddView();

        spinner.setItems("Inbox", "Broadcasts");
        spinner.setOnItemSelectedListener((view, position, id, item) -> {
            setListOfBroadcast(item.toString());
        });


        if (isNetworkAvailable()) {
            RequestParams params = new RequestParams();
            params.put("receiverUserId", mUserDetails.getIntUserId());
            params.put("userLevelId", mUserDetails.getIntUserLevelId());
            params.put("memberTypeId", mUserDetails.getIntLevelTypeId());
            showProgressDialog(this, "Please Wait!");
            ApiCall(getBroadcastsInboxList, ApiCall.GET_BROADCASTS_INBOX_LIST, params);
        } else {
            RealmResults<BroadCastItem> itemsRealmResults = mRealm.where(BroadCastItem.class).equalTo("isMyBroadCast", false).sort("addTime", Sort.DESCENDING).findAll();
            if (itemsRealmResults != null && itemsRealmResults.size() != 0) {
                this.broadCastItems.addAll(itemsRealmResults);
                broadcastsListAdapter.notifyDataSetChanged();
            }
        }


        tvOptions.setOnClickListener(view -> showDialogOptions());
        tvDone.setOnClickListener(view -> taskAddCall(mReplyCHoice, mReplyTYpe, mOptions));
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }


    public void setListOfBroadcast(String item) {
        broadCastItems.clear();
        broadcastsListAdapter.notifyDataSetChanged();
        RequestParams params = new RequestParams();
        if (item.equals("Inbox")) {
            isMyBroadCastItems = false;
            if (isNetworkAvailable()) {
                params.put("receiverUserId", mUserDetails.getIntUserId());
                params.put("userLevelId", mUserDetails.getIntUserLevelId());
                params.put("memberTypeId", mUserDetails.getIntLevelTypeId());
                showProgressDialog(this, "Please Wait!");
                ApiCall(getBroadcastsInboxList, ApiCall.GET_BROADCASTS_INBOX_LIST, params);
            } else {
                RealmResults<BroadCastItem> itemsRealmResults = mRealm.where(BroadCastItem.class).equalTo("isMyBroadCast", false).sort("addTime", Sort.DESCENDING).findAll();
                if (itemsRealmResults != null && itemsRealmResults.size() != 0) {
                    broadCastItems.addAll(itemsRealmResults);
                    broadcastsListAdapter.notifyDataSetChanged();
                }
            }
        } else {
            isMyBroadCastItems = true;
            if (isNetworkAvailable()) {
                params.put("senderUserId", mUserDetails.getIntUserId());
                showProgressDialog(this, "Please Wait!");
                ApiCall(getBrodcastsList, ApiCall.GET_BRODCASTS_LIST, params);
            } else {
                RealmResults<BroadCastItem> itemsRealmResults = mRealm.where(BroadCastItem.class).equalTo("isMyBroadCast", true).sort("addTime", Sort.DESCENDING).findAll();
                if (itemsRealmResults != null && itemsRealmResults.size() != 0) {
                    broadCastItems.addAll(itemsRealmResults);
                    broadcastsListAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    public void taskAddCall(String choice, String type, ArrayList<String> options) {
        String task = etAddTask.getText().toString().trim();
        if (TextUtils.isEmpty(task)) {
            Toast.makeText(context, "Please write something about broadcast.", Toast.LENGTH_SHORT).show();
            return;
        }

        RequestParams params = new RequestParams();

        if (!TextUtils.isEmpty(choice)) {
            params.add("broadcastType", choice);
        } else {
            showToast("Please select broadcast type in broadcast options");
            return;
        }
        if (options != null) {
            options.removeAll(Collections.singletonList(""));
        }
        if (!TextUtils.isEmpty(type)) {
            params.add("replyType", type);
            if (type.equalsIgnoreCase("1") || type.equalsIgnoreCase("2")) {
                if (options == null || options.size() == 0) {
                    Toast.makeText(context, "Please enter reply option for reply in broadcast options", Toast.LENGTH_SHORT).show();
                    return;
                } else if (options.size() < 1) {
                    Toast.makeText(context, "Please enter at least 2 reply options for reply in broadcast options", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        }
        if (options != null && options.size() != 0) {
            JSONArray array = new JSONArray();
            for (String option : options) {
                if (!TextUtils.isEmpty(option)) {
                    array.put(option);
                }
            }
            params.add("replyOptions", array.toString());
        }

        params.put("message", task);
        params.put("senderUserId", mUserDetails.getIntUserId());
        if (mObject.isUser()) {
            if (mObject.getUsers() != null && mObject.getUsers().size() != 0) {
                JSONArray array = new JSONArray();
                for (SelectionItem item : mObject.getUsers()) {
                    array.put(item.getId());
                }
                if (array.length() != 0) {
                    params.put("receiverUserIds", array.toString());
                } else {
                    Toast.makeText(context, "Please select users or levels and members for task.", Toast.LENGTH_SHORT).show();
                    return;
                }
            } else {
                Toast.makeText(context, "Please select users or levels and members for task.", Toast.LENGTH_SHORT).show();
                return;
            }
        } else {
            if (mObject.getLevels() != null && mObject.getMemberTypes() != null && mObject.getLevels().size() != 0 && mObject.getMemberTypes().size() != 0) {
                JSONArray array = new JSONArray();
                for (SelectionItem item : mObject.getLevels()) {
                    array.put(item.getId());
                }

                JSONArray array1 = new JSONArray();
                for (SelectionItem item : mObject.getMemberTypes()) {
                    array1.put(item.getId());
                }

                if (array.length() != 0 && array1.length() != 0) {
                    params.put("userLevelIds", array);
                    params.put("memberTypeId", array1);
                } else {
                    Toast.makeText(context, "Please select users or levels and members for task.", Toast.LENGTH_SHORT).show();
                    return;
                }
            } else {
                Toast.makeText(context, "Please select users or levels and members for task.", Toast.LENGTH_SHORT).show();
                return;
            }
        }


        ApiCall(saveBrodcast, ApiCall.SAVE_BRODCAST, params);
    }


    @SuppressLint("SetTextI18n")
    public void setAddView() {
        if (mObject == null || mObject.isUser()) {
            if (mObject == null || mObject.getUsers().size() == 0) {
                mAddView.setVisibility(View.GONE);
            } else {
                usersCount.setVisibility(View.VISIBLE);
                otherCount.setVisibility(View.GONE);
                usersCount.setText(mObject.getUsers().size() + " Users");
                mAddView.setVisibility(View.VISIBLE);
            }
        } else {
            if (mObject.getLevels() != null && mObject.getMemberTypes() != null && mObject.getLevels().size() != 0 && mObject.getMemberTypes().size() != 0) {
                usersCount.setVisibility(View.GONE);
                otherCount.setVisibility(View.VISIBLE);
                otherCount.setText(mObject.getLevels().size() + " Levels and " + mObject.getMemberTypes().size() + "  Member Types");
                mAddView.setVisibility(View.VISIBLE);
            } else {
                mAddView.setVisibility(View.GONE);
            }
        }
    }


    @OnClick({R.id.ivBack, R.id.fabtnAdd})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.fabtnAdd:
                if (isNetworkAvailable()) {
                    Intent i = new Intent(BroadCastsActivity.this, SearchUserActivity.class);
                    startActivityForResult(i, 2020);
                } else {
                    showToast(getString(R.string.noConnection));
                }
                break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 2020) {
            if (resultCode == RESULT_OK) {
                Gson gson = new Gson();
                String userData = data.getStringExtra("data");
                mObject = gson.fromJson(userData, SelectionObject.class);
                if (mObject != null) {
                    setSelectionItem(mObject);
                    setAddView();
                }
            }
        }

    }


    private void showDialogOptions() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_brodcasts);


        final LinearLayout replytype_holder = dialog.findViewById(R.id.replytype_holder);
        final LinearLayout options_holder = dialog.findViewById(R.id.options_holder);
        final Spinner replyable_choice = dialog.findViewById(R.id.replyable_choice);
        final Spinner reply_type = dialog.findViewById(R.id.reply_type);

        final ArrayList<String> items = new ArrayList<>();
        final RecyclerView recyclerView = dialog.findViewById(R.id.recyclerView_options);
        CustomButton mDone = dialog.findViewById(R.id.submit);
        final NestedScrollView scrollView = dialog.findViewById(R.id.nes);

        final OptionsAddAdapter adapter = new OptionsAddAdapter(this, items);

        final String[] replyableChoice = {""};
        final String[] replyType = {""};

        options_holder.setVisibility(View.GONE);
        replytype_holder.setVisibility(View.GONE);

        final List<String> choice = new ArrayList<>();
        choice.add("Yes");
        choice.add("No");
        final List<String> type = new ArrayList<>();
        type.add("Simple Text");
        type.add("Single Choice");
        type.add("Multiple Choice");

        ArrayAdapter<String> replyableChoiceAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, choice);
        ArrayAdapter<String> replyTypeAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, type);
        replyable_choice.setAdapter(replyableChoiceAdapter);
        reply_type.setAdapter(replyTypeAdapter);


        replyable_choice.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String txt = choice.get(position);
                if (txt.equalsIgnoreCase("Yes")) {
                    replyableChoice[0] = "1";
                    replytype_holder.setVisibility(View.VISIBLE);
                } else {
                    replyableChoice[0] = "0";
                    replytype_holder.setVisibility(View.GONE);
                    reply_type.setSelection(0);
                    options_holder.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        reply_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String txt = type.get(position);
                if (txt.equalsIgnoreCase("Simple Text")) {
                    replyType[0] = "0";
                    items.clear();
                    adapter.notifyDataSetChanged();
                    adapter.setData(items);
                    options_holder.setVisibility(View.GONE);
                } else if (txt.equalsIgnoreCase("Single Choice")) {
                    replyType[0] = "1";
                    options_holder.setVisibility(View.VISIBLE);
                    items.clear();
                    items.add("");
                    adapter.notifyDataSetChanged();
                    adapter.setData(items);
                } else if (txt.equalsIgnoreCase("Multiple Choice")) {
                    replyType[0] = "2";
                    options_holder.setVisibility(View.VISIBLE);
                    items.clear();
                    items.add("");
                    adapter.notifyDataSetChanged();
                    adapter.setData(items);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);
        recyclerView.setNestedScrollingEnabled(false);

        recyclerView.setAdapter(adapter);
        adapter.setData(items);
        adapter.setOnItemClickListener(position -> {
            ArrayList<String> stringArrayList = adapter.getData();
            items.clear();
            items.addAll(stringArrayList);
            items.add("");
            adapter.setData(items);
            adapter.notifyDataSetChanged();
        });
        mDone.setOnClickListener(view -> {
            ArrayList<String> options = adapter.getData();
            String replyCHoice = replyableChoice[0];
            String replyTYpe = replyType[0];
            if (options != null) {
                options.removeAll(Collections.singletonList(""));
            }

            if (replyTYpe.equalsIgnoreCase("1") || replyTYpe.equalsIgnoreCase("2")) {
                if (options == null || options.size() == 0) {
                    Toast.makeText(context, "Please enter options for selection", Toast.LENGTH_SHORT).show();
                    return;
                } else if (options.size() < 1) {
                    Toast.makeText(context, "Please enter at least 2 options for selection", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            mOptions = options;
            mReplyCHoice = replyCHoice;
            mReplyTYpe = replyTYpe;

            dialog.dismiss();
//            taskAddCall(replyCHoice, replyTYpe, options);
        });
        scrollView.fullScroll(View.FOCUS_DOWN);
        dialog.show();
    }


    @Override
    public void OnResponce(JSONObject object, ApiCall type) {
        hideProgressDialog(this);
        Gson gson = new GsonBuilder().create();
        JSONArray array;
        try {
            switch (type) {
                case GET_BRODCASTS_LIST:
                case GET_BROADCASTS_INBOX_LIST:
                    broadCastItems.clear();
                    array = object.getJSONObject("data").getJSONArray("broadcastsData");
                    mRealm.beginTransaction();
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object1 = array.getJSONObject(i);
                        BroadCastItem item = gson.fromJson(object1.toString(), BroadCastItem.class);
                        item.setMyBroadCast(isMyBroadCastItems);
                        mRealm.copyToRealmOrUpdate(item);
                        broadCastItems.add(item);
                    }
                    mRealm.commitTransaction();
                    broadcastsListAdapter.setIsMyBroadCast(isMyBroadCastItems);
                    broadcastsListAdapter.notifyDataSetChanged();
                    break;

                case SAVE_BRODCAST:
                    if (isMyBroadCastItems) {
                        JSONObject object1 = object.getJSONObject("data").getJSONArray("broadcastData").getJSONObject(0);
                        mRealm.beginTransaction();
                        BroadCastItem item = gson.fromJson(object1.toString(), BroadCastItem.class);
                        mRealm.copyToRealmOrUpdate(item);
                        this.broadCastItems.add(0, item);
                        mRealm.commitTransaction();
                        broadcastsListAdapter.notifyDataSetChanged();
                    }
                    etAddTask.setText("");
                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnError(JSONObject object, ApiCall type) {
        hideProgressDialog(this);
    }


    @Override
    public void onBackPressed() {
        if (isFromNotification) {
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
        }
        super.onBackPressed();
    }
}
