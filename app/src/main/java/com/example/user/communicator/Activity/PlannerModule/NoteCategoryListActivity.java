package com.example.user.communicator.Activity.PlannerModule;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.user.communicator.Adapter.PlannerAdapter.PlannerCategorylistListAdapter;
import com.example.user.communicator.Application;
import com.example.user.communicator.ConnectivityReceiver;
import com.example.user.communicator.Fragment.PlannerFragment;
import com.example.user.communicator.Model.NoteCategory.Datum;
import com.example.user.communicator.R;
import com.example.user.communicator.Utility.DividerItemDecoration;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

public class NoteCategoryListActivity extends BaseActivity implements ConnectivityReceiver.ConnectivityReceiverListener {

    @BindView(R.id.listView)
    ListView listView;
    @BindView(R.id.tvCategory)
    EditText tvCategory;
    @BindView(R.id.ivSave)
    ImageView ivSave;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.rvCategoryList)
    RecyclerView rvCategoryList;

    String strUserId;
    Realm mRealm;
    com.example.user.communicator.Model.Login.Datum mUserDetails;
    PlannerCategorylistListAdapter plannerCategorylistListAdapter;
    ArrayList<Datum> categoryArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_category_list);
        ButterKnife.bind(this);
        SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences("Login", MODE_PRIVATE);
        String data = sharedpreferences.getString("UserData", null);
        Gson gson = new Gson();
        mUserDetails = gson.fromJson(data, com.example.user.communicator.Model.Login.Datum.class);
        strUserId = mUserDetails.getIntUserId();
        mRealm = Realm.getDefaultInstance();
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        rvCategoryList.setLayoutManager(layoutManager);
        rvCategoryList.addItemDecoration(
                new DividerItemDecoration(ContextCompat.getDrawable(getApplicationContext(),
                        R.drawable.item_decorator)));

        boolean isConnected = ConnectivityReceiver.isConnected();
        if (isConnected) {
            loadCategory();
        } else {
            loadCategory();
        }
//        loadCategory();

        String val = tvCategory.getText().toString();
        if (val.length() > 0) {
            ivSave.setVisibility(View.VISIBLE);
        }

        tvCategory.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                tvCategory.setCursorVisible(true);
                return false;
            }
        });

        tvCategory.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                String val = tvCategory.getText().toString();
                if (val.length() > 0) {
                    ivSave.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                String val = tvCategory.getText().toString();
                if (val.length() > 0) {
                    ivSave.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                //here you will get changed text in textview
//                    getAllNotes("0");

                String val = tvCategory.getText().toString();
                if (val.length() > 0) {
                    ivSave.setVisibility(View.VISIBLE);
                }

            }
        });

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ivSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable()) {
                    saveCategory();
                } else {
                    showToast(getString(R.string.noConnection));
                }

            }
        });

//        rvCategoryList.addOnItemTouchListener(new RecyclerItemClickListener(NoteCategoryListActivity.this, new RecyclerItemClickListener.OnLongClickListener() {
//            @Override
//            public void onItemClick(View view, int position) {
//
////                Intent intent=new Intent(PlannerActivity.this,NoteDetailActivity.class);
////                startActivity(intent);
//
//                int resultCode = 2;
//                ColleborateDatum temp = categoryArrayList.get(position);
//                Log.e("selecteddata", temp.getStrMainCategoryName() + "");
//                Intent intent = new Intent();
//                intent.putExtra("SELECTED_DATA", temp.getStrMainCategoryName());
//                intent.putExtra("ID", temp.getId());
//                setResult(resultCode, intent);
//                finish();
//
//            }
//        }));


//        tvCategory.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View v, int keyCode, KeyEvent event) {
//                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
//                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
//                    // Perform action on key press
//                    DatumOper sample = new DatumOper();
//                    sample.setDate("11 Feb 2019");
//                    sample.setTitle(tvCategory.getText().toString());
//                    sample.setSubtitle(tvCategory.getText().toString()+"..New Project in progress");
//                    categoryArrayList.add(0,sample);
//                    operationAdapter.notifyDataSetChanged();
//                    tvCategory.setText("");
//                    return true;
//
//                }
//                return false;
//            }
//        });
    }

    public void loadCategory() {
        if (isNetworkAvailable()) {
            RequestParams params = new RequestParams();
            params.put("strCreateUserId", strUserId);
            params.put("strCategoryTypeId", PlannerFragment.strCategoryTypeId);
            params.put("strCategoryId", PlannerFragment.strCategoryId);
            params.put("strSubCategoryId", PlannerFragment.strSubCategoryId);
            showProgressDialog(this, "Please Wait...");
            ApiCallWithToken(urlNoteCategory, ApiCall.CATEGORYLIST, params);
        } else {
            categoryArrayList.clear();
            RealmResults<Datum> itemsRealmResults = mRealm.where(Datum.class).findAll();
            if (itemsRealmResults != null && itemsRealmResults.size() != 0) {
                categoryArrayList.addAll(itemsRealmResults);
//                plannerCategorylistListAdapter = new PlannerCategorylistListAdapter(NoteCategoryListActivity.this, categoryArrayList);//,(PlannerCategorylistListAdapter.Callback) callback
                rvCategoryList.setAdapter(plannerCategorylistListAdapter);
            }
        }
    }

    public void saveCategory() {

        String catName = tvCategory.getText().toString();
        RequestParams params = new RequestParams();
        params.put("strMainCategoryName", catName);
        params.put("strCreateUserId", strUserId);
//        params.put("intProjectId", mUserDetails.getArryObjModuleUser().get(0).getIntProjectId());
//        params.put("intOrganisationId", mUserDetails.getArryObjModuleUser().get(0).getIntOrganisationId());
        showProgressDialog(this, "Please Wait...");
        ApiCallWithToken(urlCategorySave, ApiCall.CATEGORYSAVE, params);

    }

    @Override
    public void OnResponce(JSONObject data, ApiCall type) {

        JSONArray array = null;
        try {
            if (type == ApiCall.CATEGORYLIST) {
                array = data.getJSONArray("data");
                hideProgressDialog(NoteCategoryListActivity.this);
                if (array.length() != 0) {

                    Gson gson = new Gson();
                    categoryArrayList.clear();
                    Datum datum = new Datum();
                    datum.setId("0");
                    datum.setStrMainCategoryName("All");
                    categoryArrayList.add(0, datum);
                    mRealm.beginTransaction();
                    for (int i = 0; i < array.length(); i++) {
                        try {

                            JSONObject object = array.getJSONObject(i);
                            Datum item = gson.fromJson(object.toString(), Datum.class);
                            mRealm.copyToRealmOrUpdate(item);
                            categoryArrayList.add(item);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    mRealm.commitTransaction();
                    if (categoryArrayList.size() != 0) {
//                        plannerCategorylistListAdapter = new PlannerCategorylistListAdapter(NoteCategoryListActivity.this, categoryArrayList);
                        rvCategoryList.setAdapter(plannerCategorylistListAdapter);
                    }
                }
            } else if (type == ApiCall.CATEGORYSAVE) {
                array = data.getJSONArray("data");
                hideProgressDialog(NoteCategoryListActivity.this);
                if (array.length() != 0) {
                    Gson gson = new Gson();
                    for (int i = 0; i < array.length(); i++) {
                        try {

                            JSONObject object = array.getJSONObject(i);
                            Datum item = gson.fromJson(object.toString(), Datum.class);
                            plannerCategorylistListAdapter.insertItem(item, 0);
                            plannerCategorylistListAdapter.notifyItemInserted(0);
                            tvCategory.setText("");
                            ivSave.setVisibility(View.GONE);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        super.OnResponce(data, type);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            color = Color.WHITE;
            loadCategory();
        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
            loadCategory();
        }

        Snackbar snackbar = Snackbar
                .make(findViewById(R.id.rvCategoryList), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(color);
        snackbar.show();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        Application.getInstance().setConnectivityListener(this);
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        showSnack(isConnected);
    }
}
