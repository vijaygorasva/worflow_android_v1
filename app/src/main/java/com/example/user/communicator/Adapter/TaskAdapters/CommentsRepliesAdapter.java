package com.example.user.communicator.Adapter.TaskAdapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.TaskModels.Replies;
import com.example.user.communicator.R;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentsRepliesAdapter extends RecyclerView.Adapter<CommentsRepliesAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Replies> commentDataArrayList;
    private OnClickListner onClick;
    //    public final String BASE_URL = "http://52.66.249.75:9999/";
    public final String BASE_URL_SEVER = "http://52.66.249.75:9999/";
    public final String BASE_URL_local = "http://:9999/";
    public final String BASE_URL_TEST = "http://13.232.37.104:9999/";
    public final String BASE_URL = BASE_URL_TEST;

    public CommentsRepliesAdapter(Context context, ArrayList<Replies> commentDataArrayList) {
        this.commentDataArrayList = commentDataArrayList;
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_comment_replies, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(commentDataArrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return commentDataArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setOnClickListner(OnClickListner onClickListner) {
        this.onClick = onClickListner;
    }

    private String getMillsToDate(long mills) {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
        return df.format(new Date(mills));
    }

    private String getDiffrenece(long startDate, long endDate) {

//        long difference = (endDate - startDate) + 321153;
        long difference = (endDate - startDate);
        long sec = difference / 1000 % 60;
        long min = difference / (60 * 1000) % 60;
        long hours = difference / (60 * 60 * 1000) % 24;
        long diffDays = difference / (24 * 60 * 60 * 1000);

//        return (hours == 0 ? (min == 0 ? (sec + " sec") : (min + " min")) : (hours < 24 ? (hours + " hrs") : (getMillsToDate(startDate))));

        return (diffDays == 0 ? ((hours == 0 ? (min == 0 ? ("Just now") : (min + " min")) : (hours + " hrs"))) : (getMillsToDate(startDate)));
    }


    public interface OnClickListner {
        void OnClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.prof_img)
        ImageView prof_img;

        @BindView(R.id.name)
        CustomTextView name;

        @BindView(R.id.time)
        CustomTextView time;

        @BindView(R.id.commentTxt)
        CustomTextView commentTxt;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        @SuppressLint("SetTextI18n")
        public void bind(final Replies item) {

//            String tim = getMillsToDate(item.getAddTime());
            String diff = getDiffrenece(item.getAddTime(), System.currentTimeMillis());
            if (!TextUtils.isEmpty(diff)) {
                time.setText(diff);
            } else {
                time.setText("");
            }
            if (item.getIntUserId().getImg() != null) {
                if (!TextUtils.isEmpty(item.getIntUserId().getImg().getPath())) {
                    Picasso.get().load(BASE_URL + item.getIntUserId().getImg().getPath()).placeholder(R.drawable.ic_error_male).error(R.drawable.ic_error_male).into(prof_img);
                }
            }
            name.setText(item.getIntUserId().getStrFirstName() + " " + item.getIntUserId().getStrLastName());
            commentTxt.setText(item.getComment());

        }
    }

}