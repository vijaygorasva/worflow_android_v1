package com.example.user.communicator.Dialog;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.communicator.R;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class AddAudioDialog extends DialogFragment {

    Callback callback;
    @BindView(R.id.btnRecord)
    ImageView btnRecord;
    @BindView(R.id.txtTimer)
    TextView TxtTimer;
    @BindView(R.id.chronometer)
    Chronometer chronometer;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.llButtons)
    LinearLayout llButtons;

    private boolean isStart;
    String AudioSavePathInDevice = null;
    MediaRecorder mediaRecorder;
    File audioFile;

    int displayWidth, displayHeight;
    public static final int RequestPermissionCode = 1;

    public static AddAudioDialog newIntance(int displayWidth, int displayHeight, Callback callback) {

        Bundle args = new Bundle();
        args.putInt("displayWidth", displayWidth);
        args.putInt("displayHeight", displayHeight);

        AddAudioDialog dialog = new AddAudioDialog();
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        this.context=MainActivity;
        dialog.setArguments(args);
        dialog.setCallback(callback);
        return dialog;
    }


    public AddAudioDialog() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, getTheme());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_audio, container, false);
        ButterKnife.bind(this, view);
        Bundle args = getArguments();
        displayWidth = args.getInt("displayWidth");
        displayHeight = args.getInt("displayHeight");
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.border_roundeed);
        Window window = getDialog().getWindow();
//        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        setCancelable(true);
        view.setMinimumWidth((int) (displayWidth * 0.8f));
        view.setMinimumHeight((int) (displayHeight * 0.4f));
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        random = new Random();
//        spinner.setItems("All Staff", "All Head");
//        int index=spinner.getSelectedIndex();
        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometerChanged) {
                chronometer = chronometerChanged;
            }
        });
        btnRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startStopChronometer(v);
            }
        });
    }

    public void startStopChronometer(View view) {
        TxtTimer.setVisibility(View.GONE);
        chronometer.setVisibility(View.VISIBLE);
        llButtons.setVisibility(View.VISIBLE);
        if (isStart) {
            chronometer.stop();
            isStart = false;
        } else {
            chronometer.setBase(SystemClock.elapsedRealtime());
            chronometer.start();
            isStart = true;
        }

        startRecord();
        setCancelable(false);
    }

    private void stopRecording() {
        try {

            mediaRecorder.stop();
            mediaRecorder.reset();   // You can reuse the object by going back to setAudioSource() step
            mediaRecorder.release();
            chronometer.stop();

        } catch (RuntimeException stopException) {
            //handle cleanup here
        }
//        camera.lock();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {

        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

//    @OnClick(R.id.btnCancel)
//    public void onCancelClick() {
//        dismiss();
//    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick({R.id.btnSubmit, R.id.btnCancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {

//            case R.id.btnRecord:
//
//                break;

            case R.id.btnSubmit:
                stopRecording();
                if (isNetworkAvailable()) {
                    callback.edit(AddAudioDialog.this, audioFile, chronometer.getText().toString());
                } else {
                    showToast("No Internet Connection");
                }
                dismiss();

                break;
            case R.id.btnCancel:
                stopRecording();
                audioFile = new File("");

                callback.edit(AddAudioDialog.this, audioFile, chronometer.getText().toString());
                dismiss();
                break;

        }
    }

//    @OnClick(R.id.btnSubmit)
//    public void onSubmitClick() {
//
//        callback.edit(AddNewTaskDialog.this,memType);
//        dismiss();
//
//    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public interface Callback {
        void edit(AddAudioDialog dialog, File name, String time);
    }

    public void startRecord() {

        if (checkPermission()) {

            File folder = new File(Environment.getExternalStorageDirectory() + File.separator + "WorFlow");
            File newfolder = new File(folder, "WorFlow Audios");

            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            File f = new File(newfolder + File.separator + "AUD_" + timestamp + ".wav");
            AudioSavePathInDevice =
                    newfolder + "/" + "AUD_" + timestamp + ".wav";
            MediaRecorderReady();

            try {
                chronometer.start();
                mediaRecorder.prepare();
                mediaRecorder.start();
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

//            buttonStart.setEnabled(false);
//            buttonStop.setEnabled(true);

        } else {
            requestPermission();
        }

    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                if (grantResults.length > 0) {
                    boolean StoragePermission = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    boolean RecordPermission = grantResults[1] ==
                            PackageManager.PERMISSION_GRANTED;

                    if (StoragePermission && RecordPermission) {
                        showToast("Permission Granted");
                    } else {
                        showToast("Permission Denied");
                    }
                }
                break;
        }
    }


    public void MediaRecorderReady() {
        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mediaRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        mediaRecorder.setOutputFile(AudioSavePathInDevice);
        audioFile = new File(AudioSavePathInDevice);

    }

    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getContext(),
                WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getContext(),
                RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public void showToast(String message) {
        LayoutInflater inflater = getLayoutInflater();
        View layouttoast = inflater.inflate(R.layout.toastcustom, (ViewGroup) getActivity().findViewById(R.id.toastcustom));
        ((TextView) layouttoast.findViewById(R.id.texttoast)).setText(message);

        Toast mytoast = new Toast(getActivity());
        mytoast.setView(layouttoast);
        mytoast.setDuration(Toast.LENGTH_LONG);
        mytoast.setGravity(Gravity.CENTER, 0, 0);
        mytoast.show();
    }
}