package com.example.user.communicator.Activity.TaskModule.DetailsActivity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.user.communicator.Adapter.TaskAdapters.DurationAdapter;
import com.example.user.communicator.Adapter.TaskAdapters.UsersAdapter;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.TaskModels.Duration;
import com.example.user.communicator.Model.TaskModels.SelectionItems.User;
import com.example.user.communicator.Model.TaskModels.TaskDetailItem;
import com.example.user.communicator.R;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

public class ActivitysPageFragment extends BaseFragment implements DurationAdapter.Callback {

    @BindView(R.id.durationsLayout)
    LinearLayout durationsLayout;

    @BindView(R.id.spinnerView)
    CardView spinnerView;

    @BindView(R.id.durationsView)
    RecyclerView userView;

    @BindView(R.id.noData)
    CustomTextView noData;

    @BindView(R.id.spinner)
    Spinner spinner;

    @BindView(R.id.tvTotalTime)
    CustomTextView tvTotalTime;

    LinearLayoutManager linearLayoutManager2;
    UsersAdapter usersAdapter;
    DurationAdapter durationAdapter;
    List<String> users = new ArrayList<>();
    ArrayList<User> userArrayList = new ArrayList<>();
    ArrayList<Duration> durationArrayList = new ArrayList<>();
    TaskDetailItem items;
    Context context;
    String task_Id;
    Realm mRealm;
    Datum mUserData;

    public static ActivitysPageFragment newInstance(String _id, boolean isFromNotification) {
        ActivitysPageFragment fragmentFirst = new ActivitysPageFragment();
        Bundle args = new Bundle();
        args.putString("_id", _id);
        args.putBoolean("isFromNotification", isFromNotification);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.layout_activitys_fragment, container, false);
        ButterKnife.bind(this, rootView);

        context = getActivity();
        mRealm = Realm.getDefaultInstance();
        mUserData = getLoginData();
        task_Id = String.valueOf(getArguments().getString("_id"));
        if (isNetworkAvailable()) {
            if (!TextUtils.isEmpty(task_Id)) {
                getData(task_Id);
            }
        } else {
            if (!TextUtils.isEmpty(task_Id)) {
                items = mRealm.where(TaskDetailItem.class).equalTo("intTaskDocumentNo", Integer.valueOf(task_Id)).findFirst();
                setData();
            }
        }

        return rootView;
    }


    public void setData() {
        linearLayoutManager2 = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        userView.setLayoutManager(linearLayoutManager2);
        if (items.getIntCreateUserId().equalsIgnoreCase(mUserData.getIntUserId())) {
            users.clear();
            for (int i = 0; i < items.getUsers().size(); i++) {
                User user = items.getUsers().get(i);
                users.add(user.getStrFirstName() + " " + user.getStrLastName());
            }
            if (users.size() != 0) {
                ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, users);
                dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(dataAdapter);
                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        userArrayList.clear();
                        ArrayList<Duration> durations = new ArrayList<>();
                        for (int i = 0; i < items.getUsers().size(); i++) {
                            User user = items.getUsers().get(i);
                            String name = user.getStrFirstName() + " " + user.getStrLastName();
                            if (name.equalsIgnoreCase(users.get(position))) {
                                if (user.getDurations().size() != 0) {
                                    for (Duration d : items.getUsers().get(i).getDurations()) {
                                        if (d.getEndTime() != 0) {
                                            durations.add(d);
                                        }
                                    }
                                }
                            }
                        }
                        durationArrayList.clear();
                        if (durations.size() != 0) {
                            durationArrayList.addAll(durations);
                        }
                        setadpter();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                spinner.setSelection(0);
            } else {
                spinnerView.setVisibility(View.GONE);
                noData.setVisibility(View.VISIBLE);
            }

//        ArrayList<String> timeLsit=new ArrayList<>(durationAdapter.getTimeList());

        } else {
            spinnerView.setVisibility(View.GONE);
            boolean isF = false;
            ArrayList<Duration> durations = new ArrayList<>();
            for (int i = 0; i < items.getUsers().size() && !isF; i++) {
                if (items.getUsers().get(i).get_id().equalsIgnoreCase(mUserData.getIntUserId())) {
                    if (items.getUsers().get(i).getDurations().size() != 0) {
                        for (Duration d : items.getUsers().get(i).getDurations()) {
                            if (d.getEndTime() != 0) {
                                durations.add(d);
                            }
                        }
                    }
                    isF = true;
                }
            }
            durationArrayList.clear();
            if (durations.size() != 0) {
                durationArrayList.addAll(durations);
            }
            durationAdapter = new DurationAdapter(context, durationArrayList, items, this);
            userView.setAdapter(durationAdapter);
            if (durationArrayList.size() != 0) {
                noData.setVisibility(View.GONE);
            } else {
                noData.setVisibility(View.VISIBLE);
            }
        }


    }


    public void setadpter() {

        durationAdapter = new DurationAdapter(context, durationArrayList, items, this);
        userView.setAdapter(durationAdapter);
        if (durationArrayList.size() != 0) {
            Log.e("timestamp_setDat", "timestamp_setDat");
            noData.setVisibility(View.GONE);
            tvTotalTime.setVisibility(View.VISIBLE);
//            Log.e("timestamp_setDat", DurationAdapter.timestampsList.size() + ".....setDat....2222..." + DurationAdapter.tot);
//            Log.e("timestampsList", DurationAdapter.tot + "........");
//            tvTotalTime.setText("Time  : " + DurationAdapter.tot );

        } else {
            tvTotalTime.setVisibility(View.GONE);
            noData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void OnResponce(JSONObject object, ApiCall type) {
        hideProgressDialog();
        try {
            if (type == ApiCall.GET_TASK_DATA) {
                Gson gson = new Gson();
                TaskDetailItem items = gson.fromJson(object.getJSONObject("data").getJSONObject("taskData").toString(), TaskDetailItem.class);
                items.setDetailAvailable(true);
                mRealm.beginTransaction();
                mRealm.copyToRealmOrUpdate(items);
                mRealm.commitTransaction();
                this.items = mRealm.where(TaskDetailItem.class).equalTo("_id", items.get_id()).findFirst();

                setData();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnError(JSONObject object, ApiCall type) {
        try {
            if (type == ApiCall.GET_TASK_DATA) {
                showToast(object.getString("message"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getData(String id) {
        RequestParams params = new RequestParams();
        params.put("intTaskDocumentNo", id);
//        showProgressDialog("Please wait...");
        ApiCallWithToken(getTaskData, ApiCall.GET_TASK_DATA, params);
    }

    @Override
    public void timeTotal(String total) {
        if (total.length() == 0) {
            tvTotalTime.setVisibility(View.GONE);
        } else {
            tvTotalTime.setVisibility(View.VISIBLE);
            tvTotalTime.setText("Time  : " + DurationAdapter.tot);
        }
    }
}
