package com.example.user.communicator.Model.Notes;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class ArryCollaboratUserDatum extends RealmObject implements Parcelable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("itemName")
    @Expose
    private String itemName;
    @SerializedName("imgPic")
    @Expose
    private String imgPic;
    @SerializedName("category")
    @Expose
    private String category;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getImgPic() {
        return imgPic;
    }

    public void setImgPic(String imgPic) {
        this.imgPic = imgPic;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public ArryCollaboratUserDatum(String id, String itemName, String imgPic, String category) {
        this.id = id;
        this.itemName = itemName;
        this.imgPic = imgPic;
        this.category = category;
    }

    @Override
    public int describeContents() {
        return 0;
    }
    public static final Creator<ArryCollaboratUserDatum> CREATOR = new Creator<ArryCollaboratUserDatum>() {
        @Override
        public ArryCollaboratUserDatum createFromParcel(Parcel in) {
            return new ArryCollaboratUserDatum(in);
        }

        @Override
        public ArryCollaboratUserDatum[] newArray(int size) {
            return new ArryCollaboratUserDatum[size];
        }
    };

    public ArryCollaboratUserDatum() {

    }

    protected ArryCollaboratUserDatum(Parcel in) {
        id = in.readString();
        itemName = in.readString();
        imgPic = in.readString();
        category = in.readString();
    }


    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(id);
        parcel.writeString(itemName);
        parcel.writeString(imgPic);
        parcel.writeString(category);
    }


}
