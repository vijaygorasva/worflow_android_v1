package com.example.user.communicator.Adapter.TaskAdapters;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.user.communicator.Activity.TaskModule.PDFViewerActivity;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.TaskModels.CommentData;
import com.example.user.communicator.Model.TaskModels.Replies;
import com.example.user.communicator.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {

    private Context context;
    private ArrayList<CommentData> commentDataArrayList;
    private OnClickListner onClick;
//    public final String BASE_URL = "http://52.66.249.75:9999/";

    public final String BASE_URL_SEVER = "http://52.66.249.75:9999/";
    public final String BASE_URL_local = "http://:9999/";
    public final String BASE_URL_TEST = "http://13.232.37.104:9999/";
    public final String BASE_URL = BASE_URL_TEST;

    public CommentsAdapter(Context context, ArrayList<CommentData> commentDataArrayList) {
        this.commentDataArrayList = commentDataArrayList;
        this.context = context;

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(commentDataArrayList.get(holder.getAdapterPosition()));
    }

    @Override
    public int getItemCount() {
        return commentDataArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setOnClickListner(OnClickListner onClickListner) {
        this.onClick = onClickListner;
    }

    private String getMillsToDate(long mills) {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
        return df.format(new Date(mills));
    }

    private String getDiffrenece(long startDate, long endDate) {

//        long difference = (endDate - startDate) + 321153;
        long difference = (endDate - startDate);
        long sec = difference / 1000 % 60;
        long min = difference / (60 * 1000) % 60;
        long hours = difference / (60 * 60 * 1000) % 24;
        long diffDays = difference / (24 * 60 * 60 * 1000);

//        return (hours == 0 ? (min == 0 ? (sec + " sec") : (min + " min")) : (hours < 24 ? (hours + " hrs") : (getMillsToDate(startDate))));

        return (diffDays == 0 ? ((hours == 0 ? (min == 0 ? ("Just now") : (min + " min")) : (hours + " hrs"))) : (getMillsToDate(startDate)));
    }


    public interface OnClickListner {
        void OnClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.prof_img)
        ImageView prof_img;

        @BindView(R.id.reply)
        LinearLayout reply;

        @BindView(R.id.name)
        CustomTextView name;

        @BindView(R.id.time)
        CustomTextView time;

        @BindView(R.id.commentTxt)
        CustomTextView commentTxt;

        @BindView(R.id.fileName)
        CustomTextView fileName;

        @BindView(R.id.line)
        View line;

        @BindView(R.id.repliesView)
        RecyclerView repliesView;

        @BindView(R.id.ivImg)
        ImageView mImg;

        CommentsRepliesAdapter replyAdapter;
        ArrayList<Replies> repliesArrayList = new ArrayList<>();
        LinearLayoutManager linearLayoutManager;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        @SuppressLint("SetTextI18n")
        public void bind(final CommentData item) {

//            String tim = getMillsToDate(item.getAddTime());
            String diff = getDiffrenece(item.getAddTime(), System.currentTimeMillis());
            if (!TextUtils.isEmpty(diff)) {
                time.setText(diff);
            } else {
                time.setText("");
            }
            if (item.getIntUserId().getImg() != null) {
                if (!TextUtils.isEmpty(item.getIntUserId().getImg().getPath())) {
                    Picasso.get().load(BASE_URL + item.getIntUserId().getImg().getPath()).placeholder(R.drawable.ic_error_male).error(R.drawable.ic_error_male).into(prof_img);
                } else {
                    prof_img.setImageResource(R.drawable.ic_error_male);
                }
            } else {
                prof_img.setImageResource(R.drawable.ic_error_male);
            }
            name.setText(item.getIntUserId().getStrFirstName() + " " + item.getIntUserId().getStrLastName());
            commentTxt.setText(item.getComment());
            reply.setOnClickListener(view -> {
                if (onClick != null) {
                    onClick.OnClick(getAdapterPosition());
                }
            });
            if (item.getIsHaveAttachment() == 1) {
                if (!TextUtils.isEmpty(item.getOriginalName())) {
                    mImg.setVisibility(View.VISIBLE);
                    fileName.setVisibility(View.VISIBLE);
                    fileName.setText(item.getOriginalName());
                    if (item.getFileType().equalsIgnoreCase("DOC")) {
                        mImg.setImageResource(R.drawable.ic_pdf);
                    } else if (item.getFileType().equalsIgnoreCase("IMAGE")) {
                        mImg.setImageResource(R.drawable.ic_gallery);
                    }
                } else {
                    mImg.setVisibility(View.GONE);
                    fileName.setVisibility(View.GONE);
                }
            } else {
                mImg.setVisibility(View.GONE);
                fileName.setVisibility(View.GONE);
            }
            fileName.setOnClickListener(v -> {
                if (item.getFileType().equalsIgnoreCase("IMAGE")) {
                    if (!TextUtils.isEmpty(item.getFilePath())) {
//                        Intent i = new Intent(context, ImageShowActivity.class);
//                        i.putExtra("img_url", item.getFilePath());
//                        context.startActivity(i);
                        showImagePopup(item.getFilePath());
                    }
                } else {
                    if (!TextUtils.isEmpty(item.getFilePath())) {
                        File imgFile = new File(item.getFilePath());
                        if (imgFile.exists()) {
                            Intent i = new Intent(context, PDFViewerActivity.class);
                            i.putExtra("path", item.getFilePath());
                            context.startActivity(i);
                        } else {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(BASE_URL + item.getFilePath()));
                            context.startActivity(browserIntent);
                        }
                    }
                }
            });

//            if (item.getReplies() != null && item.getReplies().size() != 0) {
//                repliesArrayList.addAll(item.getReplies().sort("addTime", Sort.ASCENDING));
//            }
            repliesArrayList.clear();
            if (item.getReplies() != null && item.getReplies().size() != 0) {
                repliesArrayList.addAll(item.getReplies());
            }
            line.setVisibility(View.VISIBLE);
            linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            repliesView.setLayoutManager(linearLayoutManager);
            replyAdapter = new CommentsRepliesAdapter(context, repliesArrayList);
            repliesView.setAdapter(replyAdapter);

        }


        void showImagePopup(String path) {
            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setCancelable(true);
            dialog.setCanceledOnTouchOutside(true);
            dialog.setContentView(R.layout.dialog_image);

            ImageView mImg = dialog.findViewById(R.id.img);

            File imgFile = new File(path);
            if (imgFile.exists()) {
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                mImg.setImageBitmap(myBitmap);
            } else {
                Picasso.get().load(BASE_URL + path).into(mImg);
            }
            dialog.show();
        }

    }

}
