package com.example.user.communicator.Activity.AlertsModule;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.user.communicator.Activity.PlannerModule.NoteDetailActivity;
import com.example.user.communicator.Activity.TaskModule.BaseActivity;
import com.example.user.communicator.Activity.TaskModule.DetailsActivity.DetailsTabActivity;
import com.example.user.communicator.Adapter.InboxAdapters.InboxAdapter;
import com.example.user.communicator.Model.InboxModel.Inbox;
import com.example.user.communicator.Model.InboxModel.NotificationData;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.R;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InboxActivity extends BaseActivity {


    @BindView(R.id.recyclerView)

    RecyclerView mRecyclerView;

    @BindView(R.id.vLoading)
    LinearLayout vLoading;

    @BindView(R.id.ivBack)
    ImageView ivBack;

    private ArrayList<Inbox> inboxArrayList = new ArrayList<>();
    InboxAdapter inboxAdapter;
    LinearLayoutManager linearLayoutManager;
    Datum mUserDetails;
    boolean isAllLoaded = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_inbox);
        ButterKnife.bind(this);

        mUserDetails = getLoginData();
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        inboxAdapter = new InboxAdapter(this, inboxArrayList);
        mRecyclerView.setAdapter(inboxAdapter);
        inboxAdapter.setOnBottomListner(onBottomListner);
        inboxAdapter.setOnClickListner(onClickListner);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

//        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//
//                int totalItemCount = linearLayoutManager.getItemCount();
//                int lastVisible = linearLayoutManager.findLastVisibleItemPosition();
//
//                boolean endHasBeenReached = lastVisible + 5 >= totalItemCount;
//                if (totalItemCount > 0 && endHasBeenReached) {
//
//                }
//            }
//        });

        RequestParams params = new RequestParams();
        params.put("intUserId", mUserDetails.getIntUserId());
        params.put("lastNotificationId", "");

        ApiCall(getNotification, ApiCall.GET_NOTIFICATION, params);

    }

    InboxAdapter.OnBottomListner onBottomListner = () -> {
        if (!isAllLoaded) {
            RequestParams params = new RequestParams();
            params.put("intUserId", mUserDetails.getIntUserId());
            params.put("lastNotificationId", inboxArrayList.get(inboxArrayList.size() - 1).get_id());
            ApiCall(getNotification, ApiCall.GET_NOTIFICATION, params);
        }

    };
    InboxAdapter.OnClickListner onClickListner = position -> {
        if (!TextUtils.isEmpty(inboxArrayList.get(position).getNotificationData().getIntTaskDocumentNo())) {
            showToast(inboxArrayList.get(position).getNotificationData().getType() + ".....");
            if (inboxArrayList.get(position).getNotificationData().getType().equals("planner_note")) {
                Intent intent = new Intent(InboxActivity.this, NoteDetailActivity.class);
                intent.putExtra("type", "notification");
                intent.putExtra("plannerId", inboxArrayList.get(position).getNotificationData().getIntTaskDocumentNo());
                startActivity(intent);
            } else {
                Intent intent = new Intent(InboxActivity.this, DetailsTabActivity.class);
                int _id = Integer.parseInt(inboxArrayList.get(position).getNotificationData().getIntTaskDocumentNo());
                intent.putExtra("isAppIsInBackground", false);
                intent.putExtra("isFromNotification", true);
                intent.putExtra("intUserId", inboxArrayList.get(position).getIntUserId());
                intent.putExtra("_id", _id);
                startActivity(intent);
            }
        }
    };


    @Override
    public void OnResponce(JSONObject object, ApiCall type) {
        try {
            if (type == ApiCall.GET_NOTIFICATION) {
                vLoading.setVisibility(View.GONE);
                Gson gson = new Gson();
                JSONArray array = object.getJSONObject("data").getJSONArray("notificationsData");
                if (array.length() == 0 || array.length() < 20) {
                    isAllLoaded = true;
                }
                for (int i = 0; i < array.length(); i++) {
                    JSONObject jsonObject = array.getJSONObject(i);
                    Inbox inbox = new Inbox();
                    inbox.set_id(jsonObject.getString("_id"));
                    inbox.setType(jsonObject.getString("type"));
                    inbox.setIntUserId(jsonObject.getString("intUserId"));
                    if (jsonObject.has("add_time")) {
                        inbox.setAdd_time(jsonObject.getLong("add_time"));
                    }
                    NotificationData notificationData = gson.fromJson(jsonObject.getString("data"), NotificationData.class);
                    inbox.setNotificationData(notificationData);
                    inboxArrayList.add(inbox);
                }
                inboxAdapter.notifyDataSetChanged();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
