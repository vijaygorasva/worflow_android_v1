package com.example.user.communicator.Activity.PlannerModule;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.user.communicator.Adapter.PlannerAdapter.ColleborateListAdapter;
import com.example.user.communicator.Application;
import com.example.user.communicator.ConnectivityReceiver;
import com.example.user.communicator.Model.Colleborate.ColleborateDatum;
import com.example.user.communicator.Model.Login.ObjModuledetailLevel;
import com.example.user.communicator.R;
import com.example.user.communicator.Utility.DividerItemDecoration;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;

public class ColleborateActivity extends BaseActivity {//} implements ConnectivityReceiver.ConnectivityReceiverListener {

    @BindView(R.id.rvColList)
    RecyclerView rvColList;
    @BindView(R.id.tvShare)
    TextView tvShare;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rlColleborators)
    LinearLayout rlColleborators;
    String strUserId;
    ColleborateListAdapter colleborateListAdapter;
    ArrayList<ColleborateDatum> arrayList = new ArrayList<>();
    @BindView(R.id.llToolbar)
    LinearLayout llToolbar;
    @BindView(R.id.etSearch)
    EditText etSearch;

    @BindView(R.id.ivClose)
    ImageView ivClose;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    String activiType = "other", intPlannerId, text;
    ArrayList<ColleborateDatum> arryCollaboratUserData = new ArrayList<>();
    ArrayList<ColleborateDatum> newCollbData = new ArrayList<>();
    ArrayList<ColleborateDatum> multiselectData = new ArrayList<>();
    com.example.user.communicator.Model.Login.Datum mUserDetails;
    String isDelete = "no";
    public static String createdBy;
    Realm mRealm;
    private boolean internetConnected = true;
    public static String strCategoryTypeId,
            strCategoryId,
            strSubCategoryId;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colleborate);

        Typeface font_roboto_regular = Typeface.createFromAsset(getAssets(), "fonts/roboto_regular.ttf");
        Typeface font_roboto_light = Typeface.createFromAsset(getAssets(), "fonts/roboto_light.ttf");

        ButterKnife.bind(this);
        mRealm = Realm.getDefaultInstance();

        SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences("Login", MODE_PRIVATE);
        String data = sharedpreferences.getString("UserData", null);
        Gson gson = new Gson();
        mUserDetails = gson.fromJson(data, com.example.user.communicator.Model.Login.Datum.class);
        strUserId = mUserDetails.getIntUserId();

        toolbar.setTitle("Share To");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_white);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        rvColList.setHasFixedSize(true);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSearch.setText("");
            }
        });

        rvColList.setLayoutManager(layoutManager);
        rvColList.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(this, R.drawable.divider)));
        activiType = getIntent().getStringExtra("Type");

        colleborateListAdapter = new ColleborateListAdapter(this, arryCollaboratUserData);
        createdBy = getIntent().getStringExtra("createdBy");
        strCategoryTypeId = getIntent().getStringExtra("strCategoryTypeId");
        strCategoryId = getIntent().getStringExtra("strCategoryId");
        strSubCategoryId = getIntent().getStringExtra("strSubCategoryId");
        newCollbData.addAll(colleborateListAdapter.getSelectedData());

        if (activiType.equals("NoteDetail")) {

            intPlannerId = getIntent().getStringExtra("intPlannerId");
            arryCollaboratUserData = getIntent().getParcelableArrayListExtra("Colleborators");

            if (arryCollaboratUserData.size() > 0) {
                multiselectData.addAll(arryCollaboratUserData);
            }

            if (arryCollaboratUserData.size() != 0) {
                rlColleborators.setVisibility(View.VISIBLE);

                for (int i = 0; i < arryCollaboratUserData.size(); i++) {

                    LinearLayout linearLayout = (LinearLayout) View.inflate(ColleborateActivity.this, R.layout.list_item_colleborate, null);


                    ((TextView) linearLayout.findViewById(R.id.tvName)).setTypeface(font_roboto_regular);
                    ((TextView) linearLayout.findViewById(R.id.tvDesignation)).setTypeface(font_roboto_light);

                    ImageView cvImage = linearLayout.findViewById(R.id.cvImage);
                    RelativeLayout cvSelected = linearLayout.findViewById(R.id.imgSelect);
                    LinearLayout lllMain = linearLayout.findViewById(R.id.lllMain);

                    final ImageView ivClose = linearLayout.findViewById(R.id.ivClose);
                    ivClose.setVisibility(View.GONE);

                    Glide.with(ColleborateActivity.this)
                            .load(arryCollaboratUserData.get(i).getImgPic())
                            .placeholder(R.drawable.ic_placeholder)
                            .dontAnimate()
                            .into(cvImage);

                    Boolean isOwner = false;
                    final String lastName, firstName;
                    if (!arryCollaboratUserData.get(i).getId().equals(strUserId)) {
                        newCollbData.add(arryCollaboratUserData.get(i));
                        cvSelected.setVisibility(View.VISIBLE);
                        String name = arryCollaboratUserData.get(i).getItemName();
                        final String[] split = name.split(":");
                        firstName = split[0];
                        if (split.length == 2) {
                            lastName = split[1];
                            ((TextView) linearLayout.findViewById(R.id.tvName)).setText(firstName + " " + lastName);
                        } else {
                            ((TextView) linearLayout.findViewById(R.id.tvName)).setText(firstName);
                        }
//                        ((TextView) linearLayout.findViewById(R.id.tvName)).setText(arryCollaboratUserData.get(i).getItemName());
                        ((TextView) linearLayout.findViewById(R.id.tvDesignation)).setText(arryCollaboratUserData.get(i).getCategory());

                        int finalI1 = i;
                        lllMain.setOnClickListener(view -> {

                            if (isNetworkAvailable()) {
                                String val = arryCollaboratUserData.get(finalI1).getId();
                                for (int j = 0; j < newCollbData.size(); j++) {
                                    if (newCollbData.get(j).getId().equals(arryCollaboratUserData.get(finalI1).getId())) {
                                        newCollbData.remove(j);
                                    } else {
                                    }
                                }
                                rlColleborators.removeView(linearLayout);
                                isDelete = "yes";
                                shareTo(newCollbData);//multiselectData

                            } else {
                                showToast(getString(R.string.noConnection));
                            }

                        });

                        rlColleborators.addView(linearLayout);
                    } else {

//                        Log.e("Created",ColleborateActivity.createdBy+"..."+strUserId+"..."+arryCollaboratUserData.get(i).getId());
                        if (ColleborateActivity.createdBy.equals(arryCollaboratUserData.get(i).getId())) {
                            cvSelected.setVisibility(View.GONE);
                            isOwner = true;
                            String name = arryCollaboratUserData.get(i).getItemName();
                            final String[] split = name.split(":");
                            firstName = split[0];
                            if (split.length == 2) {
                                lastName = split[1];
                                ((TextView) linearLayout.findViewById(R.id.tvName)).setText(firstName + " " + lastName);
                            } else {
                                ((TextView) linearLayout.findViewById(R.id.tvName)).setText(firstName);
                            }
                            String val = ((TextView) linearLayout.findViewById(R.id.tvName)).getText() + "<i>" + " (Owner)" + "</i> ";
                            ((TextView) linearLayout.findViewById(R.id.tvName)).setText(Html.fromHtml(val));
                            Toast.makeText(this, ColleborateActivity.createdBy + "...Same..." + arryCollaboratUserData.get(i).getId(), Toast.LENGTH_LONG).show();
                            rlColleborators.addView(linearLayout, 0);
                        }

//                        if(arryCollaboratUserData.get(finalI1).getId().equals(strUserId)){
//                                new AlertDialog.Builder(this)
//                                        .setTitle("")
//                                        .setMessage("This note will no longer be shared with you.")
//                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                                            @Override
//                                            public void onClick(DialogInterface dialog, int which) {
//
//                                                dialog.dismiss();
//                                            }
//                                        })
//                                        .show();
//                            }
                    }
                }
            }
        } else {
            intPlannerId = getIntent().getStringExtra("intPlannerId");
//            strPlannerMainId = getIntent().getStringExtra("strPlannerMainId");
            arryCollaboratUserData.clear();
        }

        boolean isConnected = ConnectivityReceiver.isConnected();
        if (isConnected) {

            etSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    String val = etSearch.getText().toString();
                    if (TextUtils.isEmpty(val)) {

//                            tvShare.setVisibility(View.INVISIBLE);
                        ivClose.setVisibility(View.GONE);
                    } else {
                        ivClose.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    String val = etSearch.getText().toString();
                    if (TextUtils.isEmpty(val)) {

//                            tvShare.setVisibility(View.INVISIBLE);
                        ivClose.setVisibility(View.GONE);
                    } else {
                        ivClose.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                    String val = etSearch.getText().toString();
                    if (TextUtils.isEmpty(val)) {

//                            tvShare.setVisibility(View.INVISIBLE);
                        ivClose.setVisibility(View.GONE);
                    } else {
                        ivClose.setVisibility(View.VISIBLE);

                    }
                    arrayList = new ArrayList<>();
                    text = val;
                    getColleborateData(val);

                }
            });

        } else {

            etSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    String val = etSearch.getText().toString();
                    if (TextUtils.isEmpty(val)) {

//                            tvShare.setVisibility(View.INVISIBLE);
                        ivClose.setVisibility(View.GONE);
                    } else {
                        ivClose.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    String val = etSearch.getText().toString();
                    if (TextUtils.isEmpty(val)) {

//                            tvShare.setVisibility(View.INVISIBLE);
                        ivClose.setVisibility(View.GONE);
                    } else {
                        ivClose.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                    String val = etSearch.getText().toString();
                    if (TextUtils.isEmpty(val)) {

//                            tvShare.setVisibility(View.INVISIBLE);
                        ivClose.setVisibility(View.GONE);
                    } else {
                        ivClose.setVisibility(View.VISIBLE);
                    }
                    arrayList = new ArrayList<>();
                    text = val;
                    getColleborateData(val);
                }
            });

        }

    }

    @Override
    public void onBackPressed() {

        if (activiType.equals("AddNewNote")) {

            Intent intent = new Intent();
            intent.putExtra("ID", intPlannerId);
            intent.putParcelableArrayListExtra("Colleborators", multiselectData);
            (ColleborateActivity.this).setResult(3, intent);
            finish();

        } else {

            Intent intent = new Intent();
            intent.putParcelableArrayListExtra("Colleborators", multiselectData);
            intent.putExtra("ID", intPlannerId);
            (ColleborateActivity.this).setResult(4, intent);
            finish();

        }
        super.onBackPressed();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @OnClick({R.id.ivBack, R.id.tvShare})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.ivBack:

                onBackPressed();
                break;

            case R.id.tvShare:

                if (isNetworkAvailable()) {
                    shareTo(newCollbData);
                } else {
                    showToast(getString(R.string.noConnection));
                }

                break;
        }
    }

    public void getColleborateData(String text) {

        String intModuleId = null;
        ArrayList<ObjModuledetailLevel> objModuledetailLevels = new ArrayList<ObjModuledetailLevel>(mUserDetails.getArryObjModuleLevel().get(0).getObjModuledetails());

        for (ObjModuledetailLevel objDetailLevel : objModuledetailLevels) {
            if (objDetailLevel.getStrModuleName().equals("PLANNERNOTE")) {
                intModuleId = objDetailLevel.getId();
            }
        }
        if (intModuleId != null) {

            if (isNetworkAvailable()) {
                RequestParams params = new RequestParams();
                params.put("intModuleId", intModuleId);
                params.put("intLevelTypeId", mUserDetails.getIntLevelTypeId());
                params.put("intMemberTypeId", mUserDetails.getIntMemberTypeId());
                if (arryCollaboratUserData.size() != 0) {
                    params.put("intPlannerId", String.valueOf(intPlannerId));
                }
                ApiCallWithToken(urlColList, ApiCall.COLLEBORATELIST, params);
            } else {
                arrayList.clear();
                RealmResults<ColleborateDatum> itemsRealmResults = mRealm.where(ColleborateDatum.class).findAll();
                if (itemsRealmResults.size() != 0) {
                    arrayList.addAll(itemsRealmResults);
                    filter(arrayList, text);
                }
            }
        }
    }

    public void shareTo(final ArrayList<ColleborateDatum> multiselect) {

        multiselect.addAll(colleborateListAdapter.getSelectedData());

        RequestParams params = new RequestParams();

        JSONArray collborteId = new JSONArray();
        if (multiselect.size() > 0) {
            JSONObject list = new JSONObject();
            try {
                list.put("id", strUserId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            collborteId.put(list);
        }
        try {
            for (int i = 0; i < multiselect.size(); i++) {
                if (multiselect.get(i).getId() != null) {
                    JSONObject list1 = new JSONObject();
                    list1.put("id", multiselect.get(i).getId());
                    collborteId.put(list1);

                }
            }
        } catch (JSONException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        params.put("arryCollaboratUserId", collborteId.toString());//moduleId

//        Log.e("multiselect_json", "<....>" + multiselect.size() + "..." + collborteId.length() + "...." + collborteId);

        if (intPlannerId.equals("0")) {
            params.put("intPlannerId", intPlannerId);
            params.put("strCreateUserId", strUserId);
        } else {
            params.put("intPlannerId", intPlannerId);
            params.put("strModifiedUserId", strUserId);
        }

        params.put("strCategoryTypeId", strCategoryTypeId);
        params.put("strCategoryId", strCategoryId);
        params.put("strSubCategoryId", strSubCategoryId);

        showProgressDialog(this, "Please Wait...");
        ApiCallWithToken(urlColSave, ApiCall.COLLABORATESAVE, params);
    }

    @Override
    public void OnResponce(JSONObject data, ApiCall type) {
        JSONArray array = null;
        try {
            if (type == ApiCall.COLLEBORATELIST) {
                array = data.getJSONArray("data");
                hideProgressDialog(ColleborateActivity.this);
                if (array.length() != 0) {

                    Gson gson = new Gson();
                    arrayList.clear();
                    mRealm.beginTransaction();
                    for (int i = 0; i < array.length(); i++) {
                        try {
                            JSONObject object = array.getJSONObject(i);
                            ColleborateDatum item = gson.fromJson(object.toString(), ColleborateDatum.class);
//                            if (!item.getId().contains(mUserDetails.getIntUserId())) {
                            arrayList.add(item);
//                            }
                            mRealm.copyToRealmOrUpdate(item);
                            filter(arrayList, text);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    mRealm.commitTransaction();
                }
            } else if (type == ApiCall.COLLABORATESAVE) {
                String planer_ID = null;
                hideProgressDialog(ColleborateActivity.this);
                array = data.getJSONArray("data");
                if (array.length() != 0) {
                    for (int i = 0; i < array.length(); i++) {

                        try {
                            JSONObject object = array.getJSONObject(i);
                            planer_ID = object.getString("intPlannerId");
                            Gson gson = new Gson();
                            ColleborateDatum item = gson.fromJson(object.toString(), ColleborateDatum.class);
                            arryCollaboratUserData.add(item);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    if (isDelete.equals("no")) {//works only if it is not delte

                        if (activiType.equals("AddNewNote")) {

                            Intent intent = new Intent();
                            intent.putExtra("ID", planer_ID);
                            intent.putParcelableArrayListExtra("Colleborators", multiselectData);
                            (ColleborateActivity.this).setResult(3, intent);
                            finish();

                        } else {

                            Intent intent = new Intent();
                            intent.putParcelableArrayListExtra("Colleborators", multiselectData);
                            intent.putExtra("ID", planer_ID);
                            (ColleborateActivity.this).setResult(4, intent);
                            finish();

                        }
                    } else {
                        isDelete = "no";
                    }

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        super.OnResponce(data, type);
    }

    public void filter(ArrayList<ColleborateDatum> arrayList, String charText) {
        ArrayList<ColleborateDatum> mainDataList = new ArrayList<ColleborateDatum>();
        charText = charText.toLowerCase(Locale.getDefault());
        mainDataList.clear();
        rvColList.removeAllViews();

        if (charText.length() == 0) {
            mainDataList.clear();
            colleborateListAdapter = new ColleborateListAdapter(ColleborateActivity.this, mainDataList);
            rvColList.setAdapter(colleborateListAdapter);
            colleborateListAdapter.notifyDataSetChanged();
        } else {

            for (ColleborateDatum wp : arrayList) {

                if (!mainDataList.contains(wp)) {
                    if (wp.getItemName().toLowerCase(Locale.getDefault()).startsWith(charText)) {
                        mainDataList.add(wp);
                    }
                } else {
                    mainDataList.remove(wp);
                }

                isDelete = "no";
                colleborateListAdapter = new ColleborateListAdapter(ColleborateActivity.this, mainDataList);
                rvColList.setAdapter(colleborateListAdapter);
                colleborateListAdapter.notifyDataSetChanged();
                if (mainDataList.size() == 0) {

//                            tvShare.setVisibility(View.INVISIBLE);
                } else tvShare.setVisibility(View.VISIBLE);

            }
        }
    }

    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = Application.getConnectivityStatusString(context);
            setSnackbarMessage(status, false);
        }
    };

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerInternetCheckReceiver();
    }

    private void registerInternetCheckReceiver() {
        IntentFilter internetFilter = new IntentFilter();
        internetFilter.addAction("android.net.wifi.STATE_CHANGE");
        internetFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(broadcastReceiver, internetFilter);
    }

    private void setSnackbarMessage(String status, boolean showBar) {
        String internetStatus = "";
        Snackbar snackbar;
        if (status.equalsIgnoreCase("Wifi enabled") || status.equalsIgnoreCase("Mobile data enabled")) {
            internetStatus = "Connecting..";
        } else {
            internetStatus = "No Internet Connection";
        }

        if (internetStatus.equalsIgnoreCase("No Internet Connection")) {
            if (internetConnected) {
                snackbar = Snackbar.make(findViewById(R.id.rvColList), internetStatus, Snackbar.LENGTH_INDEFINITE);

                snackbar.setActionTextColor(Color.WHITE);
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbar.show();
                internetConnected = false;
                etSearch.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        String val = etSearch.getText().toString();
                        if (TextUtils.isEmpty(val)) {

//                            tvShare.setVisibility(View.INVISIBLE);
                            ivClose.setVisibility(View.GONE);
                        } else {
                            ivClose.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        String val = etSearch.getText().toString();
                        if (TextUtils.isEmpty(val)) {

//                            tvShare.setVisibility(View.INVISIBLE);
                            ivClose.setVisibility(View.GONE);
                        } else {
                            ivClose.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String val = etSearch.getText().toString();
                        if (TextUtils.isEmpty(val)) {

//                            tvShare.setVisibility(View.INVISIBLE);
                            ivClose.setVisibility(View.GONE);
                        } else {
                            ivClose.setVisibility(View.VISIBLE);
                        }
                        arrayList = new ArrayList<>();
                        text = val;
                        getColleborateData(val);
                    }
                });
            }
        } else {
            if (!internetConnected) {
                internetConnected = true;
                snackbar = Snackbar.make(findViewById(R.id.rvColList), internetStatus, Snackbar.LENGTH_LONG);
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);

                snackbar.setActionTextColor(Color.RED);
                snackbar.show();

                etSearch.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        String val = etSearch.getText().toString();
                        if (TextUtils.isEmpty(val)) {

//                            tvShare.setVisibility(View.INVISIBLE);
                            ivClose.setVisibility(View.GONE);
                        } else {
                            ivClose.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        String val = etSearch.getText().toString();
                        if (TextUtils.isEmpty(val)) {
//
//                            tvShare.setVisibility(View.INVISIBLE);
                            ivClose.setVisibility(View.GONE);
                        } else {
                            ivClose.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        String val = etSearch.getText().toString();
                        if (TextUtils.isEmpty(val)) {
//
//                            tvShare.setVisibility(View.INVISIBLE);
                            ivClose.setVisibility(View.GONE);
                        } else {
                            ivClose.setVisibility(View.VISIBLE);
                        }
                        arrayList = new ArrayList<>();
                        text = val;
                        getColleborateData(val);
                    }
                });
            }
        }
    }

}
