package com.example.user.communicator.Model.ChatModels;


import java.util.ArrayList;

public class ChatItem {

    private LastMessageData lastMessageData;
    private ArrayList<String> memberIds = new ArrayList<>();
    private String senderId;
    private String _id;
    private String intUserId;
    private String name;
    private long addTime = 0;
    private int status;

    public LastMessageData getLastMessageData() {
        return lastMessageData;
    }

    public void setLastMessageData(LastMessageData lastMessageData) {
        this.lastMessageData = lastMessageData;
    }

    public ArrayList<String> getMemberIds() {
        return memberIds;
    }

    public void setMemberIds(ArrayList<String> memberIds) {
        this.memberIds = memberIds;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getIntUserId() {
        return intUserId;
    }

    public void setIntUserId(String intUserId) {
        this.intUserId = intUserId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getAddTime() {
        return addTime;
    }

    public void setAddTime(long addTime) {
        this.addTime = addTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
