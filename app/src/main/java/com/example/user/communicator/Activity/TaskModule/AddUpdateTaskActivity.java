package com.example.user.communicator.Activity.TaskModule;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.user.communicator.CustomViews.CustomButton;
import com.example.user.communicator.CustomViews.CustomEditText;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.TaskModels.TaskItems;
import com.example.user.communicator.R;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class AddUpdateTaskActivity extends BaseActivity implements View.OnClickListener {


    @BindView(R.id.etName)
    CustomEditText mEtName;

    @BindView(R.id.ivPriority)
    ImageView mPriority;

    @BindView(R.id.deleteFile)
    ImageView mDeleteFile;

    @BindView(R.id.deleteImage)
    ImageView mDeleteImage;

    @BindView(R.id.imgSelection)
    RelativeLayout mImgSelection;

    @BindView(R.id.fileSelection)
    RelativeLayout mFileSelection;

    @BindView(R.id.startDateSelection)
    RelativeLayout mStartDateSelection;

    @BindView(R.id.endDateSelection)
    RelativeLayout mEndDateSelection;

    @BindView(R.id.btnSubmit)
    CustomButton mBtnSubmit;

    @BindView(R.id.img)
    CustomTextView mImagPathTxt;


    @BindView(R.id.file)
    CustomTextView mFilePathTxt;

    @BindView(R.id.startDate)
    CustomTextView mStartDate;

    @BindView(R.id.endDate)
    CustomTextView mEndDate;


    boolean isImage = false, isFile = false, isUpdate = false;
    long minDate = 0, maxDate = 0;
    String mPriorityValue;
    TaskItems taskItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_task);
        ButterKnife.bind(this);

        mImgSelection.setOnClickListener(this);
        mImagPathTxt.setOnClickListener(this);
        mFilePathTxt.setOnClickListener(this);
        mFileSelection.setOnClickListener(this);
        mStartDateSelection.setOnClickListener(this);
        mStartDate.setOnClickListener(this);
        mEndDate.setOnClickListener(this);
        mEndDateSelection.setOnClickListener(this);
        mBtnSubmit.setOnClickListener(this);
        mPriority.setOnClickListener(this);
        mDeleteFile.setOnClickListener(this);
        mDeleteImage.setOnClickListener(this);

        minDate = System.currentTimeMillis();
        isUpdate = getIntent().getBooleanExtra("isUpdate", false);
        if (isUpdate) {
            String items = getIntent().getStringExtra("item");
            Gson gson = new Gson();
            taskItems = gson.fromJson(items, TaskItems.class);
            setData(taskItems);
        }
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public void onClick(View v) {

        if (v == mImagPathTxt) {
            if (mayRequest()) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, 5555);
            }
        }
        if (v == mFilePathTxt) {
            if (mayRequest()) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("application/pdf");
                startActivityForResult(intent, 5252);
            }
        }
        if (v == mStartDate) {
            if (!TextUtils.isEmpty(mEndDate.getText().toString())) {
                maxDate = stringToMills(mEndDate.getText().toString());
            }
            showDatePicker(mStartDate, System.currentTimeMillis(), maxDate);
        }
        if (v == mEndDate) {
            if (!TextUtils.isEmpty(mStartDate.getText().toString())) {
                minDate = stringToMills(mStartDate.getText().toString());
            }
            showDatePicker(mEndDate, minDate, 0);
        }
        if (v == mBtnSubmit) {
            submitTask();
        }
        if (v == mPriority) {
            showPriorityDilogue();
        }
        if (v == mDeleteImage) {
            mImagPathTxt.setText("");
            isImage = false;
            mDeleteImage.setVisibility(View.GONE);
        }
        if (v == mDeleteFile) {
            mFilePathTxt.setText("");
            isFile = false;
            mDeleteFile.setVisibility(View.GONE);
        }
    }

    public void setData(TaskItems taskItems) {
//        mEtName.setText(taskItems.getTitle());
//        mStartDate.setText(taskItems.getStartDate());
//        mEndDate.setText(taskItems.getEndDate());
//
//
//        minDate = stringToMills(taskItems.getStartDate());
//        maxDate = stringToMills(taskItems.getEndDate());
//
//        if (taskItems.getPriority().equals("1")) {
//            mPriorityValue = "1";
//            mPriority.setImageResource(R.drawable.ic_prior_blue);
//        } else if (taskItems.getPriority().equals("2")) {
//            mPriorityValue = "2";
//            mPriority.setImageResource(R.drawable.ic_prior_orange);
//
//        } else if (taskItems.getPriority().equals("3")) {
//            mPriorityValue = "3";
//            mPriority.setImageResource(R.drawable.ic_prior_red);
//        } else if (taskItems.getPriority().equals("4")) {
//            mPriorityValue = "4";
//            mPriority.setImageResource(R.drawable.ic_prior_default);
//        }


//        if (!TextUtils.isEmpty(taskItems.getImagePath())) {
//            mImagPathTxt.setText(taskItems.getImagePath());
//            mDeleteImage.setVisibility(View.VISIBLE);
//        }
//        if (!TextUtils.isEmpty(taskItems.getFilePath())) {
//            mFilePathTxt.setText(taskItems.getFilePath());
//            mDeleteFile.setVisibility(View.VISIBLE);
//        }
    }

    public void showPriorityDilogue() {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Choose priority");
        final String[] priority;
        if (isUpdate) {
            priority = new String[]{"Low ", "Medium ", "High ", "Done "};
        } else {
            priority = new String[]{"Low ", "Medium ", "High "};
        }

        adb.setItems(priority, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                String clickedItemValue = Arrays.asList(priority).get(which);
                if (clickedItemValue.equals("Low ")) {
                    mPriorityValue = "1";
                    mPriority.setImageResource(R.drawable.ic_prior_blue);
                } else if (clickedItemValue.equals("Medium ")) {
                    mPriorityValue = "2";
                    mPriority.setImageResource(R.drawable.ic_prior_orange);

                } else if (clickedItemValue.equals("High ")) {
                    mPriorityValue = "3";
                    mPriority.setImageResource(R.drawable.ic_prior_red);
                } else if (clickedItemValue.equals("Done ")) {
                    mPriorityValue = "4";
                    mPriority.setImageResource(R.drawable.ic_prior_default);
                }
            }
        });
        adb.show();
    }

    public void submitTask() {
//        String etText = mEtName.getText().toString().trim();
//        String imagePath = mImagPathTxt.getText().toString().trim();
//        String filePath = mFilePathTxt.getText().toString().trim();
//        String startDate = mStartDate.getText().toString().trim();
//        String endDate = mEndDate.getText().toString().trim();
//
//        if (TextUtils.isEmpty(etText)) {
//            showToast("Please enter task description!");
//            return;
//        }
//        if (TextUtils.isEmpty(startDate)) {
//            showToast("Please select start date!");
//            return;
//        }
//        if (TextUtils.isEmpty(endDate)) {
//            showToast("Please select end date!");
//            return;
//        }
//        if (TextUtils.isEmpty(mPriorityValue)) {
//            showToast("Please select task priority!");
//            return;
//        }
//
//        TaskItems item = new TaskItems();
//        item.setTitle(etText);
//        item.setStartDate(startDate);
//        item.setEndDate(endDate);
//        item.setPriority(mPriorityValue);
//        ArrayList<TaskItems> items = getTaskItems();
//        if (isUpdate) {
//            boolean isF = false;
//            for (int i = 0; i < items.size() && !isF; i++) {
//                if (items.get(i).get_id().equalsIgnoreCase(taskItems.get_id())) {
//                    items.set(i, item);
//                    isF = true;
//                }
//            }
//        } else {
//            item.set_id(String.valueOf(items.size()));
//            Gson gson = new Gson();
//            boolean isUsers = getIntent().getBooleanExtra("isUsers", false);
//            if (isUsers) {
//                String users = getIntent().getStringExtra("users");
//                Type type = new TypeToken<ArrayList<SelectionItem>>() {
//                }.getType();
//                ArrayList<SelectionItem> usersItems = gson.fromJson(users, type);
//                item.setUsers(usersItems);
//            } else {
//                String levels = getIntent().getStringExtra("levels");
//                String membertypes = getIntent().getStringExtra("membertypes");
//                Type type = new TypeToken<ArrayList<SelectionItem>>() {
//                }.getType();
//                ArrayList<SelectionItem> levelsItems = gson.fromJson(levels, type);
//                ArrayList<SelectionItem> membersItem = gson.fromJson(membertypes, type);
//                item.setLevels(levelsItems);
//                item.setMemberTypes(membersItem);
//            }
//            items.add(0, item);
//        }
//        setTaskItems(items);
//        Intent i = new Intent();
//        setResult(RESULT_OK, i);
//        finish();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null && data.getData() != null) {
            if (requestCode == 5555) {
                Uri uri = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                    bitmap = compressedBitmap(bitmap);
                    String path = storeImage(bitmap);
                    mImagPathTxt.setText(path);
                    isImage = true;
                    mDeleteImage.setVisibility(View.VISIBLE);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (data != null && data.getData() != null) {
            if (requestCode == 5252 && resultCode == RESULT_OK) {
                Uri selectedPdf = data.getData();
                if (selectedPdf.getLastPathSegment().endsWith("pdf")) {
                    String displayName = getPath(this, selectedPdf);
                    mFilePathTxt.setText(displayName);
                    isFile = true;
                    mDeleteFile.setVisibility(View.VISIBLE);
                }
            }
        }
    }


    private boolean mayRequest() {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

}
