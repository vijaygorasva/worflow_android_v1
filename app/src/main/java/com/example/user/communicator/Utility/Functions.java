package com.example.user.communicator.Utility;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.Window;
import android.widget.TextView;


import com.example.user.communicator.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Functions {

    public static ProgressDialog pDialog;
    public static void showAlert(String title, String message, Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_error_message);
        TextView dialogHead = (TextView) dialog.findViewById(R.id.dialogHead);
        TextView dialogBody = (TextView) dialog.findViewById(R.id.dialogBody);
        TextView btOk = (TextView) dialog.findViewById(R.id.btOk);
        dialogHead.setText(title);
        dialogBody.setText(message);
        btOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public static void showProgressDialog(Context ct, String message) {
        if (pDialog != null && pDialog.isShowing()) {
            return;
        } else {
            pDialog = new ProgressDialog(ct);
            pDialog.setMessage(message);
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }

    public static void hideProgressDialog(Context ct) {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    public static String formatDate(String inputFormat, String outputFormat, String inputDate) {

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {

        }

        return outputDate;
    }

    public static void bitmapToFile(Context ct, Bitmap bitmap, String fileName) {
        File f = new File(ct.getFilesDir(), fileName);
        try {
            f.createNewFile();
        } catch (Exception e) {

        }
//Convert bitmap to byte array
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();

//write the bytes in file
        try {
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (Exception e) {

        }
    }
}
