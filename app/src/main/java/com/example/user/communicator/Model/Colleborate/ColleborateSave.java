
package com.example.user.communicator.Model.Colleborate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ColleborateSave {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private DataSave data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataSave getData() {
        return data;
    }

    public void setData(DataSave data) {
        this.data = data;
    }

}
