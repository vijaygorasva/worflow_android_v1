package com.example.user.communicator.Activity.ChatModule;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.user.communicator.Activity.TaskModule.BaseActivity;
import com.example.user.communicator.Activity.TaskModule.ImageShowActivity;
import com.example.user.communicator.Adapter.ChatAdapters.MessageListAdapter;
import com.example.user.communicator.CustomViews.CustomEditText;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.ChatModels.Messages;
import com.example.user.communicator.Model.ChatModels.MessagesData;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Mqtt.Interfaces;
import com.example.user.communicator.Mqtt.MqttUtil;
import com.example.user.communicator.R;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;
import com.michael.easydialog.EasyDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.example.user.communicator.Mqtt.Interfaces.mPublishMessage;

public class ChatDetailActivity extends BaseActivity implements View.OnClickListener {

    @BindView(R.id.cameraPickerBtn)
    ImageView mCameraPickerBtn;
    @BindView(R.id.back)
    ImageView mBack;
    @BindView(R.id.sendMsg)
    ImageView mSend;
    @BindView(R.id.pimg)
    ImageView mProfileImg;
    @BindView(R.id.name)
    CustomTextView toolTitle;
    @BindView(R.id.msgListView)
    RecyclerView mMessageRecycler;
    @BindView(R.id.vLoading)
    LinearLayout vLoading;
    @BindView(R.id.edittext_chatbox)
    CustomEditText mTxtMsg;
    @BindView(R.id.audioBtn)
    ImageView mAudioBtn;

    @BindView(R.id.timeTxt)
    CustomTextView timeTxt;
    @BindView(R.id.audioView)
    RelativeLayout mAudioView;


    private LinearLayoutManager mLinearLayoutManager;
    private MessageListAdapter mMessageAdapter;
    public ArrayList<Messages> messageArray = new ArrayList<>();
    public Gson g;
    String mCurruntUser, mUserName, LastMsgId;
    Datum mUserData;
    boolean isAllget = false, happen = true, IsHappen = true, isChatUpdated = false;
    boolean isFirst = true;
    private boolean isRecordStart = false;
    private MediaRecorder recorder = null;
    private String fileName = null;
    private boolean isImage = false;
    private boolean isAudio = false;
    private boolean isFile = false;
    private CountDownTimer countDownTimer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_details);
        ButterKnife.bind(this);


        mUserData = getLoginData();
        Interfaces.setgetNewMessageListener(newMessageListener);
        mCurruntUser = getIntent().getStringExtra("userId");
        mUserName = getIntent().getStringExtra("userName");
        MqttUtil.curruntChatUser = mCurruntUser;
        toolTitle.setText(mUserName);
        mBack.setOnClickListener(this);
        mCameraPickerBtn.setOnClickListener(this);
        mAudioBtn.setOnClickListener(this);
        g = new Gson();
        mMessageAdapter = new MessageListAdapter(this, messageArray, mUserData.getIntUserId(), mUserName);
        mLinearLayoutManager = new LinearLayoutManager(this);

        mMessageRecycler.setLayoutManager(mLinearLayoutManager);
        mMessageRecycler.setAdapter(mMessageAdapter);

        mMessageAdapter.setOnImageClickListener(onImageClickListener);


        mAudioBtn.setOnTouchListener((v, e) -> {
            switch (e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    startRec();
                    break;
                case MotionEvent.ACTION_MOVE:

                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    stopRecording();
                    break;
            }
            return false;
        });


        mSend.setOnClickListener(v -> {
            if (mTxtMsg.getText().toString().equalsIgnoreCase("")) {
                Toast.makeText(ChatDetailActivity.this, "Please enter Message . ", Toast.LENGTH_SHORT).show();
                return;
            }
            String topic = "new_message/w_group_" + mCurruntUser;
            JSONObject jsonObject = new JSONObject();
            Messages msg = new Messages();
            msg.setSenderId(mUserData.getIntUserId());
            msg.setGroupId(mCurruntUser);
            msg.setMessage(mTxtMsg.getText().toString());
            msg.setMediaPath("");
            msg.setMediaType("");
            msg.setHaveMedia(false);
            msg.setSendTime(System.currentTimeMillis());
            msg.setDate(LongToDate(msg.getSendTime()));
            try {
                jsonObject.put("senderId", mUserData.getIntUserId());
                jsonObject.put("groupId", mCurruntUser);
                jsonObject.put("strFirstName", mUserData.getStrFirstName());
                jsonObject.put("strLastName", mUserData.getStrLastName());
                jsonObject.put("message", mTxtMsg.getText().toString());
                jsonObject.put("mediaPath", "");
                jsonObject.put("isHaveMedia", false);
                jsonObject.put("sendTime", System.currentTimeMillis());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (isNetworkConnected()) {
                if (MqttUtil.client != null) {
                    if (MqttUtil.client.isConnected()) {
                        messageArray.add(msg);
                        String js = jsonObject.toString();
                        mPublishMessage.PublishMessage(topic, js);
                        mMessageAdapter.notifyDataSetChanged();
                        mMessageRecycler.scrollToPosition(messageArray.size() - 1);
                        mTxtMsg.setText("");
                        isChatUpdated = true;
                    } else {
                        connectMqtt(mCurruntUser);
                        Toast.makeText(ChatDetailActivity.this, "Message not send! please try again!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    connectMqtt(mCurruntUser);
                }
            } else {
                Toast.makeText(ChatDetailActivity.this, "No network connection!", Toast.LENGTH_SHORT).show();
            }
        });
//        mMessageRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//                try {
//                    if (LastMsgId.equals(messageArray.get(0).get_id())) {
//                        if (IsHappen) {
//                            if (getFirstVisibleItemPosition() == 0 && !isAllget && happen) {
//                                happen = false;
//                                RequestParams params = new RequestParams();
//                                params.put("intUserId", mUserData.getIntUserId());
//                                params.put("groupId", mCurruntUser);
////                                params.put("lastMsgId", LastMsgId);
//                                ApiCall(getHistoryChatGroup, ApiCall.GET_HISTORY_CHAT_GROUP, params);
//                            }
//                        }
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//        });
        if (isNetworkConnected()) {
            vLoading.setVisibility(View.VISIBLE);
            RequestParams params = new RequestParams();
            params.put("intUserId", mUserData.getIntUserId());
            params.put("groupId", mCurruntUser);
            ApiCall(getHistoryChatGroup, ApiCall.GET_HISTORY_CHAT_GROUP, params);
        } else {
            vLoading.setVisibility(View.GONE);
            showToast("No Connection!");
        }
        connectMqtt(mCurruntUser);

    }

    @Override
    public void onClick(View v) {
        if (v == mBack) {
            onBackPressed();
        }
        if (v == mCameraPickerBtn) {
            openDialog();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (data != null && data.getData() != null) {
            if (requestCode == 5555) {
                Uri uri = data.getData();
                Intent i = new Intent(this, ImageScreenActivity.class);
                i.putExtra("uri", String.valueOf(uri));
                startActivityForResult(i, 9999);
            }
        }
        if (data != null && data.hasExtra("uri")) {
            if (requestCode == 9999) {
                try {
                    Uri uri = Uri.parse(data.getStringExtra("uri"));
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                    bitmap = compressedBitmap(bitmap);
                    String path = storeImage(bitmap);
                    if (isNetworkConnected()) {
                        vLoading.setVisibility(View.VISIBLE);
                        try {
                            File myFile = new File(path);
                            RequestParams params = new RequestParams();
                            params.put("media", myFile);
                            isAudio = false;
                            isImage = true;
                            isFile = false;
                            ApiCall(addMediaChatGroup, ApiCall.ADD_MEDIA_GROUP, params);
                        } catch (FileNotFoundException e) {
                            vLoading.setVisibility(View.GONE);
                            showToast("Unknown error!");
                            e.printStackTrace();
                        }
                    } else {
                        showToast("No Connection!");
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (data != null && data.getData() != null) {
            if (requestCode == 5252 && resultCode == RESULT_OK) {
                Uri selectedPdf = data.getData();
                String mainPath = getPath(this, selectedPdf);
                sendFile(mainPath);
            }
        }
    }

    @Override
    public void OnError(JSONObject object, ApiCall type) {
        vLoading.setVisibility(View.GONE);
    }

    @Override
    public void OnResponce(JSONObject o, ApiCall type) {
        vLoading.setVisibility(View.GONE);
        if (type == ApiCall.GET_HISTORY_CHAT_GROUP) {
            if (TextUtils.isEmpty(LastMsgId)) {
                try {
                    Gson gson = new Gson();
                    JSONArray array = o.getJSONObject("data").getJSONArray("messagesData");
                    ArrayList<Messages> messageArrayList = new ArrayList<>();
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        Messages message = gson.fromJson(object.toString(), Messages.class);
                        messageArrayList.add(message);
                    }

                    if (messageArrayList.size() != 0) {
                        ArrayList<Messages> messagesArrayList = new ArrayList<>(messageArrayList);
                        Collections.reverse(messagesArrayList);
                        messageArray.addAll(messagesArrayList);
                        for (int i = 0; i < messageArray.size(); i++) {
                            messageArray.get(i).setDate(LongToDate(messageArray.get(i).getSendTime()));
                        }
                        LastMsgId = messageArray.get(0).get_id();
                        mMessageAdapter.notifyDataSetChanged();
                        mMessageRecycler.scrollToPosition(messageArray.size() - 1);
//                        if (messageArray.get(messageArray.size() - 1).getStatus() == 0) {
//                            JSONObject jsonObject = new JSONObject();
//                            try {
//                                jsonObject.put("senderId", mCurruntUser);
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            try {
//                                jsonObject.put("receiverId", mUserData.getIntUserId());
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            mPublishMessage.PublishMessage("message_read", jsonObject.toString());
//                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Gson gson = new Gson();
                MessagesData messagesData = gson.fromJson(o.toString(), MessagesData.class);
                if (messagesData.getData() != null) {
                    List<Messages> imessageArray = messagesData.getData().getMessagesList();
                    if (imessageArray != null && imessageArray.size() != 0) {
                        Collections.reverse(imessageArray);
                        for (int i = 0; i < imessageArray.size(); i++) {
                            imessageArray.get(i).setDate(LongToDate(imessageArray.get(i).getSendTime()));
                        }
                        ArrayList<Messages> myObject = new ArrayList<>();
                        myObject.addAll(messageArray);
                        messageArray.clear();
                        messageArray.addAll(imessageArray);
                        messageArray.addAll(myObject);
                        LastMsgId = messageArray.get(0).get_id();
                        mMessageAdapter.notifyDataSetChanged();
                        boolean breakout = true;
                        for (int i = 0; i < messageArray.size() && breakout; i++) {
                            Messages m = messageArray.get(i);
                            if (m.get_id().equals(myObject.get(mLinearLayoutManager.findLastVisibleItemPosition()).get_id())) {
                                mMessageRecycler.scrollToPosition(i);
                                breakout = false;
                            }
                        }
                        imessageArray.clear();
                    } else {
                        isAllget = true;
                    }
                }
            }
            happen = true;
        }
        if (type == ApiCall.ADD_MEDIA_GROUP) {
            try {
                String fileName = o.getJSONObject("data").getString("mediaPath");
                String mediaOriginalName = o.getJSONObject("data").getString("mediaOriginalName");
                if (isImage) {
                    sendImage(fileName, mediaOriginalName);
                }
                if (isAudio) {
                    sendAudio(fileName, mediaOriginalName);
                }
                if (isFile) {
                    sendPdfFile(fileName, mediaOriginalName);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public void sendImage(String image, String mediaOriginalName) {
        String topic = "new_message/w_group_" + mCurruntUser;
        JSONObject jsonObject = new JSONObject();
        Messages msg = new Messages();
        msg.setSenderId(mUserData.getIntUserId());
        msg.setGroupId(mCurruntUser);
        msg.setMessage("");
        msg.setMediaPath(image);
        msg.setHaveMedia(true);
        msg.setMediaType("IMAGE");
        msg.setMediaOriginalName(mediaOriginalName);
        msg.setSendTime(System.currentTimeMillis());
        msg.setDate(LongToDate(msg.getSendTime()));
        try {
            jsonObject.put("senderId", mUserData.getIntUserId());
            jsonObject.put("groupId", mCurruntUser);
            jsonObject.put("strFirstName", mUserData.getStrFirstName());
            jsonObject.put("strLastName", mUserData.getStrLastName());
            jsonObject.put("message", "");
            jsonObject.put("mediaPath", image);
            jsonObject.put("mediaType", "IMAGE");
            jsonObject.put("mediaOriginalName", mediaOriginalName);
            jsonObject.put("isHaveMedia", true);
            jsonObject.put("sendTime", System.currentTimeMillis());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (isNetworkConnected()) {
            if (MqttUtil.client != null) {
                if (MqttUtil.client.isConnected()) {
                    messageArray.add(msg);
                    String js = jsonObject.toString();
                    mPublishMessage.PublishMessage(topic, js);
                    mMessageAdapter.notifyDataSetChanged();
                    mMessageRecycler.scrollToPosition(messageArray.size() - 1);
//                            sendMsg(mTxtMsg.getText().toString());
                    mTxtMsg.setText("");
                    isChatUpdated = true;
                } else {
                    connectMqtt(mCurruntUser);
                    Toast.makeText(ChatDetailActivity.this, "Message not send! please try again!", Toast.LENGTH_SHORT).show();
                }
            } else {
                connectMqtt(mCurruntUser);
            }
        } else {
            Toast.makeText(ChatDetailActivity.this, "No network connection!", Toast.LENGTH_SHORT).show();
        }
    }

    private int getFirstVisibleItemPosition() {
        return mLinearLayoutManager.findFirstVisibleItemPosition();
    }

    Interfaces.getNewMessageListener newMessageListener = new Interfaces.getNewMessageListener() {
        @Override
        public void getNewMessage(String s) {
//            JSONObject jsonObject = new JSONObject();
//            try {
//                jsonObject.put("senderId", mCurruntUser);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            try {
//                jsonObject.put("receiverId", mUserData.getIntUserId());
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            mPublishMessage.PublishMessage("message_read", jsonObject.toString());
            Messages msg = g.fromJson(s, Messages.class);
            msg.setDate(LongToDate(msg.getSendTime()));
            if (!msg.getSenderId().equalsIgnoreCase(mUserData.getIntUserId())) {
                messageArray.add(msg);
                mMessageAdapter.notifyDataSetChanged();
                mMessageRecycler.scrollToPosition(messageArray.size() - 1);
            }
            isChatUpdated = true;
        }
    };
    MessageListAdapter.OnImageClickListener onImageClickListener = (position, path) -> {
        Intent i = new Intent(ChatDetailActivity.this, ImageShowActivity.class);
        i.putExtra("img_url", path);
        startActivity(i);
    };


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == 1001) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, 5555);
            } else {
                showToast("Storage permission is required for image selection!");
            }
        }
        if (requestCode == 1002) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            } else {
                Toast.makeText(this, "Record audio and Storage permissions are required!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent();
        i.putExtra("isChatUpdated", isChatUpdated);
        setResult(RESULT_OK, i);
        finish();
    }

    public String LongToDate(Long Data) {
        Date time = new Date(Data);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        dateFormat.setTimeZone(tz);
        return dateFormat.format(time);
    }

    private boolean mayRequest() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, 1001);
        return false;
    }

    public void startRec() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || checkSelfPermission(RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, 1002);
            } else {
                startRecord();
            }
        }
    }

    private void startRecord() {
        if (!isRecordStart) {
            isRecordStart = true;
            startAudioRecording();
        } else {
            if (countDownTimer != null) {
                countDownTimer.onFinish();
            }
        }
    }

    private void startAudioRecording() {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory() + File.separator + "Worflow");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("Worflow:", "Failed to create directory");
                return;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
        File file = new File(mediaStorageDir.getPath() + File.separator + "AUD_" + timeStamp + ".mp3");
        fileName = file.getAbsolutePath();
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setOutputFile(fileName);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        try {
            recorder.prepare();
        } catch (IOException e) {
            Log.e("Audio:", "prepare() failed");
        }
        mAudioView.setVisibility(View.VISIBLE);
//        mAudioBtn.setScaleX(2f);
//        mAudioBtn.setScaleY(2f);
        startTimer();
        recorder.start();
    }

    private void stopRecording() {
        if (countDownTimer != null) {
            countDownTimer.onFinish();
        }
    }

    public void startTimer() {
        countDownTimer = new CountDownTimer(60000, 1000) {
            public void onTick(long millisUntilFinished) {
                int sec = (int) ((60000 - millisUntilFinished) / 1000);
                if (sec < 10) {
                    timeTxt.setText("00:0" + sec);
                } else if (sec < 60) {
                    timeTxt.setText("00:" + sec);
                } else {
                    timeTxt.setText("01:00");
                }
            }

            @Override
            public void onFinish() {
//                mAudioBtn.setScaleX(1f);
//                mAudioBtn.setScaleY(1f);
                mAudioView.setVisibility(View.GONE);
                isRecordStart = false;
                recorder.stop();
                recorder.release();
                recorder = null;
                sendAudioFile();
            }
        }.start();
    }

    public void openDialog() {
        View view = this.getLayoutInflater().inflate(R.layout.dialog_media, null);
        EasyDialog dialog = new EasyDialog(this)
                .setLayout(view)
                .setBackgroundColor(getResources().getColor(R.color.white))
                .setLocationByAttachedView(mCameraPickerBtn)
                .setGravity(EasyDialog.GRAVITY_TOP)
                .setTouchOutsideDismiss(true)
                .setMatchParent(false)
                .setMarginLeftAndRight(24, 24)
                .setOutsideColor(getResources().getColor(R.color.transparent));
        ImageView mImg = view.findViewById(R.id.img);
        ImageView mFile = view.findViewById(R.id.file);


        mImg.setOnClickListener(v -> {
            if (!isNetworkAvailable()) {
                Toast.makeText(ChatDetailActivity.this, getString(R.string.noConnection), Toast.LENGTH_SHORT).show();
                return;
            }
            if (mayRequest()) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 5555);
                dialog.dismiss();
            }
        });
        mFile.setOnClickListener(v -> {
            if (!isNetworkAvailable()) {
                Toast.makeText(ChatDetailActivity.this, getString(R.string.noConnection), Toast.LENGTH_SHORT).show();
                return;
            }
            if (mayRequest()) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("application/pdf");
                startActivityForResult(intent, 5252);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void sendAudioFile() {
        countDownTimer.cancel();
        if (isNetworkConnected()) {
            vLoading.setVisibility(View.VISIBLE);
            try {
                File myFile = new File(fileName);
                RequestParams params = new RequestParams();
                params.put("media", myFile);
                isAudio = true;
                isImage = false;
                isFile = false;
                ApiCall(addMediaChatGroup, ApiCall.ADD_MEDIA_GROUP, params);
            } catch (FileNotFoundException e) {
                vLoading.setVisibility(View.GONE);
                showToast("Unknown error!");
                e.printStackTrace();
            }
        } else {
            showToast("No Connection!");
        }
    }

    public void sendFile(String path) {
        if (isNetworkConnected()) {
            vLoading.setVisibility(View.VISIBLE);
            try {
                File myFile = new File(path);
                RequestParams params = new RequestParams();
                params.put("media", myFile);
                isAudio = false;
                isImage = false;
                isFile = true;
                ApiCall(addMediaChatGroup, ApiCall.ADD_MEDIA_GROUP, params);
            } catch (FileNotFoundException e) {
                vLoading.setVisibility(View.GONE);
                showToast("Unknown error!");
                e.printStackTrace();
            }
        } else {
            showToast("No Connection!");
        }
    }

    public void sendPdfFile(String image, String mediaOriginalName) {
        String topic = "new_message/w_group_" + mCurruntUser;
        JSONObject jsonObject = new JSONObject();
        Messages msg = new Messages();
        msg.setSenderId(mUserData.getIntUserId());
        msg.setGroupId(mCurruntUser);
        msg.setMessage("");
        msg.setMediaPath(image);
        msg.setHaveMedia(true);
        msg.setMediaType("FILE");
        msg.setMediaOriginalName(mediaOriginalName);
        msg.setSendTime(System.currentTimeMillis());
        msg.setDate(LongToDate(msg.getSendTime()));
        try {
            jsonObject.put("senderId", mUserData.getIntUserId());
            jsonObject.put("groupId", mCurruntUser);
            jsonObject.put("strFirstName", mUserData.getStrFirstName());
            jsonObject.put("strLastName", mUserData.getStrLastName());
            jsonObject.put("message", "");
            jsonObject.put("mediaPath", image);
            jsonObject.put("mediaType", "FILE");
            jsonObject.put("mediaOriginalName", mediaOriginalName);
            jsonObject.put("isHaveMedia", true);
            jsonObject.put("sendTime", System.currentTimeMillis());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (isNetworkConnected()) {
            if (MqttUtil.client != null) {
                if (MqttUtil.client.isConnected()) {
                    messageArray.add(msg);
                    String js = jsonObject.toString();
                    mPublishMessage.PublishMessage(topic, js);
                    mMessageAdapter.notifyDataSetChanged();
                    mMessageRecycler.scrollToPosition(messageArray.size() - 1);
                    mTxtMsg.setText("");
                    isChatUpdated = true;
                } else {
                    connectMqtt(mCurruntUser);
                    Toast.makeText(ChatDetailActivity.this, "Message not send! please try again!", Toast.LENGTH_SHORT).show();
                }
            } else {
                connectMqtt(mCurruntUser);
            }
        } else {
            Toast.makeText(ChatDetailActivity.this, "No network connection!", Toast.LENGTH_SHORT).show();
        }
    }

    public void sendAudio(String image, String mediaOriginalName) {
        String topic = "new_message/w_group_" + mCurruntUser;
        JSONObject jsonObject = new JSONObject();
        Messages msg = new Messages();
        msg.setSenderId(mUserData.getIntUserId());
        msg.setGroupId(mCurruntUser);
        msg.setMessage("");
        msg.setMediaPath(image);
        msg.setHaveMedia(true);
        msg.setMediaType("AUDIO");
        msg.setMediaOriginalName(mediaOriginalName);
        msg.setSendTime(System.currentTimeMillis());
        msg.setDate(LongToDate(msg.getSendTime()));
        try {
            jsonObject.put("senderId", mUserData.getIntUserId());
            jsonObject.put("groupId", mCurruntUser);
            jsonObject.put("strFirstName", mUserData.getStrFirstName());
            jsonObject.put("strLastName", mUserData.getStrLastName());
            jsonObject.put("message", "");
            jsonObject.put("mediaPath", image);
            jsonObject.put("mediaType", "AUDIO");
            jsonObject.put("mediaOriginalName", mediaOriginalName);
            jsonObject.put("isHaveMedia", true);
            jsonObject.put("sendTime", System.currentTimeMillis());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (isNetworkConnected()) {
            if (MqttUtil.client != null) {
                if (MqttUtil.client.isConnected()) {
                    messageArray.add(msg);
                    String js = jsonObject.toString();
                    mPublishMessage.PublishMessage(topic, js);
                    mMessageAdapter.notifyDataSetChanged();
                    mMessageRecycler.scrollToPosition(messageArray.size() - 1);
                    mTxtMsg.setText("");
                    isChatUpdated = true;
                } else {
                    connectMqtt(mCurruntUser);
                    Toast.makeText(ChatDetailActivity.this, "Message not send! please try again!", Toast.LENGTH_SHORT).show();
                }
            } else {
                connectMqtt(mCurruntUser);
            }
        } else {
            Toast.makeText(ChatDetailActivity.this, "No network connection!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        MqttUtil.curruntChatUser = "";
        super.onDestroy();
    }
}
