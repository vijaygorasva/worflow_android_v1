package com.example.user.communicator.Activity.ChatModule;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import com.example.user.communicator.Activity.TaskModule.BaseActivity;
import com.example.user.communicator.Activity.TaskModule.SearchUserActivity;
import com.example.user.communicator.Adapter.ChatAdapters.ChatInboxAdapter;
import com.example.user.communicator.CustomViews.CustomEditText;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.ChatModels.ChatItem;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.TaskModels.SelectionItem;
import com.example.user.communicator.Model.TaskModels.SelectionObject;
import com.example.user.communicator.R;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.example.user.communicator.Activity.TaskModule.BaseActivity.ApiCall.ADD_CHAT_GROUP;
import static com.example.user.communicator.Activity.TaskModule.BaseActivity.ApiCall.GET_CHAT_GROUP;

public class ChatActivity extends BaseActivity {

    @BindView(R.id.fabtnAdd)
    FloatingActionButton mAdd;

    @BindView(R.id.vLoading)
    LinearLayout vLoading;

    @BindView(R.id.recyclerView)
    RecyclerView mChatRecyclerView;

    LinearLayoutManager linearLayoutManager;
    ChatInboxAdapter chatInboxAdapter;
    ArrayList<ChatItem> chatItemArrayList = new ArrayList<>();
    SelectionObject mObject;
    Datum mUserData;
    String BROADCAST_ACTION = "com.example.user.communicator.Chat";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_inbox);
        ButterKnife.bind(this);
        mUserData = getLoginData();

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mChatRecyclerView.setLayoutManager(linearLayoutManager);
        chatInboxAdapter = new ChatInboxAdapter(this, chatItemArrayList);
        chatInboxAdapter.setOnItemClickListener(onItemClickList);
        mChatRecyclerView.setAdapter(chatInboxAdapter);

        mAdd.setOnClickListener(v -> {
            Intent i = new Intent(this, SearchUserActivity.class);
            startActivityForResult(i, 2020);
        });
        IntentFilter filter = new IntentFilter();
        filter.addAction(BROADCAST_ACTION);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(notificationBroadcaster, filter);
        getChatList();
    }

    public void getChatList() {
        RequestParams params = new RequestParams();
        params.put("intUserId", mUserData.getIntUserId());
        vLoading.setVisibility(View.VISIBLE);
        ApiCall(getChatGroup1, GET_CHAT_GROUP, params);
    }

    ChatInboxAdapter.OnItemClickList onItemClickList = (view, position) -> {
        ChatItem mItem = chatItemArrayList.get(position);
        Intent i = new Intent(ChatActivity.this, ChatDetailActivity.class);
        i.putExtra("userId", mItem.get_id());
        i.putExtra("userName", mItem.getName());
        startActivityForResult(i, 3333);
    };


    @Override
    public void OnResponce(JSONObject object, ApiCall type) {
        vLoading.setVisibility(View.GONE);
        try {
            if (type == ADD_CHAT_GROUP) {
                getChatList();
            }
            if (type == GET_CHAT_GROUP) {
                Gson g = new Gson();
                JSONArray array = object.getJSONObject("data").getJSONArray("groupsData");
                chatItemArrayList.clear();
                for (int i = 0; i < array.length(); i++) {
                    JSONObject object1 = array.getJSONObject(i);
                    ChatItem item = g.fromJson(object1.toString(), ChatItem.class);
                    chatItemArrayList.add(item);
                }
                chatInboxAdapter.notifyDataSetChanged();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnError(JSONObject object, ApiCall type) {
        vLoading.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 2020) {
            if (resultCode == RESULT_OK) {
                Gson gson = new Gson();
                String userData = data.getStringExtra("data");
                mObject = gson.fromJson(userData, SelectionObject.class);
                if (mObject != null) {
                    showGroupAddDialog();
                }
            }
        }
        if (requestCode == 3333) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    boolean isChatUpdated = data.getBooleanExtra("isChatUpdated", false);
                    if (isChatUpdated) {
                        getChatList();
                    }
                }
            }
        }
    }

    public void showGroupAddDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dialog_add_group);
        CustomEditText mTitle = dialog.findViewById(R.id.sub_task_title);
//        CustomEditText mDesc = dialog.findViewById(R.id.desc);
        CustomTextView btOk = dialog.findViewById(R.id.submit_sub_task);
        CustomTextView close = dialog.findViewById(R.id.close);

        btOk.setOnClickListener(v -> {
            String title = mTitle.getText().toString().trim();
//            String desc = mDesc.getText().toString().trim();
            if (TextUtils.isEmpty(title)) {
                showToast("Please enter Title");
                return;
            }
//            if (TextUtils.isEmpty(desc)) {
//                showToast("Please enter Description");
//                return;
//            }
            if (mObject == null || mObject.getUsers().size() == 0) {
                showToast("Please select at least one User");
                return;
            }
            RequestParams params = new RequestParams();

            JSONArray jsonArray = new JSONArray();
            for (SelectionItem item : mObject.getUsers()) {
                jsonArray.put(item.getId());
            }

            params.put("memberIds", jsonArray.toString());
            params.put("intUserId", mUserData.getIntUserId());
            params.put("groupName", title);
            vLoading.setVisibility(View.VISIBLE);
            ApiCall(addChatGroup, ADD_CHAT_GROUP, params);

            dialog.dismiss();
        });
        close.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    BroadcastReceiver notificationBroadcaster = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                boolean isChatUpdate = intent.getBooleanExtra("isChatUpdate", false);
                if (isChatUpdate) {
                    getChatList();
                }
            }
        }
    };

    @Override
    protected void onDestroy() {
        unregisterReceiver(notificationBroadcaster);
        super.onDestroy();
    }
}
