package com.example.user.communicator.Activity.TaskModule;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.example.user.communicator.Adapter.TaskAdapters.TaskWorkHistoryAdapter;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.TaskModels.TaskItems;
import com.example.user.communicator.Model.TaskModels.TaskWorkHistory;
import com.example.user.communicator.R;
import com.example.user.communicator.Utility.TimeCounter;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Timer;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TaskDetailsActivity extends BaseActivity {


    @BindView(R.id.startTimer)
    ImageView mStartTimer;

    @BindView(R.id.stopTimer)
    ImageView mStopTimer;

    @BindView(R.id.timeTxt)
    CustomTextView mTimeTxt;

    @BindView(R.id.date)
    CustomTextView mDate;


    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    @BindView(R.id.notext)
    CustomTextView mNoTxt;

    ArrayList<TaskWorkHistory> workHistoryArrayList = new ArrayList<>();
    LinearLayoutManager linearLayoutManager;
    TaskWorkHistoryAdapter workHistoryAdapter;

    Timer mTimer;

    Activity activity;
    TaskItems taskItems;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);
        ButterKnife.bind(this);
        activity = this;

        mDate.setText(getCurruntDate());

//        mStartTimer.setOnClickListener(this);
//        mStopTimer.setOnClickListener(this);

        String items = getIntent().getStringExtra("item");
        Gson gson = new Gson();
        taskItems = gson.fromJson(items, TaskItems.class);
        if (TimeCounter.finalTaskItems == null) {
            TimeCounter.finalTaskItems = gson.fromJson(items, TaskItems.class);
            mStartTimer.setVisibility(View.VISIBLE);
            mStopTimer.setVisibility(View.GONE);
        } else {
            if (taskItems.get_id().equalsIgnoreCase(TimeCounter.finalTaskItems.get_id())) {
                TimeCounter.setTextView(mTimeTxt);
                TimeCounter.setActivity(this);
                mStartTimer.setVisibility(View.GONE);
                mStopTimer.setVisibility(View.VISIBLE);
            } else {
                mStartTimer.setVisibility(View.GONE);
                mStopTimer.setVisibility(View.GONE);
            }
        }
//        if (taskItems.getWorkHistories() != null) {
//            if (taskItems.getWorkHistories().size() != 0) {
//                workHistoryArrayList.addAll(taskItems.getWorkHistories());
//            }
//        }
        workHistoryAdapter = new TaskWorkHistoryAdapter(this, workHistoryArrayList);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(workHistoryAdapter);
        if (workHistoryArrayList.size() != 0) {
            workHistoryAdapter.filter(mDate.getText().toString());
        } else {
            mNoTxt.setVisibility(View.VISIBLE);
        }


        mDate.setOnClickListener(view -> {
            final Calendar calendar = Calendar.getInstance();
            DatePickerDialog.OnDateSetListener date = (view1, year, monthOfYear, dayOfMonth) -> {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                String formattedDate = df.format(calendar.getTime());
                mDate.setText(formattedDate);
                workHistoryAdapter.filter(formattedDate);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (workHistoryArrayList.size() == 0) {
                                    mNoTxt.setVisibility(View.VISIBLE);
                                } else {
                                    mNoTxt.setVisibility(View.GONE);
                                }
                            }
                        });
                    }
                }, 500);
            };
            DatePickerDialog dpd = new DatePickerDialog(TaskDetailsActivity.this,
                    date, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            dpd.getDatePicker().setMinDate(System.currentTimeMillis());
            dpd.show();
        });

    }


//    public void stopTimer() {
//        if (TimeCounter.mTimer != null) {
//            TimeCounter.mTimer.cancel();
//            TimeCounter.finalTaskItems.getWorkHistories().get(TimeCounter.finalTaskItems.getWorkHistories().size() - 1).setEndTime(getCurruntTime());
//            TimeCounter.finalTaskItems.getWorkHistories().get(TimeCounter.finalTaskItems.getWorkHistories().size() - 1).
//                    setTotalTime((hours < 10 ? (hours != 0 ? "0" + hours + " hours " : "") : hours + " hours ")
//                            + (minutes < 10 ? (minutes != 0 ? "0" + minutes + " minutes " : "") : minutes + " minutes ")
//                            + (seconds < 10 ? (seconds != 0 ? "0" + seconds + " seconds " : "") : seconds + " seconds "));
//            ArrayList<TaskItems> itemsArrayList = getTaskItems();
//            boolean isF = false;
//            for (int i = 0; i < itemsArrayList.size() && !isF; i++) {
//                if (itemsArrayList.get(i).get_id().equalsIgnoreCase(TimeCounter.finalTaskItems.get_id())) {
//                    isF = true;
//                    Gson gson = new Gson();
//                    String data = gson.toJson(TimeCounter.finalTaskItems);
//                    taskItems = gson.fromJson(data, TaskItems.class);
//                    TaskItems items = gson.fromJson(data, TaskItems.class);
//                    itemsArrayList.set(i, items);
//                    setTaskItems(itemsArrayList);
//                    workHistoryAdapter.setAllTaskItems(itemsArrayList.get(i).getWorkHistories());
//                    workHistoryAdapter.filter(mDate.getText().toString());
//                    mNoTxt.setVisibility(View.GONE);
//                    mStartTimer.setVisibility(View.VISIBLE);
//                    mStopTimer.setVisibility(View.GONE);
//                }
//            }
//            TimeCounter.setAllClear();
//        }
//    }


//    @Override
//    public void onClick(View v) {
//        if (v == mStartTimer) {
////            Gson gson = new Gson();
////            String item = gson.toJson(taskItems, TaskItems.class);
////            if (TimeCounter.finalTaskItems == null) {
////                TimeCounter.finalTaskItems = gson.fromJson(item, TaskItems.class);
////            }
////            mStartTimer.setVisibility(View.GONE);
////            mStopTimer.setVisibility(View.VISIBLE);
////            TaskWorkHistory history = new TaskWorkHistory();
////            history.setTaskId(TimeCounter.finalTaskItems.get_id());
////            history.setWorkDate(getCurruntDate());
////            history.setStartTime(getCurruntTime());
////            TimeCounter.finalTaskItems.getWorkHistories().add(history);
////            TimeCounter.setTextView(mTimeTxt);
////            TimeCounter.setActivity(this);
////            TimeCounter.startTimer();
////            ArrayList<TaskItems> itemsArrayList = getTaskItems();
////            boolean isF = false;
////            for (int i = 0; i < itemsArrayList.size() && !isF; i++) {
////                if (itemsArrayList.get(i).get_id().equalsIgnoreCase(TimeCounter.finalTaskItems.get_id())) {
////                    isF = true;
////                    String data = gson.toJson(TimeCounter.finalTaskItems);
////                    TaskItems items = gson.fromJson(data, TaskItems.class);
////                    itemsArrayList.set(i, items);
////                    setTaskItems(itemsArrayList);
////                }
////            }
//        }
//        if (v == mStopTimer) {
//            stopTimer();
//        }
//    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent();
        setResult(RESULT_OK, i);
        TimeCounter.setTextView(null);
        TimeCounter.setActivity(null);
        if (!TimeCounter.isRunning) {
            TimeCounter.setAllClear();
        }
        finish();
    }
}
