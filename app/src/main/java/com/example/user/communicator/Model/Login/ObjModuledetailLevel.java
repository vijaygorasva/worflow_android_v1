
package com.example.user.communicator.Model.Login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.annotations.PrimaryKey;

public class ObjModuledetailLevel {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("strModuleName")
    @Expose
    private String strModuleName;
    @SerializedName("blnView")
    @Expose
    private String blnView;
    @SerializedName("blnSave")
    @Expose
    private String blnSave;
    @SerializedName("blnUpdate")
    @Expose
    private String blnUpdate;
    @SerializedName("blnDelete")
    @Expose
    private String blnDelete;
    @SerializedName("blnPrint")
    @Expose
    private String blnPrint;
    @SerializedName("blnRePrint")
    @Expose
    private String blnRePrint;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStrModuleName() {
        return strModuleName;
    }

    public void setStrModuleName(String strModuleName) {
        this.strModuleName = strModuleName;
    }

    public String getBlnView() {
        return blnView;
    }

    public void setBlnView(String blnView) {
        this.blnView = blnView;
    }

    public String getBlnSave() {
        return blnSave;
    }

    public void setBlnSave(String blnSave) {
        this.blnSave = blnSave;
    }

    public String getBlnUpdate() {
        return blnUpdate;
    }

    public void setBlnUpdate(String blnUpdate) {
        this.blnUpdate = blnUpdate;
    }

    public String getBlnDelete() {
        return blnDelete;
    }

    public void setBlnDelete(String blnDelete) {
        this.blnDelete = blnDelete;
    }

    public String getBlnPrint() {
        return blnPrint;
    }

    public void setBlnPrint(String blnPrint) {
        this.blnPrint = blnPrint;
    }

    public String getBlnRePrint() {
        return blnRePrint;
    }

    public void setBlnRePrint(String blnRePrint) {
        this.blnRePrint = blnRePrint;
    }

}
