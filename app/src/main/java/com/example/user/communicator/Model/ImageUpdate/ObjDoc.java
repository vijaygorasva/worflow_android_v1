
package com.example.user.communicator.Model.ImageUpdate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ObjDoc {

    @SerializedName("intPlannerId")
    @Expose
    private Integer intPlannerId;
    @SerializedName("imgNote")
    @Expose
    private ImgNote imgNote;
    @SerializedName("intPlannerMainId")
    @Expose
    private String intPlannerMainId;

    public Integer getIntPlannerId() {
        return intPlannerId;
    }

    public void setIntPlannerId(Integer intPlannerId) {
        this.intPlannerId = intPlannerId;
    }

    public ImgNote getImgNote() {
        return imgNote;
    }

    public void setImgNote(ImgNote imgNote) {
        this.imgNote = imgNote;
    }

    public String getIntPlannerMainId() {
        return intPlannerMainId;
    }

    public void setIntPlannerMainId(String intPlannerMainId) {
        this.intPlannerMainId = intPlannerMainId;
    }

}
