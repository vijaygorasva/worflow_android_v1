package com.example.user.communicator.Activity.TaskModule;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.user.communicator.Adapter.TaskAdapters.AttachmentAdapter;
import com.example.user.communicator.Adapter.TaskAdapters.FilterTypeAdapter;
import com.example.user.communicator.CustomViews.CustomButton;
import com.example.user.communicator.CustomViews.CustomEditText;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Dialog.AllCategoryListDialog;
import com.example.user.communicator.Fragment.DynamicCategoryFragment;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.TaskModels.FilterType;
import com.example.user.communicator.Model.TaskModels.LevelObject;
import com.example.user.communicator.Model.TaskModels.MemberObject;
import com.example.user.communicator.Model.TaskModels.SelectionItem;
import com.example.user.communicator.Model.TaskModels.SelectionItems.FileData;
import com.example.user.communicator.Model.TaskModels.SelectionItems.User;
import com.example.user.communicator.Model.TaskModels.SelectionObject;
import com.example.user.communicator.Model.TaskModels.TaskDetailItem;
import com.example.user.communicator.Model.TaskModels.TaskItems;
import com.example.user.communicator.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

public class EditTaskActivity extends BaseActivity implements DynamicCategoryFragment.OnCategorySelectedListener {


    @BindView(R.id.etAddTask)
    CustomEditText etAddTask;

    @BindView(R.id.usersCount)
    CustomTextView usersCount;

    @BindView(R.id.otherCount)
    CustomTextView otherCount;

    @BindView(R.id.priority)
    LinearLayout priority;

    @BindView(R.id.tvDash)
    CustomTextView tvDash;

    @BindView(R.id.tvPriorType)
    CustomTextView tvPriorType;

    @BindView(R.id.updateTask)
    CustomButton updateTask;

    @BindView(R.id.tvStartDate)
    CustomTextView tvStartDate;

    @BindView(R.id.tvEndDate)
    CustomTextView tvEndDate;

    @BindView(R.id.tvDuration)
    CustomTextView tvDuration;

    @BindView(R.id.deleteTime)
    ImageView deleteTime;

    @BindView(R.id.attachmentView)
    RecyclerView attachmentView;

    @BindView(R.id.tvCategory)
    CustomTextView tvCategory;

    @BindView(R.id.dot)
    ImageView dot;

    String categoryID = "0";
    String categoryName = "";
    String categoryTypeID = "";
    String categoryTypeName = "";
    String subCategoryID = "";
    String subCategoryName = "";
    LinearLayoutManager linearLayoutManager;
    AttachmentAdapter adapter;
    ArrayList<FileData> attachmentItemArrayList = new ArrayList<>();
    TaskDetailItem items;
    Realm mRealm;
    Datum mUserData;
    String task_Id;
    String mH = "";
    String mM = "";
    boolean isUpdated = false;
    boolean isCategoryChanged = false;
    boolean isTimeDeleted = false;
    AllCategoryListDialog allCategoryListDialog;
    ArrayList<FilterType> filterTypes = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_edit_task);
        ButterKnife.bind(this);
        task_Id = String.valueOf(getIntent().getIntExtra("_id", 0));
        mRealm = Realm.getDefaultInstance();
        mUserData = getLoginData();
        if (isNetworkAvailable()) {
            if (!TextUtils.isEmpty(task_Id)) {
                getData(task_Id);
            }
        } else {
            if (!TextUtils.isEmpty(task_Id)) {
                items = mRealm.where(TaskDetailItem.class).equalTo("intTaskDocumentNo", Integer.valueOf(task_Id)).findFirst();
                setData();
            }
        }

        updateTask.setOnClickListener(v -> {
            updatetask();
        });

        tvCategory.setOnClickListener(v -> {
//            CategoryDialog.newIntance((dialog, categotyid, categotyName) -> {
//                tvCategory.setText(categotyName);
//                categoryID = categotyid;
//            }).show(getSupportFragmentManager().beginTransaction(), "CategoryDialog");

            allCategoryListDialog = AllCategoryListDialog.newIntance("TASK", false);

            if (allCategoryListDialog != null) {
                allCategoryListDialog.show(getSupportFragmentManager().beginTransaction(), "my-dialog");
            }

        });

        deleteTime.setOnClickListener(v -> {
            isTimeDeleted = true;
            tvStartDate.setText("");
            tvEndDate.setText("");
            tvDuration.setText("");
            deleteTime.setVisibility(View.GONE);
            tvDash.setVisibility(View.GONE);
            tvDuration.setVisibility(View.GONE);
            tvStartDate.setVisibility(View.GONE);
            tvEndDate.setVisibility(View.GONE);
        });
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2020) {
            if (resultCode == RESULT_OK) {
                Gson gson = new Gson();
                String userData = data.getStringExtra("data");
                SelectionObject mObject = gson.fromJson(userData, SelectionObject.class);
                if (mObject != null) {
                    setSelectionEditItem(mObject);
                    if (mObject.isUser() && mObject.getUsers().size() != 0) {
                        usersCount.setVisibility(View.VISIBLE);
                        otherCount.setVisibility(View.GONE);
                        usersCount.setText(mObject.getUsers().size() + " Users");
                    } else if (mObject.getLevels() != null && mObject.getMemberTypes() != null &&
                            mObject.getLevels().size() != 0 && mObject.getMemberTypes().size() != 0) {
                        usersCount.setVisibility(View.GONE);
                        otherCount.setVisibility(View.VISIBLE);
                        otherCount.setText(mObject.getLevels().size() + " Levels and " + mObject.getMemberTypes().size() + "  Member Types");
                    }
                }
            }
        }
    }

    @Override
    public void OnResponce(JSONObject object, ApiCall type) {

        try {
            Gson gson = new GsonBuilder().create();
            if (type == ApiCall.GET_TASK_DATA) {
                hideProgressDialog(this);
                TaskDetailItem items1 = gson.fromJson(object.getJSONObject("data").getJSONObject("taskData").toString(), TaskDetailItem.class);
                TaskItems items2 = gson.fromJson(object.getJSONObject("data").getJSONObject("taskData").toString(), TaskItems.class);
                mRealm.beginTransaction();
                mRealm.copyToRealmOrUpdate(items1);
                mRealm.copyToRealmOrUpdate(items2);
                mRealm.commitTransaction();
                this.items = mRealm.where(TaskDetailItem.class).equalTo("intTaskDocumentNo", Integer.valueOf(task_Id)).findFirst();
                if (isUpdated) {
                    onBack();
                } else {
                    setData();
                }
            }
            if (type == ApiCall.UPDATE_TASK) {
                getData(task_Id);
                isUpdated = true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onBack() {
        setSelectionEditItem(null);
        Intent i = new Intent();
        i.putExtra("isUpdated", isUpdated);
        i.putExtra("isCategoryChanged", isCategoryChanged);
        setResult(RESULT_OK, i);
        finish();
    }


    @SuppressLint({"SetTextI18n", "RestrictedApi"})
    public void setData() {

        if (items.getObjFileData() != null && items.getObjFileData().size() != 0) {
            attachmentItemArrayList.addAll(items.getObjFileData());
        }

        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        attachmentView.setLayoutManager(linearLayoutManager);
        adapter = new AttachmentAdapter(this, attachmentItemArrayList, true);

        attachmentView.setAdapter(adapter);

        String cat = items.getStrCategoryName() +
                (!TextUtils.isEmpty(items.getStrSubCategoryName()) ? " > " + items.getStrSubCategoryName() : "");

        tvCategory.setText(cat);
        categoryID = items.getStrCategory();
        categoryName = items.getStrCategoryName();
        subCategoryID = items.getStrSubCategory();
        subCategoryName = items.getStrSubCategoryName();
        categoryTypeID = items.getStrCategoryType();
        categoryTypeName = items.getStrCategoryTypeName();

        if (items.getDatStartToDueDate().size() != 0) {

            tvDash.setVisibility(View.VISIBLE);
            tvStartDate.setVisibility(View.VISIBLE);
            tvEndDate.setVisibility(View.VISIBLE);
            deleteTime.setVisibility(View.VISIBLE);
            tvStartDate.setText(getMillsToDate(items.getDatStartToDueDate().get(0).getStartDate()));
            tvEndDate.setText(getMillsToDate(items.getDatStartToDueDate().get(0).getDueDate()));
            tvDuration.setVisibility(View.GONE);

        } else if (!TextUtils.isEmpty(items.getIntHour()) && !TextUtils.isEmpty(items.getIntMinut())) {
            tvDuration.setVisibility(View.VISIBLE);
            if (items.getIntHour().equalsIgnoreCase("1")) {
                if (items.getIntMinut().equalsIgnoreCase("1")) {
                    tvDuration.setText(" " + items.getIntHour() + " hour " + items.getIntMinut() + " minute");
                } else {
                    tvDuration.setText(" " + items.getIntHour() + " hour " + items.getIntMinut() + " minutes");
                }
            } else {
                if (items.getIntMinut().equalsIgnoreCase("1")) {
                    tvDuration.setText(" " + items.getIntHour() + " hours " + items.getIntMinut() + " minute");
                } else {
                    tvDuration.setText(" " + items.getIntHour() + " hours " + items.getIntMinut() + " minutes");
                }
            }
            tvStartDate.setVisibility(View.GONE);
            tvEndDate.setVisibility(View.GONE);
            tvDash.setVisibility(View.GONE);

        } else {
            deleteTime.setVisibility(View.GONE);
            tvDuration.setVisibility(View.GONE);
            tvDash.setVisibility(View.GONE);
            tvStartDate.setVisibility(View.GONE);
            tvEndDate.setVisibility(View.GONE);
        }


        String prior = items.getStrPriorityValue();
        FilterType filterType = new FilterType();
        filterType.setName("Low");
        filterType.setKey("1");
        filterTypes.add(filterType);

        FilterType filterType1 = new FilterType();
        filterType1.setName("Medium");
        filterType1.setKey("2");
        filterTypes.add(filterType1);

        FilterType filterType2 = new FilterType();
        filterType2.setName("High");
        filterType2.setKey("3");
        filterTypes.add(filterType2);
        if (!TextUtils.isEmpty(prior)) {
            filterTypes.get(Integer.parseInt(prior) - 1).setSelected(true);
            priority.setVisibility(View.VISIBLE);
            switch (prior) {
                case "1":
                    tvPriorType.setText("Low Priority");
                    tvPriorType.setTextColor(getResources().getColor(R.color.blue));
                    dot.setColorFilter(ContextCompat.getColor(this, R.color.blue), android.graphics.PorterDuff.Mode.MULTIPLY);
                    break;
                case "2":
                    tvPriorType.setText("Medium Priority");
                    tvPriorType.setTextColor(getResources().getColor(R.color.orange));
                    dot.setColorFilter(ContextCompat.getColor(this, R.color.orange), android.graphics.PorterDuff.Mode.MULTIPLY);
                    break;
                case "3":
                    tvPriorType.setText("High Priority");
                    tvPriorType.setTextColor(getResources().getColor(R.color.red));
                    dot.setColorFilter(ContextCompat.getColor(this, R.color.red), android.graphics.PorterDuff.Mode.MULTIPLY);
                    break;
                case "4":
                    tvPriorType.setText("Done");
                    tvPriorType.setTextColor(getResources().getColor(R.color.colorPrimary));
                    dot.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary), android.graphics.PorterDuff.Mode.MULTIPLY);
                    break;
            }
        } else {
            priority.setVisibility(View.GONE);
        }

        int userCount = 0;
        int memberCount = 0;
        int levelCount = 0;

        if (items.getUsers() != null && items.getUsers().size() != 0) {
            userCount = items.getUsers().size();
        }
        if (items.getMemberTypes() != null && items.getMemberTypes().size() != 0) {
            memberCount = items.getMemberTypes().size();
        }
        if (items.getLevelsOne() != null && items.getLevelsOne().size() != 0) {
            levelCount = items.getLevelsOne().size();
        }
        if (items.getLevelsTwo() != null && items.getLevelsTwo().size() != 0) {
            levelCount += items.getLevelsTwo().size();
        }
        if (items.getLevelsThree() != null && items.getLevelsThree().size() != 0) {
            levelCount += items.getLevelsThree().size();
        }
        if (items.getLevelsFour() != null && items.getLevelsFour().size() != 0) {
            levelCount += items.getLevelsFour().size();
        }
        if (userCount != 0) {
            usersCount.setVisibility(View.VISIBLE);
            otherCount.setVisibility(View.GONE);
            usersCount.setText(userCount + " Users");
        } else if (memberCount != 0) {
            usersCount.setVisibility(View.GONE);
            otherCount.setVisibility(View.VISIBLE);
            otherCount.setText(levelCount + " Levels and " + memberCount + "  Member Types");
        }

        priority.setOnClickListener(view -> {
            if (items.getIsClosed() == 0) {
                AddPrior();
            }
        });

        usersCount.setOnClickListener(v -> {
            Intent i = new Intent(this, SearchUserActivity.class);
            i.putExtra("_id", task_Id);
            i.putExtra("isFromEditTask", true);
            startActivityForResult(i, 2020);
        });
        otherCount.setOnClickListener(v -> {
            Intent i = new Intent(this, SearchUserActivity.class);
            i.putExtra("_id", task_Id);
            i.putExtra("isFromEditTask", true);
            startActivityForResult(i, 2020);
        });
        etAddTask.setText(items.getStrDescription());


        tvStartDate.setOnClickListener(v -> {
            String maxDate = tvEndDate.getText().toString().trim();
            if (!TextUtils.isEmpty(maxDate)) {
                SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                try {
                    Date d = df.parse(maxDate);
                    long milliseconds = d.getTime();
                    showDatePickerEdit(tvStartDate, System.currentTimeMillis(), milliseconds);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else {
                showDatePickerEdit(tvStartDate, System.currentTimeMillis(), 0);
            }
        });
        tvEndDate.setOnClickListener(v -> {
            String minDate = tvStartDate.getText().toString().trim();
            if (!TextUtils.isEmpty(minDate)) {
                SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                try {
                    Date d = df.parse(minDate);
                    long milliseconds = d.getTime();
                    showDatePickerEdit(tvEndDate, milliseconds, 0);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else {
                showDatePickerEdit(tvEndDate, System.currentTimeMillis(), 0);
            }
        });
        tvDuration.setOnClickListener(v -> {
            showDurationDialog();
        });

    }

    private void AddPrior() {
        String prior1 = items.getStrPriorityValue();
        if (!TextUtils.isEmpty(prior1)) {
            if (prior1.equalsIgnoreCase("4")) {
                Toast.makeText(this, "Task is already complete!", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        if (!isNetworkAvailable()) {
            Toast.makeText(this, getString(R.string.noConnection), Toast.LENGTH_SHORT).show();
            return;
        }
        showPriorityDialog();
    }

    private void showPriorityDialog() {
        Context context = this;
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dilogue_filter_type);

        CustomTextView textView = dialog.findViewById(R.id.title);
        textView.setText("Choose priority");
        RecyclerView recyclerView = dialog.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        FilterTypeAdapter adapter = new FilterTypeAdapter(context, filterTypes);
        recyclerView.setAdapter(adapter);
        adapter.setOnClickListner(position -> {
            for (int i = 0; i < filterTypes.size(); i++) {
                filterTypes.get(i).setSelected(false);
            }
            filterTypes.get(position).setSelected(true);
            String prior = filterTypes.get(position).getKey();
            adapter.notifyDataSetChanged();
            dialog.dismiss();
            tvPriorType.setVisibility(View.VISIBLE);
            switch (prior) {
                case "1":
                    tvPriorType.setText("Low Priority");
                    tvPriorType.setTextColor(getResources().getColor(R.color.blue));
                    dot.setColorFilter(ContextCompat.getColor(this, R.color.blue), android.graphics.PorterDuff.Mode.MULTIPLY);
                    break;
                case "2":
                    tvPriorType.setText("Medium Priority");
                    tvPriorType.setTextColor(getResources().getColor(R.color.orange));
                    dot.setColorFilter(ContextCompat.getColor(this, R.color.orange), android.graphics.PorterDuff.Mode.MULTIPLY);
                    break;
                case "3":
                    tvPriorType.setText("High Priority");
                    tvPriorType.setTextColor(getResources().getColor(R.color.red));
                    dot.setColorFilter(ContextCompat.getColor(this, R.color.red), android.graphics.PorterDuff.Mode.MULTIPLY);
                    break;
                case "4":
                    tvPriorType.setText("Done");
                    tvPriorType.setTextColor(getResources().getColor(R.color.colorPrimary));
                    dot.setColorFilter(ContextCompat.getColor(this, R.color.colorPrimary), android.graphics.PorterDuff.Mode.MULTIPLY);
                    break;
            }
        });
        dialog.show();
    }

//    private void showPriorityDialog() {
//        AlertDialog.Builder adb = new AlertDialog.Builder(this);
//        adb.setTitle("Choose priority");
//        final String[] priority;
//        priority = new String[]{"Low ", "Medium ", "High "};
//        adb.setItems(priority, (dialog, which) -> {
//            String clickedItemValue = Arrays.asList(priority).get(which);
//            tvPriorType.setVisibility(View.VISIBLE);
//            switch (clickedItemValue) {
//                case "Low ":
//                    tvPriorType.setText("Low Priority");
//                    tvPriorType.setTextColor(getResources().getColor(R.color.blue));
//                    break;
//                case "Medium ":
//                    tvPriorType.setText("Medium Priority");
//                    tvPriorType.setTextColor(getResources().getColor(R.color.orange));
//                    break;
//                case "High ":
//                    tvPriorType.setText("High Priority");
//                    tvPriorType.setTextColor(getResources().getColor(R.color.red));
//                    break;
//            }
//        });
//        adb.show();
//    }

    private void showDurationDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_duration);

        final CustomEditText hour = dialog.findViewById(R.id.hour);
        final CustomEditText minutes = dialog.findViewById(R.id.minutes);

        CustomButton submit = dialog.findViewById(R.id.submit);

        submit.setOnClickListener(view -> {
            final String h = hour.getText().toString().trim();
            final String m = minutes.getText().toString().trim();
            if (TextUtils.isEmpty(h)) {
                Toast.makeText(this, "Please enter hours", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(m)) {
                Toast.makeText(this, "Please enter minutes", Toast.LENGTH_SHORT).show();
                return;
            } else if (Integer.parseInt(m) >= 60) {
                Toast.makeText(this, "Please select valid minutes that are under 60.", Toast.LENGTH_SHORT).show();
                return;
            }
            mH = h;
            mM = m;
            dialog.dismiss();
            if (h.equalsIgnoreCase("1")) {
                if (m.equalsIgnoreCase("1")) {
                    tvDuration.setText(" " + h + " hour " + m + " minute");
                } else {
                    tvDuration.setText(" " + h + " hour " + m + " minutes");
                }
            } else {
                if (items.getIntMinut().equalsIgnoreCase("1")) {
                    tvDuration.setText(" " + h + " hours " + m + " minute");
                } else {
                    tvDuration.setText(" " + h + " hours " + m + " minutes");
                }
            }
        });
        dialog.show();

    }


    public void showDatePickerEdit(final CustomTextView textView, long minDate, long maxDate) {
        final Calendar calendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.US);
            String formattedDate = df.format(calendar.getTime());
            textView.setText(formattedDate);
        };
        DatePickerDialog dpd = new DatePickerDialog(this, date,
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        if (minDate != 0) {
            dpd.getDatePicker().setMinDate(minDate);
        }
        if (maxDate != 0) {
            dpd.getDatePicker().setMaxDate(maxDate);
        }
        dpd.show();
    }


    public void getData(String id) {
        RequestParams params = new RequestParams();
        params.put("intTaskDocumentNo", id);
        showProgressDialog(this, "Please wait...");
        ApiCallWithToken(getTaskData, ApiCall.GET_TASK_DATA, params);
    }


    public boolean isUpdate(JSONArray userArray) {
        if (!isCategoryChanged()) {
            if (etAddTask.getText().toString().trim().equalsIgnoreCase(items.getStrDescription())) {
                JSONArray array = new JSONArray();
                for (User item : items.getUsers()) {
                    try {
                        JSONObject object = new JSONObject();
                        object.put("id", item.get_id());
                        array.put(object);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (userArray.toString().equalsIgnoreCase(array.toString())) {
                    if (!isDatesChanged()) {
                        if (!isPriorityChanged()) {
                            return isFilesChanged();
                        }
                    }
                }
            }
        }
        return true;
    }


    public boolean isDatesChanged() {
        if (items.getDatStartToDueDate().size() != 0) {
            String startDate = tvStartDate.getText().toString().trim();
            String endDate = tvEndDate.getText().toString().trim();
            String startDate1 = getMillsToDate1(items.getDatStartToDueDate().get(0).getStartDate());
            String endDate1 = getMillsToDate1(items.getDatStartToDueDate().get(0).getDueDate());
            return !startDate.equalsIgnoreCase(startDate1) || !endDate.equalsIgnoreCase(endDate1);
        } else if (!TextUtils.isEmpty(items.getIntHour()) && !TextUtils.isEmpty(items.getIntMinut())) {
            String duration;
            if (items.getIntHour().equalsIgnoreCase("1")) {
                if (items.getIntMinut().equalsIgnoreCase("1")) {
                    duration = " " + items.getIntHour() + " hour " + items.getIntMinut() + " minute";
                } else {
                    duration = " " + items.getIntHour() + " hour " + items.getIntMinut() + " minutes";
                }
            } else {
                if (items.getIntMinut().equalsIgnoreCase("1")) {
                    duration = " " + items.getIntHour() + " hours " + items.getIntMinut() + " minute";
                } else {
                    duration = " " + items.getIntHour() + " hours " + items.getIntMinut() + " minutes";
                }
            }
            return !duration.equalsIgnoreCase(tvDuration.getText().toString());
        } else {
            return false;
        }
    }


    public boolean isPriorityChanged() {
        if (!TextUtils.isEmpty(items.getStrPriorityValue())) {
            String clickedItemValue = tvPriorType.getText().toString();
            String prior = "";
            switch (clickedItemValue) {
                case "Low Priority":
                    prior = "1";
                    break;
                case "Medium Priority":
                    prior = "2";
                    break;
                case "High Priority":
                    prior = "3";
                    break;
            }
            return !prior.equalsIgnoreCase(items.getStrPriorityValue());
        } else {
            return false;
        }
    }


    public boolean isFilesChanged() {
        if (items.getObjFileData() != null && items.getObjFileData().size() != 0) {
            return items.getObjFileData().size() != attachmentItemArrayList.size();
        } else {
            return false;
        }
    }

    public boolean isCategoryChanged() {
        return !tvCategory.getText().toString().trim().equalsIgnoreCase((TextUtils.isEmpty(items.getStrSubCategoryName())
                ? items.getStrCategoryName() : items.getStrCategoryName() + " > " + items.getStrSubCategoryName()));
    }


    public String getMillsToDate1(String mills) {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        return df.format(new Date(mills));
    }

    public void updatetask() {
        String startDate = tvStartDate.getText().toString().trim();
        String endDate = tvEndDate.getText().toString().trim();
        String desc = etAddTask.getText().toString().trim();

        if (TextUtils.isEmpty(desc)) {
            showToast("Please enter Task Description");
            return;
        }

        RequestParams params = new RequestParams();
        params.put("intTaskDocumentNo", task_Id);
        params.put("strCategoryName", categoryName);
        params.put("strCategory", categoryID);
        params.put("strSubCategory", subCategoryID);
        params.put("strSubCategoryName", subCategoryName);
        params.put("strCategoryType", categoryTypeID);
        params.put("strCategoryTypeName", categoryTypeName);

        isCategoryChanged = isCategoryChanged();
        SelectionObject mObject = getSelectionEditItem();
        if (mObject != null) {
            if (mObject.isUser()) {
                if (mObject.getUsers() != null && mObject.getUsers().size() != 0) {
                    JSONArray array = new JSONArray();
                    for (SelectionItem item : mObject.getUsers()) {
                        try {
                            JSONObject object = new JSONObject();
                            object.put("id", item.getId());
                            array.put(object);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if (array.length() != 0) {
                        if (!isUpdate(array)) {
                            finish();
                            return;
                        }
                        params.put("arryOfobjstrUserCollectionData", array.toString());
                    } else {
                        showToast("Please select users or levels and members for task.");
                        return;
                    }
                } else {
                    showToast("Please select users or levels and members for task.");
                    return;
                }
            } else {
                if (mObject.getLevels() != null && mObject.getMemberTypes() != null && mObject.getLevels().size() != 0 && mObject.getMemberTypes().size() != 0) {
                    JSONArray array = new JSONArray();
                    for (SelectionItem item : mObject.getLevels()) {
                        try {
                            JSONObject object = new JSONObject();
                            object.put("id", item.getId());
                            array.put(object);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    JSONArray array1 = new JSONArray();
                    for (SelectionItem item : mObject.getMemberTypes()) {
                        try {
                            JSONObject object = new JSONObject();
                            object.put("id", item.getId());
                            array1.put(object);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    if (array.length() != 0 && array1.length() != 0) {
                        params.put("arryOfobjstrLevelCollection", array);
                        params.put("arryOfobjstrMemberCollection", array1);
                    } else {
                        showToast("Please select users or levels and members for task.");
                        return;
                    }
                } else {
                    showToast("Please select users or levels and members for task.");
                    return;
                }
            }
        } else if (items.getUsers() != null && items.getUsers().size() != 0) {
            JSONArray array = new JSONArray();
            for (User item : items.getUsers()) {
                try {
                    JSONObject object = new JSONObject();
                    object.put("id", item.get_id());
                    array.put(object);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (array.length() != 0) {
                if (!isUpdate(array)) {
                    finish();
                    return;
                }
                params.put("arryOfobjstrUserCollectionData", array.toString());
            } else {
                showToast("Please select users or levels and members for task.");
                return;
            }

        } else if (items.getMemberTypes() != null && items.getMemberTypes().size() != 0) {

            JSONArray array = new JSONArray();
            if (items.getLevelsOne().size() != 0) {
                for (LevelObject item : items.getLevelsOne()) {
                    try {
                        JSONObject object = new JSONObject();
                        object.put("id", item.get_id());
                        array.put(object);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (items.getLevelsTwo().size() != 0) {
                for (LevelObject item : items.getLevelsTwo()) {
                    try {
                        JSONObject object = new JSONObject();
                        object.put("id", item.get_id());
                        array.put(object);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (items.getLevelsThree().size() != 0) {
                for (LevelObject item : items.getLevelsThree()) {
                    try {
                        JSONObject object = new JSONObject();
                        object.put("id", item.get_id());
                        array.put(object);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            if (items.getLevelsFour().size() != 0) {
                for (LevelObject item : items.getLevelsFour()) {
                    try {
                        JSONObject object = new JSONObject();
                        object.put("id", item.get_id());
                        array.put(object);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            JSONArray array1 = new JSONArray();
            for (MemberObject item : items.getMemberTypes()) {
                try {
                    JSONObject object = new JSONObject();
                    object.put("id", item.get_id());
                    array1.put(object);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            if (array.length() != 0 && array1.length() != 0) {
                params.put("arryOfobjstrLevelCollection", array);
                params.put("arryOfobjstrMemberCollection", array1);
            } else {
                showToast("Please select users or levels and members for task.");
                return;
            }

        } else {
            showToast("Please select users or levels and members for task.");
            return;
        }


        params.put("strDescription", desc);
        if (!isTimeDeleted) {
            if (!TextUtils.isEmpty(startDate) && !TextUtils.isEmpty(endDate)) {
                try {
                    SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                    Date sdate = df.parse(startDate);
                    Date edate = df.parse(endDate);
                    JSONArray array = new JSONArray();
                    array.put(sdate.getTime());
                    array.put(edate.getTime());
                    params.put("datStartToDueDate", array.toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } else {
                if (!TextUtils.isEmpty(mH) && !TextUtils.isEmpty(mM)) {
                    params.put("intHour", mH);
                    params.put("intMinut", mM);
                } else if (!TextUtils.isEmpty(items.getIntHour()) && !TextUtils.isEmpty(items.getIntMinut())) {
                    params.put("intHour", items.getIntHour());
                    params.put("intMinut", items.getIntMinut());
                }
            }
        }
        String clickedItemValue = tvPriorType.getText().toString();
        if (!TextUtils.isEmpty(clickedItemValue)) {
            String prior = "";
            switch (clickedItemValue) {
                case "Low Priority":
                    prior = "1";
                    break;
                case "Medium Priority":
                    prior = "2";
                    break;
                case "High Priority":
                    prior = "3";
                    break;
            }
            params.put("strPriorityValue", prior);
        }
        params.put("intCreateUserId", getLoginData().getIntUserId());
        if (attachmentItemArrayList.size() != 0) {
            try {

                JSONArray array = new JSONArray();
                for (FileData data : attachmentItemArrayList) {
                    JSONObject object = new JSONObject();
                    object.put("fileType", data.getFileType());
                    object.put("filePath", data.getFilePath());
                    object.put("originalName", data.getOriginalName());
                    array.put(object);
                }
                params.put("objFileData", array.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else {
            JSONArray jsArray = new JSONArray();
            params.put("objFileData", jsArray.toString());
        }

        showProgressDialog(this, "Please wait...");
        ApiCallWithToken(update_task, ApiCall.UPDATE_TASK, params);

    }

    @Override
    public void onBackPressed() {
        onBack();
    }

    @Override
    public void onCategorySelected(Boolean isActive, String tabName, String tabID, String mainCat, String mainCatID, String subCatID, String subCatName) {
        if (allCategoryListDialog != null) {
            allCategoryListDialog.dismiss();
        }
        if (mainCat.equals("All")) {
            tvCategory.setText(mainCat);
        } else {
            if (!TextUtils.isEmpty(subCatName)) {
                tvCategory.setText(mainCat + " > " + subCatName);
            } else {
                tvCategory.setText(mainCat);
            }
        }
        categoryName = mainCat;
        categoryID = mainCatID;
        categoryTypeID = tabID;
        categoryTypeName = tabName;
        subCategoryID = subCatID;
        subCategoryName = subCatName;
    }
}
