package com.example.user.communicator.Model.Notes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateModel {

    @SerializedName("planerid")
    @Expose
    private String planerid;
    @SerializedName("offlineid")
    @Expose
    private String offlineid;

    public String getPlanerid() {
        return planerid;
    }

    public void setPlanerid(String planerid) {
        this.planerid = planerid;
    }

    public String getOfflineid() {
        return offlineid;
    }

    public void setOfflineid(String offlineid) {
        this.offlineid = offlineid;
    }
}
