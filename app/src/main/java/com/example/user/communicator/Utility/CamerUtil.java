package com.example.user.communicator.Utility;


import android.graphics.drawable.AnimationDrawable;
import android.os.Environment;
import android.view.View;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;


public class CamerUtil {
    /** Create a File for saving an image */
    // http://developer.android.com/guide/topics/media/camera.html#saving-media
    public static File getOutputMediaFile()
    {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "FrontCam");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists())
        {
            if (!mediaStorageDir.mkdirs()) return null;
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;

        mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }

    public static AnimationDrawable getAnimation(View v)
    {
        return ((AnimationDrawable) v.getBackground());
    }

    public static void startAnimation(View v)
    {
        getAnimation(v).start();
    }

    public static void stopAnimation(View v)
    {
        getAnimation(v).stop();
    }

    public static void restartAnimation(View v)
    {
        stopAnimation(v);
        startAnimation(v);
    }
}
