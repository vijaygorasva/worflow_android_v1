package com.example.user.communicator.Adapter.TaskAdapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.user.communicator.Activity.TaskModule.ImageShowActivity;
import com.example.user.communicator.Activity.TaskModule.PDFViewerActivity;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.TaskModels.SelectionItems.FileData;
import com.example.user.communicator.R;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class AttachmentAdapter extends RecyclerView.Adapter<AttachmentAdapter.ViewHolder> {

    private Context context;
    private ArrayList<FileData> taskItems;
    private OnClickListner onClick;
//    public final String BASE_URL = "http://52.66.249.75:9999/";

    public final String BASE_URL_SEVER = "http://52.66.249.75:9999/";
    public final String BASE_URL_local = "http://:9999/";
    public final String BASE_URL_TEST = "http://13.232.37.104:9999/";

    public final String BASE_URL = BASE_URL_TEST;

    ArrayList<String> allImages = new ArrayList<String>();
    private ArrayList<FileData> allTaskItems = new ArrayList<>();
    private boolean isEdit = false;

    public AttachmentAdapter(Context context, ArrayList<FileData> taskItems, boolean isEdit) {
        this.taskItems = taskItems;
        this.context = context;
        this.isEdit = isEdit;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.attachment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(taskItems.get(position));
    }

    @Override
    public int getItemCount() {
        return taskItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setOnClickListner(OnClickListner onClickListner) {
        this.onClick = onClickListner;
    }

    public interface OnClickListner {
        void OnClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.tvDocName)
        CustomTextView tvDocName;

        @BindView(R.id.holder)
        RelativeLayout mHolder;

        @BindView(R.id.delete)
        ImageView delete;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        public void bind(final FileData items) {

            if (isEdit) {
                delete.setVisibility(View.VISIBLE);
            } else {
                delete.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(items.getOriginalName())) {
                tvDocName.setText(items.getOriginalName());
            }

            if (!isEdit) {
                if (items.getFileType().equalsIgnoreCase("IMAGE")) {
                    allImages.add(BASE_URL + items.getFilePath());
                }
            }
            mHolder.setOnClickListener(v -> {
                if (!isEdit) {
                    if (items.getFileType().equalsIgnoreCase("IMAGE")) {
                        if (!TextUtils.isEmpty(items.getFilePath())) {
                            Intent i = new Intent(context, ImageShowActivity.class);
                            i.putExtra("type", "TASK");
                            i.putExtra("img_pos", getAdapterPosition());
                            i.putStringArrayListExtra("img_list", allImages);
                            context.startActivity(i);
                        }
                    } else {
                        if (!TextUtils.isEmpty(items.getFilePath())) {
                            File imgFile = new File(items.getFilePath());
                            if (imgFile.exists()) {
                                Intent i = new Intent(context, PDFViewerActivity.class);
                                i.putExtra("path", items.getFilePath());
                                context.startActivity(i);
                            } else {
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(BASE_URL + items.getFilePath()));
                                context.startActivity(browserIntent);
                            }
                        }
                    }
                }
            });

            delete.setOnClickListener(v -> {
                taskItems.remove(getAdapterPosition());
                notifyAdapter();
            });

        }
    }

    public void notifyAdapter() {
        this.notifyDataSetChanged();
    }
}
