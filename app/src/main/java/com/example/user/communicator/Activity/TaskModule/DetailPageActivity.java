package com.example.user.communicator.Activity.TaskModule;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.user.communicator.Activity.MainActivity;
import com.example.user.communicator.Adapter.TaskAdapters.AttachmentAdapter;
import com.example.user.communicator.Adapter.TaskAdapters.CommentsAdapter;
import com.example.user.communicator.Adapter.TaskAdapters.DurationAdapter;
import com.example.user.communicator.Adapter.TaskAdapters.SubTasksAdapter;
import com.example.user.communicator.Adapter.TaskAdapters.UsersAdapter;
import com.example.user.communicator.CustomViews.CustomButton;
import com.example.user.communicator.CustomViews.CustomEditText;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.TaskModels.CommentData;
import com.example.user.communicator.Model.TaskModels.Duration;
import com.example.user.communicator.Model.TaskModels.SelectionItems.FileData;
import com.example.user.communicator.Model.TaskModels.SelectionItems.User;
import com.example.user.communicator.Model.TaskModels.SubTaskData;
import com.example.user.communicator.Model.TaskModels.TaskDetailItem;
import com.example.user.communicator.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.Sort;

public class DetailPageActivity extends BaseActivity {


    @BindView(R.id.tvTitle)
    CustomTextView tvTitle;

    @BindView(R.id.etTitle)
    CustomEditText etTitle;

    @BindView(R.id.commentEdtTxt)
    CustomEditText mCommentEdtTxt;

    @BindView(R.id.send)
    ImageView mSend;

    @BindView(R.id.tvHeaderType)
    CustomTextView tvHeaderType;

    @BindView(R.id.tvPriorType)
    CustomTextView tvPriorType;

    @BindView(R.id.tvStartDate)
    CustomTextView tvStartDate;

    @BindView(R.id.tvDash)
    CustomTextView tvDash;

    @BindView(R.id.tvEndDate)
    CustomTextView tvEndDate;

    @BindView(R.id.tvDuration)
    CustomTextView tvDuration;

    @BindView(R.id.attachmentView)
    RecyclerView attachmentView;

    @BindView(R.id.commentsView)
    RecyclerView commentsView;

    @BindView(R.id.durationsView)
    RecyclerView userView;

    @BindView(R.id.subTasksView)
    RecyclerView subTasksView;

    @BindView(R.id.durationsLayout)
    LinearLayout durationsLayout;

    @BindView(R.id.subTasksLayout)
    LinearLayout subTasksLayout;

    @BindView(R.id.prof_img)
    ImageView prof_img;

    @BindView(R.id.name)
    CustomTextView name;

    @BindView(R.id.tvDate)
    CustomTextView tvDate;

    @BindView(R.id.noOfComments)
    CustomTextView noOfComments;

    AttachmentAdapter adapter;
    CommentsAdapter commentsAdapter;
    UsersAdapter usersAdapter;
    DurationAdapter durationAdapter;
    SubTasksAdapter subTasksAdapter;

    LinearLayoutManager linearLayoutManager, linearLayoutManager1, linearLayoutManager2, linearLayoutManager3;
    ArrayList<FileData> attachmentItemArrayList = new ArrayList<>();
    ArrayList<CommentData> commentDataArrayList = new ArrayList<>();
    ArrayList<SubTaskData> subTaskDataArrayList = new ArrayList<>();
    ArrayList<User> userArrayList = new ArrayList<>();
    ArrayList<Duration> durationArrayList = new ArrayList<>();
    TaskDetailItem items;
    Context context;
    String task_Id;
    Realm mRealm;
    boolean isUpdated = false;
    Datum mUserData;
    boolean isFromNotification = false;
    String subTaskId;

    @BindView(R.id.addSubTask)
    FloatingActionButton mAddSubTasks;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_page);
        ButterKnife.bind(this);

        context = this;
        mRealm = Realm.getDefaultInstance();
        mUserData = getLoginData();
        task_Id = String.valueOf(getIntent().getIntExtra("_id", 0));
        isFromNotification = getIntent().getBooleanExtra("isFromNotification", false);
        if (isNetworkAvailable()) {
            if (!TextUtils.isEmpty(task_Id)) {
                getData(task_Id);
            }
        } else {
            if (!TextUtils.isEmpty(task_Id)) {
                items = mRealm.where(TaskDetailItem.class).equalTo("intTaskDocumentNo", Integer.valueOf(task_Id)).findFirst();
                setData();
            }
        }

        mAddSubTasks.setOnClickListener(view -> {
            showSubTaskAddDialog();
        });
    }


    @SuppressLint({"SetTextI18n", "RestrictedApi"})
    public void setData() {
        tvHeaderType.setText(items.getStrCategoryName());
        if (items.getIntCreateUserId().equalsIgnoreCase(mUserData.getIntUserId())) {
            mAddSubTasks.setVisibility(View.VISIBLE);
        } else {
            mAddSubTasks.setVisibility(View.GONE);
        }

        if (items.getObjFileData() != null && items.getObjFileData().size() != 0) {
            attachmentItemArrayList.addAll(items.getObjFileData());
        }
        if (items.getTaskComments() != null && items.getTaskComments().size() != 0) {
            commentDataArrayList.addAll(items.getTaskComments().sort("addTime", Sort.ASCENDING));
        }
        if (items.getSubTasksData() != null && items.getSubTasksData().size() != 0) {
            subTaskDataArrayList.addAll(items.getSubTasksData().sort("addTime", Sort.ASCENDING));
        }
        linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        linearLayoutManager1 = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        linearLayoutManager2 = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        linearLayoutManager3 = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        attachmentView.setLayoutManager(linearLayoutManager);
        commentsView.setLayoutManager(linearLayoutManager1);
        userView.setLayoutManager(linearLayoutManager2);
        subTasksView.setLayoutManager(linearLayoutManager3);
        adapter = new AttachmentAdapter(context, attachmentItemArrayList, false);
        commentsAdapter = new CommentsAdapter(context, commentDataArrayList);
        subTasksAdapter = new SubTasksAdapter(context, subTaskDataArrayList);
        subTasksAdapter.setOnClickListner(onClickListner);

        attachmentView.setAdapter(adapter);
        commentsView.setAdapter(commentsAdapter);
        subTasksView.setAdapter(subTasksAdapter);
        if (subTaskDataArrayList.size() == 0) {
            subTasksLayout.setVisibility(View.GONE);
        } else {
            subTasksLayout.setVisibility(View.VISIBLE);
        }
        if (items.getIntCreateUserId().equalsIgnoreCase(mUserData.getIntUserId())) {
            for (int i = 0; i < items.getUsers().size(); i++) {
                if (items.getUsers().get(i).getDurations().size() != 0) {
                    if (items.getUsers().get(i).getDurations().get(0).getEndTime() != 0) {
                        userArrayList.add(items.getUsers().get(i));
                    }
                }
            }
            usersAdapter = new UsersAdapter(context, userArrayList, items);
            userView.setAdapter(usersAdapter);
            if (userArrayList.size() == 0) {
                durationsLayout.setVisibility(View.GONE);
            } else {
                durationsLayout.setVisibility(View.VISIBLE);
            }
        } else {
            boolean isF = false;
            ArrayList<Duration> durations = new ArrayList<>();
            for (int i = 0; i < items.getUsers().size() && !isF; i++) {
                if (items.getUsers().get(i).get_id().equalsIgnoreCase(mUserData.getIntUserId())) {
                    if (items.getUsers().get(i).getDurations().size() != 0) {
                        for (Duration d : items.getUsers().get(i).getDurations()) {
                            if (d.getEndTime() != 0) {
                                durations.add(d);
                            }
                        }
                    }
                    isF = true;
                }
            }
            durationArrayList.clear();
            if (durations != null) {
                durationArrayList.addAll(durations);
            }
            setDurationAdapter();
        }

        if (items.getCreateUser() != null) {
            if (items.getCreateUser().getImg() != null) {
                if (!TextUtils.isEmpty(items.getCreateUser().getImg().getPath())) {
                    Picasso.get().load(BASE_URL + items.getCreateUser().getImg().getPath()).placeholder(R.drawable.ic_error_male).error(R.drawable.ic_error_male).into(prof_img);
                }
            }
            if (!TextUtils.isEmpty(items.getCreateUser().getStrFirstName()) || !TextUtils.isEmpty(items.getCreateUser().getStrLastName())) {
                name.setText(items.getCreateUser().getStrFirstName() + " " + items.getCreateUser().getStrLastName());
            }
        }
        if (commentDataArrayList.size() != 0) {
            noOfComments.setText(" " + String.valueOf(commentDataArrayList.size()));
        } else {
            noOfComments.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(items.getCreatedDate())) {
            tvDate.setText("  Created on " + getMillsToDate(items.getCreatedDate()));
        }

        if (items.getDatStartToDueDate().size() != 0) {
            tvDuration.setVisibility(View.VISIBLE);
            tvDash.setVisibility(View.VISIBLE);
            tvStartDate.setVisibility(View.VISIBLE);
            tvEndDate.setVisibility(View.VISIBLE);
            tvStartDate.setText(getMillsToDate(items.getDatStartToDueDate().get(0).getStartDate()));
            tvEndDate.setText(getMillsToDate(items.getDatStartToDueDate().get(0).getDueDate()));
            long duration = getDaysBetweenDates(getMillsToDate(items.getDatStartToDueDate().get(0).getStartDate()), getMillsToDate(items.getDatStartToDueDate().get(0).getDueDate()));

            tvDuration.setText(duration == 1 ? (" " + duration + " day") : (" " + duration + " days"));

        } else if (!TextUtils.isEmpty(items.getIntHour()) && !TextUtils.isEmpty(items.getIntMinut())) {
            String duration = (items.getIntMinut().equalsIgnoreCase("1") ? (items.getIntHour() + " hour ") : (items.getIntHour() + " hours ")) +
                    (items.getIntHour().equalsIgnoreCase("1") ? (items.getIntMinut() + " minute") : (items.getIntMinut() + " minutes"));

            tvDuration.setText(duration);
            tvStartDate.setVisibility(View.GONE);
            tvEndDate.setVisibility(View.GONE);
            tvDash.setVisibility(View.GONE);

        } else {
            tvDash.setVisibility(View.GONE);
            tvDuration.setVisibility(View.GONE);
            tvStartDate.setVisibility(View.GONE);
            tvEndDate.setVisibility(View.GONE);
        }


        String prior = items.getStrPriorityValue();

        if (!TextUtils.isEmpty(prior)) {
            tvPriorType.setVisibility(View.VISIBLE);
            switch (prior) {
                case "1":
                    tvPriorType.setText("Low Priority");
                    tvPriorType.setTextColor(context.getResources().getColor(R.color.blue));
                    break;
                case "2":
                    tvPriorType.setText("Medium Priority");
                    tvPriorType.setTextColor(context.getResources().getColor(R.color.orange));
                    break;
                case "3":
                    tvPriorType.setText("High Priority");
                    tvPriorType.setTextColor(context.getResources().getColor(R.color.red));
                    break;
                case "4":
                    tvTitle.setPaintFlags(tvTitle.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    tvTitle.setTextColor(Color.GRAY);
                    tvPriorType.setText("Done");
                    tvPriorType.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    break;
            }
        } else {
            tvPriorType.setVisibility(View.GONE);
        }

        tvTitle.setText(items.getStrDescription());

        mSend.setOnClickListener(v -> {
            if (!isNetworkAvailable()) {
                showToast(context.getString(R.string.noConnection));
                return;
            }
            String txt = mCommentEdtTxt.getText().toString().trim();
            if (!TextUtils.isEmpty(txt)) {
                sendComment(txt);
            }
        });

    }

    public void setDurationAdapter() {
        durationAdapter = new DurationAdapter(context, durationArrayList, items, this::getData);
        userView.setAdapter(durationAdapter);
        if (durationArrayList.size() == 0) {
            durationsLayout.setVisibility(View.GONE);
        } else {
            durationsLayout.setVisibility(View.VISIBLE);
        }
    }
    @Override
    public void OnResponce(JSONObject object, ApiCall type) {
        hideProgressDialog(DetailPageActivity.this);
        try {
            Gson gson = new GsonBuilder().create();
            if (type == ApiCall.ADD_COMMENT) {
                CommentData commentData = gson.fromJson(object.getJSONObject("data").getJSONObject("commentData").toString(), CommentData.class);
                if (commentData != null) {
                    commentDataArrayList.add(0, commentData);
                    mRealm.beginTransaction();
                    TaskDetailItem items = mRealm.where(TaskDetailItem.class).equalTo("_id", this.items.get_id()).findFirst();
                    items.getTaskComments().add(commentData);
                    mRealm.commitTransaction();
                    commentsAdapter.notifyDataSetChanged();
                    isUpdated = true;
                }
                mCommentEdtTxt.setText("");
            }
            if (type == ApiCall.GET_TASK_DATA) {
                TaskDetailItem items = gson.fromJson(object.getJSONObject("data").getJSONObject("taskData").toString(), TaskDetailItem.class);
                items.setDetailAvailable(true);
                mRealm.beginTransaction();
                mRealm.copyToRealmOrUpdate(items);
                mRealm.commitTransaction();
                this.items = mRealm.where(TaskDetailItem.class).equalTo("_id", items.get_id()).findFirst();
                setData();
            }
            if (type == ApiCall.ADD_SUBTASK_DATA) {
                SubTaskData data = gson.fromJson(object.getJSONObject("data").getJSONObject("subTaskData").toString(), SubTaskData.class);
                mRealm.beginTransaction();
                this.items.getSubTasksData().add(data);
                mRealm.commitTransaction();
                subTaskDataArrayList.clear();
                if (items.getSubTasksData() != null && items.getSubTasksData().size() != 0) {
                    subTaskDataArrayList.addAll(items.getSubTasksData().sort("addTime", Sort.ASCENDING));
                }
                subTasksAdapter.notifyDataSetChanged();
                if (subTaskDataArrayList.size() == 0) {
                    subTasksLayout.setVisibility(View.GONE);
                } else {
                    subTasksLayout.setVisibility(View.VISIBLE);
                }
            }
            if (type == ApiCall.REMOVE_SUB_TASK_DATA) {
                SubTaskData data = mRealm.where(SubTaskData.class).equalTo("_id", subTaskId).findFirst();
                mRealm.beginTransaction();
                if (data != null) {
                    data.deleteFromRealm();
                }
                mRealm.commitTransaction();
                subTaskDataArrayList.clear();
                if (items.getSubTasksData() != null && items.getSubTasksData().size() != 0) {
                    subTaskDataArrayList.addAll(items.getSubTasksData().sort("addTime", Sort.ASCENDING));
                }
                subTasksAdapter.notifyDataSetChanged();
                if (subTaskDataArrayList.size() == 0) {
                    subTasksLayout.setVisibility(View.GONE);
                } else {
                    subTasksLayout.setVisibility(View.VISIBLE);
                }
                subTaskId = "";
            }
        } catch (JSONException e) {
            mCommentEdtTxt.setText("");
            e.printStackTrace();
        }

    }

    @Override
    public void OnError(JSONObject object, ApiCall type) {
        try {
            if (type == ApiCall.ADD_COMMENT) {
                showToast(object.getString("message"));
            }
            if (type == ApiCall.GET_TASK_DATA) {
                showToast(object.getString("message"));
                finish();
            }
            if (type == ApiCall.ADD_SUBTASK_DATA) {
                hideProgressDialog(this);
                showToast(object.getString("message"));
            }
            if (type == ApiCall.REMOVE_SUB_TASK_DATA) {
                subTaskId = "";
                hideProgressDialog(this);
                showToast(object.getString("message"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    SubTasksAdapter.OnClickListner onClickListner = position -> {
        subTaskId = subTaskDataArrayList.get(position).get_id();
        RequestParams params = new RequestParams();
        params.put("intTaskDocumentNo", items.getIntTaskDocumentNo());
        params.put("subTaskId", subTaskDataArrayList.get(position).get_id());
        showProgressDialog(this, "Please wait...");
        ApiCallWithToken(removeSubTaskData, ApiCall.REMOVE_SUB_TASK_DATA, params);
    };


    public void sendComment(String comment) {
        RequestParams params = new RequestParams();
        params.put("intTaskDocumentNo", items.getIntTaskDocumentNo());
        params.put("intUserId", mUserData.getIntUserId());
        params.put("comment", comment);
        showProgressDialog(this, "Please wait...");
        ApiCallWithToken(addComment, ApiCall.ADD_COMMENT, params);
    }

    public long getDaysBetweenDates(String start, String end) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
        Date startDate, endDate;
        long numberOfDays = 0;
        try {
            startDate = dateFormat.parse(start);
            endDate = dateFormat.parse(end);
            numberOfDays = getUnitBetweenDates(startDate, endDate, TimeUnit.DAYS);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return numberOfDays;
    }

    private long getUnitBetweenDates(Date startDate, Date endDate, TimeUnit unit) {
        long timeDiff = endDate.getTime() - startDate.getTime();
        return unit.convert(timeDiff, TimeUnit.MILLISECONDS);
    }

    public void getData(String id) {
        RequestParams params = new RequestParams();
        params.put("intTaskDocumentNo", id);
        showProgressDialog(this, "Please wait...");
        ApiCallWithToken(getTaskData, ApiCall.GET_TASK_DATA, params);
    }

    @Override
    public void onBackPressed() {
        if (isFromNotification) {
            Intent i = new Intent(this, MainActivity.class);
            startActivity(i);
        } else {
            Intent i = new Intent();
            i.putExtra("isUpdated", isUpdated);
            setResult(RESULT_OK, i);
            finish();
        }
    }


    public void showSubTaskAddDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_sub_task);
        CustomEditText mTitle = dialog.findViewById(R.id.sub_task_title);
        CustomEditText mDesc = dialog.findViewById(R.id.desc);
        CustomButton btOk = dialog.findViewById(R.id.submit_sub_task);

        btOk.setOnClickListener(v -> {
            String title = mTitle.getText().toString().trim();
            String desc = mDesc.getText().toString().trim();
            if (TextUtils.isEmpty(title)) {
                showToast("Please enter Title");
                return;
            }
            if (TextUtils.isEmpty(desc)) {
                showToast("Please enter Description");
                return;
            }

            RequestParams params = new RequestParams();
            params.put("intTaskDocumentNo", items.getIntTaskDocumentNo());
            params.put("intUserId", mUserData.getIntUserId());
            params.put("title", title);
            params.put("description", desc);
            showProgressDialog(context, "Please wait...");
            ApiCallWithToken(addSubTaskData, ApiCall.ADD_SUBTASK_DATA, params);

            dialog.dismiss();
        });
        dialog.show();
    }

}
