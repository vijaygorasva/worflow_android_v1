package com.example.user.communicator.Activity.TaskModule;import android.content.Context;import android.content.Intent;import android.content.SharedPreferences;import android.graphics.Bitmap;import android.graphics.BitmapFactory;import android.os.Bundle;import android.support.annotation.Nullable;import android.support.v4.view.PagerAdapter;import android.support.v4.view.ViewPager;import android.text.TextUtils;import android.util.Log;import android.view.LayoutInflater;import android.view.View;import android.view.ViewGroup;import android.widget.ImageView;import com.bumptech.glide.Glide;import com.example.user.communicator.Model.Login.Datum;import com.example.user.communicator.Model.Notes.ImgDetails;import com.example.user.communicator.Model.Notes.Item;import com.example.user.communicator.R;import com.google.gson.Gson;import com.loopj.android.http.RequestParams;import org.json.JSONArray;import org.json.JSONException;import org.json.JSONObject;import java.util.ArrayList;import java.util.Timer;import io.realm.Realm;import io.realm.RealmList;public class ImageShowActivity extends BaseActivity {    private static ViewPager mPager;    private static int currentPage = 0;    //    ImageView mImg;    int pos;    ArrayList<ImgDetails> imgDetails = new ArrayList();    String type, strImageId, plannerId, strCategoryTypeId, strCategoryId, strSubCategoryId;    //    private static final Integer[] XMEN = {R.drawable.dummy, R.drawable.ic_logo, R.drawable.dummy, R.drawable.ic_logo};//    private ArrayList<String> XMENArray = new ArrayList<String>();//    ArrayList<String> allImages = new ArrayList<String>();    @Override    protected void onCreate(@Nullable Bundle savedInstanceState) {        super.onCreate(savedInstanceState);        setContentView(R.layout.activity_imageshow);        type = getIntent().getStringExtra("type");        pos = getIntent().getIntExtra("img_pos", 0);        imgDetails.addAll(getIntent().getParcelableArrayListExtra("img_list"));        if (type.equals("Detail")) {            strImageId = getIntent().getStringExtra("strImageId");            plannerId = getIntent().getStringExtra("intPlannerId");            strCategoryTypeId = getIntent().getStringExtra("strCategoryTypeId");            strCategoryId = getIntent().getStringExtra("strCategoryId");            strSubCategoryId = getIntent().getStringExtra("strSubCategoryId");        }        init();    }    private void init() {        mPager = (ViewPager) findViewById(R.id.pager);        mPager.setAdapter(new MyAdapter(this, imgDetails));        // Auto start of viewpager        if (currentPage == imgDetails.size()) {            currentPage = pos;        }        mPager.setCurrentItem(pos, true);        Timer swipeTimer = new Timer();//        swipeTimer.schedule(new TimerTask() {//            @Override//            public void run() {//                handler.post(Update);//            }//        }, 2500, 2500);    }    public void deleteImage(String id) {        SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences("Login", MODE_PRIVATE);        String data = sharedpreferences.getString("UserData", null);        Gson gson = new Gson();        Datum mUserDetails = gson.fromJson(data, Datum.class);        String strUserId = mUserDetails.getIntUserId();        RequestParams params = new RequestParams();        Log.e("Delete", pos + "..." + id + "...." + imgDetails.get(pos).getIntImageId());        params.put("strImageId", id);        params.put("intPlannerId", plannerId);        params.put("strModifiedUserId", strUserId);        params.put("strCategoryTypeId", strCategoryTypeId);        params.put("strCategoryId", strCategoryId);        params.put("strSubCategoryId", strSubCategoryId);        showProgressDialog(ImageShowActivity.this, "Removing...");        Log.e("Dele_img", urldeleteNoteImg + "..." + params);        ApiCallWithToken(urldeleteNoteImg, ApiCall.NOTE_IMAGE_DELETE, params);    }    @Override    public void OnResponce(JSONObject data, ApiCall type) {        JSONArray array = null;        hideProgressDialog(getApplicationContext());        if (type == ApiCall.NOTE_IMAGE_DELETE) {            Realm mRealm = Realm.getDefaultInstance();            RealmList<ImgDetails> imgDetailss = new RealmList<>();            mPager.setAdapter(new MyAdapter(this, imgDetails));            Gson gson = new Gson();            try {                array = data.getJSONArray("data");                if (data.length() != 0) {                    for (int i = 0; i < array.length(); i++) {                        try {                            JSONObject object = array.getJSONObject(i);                            Item img = gson.fromJson(object.toString(), Item.class);                            mRealm.beginTransaction();                            mRealm.copyToRealmOrUpdate(img);                            imgDetailss.addAll(img.getImgNoteList());                            mRealm.commitTransaction();                            plannerId = String.valueOf(img.getIntPlannerId());                            Log.e("ImageSave", plannerId + "...." + imgDetailss.size() + "...");                        } catch (JSONException e) {                            e.printStackTrace();                        }                    }                    showToast("Removed");                }            } catch (JSONException e) {                e.printStackTrace();            }        }        super.OnResponce(data, type);    }    @Override    public void onBackPressed() {        Intent intent = new Intent();        intent.putExtra("plannerId", plannerId);        (ImageShowActivity.this).setResult(1001, intent);        finish();        super.onBackPressed();    }    public class MyAdapter extends PagerAdapter {        private ArrayList<ImgDetails> images;        private LayoutInflater inflater;        private Context context;        public MyAdapter(Context context, ArrayList<ImgDetails> images) {            this.context = context;            this.images = images;            inflater = LayoutInflater.from(context);        }        @Override        public void destroyItem(ViewGroup container, int position, Object object) {            container.removeView((View) object);        }        @Override        public int getCount() {            return images.size();        }        @Override        public Object instantiateItem(ViewGroup view, int position) {            View myImageLayout = inflater.inflate(R.layout.slide, view, false);            ImageView myImage = (ImageView) myImageLayout.findViewById(R.id.image);            ImageView ivDelete = (ImageView) myImageLayout.findViewById(R.id.ivDelete);            if (TextUtils.isEmpty(images.get(position).getFilename())) {                Bitmap myBitmap = BitmapFactory.decodeFile(images.get(position).getFilename());                myImage.setImageBitmap(myBitmap);            } else {//                if (type.equals("PLANNER")) {//                    myImage.setImageURI(Uri.parse(images.get(position).getFilename()));////                } else {                String imagePath = URL_ImageUpload_ROOT + images.get(position).getFilename();                Log.e("imagePath", plannerId + "...." + imagePath + "...");                Glide.with(getApplicationContext())                        .load(imagePath)                        .into(myImage);//                }            }            view.addView(myImageLayout, 0);            ivDelete.setOnClickListener(new View.OnClickListener() {                @Override                public void onClick(View v) {                    mPager.removeView(view);                    mPager.setCurrentItem(position + 1, true);                    deleteImage(images.get(position).getIntImageId());                    imgDetails.remove(position);                    notifyDataSetChanged();                }            });            return myImageLayout;        }        @Override        public boolean isViewFromObject(View view, Object object) {            return view.equals(object);        }    }}