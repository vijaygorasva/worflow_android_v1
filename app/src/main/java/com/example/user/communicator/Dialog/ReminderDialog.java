package com.example.user.communicator.Dialog;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.InputType;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.user.communicator.R;
import com.example.user.communicator.Utility.DateUtils;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReminderDialog extends DialogFragment {

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    @BindView(R.id.ivDelete)
    ImageView ivDelete;

    @BindView(R.id.etTime)
    MaterialEditText etTime;

    @BindView(R.id.etDate)
    MaterialEditText etDate;

    @BindView(R.id.btnCancel)
    Button btnCancel;

    Callback callback;
    @BindView(R.id.spinner)
    MaterialSpinner spinner;

    String oldTime, oldType, oldDate;
    String memType = "Do not repeat", date;
    private int mYear, mMonth, mDay;

    Boolean isValue = false;

    public static ReminderDialog newIntance(String oldType, String oldDate, String oldTime, Callback callback) {
        Bundle args = new Bundle();
        args.putString("oldType", oldType);
        args.putString("oldDate", oldDate);
        args.putString("oldTime", oldTime);

        ReminderDialog dialog = new ReminderDialog();
        dialog.setArguments(args);
        dialog.setCallback(callback);
        return dialog;
    }

//    private void setArguments(ArrayList<DependencyList> dependencyListArrayList) {
//        dependencyListArrayList.addAll(dependencyListArrayList);
//    }

    public ReminderDialog() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, getTheme());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_reminder, container, false);
        ButterKnife.bind(this, view);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.border_roundeed);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();

        oldType = args.getString("oldType");
        oldDate = args.getString("oldDate");
        oldTime = args.getString("oldTime");
        if (!oldDate.equals("null")) {
            etDate.setText(oldDate);
            isValue = true;
            etTime.setText(oldTime);
        }
        etDate.setInputType(InputType.TYPE_NULL);
        etTime.setInputType(InputType.TYPE_NULL);

        spinner.setItems("Do not repeat", "Daily", "Weekly", "Monthly");
        int index;

        if (oldType.equals("Monthly")) {
            index = 3;
        } else if (oldType.equals("Weekly")) {
            index = 2;
        } else if (oldType.equals("Daily")) {
            index = 1;
        } else {
            index = 0;
        }

        if ((!oldType.equals("null")) || (!oldDate.equals("null")) || (!oldTime.equals("null"))) {
            ivDelete.setVisibility(View.VISIBLE);
        } else ivDelete.setVisibility(View.GONE);

        spinner.setSelectedIndex(index);
        spinner.setOnItemSelectedListener((MaterialSpinner.OnItemSelectedListener<String>) (view13, position, id, item) -> {
            memType = item;
        });

        ivDelete.setOnClickListener(v -> {
            callback.edit(ReminderDialog.this, "", "", "");
            dismiss();
        });

        etDate.setOnClickListener(v -> {
            etDate.setHideUnderline(false);
            etTime.setHideUnderline(true);
            if (!oldDate.equals("null")) {
                etDate.setText(oldDate);
                isValue = true;
                SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    Date dt1 = format1.parse(oldDate);
                    String year = (String) DateFormat.format("yyyy", dt1); // Thursday
                    String month = (String) DateFormat.format("MM", dt1);
                    String day = (String) DateFormat.format("dd", dt1);
                    mYear = Integer.parseInt(year);
                    mMonth = Integer.parseInt(month) - 1;
                    mDay = Integer.parseInt(day);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            etDate.setInputType(InputType.TYPE_NULL);
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view12, int year, int monthOfYear, int dayOfMonth) {
                            date = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
                            SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
                            Date dateObj = null;
                            try {
                                dateObj = curFormater.parse(date);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            SimpleDateFormat postFormater = new SimpleDateFormat("yyyy-MM-dd");
                            String newDateStr = postFormater.format(dateObj);
                            oldDate = newDateStr;
                            etDate.setText(newDateStr);
                            isValue = true;
                            etTime.requestFocus();

                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.show();
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);

        });

        etTime.setOnClickListener(v -> {

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, 1);
            dateFormat.format(cal.getTime());

            etTime.setHideUnderline(false);
            etDate.setHideUnderline(true);
            etTime.setInputType(InputType.TYPE_NULL);
            int hour, minute;
            String amPm = "";
            if (!oldTime.equals("null")) {
                hour = Integer.parseInt(oldTime.substring(0, 2));
                minute = Integer.parseInt(oldTime.substring(3, 5));
                amPm = oldTime.substring(6, 8);
                if (amPm.equals("PM")) {
                    hour = hour + 12;
                }
            } else {
                Calendar mcurrentTime = Calendar.getInstance();
                hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                minute = mcurrentTime.get(Calendar.MINUTE);
            }

            String date;

            try {
                Boolean check_today = DateUtils.isToday(new SimpleDateFormat("yyyy-MM-dd").parse(etDate.getText().toString()));

                if (check_today) {
                    date = dateFormat.format(cal.getTime());
                } else {
                    date = etDate.getText().toString();
                }
                if ((new SimpleDateFormat("yyyy-MM-dd").parse(date).before(new Date()))) {
                    etDate.setErrorColor(Color.RED);
                    etDate.setError("The date has expired.." + date + "..." + check_today);
                    isValue = false;
                } else {
                    TimePickerDialog mTimePicker = new TimePickerDialog(getContext(),
                            (TimePicker view1, int hourOfDay, int minute1) -> {

                                getFormatedTime(hourOfDay, minute1);
                                Calendar datetime = Calendar.getInstance();
                                Calendar c = Calendar.getInstance();
                                datetime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                datetime.set(Calendar.MINUTE, minute1);
                                if (check_today) {
                                    if (datetime.getTimeInMillis() > c.getTimeInMillis()) {
                                        int hourDay = hourOfDay % 12;
                                        etTime.setText(String.format("%02d:%02d %s", hourDay == 0 ? 12 : hourDay, minute1, hourOfDay < 12 ? "am" : "pm"));
                                        oldTime = getFormatedTime(hourOfDay, minute1);
                                        isValue = true;
                                    } else {
                                        etTime.setText("");
                                        etTime.setError("The time has expired");
                                        etTime.setErrorColor(Color.RED);
                                        isValue = false;
                                    }
                                } else {
                                    int hourDay = hourOfDay % 12;
                                    etTime.setText(String.format("%02d:%02d %s", hourDay == 0 ? 12 : hourDay, minute1, hourOfDay < 12 ? "am" : "pm"));
                                    oldTime = getFormatedTime(hourOfDay, minute1);
                                    isValue = true;
                                }
                            }, hour, minute, false);

                    mTimePicker.setTitle("Select Time");
                    mTimePicker.show();
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        });

    }

    public String getFormatedTime(int h, int m) {
        final String OLD_FORMAT = "HH:mm";
        final String NEW_FORMAT = "hh:mm a";

        String oldDateString = h + ":" + m;
        String newDateString = "";

        try {
            SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT, getCurrentLocale());
            Date d = sdf.parse(oldDateString);
            sdf.applyPattern(NEW_FORMAT);
            newDateString = sdf.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return newDateString;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public Locale getCurrentLocale() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return getResources().getConfiguration().getLocales().get(0);
        } else {
            //noinspection deprecation
            return getResources().getConfiguration().locale;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {

        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btnCancel)
    public void onCancelClick() {
        dismiss();
    }

    @Override
    public void onDestroyView() {

        super.onDestroyView();
    }

    @OnClick(R.id.btnSubmit)
    public void onSubmitClick() {

        if (isNetworkAvailable()) {
            if (isValue) {
                String time = etTime.getText().toString();
                String date = etDate.getText().toString();
                callback.edit(ReminderDialog.this, date, time, memType);
                dismiss();
            }
        } else {
            showToast("No Internet Connection");
        }

    }

    public void showToast(String message) {
        LayoutInflater inflater = getLayoutInflater();
        View layouttoast = inflater.inflate(R.layout.toastcustom, (ViewGroup) getActivity().findViewById(R.id.toastcustom));
        ((TextView) layouttoast.findViewById(R.id.texttoast)).setText(message);

        Toast mytoast = new Toast(getActivity());
        mytoast.setView(layouttoast);
        mytoast.setDuration(Toast.LENGTH_LONG);
        mytoast.setGravity(Gravity.CENTER, 0, 0);
        mytoast.show();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public interface Callback {
        void edit(ReminderDialog dialog, String date, String time, String name);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}