package com.example.user.communicator.Activity.ChatModule;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.example.user.communicator.Activity.TaskModule.BaseActivity;
import com.example.user.communicator.R;

public class WebActivity extends BaseActivity {
    WebView webView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        webView = findViewById(R.id.webView);

        String url = getIntent().getStringExtra("url");
        String doc = "https://docs.google.com/gview?embedded=true&url=" + url;
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.setWebChromeClient(new WebChromeClient());
        webView.loadUrl(doc);
    }
}
