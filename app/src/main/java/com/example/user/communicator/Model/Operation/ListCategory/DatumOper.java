
package com.example.user.communicator.Model.Operation.ListCategory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class DatumOper extends RealmObject {

    @PrimaryKey
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("strOperationMainCategoryName")
    @Expose
    private String strOperationMainCategoryName;
    @SerializedName("intCreateUserId")
    @Expose
    private String intCreateUserId;
    @SerializedName("datCreateDateAndTime")
    @Expose
    private String datCreateDateAndTime;
    @SerializedName("intProjectId")
    @Expose
    private String intProjectId;
    @SerializedName("intOrganisationId")
    @Expose
    private String intOrganisationId;

    public String getDatLastModifiedDateAndTime() {
        return datLastModifiedDateAndTime;
    }

    public void setDatLastModifiedDateAndTime(String datLastModifiedDateAndTime) {
        this.datLastModifiedDateAndTime = datLastModifiedDateAndTime;
    }

    public String getIntLastModifiedUserId() {
        return intLastModifiedUserId;
    }

    public void setIntLastModifiedUserId(String intLastModifiedUserId) {
        this.intLastModifiedUserId = intLastModifiedUserId;
    }

    @SerializedName("datLastModifiedDateAndTime")
    @Expose
    private String datLastModifiedDateAndTime;
    @SerializedName("intLastModifiedUserId")
    @Expose
    private String intLastModifiedUserId;
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStrOperationMainCategoryName() {
        return strOperationMainCategoryName;
    }

    public void setStrOperationMainCategoryName(String strOperationMainCategoryName) {
        this.strOperationMainCategoryName = strOperationMainCategoryName;
    }

    public String getIntCreateUserId() {
        return intCreateUserId;
    }

    public void setIntCreateUserId(String intCreateUserId) {
        this.intCreateUserId = intCreateUserId;
    }

    public String getDatCreateDateAndTime() {
        return datCreateDateAndTime;
    }

    public void setDatCreateDateAndTime(String datCreateDateAndTime) {
        this.datCreateDateAndTime = datCreateDateAndTime;
    }

    public String getIntProjectId() {
        return intProjectId;
    }

    public void setIntProjectId(String intProjectId) {
        this.intProjectId = intProjectId;
    }

    public String getIntOrganisationId() {
        return intOrganisationId;
    }

    public void setIntOrganisationId(String intOrganisationId) {
        this.intOrganisationId = intOrganisationId;
    }

}
