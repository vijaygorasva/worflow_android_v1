package com.example.user.communicator.Model.BrodcastsModels;

public class OptionItem {

    private String name;
    private boolean isChacked = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChacked() {
        return isChacked;
    }

    public void setChacked(boolean chacked) {
        isChacked = chacked;
    }
}
