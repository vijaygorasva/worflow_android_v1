package com.example.user.communicator.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.user.communicator.Activity.TaskModule.BaseActivity;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.R;
import com.google.gson.Gson;

import butterknife.ButterKnife;
import io.realm.Realm;

public class SplashscreenActivity extends BaseActivity {

    private static int SPLASH_TIME_OUT = 2000;

    Realm mRealm;
    Datum mUserDetails;
    String strUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        ButterKnife.bind(this);
        mRealm = Realm.getDefaultInstance();
        mUserDetails = getLoginData();
        new Handler().postDelayed(() -> {
            if (mUserDetails == null) {
                Intent i = new Intent(SplashscreenActivity.this, LoginActivity.class);//MainActivity
                startActivity(i);
                finish();
            } else {
                Intent i = new Intent(SplashscreenActivity.this, MainActivity.class);//MainActivity
                startActivity(i);
                finish();
            }

        }, SPLASH_TIME_OUT);
    }
}
