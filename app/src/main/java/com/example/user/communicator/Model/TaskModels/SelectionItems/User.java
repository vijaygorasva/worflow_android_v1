package com.example.user.communicator.Model.TaskModels.SelectionItems;


import com.example.user.communicator.Model.TaskModels.Duration;
import com.example.user.communicator.Model.TaskModels.ImageData;

import io.realm.RealmList;
import io.realm.RealmObject;

public class User extends RealmObject {

    private String _id;
    private String intLevelTypeId;
    private String strFirstName;
    private String strLastName;
    private int isRunning;
    private ImageData img;
    private RealmList<Duration> durations = new RealmList<>();

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getIntLevelTypeId() {
        return intLevelTypeId;
    }

    public void setIntLevelTypeId(String intLevelTypeId) {
        this.intLevelTypeId = intLevelTypeId;
    }

    public String getStrFirstName() {
        return strFirstName;
    }

    public void setStrFirstName(String strFirstName) {
        this.strFirstName = strFirstName;
    }

    public String getStrLastName() {
        return strLastName;
    }

    public void setStrLastName(String strLastName) {
        this.strLastName = strLastName;
    }

    public int getIsRunning() {
        return isRunning;
    }

    public void setIsRunning(int isRunning) {
        this.isRunning = isRunning;
    }

    public RealmList<Duration> getDurations() {
        return durations;
    }

    public void setDurations(RealmList<Duration> durations) {
        this.durations = durations;
    }

    public ImageData getImg() {
        return img;
    }

    public void setImg(ImageData img) {
        this.img = img;
    }
}
