package com.example.user.communicator.Adapter.TaskAdapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.TaskModels.Duration;
import com.example.user.communicator.Model.TaskModels.TaskDetailItem;
import com.example.user.communicator.R;
import com.example.user.communicator.Utility.DateUtils;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DurationAdapter extends RecyclerView.Adapter<DurationAdapter.ViewHolder> {

    private Context context;
    private ArrayList<Duration> taskItems;
    TaskDetailItem mItem;
    //    private OnClickListner onClick;
    public static String tot;
    public static ArrayList<String> timestampsList = new ArrayList<String>();
    Callback callback;

    public DurationAdapter(Context context, ArrayList<Duration> taskItems, TaskDetailItem mItem, Callback callback) {
        this.taskItems = taskItems;
        this.context = context;
        this.mItem = mItem;
        this.callback = callback;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_duration_item, parent, false);
        return new ViewHolder(view);
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(taskItems.get(position));
    }

    @Override
    public int getItemCount() {
        return taskItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

//    public void setOnClickListner(OnClickListner onClickListner) {
//        this.onClick = onClickListner;
//    }


    private String getMillsToDate(long mills) {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
        return df.format(new Date(mills));
    }

    private String getMillsToDate1(long mills) {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        return df.format(new Date(mills));
    }

    private String getDiffrenece(long startDate, long endDate) {
        long difference = (endDate - startDate);
        long sec = difference / 1000 % 60;
        long min = difference / (60 * 1000) % 60;
        long hours = difference / (60 * 60 * 1000) % 24;
        long days = difference / (24 * 60 * 60 * 1000);
        hours = (hours < 0 ? -hours : hours);

        return (days != 0 ? (days != 1 ? days + " days " : days + " day ") : "") +
                (hours != 0 ? (hours != 1 ? hours + " hours " : hours + " hour ") : "") +
                (min != 0 ? (min != 1 ? min + " minutes " : min + " minute ") : "") +
                (sec != 0 ? (sec != 1 ? sec + " seconds " : sec + " second ") : " ");
    }

    public void setOnClickListner(Callback callback1) {
        this.callback = callback1;
    }

    public interface OnClickListner {
        void OnClick(int position);
    }

    public interface Callback {
        void timeTotal(String total);

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.timeLine)
        CustomTextView timeline;

        @BindView(R.id.name)
        CustomTextView name;

        @BindView(R.id.totaltime)
        CustomTextView totalTime;

        @BindView(R.id.d)
        CustomTextView dTxt;

        @BindView(R.id.day_count)
        CustomTextView day_count;

        @BindView(R.id.hours_count)
        CustomTextView hours_count;

        @BindView(R.id.minutes_count)
        CustomTextView minutes_count;

        @BindView(R.id.days)
        LinearLayout mDays;

        @BindView(R.id.hours)
        LinearLayout mHours;

        @BindView(R.id.minutes)
        LinearLayout mMinutes;

        @BindView(R.id.line)
        View line;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        @SuppressLint("SetTextI18n")
        public void bind(final Duration item) {

            if (getAdapterPosition() == 0) {
                timestampsList.clear();
            }
            if (item.getEndTime() != 0 && !TextUtils.isEmpty(getMillsToDate(item.getEndTime()))) {

                Datum mUser = getLoginData();
                name.setText(mUser.getStrFirstName() + " " + mUser.getStrLastName());

                timeline.setText(getMillsToDate(item.getStartTime()) + " to " + getMillsToDate(item.getEndTime()));
//                if (mItem.getDatStartToDueDate().size() != 0) {
//                    totalTime.setText(getMillsToDate1(mItem.getDatStartToDueDate().get(0)) + " to " +
//                            getMillsToDate1(mItem.getDatStartToDueDate().get(1)));
//                } else {
//                    totalTime.setVisibility(View.GONE);
//                }

                long difference = (item.getEndTime() - item.getStartTime());
                long sec = difference / 1000 % 60;
                long min = difference / (60 * 1000) % 60;
                long hours = difference / (60 * 60 * 1000) % 24;
                long days = difference / (24 * 60 * 60 * 1000);


                String d = (days != 0 ? (days != 1 ? "days " : "day ") : "");

                if (days != 0) {
                    mDays.setVisibility(View.VISIBLE);
                    day_count.setText(String.valueOf(days));
                    dTxt.setText(d);
                } else {
                    mDays.setVisibility(View.GONE);
                }
                if (hours != 0) {
                    mHours.setVisibility(View.VISIBLE);
                    hours_count.setText(String.valueOf(hours));
                } else {
                    mHours.setVisibility(View.GONE);
                }
                if (min != 0) {
                    mMinutes.setVisibility(View.VISIBLE);
                    minutes_count.setText(String.valueOf(min));
                } else {
                    mMinutes.setVisibility(View.GONE);
                }
                if (days == 0 && hours == 0 && min == 0) {
                    if (sec != 0) {
                        mDays.setVisibility(View.VISIBLE);
                        day_count.setText(String.valueOf(sec));
                        dTxt.setText("sec");
                    }
                }

                timestampsList.add(hours + ":" + min + ":" + sec);//"01:00:05"
            } else {
                itemView.setVisibility(View.GONE);
            }

            if (getAdapterPosition() == taskItems.size() - 1) {
                line.setVisibility(View.GONE);
                tot = DateUtils.totalTimeTaken(timestampsList);
                callback.timeTotal(tot);
            } else {
                line.setVisibility(View.VISIBLE);
            }
        }
    }

    public Datum getLoginData() {
        SharedPreferences sharedpreferences = context.getApplicationContext().getSharedPreferences("Login", context.MODE_PRIVATE);
        String data = sharedpreferences.getString("UserData", null);
        if (!TextUtils.isEmpty(data)) {
            Gson gson = new Gson();
//            return gson.fromJson(data, Login.class);
            return gson.fromJson(data, Datum.class);
        }
        return null;
    }


}
