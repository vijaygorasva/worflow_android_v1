
package com.example.user.communicator.Model.Notes;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Item extends RealmObject implements Parcelable {

    public static final Parcelable.Creator<Item> CREATOR = new Parcelable.Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };
    @SerializedName("canRedo")//True if the model can currently redo.
    @Expose
    private Boolean canRedo;
    @SerializedName("canUndo")
    @Expose
    private Boolean canUndo;//True if the model can currently undo.
    @SerializedName("pinned")
    @Expose
    private String pinned;
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("strTitle")
    @Expose
    private String strTitle;
    @SerializedName("strValue")
    @Expose
    private String strValue;
    @SerializedName("strColor")
    @Expose
    private String strColor;
    @SerializedName("intCreateUserId")
    @Expose
    private String intCreateUserId;
    @SerializedName("datCreateDateAndTime")
    @Expose
    private String datCreateDateAndTime;
    @PrimaryKey
    @SerializedName("intPlannerId")
    @Expose
    private int intPlannerId;
    @SerializedName("strStatus")
    @Expose
    private String strStatus;
    @SerializedName("blnPinNote")
    @Expose
    private Boolean blnPinNote;
    @SerializedName("intPlannerMainId")
    @Expose
    private String intPlannerMainId;
    @SerializedName("datModifiedDateAndTime")
    @Expose
    private String datModifiedDateAndTime;
//    @SerializedName("strMainCategoryname")
//    @Expose
//    private String strMainCategoryname;
//    @SerializedName("intHeight")
//    @Expose
//    private int intHeight;
//    @SerializedName("intWeight")
//    @Expose
//    private int intWeight;
    @SerializedName("intModifiedUserId")
    @Expose
    private String intModifiedUserId;
    @SerializedName("intOrganisationId")
    @Expose
    private String intOrganisationId;
    @SerializedName("intProjectId")
    @Expose
    private String intProjectId;

    @SerializedName("arrayObjAudioData")//arrayObjAudioData
    @Expose
    private RealmList<AudioDetails> fileAudioList;
    @SerializedName("arrayObjImgData")//arrayObjImgData
    @Expose
    private RealmList<ImgDetails> imgNoteList;

//    @SerializedName("imgNote")
//    @Expose
//    private ImgDetails imgNote;

    //    public ImgDetails getImgNote() {
//        return imgNote;
//    }
//
//    public void setImgNote(ImgDetails imgNote) {
//        this.imgNote = imgNote;
//    }
//
    @SerializedName("objRemainder")
    @Expose
    private ObjRemainder objRemainder;

    @SerializedName("arryObjstrTodoList")
    @Expose
    private RealmList<TodoListItem> arryObjstrTodoList = null;
    private boolean isSelected = false;
    @SerializedName("arryCollaboratUserId")
    @Expose
    private RealmList<String> arryCollaboratUserId = null;

    @SerializedName("arryRemoveCollaborateUserId")
    @Expose
    private RealmList<String> arryRemoveCollaborateUserId = null;


    @SerializedName("strLogAction")
    @Expose
    private String strLogAction;

    @SerializedName("strCategoryType")
    @Expose
    private String strCategoryTypeName;

    @SerializedName("strCategoryName")
    @Expose
    private String strCategoryName;

    @SerializedName("strDefaultSubName")
    @Expose
    private String strDefaultSubName;

    @SerializedName("fkIntCategoryTypeId")
    @Expose
    private String fkIntCategoryTypeId;

    @SerializedName("fkIntCategoryId")
    @Expose
    private String fkIntCategoryId;

    @SerializedName("fkIntSubCategoryId")
    @Expose
    private String fkIntSubCategoryId;

    protected Item(Parcel in) {
        id = in.readString();
        strTitle = in.readString();
        strValue = in.readString();
        strColor = in.readString();
        intCreateUserId = in.readString();
        datCreateDateAndTime = in.readString();
        intPlannerId = in.readInt();
        strStatus = in.readString();
        intPlannerMainId = in.readString();
        datModifiedDateAndTime = in.readString();
//        intWeight =


//        intHeight = in.readInt();
        intModifiedUserId = in.readString();
        intOrganisationId = in.readString();
        intProjectId = in.readString();

//        fileAudio = in.readParcelable(AudioDetails.class.getClassLoader());
        in.readTypedList(fileAudioList, AudioDetails.CREATOR);
        in.readTypedList(imgNoteList, ImgDetails.CREATOR);
//        imgNote = in.readParcelable(ImgDetails.class.getClassLoader());
        objRemainder = in.readParcelable(ObjRemainder.class.getClassLoader());

        this.arryObjstrTodoList = new RealmList<>();
        this.arryCollaboratUserId = new RealmList<>();
        in.readTypedList(arryObjstrTodoList, TodoListItem.CREATOR);
        in.readStringList(arryCollaboratUserId);
        this.arryRemoveCollaborateUserId = new RealmList<>();
        in.readStringList(arryRemoveCollaborateUserId);
        blnPinNote = in.readByte() != 0;
        strLogAction = in.readString();
        strCategoryTypeName = in.readString();
        strCategoryName = in.readString();
        strDefaultSubName = in.readString();

        fkIntCategoryTypeId = in.readString();
        fkIntCategoryId = in.readString();
        fkIntSubCategoryId = in.readString();
    }


    public String getStrCategoryTypeName() {
        return strCategoryTypeName;
    }

    public void setStrCategoryTypeName(String strCategoryTypeName) {
        this.strCategoryTypeName = strCategoryTypeName;
    }

    public String getStrDefaultSubName() {
        return strDefaultSubName;
    }

    public void setStrDefaultSubName(String strDefaultSubName) {
        this.strDefaultSubName = strDefaultSubName;
    }

    public String getStrCategoryName() {
        return strCategoryName;
    }

    public void setStrCategoryName(String strCategoryName) {
        this.strCategoryName = strCategoryName;
    }

    public String getfkIntCategoryTypeId() {
        return fkIntCategoryTypeId;
    }

    public void setfkIntCategoryTypeId(String fkIntCategoryTypeId) {
        this.fkIntCategoryTypeId = fkIntCategoryTypeId;
    }

    public String getfkIntCategoryId() {
        return fkIntCategoryId;
    }

    public void setfkIntCategoryId(String fkIntCategoryId) {
        this.fkIntCategoryId = fkIntCategoryId;
    }

    public String getfkIntSubCategoryId() {
        return fkIntSubCategoryId;
    }

    public void setfkIntSubCategoryId(String fkIntSubCategoryId) {
        this.fkIntSubCategoryId = fkIntSubCategoryId;
    }

    public String getStrLogAction() {
        return strLogAction;
    }

    public void setStrLogAction(String strLogAction) {
        this.strLogAction = strLogAction;
    }




    public Item() {
    }

    public String getPinned() {
        return pinned;
    }

    public void setPinned(String pinned) {
        this.pinned = pinned;
    }

    public Boolean getBlnPinNote() {
        return blnPinNote;
    }

    public void setBlnPinNote(Boolean blnPinNote) {
        this.blnPinNote = blnPinNote;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }


    public String getStrTitle() {
        return strTitle;
    }

    public void setStrTitle(String strTitle) {
        this.strTitle = strTitle;
    }

    public String getStrValue() {
        return strValue;
    }

    public void setStrValue(String strValue) {
        this.strValue = strValue;
    }

    public String getStrColor() {
        return strColor;
    }

    public void setStrColor(String strColor) {
        this.strColor = strColor;
    }

    public RealmList<TodoListItem> getArryObjstrTodoList() {
        return arryObjstrTodoList;
    }

    public void setArryObjstrTodoList(RealmList<TodoListItem> arryObjstrTodoList) {
        this.arryObjstrTodoList = arryObjstrTodoList;
    }

    public RealmList<ImgDetails> getImgNoteList() {
        return imgNoteList;
    }

    public void setImgNoteList(RealmList<ImgDetails> imgNoteList) {
        this.imgNoteList = imgNoteList;
    }

    public String getIntCreateUserId() {
        return intCreateUserId;
    }

    public void setIntCreateUserId(String intCreateUserId) {
        this.intCreateUserId = intCreateUserId;
    }

    public String getDatCreateDateAndTime() {
        return datCreateDateAndTime;
    }

    public void setDatCreateDateAndTime(String datCreateDateAndTime) {
        this.datCreateDateAndTime = datCreateDateAndTime;
    }

    public int getIntPlannerId() {
        return intPlannerId;
    }

    public void setIntPlannerId(int intPlannerId) {
        this.intPlannerId = intPlannerId;
    }

    public ObjRemainder getObjRemainder() {
        return objRemainder;
    }

    public void setObjRemainder(ObjRemainder objRemainder) {
        this.objRemainder = objRemainder;
    }

    public String getStrStatus() {
        return strStatus;
    }

    public void setStrStatus(String strStatus) {
        this.strStatus = strStatus;
    }

    public String getIntPlannerMainId() {
        return intPlannerMainId;
    }

    public void setIntPlannerMainId(String intPlannerMainId) {
        this.intPlannerMainId = intPlannerMainId;
    }

    public String getDatModifiedDateAndTime() {
        return datModifiedDateAndTime;
    }

    public void setDatModifiedDateAndTime(String datModifiedDateAndTime) {
        this.datModifiedDateAndTime = datModifiedDateAndTime;
    }


    public String get_id() {
        return id;
    }

    public void set_id(String _id) {
        this.id = _id;
    }

    public RealmList<AudioDetails> getFileAudioList() {
        return fileAudioList;
    }

    public void setFileAudioList(RealmList<AudioDetails> fileAudioList) {
        this.fileAudioList = fileAudioList;
    }

    //
    public RealmList<String> getArryCollaboratUserId() {
        return arryCollaboratUserId;
    }

    public void setArryCollaboratUserId(RealmList<String> arryCollaboratUserId) {
        this.arryCollaboratUserId = arryCollaboratUserId;
    }

    public RealmList<String> getRemoveArryCollaboratUserId() {
        return arryRemoveCollaborateUserId;
    }

    public void setRemoveArryCollaboratUserId(RealmList<String> arryRemoveCollaborateUserId) {
        this.arryRemoveCollaborateUserId = arryRemoveCollaborateUserId;
    }

//    public int getIntHeight() {
//        return intHeight;
//    }
//
//    public void setIntHeight(int intHeight) {
//        this.intHeight = intHeight;
//    }

    public String getIntModifiedUserId() {
        return intModifiedUserId;
    }

    public void setIntModifiedUserId(String intModifiedUserId) {
        this.intModifiedUserId = intModifiedUserId;
    }

    public String getIntOrganisationId() {
        return intOrganisationId;
    }

    public void setIntOrganisationId(String intOrganisationId) {
        this.intOrganisationId = intOrganisationId;
    }

    public String getIntProjectId() {
        return intProjectId;
    }

    public void setIntProjectId(String intProjectId) {
        this.intProjectId = intProjectId;
    }

//    public int getIntWeight() {
//        return intWeight;
//    }
//
//    public void setIntWeight(int intWeight) {
//        this.intWeight = intWeight;
//    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(id);
        parcel.writeString(strTitle);
        parcel.writeString(strValue);
        parcel.writeString(strColor);
        parcel.writeString(intCreateUserId);
        parcel.writeString(datCreateDateAndTime);
        parcel.writeInt(intPlannerId);
        parcel.writeString(strStatus);
        parcel.writeString(intPlannerMainId);
        parcel.writeString(datModifiedDateAndTime);
//        parcel.writeInt(intWeight);
//        parcel.writeInt(intHeight);
        parcel.writeString(intModifiedUserId);
        parcel.writeString(intOrganisationId);
        parcel.writeString(intProjectId);
//        parcel.writeParcelable(fileAudio, i);
        parcel.writeTypedList(fileAudioList);
        parcel.writeTypedList(imgNoteList);
        parcel.writeParcelable(objRemainder, i);
        parcel.writeTypedList(arryObjstrTodoList);
        parcel.writeStringList(arryCollaboratUserId);
        parcel.writeStringList(arryRemoveCollaborateUserId);
        parcel.writeByte((byte) (blnPinNote ? 1 : 0));     //if myBoolean == true, byte == 1
        parcel.writeString(strLogAction);

        parcel.writeString(strCategoryTypeName);
        parcel.writeString(strCategoryName);
        parcel.writeString(strDefaultSubName);

        parcel.writeString(fkIntCategoryTypeId);
        parcel.writeString(fkIntCategoryId);
        parcel.writeString(fkIntSubCategoryId);

    }

}
