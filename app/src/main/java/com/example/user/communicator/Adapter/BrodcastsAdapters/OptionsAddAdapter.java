package com.example.user.communicator.Adapter.BrodcastsAdapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.communicator.CustomViews.CustomEditText;
import com.example.user.communicator.R;

import java.util.ArrayList;


public class OptionsAddAdapter extends RecyclerView.Adapter<OptionsAddAdapter.ViewHolder> {

    public int pos;
    private OnItemClickList mItemClickList;
    private ArrayList<String> mItems;
    private ArrayList<String> mStoredAllItems = new ArrayList<>();
    private Context mContext;
    private RecyclerView recyclerView;


    public OptionsAddAdapter(Context context, ArrayList<String> Items) {
        this.mContext = context;
        this.mItems = Items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(mContext)
                .inflate(R.layout.item_option, parent, false);
        return new ViewHolder(inflatedView);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.setIsRecyclable(false);

        String name = mItems.get(position);
        final boolean[] isFirst = {false};
        if (!TextUtils.isEmpty(name)) {
            holder.mText.setText(name);
        } else {
            isFirst[0] = true;
        }
        holder.mText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (isFirst[0]) {
                    if (mItemClickList != null) {
                        String edt = holder.mText.getText().toString();
                        mStoredAllItems.set(position, edt);
                        mItemClickList.onItemClick(position);
                    }
                } else {
                    String edt = editable.toString();
                    mStoredAllItems.set(position, edt);
                }
            }
        });
        if (position == mItems.size() - 2) {
            holder.mText.requestFocus();
        }
    }

    public ArrayList<String> getData() {
        return mStoredAllItems;
    }

    public void setData(ArrayList<String> items) {
        mStoredAllItems.clear();
        mStoredAllItems.addAll(items);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public void setOnItemClickListener(final OnItemClickList mItemClickListener) {
        this.mItemClickList = mItemClickListener;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public interface OnItemClickList {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        CustomEditText mText;


        private ViewHolder(final View view) {
            super(view);
            mText = view.findViewById(R.id.text);
        }
    }

}