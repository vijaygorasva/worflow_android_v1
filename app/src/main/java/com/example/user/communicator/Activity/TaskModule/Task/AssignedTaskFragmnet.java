package com.example.user.communicator.Activity.TaskModule.Task;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.user.communicator.Activity.TaskModule.DetailsActivity.BaseFragment;
import com.example.user.communicator.Activity.TaskModule.SearchUserActivity;
import com.example.user.communicator.Adapter.TaskAdapters.FilterTypeAdapter;
import com.example.user.communicator.Adapter.TaskAdapters.TaskListAdapter;
import com.example.user.communicator.CustomViews.CustomEditText;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.CustomViews.CustomeViewPager;
import com.example.user.communicator.Dialog.AllCategoryListDialog;
import com.example.user.communicator.Dialog.FilterDialog;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.TaskModels.FilterType;
import com.example.user.communicator.Model.TaskModels.SelectionItem;
import com.example.user.communicator.Model.TaskModels.SelectionObject;
import com.example.user.communicator.Model.TaskModels.TaskItems;
import com.example.user.communicator.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

import static android.app.Activity.RESULT_OK;


public class AssignedTaskFragmnet extends BaseFragment implements IOnBackPressed {

    @BindView(R.id.rvTaskList)
    RecyclerView rvTaskList;

    @BindView(R.id.etAddTask)
    CustomEditText etAddTask;

    @BindView(R.id.tvDone)
    CustomTextView tvDone;

    @BindView(R.id.usersCount)
    CustomTextView usersCount;

    @BindView(R.id.otherCount)
    CustomTextView otherCount;

    @BindView(R.id.filterType)
    ImageView filterType;

    TaskListAdapter taskListAdapter;
    ArrayList<TaskItems> taskItems = new ArrayList<>();

    @BindView(R.id.fabtnAdd)
    FloatingActionButton fabtnAdd;

    @BindView(R.id.fabtnDelete)
    com.getbase.floatingactionbutton.FloatingActionButton fabtnDelete;

    @BindView(R.id.fabSearch11)
    FloatingActionButton fabSearch1;

    @BindView(R.id.tvTypeCategory)
    CustomTextView tvTypeCategory;

    @BindView(R.id.tvMainCategory)
    CustomTextView tvMainCategory;

    @BindView(R.id.tvSubCategory)
    CustomTextView tvSubCategory;

//    @BindView(R.id.line)
//    View mLine;

    Boolean isFab = false;
    Context context;

    SelectionObject mObject;
    Datum mUserDetails;

    ArrayList<SelectionItem> selectionItemsUsers = new ArrayList<>();
    ArrayList<SelectionItem> selectionItemsLevels = new ArrayList<>();
    ArrayList<SelectionItem> selectionItemsMembers = new ArrayList<>();
    ArrayList<Integer> taskItemsIDs = new ArrayList<>();

    ArrayList<FilterType> filterTypes = new ArrayList<>();
    @BindView(R.id.vLoading)
    LinearLayout vLoading;
    @BindView(R.id.cvTasks)
    CardView mAddView;
    boolean isSearch = false;
    Realm mRealm;
    String categoryID = "0";
    String categoryName = "";
    String categoryTypeID = "";
    String categoryTypeName = "";
    String subCategoryID = "";
    String subCategoryName = "";
    String mFilterType = "ADD_TIME";
    String BROADCAST_ACTION = "com.example.user.communicator";
    public String mSeacrhText = "";
    ViewPager viewPager;
    AllCategoryListDialog allCategoryListDialog;

    public static AssignedTaskFragmnet newInstance(CustomeViewPager viewPager) {
        AssignedTaskFragmnet fragmentFirst = new AssignedTaskFragmnet();
        Bundle args = new Bundle();
        args.putSerializable("viewPager", viewPager);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @SuppressLint("RestrictedApi")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_assigned_task, container, false);
        ButterKnife.bind(this, rootView);

        context = getActivity();
        mRealm = Realm.getDefaultInstance();
        viewPager = (ViewPager) getArguments().getSerializable("viewPager");

        mUserDetails = getLoginData();

        rvTaskList.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        rvTaskList.setLayoutManager(mLayoutManager);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        rvTaskList.setLayoutManager(layoutManager);

        taskListAdapter = new TaskListAdapter(getActivity(), taskItems, rvTaskList);
        taskListAdapter.setIsAssignedTask(false);
        taskListAdapter.setOnClickListner(onClickListner);
        rvTaskList.setAdapter(taskListAdapter);

        tvDone.setOnClickListener(view -> taskAddCall());

        tvTypeCategory.setOnClickListener(v -> {

            if (TextUtils.isEmpty(subCategoryName) && TextUtils.isEmpty(categoryName)) {
                allCategoryListDialog = AllCategoryListDialog.newIntance("TASK", false);

                if (allCategoryListDialog != null) {
                    allCategoryListDialog.show(getActivity().getSupportFragmentManager().beginTransaction(), "my-dialog");
                }
            } else {
                getSelectedCategory("", "", "", "", "", "");

            }
            tvMainCategory.setVisibility(View.GONE);
            tvSubCategory.setVisibility(View.GONE);
            mAddView.setVisibility(View.GONE);
        });

        tvMainCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvSubCategory.setVisibility(View.GONE);

                if (TextUtils.isEmpty(subCategoryName)) {
                    allCategoryListDialog = AllCategoryListDialog.newIntance("TASK", false);

                    if (allCategoryListDialog != null) {
                        allCategoryListDialog.show(getActivity().getSupportFragmentManager().beginTransaction(), "my-dialog");
                    }
                } else {
                    getSelectedCategory(categoryTypeName, categoryTypeID, categoryName, categoryID, "", "");
                }
            }
        });

        tvSubCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSelectedCategory(categoryTypeName, categoryTypeID, categoryName, categoryID, subCategoryID, subCategoryName);
            }
        });

        fabtnAdd.setVisibility(View.GONE);
        filterType.setVisibility(View.VISIBLE);
//        mLine.setVisibility(View.VISIBLE);

        fabSearch1.setOnClickListener(v -> {
            if (!isSearch) {
                isSearch = true;
                Interface.onChangeVisibility.OnChangevisibility(View.VISIBLE);
//            searchView.setVisibility(View.VISIBLE);
//            fabSearch1.setVisibility(View.GONE);
                fabtnAdd.setVisibility(View.GONE);
                mAddView.setVisibility(View.GONE);
            } else {
                isSearch = false;
                Interface.onChangeVisibility.OnChangevisibility(View.GONE);
                fabtnAdd.setVisibility(View.VISIBLE);
                setListOfTasks();
            }
        });

        usersCount.setOnClickListener(v -> {
            Intent i = new Intent(getActivity(), SearchUserActivity.class);
            startActivityForResult(i, 2020);
        });

        fabtnDelete.setOnClickListener(v -> {
            JSONArray jsonArray = new JSONArray();
            for (int s : taskItemsIDs) {
                jsonArray.put(taskItems.get(s).get_id());
            }
            removeTask(jsonArray);
        });

        filterType.setOnClickListener(v -> {
//            showFilterTypeDialog();
            FilterDialog.newIntance("ASSIGNED", categoryTypeID, categoryID, subCategoryID, new FilterDialog.Callback() {
                @Override
                public void setFilterData(ArrayList<TaskItems> taskItems) {
                    if (taskItems.size() == 0) {
                        showToast("No task found in this category");
                    } else {
                        taskListAdapter = new TaskListAdapter(getActivity(), taskItems, rvTaskList);
                        taskListAdapter.setIsAssignedTask(true);
                        rvTaskList.setAdapter(taskListAdapter);
                    }
                }

            }).show(getActivity().getSupportFragmentManager(), "FilterDialog");
        });

//        filterType.setText("Newly Added");

        FilterType filterType = new FilterType();
        filterType.setName("Newly Added");
        filterType.setKey("ADD_TIME");
        filterType.setSelected(true);
        filterTypes.add(filterType);

        FilterType filterType1 = new FilterType();
        filterType1.setName("Recently Updated");
        filterType1.setKey("UPDATE_TIME");
        filterTypes.add(filterType1);

        FilterType filterType2 = new FilterType();
        filterType2.setName("Priority Low to High");
        filterType2.setKey("PRIORITY_LOW_TO_HIGH");
        filterTypes.add(filterType2);

        FilterType filterType3 = new FilterType();
        filterType3.setName("Priority High to Low");
        filterType3.setKey("PRIORITY_HIGH_TO_LOW");
        filterTypes.add(filterType3);

        mObject = getSelectionItem();
        Interface.setOnSearchTextListner(onSearchTextListner);
        Interface.setOnPerformSearchListner(onPerformSearchListner);
        setAddView();
        setListOfTasks();
        IntentFilter filter = new IntentFilter();
        filter.addAction(BROADCAST_ACTION);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        getActivity().registerReceiver(notificationBroadcaster, filter);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        return rootView;
    }

    TaskListAdapter.OnClickListner onClickListner = new TaskListAdapter.OnClickListner() {
        @Override
        public void OnClick(int position) {
            if (taskItems.get(position).isSelected()) {
                mRealm.beginTransaction();
                taskItems.get(position).setSelected(false);
                mRealm.commitTransaction();
                boolean isF = false;
                for (int i = 0; i < taskItemsIDs.size() && !isF; i++) {
                    if (position == taskItemsIDs.get(i)) {
                        taskItemsIDs.remove(i);
                        isF = true;
                    }
                }
            } else {
                mRealm.beginTransaction();
                taskItems.get(position).setSelected(true);
                mRealm.commitTransaction();
                taskItemsIDs.add(position);
            }
            taskListAdapter.notifyItemChanged(position);
            if (taskItemsIDs.size() == 0) {
                taskListAdapter.isSelectionMode = false;
            } else {
                taskListAdapter.isSelectionMode = true;
            }
            setDeleteBtn();
        }
    };

    @SuppressLint("RestrictedApi")
    public void setDeleteBtn() {
        if (taskListAdapter.isSelectionMode) {
            fabtnDelete.setVisibility(View.VISIBLE);
            fabtnAdd.setVisibility(View.GONE);
            fabSearch1.setVisibility(View.GONE);
        } else {
            fabtnDelete.setVisibility(View.GONE);
            if (!isSearch) {
                fabtnAdd.setVisibility(View.VISIBLE);
            }

            fabSearch1.setVisibility(View.VISIBLE);
        }
    }

    Interface.OnSearchTextListner onSearchTextListner = text -> {
        if (viewPager.getCurrentItem() == 1) {
            mSeacrhText = text;
            if (mSeacrhText.length() > 3) {
                performSearch();
            }
        }
    };

    Interface.OnPerformSearchListner onPerformSearchListner = search -> {
        if (viewPager.getCurrentItem() == 1) {
            if (search) {
                performSearch();
            } else {
                setListOfTasks();
            }
        }
    };

    BroadcastReceiver notificationBroadcaster = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String intUserId = intent.getStringExtra("intUserId");
            String intTaskDocumentNo = intent.getStringExtra("intTaskDocumentNo");
            if (!isSearch) {
                getData(intTaskDocumentNo);
            }
        }
    };

    @Override
    public void OnError(JSONObject object, ApiCall type) {
        showToast("error..." + type);
        vLoading.setVisibility(View.GONE);
        try {
            showToast(object.getString("msg"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void OnResponce(JSONObject data, ApiCall type) {

        try {
            Gson gson = new GsonBuilder().create();
            JSONArray array;
            vLoading.setVisibility(View.GONE);
            switch (type) {
                case GET_MYTASK:
                case GET_ASSIGNED_TASK:
                    taskItems.clear();
                    array = data.getJSONObject("data").getJSONArray("tasksData");
                    if (array.length() != 0) {
                        mRealm.beginTransaction();
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            TaskItems items = gson.fromJson(object.toString(), TaskItems.class);
                            items.setMyTask(false);
                            mRealm.copyToRealmOrUpdate(items);
                            this.taskItems.add(items);
                        }
                        mRealm.commitTransaction();
                    }
//                    Collections.sort(taskItems, (s1, s2) -> {
//                        long rollno1 = s1.getCreatedDate();
//                        long rollno2 = s2.getCreatedDate();
//                        Date date1 = new Date(rollno1);
//                        Date date2 = new Date(rollno2);
//                        return date2.compareTo(date1);
//                    });

                    taskListAdapter = new TaskListAdapter(getActivity(), taskItems, rvTaskList);
                    taskListAdapter.setIsAssignedTask(true);
                    rvTaskList.setAdapter(taskListAdapter);

//                    taskListAdapter.setIsAssignedTask(true);
                    taskListAdapter.notifyDataSetChanged();
//                    setMyTaskItems(this.taskItems);
                    etAddTask.setText("");
                    break;

                case SEARCH_ASSIGNED_TASK:
                case SEARCH_MY_TASKS:
                    taskItems.clear();
                    array = data.getJSONObject("data").getJSONArray("tasksData");
                    if (array.length() != 0) {
                        mRealm.beginTransaction();
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            TaskItems items = gson.fromJson(object.toString(), TaskItems.class);
                            items.setMyTask(false);
                            mRealm.copyToRealmOrUpdate(items);
                            this.taskItems.add(items);
                        }
                        mRealm.commitTransaction();
                    }
//                    Collections.sort(taskItems, (s1, s2) -> {
//                        long rollno1 = s1.getCreatedDate();
//                        long rollno2 = s2.getCreatedDate();
//                        Date date1 = new Date(rollno1);
//                        Date date2 = new Date(rollno2);
//                        return date2.compareTo(date1);
//                    });
                    taskListAdapter.setIsAssignedTask(true);
                    taskListAdapter.notifyDataSetChanged();
                    break;
                case GET_TASK_DATA:
                    TaskItems items1 = gson.fromJson(data.getJSONObject("data").getJSONObject("taskData").toString(), TaskItems.class);
                    if (items1.getCreateUser().get_id().equalsIgnoreCase(mUserDetails.getIntUserId())) {
                        mRealm.beginTransaction();
                        mRealm.copyToRealmOrUpdate(items1);
                        mRealm.commitTransaction();
                        if (tvMainCategory.getText().toString().equalsIgnoreCase(items1.getStrCategoryName()) ||
                                tvMainCategory.getText().toString().equalsIgnoreCase("All")) {//tvTypeCategory
                            for (int i = 0; i < taskItems.size(); i++) {
                                if (taskItems.get(i).getIntTaskDocumentNo() == items1.getIntTaskDocumentNo()) {
                                    taskItems.remove(i);
                                }
                            }
                            taskItems.add(0, items1);
                            taskListAdapter.notifyDataSetChanged();
                        }
                    }
                    break;
                case REMOVE_TASK:
                    mRealm.beginTransaction();
                    ArrayList<String> ids = new ArrayList<>();
                    for (int position : taskItemsIDs) {
                        ids.add(taskItems.get(position).get_id());
                        mRealm.where(TaskItems.class).equalTo("_id", taskItems.get(position).get_id()).findAll().deleteAllFromRealm();
                    }
                    mRealm.commitTransaction();
                    for (int i = 0; i < ids.size(); i++) {
                        boolean isF = false;
                        for (int j = 0; j < taskItems.size() && !isF; j++) {
                            if (taskItems.get(j).get_id().equalsIgnoreCase(ids.get(i))) {
                                taskItems.remove(j);
                                isF = true;
                            }
                        }
                    }
                    taskListAdapter.isSelectionMode = false;
                    taskItemsIDs.clear();
                    setDeleteBtn();
                    taskListAdapter.notifyDataSetChanged();
                    showToast(data.getString("message"));
                    break;
                case ADD_TASK:
                    JSONObject object = data.getJSONObject("data").getJSONObject("taskData");
                    mRealm.beginTransaction();
                    TaskItems items = gson.fromJson(object.toString(), TaskItems.class);
                    items.setMyTask(false);
                    mRealm.copyToRealmOrUpdate(items);
                    this.taskItems.add(0, items);
                    mRealm.commitTransaction();
                    taskListAdapter.notifyDataSetChanged();
                    etAddTask.setText("");
//                    mObject = new SelectionObject();
//                    usersCount.setText("");
//                    usersCount.setVisibility(View.GONE);
                    break;

                case GET_USERS_DATA:
                    array = data.getJSONArray("data");
                    if (array.length() != 0) {
                        selectionItemsUsers.clear();
                        for (int i = 0; i < array.length(); i++) {
                            try {
                                JSONObject object1 = array.getJSONObject(i);
                                SelectionItem item = gson.fromJson(object1.toString(), SelectionItem.class);
                                selectionItemsUsers.add(item);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    break;

                case GET_LEVELS:
                    array = data.getJSONArray("data");
                    if (array.length() != 0) {
                        selectionItemsLevels.clear();
                        for (int i = 0; i < array.length(); i++) {
                            try {
                                JSONObject object2 = array.getJSONObject(i);
                                SelectionItem item = gson.fromJson(object2.toString(), SelectionItem.class);
                                selectionItemsLevels.add(item);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    break;

                case GET_MEMBER_TYPES:
                    array = data.getJSONArray("data");
                    if (array.length() != 0) {
                        selectionItemsMembers.clear();
                        for (int i = 0; i < array.length(); i++) {
                            try {
                                JSONObject object3 = array.getJSONObject(i);
                                SelectionItem item = gson.fromJson(object3.toString(), SelectionItem.class);
                                selectionItemsMembers.add(item);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    break;

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        vLoading.setVisibility(View.GONE);
    }


    public void getData(String id) {
        RequestParams params = new RequestParams();
        params.put("intTaskDocumentNo", id);
//        showProgressDialog("Please wait...");
        vLoading.setVisibility(View.VISIBLE);
        ApiCallWithToken(getTaskData, ApiCall.GET_TASK_DATA, params);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().unregisterReceiver(notificationBroadcaster);
    }

    @SuppressLint("RestrictedApi")
    @OnClick({R.id.fabtnAdd})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.fabtnAdd:

                if (tvTypeCategory.getText().toString().equalsIgnoreCase("All")) {
                    isFab = true;
                    allCategoryListDialog = AllCategoryListDialog.newIntance("TASK", false);

                    if (allCategoryListDialog != null) {
                        allCategoryListDialog.show(getActivity().getSupportFragmentManager().beginTransaction(), "my-dialog");
                    }
                } else {
                    Intent i = new Intent(getActivity(), SearchUserActivity.class);
                    startActivityForResult(i, 2020);
                }
                break;
        }
    }

    private void showFilterTypeDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dilogue_filter_type);

        RecyclerView recyclerView = dialog.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        filterTypes.get(0).setName("Newly Added");

        FilterTypeAdapter adapter = new FilterTypeAdapter(getActivity(), filterTypes);
        recyclerView.setAdapter(adapter);
        adapter.setOnClickListner(position -> {
            for (int i = 0; i < filterTypes.size(); i++) {
                filterTypes.get(i).setSelected(false);
            }
            filterTypes.get(position).setSelected(true);
            mFilterType = filterTypes.get(position).getKey();
//            filterType.setText(filterTypes.get(position).getName());
            adapter.notifyDataSetChanged();
            dialog.dismiss();
            if (isSearch) {
                performSearch();
            } else {
                setListOfTasks();
            }
        });
        dialog.show();
    }

    public void performSearch() {
        if (isNetworkAvailable()) {
            if (!TextUtils.isEmpty(mSeacrhText)) {
                RequestParams params = new RequestParams();
                params.put("intUserId", mUserDetails.getIntUserId());
                params.put("searchText", mSeacrhText);
                params.put("sortBy", mFilterType);
                params.put("strCategory", categoryID);
                if (!TextUtils.isEmpty(subCategoryID)) {
                    params.put("strSubCategory", subCategoryID);
                }
//        showProgressDialog("Please wait...");
                vLoading.setVisibility(View.VISIBLE);
                ApiCallWithToken(searchAssignedTask, ApiCall.SEARCH_ASSIGNED_TASK, params);
            } else {
                setListOfTasks();
            }
        } else {
            vLoading.setVisibility(View.GONE);
            showNetworkAlert();
        }
    }

    @SuppressLint("RestrictedApi")
    public void setListOfTasks() {
        taskListAdapter.isSelectionMode = false;
        taskItemsIDs.clear();
        isSearch = false;
        Interface.onChangeVisibility.OnChangevisibility(View.GONE);
//        searchView.setVisibility(View.GONE);
        fabSearch1.setVisibility(View.VISIBLE);
        RequestParams params = new RequestParams();
        taskItems.clear();
        taskListAdapter.notifyDataSetChanged();
        fabtnAdd.setVisibility(View.VISIBLE);

        if (isNetworkAvailable()) {
            params.put("intUserId", mUserDetails.getIntUserId());
            params.put("sortBy", mFilterType);
            params.put("strCategory", categoryID);
            if (!TextUtils.isEmpty(subCategoryID)) {
                params.put("strSubCategory", subCategoryID);
            }
//        showProgressDialog("Please wait...");
            vLoading.setVisibility(View.VISIBLE);
            Log.e("getAssignedTask", getAssignedTask + "....." + params + "....");
            ApiCallWithToken(getAssignedTask, ApiCall.GET_ASSIGNED_TASK, params);
        } else {
            taskItems.clear();
            RealmResults<TaskItems> itemsRealmResults = mRealm.where(TaskItems.class).equalTo("isMyTask", false).sort("createdDate", Sort.DESCENDING).findAll();
            if (itemsRealmResults != null && itemsRealmResults.size() != 0) {
                taskItems.addAll(itemsRealmResults);
                Log.e("taskItems", itemsRealmResults.size() + "....." + taskItems.size() + "....");
                for (int i = 0; i < taskItems.size(); i++) {
                    Log.e("taskItems_item", taskItems.get(i) + ">..." + "....");
                }
                taskListAdapter.notifyDataSetChanged();
            }
            vLoading.setVisibility(View.GONE);
        }

//        if (filterType.getText().toString().trim().equalsIgnoreCase("Newly Assigned") ||
//                filterType.getText().toString().trim().equalsIgnoreCase("Newly Added")) {
//            filterType.setText("Newly Added");
//        }
        setAddView();
        setDeleteBtn();
    }

    public void taskAddCall() {
        String task = etAddTask.getText().toString().trim();
        if (TextUtils.isEmpty(task)) {
            Toast.makeText(context, "Please write something about task.", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(categoryTypeID)) {
            Toast.makeText(context, "Please select a category.", Toast.LENGTH_SHORT).show();
            return;
        }

        RequestParams params = new RequestParams();
        params.put("strDescription", task);
        params.put("intCreateUserId", mUserDetails.getIntUserId());
        params.put("strCategoryName", categoryName);
        params.put("strCategory", categoryID);
        params.put("strSubCategory", subCategoryID);
        params.put("strSubCategoryName", subCategoryName);
        params.put("strCategoryType", categoryTypeID);
        params.put("strCategoryTypeName", categoryTypeName);
        if (mObject.isUser()) {
            if (mObject.getUsers() != null && mObject.getUsers().size() != 0) {
                JSONArray array = new JSONArray();
                for (SelectionItem item : mObject.getUsers()) {
                    try {
                        JSONObject object = new JSONObject();
                        object.put("id", item.getId());
                        array.put(object);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (array.length() != 0) {
                    params.put("arryOfobjstrUserCollectionData", array.toString());
                } else {
                    Toast.makeText(context, "Please select users or levels and members for task.", Toast.LENGTH_SHORT).show();
                    return;
                }
            } else {
                Toast.makeText(context, "Please select users or levels and members for task.", Toast.LENGTH_SHORT).show();
                return;
            }
        } else {
            if (mObject.getLevels() != null && mObject.getMemberTypes() != null && mObject.getLevels().size() != 0 && mObject.getMemberTypes().size() != 0) {
                JSONArray array = new JSONArray();
                for (SelectionItem item : mObject.getLevels()) {
                    try {
                        JSONObject object = new JSONObject();
                        object.put("id", item.getId());
                        array.put(object);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                JSONArray array1 = new JSONArray();
                for (SelectionItem item : mObject.getMemberTypes()) {
                    try {
                        JSONObject object = new JSONObject();
                        object.put("id", item.getId());
                        array1.put(object);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if (array.length() != 0 && array1.length() != 0) {
                    params.put("arryOfobjstrLevelCollection", array);
                    params.put("arryOfobjstrMemberCollection", array1);
                } else {
                    Toast.makeText(context, "Please select users or levels and members for task.", Toast.LENGTH_SHORT).show();
                    return;
                }
            } else {
                Toast.makeText(context, "Please select users or levels and members for task.", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        ApiCallWithToken(addTask, ApiCall.ADD_TASK, params);
    }


    public void setAddView() {
        if (mObject == null || mObject.isUser()) {
            if (mObject == null || mObject.getUsers().size() == 0) {
                mAddView.setVisibility(View.GONE);
            } else {
                usersCount.setVisibility(View.VISIBLE);
                otherCount.setVisibility(View.GONE);
                usersCount.setText(mObject.getUsers().size() + " Users");
                if (tvMainCategory.getText().toString().trim().equalsIgnoreCase("All")) {
                    mAddView.setVisibility(View.GONE);
                } else {
                    mAddView.setVisibility(View.VISIBLE);
                }

            }
        } else {
            if (mObject.getLevels() != null && mObject.getMemberTypes() != null && mObject.getLevels().size() != 0 && mObject.getMemberTypes().size() != 0) {
                usersCount.setVisibility(View.GONE);
                otherCount.setVisibility(View.VISIBLE);
                otherCount.setText(mObject.getLevels().size() + " Levels and " + mObject.getMemberTypes().size() + "  Member Types");
                if (tvMainCategory.getText().toString().trim().equalsIgnoreCase("All")) {
                    mAddView.setVisibility(View.GONE);
                } else {
                    mAddView.setVisibility(View.VISIBLE);
                }

            } else {
                mAddView.setVisibility(View.GONE);
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (viewPager.getCurrentItem() == 1) {
            if (requestCode == 5252 || requestCode == 5555) {
                taskListAdapter.onAttachmentResult(requestCode, resultCode, data);
            } else if (requestCode == 2020) {
                if (resultCode == RESULT_OK) {
                    Gson gson = new Gson();
                    String userData = data.getStringExtra("data");
                    Log.e("onBack_userData", userData + "...");
                    mObject = gson.fromJson(userData, SelectionObject.class);
                    if (mObject != null) {
                        setSelectionItem(mObject);
                        setAddView();
                    }
                }
            } else if (requestCode == 123) {
                boolean isUpdated = data.getBooleanExtra("isUpdated", false);
                boolean isCategoryChanged = data.getBooleanExtra("isCategoryChanged", false);
                int _id = data.getIntExtra("_id", 0);
                int position = data.getIntExtra("position", 0);
//            if (isUpdated) {
//                if (isMyTaskItems) {
//                    setListOfTasks("My Tasks");
//                } else {
//                    setListOfTasks("Assigned Tasks");
//                }
//            }

                if (isCategoryChanged) {
                    if (!TextUtils.isEmpty(subCategoryName)) {
                        taskItems.remove(position);
                        taskListAdapter.notifyDataSetChanged();
                    } else {
                        TaskItems tItem = mRealm.where(TaskItems.class).equalTo("intTaskDocumentNo", _id).findFirst();
                        taskItems.set(position, tItem);
                        taskListAdapter.notifyItemChanged(position);
                    }
                } else {
                    TaskItems tItem = mRealm.where(TaskItems.class).equalTo("intTaskDocumentNo", _id).findFirst();
                    taskItems.set(position, tItem);
                    taskListAdapter.notifyItemChanged(position);
                }

            }
        }
    }

    private void removeTask(JSONArray array) {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        if (array.length() == 1) {
            alertDialogBuilder.setMessage("Are you sure , you want to Delete this Task?");
        } else {
            alertDialogBuilder.setMessage("Are you sure , you want to Delete this Tasks?");
        }
        alertDialogBuilder.setPositiveButton("Ok", (dialog, which) -> {
            dialog.dismiss();
            RequestParams params = new RequestParams();
            params.put("strTaskIds", array);
            vLoading.setVisibility(View.VISIBLE);
            ApiCallWithToken(deleteTask, ApiCall.REMOVE_TASK, params);
        });
        alertDialogBuilder.setNegativeButton("Cancel", (d, w) -> {
            d.dismiss();
        });
        AlertDialog dialog = alertDialogBuilder.create();
        dialog.show();
    }


    @Override
    public boolean onBackPressed() {
        if (taskListAdapter.isSelectionMode) {
            taskListAdapter.isSelectionMode = false;
            for (int pos : taskItemsIDs) {
                taskItems.get(pos).setSelected(false);
            }
            taskItemsIDs.clear();
            setDeleteBtn();
            return true;
        } else {
            return false;
        }
    }

    @SuppressLint("RestrictedApi")
    public void getSelectedCategory(String tabName, String tabID, String mainCat, String mainCatID, String subCatID, String subCatName) {

        if (allCategoryListDialog != null) {
            allCategoryListDialog.dismiss();
        }

        if (!subCatName.equalsIgnoreCase("All")) {
            if (!isSearch) {
                fabtnAdd.setVisibility(View.VISIBLE);
            } else {
                fabtnAdd.setVisibility(View.GONE);
            }
        }

        if (!TextUtils.isEmpty(mainCat)) {
            tvMainCategory.setVisibility(View.VISIBLE);
            if (mainCat.equals("All")) {
                tvMainCategory.setText(tabName);
            } else {
                tvMainCategory.setText(mainCat);
            }
        }

        if (!TextUtils.isEmpty(subCatName)) {
            tvSubCategory.setVisibility(View.VISIBLE);
            tvSubCategory.setText(subCatName);
        }

        categoryName = mainCat;
        categoryID = mainCatID;
        categoryTypeID = tabID;
        categoryTypeName = tabName;
        subCategoryID = subCatID;
        subCategoryName = subCatName;
//        mObject = new SelectionObject();
        setListOfTasks();
        if (isFab) {
            isFab = false;
            Intent i = new Intent(getActivity(), SearchUserActivity.class);
            startActivityForResult(i, 2020);
        }

    }
}