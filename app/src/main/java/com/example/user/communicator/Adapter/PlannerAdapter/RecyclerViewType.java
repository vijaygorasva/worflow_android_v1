package com.example.user.communicator.Adapter.PlannerAdapter;

/**
 * Created by sonu on 24/07/17.
 */

public enum RecyclerViewType {
    LINEAR_VERTICAL, LINEAR_HORIZONTAL, GRID;
}
