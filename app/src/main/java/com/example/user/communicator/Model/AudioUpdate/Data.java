
package com.example.user.communicator.Model.AudioUpdate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {

    @SerializedName("intPlannerId")
    @Expose
    private Integer intPlannerId;
    @SerializedName("fileAudio")
    @Expose
    private FileAudio fileAudio;
    @SerializedName("intPlannerMainId")
    @Expose
    private String intPlannerMainId;

    public Integer getIntPlannerId() {
        return intPlannerId;
    }

    public void setIntPlannerId(Integer intPlannerId) {
        this.intPlannerId = intPlannerId;
    }

    public FileAudio getFileAudio() {
        return fileAudio;
    }

    public void setFileAudio(FileAudio fileAudio) {
        this.fileAudio = fileAudio;
    }

    public String getIntPlannerMainId() {
        return intPlannerMainId;
    }

    public void setIntPlannerMainId(String intPlannerMainId) {
        this.intPlannerMainId = intPlannerMainId;
    }

}
