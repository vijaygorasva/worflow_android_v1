package com.example.user.communicator.Activity.PlannerModule;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.user.communicator.Adapter.PlannerAdapter.ViewPagerAdapter;
import com.example.user.communicator.ConnectivityReceiver;
import com.example.user.communicator.CustomViews.CustomEditText;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Fragment.DynamicCategoryFragment;
import com.example.user.communicator.Fragment.OperationsFragment;
import com.example.user.communicator.Fragment.PlannerFragment;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.Notes.Item;
import com.example.user.communicator.Model.Notes.ObjRemainder;
import com.example.user.communicator.Model.Reminder.Reminder;
import com.example.user.communicator.R;
import com.example.user.communicator.Service.WorkerService;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

public class PlannerTabActivity extends BaseActivity implements PlannerFragment.OnSearchSelectedListener, DynamicCategoryFragment.OnCategorySelectedListener {//}, ConnectivityReceiver.ConnectivityReceiverListener {
    public static final int REQUEST_CODE_ALARM = 0;
    //This is our tablayout
    private TabLayout tabLayout;

    //This is our viewPager
    private ViewPager viewPager;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.llMain)
    RelativeLayout llMain;

    @BindView(R.id.llToolbar)
    LinearLayout llToolbar;

    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.etTitle)
    CustomEditText etTitle;

    @BindView(R.id.tvNetwrokConnetion)
    CustomTextView tvNetwrokConnetion;

    @BindView(R.id.ivClose)
    ImageView ivClose;

    Boolean isSelected = false;
    //Fragments
    PlannerFragment plannerFragment;
    OperationsFragment operationsFragment;

    public static String strCategoryTypeId,
            strCategoryId,
            strSubCategoryId;

    public static String strCategoryTypeName, strCategoryName, strSubCategoryName;

    InputMethodManager imm;
    ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_tab);
        ButterKnife.bind(this);

        imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
//        InputMethodManager imm = InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//        imm.showSoftInput(etSearch, InputMethodManager.SHOW_IMPLICIT);
        toolbar.setTitle("");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
//        viewPager.setOffscreenPageLimit(2);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                toolbar.setVisibility(View.GONE);
                viewPager.setCurrentItem(position, false);

//                tabLayout.setSelectedTabIndicatorColor(0xFFFFFFFF);
                if (position == 0) {
                    plannerFragment.getNewNoteList();
                } else {
                    operationsFragment.getAllSubCategory();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });

        if (isNetworkAvailable()) {
            getReminders();
        } else {
            Realm mRealm = Realm.getDefaultInstance();
            RealmResults<Item> itemList = mRealm.where(Item.class).findAll();//.equalTo("intPlannerId",28)

            RealmResults<Reminder> apiPlannerList = mRealm.where(Reminder.class).findAll();//.equalTo("intId",28)
            Log.e("ALL_LISt", apiPlannerList.size() + "...alarm...." + itemList.size());
        }
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        tabLayout.setupWithViewPager(viewPager);

    }


    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_search, menu);
        // Associate searchable configuration with the SearchView
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
//        etSearch.setFocusable(true);
//        etSearch.setCursorVisible(true);
//        etSearch.setClickable(true);

        toolbar.setVisibility(View.GONE);
        boolean isConnected = ConnectivityReceiver.isConnected();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isConnected", isConnected);

        plannerFragment = new PlannerFragment();
        plannerFragment.putArguments(bundle);

        operationsFragment = new OperationsFragment();
        operationsFragment.putArguments(bundle);

        adapter.addFragment(plannerFragment, "Notes");
        adapter.addFragment(operationsFragment, "Operations");
//        plannerFragment.putArguments(bundle);

        viewPager.setAdapter(adapter);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("INSERTT_Tab", resultCode + "..." + requestCode + "..." + data);
        if (requestCode == 1 && resultCode == 1) {
            plannerFragment.onActivityResult(requestCode, resultCode, data);
        } else if (requestCode == 3 && resultCode == 3) {
            plannerFragment.onActivityResult(requestCode, resultCode, data);//65539
        }
    }

    @Override
    public void onSearchSelected(Boolean isClicked) {

//        imm.showSoftInput(etTitle, InputMethodManager.SHOW_IMPLICIT);
        if (isClicked) {

            etSearch.setFocusable(true);
            etSearch.setFocusableInTouchMode(true);
            etSearch.setCursorVisible(true);
            showToast("isClicked..." + isClicked);
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            llToolbar.setVisibility(View.VISIBLE);
            toolbar.setVisibility(View.VISIBLE);
            etSearch.setVisibility(View.VISIBLE);
            etSearch.setClickable(true);

//            if (imm != null) {
            imm.showSoftInput(etSearch, InputMethodManager.SHOW_FORCED);
//            }
            etSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    String val = etSearch.getText().toString();
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    String val = etSearch.getText().toString();
                    if (!TextUtils.isEmpty(val)) {
//                        plannerFragment.filter(val);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                    String val = etSearch.getText().toString();
                    if (!TextUtils.isEmpty(val)) {
                        plannerFragment.filter(val);
                    } else plannerFragment.filter("");
                }
            });

            ivClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    llToolbar.setVisibility(View.GONE);
                    toolbar.setVisibility(View.GONE);
                    etSearch.setText("");
                    plannerFragment.filter("");
                }
            });

        } else {
            llToolbar.setVisibility(View.GONE);
            toolbar.setVisibility(View.GONE);
            etSearch.setText("");
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//            if (imm != null) {
//                imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
//            }
        }

    }


    @Override
    public void onBackPressed() {

        if (viewPager.getCurrentItem() == 0) {
            PlannerFragment.selectionList.clear();

            if (PlannerFragment.isInActionMode) {
                plannerFragment.clearActionMode();
            } else {
                super.onBackPressed();
            }
        } else {
            if (OperationsFragment.deleteList.size() > 0) {
                operationsFragment.clearActionMode();
            } else {
                super.onBackPressed();
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {

        super.onResume();
    }


    @Override
    public void onCategorySelected(Boolean isFabActive, String tabName, String tabID, String mainCatName, String mainCatID, String subCatID, String subCatName) {
//        operationsFragment.recvData(tabName,tabID,mainCatName,mainCatID,subCatID,subCatName);

        strCategoryTypeName = tabName;
        strCategoryTypeId = tabID;

        strCategoryName = mainCatName;
        strCategoryId = mainCatID;

        strSubCategoryId = subCatID;
        strSubCategoryName = subCatName;

        if (viewPager.getCurrentItem() == 0) {
            plannerFragment.getCategory(isFabActive, tabName, tabID, mainCatName, mainCatID, subCatID, subCatName);
        } else if (viewPager.getCurrentItem() == 1) {
            operationsFragment.getCategory(tabName, tabID, mainCatName, mainCatID, subCatID, subCatName);
        }

    }

    public void getReminders() {

        Datum mUserDetails = getLoginData();
        String strUserId = mUserDetails.getIntUserId();

        RequestParams params = new RequestParams();
        params.put("strUserId", String.valueOf(strUserId));
        ApiCallWithToken(reminderList, ApiCall.REMINDERLIST, params);

    }

    @Override
    public void OnResponce(JSONObject data, ApiCall type) {
        Realm mRealm=Realm.getDefaultInstance();
        JSONArray array = null;
        Gson gson = new Gson();
//        try {

        if (type == ApiCall.REMINDERLIST) {
            ArrayList<ObjRemainder> objRemainderArrayList = new ArrayList<>();
            ArrayList<Reminder> reminderArrayList = new ArrayList<>();
            Reminder reminder=new Reminder();
            try {
                array = data.getJSONArray("data");
                if (array.length() != 0) {
                    for (int i = 0; i < array.length(); i++) {

                        try {
                            JSONObject object = array.getJSONObject(i);
                            mRealm.beginTransaction();
                            Item item = gson.fromJson(object.toString(), Item.class);
                            String planer_ID = String.valueOf(item.getIntPlannerId());

                            reminder.setPlannerId(planer_ID);
                            reminder.setItem(item);
                            reminderArrayList.add(reminder);
                            mRealm.copyToRealmOrUpdate(reminder);
                            mRealm.commitTransaction();
                            ObjRemainder objDetails = gson.fromJson(object.getJSONObject("objRemainder").toString(), ObjRemainder.class);
                            if (objDetails.getStrRemainderDate() != null) {
                                Log.e("ObjRemainder", new Gson().toJson(objDetails) + "");
                                objRemainderArrayList.add(objDetails);
                            }
                        } catch (JSONException e) {

                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("reminderArrayList", reminderArrayList.size() + "");
            if (reminderArrayList.size() > 0) {

                Gson gson1 = new Gson();
//                String list = gson1.toJson(objRemainderArrayList);
                String list = gson1.toJson(reminderArrayList);
                Intent intent = new Intent(PlannerTabActivity.this, WorkerService.class);
                intent.setAction(Intent.ACTION_MAIN);
                intent.putExtra("json", list);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                PendingIntent pendingIntent = PendingIntent.getService(PlannerTabActivity.this, REQUEST_CODE_ALARM, intent, 0);

                int alarmType = AlarmManager.ELAPSED_REALTIME;

//                Calendar calendar = Calendar.getInstance();
//                calendar.setTimeInMillis(System.currentTimeMillis());
//                calendar.set(Calendar.HOUR_OF_DAY, 10);
//                calendar.set(Calendar.MINUTE, 35);
                final int FIFTEEN_SEC_MILLIS = 15000;

                AlarmManager alarmManager = (AlarmManager) PlannerTabActivity.this.getSystemService(PlannerTabActivity.this.ALARM_SERVICE);
//                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, ti,
//                            1000 * 60 * 2, pendingIntent);

                alarmManager.setRepeating(alarmType, SystemClock.elapsedRealtime() + FIFTEEN_SEC_MILLIS,
                        FIFTEEN_SEC_MILLIS, pendingIntent);

            }
//                    }
//                }
        }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
        super.OnResponce(data, type);


    }
}
