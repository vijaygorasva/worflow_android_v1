package com.example.user.communicator.Adapter.TaskAdapters;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.icu.text.DateFormat;
import android.icu.util.TimeZone;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.transition.TransitionManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.example.user.communicator.Activity.TaskModule.DetailsActivity.DetailsTabActivity;
import com.example.user.communicator.CustomViews.CustomButton;
import com.example.user.communicator.CustomViews.CustomEditText;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.TaskModels.DueDate;
import com.example.user.communicator.Model.TaskModels.Duration;
import com.example.user.communicator.Model.TaskModels.FilterType;
import com.example.user.communicator.Model.TaskModels.SelectionItems.FileData;
import com.example.user.communicator.Model.TaskModels.TaskDetailItem;
import com.example.user.communicator.Model.TaskModels.TaskItems;
import com.example.user.communicator.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import io.realm.Realm;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static android.icu.util.ULocale.getName;

public class TaskListAdapter extends RecyclerView.Adapter<TaskListAdapter.ViewHolder> {
    private Context context;
    private ArrayList<TaskItems> taskItems;
    private ProgressDialog pDialog;
    private int position = 9999;
    private boolean isAssignedTask = true;

//    private final String URL_ROOT = "http://52.66.249.75:9999/api/";
//    private final String BASE_URL = "http://52.66.249.75:9999/";

    public final String URL_ImageUpload_SERVER = "http://52.66.249.75:9999/uploads/";
    public final String URL_ROOT_SERVER = "http://52.66.249.75:9999/api/";//52.66.249.75
    public final String BASE_URL_SEVER = "http://52.66.249.75:9999/";

    public final String URL_ImageUpload_LOCAL = "http://:9999/uploads/";
    public final String URL_ROOT_local = "http://:9999/api/";//52.66.249.75
    public final String BASE_URL_local = "http://:9999/";

    public final String URL_ROOT_TEST = "http://13.232.37.104:9999/api/";//http://13.232.37.104:9999
    public final String URL_ImageUpload_ROOT_TEST = "http://13.232.37.104:9999/uploads/";
    public final String BASE_URL_TEST = "http://13.232.37.104:9999/";

    public final String URL_ROOT = URL_ROOT_TEST;
    public final String URL_ImageUpload_ROOT = URL_ImageUpload_ROOT_TEST;
    public final String BASE_URL = BASE_URL_TEST;


    private final String updateTask = URL_ROOT + "task/updatetaskdata";
    private final String uploadFile = URL_ROOT + "task/upload_file";
    private final String changeStatus = URL_ROOT + "task/change_status";
    private final String deleteTask = URL_ROOT + "task/delete";
    private final String startTask = URL_ROOT + "task/start";
    private final String endTask = URL_ROOT + "task/end";
    private final String closeTask = URL_ROOT + "task/close";
    public final String changeProgressStatus = URL_ROOT + "task/onChangeProgress";
    private View mItemView;
    private Datum mUserData;

    private int expandPos = 9999;
    private boolean isExp = false;
    private RecyclerView recyclerView;
    public final String DOCUMENTS_DIR = "documents";
    private OnClickListner onClick;
    public boolean isSelectionMode = false;

    public static final String URL_REGEX = "^((https?|ftp)://|(www|ftp)\\.)?[a-z0-9-]+(\\.[a-z0-9-]+)+([/?].*)?$";///^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/

    public TaskListAdapter(Context context, ArrayList<TaskItems> taskItems, RecyclerView recyclerView) {
        this.taskItems = taskItems;
        this.context = context;
        mUserData = getLoginData();
        this.recyclerView = recyclerView;
    }


    public void setIsAssignedTask(boolean isAssignedTask) {
        this.isAssignedTask = isAssignedTask;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_test, parent, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(taskItems.get(position), holder);
    }

    @Override
    public int getItemCount() {
        return taskItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private void showStatusDialog(String status, ViewHolder holder) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dilogue_filter_type);

        CustomTextView textView = dialog.findViewById(R.id.title);
        textView.setText("Change Status");
        RecyclerView recyclerView = dialog.findViewById(R.id.recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        ArrayList<FilterType> filterTypes = new ArrayList<>();

        if (!isAssignedTask) {

            FilterType filterType = new FilterType();
            filterType.setName("Pending");
            filterType.setKey("Pending");
            filterTypes.add(filterType);

            FilterType filterType1 = new FilterType();
            filterType1.setName("Running");
            filterType1.setKey("Running");
            filterTypes.add(filterType1);

            FilterType filterType2 = new FilterType();
            filterType2.setName("Pause");
            filterType2.setKey("Pause");
            filterTypes.add(filterType2);

            FilterType filterType5 = new FilterType();
            filterType5.setName("Completed");
            filterType5.setKey("Completed");
            filterTypes.add(filterType5);

            FilterType filterType6 = new FilterType();
            filterType6.setName("Progress");
            filterType6.setKey("Progress");
            filterTypes.add(filterType6);

        }
        if (isAssignedTask) {
            FilterType filterType3 = new FilterType();
            filterType3.setName("Due");
            filterType3.setKey("Due");
            filterTypes.add(filterType3);
        }


        if (isAssignedTask) {
            FilterType filterType4 = new FilterType();
            filterType4.setName("Closed");
            filterType4.setKey("Closed");
            filterTypes.add(filterType4);
        }

        if (!TextUtils.isEmpty(status)) {
            for (int i = 0; i < filterTypes.size(); i++) {
                if (filterTypes.get(i).getKey().equalsIgnoreCase(status)) {
                    filterTypes.get(i).setSelected(true);
                }
            }
        }

        FilterTypeAdapter adapter = new FilterTypeAdapter(context, filterTypes);
        recyclerView.setAdapter(adapter);
        adapter.setOnClickListner(position -> {
            for (int i = 0; i < filterTypes.size(); i++) {
                filterTypes.get(i).setSelected(false);
            }
            filterTypes.get(position).setSelected(true);
            String prior = filterTypes.get(position).getKey();
            adapter.notifyDataSetChanged();

            if (!TextUtils.isEmpty(prior)) {
                if (prior.equalsIgnoreCase("Closed")) {
                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    alertDialogBuilder.setMessage("Are you sure , you want to Close this Task?");
                    alertDialogBuilder.setPositiveButton("Ok", (dialog1, which) -> {
                        dialog1.dismiss();
                        dialog.dismiss();
                        RequestParams params = new RequestParams();
                        params.put("intTaskDocumentNo", taskItems.get(this.position).getIntTaskDocumentNo());
                        params.put("intUserId", mUserData.getIntUserId());
                        params.put("taskStatus", prior);
                        showProgressDialog();
                        ArrayList<String> data = new ArrayList<>();
                        data.add(prior);
                        data.add("taskStatus");
                        ApiCallWithToken(changeStatus, ApiCall.CHANGE_STATUS, params, data);
                    });
                    alertDialogBuilder.setNegativeButton("Cancel", (d, w) -> {
                        d.dismiss();
                    });
                    AlertDialog dialog1 = alertDialogBuilder.create();
                    dialog1.show();
                } else if (prior.equalsIgnoreCase("Progress")) {

                    dialog.dismiss();
                    final Dialog dialogProgress = new Dialog(context);
                    dialogProgress.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialogProgress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialogProgress.setCancelable(true);
                    dialogProgress.setCanceledOnTouchOutside(true);
                    dialogProgress.setContentView(R.layout.dialog_progress_type);

                    CustomTextView textViewProgress = dialogProgress.findViewById(R.id.title);
                    textViewProgress.setText("Progress");
                    int finalPer = 0;
                    if (!TextUtils.isEmpty(taskItems.get(this.position).getIntProgress())) {
                        finalPer = Integer.parseInt(taskItems.get(this.position).getIntProgress());
                    }
                    IndicatorSeekBar listenerSeekBar = dialogProgress.findViewById(R.id.listener);
                    listenerSeekBar.setProgress(finalPer);

                    int finalPer1 = finalPer;
                    final int finalPer2 = finalPer;
                    int taskId = taskItems.get(this.position).getIntTaskDocumentNo();
                    listenerSeekBar.setOnSeekChangeListener(new OnSeekChangeListener() {
                        @Override
                        public void onSeeking(SeekParams seekParams) {//after changed
                            holder.tvProgress.setVisibility(View.VISIBLE);
                            holder.tvProgress.setText("( " + seekParams.progress + " % )");
                            RequestParams params = new RequestParams();
                            params.put("intTaskDocumentNo", taskId);
                            params.put("intUserId", mUserData.getIntUserId());
                            params.put("intProgress", seekParams.progress);
//                            vLoading.setVisibility(View.VISIBLE);
//                            task_status = prior;
                            ArrayList<String> data = new ArrayList<>();
                            data.add(prior);
                            data.add(String.valueOf(finalPer2));
                            ApiCallWithToken(changeProgressStatus, ApiCall.CHANGE_PROGRESS_STATUS, params, data);

                        }

                        @Override
                        public void onStartTrackingTouch(IndicatorSeekBar seekBar) {//initally

                        }

                        @Override
                        public void onStopTrackingTouch(IndicatorSeekBar seekBar) {//after changed

                        }

                    });
                    dialogProgress.show();

                } else {
                    dialog.dismiss();
                    RequestParams params = new RequestParams();
                    params.put("intTaskDocumentNo", taskItems.get(this.position).getIntTaskDocumentNo());
                    params.put("intUserId", mUserData.getIntUserId());
                    params.put("taskStatus", prior);
                    showProgressDialog();
                    ArrayList<String> data = new ArrayList<>();
                    data.add(prior);
                    data.add("taskStatus");
                    ApiCallWithToken(changeStatus, ApiCall.CHANGE_STATUS, params, data);
                }
            }
        });
        dialog.show();
    }

    public void setOnClickListner(OnClickListner onClickListner) {
        this.onClick = onClickListner;
    }

    public interface OnClickListner {
        void OnClick(int position);
    }

    private void setStatusBKG(CustomTextView taskStatus, String status) {
        GradientDrawable bgShape = (GradientDrawable) taskStatus.getBackground();
        switch (status) {
            case "Pending":
                taskStatus.setTextColor(ContextCompat.getColor(context, R.color.sub_text_color));
                bgShape.setColor(ContextCompat.getColor(context, R.color.white));
                bgShape.setStroke(1, ContextCompat.getColor(context, R.color.sub_text_color));
                break;
            case "Running":
                taskStatus.setTextColor(ContextCompat.getColor(context, R.color.white));
                bgShape.setColor(ContextCompat.getColor(context, R.color.color_running));
                bgShape.setStroke(1, ContextCompat.getColor(context, R.color.transparent));
                break;
            case "Pause":
                taskStatus.setTextColor(ContextCompat.getColor(context, R.color.white));
                bgShape.setColor(ContextCompat.getColor(context, R.color.color_pending));
                bgShape.setStroke(1, ContextCompat.getColor(context, R.color.transparent));
                break;
            case "Due":
                taskStatus.setTextColor(ContextCompat.getColor(context, R.color.white));
                bgShape.setColor(ContextCompat.getColor(context, R.color.color_due));
                bgShape.setStroke(1, ContextCompat.getColor(context, R.color.transparent));
                break;
            case "Completed":
                taskStatus.setTextColor(ContextCompat.getColor(context, R.color.white));
                bgShape.setColor(ContextCompat.getColor(context, R.color.color_completed));
                bgShape.setStroke(1, ContextCompat.getColor(context, R.color.transparent));
                break;
            case "Closed":
                taskStatus.setTextColor(ContextCompat.getColor(context, R.color.white));
                bgShape.setColor(ContextCompat.getColor(context, R.color.color_closed));
                bgShape.setStroke(1, ContextCompat.getColor(context, R.color.transparent));
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    private void OnResponse(JSONObject response, ApiCall type, ArrayList<String> data) {
        try {
            Realm realm = Realm.getDefaultInstance();
            Gson gson = new GsonBuilder().create();
            Duration duration;
            TaskItems item;
            switch (type) {
                case REMOVE_TASK:
                    expandPos = -1;
                    realm.beginTransaction();
                    realm.where(TaskItems.class).equalTo("_id", taskItems.get(position).get_id()).findAll().deleteAllFromRealm();
                    realm.commitTransaction();
                    taskItems.remove(position);
                    notifyDataSetChanged();
                    showToast(response.getString("message"));
                    break;

                case START_TASK:
                    duration = gson.fromJson(response.getJSONObject("data").getJSONObject("durationData").toString(), Duration.class);
                    realm.beginTransaction();
                    TaskItems item1 = realm.where(TaskItems.class).equalTo("isRunning", 1).findFirst();
                    TaskItems item2 = realm.where(TaskItems.class).equalTo("_id",
                            taskItems.get(position).get_id()).findFirst();
                    if (item1 != null) {
                        ///----///////////////////////////                         item1.getTaskDurationsData().get(item1.getTaskDurationsData().size() - 1).setEndTime(System.currentTimeMillis());
                        item1.setIsRunning(0);
                    }
                    item2.getTaskDurationsData().add(0, duration);
                    item2.setIsRunning(1);
                    realm.commitTransaction();
                    if (item1 != null) {
                        boolean isF = false;
                        for (int i = 0; i < taskItems.size() && !isF; i++) {
                            if (item1.get_id().equalsIgnoreCase(taskItems.get(i).get_id())) {
                                isF = true;
                                ///----///////////////////////////                                  taskItems.get(i).getTaskDurationsData().get(taskItems.get(i).getTaskDurationsData().size() - 1).setEndTime(System.currentTimeMillis());
                                taskItems.get(i).setIsRunning(0);
                                notifyItemChanged(i);
                            }
                        }
                    }
                    realm.beginTransaction();
                    taskItems.get(position).getTaskDurationsData().add(0, duration);
                    taskItems.get(position).setIsRunning(1);
                    realm.commitTransaction();
                    notifyItemChanged(position);
                    break;

                case CLOSE_TASK:
                    taskItems.get(position).setIsClosed(1);
                    realm.beginTransaction();
                    item = realm.where(TaskItems.class).equalTo("_id", taskItems.get(position).get_id()).findFirst();
                    item.setIsClosed(1);
                    realm.commitTransaction();
                    notifyItemChanged(position);
                    showToast(response.getString("message"));
                    break;

                case END_TASK:
                    duration = gson.fromJson(response.getJSONObject("data").getJSONObject("durationData").toString(), Duration.class);
                    realm.beginTransaction();
                    taskItems.get(position).getTaskDurationsData().set(0, duration);
                    taskItems.get(position).setIsRunning(0);
                    item = realm.where(TaskItems.class).equalTo("_id", taskItems.get(position).get_id()).findFirst();
                    item.getTaskDurationsData().set(0, duration);
                    item.setIsRunning(0);
                    realm.commitTransaction();
                    notifyItemChanged(position);
                    break;

                case UPDATE_DURATION:
                    taskItems.get(position).setIntHour(data.get(0));
                    taskItems.get(position).setIntMinut(data.get(1));
                    realm.beginTransaction();
                    item = realm.where(TaskItems.class).equalTo("_id", taskItems.get(position).get_id()).findFirst();
                    item.setIntHour(data.get(0));
                    item.setIntMinut(data.get(1));
                    realm.commitTransaction();
                    if (taskItems.size() < 10) {
                        notifyDataSetChanged();
                    } else {
                        notifyItemChanged(position);
                    }
                    break;

                case CHANGE_STATUS:
                    String status = data.get(0);
                    int tvStatusResourceID = context.getResources().getIdentifier(data.get(1), "id", context.getPackageName());
                    CustomTextView taskStatus = mItemView.findViewById(tvStatusResourceID);
                    taskStatus.setText(status);
                    setStatusBKG(taskStatus, status);
                    realm.beginTransaction();
                    item = realm.where(TaskItems.class).equalTo("_id", taskItems.get(position).get_id()).findFirst();
                    item.setTaskStatus(status);
                    if (status.equalsIgnoreCase("Closed")) {
                        item.setIsClosed(1);
                        taskItems.get(position).setIsClosed(1);
                    }
                    taskItems.get(position).setTaskStatus(status);
                    realm.commitTransaction();
                    notifyItemChanged(position);
                    if (status.equalsIgnoreCase("Running")) {
                        if (!isNetworkAvailable()) {
                            Toast.makeText(context, context.getString(R.string.noConnection), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        startWork();
                    } else if (status.equalsIgnoreCase("Pause")) {
                        if (!isNetworkAvailable()) {
                            Toast.makeText(context, context.getString(R.string.noConnection), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        endWork();
                    }
                    break;

                case UPDATE_PRIORITY:
                    String finalPrior = data.get(0);
                    int resourceID = context.getResources().getIdentifier(data.get(1), "id", context.getPackageName());
                    int resourceID2 = context.getResources().getIdentifier(data.get(2), "id", context.getPackageName());
                    int resourceID3 = context.getResources().getIdentifier(data.get(3), "id", context.getPackageName());
                    CustomTextView tvPriorType = mItemView.findViewById(resourceID);
                    CustomTextView tvAddPrior = mItemView.findViewById(resourceID2);
                    ImageView dot = mItemView.findViewById(resourceID3);
                    dot.setVisibility(View.VISIBLE);
                    tvAddPrior.setVisibility(View.GONE);
                    tvPriorType.setVisibility(View.VISIBLE);
                    switch (finalPrior) {
                        case "1":
                            tvPriorType.setText("Low Priority");
                            tvPriorType.setTextColor(context.getResources().getColor(R.color.blue));
                            dot.setColorFilter(ContextCompat.getColor(context, R.color.blue), android.graphics.PorterDuff.Mode.MULTIPLY);
                            break;
                        case "2":
                            tvPriorType.setText("Medium Priority");
                            tvPriorType.setTextColor(context.getResources().getColor(R.color.orange));
                            dot.setColorFilter(ContextCompat.getColor(context, R.color.orange), android.graphics.PorterDuff.Mode.MULTIPLY);
                            break;
                        case "3":
                            tvPriorType.setText("High Priority");
                            tvPriorType.setTextColor(context.getResources().getColor(R.color.red));
                            dot.setColorFilter(ContextCompat.getColor(context, R.color.red), android.graphics.PorterDuff.Mode.MULTIPLY);
                            break;
                        case "4":
                            tvPriorType.setText("Done");
                            tvPriorType.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                            dot.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), android.graphics.PorterDuff.Mode.MULTIPLY);
                            int resourceID1 = context.getResources().getIdentifier(data.get(4), "id", context.getPackageName());
                            ImageView ivComplete = mItemView.findViewById(resourceID1);
                            ivComplete.setVisibility(View.GONE);
                            break;
                    }
                    realm.beginTransaction();
                    taskItems.get(position).setStrPriorityValue(finalPrior);
                    item = realm.where(TaskItems.class).equalTo("_id", taskItems.get(position).get_id()).findFirst();
                    item.setStrPriorityValue(finalPrior);
                    realm.commitTransaction();
                    break;

                case UPLOAD_FILE:
                    if (isAssignedTask) {
                        realm.beginTransaction();
                        FileData fileData = gson.fromJson(response.getJSONObject("data").getJSONObject("fileData").toString(), FileData.class);
                        taskItems.get(position).getObjFileData().add(0, fileData);
                        TaskItems items = realm.where(TaskItems.class).equalTo("_id", taskItems.get(position).get_id()).findFirst();
                        items.getObjFileData().add(0, fileData);
                        realm.commitTransaction();
                        TransitionManager.beginDelayedTransition(recyclerView);
                        notifyDataSetChanged();
                    }
                    showToast(response.getString("message"));
                    break;
                case CHANGE_PROGRESS_STATUS:
                    String val = response.getJSONObject("data").getString("intProgress");
                    if (val.length() != 0) {
                        realm.beginTransaction();
                        taskItems.get(position).setIntProgress(val);
                        realm.copyToRealmOrUpdate(taskItems.get(position));
                        realm.commitTransaction();

                    }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void OnError(JSONObject response, ApiCall type) {
        if (type == ApiCall.UPDATE_DURATION) {
            taskItems.get(position).getDatStartToDueDate().clear();
        }
        try {
            showToast(response.getString("message"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void removeTask() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage("Are you sure , you want to Delete this Task?");
        alertDialogBuilder.setPositiveButton("Ok", (dialog, which) -> {
            dialog.dismiss();
            RequestParams params = new RequestParams();
            params.put("strTaskId", taskItems.get(position).get_id());
            showProgressDialog();
            ApiCallWithToken(deleteTask, ApiCall.REMOVE_TASK, params, null);
        });
        alertDialogBuilder.setNegativeButton("Cancel", (d, w) -> {
            d.dismiss();
        });
        AlertDialog dialog = alertDialogBuilder.create();
        dialog.show();
    }

    private void closeThisTask() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage("Are you sure , you want to Close this Task?");
        alertDialogBuilder.setPositiveButton("Ok", (dialog, which) -> {
            dialog.dismiss();
            RequestParams params = new RequestParams();
            params.put("intTaskDocumentNo", taskItems.get(position).getIntTaskDocumentNo());
            showProgressDialog();
            ApiCallWithToken(closeTask, ApiCall.CLOSE_TASK, params, null);
        });
        alertDialogBuilder.setNegativeButton("Cancel", (d, w) -> {
            d.dismiss();
        });
        AlertDialog dialog = alertDialogBuilder.create();
        dialog.show();
    }

    private void startWork() {
        RequestParams params = new RequestParams();
        params.put("intTaskDocumentNo", taskItems.get(position).getIntTaskDocumentNo());
        params.put("intUserId", mUserData.getIntUserId());
//        showProgressDialog();
        ApiCallWithToken(startTask, ApiCall.START_TASK, params, null);
    }

    private void endWork() {
        RequestParams params = new RequestParams();
        String _id = "";
        boolean isF = false;
        for (int i = 0; i < taskItems.get(position).getTaskDurationsData().size() && !isF; i++) {
            ///----///////////////////////////
//            if (taskItems.get(position).getTaskDurationsData().get(i).getEndTime() == 0) {
//                _id = taskItems.get(position).getTaskDurationsData().get(i).get_id();
//                isF = true;
//            }
            ///----///////////////////////////
        }
        params.put("durationId", _id);
//        showProgressDialog();
        ApiCallWithToken(endTask, ApiCall.END_TASK, params, null);
    }

    private void showDurationDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_duration);

        final CustomEditText hour = dialog.findViewById(R.id.hour);
        final CustomEditText minutes = dialog.findViewById(R.id.minutes);

        CustomButton submit = dialog.findViewById(R.id.submit);

        submit.setOnClickListener(view -> {
            final String h = hour.getText().toString().trim();
            final String m = minutes.getText().toString().trim();
            if (TextUtils.isEmpty(h)) {
                Toast.makeText(context, "Please enter hours", Toast.LENGTH_SHORT).show();
                return;
            }
            if (TextUtils.isEmpty(m)) {
                Toast.makeText(context, "Please enter minutes", Toast.LENGTH_SHORT).show();
                return;
            } else if (Integer.parseInt(m) >= 60) {
                Toast.makeText(context, "Please select valid minutes that are under 60.", Toast.LENGTH_SHORT).show();
                return;
            }
            dialog.dismiss();

            RequestParams params = new RequestParams();
            params.put("intTaskDocumentNo", taskItems.get(position).getIntTaskDocumentNo());
            params.put("intCreateUserId", mUserData.getIntUserId());
            params.put("intHour", h);
            params.put("intMinut", m);
            showProgressDialog();
            ArrayList<String> data = new ArrayList<>();
            data.add(h);
            data.add(m);
            ApiCallWithToken(updateTask, ApiCall.UPDATE_DURATION, params, data);
        });
        dialog.show();

    }

    private void setTaskDone() {
        String prior = "4";
        RequestParams params = new RequestParams();
        params.put("intTaskDocumentNo", taskItems.get(position).getIntTaskDocumentNo());
        params.put("intCreateUserId", mUserData.getIntUserId());
        params.put("strPriorityValue", prior);
        showProgressDialog();
        ArrayList<String> data = new ArrayList<>();
        data.add(prior);
        data.add("tvPriorType");
        data.add("ivComplete");
        data.add("dot");
        data.add("ivRemove");
        ApiCallWithToken(updateTask, ApiCall.UPDATE_PRIORITY, params, data);
    }


    private void showPriorityDialog(String prior1) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dilogue_filter_type);

        CustomTextView textView = dialog.findViewById(R.id.title);
        textView.setText("Change priority");
        RecyclerView recyclerView = dialog.findViewById(R.id.recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        ArrayList<FilterType> filterTypes = new ArrayList<>();
        FilterType filterType = new FilterType();
        filterType.setName("Low");
        filterType.setKey("1");
        filterTypes.add(filterType);

        FilterType filterType1 = new FilterType();
        filterType1.setName("Medium");
        filterType1.setKey("2");
        filterTypes.add(filterType1);

        FilterType filterType2 = new FilterType();
        filterType2.setName("High");
        filterType2.setKey("3");
        filterTypes.add(filterType2);
        if (!TextUtils.isEmpty(prior1)) {
            filterTypes.get(Integer.parseInt(prior1) - 1).setSelected(true);
        }

        FilterTypeAdapter adapter = new FilterTypeAdapter(context, filterTypes);
        recyclerView.setAdapter(adapter);
        adapter.setOnClickListner(position -> {
            for (int i = 0; i < filterTypes.size(); i++) {
                filterTypes.get(i).setSelected(false);
            }
            filterTypes.get(position).setSelected(true);
            String prior = filterTypes.get(position).getKey();
            adapter.notifyDataSetChanged();
            dialog.dismiss();
            if (!TextUtils.isEmpty(prior)) {
                RequestParams params = new RequestParams();
                params.put("intTaskDocumentNo", taskItems.get(this.position).getIntTaskDocumentNo());
                params.put("intCreateUserId", mUserData.getIntUserId());
                params.put("strPriorityValue", prior);
                showProgressDialog();
                ArrayList<String> data = new ArrayList<>();
                data.add(prior);
                data.add("tvPriorType");
                data.add("tvAddPrior");
                data.add("dot");
                ApiCallWithToken(updateTask, ApiCall.UPDATE_PRIORITY, params, data);
            }
        });
        dialog.show();
    }

    public enum ApiCall {
        CLOSE_TASK, REMOVE_TASK, START_TASK, END_TASK, UPDATE_DURATION, UPDATE_PRIORITY, UPLOAD_FILE, CHANGE_STATUS, CHANGE_PROGRESS_STATUS
    }

//    private void showPriorityDialog() {
//        AlertDialog.Builder adb = new AlertDialog.Builder(context);
//        adb.setTitle("Choose priority");
//        final String[] priority;
//        priority = new String[]{"Low ", "Medium ", "High "};
//        adb.setItems(priority, (dialog, which) -> {
//            String clickedItemValue = Arrays.asList(priority).get(which);
//            String prior = "";
//            switch (clickedItemValue) {
//                case "Low ":
//                    prior = "1";
//                    break;
//                case "Medium ":
//                    prior = "2";
//                    break;
//                case "High ":
//                    prior = "3";
//                    break;
//            }
//            if (!TextUtils.isEmpty(prior)) {
//                RequestParams params = new RequestParams();
//                params.put("intTaskDocumentNo", taskItems.get(position).getIntTaskDocumentNo());
//                params.put("intCreateUserId", mUserData.getIntUserId());
//                params.put("strPriorityValue", prior);
//                showProgressDialog();
//                ArrayList<String> data = new ArrayList<>();
//                data.add(prior);
//                data.add("tvPriorType");
//                data.add("tvAddPrior");
//                data.add("dot");
//                ApiCall(updateTask, ApiCall.UPDATE_PRIORITY, params, data);
//            }
//        });
//        adb.show();
//    }

    public void onAttachmentResult(int requestCode, int resultCode, Intent data) {

        boolean isImage = false;
        boolean isFile = false;
        String mainPath = "";

        if (data != null && data.getData() != null) {
            if (requestCode == 5555) {
                Uri uri = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
                    bitmap = compressedBitmap(bitmap);
                    mainPath = storeImage(bitmap);
                    isImage = true;


                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (data != null && data.getData() != null) {
            if (requestCode == 5252 && resultCode == RESULT_OK) {
                Uri selectedPdf = data.getData();
                mainPath = getPath(context, selectedPdf);
                if (selectedPdf.getLastPathSegment().endsWith("pdf")) {
                    isFile = true;
                } else if (mainPath.endsWith("pdf")) {
                    isFile = true;
                } else if (isGoogleDriveDocument(selectedPdf)) {
                    isFile = true;
                }
            }
        }

        if (isImage || isFile) {
            RequestParams params = new RequestParams();
            params.put("intTaskDocumentNo", taskItems.get(position).getIntTaskDocumentNo());
            if (isImage) {
                params.put("fileType", "IMAGE");
            } else if (isFile) {
                params.put("fileType", "DOC");
            } else {
                showToast("Not valid data!");
                return;
            }
            if (TextUtils.isEmpty(mainPath)) {
                showToast("Not valid path!");
                return;
            }
            File file = new File(mainPath);
            if (file.exists()) {
                try {
                    params.put("file", file);
                } catch (FileNotFoundException e) {
                    showToast("Not valid path!");
                    e.printStackTrace();
                    return;
                }
            } else {
                showToast("Not valid path!");
                return;
            }

            params.put("intUserId", mUserData.getIntUserId());
            showProgressDialog();
            ApiCallWithToken(uploadFile, ApiCall.UPLOAD_FILE, params, null);
        }

    }

    public String convertTZtoMillisec(String date) {
        String givenDateString = "Tue Jul 12 08:09:28 GMT+05:30 2019";//"12 Jul 2019 08:09 AM";//dd MMM yyyy HH:mm z
        SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");//
        try {
            Date mDate = sdf.parse(givenDateString);
            long timeInMilliseconds = mDate.getTime();
            Log.e("Date in milli :: ", "" + timeInMilliseconds);
            Log.e("Date_date", getMillsToDate(timeInMilliseconds));

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public String convertDate(String type, String dat) {
        Log.e("convertDate", type + "...." + dat);
        Date inputDate = null;
        DateFormat gmtTimeFormat = null, gmtFormat = null;
        android.icu.text.SimpleDateFormat inputPattern = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            inputPattern = new android.icu.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

            try {
                inputDate = inputPattern.parse(dat);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            gmtFormat = new android.icu.text.SimpleDateFormat("MMM dd");//, h:mm a
            gmtTimeFormat = new android.icu.text.SimpleDateFormat("h:mm a");
            gmtFormat.setTimeZone(TimeZone.getTimeZone("IST"));
            gmtTimeFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        }

        return gmtFormat.format(inputDate) + " " + gmtTimeFormat.format(inputDate);
    }

    private String getMillsToDate(long mills) {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
        return df.format(new Date(mills));
    }

    private String getMillsToDate(String mills) {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
        return df.format(new Date(mills));
    }

    private String getDataColumn(Context context, Uri uri, String selection,
                                 String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }


    private boolean mayRequest() {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || context.checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }


    @TargetApi(Build.VERSION_CODES.KITKAT)
    public String getPath(final Context context, final Uri uri) {
        if (uri == null) return null;

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
//                final Uri contentUri = ContentUris.withAppendedId(
//                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
//
//                return getDataColumn(context, contentUri, null, null);

                if (id != null && id.startsWith("raw:")) {
                    return id.substring(4);
                }

                String[] contentUriPrefixesToTry = new String[]{
                        "content://downloads/public_downloads",
                        "content://downloads/my_downloads",
                        "content://downloads/all_downloads"
                };

                for (String contentUriPrefix : contentUriPrefixesToTry) {
                    Uri contentUri = ContentUris.withAppendedId(Uri.parse(contentUriPrefix), Long.valueOf(id));
                    try {
                        String path = getDataColumn(context, contentUri, null, null);
                        if (path != null) {
                            return path;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                // path could not be retrieved using ContentResolver, therefore copy file to accessible cache using streams
                String fileName = getFileName(context, uri);
                File cacheDir = getDocumentCacheDir(context);
                File file = generateFileName(fileName, cacheDir);
                String destinationPath = null;
                if (file != null) {
                    destinationPath = file.getAbsolutePath();
                    saveFileFromUri(context, uri, destinationPath);
                }

                return destinationPath;
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            } else if (isGoogleDriveDocument(uri)) {
                return getDriveFilePath(uri, context);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private static void saveFileFromUri(Context context, Uri uri, String destinationPath) {
        InputStream is = null;
        BufferedOutputStream bos = null;
        try {
            is = context.getContentResolver().openInputStream(uri);
            bos = new BufferedOutputStream(new FileOutputStream(destinationPath, false));
            byte[] buf = new byte[1024];
            is.read(buf);
            do {
                bos.write(buf);
            } while (is.read(buf) != -1);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (is != null) is.close();
                if (bos != null) bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Nullable
    public static File generateFileName(@Nullable String name, File directory) {
        if (name == null) {
            return null;
        }

        File file = new File(directory, name);

        if (file.exists()) {
            String fileName = name;
            String extension = "";
            int dotIndex = name.lastIndexOf('.');
            if (dotIndex > 0) {
                fileName = name.substring(0, dotIndex);
                extension = name.substring(dotIndex);
            }

            int index = 0;

            while (file.exists()) {
                index++;
                name = fileName + '(' + index + ')' + extension;
                file = new File(directory, name);
            }
        }

        try {
            if (!file.createNewFile()) {
                return null;
            }
        } catch (IOException e) {
            return null;
        }


        return file;
    }

    public File getDocumentCacheDir(@NonNull Context context) {
        File dir = new File(context.getCacheDir(), DOCUMENTS_DIR);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dir;
    }

    public String getPath2(final Context context, final Uri uri) {
        String absolutePath = getPath(context, uri);
        return absolutePath != null ? absolutePath : uri.toString();
    }

    public String getFileName(@NonNull Context context, Uri uri) {
        String mimeType = context.getContentResolver().getType(uri);
        String filename = null;

        if (mimeType == null) {
            String path = getPath2(context, uri);
            if (path == null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    filename = getName(uri.toString());
                }
            } else {
                File file = new File(path);
                filename = file.getName();
            }
        } else {
            Cursor returnCursor = context.getContentResolver().query(uri, null,
                    null, null, null);
            if (returnCursor != null) {
                int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                returnCursor.moveToFirst();
                filename = returnCursor.getString(nameIndex);
                returnCursor.close();
            }
        }

        return filename;
    }

    private String getDriveFilePath(Uri uri, Context context) {
        showProgressDialog();
        Cursor returnCursor = context.getContentResolver().query(uri, null, null, null, null);
        /*
         * Get the column indexes of the data in the Cursor,
         *     * move to the first row in the Cursor, get the data,
         *     * and display it.
         * */
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
        returnCursor.moveToFirst();
        String name = (returnCursor.getString(nameIndex));
        if (!name.endsWith("pdf")) {
            name = name + ".pdf";
        }
        String size = (Long.toString(returnCursor.getLong(sizeIndex)));
        File file = generateFileName(name, context.getCacheDir());
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(uri);
            FileOutputStream outputStream = new FileOutputStream(file);
            int read = 0;
            int maxBufferSize = 1024 * 1024;
            int bytesAvailable = inputStream.available();

            //int bufferSize = 1024;
            int bufferSize = Math.min(bytesAvailable, maxBufferSize);

            final byte[] buffers = new byte[bufferSize];
            while ((read = inputStream.read(buffers)) != -1) {
                outputStream.write(buffers, 0, read);
            }
            Log.e("File Size", "Size " + file.length());
            inputStream.close();
            outputStream.close();
            Log.e("File Path", "Path " + file.getPath());
            Log.e("File Size", "Size " + file.length());
        } catch (Exception e) {
            Log.e("Exception", e.getMessage());
        }
        hideProgressDialog();
        return file.getAbsolutePath();
    }

    public boolean isGoogleDriveDocument(Uri uri) {
        return "com.google.android.apps.docs.storage".equals(uri.getAuthority()) ||
                "com.google.android.apps.docs.storage.legacy".equals(uri.getAuthority());
    }

    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    private Bitmap compressedBitmap(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] BYTE;
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        BYTE = byteArrayOutputStream.toByteArray();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;
        return BitmapFactory.decodeByteArray(BYTE, 0, BYTE.length, options);
    }

    private String storeImage(Bitmap image) {

        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            return null;
        }
        try {
            String s = getSize(String.valueOf(image.getWidth()), String.valueOf(image.getHeight()));
            String size = getSize(s.substring(0, s.indexOf(",")), s.substring(s.indexOf(",") + 1));
            image = Bitmap.createScaledBitmap(image, Integer.parseInt(size.substring(0, size.indexOf(","))), Integer.parseInt(size.substring(size.indexOf(",") + 1)), true);
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.JPEG, 70, fos);
            fos.close();
            return pictureFile.getAbsolutePath();
        } catch (FileNotFoundException e) {
            return null;
        } catch (IOException e) {
            return null;
        }
    }

    private File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + context.getPackageName()
                + "/Files");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss", Locale.US).format(new Date());
        File mediaFile;
        String mImageName = "MI_" + timeStamp + ".jpeg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    private void showDatePicker(final TaskItems items, final boolean isStart, String minDate) {
        final boolean[] isApiCallInitiate = {false};
        final Calendar calendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            String dat = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year;
            SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
            Date dateObj = null;
            try {
                dateObj = curFormater.parse(dat);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat postFormater = new SimpleDateFormat("yyyy-MM-dd");// hh:mm a
            String newDateStr = postFormater.format(dateObj);

            Log.e("showDatePicker", dat + "..." + newDateStr);
            if (isStart) {

                DueDate durStart = new DueDate();
                durStart.setStartDate(String.valueOf(calendar.getTimeInMillis()));//2019-09-21
                durStart.setDueDate(newDateStr);//2019-09-21
                taskItems.get(position).getDatStartToDueDate().add(durStart);
            } else {
                isApiCallInitiate[0] = true;
                JSONArray array = new JSONArray();
//                array.put(taskItems.get(position).getDatStartToDueDate().get(0).getStartDate());
//                array.put(taskItems.get(position).getDatStartToDueDate().get(0).getDueDate());

                array.put(taskItems.get(position).getDatStartToDueDate().get(0));
                array.put(calendar.getTimeInMillis());

                RequestParams params = new RequestParams();
                params.put("intTaskDocumentNo", items.getIntTaskDocumentNo());
                params.put("intCreateUserId", mUserData.getIntUserId());
                params.put("datStartToDueDate", array.toString());
                showProgressDialog();
                try {
                    Datum userData = getLoginData();
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.setTimeout(60 * 1000);

                    if (userData != null) {
                        SharedPreferences sharedpreferences = context.getSharedPreferences("Login", MODE_PRIVATE);
                        String token = sharedpreferences.getString("Token", null);
                        client.addHeader("Authorization", "Bearer " + token);

                        params.put("intProjectId", userData.getArryselectedProjectAllItems().get(0).getId());
                        params.put("intOrganisationId", userData.getArryselectedOrganisationAllItems().get(0).getId());
                    }

                    Log.e("DATE_Start", updateTask + "......" + params);
                    client.setURLEncodingEnabled(true);
                    client.post(context, updateTask, params, new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                            hideProgressDialog();
                            try {
                                if (response.getBoolean("success")) {
//                                    taskItems.get(position).getDatStartToDueDate().add(0, taskItems.get(position).getDatStartToDueDate().get(0));
//                                    taskItems.get(position).getDatStartToDueDate().add(1, calendar.getTimeInMillis());
                                    DueDate durStart = new DueDate();
                                    durStart.setStartDate(taskItems.get(position).getDatStartToDueDate().get(0).getStartDate());
                                    durStart.setDueDate(String.valueOf(calendar.getTimeInMillis()));

                                    if (taskItems.size() < 10) {
                                        notifyDataSetChanged();
                                    } else {
                                        notifyItemChanged(position);
                                    }
                                } else {
                                    taskItems.get(position).getDatStartToDueDate().clear();
                                }
                                showToast(response.getString("message"));
                            } catch (JSONException e) {
                                taskItems.get(position).getDatStartToDueDate().clear();
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            hideProgressDialog();
                            taskItems.get(position).getDatStartToDueDate().clear();
                            showToast("Oops!!! Please try again");
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            hideProgressDialog();
                            taskItems.get(position).getDatStartToDueDate().clear();
                            showToast("Oops!!! Please try again");
                        }
                    });
                } catch (Exception e) {
                    hideProgressDialog();
                    taskItems.get(position).getDatStartToDueDate().clear();
                    e.printStackTrace();
                }
            }
            if (isStart) {
                String min;
                min = taskItems.get(position).getDatStartToDueDate().get(0).getStartDate();
                showDatePicker(items, false, min);
            }
        };
        DatePickerDialog dpd = new DatePickerDialog(context, date, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        if (!TextUtils.isEmpty(minDate)) {
            dpd.getDatePicker().setMinDate(convertDateToMilliSecound(minDate));
        }
        dpd.setOnDismissListener(dialog -> {
            if (!isApiCallInitiate[0]) {
                if (!isStart) {
                    if (taskItems.get(position).getDatStartToDueDate().size() == 2) {
                        Realm realm = Realm.getDefaultInstance();
                        realm.beginTransaction();
                        TaskItems item = realm.where(TaskItems.class).equalTo("_id", taskItems.get(position).get_id()).findFirst();
                        item.getDatStartToDueDate().addAll(taskItems.get(position).getDatStartToDueDate());
                        realm.commitTransaction();
                    } else {
                        taskItems.get(position).getDatStartToDueDate().clear();
                    }
                }
            }
        });
        dpd.show();
    }

    public long convertDateToMilliSecound(String myDate) {
//        String myDate = "10/20/2017 8:10 AM";////2019-09-21
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date date = null;
        try {
            date = sdf.parse(myDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long millis = date.getTime();
        return millis;
    }

    private long getDaysBetweenDates(String start, String end) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
        Date startDate, endDate;
        long numberOfDays = 0;
        try {
            startDate = dateFormat.parse(start);
            endDate = dateFormat.parse(end);
            numberOfDays = getUnitBetweenDates(startDate, endDate, TimeUnit.DAYS);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return numberOfDays;
    }

    public String getSize(String _width, String _height) {
        int width = Integer.parseInt(_width);
        int height = Integer.parseInt(_height);
        if (width > height) {
            if (width > 720) {
                height = (height * 720) / width;
                width = 720;
            }
            return width + "," + height;
        } else {
            if (height > 720) {
                width = (width * 720) / height;
                height = 720;
            }
            return width + "," + height;
        }
    }

    private void showToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private long getUnitBetweenDates(Date startDate, Date endDate, TimeUnit unit) {
        long timeDiff = endDate.getTime() - startDate.getTime();
        return unit.convert(timeDiff, TimeUnit.MILLISECONDS);
    }

    public Datum getLoginData() {
        SharedPreferences sharedpreferences = context.getSharedPreferences("Login", MODE_PRIVATE);
        String data = sharedpreferences.getString("UserData", null);
        if (!TextUtils.isEmpty(data)) {
            Gson gson = new Gson();
            //            return gson.fromJson(data, Login.class);
            return gson.fromJson(data, Datum.class);
        }
        return null;
    }


    public void showProgressDialog() {
        if (pDialog == null || !pDialog.isShowing()) {
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Please Wait!");
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }

    public void hideProgressDialog() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    public void ApiCallWithToken(String url, final ApiCall type, RequestParams requestParams, ArrayList<String> data) {

        try {
            Datum userData = getLoginData();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(60 * 1000);
            if (userData != null) {
                SharedPreferences sharedpreferences = context.getSharedPreferences("Login", MODE_PRIVATE);
                String token = sharedpreferences.getString("Token", null);
                client.addHeader("Authorization", "Bearer " + token);

                requestParams.put("intProjectId", userData.getArryselectedProjectAllItems().get(0).getId());
                requestParams.put("intOrganisationId", userData.getArryselectedOrganisationAllItems().get(0).getId());
            }

//            ByteArrayEntity entity = new ByteArrayEntity(requestParams.toString().getBytes("UTF-8"));
//            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            client.setURLEncodingEnabled(true);
            client.post(context, url, requestParams, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    hideProgressDialog();
                    try {
                        if (response.getBoolean("success")) {
                            OnResponse(response, type, data);
                        } else {
                            OnError(response, type);
                        }
//                        showToast(response.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    hideProgressDialog();
                    try {
                        showToast(errorResponse.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    OnError(errorResponse, type);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    hideProgressDialog();
                    showToast(responseString);
                    JSONObject object = null;
                    try {
                        object = new JSONObject(responseString);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    OnError(object, type);
                }
            });
        } catch (Exception e) {
            hideProgressDialog();
            e.printStackTrace();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

//        @BindView(R.id.expandableLayout)
//        ExpandableLayout expandableLayout;

        @BindView(R.id.mainHolder)
        LinearLayout mainHolder;

        @BindView(R.id.tvTitle)
        CustomTextView tvTitle;

        @BindView(R.id.tvDate)
        CustomTextView tvDate;

        @BindView(R.id.tvDone)
        CustomTextView tvDone;


        @BindView(R.id.etTitle)
        CustomEditText etTitle;

        @BindView(R.id.ivUnreadDot)
        ImageView ivUnreadDot;

        @BindView(R.id.tvHeaderType)
        CustomTextView tvHeaderType;

        @BindView(R.id.tvPriorType)
        CustomTextView tvPriorType;

        @BindView(R.id.tvStartDate)
        CustomTextView tvStartDate;

        @BindView(R.id.tvDash)
        CustomTextView tvDash;

        @BindView(R.id.tvEndDate)
        CustomTextView tvEndDate;

        @BindView(R.id.tvDuration)
        CustomTextView tvDuration;

        @BindView(R.id.taskStatus)
        CustomTextView taskStatus;

        @BindView(R.id.prof_img)
        ImageView prof_img;

        @BindView(R.id.name)
        CustomTextView name;

        @BindView(R.id.prof_img1)
        ImageView prof_img1;

        @BindView(R.id.name1)
        CustomTextView name1;

        @BindView(R.id.dateAndNameView)
        RelativeLayout dateAndNameView;

        @BindView(R.id.cvTasks_Two)
        CardView cvTasks_Two;

        @BindView(R.id.ivInfo)
        ImageView ivInfo;

        @BindView(R.id.ivComplete)
        ImageView ivComplete;

        @BindView(R.id.img_gallery)
        ImageView img_gallery;

        @BindView(R.id.img_duration)
        ImageView img_duration;

        @BindView(R.id.img_cal)
        ImageView img_cal;

        @BindView(R.id.img_file)
        ImageView img_file;


        @BindView(R.id.ivStart)
        ImageView ivStart;


        @BindView(R.id.ivStop)
        ImageView ivStop;

        @BindView(R.id.ivClose)
        ImageView ivClose;

        @BindView(R.id.dot)
        ImageView dot;

        @BindView(R.id.tvAddPrior)
        CustomTextView tvAddPrior;

        @BindView(R.id.tvProgress)
        CustomTextView tvProgress;

        @BindView(R.id.attachmentView)
        RecyclerView attachmentView;

        @BindView(R.id.editHolder)
        LinearLayout editHolder;

        @BindView(R.id.moreView)
        LinearLayout moreView;

        @BindView(R.id.priorityBar)
        LinearLayout priorityBar;

        @BindView(R.id.priority)
        LinearLayout priority;


        @BindView(R.id.llMain)
        LinearLayout llMain;

        AttachmentAdapter adapter;
        LinearLayoutManager linearLayoutManager;
        ArrayList<FileData> attachmentItemArrayList = new ArrayList<>();
        private int originalHeight = 0;
        private int moreHeight;
        private boolean isViewExpanded = false;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @SuppressLint("SetTextI18n")
        public void bind(final TaskItems items, final ViewHolder holder) {

            if (!TextUtils.isEmpty(items.getStrSubCategoryName())) {
                tvHeaderType.setText(items.getStrCategoryName() + " > " + items.getStrSubCategoryName());
            } else {
                tvHeaderType.setText(items.getStrCategoryName());
            }
            ivInfo.setVisibility(View.VISIBLE);
            if (!TextUtils.isEmpty(items.getIntProgress())) {
                tvProgress.setVisibility(View.VISIBLE);
                tvProgress.setText("( " + items.getIntProgress() + " % )");
            } else {
                tvProgress.setVisibility(View.GONE);
            }

            if (isAssignedTask) {

                ivStart.setVisibility(View.GONE);
                ivStop.setVisibility(View.GONE);
                editHolder.setVisibility(View.VISIBLE);

                if (items.getUsers() != null && items.getUsers().size() != 0) {
                    if (items.getUsers().get(0).getImg() != null) {
                        if (!TextUtils.isEmpty(items.getUsers().get(0).getImg().getPath())) {
                            Picasso.get().load(BASE_URL + items.getUsers().get(0).getImg().getPath()).placeholder(R.drawable.ic_error_male).error(R.drawable.ic_error_male).into(prof_img);
                            Picasso.get().load(BASE_URL + items.getUsers().get(0).getImg().getPath()).placeholder(R.drawable.ic_error_male).error(R.drawable.ic_error_male).into(prof_img1);
                        }
                    }
                    if (!TextUtils.isEmpty(items.getUsers().get(0).getStrFirstName()) || !TextUtils.isEmpty(items.getUsers().get(0).getStrLastName())) {
                        String names = "";
                        if (items.getUsers().size() > 1) {
                            names = "( + " + (items.getUsers().size() - 1) + " More)";
                        }
                        name.setText(items.getUsers().get(0).getStrFirstName() + " " + items.getUsers().get(0).getStrLastName() + names);
                        name1.setText(items.getUsers().get(0).getStrFirstName() + " " + items.getUsers().get(0).getStrLastName() + names);
                    } else {
//                        showToast(isAssignedTask+".....2");
                    }
                } else {
//                    showToast(isAssignedTask + "....4");
                }
            } else {

                editHolder.setVisibility(View.VISIBLE);

                if (!TextUtils.isEmpty(items.getIsTaskUpdated())) {
                    if (items.getIsTaskUpdated().equalsIgnoreCase("0")) {
                        ivUnreadDot.setVisibility(View.GONE);
                    } else {
                        ivUnreadDot.setVisibility(View.VISIBLE);
                    }
                }
                if (items.getCreateUser() != null) {
                    if (items.getCreateUser().getImg() != null) {
                        if (!TextUtils.isEmpty(items.getCreateUser().getImg().getPath())) {
                            Picasso.get().load(BASE_URL + items.getCreateUser().getImg().getPath()).placeholder(R.drawable.ic_error_male).error(R.drawable.ic_error_male).into(prof_img);
                            Picasso.get().load(BASE_URL + items.getCreateUser().getImg().getPath()).placeholder(R.drawable.ic_error_male).error(R.drawable.ic_error_male).into(prof_img1);
                        }
                    }
                    if (items.getCreateUser().get_id().equals(mUserData.getIntUserId())) {
                        name1.setText("Me");
                    } else {
                        if (!TextUtils.isEmpty(items.getCreateUser().getStrFirstName()) || !TextUtils.isEmpty(items.getCreateUser().getStrLastName())) {
                            String names = "";
                            if (items.getUsers().size() > 1) {
                                names = "( + " + (items.getUsers().size() - 1) + " More)";
                            }
                            name.setText(items.getCreateUser().getStrFirstName() + " " + items.getCreateUser().getStrLastName() + names);
                            name1.setText(items.getCreateUser().getStrFirstName() + " " + items.getCreateUser().getStrLastName() + names);
                        }
                    }
                }
            }
            if (!TextUtils.isEmpty(items.getCreatedDate())) {
//                tvDate.setText("\u2022  Created on " + getMillsToDate(items.getCreatedDate()));
                tvDate.setText(convertDate("One", items.getCreatedDate()));
            }
            Log.e("Date_orgC0nverted", items.get_id() + "..." + items.getCreatedDate() + "...." + convertDate("Two", items.getCreatedDate()));
            attachmentItemArrayList = new ArrayList<>(items.getObjFileData());
            linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            attachmentView.setLayoutManager(linearLayoutManager);
            adapter = new AttachmentAdapter(context, attachmentItemArrayList, false);
            attachmentView.setAdapter(adapter);

            if (!TextUtils.isEmpty(items.getTaskStatus())) {
                taskStatus.setVisibility(View.VISIBLE);
                taskStatus.setText(items.getTaskStatus());
                setStatusBKG(taskStatus, items.getTaskStatus());
            } else {
                taskStatus.setVisibility(View.GONE);
            }//getMillsToDate(items.getCreatedDate()));

//            convertTZtoMillisec(getMillsToDate(items.getCreatedDate()));
            if (items.getDatStartToDueDate().size() != 0) {
                tvDuration.setVisibility(View.VISIBLE);
                tvDash.setVisibility(View.VISIBLE);
                tvStartDate.setVisibility(View.VISIBLE);
                tvEndDate.setVisibility(View.VISIBLE);
                tvStartDate.setText(convertDate("Three", items.getDatStartToDueDate().get(0).getStartDate()));
                tvEndDate.setText(convertDate("four", items.getDatStartToDueDate().get(0).getDueDate()));
                long duration = getDaysBetweenDates(convertDate("five", items.getDatStartToDueDate().get(0).getDueDate()), convertDate("sixe", items.getDatStartToDueDate().get(0).getStartDate()));
                if (duration == 1) {
                    tvDuration.setText(" " + duration + " day");
                } else {
                    tvDuration.setText(" " + duration + " days");
                }
                img_duration.setVisibility(View.GONE);
                img_cal.setVisibility(View.GONE);
            } else if (!TextUtils.isEmpty(items.getIntHour()) && !TextUtils.isEmpty(items.getIntMinut())) {
//                if (items.getIntHour().equalsIgnoreCase("1")) {
//                    if (items.getIntMinut().equalsIgnoreCase("1")) {
//                        tvDuration.setText(" " + items.getIntHour() + " hour " + items.getIntMinut() + " minute");
//                    } else {
//                        tvDuration.setText(" " + items.getIntHour() + " hour " + items.getIntMinut() + " minutes");
//                    }
//                } else {
//                    if (items.getIntMinut().equalsIgnoreCase("1")) {
//                        tvDuration.setText(" " + items.getIntHour() + " hours " + items.getIntMinut() + " minute");
//                    } else {
//                        tvDuration.setText(" " + items.getIntHour() + " hours " + items.getIntMinut() + " minutes");
//                    }
//                }
                tvDuration.setText(" " + items.getIntHour() + ":" + items.getIntMinut() + " hours");
                tvStartDate.setVisibility(View.GONE);
                tvEndDate.setVisibility(View.GONE);
                tvDash.setVisibility(View.GONE);
                img_duration.setVisibility(View.GONE);
                img_cal.setVisibility(View.GONE);

            } else {
                tvDash.setVisibility(View.GONE);
                tvDuration.setVisibility(View.GONE);
                tvStartDate.setVisibility(View.GONE);
                tvEndDate.setVisibility(View.GONE);
                img_duration.setVisibility(View.VISIBLE);
                img_cal.setVisibility(View.VISIBLE);
            }

            String prior = items.getStrPriorityValue();

            if (!TextUtils.isEmpty(prior)) {
                tvPriorType.setVisibility(View.VISIBLE);
                tvAddPrior.setVisibility(View.GONE);
                dot.setVisibility(View.VISIBLE);
                switch (prior) {
                    case "1":
                        tvPriorType.setText("Low Priority");
                        tvPriorType.setTextColor(context.getResources().getColor(R.color.blue));
                        dot.setColorFilter(ContextCompat.getColor(context, R.color.blue), android.graphics.PorterDuff.Mode.MULTIPLY);
                        break;
                    case "2":
                        tvPriorType.setText("Medium Priority");
                        tvPriorType.setTextColor(context.getResources().getColor(R.color.orange));
                        dot.setColorFilter(ContextCompat.getColor(context, R.color.orange), android.graphics.PorterDuff.Mode.MULTIPLY);
                        break;
                    case "3":
                        tvPriorType.setText("High Priority");
                        tvPriorType.setTextColor(context.getResources().getColor(R.color.red));
                        dot.setColorFilter(ContextCompat.getColor(context, R.color.red), android.graphics.PorterDuff.Mode.MULTIPLY);
                        break;
                    case "4":
                        tvTitle.setPaintFlags(tvTitle.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        tvTitle.setTextColor(Color.GRAY);
                        tvPriorType.setText("Done");
                        tvPriorType.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                        dot.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), android.graphics.PorterDuff.Mode.MULTIPLY);
                        ivComplete.setVisibility(View.GONE);
                        break;
                }
            } else {
                tvPriorType.setVisibility(View.GONE);
                dot.setVisibility(View.GONE);
                tvAddPrior.setVisibility(View.VISIBLE);
                ivComplete.setVisibility(View.GONE);
            }


//            String val=items.getStrDescription();
//            Pattern pattern = Pattern.compile(URL_REGEX);
//            Matcher m = pattern.matcher(items.getStrDescription());//replace with string to compare
//
//            tvTitle.setMovementMethod(LinkMovementMethod.getInstance());
//            if(m.find()) {
//
//                String text = "<a href='http://www.google.com'> Google </a>";
//
//                System.out.println("String contains URL");
//                Patterns.WEB_URL.matcher(val).matches();
//                showToast("String contains URL..."+ Patterns.WEB_URL.matcher(val).matches());
////                textView.setText(Html.fromHtml(text));
////                Linkify.addLinks(tvTitle, Linkify.WEB_URLS);
//            }else {
//                showToast("String NOOO contains URL");
////                tvTitle.setText(items.getStrDescription());
//            }

//            tvTitle.setText(Html.fromHtml(val, Html.FROM_HTML_MODE_COMPACT));
            tvTitle.setText(items.getStrDescription());

            tvAddPrior.setOnClickListener(view -> {
                if (!isSelectionMode) {
                    position = holder.getAdapterPosition();
                    if (items.getIsClosed() == 0) {
                        AddPrior();
                    }
                }
            });
            priority.setOnClickListener(view -> {
                if (!isSelectionMode) {
                    position = holder.getAdapterPosition();
                    if (isAssignedTask) {
                        if (items.getIsClosed() == 0) {
                            AddPrior();
                        }
                    }
                }
            });
            taskStatus.setOnClickListener(v -> {
                if (!isSelectionMode) {
                    if (!taskItems.get(getAdapterPosition()).getTaskStatus().equalsIgnoreCase("Closed")) {
                        position = holder.getAdapterPosition();
                        String status = taskItems.get(getAdapterPosition()).getTaskStatus();
                        if (!isNetworkAvailable()) {
                            Toast.makeText(context, context.getString(R.string.noConnection), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        mItemView = itemView;
                        showStatusDialog(status, holder);
                    }
                }
            });

            ivComplete.setOnClickListener(v -> {
                if (!isSelectionMode) {
                    if (!isNetworkAvailable()) {
                        Toast.makeText(context, context.getString(R.string.noConnection), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    String prior12 = items.getStrPriorityValue();
                    if (!prior12.equalsIgnoreCase("4")) {
                        position = holder.getAdapterPosition();
                        mItemView = itemView;
                        setTaskDone();
                    }
                }
            });


            img_duration.setOnClickListener(view -> {
                if (!isSelectionMode) {
                    if (!isNetworkAvailable()) {
                        Toast.makeText(context, context.getString(R.string.noConnection), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    position = holder.getAdapterPosition();
                    showDurationDialog();
                }
            });

            ivClose.setOnClickListener(view -> {
                if (!isSelectionMode) {
                    if (!isNetworkAvailable()) {
                        Toast.makeText(context, context.getString(R.string.noConnection), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    position = holder.getAdapterPosition();
                    closeThisTask();
                }
            });
            img_gallery.setOnClickListener(view -> {
                if (!isSelectionMode) {
                    if (!isNetworkAvailable()) {
                        Toast.makeText(context, context.getString(R.string.noConnection), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (mayRequest()) {
                        position = holder.getAdapterPosition();
                        Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                        galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        ((Activity) context).startActivityForResult(galleryIntent, 5555);
                    }
                }
            });
            img_cal.setOnClickListener(view -> {
                if (!isSelectionMode) {
                    if (!isNetworkAvailable()) {
                        Toast.makeText(context, context.getString(R.string.noConnection), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    position = holder.getAdapterPosition();
                    Date todayDate = Calendar.getInstance().getTime();
                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                    String todayString = formatter.format(todayDate);

                    showDatePicker(items, true, todayString);
                }
            });
            img_file.setOnClickListener(view -> {
                if (!isSelectionMode) {
                    if (!isNetworkAvailable()) {
                        Toast.makeText(context, context.getString(R.string.noConnection), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (mayRequest()) {
                        position = holder.getAdapterPosition();
                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.setType("application/pdf");
                        ((Activity) context).startActivityForResult(intent, 5252);
                    }
                }
            });

            ivStart.setOnClickListener(v -> {
                if (!isSelectionMode) {
                    if (!isNetworkAvailable()) {
                        Toast.makeText(context, context.getString(R.string.noConnection), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    position = holder.getAdapterPosition();
                    startWork();
                }
            });
            ivStop.setOnClickListener(v -> {
                if (!isSelectionMode) {
                    if (!isNetworkAvailable()) {
                        Toast.makeText(context, context.getString(R.string.noConnection), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    position = holder.getAdapterPosition();
                    endWork();
                }
            });
            ivInfo.setOnClickListener(v -> {
                if (!isAssignedTask) {
                    if (!isNetworkAvailable()) {
                        TaskDetailItem item = Realm.getDefaultInstance().where(TaskDetailItem.class).equalTo("intTaskDocumentNo", items.getIntTaskDocumentNo()).findFirst();
                        if (item != null) {
                            if (!items.isDetailAvailable()) {
                                return;
                            }
                        } else {
                            return;
                        }
                    }
                    Intent i = new Intent(context, DetailsTabActivity.class);
                    i.putExtra("_id", items.getIntTaskDocumentNo());
                    i.putExtra("position", getAdapterPosition());
                    ((Activity) context).startActivityForResult(i, 123);
                } else if (!isSelectionMode) {
                    if (!isNetworkAvailable()) {
                        TaskDetailItem item = Realm.getDefaultInstance().where(TaskDetailItem.class).equalTo("intTaskDocumentNo", items.getIntTaskDocumentNo()).findFirst();
                        if (item != null) {
                            if (!items.isDetailAvailable()) {
                                return;
                            }
                        } else {
                            return;
                        }
                    }
                    Intent i = new Intent(context, DetailsTabActivity.class);
                    i.putExtra("_id", items.getIntTaskDocumentNo());
                    i.putExtra("position", getAdapterPosition());
                    ((Activity) context).startActivityForResult(i, 123);
                }
                notifyDataSetChanged();
            });

            final boolean isExpanded = holder.getAdapterPosition() == expandPos;

            if (isExpanded) {
                if (holder.getAdapterPosition() == taskItems.size() - 1) {
                    LinearLayoutManager manager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    int distance;
                    View first = recyclerView.getChildAt(0);
                    if (first != null) {
                        int height = first.getHeight();
                        int current = recyclerView.getChildAdapterPosition(first);
                        int p = Math.abs(holder.getAdapterPosition() - current);
                        if (p > 5) {
                            distance = (p - (p - 5)) * height;
                        } else {
                            distance = p * height;
                        }
                        manager.scrollToPositionWithOffset(holder.getAdapterPosition(), distance);
                    }
                }
            }

            if (isAssignedTask) {
                editHolder.setVisibility(View.VISIBLE);
                moreView.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
//                moreView.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
//                moreView.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
                mainHolder.setOnClickListener(v -> {

                    expandPos = isExpanded ? -1 : holder.getAdapterPosition();
                    if (isExpanded) {
                        notifyItemChanged(holder.getAdapterPosition());
                        notifyDataSetChanged();

                    } else {
                        if (isSelectionMode) {
                            if (onClick != null) {
                                onClick.OnClick(holder.getAdapterPosition());
                            }
                        }

                        TransitionManager.beginDelayedTransition(recyclerView);
                        notifyDataSetChanged();
                    }

                });
            } else {
                editHolder.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
                mainHolder.setOnClickListener(v -> {
                    if (isSelectionMode) {
                        if (onClick != null) {
                            onClick.OnClick(holder.getAdapterPosition());
                        }
                    }
                });
            }


            dateAndNameView.setVisibility(View.VISIBLE);

            mainHolder.setActivated(isExpanded);

            mainHolder.setOnLongClickListener(v -> {
                isSelectionMode = true;
                if (onClick != null) {
                    onClick.OnClick(holder.getAdapterPosition());
                }
                return true;
            });

//
            if (items.getIsClosed() == 1) {
                ivStart.setVisibility(View.GONE);
                ivStop.setVisibility(View.GONE);
                editHolder.setVisibility(View.GONE);
                ivComplete.setVisibility(View.GONE);
            }

            if (items.isSelected()) {
                if (isSelectionMode) {
                    llMain.setBackgroundColor(ContextCompat.getColor(context, R.color.selected_state));
                } else {
                    llMain.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
                }
            } else {
                llMain.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
            }

        }

        private void AddPrior() {
            String prior1 = taskItems.get(getAdapterPosition()).getStrPriorityValue();
            if (!TextUtils.isEmpty(prior1)) {
                if (prior1.equalsIgnoreCase("4")) {
                    Toast.makeText(context, "Task is already complete!", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            if (!isNetworkAvailable()) {
                Toast.makeText(context, context.getString(R.string.noConnection), Toast.LENGTH_SHORT).show();
                return;
            }
            position = getAdapterPosition();
            mItemView = itemView;
            showPriorityDialog(prior1);
        }
    }


}
