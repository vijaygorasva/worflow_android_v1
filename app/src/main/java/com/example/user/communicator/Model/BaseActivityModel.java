package com.example.user.communicator.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class BaseActivityModel extends RealmObject {

    @PrimaryKey
    String id;
    String type;
    String dateTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }


}
