package com.example.user.communicator.CustomViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.EditText;

import com.example.user.communicator.R;

public class CustomEditText extends android.support.v7.widget.AppCompatEditText {

    String fontStyle;

    public CustomEditText(Context context) {
        super(context);
        applyCustomFont(context, "");
    }

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomEditText);
        final int N = a.getIndexCount();
        for (int i = 0; i < N; ++i) {
            int attr = a.getIndex(i);
            switch (attr) {
                case R.styleable.CustomEditText_customFont:
                    fontStyle = a.getString(attr);
                    applyCustomFont(context, fontStyle);
                    break;
            }
        }
        a.recycle();
    }

    public CustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context, "");
    }

    public void applyCustomFont(Context context, String fontStyle) {
        if (!TextUtils.isEmpty(fontStyle)) {
            Typeface customFont = Typeface.createFromAsset(context.getAssets(), "fonts/" + fontStyle);
            setTypeface(customFont);
        }
    }

    public void onEnterKeyPressed(Editable s) {

    }

    public void onBackspaceKeyPressed(Editable s) {

    }
//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode== KeyEvent.KEYCODE_ENTER)
//        {
//            // Just ignore the [Enter] key
//            return true;
//        }else if (keyCode== KeyEvent.KEYCODE_DEL)
//        {
//            int cursorPosition = this.getSelectionEnd() - 1;
//
//            if (cursorPosition < 0)
//            {
////                removeAll();
//            }
//            // Just ignore the [Enter] key
//            return true;
//        }
//        return super.onKeyDown(keyCode, event);
//    }

    public void onKeyDelete() {

    }

    @Override
    public void setOnKeyListener(OnKeyListener l) {
        super.setOnKeyListener(l);
    }

}
