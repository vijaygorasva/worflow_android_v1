package com.example.user.communicator.Adapter.ChatAdapters;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


import com.example.user.communicator.Activity.ChatModule.WebActivity;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.ChatModels.Messages;
import com.example.user.communicator.R;
import com.example.user.communicator.Utility.Constant;
import com.github.barteksc.pdfviewer.PDFView;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class MessageListAdapter extends RecyclerView.Adapter {

    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;

    private Context mContext;
    private ArrayList<Messages> mMessageList;
    private String mUserId;
    private OnBottomReachedListener onBottomReachedListener;
    private OnImageClickListener onImageClickListener;
    private String receviedName;
    private MediaPlayer mPlayer;
    private boolean isPlaying = false;

    public MessageListAdapter(Context context, ArrayList<Messages> messageList, String _userId, String _receviedName) {
        mContext = context;
        mMessageList = messageList;
        mUserId = _userId;
        receviedName = _receviedName;
    }

    @Override
    public int getItemCount() {
        return mMessageList.size();
    }


    @Override
    public int getItemViewType(int position) {
        Messages message = mMessageList.get(position);

        String id = message.getSenderId();
        if (mUserId.equals(id)) {
            return VIEW_TYPE_MESSAGE_SENT;
        } else {
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_sent, parent, false);
            return new SentMessageHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_received, parent, false);
            return new ReceivedMessageHolder(view);
        }

        return null;
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Messages message = mMessageList.get(position);
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                ((SentMessageHolder) holder).bind(message, position);
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((ReceivedMessageHolder) holder).bind(message, position);
        }
    }

    private class SentMessageHolder extends RecyclerView.ViewHolder {
        LinearLayout mTT1, mTT2, mTT3, mTT4;
        ImageView mImg, mPlay;
        CustomTextView messageText, timeText, mDateView;
        RelativeLayout mTimeView;

        SentMessageHolder(View itemView) {
            super(itemView);

            messageText = itemView.findViewById(R.id.text_message_body);
            timeText = itemView.findViewById(R.id.text_message_time);
            mTimeView = itemView.findViewById(R.id.timeView);
            mDateView = itemView.findViewById(R.id.dateView);
            mPlay = itemView.findViewById(R.id.play);
            mImg = itemView.findViewById(R.id.img);
            mTT1 = itemView.findViewById(R.id.tt1);
            mTT2 = itemView.findViewById(R.id.tt2);
            mTT3 = itemView.findViewById(R.id.tt3);
            mTT4 = itemView.findViewById(R.id.tt4);
        }

        void bind(final Messages message, final int position) {
            if (!message.isHaveMedia()) {
                if (!TextUtils.isEmpty(message.getMessage())) {
                    mTT1.setVisibility(View.VISIBLE);
                    mTT2.setVisibility(View.GONE);
                    mTT3.setVisibility(View.GONE);
                    mTT4.setVisibility(View.GONE);
                    messageText.setText(message.getMessage());
                }
            } else {
                if (!TextUtils.isEmpty(message.getMediaType())) {
                    if (message.getMediaType().equalsIgnoreCase("IMAGE")) {
                        if (!TextUtils.isEmpty(message.getMediaPath())) {
                            mTT1.setVisibility(View.GONE);
                            mTT2.setVisibility(View.VISIBLE);
                            mTT3.setVisibility(View.GONE);
                            mTT4.setVisibility(View.GONE);
                            Picasso.get().load(Constant.BASE_URL_IMAGES + message.getMediaPath()).placeholder(R.drawable.ic_error_male).error(R.drawable.ic_error_male).into(mImg);
                        }
                    } else if (message.getMediaType().equalsIgnoreCase("AUDIO")) {
                        if (!TextUtils.isEmpty(message.getMediaPath())) {
                            mTT1.setVisibility(View.GONE);
                            mTT2.setVisibility(View.GONE);
                            mTT3.setVisibility(View.VISIBLE);
                            mTT4.setVisibility(View.GONE);
                        }
                    } else if (message.getMediaType().equalsIgnoreCase("FILE")) {
                        if (!TextUtils.isEmpty(message.getMediaPath())) {
                            mTT1.setVisibility(View.GONE);
                            mTT2.setVisibility(View.GONE);
                            mTT3.setVisibility(View.GONE);
                            mTT4.setVisibility(View.VISIBLE);
                            mTT4.setOnClickListener(v -> {
                                Intent i = new Intent(mContext, WebActivity.class);
                                i.putExtra("url", Constant.BASE_URL_IMAGES + message.getMediaPath());
                                mContext.startActivity(i);
                            });
                        }
                    }
                }
            }

            mTT3.setOnClickListener(v -> {
                if (!TextUtils.isEmpty(message.getMediaPath())) {
                    playAudio(Constant.BASE_URL_IMAGES + message.getMediaPath(), mPlay);
                }
            });

            mImg.setOnClickListener(view -> {
                if (onImageClickListener != null) {
                    onImageClickListener.onImageClick(position, message.getMediaPath());
                }
            });

            timeText.setText(LongToTimeWithAmPm(message.getSendTime()));
            String time = getDate();
            int dateno = Integer.parseInt(time.substring(0, 2));
            int dateno1 = Integer.parseInt(message.getDate().substring(0, 2));
            String month1 = time.substring(2, 6).trim();
            String month2 = message.getDate().substring(2, 6).trim();
            if (dateno1 == dateno) {
                if (month1.equalsIgnoreCase(month2)) {
                    mDateView.setText("Today");
                } else {
                    mDateView.setText(message.getDate());
                }
            } else if (dateno - 1 == dateno1) {
                if (month1.equalsIgnoreCase(month2)) {
                    mDateView.setText("Yesterday");
                } else {
                    mDateView.setText(message.getDate());
                }
            } else {
                mDateView.setText(message.getDate());
            }

            if (position > 0) {
                if (mMessageList.get(position).getDate().equalsIgnoreCase(mMessageList.get(position - 1).getDate())) {
                    mTimeView.setVisibility(View.GONE);
                } else {
                    mTimeView.setVisibility(View.VISIBLE);
                }
            } else {
                mTimeView.setVisibility(View.VISIBLE);
            }
        }
    }

    private class ReceivedMessageHolder extends RecyclerView.ViewHolder {
        LinearLayout mTT1, mTT2, mTT3, mTT4;
        ImageView mImg, mPlay;
        CustomTextView messageText, timeText, mDateView;
        RelativeLayout mTimeView;
        ImageView profileImage;

        ReceivedMessageHolder(View itemView) {
            super(itemView);

            messageText = itemView.findViewById(R.id.text_message_body);
            timeText = itemView.findViewById(R.id.text_message_time);
            mTimeView = itemView.findViewById(R.id.timeView);
            mDateView = itemView.findViewById(R.id.dateView);
            mPlay = itemView.findViewById(R.id.play);
            mImg = itemView.findViewById(R.id.img);
            mTT1 = itemView.findViewById(R.id.message);
            mTT2 = itemView.findViewById(R.id.tt2);
            mTT3 = itemView.findViewById(R.id.tt3);
            mTT4 = itemView.findViewById(R.id.tt4);
        }

        void bind(final Messages message, final int position) {
            if (!message.isHaveMedia()) {
                if (!TextUtils.isEmpty(message.getMessage())) {
                    mTT1.setVisibility(View.VISIBLE);
                    mTT2.setVisibility(View.GONE);
                    mTT3.setVisibility(View.GONE);
                    mTT4.setVisibility(View.GONE);
                    messageText.setText(message.getMessage());
                }
            } else {
                if (!TextUtils.isEmpty(message.getMediaType())) {
                    if (message.getMediaType().equalsIgnoreCase("IMAGE")) {
                        if (!TextUtils.isEmpty(message.getMediaPath())) {
                            mTT1.setVisibility(View.GONE);
                            mTT2.setVisibility(View.VISIBLE);
                            mTT3.setVisibility(View.GONE);
                            mTT4.setVisibility(View.GONE);
                            Picasso.get().load(Constant.BASE_URL_IMAGES + message.getMediaPath()).placeholder(R.drawable.ic_error_male).error(R.drawable.ic_error_male).into(mImg);
                        }
                    } else if (message.getMediaType().equalsIgnoreCase("AUDIO")) {
                        if (!TextUtils.isEmpty(message.getMediaPath())) {
                            mTT1.setVisibility(View.GONE);
                            mTT2.setVisibility(View.GONE);
                            mTT3.setVisibility(View.VISIBLE);
                            mTT4.setVisibility(View.GONE);
                        }
                    } else if (message.getMediaType().equalsIgnoreCase("FILE")) {
                        if (!TextUtils.isEmpty(message.getMediaPath())) {
                            mTT1.setVisibility(View.GONE);
                            mTT2.setVisibility(View.GONE);
                            mTT3.setVisibility(View.GONE);
                            mTT4.setVisibility(View.VISIBLE);
                            mTT4.setOnClickListener(v -> {
                                Intent i = new Intent(mContext, WebActivity.class);
                                i.putExtra("url", Constant.BASE_URL_IMAGES + message.getMediaPath());
                                mContext.startActivity(i);
                            });
                        }
                    }
                }
            }
            mTT3.setOnClickListener(v -> {
                if (!TextUtils.isEmpty(message.getMediaPath())) {
                    playAudio(Constant.BASE_URL_IMAGES + message.getMediaPath(), mPlay);
                }
            });
            mImg.setOnClickListener(view -> {
                if (onImageClickListener != null) {
                    onImageClickListener.onImageClick(position, message.getMediaPath());
                }
            });


            timeText.setText(LongToTimeWithAmPm(message.getSendTime()));
            String time = getDate();
            int dateno = Integer.parseInt(time.substring(0, 2));
            int dateno1 = Integer.parseInt(message.getDate().substring(0, 2));
            String month1 = time.substring(2, 6).trim();
            String month2 = message.getDate().substring(2, 6).trim();
            if (dateno1 == dateno) {
                if (month1.equalsIgnoreCase(month2)) {
                    mDateView.setText("Today");
                } else {
                    mDateView.setText(message.getDate());
                }
            } else if (dateno - 1 == dateno1) {
                if (month1.equalsIgnoreCase(month2)) {
                    mDateView.setText("Yesterday");
                } else {
                    mDateView.setText(message.getDate());
                }
            } else {
                mDateView.setText(message.getDate());
            }

            if (position > 0) {
                if (mMessageList.get(position).getDate().equalsIgnoreCase(mMessageList.get(position - 1).getDate())) {
                    mTimeView.setVisibility(View.GONE);
                } else {
                    mTimeView.setVisibility(View.VISIBLE);
                }
            } else {
                mTimeView.setVisibility(View.VISIBLE);
            }

        }
    }

    private void playAudio(String fileName, ImageView mPlay) {
        if (!isPlaying) {
            if (mPlayer == null) {
                try {
                    mPlayer = new MediaPlayer();
                    mPlayer.setDataSource(fileName);
                    mPlayer.prepare();
                    mPlayer.start();
                    mPlay.setImageResource(R.drawable.ic_pause);
                    isPlaying = true;
                    mPlayer.setOnCompletionListener(mp -> stopAudioPlaying(mPlay));
                    mPlayer.setOnErrorListener((mp, what, extra) -> {
                        stopAudioPlaying(mPlay);
                        return false;
                    });
                } catch (IOException e) {
                    Log.d("Media:", "prepare() failed");
                }
            } else {
                if (!mPlayer.isPlaying()) {
                    mPlayer.start();
                    mPlay.setImageResource(R.drawable.ic_pause);
                    isPlaying = true;
                }
            }
        } else {
            pauseAudioPlaying(mPlay);
        }
    }

    private void stopAudioPlaying(ImageView mPlay) {
        if (mPlayer != null) {
            mPlay.setImageResource(R.drawable.ic_play);
            mPlayer.release();
            mPlayer = null;
            isPlaying = false;
        }
    }

    private void pauseAudioPlaying(ImageView mPlay) {
        if (isPlaying) {
            if (mPlayer != null && mPlayer.isPlaying()) {
                mPlay.setImageResource(R.drawable.ic_play);
                mPlayer.pause();
                isPlaying = false;
            }
        }
    }

    public String getDate() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat date = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        return date.format(calendar.getTime());
    }

    private String LongToTimeWithAmPm(Long Data) {
        Date time = new Date(Data);

        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a", Locale.US);
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        dateFormat.setTimeZone(tz);
        return dateFormat.format(time);
    }

    public String LongToDate(Long Data) {
        Date time = new Date(Data);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        dateFormat.setTimeZone(tz);
        return dateFormat.format(time);
    }


    public interface OnImageClickListener {
        void onImageClick(int position, String path);
    }

    public void setOnImageClickListener(OnImageClickListener onImageClickListener) {
        this.onImageClickListener = onImageClickListener;
    }

    public interface OnBottomReachedListener {
        void onBottomReached(int position);
    }

    public void setOnBottomReachedListener(OnBottomReachedListener onBottomReachedListener) {
        this.onBottomReachedListener = onBottomReachedListener;
    }
}