package com.example.user.communicator.Activity.TaskModule;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.CustomViews.CustomTimePickerDialog;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.Login.Login;
import com.example.user.communicator.Model.TaskModels.SelectionObject;
import com.example.user.communicator.Model.TaskModels.TaskItems;
import com.example.user.communicator.R;
import com.example.user.communicator.Utility.Constant;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import cz.msebera.android.httpclient.Header;

public class BaseActivity extends AppCompatActivity {

    //    public final String URL_ROOT = "http://worflow.com:9999/api/";
//    public final String URL_ROOT = "http://52.66.249.75:9999/api/";
//    public final String BASE_URL = "http://52.66.249.75:9999/";

    public final String URL_ImageUpload_SERVER = "http://52.66.249.75:9999/uploads/";
    public final String URL_ROOT_SERVER = "http://52.66.249.75:9999/api/";//52.66.249.75
    public final String BASE_URL_SEVER = "http://52.66.249.75:9999/";

    public final String URL_ImageUpload_LOCAL = "http://52.66.249.75:9999/uploads/";
    public final String URL_ROOT_local = "http://52.66.249.75:9999/api/";//52.66.249.75
    public final String BASE_URL_local = "http://52.66.249.75:9999/";

    public final String URL_ROOT_TEST = "http://13.232.37.104:9999/api/";//http://13.232.37.104:9999
    public final String URL_ImageUpload_ROOT_TEST = "http://13.232.37.104:9999/uploads/";
    public final String BASE_URL_TEST = "http://13.232.37.104:9999/";

    public final String URL_ROOT = URL_ROOT_TEST;
    public final String URL_ImageUpload_ROOT = URL_ImageUpload_ROOT_TEST;
    public final String BASE_URL = BASE_URL_TEST;

    public final String getUsers = URL_ROOT + "connection/getconnectionuser";
    public final String getLevels = URL_ROOT + "connection/getconnectionlevel";
    public final String getMembers = URL_ROOT + "connection/getconnectionmembers";
    public final String addTask = URL_ROOT + "task/savetaskdata";
    public final String deleteTask = URL_ROOT + "task/delete";
    public final String searchMyTask = URL_ROOT + "task/my_task_search";
    public final String searchAssignedTask = URL_ROOT + "task/assigned_task_search";
    public final String getAssignedTask = URL_ROOT + "task/getAssignedTasksData";
    public final String getMyTask = URL_ROOT + "task/getMyTasksData";
    public final String getTaskData = URL_ROOT + "task/getTaskData";
    public final String startTask = URL_ROOT + "task/start";
    public final String addComment = URL_ROOT + "task/add_comment";
    public final String endTask = URL_ROOT + "task/end";
    public final String getBrodcastsList = URL_ROOT + "broadcast/list";
    public final String getBroadcastsInboxList = URL_ROOT + "broadcast/inbox";
    public final String saveBrodcast = URL_ROOT + "broadcast/save";
    public final String getReplies = URL_ROOT + "broadcast/reply/list";
    public final String getBroadcastsDetails = URL_ROOT + "broadcast/details";
    public final String addSubTaskData = URL_ROOT + "task/add_subtask";
    public final String removeSubTaskData = URL_ROOT + "task/remove_subtask";
    public final String update_task = URL_ROOT + "task/update_task";
    public final String getNotification = URL_ROOT + "notifications";

    public final String urldeleteNoteImg = URL_ROOT + "planner/onClickDeleteImageData";

    public final String logout = URL_ROOT + "log/logout";
    public final String ANDROID = "ANDROID";

    private String TAG = "BaseActivity";
    public ProgressDialog pDialog;
    public final int IMAGE_PICKER_REQUEST_CODE = 10001;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (TextUtils.isEmpty(Constant.DEVICE_TOKEN)) {
            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> Constant.DEVICE_TOKEN = instanceIdResult.getToken());
        }
    }

    public boolean isValidEmail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public boolean isValidPhone(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    public String getCurruntDate() {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        return df.format(Calendar.getInstance().getTime());

    }

    public String getMillsToDate(String mills) {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        return df.format(new Date(mills));
    }

    public String getMillsToDateLong(long mills) {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        return df.format(new Date(mills));
    }

    public long stringToMills(String mDate) {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        Date date = null;
        try {
            date = df.parse(mDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long millis = 0;
        if (date != null) {
            millis = date.getTime();
        }
        return millis;
    }

    public String getCurruntTime() {
        SimpleDateFormat df = new SimpleDateFormat("hh:mm a", Locale.US);
        return df.format(Calendar.getInstance().getTime());
    }

    public String millsToDate(long mills) {
        Date date = new Date(mills);
        SimpleDateFormat df = new SimpleDateFormat("hh:mm a", Locale.US);
        return df.format(date);
    }

    public long getDaysBetweenDates(String start, String end) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
        Date startDate, endDate;
        long numberOfDays = 0;
        try {
            startDate = dateFormat.parse(start);
            endDate = dateFormat.parse(end);
            numberOfDays = getUnitBetweenDates(startDate, endDate, TimeUnit.DAYS);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return numberOfDays;
    }

    private long getUnitBetweenDates(Date startDate, Date endDate, TimeUnit unit) {
        long timeDiff = endDate.getTime() - startDate.getTime();
        return unit.convert(timeDiff, TimeUnit.MILLISECONDS);
    }

    public SharedPreferences getSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(this);
    }

    public ArrayList<TaskItems> getMyTaskItems() {
        String appInfo = getSharedPreferences().getString("MyTaskItems", "");
        Gson gson = new Gson();
        if (!TextUtils.isEmpty(appInfo)) {
            Type type = new TypeToken<ArrayList<TaskItems>>() {
            }.getType();
            return gson.fromJson(appInfo, type);
        }
        return new ArrayList<>();
    }

    public void setMyTaskItems(ArrayList<TaskItems> appInfo) {
        SharedPreferences.Editor editor1 = getSharedPreferences().edit();
        Gson gson = new Gson();
        String data = gson.toJson(appInfo);
        editor1.putString("MyTaskItems", data);
        editor1.apply();
    }

    public ArrayList<TaskItems> getAssignedTaskItems() {
        String appInfo = getSharedPreferences().getString("AssignedTaskItems", "");
        Gson gson = new Gson();
        if (!TextUtils.isEmpty(appInfo)) {
            Type type = new TypeToken<ArrayList<TaskItems>>() {
            }.getType();
            return gson.fromJson(appInfo, type);
        }
        return new ArrayList<>();
    }

    public void setAssignedTaskItems(ArrayList<TaskItems> appInfo) {
        SharedPreferences.Editor editor1 = getSharedPreferences().edit();
        Gson gson = new Gson();
        String data = gson.toJson(appInfo);
        editor1.putString("AssigneTaskItems", data);
        editor1.apply();
    }

    public SelectionObject getSelectionItem() {
        String appInfo = getSharedPreferences().getString("SelectionObject", "");
        Gson gson = new Gson();
        if (!TextUtils.isEmpty(appInfo)) {
            return gson.fromJson(appInfo, SelectionObject.class);
        }
        return new SelectionObject();
    }

    public void setSelectionItem(SelectionObject object) {
        SharedPreferences.Editor editor1 = getSharedPreferences().edit();
        Gson gson = new Gson();
        editor1.putString("SelectionObject", gson.toJson(object));
        editor1.apply();
    }

    public SelectionObject getSelectionEditItem() {
        String appInfo = getSharedPreferences().getString("SelectionEditObject", "");
        Gson gson = new Gson();
        if (!TextUtils.isEmpty(appInfo)) {
            return gson.fromJson(appInfo, SelectionObject.class);
        }
        return null;
    }

    public void setSelectionEditItem(SelectionObject object) {
        SharedPreferences.Editor editor1 = getSharedPreferences().edit();
        Gson gson = new Gson();
        if (object != null) {
            editor1.putString("SelectionEditObject", gson.toJson(object));
        } else {
            editor1.putString("SelectionEditObject", "");
        }
        editor1.apply();
    }


    public void setLoginData(Login response) {
        Gson gson = new Gson();
        String Data = gson.toJson(response);

        SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences("Login", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("UserData", Data);
        editor.apply();
    }

    public void clearLoginData() {
        SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences("Login", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("UserData", "");
        editor.apply();
    }


    public Datum getLoginData() {
        SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences("Login", MODE_PRIVATE);
        String data = sharedpreferences.getString("UserData", null);
        if (!TextUtils.isEmpty(data)) {
            Gson gson = new Gson();
//            return gson.fromJson(data, Login.class);
            return gson.fromJson(data, Datum.class);
        }
        return null;
    }


    public void showProgressDialog(Context ct, String message) {
        try {
            if (pDialog == null || !pDialog.isShowing()) {
                pDialog = new ProgressDialog(ct);
                pDialog.setMessage(message);
                pDialog.setCancelable(false);
                pDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideProgressDialog(Context ct) {
        try {
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.dismiss();
                pDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showAlert(String title, String message, Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_error_message);
        TextView dialogHead = dialog.findViewById(R.id.dialogHead);
        TextView dialogBody = dialog.findViewById(R.id.dialogBody);
        TextView btOk = dialog.findViewById(R.id.btOk);
        dialogHead.setText(title);
        dialogBody.setText(message);
        btOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }


    //------------------------------  For Image Pick -----------------------------------------------//

    public Uri generateImageUri(Context context) {
        final File root = context.getCacheDir();
        if (!root.exists() || !root.isDirectory()) {
            root.mkdirs();
        }
        final String name = Calendar.getInstance().getTimeInMillis() + ".jpg";
        final File sdImageMainDirectory = new File(root, name);
        return Uri.fromFile(sdImageMainDirectory);
    }

    public Intent createImageIntent(Context context) {
        // Camera.
        final List<Intent> cameraIntents = new ArrayList<>();
        final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = context.getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            cameraIntents.add(intent);
        }

        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        photoPickerIntent.setType("image/*");

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(photoPickerIntent, "Profile photo");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));

        return chooserIntent;
    }

    public void openImageIntent(Activity activity) {
        Intent chooserIntent = createImageIntent(activity);
        activity.startActivityForResult(chooserIntent, IMAGE_PICKER_REQUEST_CODE);
    }

    public void openImageIntent(Context context, Fragment fragment) {
        Intent chooserIntent = createImageIntent(context);
        fragment.startActivityForResult(chooserIntent, IMAGE_PICKER_REQUEST_CODE);
    }

    public void openImageIntent(Context context, android.app.Fragment fragment) {
        Intent chooserIntent = createImageIntent(context);
        fragment.startActivityForResult(chooserIntent, IMAGE_PICKER_REQUEST_CODE);
    }

    public boolean saveBitmap(Bitmap bmp, Uri uri) {
        File file = new File(uri.getPath());
        if (file.exists()) {
            file.delete();
        }

        FileOutputStream out = null;
        boolean result = false;
        try {
            out = new FileOutputStream(uri.getPath());
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public Uri getImageUri(Activity activity, Intent data) {
        if (data != null) {
            String action = data.getAction();
            Uri uri = data.getData();

            if (uri != null) {
                return uri;
            } else if ("inline-data".equals(action)) {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                if (bitmap != null) {
                    Uri outputFileUri = generateImageUri(activity);
                    if (saveBitmap(bitmap, outputFileUri)) {
                        return outputFileUri;
                    } else {
                        File file = new File(outputFileUri.getPath());
                        if (file.exists()) {
                            file.delete();
                        }
                    }
                }
            }
        }

        return null;
    }


    //---------------------------------------------------------------------------------------------//


    //----------------------------------For Real Path Pick ----------------------------------------//


    @TargetApi(Build.VERSION_CODES.KITKAT)
    public String getPath(final Context context, final Uri uri) {
        if (uri == null) return null;

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public String getDataColumn(Context context, Uri uri, String selection,
                                String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }


    public boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


    public String getRealPathFromURI(Context context, Uri uri) {
        if (uri == null) return null;

        if (ContentResolver.SCHEME_FILE.equals(uri.getScheme())) {
            return uri.getPath();
        }

        if (Build.VERSION.SDK_INT < 19) {
            return getRealPathFromURI_API11to18(context, uri);
        } else {
            return getRealPathFromURI_API19(context, uri);
        }

    }

    @SuppressLint("NewApi")
    public String getRealPathFromURI_API19(Context context, Uri uri) {
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(uri);

        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];

        String[] column = {MediaStore.Images.Media.DATA};

        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = context.getContentResolver().query(uri,
                column, sel, new String[]{id}, null);

        int columnIndex = 0;
        if (cursor != null) {
            columnIndex = cursor.getColumnIndex(column[0]);
            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        }
        return filePath;
    }


    @SuppressLint("NewApi")
    public String getRealPathFromURI_API11to18(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        String result = null;

        CursorLoader cursorLoader = new CursorLoader(
                context,
                contentUri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if (cursor != null) {
            int column_index =
                    cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(column_index);
        }

        return result;
    }

    public String getRealPathFromURI_BelowAPI11(Context context, Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        @SuppressLint("Recycle") Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = 0;
        if (cursor != null) {
            column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return "";
    }


    //---------------------------------------------------------------------------------------------//


    //------------------------------------ Utils --------------------------------------------------//
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public void showToast(String message) {

        LayoutInflater inflater = getLayoutInflater();
        View layouttoast = inflater.inflate(R.layout.toastcustom, (ViewGroup) findViewById(R.id.toastcustom));
        ((TextView) layouttoast.findViewById(R.id.texttoast)).setText(message);

        Toast mytoast = new Toast(getBaseContext());
        mytoast.setView(layouttoast);
        mytoast.setDuration(Toast.LENGTH_LONG);
        mytoast.setGravity(Gravity.CENTER, 0, 0);
        mytoast.show();
        ///Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void showNetworkAlert() {
        alert(this, "Network Error", "No network connected");
    }

    public void alert(Context context, String title, String message) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }


    //---------------------------------------------------------------------------------------------//


    //------------------------------------ Image  --------------------------------------------------//


    public Bitmap compressedBitmap(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] BYTE;
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        BYTE = byteArrayOutputStream.toByteArray();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;
        return BitmapFactory.decodeByteArray(BYTE, 0, BYTE.length, options);
    }


    public String storeImage(Bitmap image) {

        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.d(TAG,
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return null;
        }
        try {
            String s = getSize(String.valueOf(image.getWidth()), String.valueOf(image.getHeight()));
            String size = getSize(s.substring(0, s.indexOf(",")), s.substring(s.indexOf(",") + 1));
            image = Bitmap.createScaledBitmap(image, Integer.parseInt(size.substring(0, size.indexOf(","))), Integer.parseInt(size.substring(size.indexOf(",") + 1)), true);
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.JPEG, 70, fos);
            fos.close();
            return pictureFile.getAbsolutePath();
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
            return null;
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
            return null;
        }
    }

    public File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getApplicationContext().getPackageName()
                + "/Files");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss", Locale.US).format(new Date());
        File mediaFile;
        String mImageName = "MI_" + timeStamp + ".jpeg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public String getSize(String _width, String _height) {
        int width = Integer.parseInt(_width);
        int height = Integer.parseInt(_height);
        if (width > height) {
            if (width > 720) {
                height = (height * 720) / width;
                width = 720;
            }
            return width + "," + height;
        } else {
            if (height > 720) {
                width = (width * 720) / height;
                height = 720;
            }
            return width + "," + height;
        }
    }


    //---------------------For Time -----------------------------//

    public void showDatePicker(final CustomTextView textView, long minDate, long maxDate) {
        final Calendar calendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.US);
                String formattedDate = df.format(calendar.getTime());
                textView.setText(formattedDate);
            }

        };
        DatePickerDialog dpd = new DatePickerDialog(this, date, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        if (minDate != 0) {
            dpd.getDatePicker().setMinDate(minDate);
        }
        if (maxDate != 0) {
            dpd.getDatePicker().setMaxDate(maxDate);
        }
        dpd.show();
    }

    public void showTimePicker(final CustomTextView textView, int minHours, int minMinutes, int maxHours, int maxMinutes) {
        final Calendar calendar = Calendar.getInstance();
        CustomTimePickerDialog cpd = new CustomTimePickerDialog(this, new CustomTimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar.set(Calendar.MINUTE, minute);
                SimpleDateFormat dt = new SimpleDateFormat("hh:mm a", Locale.US);
                String formattedTime = dt.format(calendar.getTime());
                textView.setText(formattedTime);
            }
        }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false);
        if (minHours != 0) {
            cpd.setMin(minHours, minMinutes);
        }
        if (maxHours != 0) {
            cpd.setMax(maxHours, maxMinutes);
        }
    }


    public enum ApiCall {
        GET_USERS_DATA, GET_LEVELS, GET_MEMBER_TYPES, ADD_TASK, GET_ASSIGNED_TASK, GET_MYTASK,
        GET_BRODCASTS_LIST, GET_BROADCASTS_INBOX_LIST, SAVE_BRODCAST, ADD_COMMENT, GET_TASK_DATA,
        GET_BROADCASTS_DETAILS, GET_REPLIES, LOGOUT, ADD_SUBTASK_DATA, REMOVE_SUB_TASK_DATA, UPDATE_TASK,
        SEARCH_MY_TASKS, SEARCH_ASSIGNED_TASK, GET_NOTIFICATION, NOTE_IMAGE_DELETE
    }

    public void ApiCall(String url, final ApiCall type, RequestParams requestParams) {
        try {
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(60 * 1000);
            client.setURLEncodingEnabled(true);
            client.post(getApplicationContext(), url, requestParams, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        if (response.getBoolean("success")) {
                            OnResponce(response, type);
                        } else {
                            OnError(response, type);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    hideProgressDialog(BaseActivity.this);
                    if (errorResponse != null) {
                        try {
                            showToast(errorResponse.getString("message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        OnError(errorResponse, type);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    hideProgressDialog(BaseActivity.this);
                    showToast(responseString);
                }
            });
        } catch (Exception e) {
            hideProgressDialog(BaseActivity.this);
            e.printStackTrace();
        }
    }

    public void ApiCallWithToken(String url, final ApiCall type, RequestParams requestParams) {

        try {
            Datum userData = getLoginData();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(60 * 1000);
            if (userData != null) {
                SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences("Login", MODE_PRIVATE);
                String token = sharedpreferences.getString("Token", null);
                client.addHeader("Authorization", "Bearer " + token);

                requestParams.put("intProjectId", userData.getArryselectedProjectAllItems().get(0).getId());
                requestParams.put("intOrganisationId", userData.getArryselectedOrganisationAllItems().get(0).getId());
            }
//            ByteArrayEntity entity = new ByteArrayEntity(requestParams.toString().getBytes("UTF-8"));
//            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            Log.e("ApiToken", type + "..." + requestParams);
            client.setURLEncodingEnabled(true);
            client.post(getApplicationContext(), url, requestParams, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        if (response.getBoolean("success")) {
                            OnResponce(response, type);
                        } else {
                            OnError(response, type);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    hideProgressDialog(BaseActivity.this);
                    try {
                        showToast(errorResponse.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    OnError(errorResponse, type);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    hideProgressDialog(BaseActivity.this);
                    showToast(responseString);
                }
            });
        } catch (Exception e) {
            hideProgressDialog(BaseActivity.this);
            e.printStackTrace();
        }
    }

    public void OnResponce(JSONObject object, ApiCall type) {
        String s = object.toString();
    }

    public void OnError(JSONObject object, ApiCall type) {
        String s1 = object.toString();
    }


}
