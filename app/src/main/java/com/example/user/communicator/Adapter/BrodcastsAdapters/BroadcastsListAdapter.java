package com.example.user.communicator.Adapter.BrodcastsAdapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.user.communicator.Activity.BrodcastsModule.BroadCastDetailActivity;
import com.example.user.communicator.CustomViews.CustomEditText;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.BrodcastsModels.BroadCastItem;
import com.example.user.communicator.Model.BrodcastsModels.InBoxReplyItem;
import com.example.user.communicator.Model.BrodcastsModels.OptionItem;
import com.example.user.communicator.Model.Login.Login;
import com.example.user.communicator.R;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

import static android.content.Context.MODE_PRIVATE;


public class BroadcastsListAdapter extends RecyclerView.Adapter<BroadcastsListAdapter.ViewHolder> {

    private final String URL_ROOT = "http://13.232.124.188:9999/api/";
    private Context context;
    private ArrayList<BroadCastItem> brodcastItems;
    private OnClickListner onClick;
    private ProgressDialog pDialog;
    private boolean isMyBroadCast = true;
    private String sendReply = URL_ROOT + "broadcast/reply";

    public BroadcastsListAdapter(Context context, ArrayList<BroadCastItem> brodcastItems) {
        this.brodcastItems = brodcastItems;
        this.context = context;
    }

    public void setIsMyBroadCast(boolean isMyBroadCast) {
        this.isMyBroadCast = isMyBroadCast;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_brodcast, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(brodcastItems.get(position));
    }

    @Override
    public int getItemCount() {
        return brodcastItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void showProgressDialog(String message) {
        if (pDialog == null || !pDialog.isShowing()) {
            pDialog = new ProgressDialog(context);
            pDialog.setMessage(message);
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }

    public void hideProgressDialog() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    private void showToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public void setOnClickListner(OnClickListner onClickListner) {
        this.onClick = onClickListner;
    }

    private String getMillsToDate(long mills) {
        SimpleDateFormat df = new SimpleDateFormat("dd,MMM yyyy", Locale.US);
        return df.format(new Date(mills));
    }

    public interface OnClickListner {
        void OnClick(int position);

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        CustomTextView tvTitle;

        @BindView(R.id.tvDate)
        CustomTextView tvDate;

        @BindView(R.id.send)
        CustomTextView send;

        @BindView(R.id.ansEdtTxt)
        CustomEditText ansEdtTxt;

        @BindView(R.id.ansOptions)
        RecyclerView ansOptionsView;

        @BindView(R.id.cvTasks_Two)
        CardView mCardView;

        @BindView(R.id.ivInfo)
        ImageView ivInfo;

        @BindView(R.id.countHolder)
        LinearLayout countHolder;

        @BindView(R.id.count)
        CustomTextView count;

        ArrayList<OptionItem> optionItemArrayList = new ArrayList<>();
        OptionSelectionAdapter selectionAdapter;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final BroadCastItem item) {

            tvTitle.setText(item.getMessage());
            tvDate.setText(getMillsToDate(item.getAddTime()));

            if (!isMyBroadCast) {
                ivInfo.setVisibility(View.GONE);
                countHolder.setVisibility(View.GONE);
                boolean isAns = false;
                if (item.getReply() != null && item.getReply().size() != 0) {
                    isAns = true;
                }
                if (!TextUtils.isEmpty(item.getBroadcastType())) {
                    if (item.getBroadcastType().equalsIgnoreCase("0")) {
                        ansEdtTxt.setVisibility(View.GONE);
                        ansOptionsView.setVisibility(View.GONE);
                        send.setVisibility(View.GONE);
                    } else {
                        send.setVisibility(View.VISIBLE);
                        if (item.getReplyType().equalsIgnoreCase("0")) {
                            ansEdtTxt.setVisibility(View.VISIBLE);
                            ansOptionsView.setVisibility(View.GONE);
                            if (isAns) {
                                send.setVisibility(View.GONE);
                                ansEdtTxt.setText(item.getReply().get(0).getReplyValue().get(0));
                                ansEdtTxt.setEnabled(false);
                                ansEdtTxt.setTextColor(context.getResources().getColor(R.color.dark_grey));
                            }
                        } else {
                            send.setVisibility(View.VISIBLE);
                            ansEdtTxt.setVisibility(View.GONE);
                            ansOptionsView.setVisibility(View.VISIBLE);
                            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                            ansOptionsView.setLayoutManager(linearLayoutManager);
                            if (item.getReplyType().equalsIgnoreCase("1")) {
                                selectionAdapter = new OptionSelectionAdapter(context, optionItemArrayList, false);
                                optionItemArrayList.clear();
                                for (String option : item.getReplyOptions()) {
                                    OptionItem item1 = new OptionItem();
                                    item1.setName(option);
                                    if (isAns) {
                                        if (option.equalsIgnoreCase(item.getReply().get(0).getReplyValue().get(0))) {
                                            item1.setChacked(true);
                                        }
                                    }
                                    optionItemArrayList.add(item1);
                                }
                            } else {
                                selectionAdapter = new OptionSelectionAdapter(context, optionItemArrayList, true);
                                optionItemArrayList.clear();
                                for (String option : item.getReplyOptions()) {
                                    OptionItem item1 = new OptionItem();
                                    item1.setName(option);
                                    if (isAns) {
                                        ArrayList<String> strings = new ArrayList<>(item.getReply().get(0).getReplyValue());
                                        if (strings.contains(option)) {
                                            item1.setChacked(true);
                                        }
                                    }
                                    optionItemArrayList.add(item1);
                                }
                            }
                            if (isAns) {
                                send.setVisibility(View.GONE);
                                selectionAdapter.setIsAns(true);
                            } else {
                                selectionAdapter.setIsAns(false);
                            }
                            ansOptionsView.setAdapter(selectionAdapter);
                        }
                    }
                } else {
                    send.setVisibility(View.GONE);
                    ansEdtTxt.setVisibility(View.GONE);
                    ansOptionsView.setVisibility(View.GONE);
                }
            } else {

                countHolder.setVisibility(View.VISIBLE);
                ivInfo.setVisibility(View.VISIBLE);
                send.setVisibility(View.GONE);
                ansEdtTxt.setVisibility(View.GONE);
                ansOptionsView.setVisibility(View.GONE);

                count.setText(String.valueOf(item.getReplyCount()));


            }

            ivInfo.setOnClickListener(v -> {
                Intent i = new Intent(context, BroadCastDetailActivity.class);
                i.putExtra("broadcastId", item.get_id());
                i.putExtra("isFromNotification", false);
                context.startActivity(i);
            });

            send.setOnClickListener(v -> {
                JSONArray array = new JSONArray();
                if (item.getReplyType().equalsIgnoreCase("0")) {
                    String ans = ansEdtTxt.getText().toString().trim();
                    if (TextUtils.isEmpty(ans)) {
                        showToast("Please write something");
                        return;
                    }
                    array.put(ans);
                    sendReply(getAdapterPosition(), array);

                } else if (item.getReplyType().equalsIgnoreCase("1")) {
                    String ans = "";
                    for (OptionItem item1 : optionItemArrayList) {
                        if (item1.isChacked()) {
                            ans = item1.getName();
                        }
                    }
                    if (TextUtils.isEmpty(ans)) {
                        showToast("Please select one option");
                        return;
                    }
                    array.put(ans);
                    sendReply(getAdapterPosition(), array);
                } else if (item.getReplyType().equalsIgnoreCase("2")) {
                    for (OptionItem item1 : optionItemArrayList) {
                        if (item1.isChacked()) {
                            array.put(item1.getName());
                        }
                    }
                    if (array.length() == 0) {
                        showToast("Please select at least one option");
                        return;
                    }
                    sendReply(getAdapterPosition(), array);
                }
            });
        }

        private void sendReply(final int position, final JSONArray ans) {

            SharedPreferences sharedpreferences = context.getSharedPreferences("Login", MODE_PRIVATE);
            String data = sharedpreferences.getString("UserData", null);
            Gson gson = new Gson();

            Login mUserDetails = gson.fromJson(data, Login.class);

            RequestParams params = new RequestParams();
            params.put("intUserId", mUserDetails.getData().get(0).getIntUserId());
            params.put("broadcastId", brodcastItems.get(position).get_id());
            params.put("replyValue", ans);
            showProgressDialog("Please Wait!");
            try {
                AsyncHttpClient client = new AsyncHttpClient();
                client.setTimeout(60 * 1000);
                client.setURLEncodingEnabled(true);
                client.post(context, sendReply, params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        hideProgressDialog();
                        try {
                            if (response.getBoolean("success")) {
                                brodcastItems.get(position).getReply().clear();
                                JSONArray array = response.getJSONObject("data").getJSONArray("replyData");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject object = array.getJSONObject(i);
                                    InBoxReplyItem replyItem = gson.fromJson(object.toString(), InBoxReplyItem.class);
                                    brodcastItems.get(position).getReply().add(replyItem);
                                }
                                notifyDataSetChanged();
                            }
                            showToast(response.getString("message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        hideProgressDialog();
                        try {
                            showToast(errorResponse.getString("message"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        hideProgressDialog();
                        showToast(responseString);
                    }
                });
            } catch (Exception e) {
                hideProgressDialog();
                e.printStackTrace();
            }

        }

    }

}
