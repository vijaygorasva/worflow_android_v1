package com.example.user.communicator.Activity.TaskModule.DetailsActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;

import com.example.user.communicator.Activity.MainActivity;
import com.example.user.communicator.Activity.TaskModule.BaseActivity;
import com.example.user.communicator.Model.TaskModels.TaskDetailItem;
import com.example.user.communicator.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsTabActivity extends BaseActivity {

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.pager)
    ViewPager viewPager;
    String task_Id;
    boolean isFromNotification = false;
    boolean isAppIsInBackground = false;
    public static boolean isUpdated = false;
    public boolean isCategoryChanged = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_details_tab);
        ButterKnife.bind(this);
        task_Id = String.valueOf(getIntent().getIntExtra("_id", 0));
        isFromNotification = getIntent().getBooleanExtra("isFromNotification", false);
        isAppIsInBackground = getIntent().getBooleanExtra("isAppIsInBackground", false);

        tabLayout.addTab(tabLayout.newTab().setText("Details"));
        tabLayout.addTab(tabLayout.newTab().setText("Activities"));
        tabLayout.addTab(tabLayout.newTab().setText("Files"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        setViewPager();
    }

    public void setViewPager() {
        final PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 4521 && resultCode == RESULT_OK) {
            if (data != null) {
                boolean isUpdated = data.getBooleanExtra("isUpdated", false);
                isCategoryChanged = data.getBooleanExtra("isCategoryChanged", false);
                if (isUpdated) {
                    DetailsTabActivity.isUpdated = true;
                    if (!TextUtils.isEmpty(task_Id)) {
                        setViewPager();
                    }
                }
            }
        }
    }

    public class PagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return DetailsPageFragment.newInstance(task_Id, isFromNotification);
                case 1:
                    return ActivitysPageFragment.newInstance(task_Id, isFromNotification);
                case 2:
                    return FilesPageFrgament.newInstance(task_Id, isFromNotification);
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }

    @Override
    public void onBackPressed() {
        if (isFromNotification) {
            if (isAppIsInBackground) {
                Intent i = new Intent(this, MainActivity.class);
                startActivity(i);
            } else {
                finish();
            }
        } else {
            Intent i = new Intent();
            i.putExtra("isUpdated", isUpdated);
            i.putExtra("isCategoryChanged", isCategoryChanged);
            i.putExtra("_id", getIntent().getIntExtra("_id", 0));
            i.putExtra("position", getIntent().getIntExtra("position", 0));
            setResult(RESULT_OK, i);
            finish();
        }
    }
}
