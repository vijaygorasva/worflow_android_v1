package com.example.user.communicator.Model.TaskModels;


import io.realm.RealmObject;

public class DueDate extends RealmObject {

    private String startDate;
    private String dueDate;

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }
}
