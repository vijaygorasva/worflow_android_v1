package com.example.user.communicator.Model.InboxModel;

import io.realm.RealmObject;

public class NotificationData extends RealmObject {
    private String type;
    private String taskId;
    private String intUserId;
    private String intTaskDocumentNo;
    private String title;
    private String body;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getIntUserId() {
        return intUserId;
    }

    public void setIntUserId(String intUserId) {
        this.intUserId = intUserId;
    }

    public String getIntTaskDocumentNo() {
        return intTaskDocumentNo;
    }

    public void setIntTaskDocumentNo(String intTaskDocumentNo) {
        this.intTaskDocumentNo = intTaskDocumentNo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
