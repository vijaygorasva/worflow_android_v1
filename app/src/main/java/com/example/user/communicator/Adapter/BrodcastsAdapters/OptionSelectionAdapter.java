package com.example.user.communicator.Adapter.BrodcastsAdapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.example.user.communicator.Model.BrodcastsModels.OptionItem;
import com.example.user.communicator.R;

import java.util.ArrayList;


public class OptionSelectionAdapter extends RecyclerView.Adapter<OptionSelectionAdapter.ViewHolder> {

    private OnItemClickList mItemClickList;
    private ArrayList<OptionItem> mItems;
    private Context mContext;
    private boolean isMultiple = false;
    private boolean isAns = false;

    public OptionSelectionAdapter(Context context, ArrayList<OptionItem> Items, boolean isMultiple) {
        this.mContext = context;
        this.mItems = Items;
        this.isMultiple = isMultiple;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(mContext)
                .inflate(R.layout.item_broadcast_option_selection, parent, false);
        return new ViewHolder(inflatedView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final boolean[] lastCheck = {mItems.get(position).isChacked()};
        holder.rdBtn.setText(mItems.get(position).getName());
        holder.rdBtn.setChecked(lastCheck[0]);


//        holder.rdBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (!isMultiple) {
//                    if (isChecked) {
//                        for (int i = 0; i < mItems.size(); i++) {
//                            if (i != position) {
//                                mItems.get(i).setChacked(false);
//                            } else {
//                                mItems.get(i).setChacked(true);
//                            }
//                        }
//                        notifyDataSetChanged();
//                    }
//                } else {
//                    if (lastCheck[0] == isChecked) {
//                        holder.rdBtn.setChecked(!lastCheck[0]);
//                        lastCheck[0] = !isChecked;
//                    }
//                }
//            }
//        });

        holder.rdBtn.setOnClickListener(v -> {
            if (!isMultiple) {
                if (!lastCheck[0]) {
                    lastCheck[0] = !lastCheck[0];
                    for (int i = 0; i < mItems.size(); i++) {
                        if (i != position) {
                            mItems.get(i).setChacked(!lastCheck[0]);
                        } else {
                            mItems.get(i).setChacked(lastCheck[0]);
                        }
                    }
                } else {
                    mItems.get(position).setChacked(!lastCheck[0]);
                }
                notifyDataSetChanged();
            } else {
                holder.rdBtn.setChecked(!lastCheck[0]);
                mItems.get(position).setChacked(!lastCheck[0]);
                lastCheck[0] = !lastCheck[0];
            }
        });

        if (isAns) {
            holder.rdBtn.setEnabled(false);
            holder.rdBtn.setTextColor(mContext.getResources().getColor(R.color.dark_grey));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.rdBtn.setButtonTintList(ColorStateList.valueOf(ContextCompat.getColor(mContext, R.color.black)));
            }
            holder.rdBtn.setHighlightColor(mContext.getResources().getColor(R.color.black));
        }
    }

    public void setIsAns(boolean isAns) {
        this.isAns = isAns;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public void setOnItemClickListener(final OnItemClickList mItemClickListener) {
        this.mItemClickList = mItemClickListener;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public interface OnItemClickList {
        void onItemClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RadioButton rdBtn;


        private ViewHolder(final View view) {
            super(view);
            rdBtn = view.findViewById(R.id.rdBtn);
        }
    }
}
