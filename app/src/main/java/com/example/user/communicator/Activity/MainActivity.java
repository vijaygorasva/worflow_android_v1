package com.example.user.communicator.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.example.user.communicator.Activity.AlertsModule.InboxActivity;
import com.example.user.communicator.Activity.PlannerModule.PlannerTabActivity;
import com.example.user.communicator.Activity.TaskModule.BaseActivity;
import com.example.user.communicator.Activity.TaskModule.Task.TaskTabActivity;
import com.example.user.communicator.Model.Filter;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.TaskModels.SelectionObject;
import com.example.user.communicator.R;
import com.example.user.communicator.Utility.Constant;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;


public class MainActivity extends BaseActivity {
    @BindView(R.id.cvPlanner)
    CardView cvPlanner;
    @BindView(R.id.cvTasks)
    CardView cvTasks;
    @BindView(R.id.cvBroadcast)
    CardView cvBroadcast;
    @BindView(R.id.cvStatus)
    CardView cvStatus;
    @BindView(R.id.cvChats)
    CardView cvChats;
    @BindView(R.id.cvInsights)
    CardView cvInsights;
    @BindView(R.id.cvAlerts)
    CardView cvAlerts;

    @BindView(R.id.logout)
    ImageView mLogout;

    String strUserId;
    Realm mRealm;
    Datum mUserDetails;
    private static long back_pressed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main_new);
        ButterKnife.bind(this);

        mRealm = Realm.getDefaultInstance();
        mUserDetails = getLoginData();
        strUserId = mUserDetails.getIntUserId();
//        Intent intent = new Intent(getApplicationContext(), WorkerService.class);
//        startActivity();

    }

    @OnClick({R.id.cvPlanner, R.id.cvTasks, R.id.cvBroadcast, R.id.cvStatus, R.id.cvChats, R.id.cvInsights, R.id.cvAlerts, R.id.logout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cvPlanner:
//                showToast("THis is dummy message");
                Intent intent = new Intent(MainActivity.this, PlannerTabActivity.class);//PlannerTabActivity
                startActivity(intent);
//                finish();
                break;
            case R.id.cvTasks:

                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                RealmResults<Filter> filterRealmResults = realm.where(Filter.class).findAll();
                filterRealmResults.deleteAllFromRealm();
                realm.commitTransaction();

                Intent intent1 = new Intent(MainActivity.this, TaskTabActivity.class);
                startActivity(intent1);
//                finish();
                break;
            case R.id.cvBroadcast:
//                Intent intent2 = new Intent(MainActivity.this, BroadCastsActivity.class);
//                startActivity(intent2);
//                finish();
                break;
            case R.id.cvStatus:
//                Intent intent3 = new Intent(MainActivity.this, PlannerTabActivity.class);//DetailActivtiy  PlannerTabActivity
//                startActivity(intent3);
                break;
            case R.id.cvChats:
//                Intent intent4 = new Intent(MainActivity.this, DummyActivity.class);//ChatActivity
//                startActivity(intent4);
                break;
            case R.id.cvInsights:
//                Intent intent5 = new Intent(MainActivity.this, PlannerActivity.class);
//                startActivity(intent5);
                break;
            case R.id.cvAlerts:
                Intent intent6 = new Intent(MainActivity.this, InboxActivity.class);
                startActivity(intent6);
                break;
            case R.id.logout:
                Datum mUserData = getLoginData();
                if (mUserData != null) {
                    RequestParams params = new RequestParams();
                    params.put("intUserId", mUserData.getIntUserId());
                    params.put("deviceType", ANDROID);
                    params.put("deviceToken", Constant.DEVICE_TOKEN);
                    showProgressDialog(MainActivity.this, "Logout...");
                    ApiCall(logout, BaseActivity.ApiCall.LOGOUT, params);
                } else {
                    logOut();
                }
                break;
        }
    }

    @Override
    public void OnResponce(JSONObject object, BaseActivity.ApiCall type) {

        if (type == BaseActivity.ApiCall.LOGOUT) {
            hideProgressDialog(MainActivity.this);
            logOut();
        }
    }

    @Override
    public void onBackPressed() {

        if (back_pressed + 2000 > System.currentTimeMillis()) super.onBackPressed();
        else
            showToast("Press once again to exit!");
        back_pressed = System.currentTimeMillis();

    }

    public void logOut() {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
        deleteSelectionItem();
        clearLoginData();
        finish();
        Intent i = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(i);
//        Log.e("logOut","...//"+new Gson().toJson(mUserDetails));
    }


    public void deleteSelectionItem() {
        SharedPreferences.Editor editor1 = getSharedPreferences().edit();
        editor1.putString("SelectionObject", "");
        editor1.apply();
    }

    public SelectionObject getSelectionItem() {
        String appInfo = getSharedPreferences().getString("SelectionObject", "");
        Gson gson = new Gson();
        if (!TextUtils.isEmpty(appInfo)) {
            return gson.fromJson(appInfo, SelectionObject.class);
        }
        return new SelectionObject();
    }
    @Override
    public void OnError(JSONObject object, BaseActivity.ApiCall type) {
        hideProgressDialog(MainActivity.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1 && resultCode == 1) {
            String textData = data.getStringExtra("SELECTED_DATA");

//            Log.e("textData", textData + "");

//            getAllNotes();
//            if (textData.equals("All Locations")) {
//                location = "all";
//            } else {
//                location = textData;
//            }

        }

    }


}
