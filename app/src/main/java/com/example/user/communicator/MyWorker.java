package com.example.user.communicator;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.example.user.communicator.Utility.DateUtils;

import io.realm.Realm;

//import androidx.work.WorkerParameters;

public class MyWorker extends Worker {

    Realm mRealm;
    public static final String TASK_DESC = "TIME";

    public MyWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

//    public MyWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
//        super(context, workerParams);
//    }

    @NonNull
    @Override
    public Result doWork() {
        String taskDesc = getInputData().getString(TASK_DESC);//, "11:08");
        Log.e("taskDesc", taskDesc + "...");
        Boolean isSame = DateUtils.getCurrentTimeUsingDate(taskDesc);
        Log.e("isSame", isSame + ".." + taskDesc);
        if (isSame) sendNotification("Planner Alarm", taskDesc);
        return Result.success();
    }

    public void sendNotification(String title, String message) {

        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        //If on Oreo then notification required a notification channel.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel("default", "Default", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(getApplicationContext(), "Alaram")
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri);
//                        .setContentIntent(pendingIntent);

        notificationManager.notify(1, notificationBuilder.build());

    }

}
