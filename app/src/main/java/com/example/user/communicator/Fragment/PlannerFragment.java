package com.example.user.communicator.Fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.icu.text.SimpleDateFormat;
import android.icu.util.TimeZone;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.user.communicator.Activity.PlannerModule.AddNewNoteActivity;
import com.example.user.communicator.Adapter.PlannerAdapter.PlannerSectionAdapter;
import com.example.user.communicator.Adapter.PlannerAdapter.SectionModel;
import com.example.user.communicator.Application;
import com.example.user.communicator.Dialog.AllCategoryListDialog;
import com.example.user.communicator.Model.BaseActivityModel;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.Notes.Item;
import com.example.user.communicator.Model.Notes.TodoListItem;
import com.example.user.communicator.Model.Reminder.Reminder;
import com.example.user.communicator.R;
import com.example.user.communicator.Utility.EELViewUtils;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

public class PlannerFragment extends BasicFragment implements PlannerSectionAdapter.Callback {//}, ConnectivityReceiver.ConnectivityReceiverListener {

    public static boolean isInActionMode = false;
    public static boolean isSelectedMode = true;
    public static ArrayList<Item> selectionList = new ArrayList<>();

    @BindView(R.id.fabtnSearch)
    FloatingActionButton fabtnSearch;
    @BindView(R.id.fabtnDelete)
    FloatingActionButton fabtnDelete;
    @BindView(R.id.fabtnAdd)
    FloatingActionButton fabtnAdd;
    @BindView(R.id.llNodata)
    LinearLayout llNodata;
    @BindView(R.id.rlMainContent)
    RelativeLayout rlMainContent;
    @BindView(R.id.flMainContent)
    RelativeLayout flMainContent;
    @BindView(R.id.swipe_container)
    SwipeRefreshLayout mSwipeRefreshLayout;
    String strUserId;
    public static ArrayList<Item> allNotes = new ArrayList<>();
    ArrayList<SectionModel> sectionModelArrayList = new ArrayList<>();
    Datum mUserDetails;
    ArrayList<Item> allSharedNotes = new ArrayList<>();
    ArrayList<Item> allnotesPinned = new ArrayList<>();
    ArrayList<Item> allUnpinedNotes = new ArrayList<>();
    Realm mRealm;
    RecyclerView recyclerView;
    PlannerSectionAdapter adapter;
    //    String catID = "0";
    OnSearchSelectedListener mListener;
    String searchActive = "No";
    EELViewUtils mEelViewUtils;
    @BindView(R.id.nes)
    NestedScrollView nes;
    public static Boolean isConnected = false;
    int counter = 0;
    private boolean internetConnected = true;
    RealmResults<Item> itemsRealmResults;

    public static String plannerName = "All";
    public static String plannerNameTwo;
    public static String plannerNameThree;
    int k = 0;
    public static String strCategoryTypeId,
            strCategoryId,
            strSubCategoryId;
    public static String strCategoryTypeName, strCategoryName, strSubCategoryName;
    AllCategoryListDialog allCategoryListDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().invalidateOptionsMenu();
    }

    public static PlannerFragment newInstance(Boolean isconnected) {
        PlannerFragment fragment = new PlannerFragment();
        return fragment;
    }

    public void putArguments(Bundle args) {
        isConnected = args.getBoolean("isConnected");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_planner, container, false);
        ButterKnife.bind(this, view);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);

        recyclerView = view.findViewById(R.id.rvNoteList);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        mEelViewUtils = new EELViewUtils(flMainContent, recyclerView);
        adapter = new PlannerSectionAdapter(getActivity(), getFragmentManager(), sectionModelArrayList, this);
        recyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public View provideYourFragmentView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return null;
    }

    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = Application.getConnectivityStatusString(context);
            setSnackbarMessage(status);
        }
    };

    @Override
    public void onAttach(Context context) {
        if (context instanceof OnSearchSelectedListener) {
            mListener = (OnSearchSelectedListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + getResources().getString(R.string.exception_message));
        }
        adapter = new PlannerSectionAdapter(getActivity(), getFragmentManager(), sectionModelArrayList, this);
        super.onAttach(context);
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        nes.setNestedScrollingEnabled(false);
        mRealm = Realm.getDefaultInstance();
        recyclerView.setHasFixedSize(true);
        plannerName = "All";
        plannerNameTwo = "";
        plannerNameThree = "";
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("Login", 0);
        String data = sharedpreferences.getString("UserData", null);

        RealmResults<Reminder> itemsRealmResults11 = mRealm.where(Reminder.class).findAll();

        if (!TextUtils.isEmpty(data)) {
            Gson gson = new Gson();
            mUserDetails = gson.fromJson(data, Datum.class);
        }

        mSwipeRefreshLayout.setOnRefreshListener(() -> {
            allNotes.clear();
            itemsRealmResults = mRealm.where(Item.class).findAll();
            mEelViewUtils.showLoadingView();

//            Log.e("getCatgeory_onViewC", strCategoryTypeName + " = " + strCategoryTypeId + "..."
//                    + strCategoryName + " = " + strCategoryId + "...."
//                    + strSubCategoryName + " = " + strSubCategoryId + "....");

            if ((!TextUtils.isEmpty(strSubCategoryId)) && (!TextUtils.isEmpty(strSubCategoryId)) && (!TextUtils.isEmpty(strSubCategoryId))) {

                llNodata.setVisibility(View.VISIBLE);
            } else {

                if (TextUtils.isEmpty(strCategoryTypeId)) {
                    allNotes.addAll(itemsRealmResults);
                } else if (strCategoryName.equals("All")) {
                    RealmResults<Item> allRealmlistNotes = mRealm.where(Item.class).equalTo("fkIntCategoryTypeId", strCategoryTypeId).findAll();
                    allNotes.addAll(allRealmlistNotes);
                } else {
                    if (!TextUtils.isEmpty(strSubCategoryId)) {
                        RealmResults<Item> allRealmlistNotes = mRealm.where(Item.class).equalTo("fkIntCategoryId", strCategoryId).equalTo("fkIntSubCategoryId", strSubCategoryId).findAll();//
                        allNotes.addAll(allRealmlistNotes);
                    } else if (!TextUtils.isEmpty(strCategoryId)) {
                        RealmResults<Item> allRealmlistNotes = mRealm.where(Item.class).equalTo("fkIntCategoryId", strCategoryId).findAll();
                        allNotes.addAll(allRealmlistNotes);
                    } else if (!TextUtils.isEmpty(strCategoryTypeId)) {
                        RealmResults<Item> allRealmlistNotes = mRealm.where(Item.class).equalTo("fkIntCategoryTypeId", strCategoryTypeId).findAll();
                        allNotes.addAll(allRealmlistNotes);
                    }
                }
            }

            onItemsLoadComplete();
            getNewNoteList();
            clearActionMode();

        });

        strUserId = mUserDetails.getIntUserId();

        itemsRealmResults = mRealm.where(Item.class).findAll();
        mEelViewUtils.showLoadingView();

        strCategoryTypeId = "";
        strCategoryId = "";
        strSubCategoryId = "";

        allNotes.clear();
        allNotes.addAll(itemsRealmResults);
        onItemsLoadComplete();
        getNewNoteList();

    }

    @OnClick({R.id.fabtnAdd, R.id.fabtnSearch, R.id.fabtnDelete})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.fabtnAdd:
                if (plannerName.equalsIgnoreCase("All") && (TextUtils.isEmpty(strCategoryId)) && (TextUtils.isEmpty(strSubCategoryId))) {
                    allCategoryListDialog = AllCategoryListDialog.newIntance("PLANNER", true);
                    allCategoryListDialog.show(getFragmentManager().beginTransaction(), "my-dialog");
                } else {
                    Intent intent = new Intent(getActivity(), AddNewNoteActivity.class);//NewNoteActivity
                    intent.putExtra("plannerName", plannerName);//"CHANG" for temp add this..change to this later(tvCatgory.getText().toString())
                    startActivityForResult(intent, 3);
                    clearActionMode();
                }

                break;

            case R.id.fabtnSearch:
                if (k == 1) {
                    searchActive = "No";
                    getNewNoteList();
                    mListener.onSearchSelected(false);
                    k = 0;
                } else {
                    searchActive = "yes";
                    mListener.onSearchSelected(true);
                    k = 1;
                }
                fabtnSearch.setPadding(0, 80, 0, 0);
                getActivity().invalidateOptionsMenu();
                break;

            case R.id.fabtnDelete:

                isInActionMode = false;
                mListener.onSearchSelected(false);
                fabtnAdd.setVisibility(View.GONE);
                fabtnSearch.setVisibility(View.GONE);
                fabtnDelete.setVisibility(View.VISIBLE);

                if (isNetworkAvailable()) {
                    for (int i = 0; i < selectionList.size(); i++) {
                        int id = selectionList.get(i).getIntPlannerId();
                        adapter.removeData(selectionList.get(i));
                        adapter.notifyDataSetChanged();
//                        RealmResults<Item> deleteFromRealm = mRealm.where(Item.class).equalTo("intPlannerId", id).findAll();
//                        mRealm.beginTransaction();
//                        deleteFromRealm.deleteAllFromRealm();
//                        mRealm.commitTransaction();
//                        allNotes.remove(deleteFromRealm.get(0));
                        deletePlanner(id);
                    }
                }

                adapter.notifyDataSetChanged();
                clearActionMode();

                break;
        }
    }


    void onItemsLoadComplete() {
        mEelViewUtils.showContentViewWhite();
        allnotesPinned.clear();
        allUnpinedNotes.clear();
        allSharedNotes.clear();
        sectionModelArrayList.clear();

        if (allNotes.size() > 0) {

            for (int i = 0; i < allNotes.size(); i++) {
                Item item = allNotes.get(i);
                RealmList<String> collobarateUseId = item.getArryCollaboratUserId();
                if (collobarateUseId.size() > 0) {
                    allSharedNotes.add(item);
                } else {
                    Boolean isPinned = item.getBlnPinNote();
                    if (isPinned) {
                        allnotesPinned.add(item);
                    } else {
                        allUnpinedNotes.add(item);
                    }
                }

            }
        } else {
        }

        for (int i = 0; i < 3; i++) {
            if (i == 0) {
                sectionModelArrayList.add(new SectionModel("Pinned", allnotesPinned));
            } else if (i == 1) {
                sectionModelArrayList.add(new SectionModel("Shared", allSharedNotes));
            } else {
                sectionModelArrayList.add(new SectionModel("Others", allUnpinedNotes));
            }
        }
        adapter = new PlannerSectionAdapter(getActivity(), getFragmentManager(), sectionModelArrayList, this);
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter(adapter);
        mSwipeRefreshLayout.setRefreshing(false);

    }

    public void filter(String charText) {

        k = 0;
        ArrayList<SectionModel> sectionList = new ArrayList<>();
        ArrayList<Item> allLists = new ArrayList<>();
        charText = charText.toLowerCase(Locale.getDefault());

        if (charText.length() == 0) {
            adapter = new PlannerSectionAdapter(getActivity(), getFragmentManager(), sectionModelArrayList, this);
            recyclerView.setAdapter(adapter);
        } else {
            for (Item wp : allNotes) {
                List<TodoListItem> list = wp.getArryObjstrTodoList();
                if (wp.getStrTitle().toLowerCase(Locale.getDefault()).contains(charText)) {
                    allLists.add(wp);
                } else if (wp.getStrValue().toLowerCase(Locale.getDefault()).contains(charText)) {
                    allLists.add(wp);
                } else if (wp.getStrValue().toLowerCase(Locale.getDefault()).contains(" " + charText)) {
                    allLists.add(wp);
                } else if (list.size() != 0) {
                    for (int i = 0; i < list.size(); i++) {
                        String val = list.get(i).getStrNoteListValue();
                        if (val.toLowerCase(Locale.getDefault()).contains(charText)) {
                            allLists.add(wp);
                            i = list.size();
                        } else if (val.toLowerCase(Locale.getDefault()).contains(" " + charText)) {
                            allLists.add(wp);
                            i = list.size();
                        } else {
                        }
                    }
                }
            }
            sectionList.add(new SectionModel("", allLists));
            adapter = new PlannerSectionAdapter(getActivity(), getFragmentManager(), sectionList, this);
            recyclerView.setAdapter(adapter);
        }
        adapter.notifyDataSetChanged();
    }

    public void onBackPressed() {
        selectionList.clear();
        if (isInActionMode) {
            clearActionMode();
            adapter.notifyDataSetChanged();
        } else {
        }
        getActivity().invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.item_delete) {
        } else if (item.getItemId() == android.R.id.home) {
            if (!isInActionMode) {
            } else {
                clearActionMode();
                adapter.notifyDataSetChanged();
            }
        }
        return true;
    }

    public void clearActionMode() {
        fabtnAdd.setVisibility(View.VISIBLE);
        fabtnSearch.setVisibility(View.VISIBLE);
        fabtnDelete.setVisibility(View.GONE);
        isInActionMode = false;
        getActivity().invalidateOptionsMenu();
        selectionList.clear();
        adapter.notifyDataSetChanged();
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
    }

    public void deletePlanner(final int plannerId) {
        if (isNetworkAvailable()) {
            RequestParams params = new RequestParams();
            params.put("intPlannerId", String.valueOf(plannerId));
            params.put("strModifiedUserId", strUserId);

            ApiCallWithToken(url_noteDelete, ApiCall.DELETEPLANNER, params);
        } else {
            setSnackbarMessage(getString(R.string.noConnection));
        }
    }

    public void getNewNoteList() {

        BaseActivityModel object = mRealm.where(BaseActivityModel.class)
                .equalTo("type", "ApiCall.NEWPLANNERLIST")
                .findFirst();
        if (isNetworkAvailable()) {
            RequestParams params = new RequestParams();
            params.put("strCreateUserId", strUserId);
            params.put("strCategoryTypeId", strCategoryTypeId);
            params.put("strCategoryId", strCategoryId);
            params.put("strSubCategoryId", strSubCategoryId);

            if (object != null) {
                RealmResults<BaseActivityModel> apiPlannerList = mRealm.where(BaseActivityModel.class).equalTo("type", "ApiCall.NEWPLANNERLIST").findAll();
                params.put("strSyncDate", apiPlannerList.get(0).getDateTime());
                if (!TextUtils.isEmpty(apiPlannerList.get(0).getDateTime())) {
                    Log.e("params_1..", params + "..." + url_allNotessByTime);
                    ApiCallWithToken(url_allNotessByTime, ApiCall.NEWPLANNERLIST, params);
                }
            } else if (itemsRealmResults.size() == 0) {
                mEelViewUtils.showLoadingView();
                Log.e("params_2..", params + "..." + url_allNotess);
                ApiCallWithToken(url_allNotess, ApiCall.PLANNERLIST, params);
            }

        } else {
            String status = Application.getConnectivityStatusString(getActivity());
            setSnackbarMessage(status);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        if (requestCode == 1 && resultCode == 1) {//update
            Log.e("type_reslt", requestCode + "..." + resultCode + "...." + data);
            String typeItem = data.getStringExtra("message");
            int position = data.getIntExtra("position", -1);
            Boolean pinChanged = data.getBooleanExtra("pinChanged", false);

            Log.e("typeItem_message", position + "..." + typeItem + "..." + pinChanged + "..." + data.getParcelableExtra("item"));

            Gson gson = new Gson();
            if (adapter != null) {

                if (typeItem.equals("update")) {
//                    Item item = data.getParcelableExtra("item");
                    adapter.notifyItemChanged(position);
                    if (searchActive.equals("yes")) {
//                        mRealm.beginTransaction();
//                        mRealm.copyToRealmOrUpdate(item);
//                        mRealm.commitTransaction();
//                        adapter.restoreSearchItem(item, position);
                    } else {
//                        adapter.restoreItem(item, position);
                        getNewNoteList();
                    }
//
                } else if (typeItem.equals("delete")) {
                    String val = data.getStringExtra("item");
                    Log.e("onActivity_delete", val + "...");
                    Item item = gson.fromJson(val, Item.class);
                    adapter.removeItem(position);
                } else {//typeItem = offline
                    int plannerId = data.getIntExtra("plannerId", -1);
                    RealmResults<Item> itemsRealmResults = mRealm.where(Item.class).equalTo("intPlannerId", plannerId).findAll();

                    if (itemsRealmResults != null && itemsRealmResults.size() != 0) {
                        adapter.insertItems(itemsRealmResults.get(0), position);
                    }
                }
            }

        } else if (requestCode == 3 && resultCode == 3) {//insert

            String val = data.getStringExtra("item");

            Gson gson = new Gson();
            Item item = gson.fromJson(val, Item.class);

            Log.e("onActivity_INSERTT", new Gson().toJson(item) + "...");
            if (item == null) {
            } else {
                recyclerView.setVisibility(View.VISIBLE);
                llNodata.setVisibility(View.GONE);

                strCategoryTypeName = item.getStrCategoryTypeName();
                strCategoryTypeId = item.getfkIntCategoryTypeId();

                strCategoryName = item.getStrCategoryName();
                strCategoryId = item.getfkIntCategoryId();

                strSubCategoryId = item.getfkIntSubCategoryId();
                strSubCategoryName = item.getStrCategoryName();

                if (isNetworkAvailable()) {
                    getNewNoteList();
                } else {
                    adapter.insertItems(item, 0);
                }
            }
        } else {
            selectionList.clear();
            if (isInActionMode) {
                clearActionMode();
                adapter.refreshAdapter();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void OnResponce(JSONObject data, ApiCall type) {
        JSONArray array = null;
        Gson gson = new Gson();
        mEelViewUtils.showContentViewWhite();

        try {
            switch (type) {

                case NEWPLANNERLIST:
                    array = data.getJSONArray("data");
                    Log.e("params_NEWPLANNERLIS", allNotes.size() + "/...//" + array.length() + ",,," + array);
                    if (array.length() > 0) {

                        recyclerView.setVisibility(View.VISIBLE);
                        llNodata.setVisibility(View.GONE);
                        mRealm.beginTransaction();

                        /* api last called time to realm*/
                        Calendar mcurrentTime = Calendar.getInstance();
                        Date date = mcurrentTime.getTime();

                        TimeZone istTime = TimeZone.getTimeZone("GMT");
                        String pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS";//"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                        SimpleDateFormat gmtFormat = new SimpleDateFormat(pattern);
                        gmtFormat.setTimeZone(istTime);

                        BaseActivityModel baseActivityModel = new BaseActivityModel();
                        baseActivityModel.setType("ApiCall.NEWPLANNERLIST");
                        baseActivityModel.setDateTime(String.valueOf(gmtFormat.format(date)));//2019-05-25T06:33:28.483

                        mRealm.copyToRealmOrUpdate(baseActivityModel);
                        RealmResults<Item> itemList = mRealm.where(Item.class).findAll();
//                        Log.e("params_NEWPLANNER_first", allNotes.size()+"...."+itemList.size() + "....." + itemsRealmResults.size() + "...");

                        mRealm.commitTransaction();
                        /* api last called time to realm*/


                        for (int j = 0; j < array.length(); j++) {

                            JSONObject jsonObject = array.getJSONObject(j);
                            Item item = gson.fromJson(jsonObject.toString(), Item.class);
                            String actionType = item.getStrLogAction();
                            Log.e("params_actionType", actionType + "..." + new Gson().toJson(item));
                            mRealm.beginTransaction();
                            if (!TextUtils.isEmpty(actionType)) {

                                if (actionType.equals("ADD")) {

                                    RealmResults<Item> deleteFromRealm = mRealm.where(Item.class).equalTo("intPlannerId", item.getIntPlannerId()).findAll();
                                    Log.e("ADD", deleteFromRealm.size() + "...." + adapter.getItemCount() + "..." + allNotes.size());

                                    if (deleteFromRealm.size() == 1) {
                                        mRealm.copyToRealmOrUpdate(item);
                                    } else {
                                        mRealm.copyToRealmOrUpdate(item);
                                        adapter.insertItems(item, 0);
                                        allNotes.add(item);
                                    }

                                } else {

                                    for (int i = 0; i < allNotes.size(); i++) {
                                        Log.e("params_UPDATE_allNot2..", actionType + "/..../" + allNotes.size() + "/.../" + item.getIntPlannerId());
                                        try {

                                            if (allNotes.get(i).getIntPlannerId() == item.getIntPlannerId()) {
                                                //update and delete
                                                if (actionType.equals("UPDATE")) {
                                                    RealmList<String> allremoveColleborateUser = item.getRemoveArryCollaboratUserId();
                                                    Log.e("param_UPDATE", "UPDATE" + "/.../" + allNotes.get(i).getIntPlannerId() + "...." + allremoveColleborateUser.size());

                                                    if (allremoveColleborateUser.size() > 0) {
                                                        for (String user : allremoveColleborateUser) {
                                                            if (user.equals(strUserId)) {
                                                                RealmResults<Item> deleteFromRealm = mRealm.where(Item.class).equalTo("intPlannerId", item.getIntPlannerId()).findAll();
                                                                Log.e("params_deleteFromUpdate", deleteFromRealm.size() + "/.../" + allNotes.size() + "...." + user + "....");
                                                                if (deleteFromRealm.size() != 0) {
                                                                    allNotes.remove(i);
                                                                    adapter.removeItem(item);
                                                                    adapter.notifyDataSetChanged();
                                                                    deleteFromRealm.deleteAllFromRealm();
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        mRealm.copyToRealmOrUpdate(item);
                                                        adapter.restoreItem(item, 0);
                                                        adapter.notifyDataSetChanged();
                                                    }

                                                } else if (actionType.equals("DELETE")) {
                                                    RealmResults<Item> deleteFromRealm = mRealm.where(Item.class).equalTo("intPlannerId", item.getIntPlannerId()).findAll();
                                                    Log.e("params_deleteFromRealm", allNotes.get(i).getIntPlannerId() + "/.../" + deleteFromRealm.size() + "...." + allNotes.size() + "....");
                                                    if (deleteFromRealm.size() != 0) {
                                                        allNotes.remove(i);
                                                        adapter.removeItem(item);
                                                        adapter.notifyDataSetChanged();
                                                        deleteFromRealm.deleteAllFromRealm();
                                                    }
                                                }

                                            } else {

                                            }
                                        } catch (NullPointerException e) {
                                            Log.e("Nullpointer", e + "..");
                                        }
                                    }
                                }
                                mRealm.commitTransaction();
                            }
                        }
                    }
                    mSwipeRefreshLayout.setRefreshing(false);
                    break;

                case PLANNERLIST:
                    array = data.getJSONArray("data");
                    if (array.length() > 0) {

                        recyclerView.setVisibility(View.VISIBLE);
                        llNodata.setVisibility(View.GONE);
                        mRealm.beginTransaction();
                        allNotes.clear();
                        allSharedNotes.clear();
                        allnotesPinned.clear();
                        allUnpinedNotes.clear();
                        Calendar mcurrentTime = Calendar.getInstance();
                        Date date = mcurrentTime.getTime();

                        TimeZone istTime = TimeZone.getTimeZone("GMT");
                        String pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS";//"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                        SimpleDateFormat gmtFormat = new SimpleDateFormat(pattern);
                        gmtFormat.setTimeZone(istTime);

                        BaseActivityModel baseActivityModel = new BaseActivityModel();
                        baseActivityModel.setType("ApiCall.NEWPLANNERLIST");
                        baseActivityModel.setDateTime(String.valueOf(gmtFormat.format(date)));

                        mRealm.copyToRealmOrUpdate(baseActivityModel);

                        for (int i = 0; i < array.length(); i++) {
                            try {
                                JSONObject object = array.getJSONObject(i);
                                Log.e("object", object + "...");
                                Item item = gson.fromJson(object.toString(), Item.class);
                                allNotes.add(item);
                                mRealm.copyToRealmOrUpdate(item);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        mRealm.commitTransaction();
                        onItemsLoadComplete();

                    }
                    Log.e("param_array", "" + "/.../" + allNotes.size());
                    break;
                case DELETEPLANNER:
                    if (data.length() != 0) {
                        array = data.getJSONArray("data");
                        JSONObject jsonObject = array.getJSONObject(0);
                        int plannerId = jsonObject.getInt("intPlannerId");
                        showToast("Successfully removed.");

//                        mRealm.beginTransaction();
                        RealmResults<Item> deleteFromRealm = mRealm.where(Item.class).equalTo("intPlannerId", plannerId).findAll();
                        Log.e("params_delete_removed", deleteFromRealm.size() + "...." + allNotes.size() + "....");
//                        if (deleteFromRealm.size() != 0) {
                        adapter.notifyDataSetChanged();
//                            deleteFromRealm.deleteAllFromRealm();
//                        }
//                        mRealm.commitTransaction();
                        getNewNoteList();
                    }

                    counter = selectionList.size();
                    if (counter >= 1) {
                        fabtnAdd.setVisibility(View.GONE);
                        fabtnSearch.setVisibility(View.GONE);
                        fabtnDelete.setVisibility(View.VISIBLE);
                    } else {
                        fabtnAdd.setVisibility(View.VISIBLE);
                        fabtnSearch.setVisibility(View.VISIBLE);
                        fabtnDelete.setVisibility(View.GONE);
                    }

//                    RealmResults<Item> deleteFromRealm = mRealm.where(Item.class).equalTo("intPlannerId", item.getIntPlannerId()).findAll();
//                    Log.e("params_deleteFromRealm", deleteFromRealm.size() + "...." + "....");
//                    if (deleteFromRealm.size() != 0) {
//                        allNotes.remove(i);
//                        adapter.removeItem(item);
//                        deleteFromRealm.deleteAllFromRealm();
//                        adapter.notifyDataSetChanged();
//                    }

                    break;

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        super.OnResponce(data, type);
    }

    @Override
    public void OnError(JSONObject object, ApiCall type) {
//        llNodata.setVisibility(View.VISIBLE);

        itemsRealmResults = mRealm.where(Item.class).findAll();
        mEelViewUtils.showLoadingView();

        strCategoryTypeId = "";
        strCategoryId = "";
        strSubCategoryId = "";

        allNotes.clear();
        allNotes.addAll(itemsRealmResults);
        onItemsLoadComplete();
        getNewNoteList();

        super.OnError(object, type);
    }

    @Override
    public void multiSelect(Activity activity, int itemPosition, int listPos, String clickType) {

        if (itemPosition >= 0) {

            if (clickType.equals("OnLongClick")) {
                if (selectionList.size() == 0) {
                    if (!selectionList.contains(sectionModelArrayList.get(listPos).getItemArrayList().get(itemPosition))) {
                        selectionList.add(sectionModelArrayList.get(listPos).getItemArrayList().get(itemPosition));
                    } else {
                        selectionList.remove(sectionModelArrayList.get(listPos).getItemArrayList().get(itemPosition));
                    }
                }
                getActivity().invalidateOptionsMenu();
                isInActionMode = true;
                counter = selectionList.size();

                if (counter >= 1) {
                    fabtnAdd.setVisibility(View.GONE);
                    fabtnSearch.setVisibility(View.GONE);
                    fabtnDelete.setVisibility(View.VISIBLE);
                } else {
                    fabtnAdd.setVisibility(View.VISIBLE);
                    fabtnSearch.setVisibility(View.VISIBLE);
                    fabtnDelete.setVisibility(View.GONE);
                }
                if (getActivity().getActionBar() != null) {
                    getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
                }

            } else if (clickType.equals("OnClick")) {
                if (!selectionList.contains(sectionModelArrayList.get(listPos).getItemArrayList().get(itemPosition))) {
                    selectionList.add(sectionModelArrayList.get(listPos).getItemArrayList().get(itemPosition));
                } else {
                    selectionList.remove(sectionModelArrayList.get(listPos).getItemArrayList().get(itemPosition));
                }
                isSelectedMode = false;
                counter = selectionList.size();

                if (counter >= 1) {
                    fabtnAdd.setVisibility(View.GONE);
                    fabtnSearch.setVisibility(View.GONE);
                    fabtnDelete.setVisibility(View.VISIBLE);
                } else {
                    fabtnAdd.setVisibility(View.VISIBLE);
                    fabtnSearch.setVisibility(View.VISIBLE);
                    fabtnDelete.setVisibility(View.GONE);
                }
                getActivity().invalidateOptionsMenu();
            }
        }
    }


    @Override
    public void catChange(int catPos) {

        if (catPos == 1) {
            if (TextUtils.isEmpty(strCategoryTypeName) && TextUtils.isEmpty(strCategoryName)) {
                allCategoryListDialog = AllCategoryListDialog.newIntance("PLANNER", false);

                if (allCategoryListDialog != null) {
                    allCategoryListDialog.show(getFragmentManager().beginTransaction(), "my-dialog");//getActivity().getSupportFragmentManager().beginTransaction()
                }
            } else {
                getCategory(false, "", "", "", "", "", "");
            }

            plannerNameTwo = "";
            plannerNameThree = "";
        } else if (catPos == 2) {
            if (TextUtils.isEmpty(strSubCategoryName)) {
                allCategoryListDialog = AllCategoryListDialog.newIntance("PLANNER", false);
                if (allCategoryListDialog != null) {
                    allCategoryListDialog.show(getFragmentManager().beginTransaction(), "my-dialog");
                }
            } else {
                getCategory(false, strCategoryTypeName, strCategoryTypeId, strCategoryName, strCategoryId, "", "");
            }
            plannerNameThree = "";
        } else if (catPos == 3) {
            getCategory(false, strCategoryTypeName, strCategoryTypeId, strCategoryName, strCategoryId, strSubCategoryId, strSubCategoryName);
        }
    }

    public void getCategory(Boolean isFabActive, String tabNames, String tabIDs, String mainCatNams, String mainCatId, String subCatId, String subCatNames) {

        strCategoryTypeName = tabNames;
        strCategoryTypeId = tabIDs;

        strCategoryName = mainCatNams;
        strCategoryId = mainCatId;

        strSubCategoryId = subCatId;
        strSubCategoryName = subCatNames;

        if (allCategoryListDialog != null) {
            allCategoryListDialog.dismiss();
        }
        mRealm = Realm.getDefaultInstance();
        Log.e("fabtnAdd_getCatgeor_rec", isFabActive + "/////" + strCategoryTypeName + " = " + strCategoryTypeId + "..."
                + strCategoryName + " = " + strCategoryId + "...."
                + strSubCategoryName + " = " + strSubCategoryId + "....");

        if (strCategoryName.equals("All")) {
            plannerNameTwo = strCategoryTypeName;
        } else {
            if (!TextUtils.isEmpty(strSubCategoryName)) {
                plannerNameTwo = strCategoryName;// + " > " + strSubCategoryName;
                plannerNameThree = strSubCategoryName;
            } else {
                plannerNameTwo = strCategoryName;
            }
        }

        if (isFabActive) {
            Intent intent = new Intent(getActivity(), AddNewNoteActivity.class);//NewNoteActivity
            intent.putExtra("plannerName", plannerName);//"CHANG" for temp add this..change to this later(.getText().toString())
            startActivityForResult(intent, 3);
        }

        allNotes.clear();
        if (strCategoryName.equals("All")) {
            for (int i = 0; i < itemsRealmResults.size(); i++) {
                String CategoryTypeId = itemsRealmResults.get(i).getfkIntCategoryTypeId();
                if (CategoryTypeId.equals(strCategoryTypeId)) {
                    allNotes.add(itemsRealmResults.get(i));
                }
            }
        } else {
            if (!TextUtils.isEmpty(strSubCategoryId)) {
                RealmResults<Item> allRealmlistNotes = mRealm.where(Item.class).equalTo("fkIntCategoryId", strCategoryId).equalTo("fkIntSubCategoryId", strSubCategoryId).findAll();
                allNotes.addAll(allRealmlistNotes);
            } else if (!TextUtils.isEmpty(strCategoryId)) {
                RealmResults<Item> allRealmlistNotes = mRealm.where(Item.class).equalTo("fkIntCategoryId", strCategoryId).findAll();
                allNotes.addAll(allRealmlistNotes);
            } else if (!TextUtils.isEmpty(strCategoryTypeId)) {
                RealmResults<Item> allRealmlistNotes = mRealm.where(Item.class).equalTo("fkIntCategoryTypeId", strCategoryTypeId).findAll();
                allNotes.addAll(allRealmlistNotes);
            } else {
                RealmResults<Item> allRealmlistNotes = mRealm.where(Item.class).findAll();
                allNotes.addAll(allRealmlistNotes);
            }
        }

        if (adapter != null) {
            adapter.dialogCall();
        }
        onItemsLoadComplete();
        getNewNoteList();
    }

    public interface OnSearchSelectedListener {
        void onSearchSelected(Boolean isClicked);
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onDestroy() {
        Log.v("LOG_TAG", "onDestory");
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        registerInternetCheckReceiver();
    }

    private void registerInternetCheckReceiver() {
        IntentFilter internetFilter = new IntentFilter();
        internetFilter.addAction("android.net.wifi.STATE_CHANGE");
        internetFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        getActivity().registerReceiver(broadcastReceiver, internetFilter);
    }

    private void setSnackbarMessage(String status) {
        String internetStatus = "";
        Snackbar snackbar;
        if (status.equalsIgnoreCase("Wifi enabled") || status.equalsIgnoreCase("Mobile data enabled")) {
            internetStatus = "Connecting..";
        } else {
            internetStatus = "No Internet Connection";
        }
        if (internetStatus.equalsIgnoreCase("No Internet Connection")) {
            if (internetConnected) {
                snackbar = Snackbar.make(getActivity().findViewById(R.id.llMain), internetStatus, Snackbar.LENGTH_INDEFINITE);

                snackbar.setActionTextColor(Color.WHITE);
                View sbView = snackbar.getView();
                TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                sbView.setMinimumHeight(0);
                Typeface font_sfp_regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/roboto_light.ttf");
                textView.setTypeface(font_sfp_regular);
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(12);
                snackbar.show();
                internetConnected = false;
            }
        } else {
            if (!internetConnected) {
                getNewNoteList();

                internetConnected = true;
                snackbar = Snackbar.make(getActivity().findViewById(R.id.llMain), internetStatus, Snackbar.LENGTH_LONG);
                View sbView = snackbar.getView();
                TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                sbView.setMinimumHeight(0);
                Typeface font_sfp_regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/roboto_light.ttf");
                textView.setTypeface(font_sfp_regular);
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(12);

                snackbar.setActionTextColor(Color.WHITE);
                snackbar.show();
            }
        }
    }

}
