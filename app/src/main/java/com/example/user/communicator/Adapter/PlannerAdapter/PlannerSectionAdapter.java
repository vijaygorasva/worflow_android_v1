package com.example.user.communicator.Adapter.PlannerAdapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.user.communicator.Activity.PlannerModule.NoteDetailActivity;
import com.example.user.communicator.Activity.PlannerModule.PlannerTabActivity;
import com.example.user.communicator.ConnectivityReceiver;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Dialog.AllCategoryListDialog;
import com.example.user.communicator.Fragment.PlannerFragment;
import com.example.user.communicator.Model.Notes.AudioDetails;
import com.example.user.communicator.Model.Notes.ImgDetails;
import com.example.user.communicator.Model.Notes.Item;
import com.example.user.communicator.Model.Notes.TodoListItem;
import com.example.user.communicator.R;
import com.example.user.communicator.Utility.ItemClickListener;
import com.example.user.communicator.Utility.Utils;

import java.util.ArrayList;

import io.realm.RealmList;

import static com.bumptech.glide.Glide.with;

public class PlannerSectionAdapter extends RecyclerView.Adapter<PlannerSectionAdapter.SectionViewHolder> {//implements PlannerPinnedItemAdapter.Callback {

    public static final String EXTRA_ITEM_TRANSITION_NAME = "item_transition_name";
    OthersItemAdapter adapter;
    PinnedItemAdapter pinnedItemAdapter;
    SharedItemAdapter sharedItemAdapter;
    FragmentManager mContext;
    static int itemPosition;
    static int k = 0;
    StaggeredGridLayoutManager gridLayoutManager;
    Callback callback;

    public final String URL_ImageUpload_ROOT = "http://13.232.37.104:9999/uploads/";

    private Activity activity;
    public static int type = 0;
    private ArrayList<SectionModel> sectionModelArrayList = new ArrayList<>();
    AllCategoryListDialog allCategoryListDialog;
    SectionViewHolder sectionViewHolder;

    public PlannerSectionAdapter(Activity activity, FragmentManager context, ArrayList<SectionModel> sectionModelArrayList, Callback callback) {
        this.mContext = context;
        this.activity = activity;
        this.sectionModelArrayList = sectionModelArrayList;
        this.callback = callback;

    }

    @Override
    public SectionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View itemView = inflater.inflate(R.layout.section_custom_row_layout, parent, false);
//        ButterKnife.bind(this, itemView);
        return new SectionViewHolder(itemView);
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }


    @Override
    public void onBindViewHolder(SectionViewHolder holder, int position) {

        sectionViewHolder = holder;
        final SectionModel sectionModel = sectionModelArrayList.get(position);
        itemPosition = position;
        holder.itemRecyclerView.setHasFixedSize(true);
        holder.itemRecyclerView.setNestedScrollingEnabled(false);

        gridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        holder.itemRecyclerView.setLayoutManager(gridLayoutManager);
        holder.itemRecyclerView.setAnimation(null);

        holder.tvTypeCategory.setText(PlannerFragment.plannerName);
        if (!TextUtils.isEmpty(PlannerFragment.plannerNameTwo)) {
            holder.tvMainCategory.setVisibility(View.VISIBLE);
            holder.tvMainCategory.setText(PlannerFragment.plannerNameTwo);
        }

        if (!TextUtils.isEmpty(PlannerFragment.plannerNameThree)) {
            holder.tvSubCategory.setVisibility(View.VISIBLE);
            holder.tvSubCategory.setText(PlannerFragment.plannerNameThree);
        }

        String label = sectionModelArrayList.get(position).getSectionLabel();


        if (PlannerFragment.allNotes.size() == 0) {
            if (position == 0) {
                holder.llNodata.setVisibility(View.GONE);
                holder.llCatList.setVisibility(View.GONE);
                holder.itemRecyclerView.setVisibility(View.GONE);
                holder.sectionLabel.setVisibility(View.GONE);
            } else if (position == 1) {
                holder.llNodata.setVisibility(View.GONE);
                holder.llCatList.setVisibility(View.GONE);
                holder.itemRecyclerView.setVisibility(View.GONE);
                holder.sectionLabel.setVisibility(View.GONE);
            } else {
                holder.llCatList.setVisibility(View.VISIBLE);
                holder.llNodata.setVisibility(View.VISIBLE);
                holder.itemRecyclerView.setVisibility(View.GONE);
                holder.sectionLabel.setVisibility(View.GONE);
            }
        } else {
            holder.llNodata.setVisibility(View.GONE);

            if (position == 0) {//(label.equals("Pinned")) {
                if (sectionModelArrayList.get(0).getItemArrayList().size() > 0) {
                    holder.llCatList.setVisibility(View.VISIBLE);
                    holder.sectionLabel.setVisibility(View.VISIBLE);
                    holder.itemRecyclerView.setVisibility(View.VISIBLE);
                    holder.sectionLabel.setText(sectionModel.getSectionLabel());
                    type = 0;
                    pinnedItemAdapter = new PinnedItemAdapter(activity, sectionModelArrayList.get(0).getItemArrayList());//, this);//this);
                    holder.itemRecyclerView.setAdapter(pinnedItemAdapter);
                } else {
                    holder.llCatList.setVisibility(View.GONE);
                    holder.sectionLabel.setVisibility(View.GONE);
                    holder.itemRecyclerView.setVisibility(View.GONE);
                }
            } else if (position == 1) {//for shared

                if (sectionModelArrayList.get(0).getItemArrayList().size() > 0) {
                    holder.llCatList.setVisibility(View.GONE);
                } else {
                    holder.llCatList.setVisibility(View.VISIBLE);
                }
                if (sectionModelArrayList.get(1).getItemArrayList().size() > 0) {

                    holder.itemRecyclerView.setVisibility(View.VISIBLE);
                    holder.sectionLabel.setVisibility(View.VISIBLE);
                    holder.sectionLabel.setText(sectionModel.getSectionLabel());
                    type = 0;
                    sharedItemAdapter = new SharedItemAdapter(activity, sectionModelArrayList.get(1).getItemArrayList());//, this);//this);
                    holder.itemRecyclerView.setAdapter(sharedItemAdapter);
                } else {
                    holder.llCatList.setVisibility(View.GONE);
                    holder.sectionLabel.setVisibility(View.GONE);
                    holder.itemRecyclerView.setVisibility(View.GONE);
                }
            } else {//for others
                if (sectionModelArrayList.get(0).getItemArrayList().size() > 0) {
                    holder.llCatList.setVisibility(View.GONE);
                } else if (sectionModelArrayList.get(1).getItemArrayList().size() > 0) {
                    holder.llCatList.setVisibility(View.GONE);
                } else {
                    holder.llCatList.setVisibility(View.VISIBLE);
                }
                if (sectionModelArrayList.get(2).getItemArrayList().size() > 0) {
                    holder.sectionLabel.setVisibility(View.VISIBLE);
                    holder.itemRecyclerView.setVisibility(View.VISIBLE);
                    holder.sectionLabel.setText(sectionModel.getSectionLabel());
                    type = 1;
                    adapter = new OthersItemAdapter(activity, sectionModelArrayList.get(2).getItemArrayList());
                    holder.itemRecyclerView.setAdapter(adapter);
                } else if ((sectionModelArrayList.get(0).getItemArrayList().size() == 0)) {// && (sectionModelArrayList.get(1).getItemArrayList().size() == 0)
                    holder.llCatList.setVisibility(View.GONE);
                    holder.sectionLabel.setVisibility(View.GONE);
                    holder.itemRecyclerView.setVisibility(View.GONE);
                } else if ((sectionModelArrayList.get(0).getItemArrayList().size() > 0)) {//&& (sectionModelArrayList.get(1).getItemArrayList().size() == 0)
                    holder.llCatList.setVisibility(View.GONE);
                    holder.sectionLabel.setVisibility(View.GONE);
                    holder.itemRecyclerView.setVisibility(View.GONE);
                }
            }

        }

        holder.tvTypeCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.tvMainCategory.setVisibility(View.GONE);
                holder.tvSubCategory.setVisibility(View.GONE);
                callback.catChange(1);
            }
        });

        holder.tvMainCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.tvSubCategory.setVisibility(View.GONE);
                callback.catChange(2);
            }
        });

        holder.tvSubCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.catChange(3);
            }
        });

    }


    public interface Callback {

        void multiSelect(Activity activity, int Itempos, int listPos, String clickType);

        void catChange(int catPos);
    }

    public void dialogCall() {
        if (allCategoryListDialog != null) {
            allCategoryListDialog.dismiss();
        }
    }

    public void refreshAdapter() {
        if (itemPosition == 0) {
            pinnedItemAdapter.notifyDataSetChanged();
        } else if (itemPosition == 1) {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public int getItemCount() {
        return sectionModelArrayList.size();
    }

    public void removeData(Item item) {
        Boolean isPinned = item.getBlnPinNote();

        ArrayList<Item> allnotesPinned = new ArrayList<>();
        ArrayList<Item> allUnpinedNotes = new ArrayList<>();
        ArrayList<Item> allnotesShared = new ArrayList<>(sectionModelArrayList.get(1).getItemArrayList());

        if (sectionModelArrayList.size() == 1) {
            String type = sectionModelArrayList.get(0).getSectionLabel();
            if (type.equals("Pinned")) {
                allnotesPinned.addAll(sectionModelArrayList.get(0).getItemArrayList());
            } else {
                allUnpinedNotes.addAll(sectionModelArrayList.get(0).getItemArrayList());
            }
        } else {
            allnotesPinned.addAll(sectionModelArrayList.get(0).getItemArrayList());
            allUnpinedNotes.addAll(sectionModelArrayList.get(2).getItemArrayList());
        }

        if (isPinned) {
            for (int j = 0; j < allnotesPinned.size(); j++) {
                if (allnotesPinned.get(j).equals(item)) {
                    allnotesPinned.remove(j);
                }
            }
        } else {
            for (int j = 0; j < allUnpinedNotes.size(); j++) {
                if (allUnpinedNotes.get(j).equals(item)) {
                    allUnpinedNotes.remove(j);
                }
            }
        }
        sectionModelArrayList.clear();

        for (int i = 0; i < 3; i++) {
            if (i == 0) {
                sectionModelArrayList.add(new SectionModel("Pinned", allnotesPinned));
            } else if (i == 1) {
                sectionModelArrayList.add(new SectionModel("Shared", allnotesShared));
            } else {
                sectionModelArrayList.add(new SectionModel("Others", allUnpinedNotes));
            }
        }

    }

    public void restoreItem(Item item, int position) {
        int plannerId = item.getIntPlannerId();
        Boolean isPinned = item.getBlnPinNote();
        if (item.getArryCollaboratUserId().size() > 0) {
            int k = 0;
            for (int i = 0; i < sectionModelArrayList.get(1).getItemArrayList().size(); i++) {
                if (plannerId == sectionModelArrayList.get(1).getItemArrayList().get(i).getIntPlannerId()) {
                    k = 1;
                    sectionModelArrayList.get(1).getItemArrayList().remove(i);
                    sectionModelArrayList.get(1).getItemArrayList().add(position, item);
                    notifyDataSetChanged();
                    break;
                }
            }


            if (k == 0) {
                for (int i = 0; i < sectionModelArrayList.get(2).getItemArrayList().size(); i++) {
                    if (plannerId == sectionModelArrayList.get(2).getItemArrayList().get(i).getIntPlannerId()) {
                        sectionModelArrayList.get(2).getItemArrayList().remove(i);
                        notifyDataSetChanged();
                        break;
                    }
                }
                for (int i = 0; i < sectionModelArrayList.get(0).getItemArrayList().size(); i++) {
                    if (plannerId == sectionModelArrayList.get(0).getItemArrayList().get(i).getIntPlannerId()) {
                        sectionModelArrayList.get(0).getItemArrayList().remove(i);
                        notifyDataSetChanged();
                        break;
                    }
                }
                sectionModelArrayList.get(1).getItemArrayList().add(position, item);
                notifyDataSetChanged();
            }

        } else {

            if (isPinned) {
                int k = 0;
                for (int i = 0; i < sectionModelArrayList.get(0).getItemArrayList().size(); i++) {
                    if (plannerId == sectionModelArrayList.get(0).getItemArrayList().get(i).getIntPlannerId()) {
                        k = 1;
                        sectionModelArrayList.get(0).getItemArrayList().remove(i);
                        sectionModelArrayList.get(0).getItemArrayList().add(position, item);
                        notifyDataSetChanged();
                        break;
                    }
                }
                if (k == 0) {
                    for (int i = 0; i < sectionModelArrayList.get(2).getItemArrayList().size(); i++) {
                        if (plannerId == sectionModelArrayList.get(2).getItemArrayList().get(i).getIntPlannerId()) {
                            sectionModelArrayList.get(2).getItemArrayList().remove(i);
                            notifyDataSetChanged();
                            break;
                        }
                    }
                    for (int i = 0; i < sectionModelArrayList.get(1).getItemArrayList().size(); i++) {
                        if (plannerId == sectionModelArrayList.get(1).getItemArrayList().get(i).getIntPlannerId()) {
                            sectionModelArrayList.get(1).getItemArrayList().remove(i);
                            notifyDataSetChanged();
                            break;
                        }
                    }
//                    sectionModelArrayList.get(2).getItemArrayList().remove(position);
                    sectionModelArrayList.get(0).getItemArrayList().add(0, item);//sectionModelArrayList.get(0).getItemArrayList().size()
                    notifyDataSetChanged();
                }

            } else {
                int k = 0;
                /*check if that note was in others(it-self)*/
                for (int i = 0; i < sectionModelArrayList.get(2).getItemArrayList().size(); i++) {
                    if (plannerId == sectionModelArrayList.get(2).getItemArrayList().get(i).getIntPlannerId()) {
                        k = 1;
                        sectionModelArrayList.get(2).getItemArrayList().remove(i);
                        sectionModelArrayList.get(2).getItemArrayList().add(position, item);
                        notifyDataSetChanged();
                        break;
                    }
                }

                if (k == 0) {
                    /*check if that note was in shared or pinned*/
                    for (int i = 0; i < sectionModelArrayList.get(1).getItemArrayList().size(); i++) {
                        if (plannerId == sectionModelArrayList.get(1).getItemArrayList().get(i).getIntPlannerId()) {
                            sectionModelArrayList.get(1).getItemArrayList().remove(i);
                            notifyDataSetChanged();
                            break;
                        }
                    }
                    for (int i = 0; i < sectionModelArrayList.get(0).getItemArrayList().size(); i++) {
                        if (plannerId == sectionModelArrayList.get(0).getItemArrayList().get(i).getIntPlannerId()) {
                            sectionModelArrayList.get(0).getItemArrayList().remove(i);
                            notifyDataSetChanged();
                            break;
                        }
                    }
                    sectionModelArrayList.get(2).getItemArrayList().add(0, item);
                    notifyDataSetChanged();
                }

            }
        }
    }


    public void restoreSearchItem(Item item, int position) {
        adapter = new OthersItemAdapter(activity, sectionModelArrayList.get(0).getItemArrayList());
        adapter.restoreItem(item, position);
        notifyDataSetChanged();
    }

    public void removeItem(Item item) {
        Boolean isPinned = item.getBlnPinNote();
        int plannerId = item.getIntPlannerId();

        if (item.getArryCollaboratUserId().size() > 0) {
            for (int i = 0; i < sectionModelArrayList.get(1).getItemArrayList().size(); i++) {
                if (plannerId == sectionModelArrayList.get(1).getItemArrayList().get(i).getIntPlannerId()) {
                    sectionModelArrayList.get(1).getItemArrayList().remove(i);
                    break;
                }
            }
        } else {
            if (isPinned != null && isPinned) {

                for (int i = 0; i < sectionModelArrayList.get(0).getItemArrayList().size(); i++) {
                    if (plannerId == sectionModelArrayList.get(0).getItemArrayList().get(i).getIntPlannerId()) {
                        sectionModelArrayList.get(0).getItemArrayList().remove(i);
                    }
                }

            } else {
                for (int i = 0; i < sectionModelArrayList.get(2).getItemArrayList().size(); i++) {
                    if (plannerId == sectionModelArrayList.get(2).getItemArrayList().get(i).getIntPlannerId()) {
                        sectionModelArrayList.get(2).getItemArrayList().remove(i);
                    }
                }
            }
        }
    }

    public void removeItem(int position) {

        notifyDataSetChanged();
        if (itemPosition == 0) {
            pinnedItemAdapter.notifyItemRemoved(position);
        } else if (itemPosition == 1) {
            sharedItemAdapter.notifyItemRemoved(position);
        } else {
            adapter.notifyItemRemoved(position);
        }
    }

    public void insertItems(Item item, int position) {
        Boolean isPinned = item.getBlnPinNote();
        RealmList<String> collobarateUseId = item.getArryCollaboratUserId();
        if (collobarateUseId.size() > 0) {
            sectionModelArrayList.get(1).getItemArrayList().add(position, item);
            notifyDataSetChanged();
        } else {
            if (isPinned) {
                sectionModelArrayList.get(0).getItemArrayList().add(position, item);
                notifyDataSetChanged();
            } else {
                sectionModelArrayList.get(2).getItemArrayList().add(position, item);
                notifyDataSetChanged();
            }
        }
    }

    public class SectionViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout rlMain;
        LinearLayout llNodata, flMainContent, llCatList;
        //        private final View binding;
        private CustomTextView sectionLabel, tvTypeCategory, tvMainCategory, tvSubCategory;
        private RecyclerView itemRecyclerView;

        public SectionViewHolder(View itemView) {
            super(itemView);
            tvTypeCategory = itemView.findViewById(R.id.tvTypeCategory);
            tvMainCategory = itemView.findViewById(R.id.tvMainCategory);
            tvSubCategory = itemView.findViewById(R.id.tvSubCategory);
            sectionLabel = itemView.findViewById(R.id.section_label);
            rlMain = itemView.findViewById(R.id.rlMain);
            flMainContent = itemView.findViewById(R.id.flMainContent);
            itemRecyclerView = itemView.findViewById(R.id.item_recycler_view);
            llNodata = itemView.findViewById(R.id.llNodata);
            llCatList = itemView.findViewById(R.id.llCatList);
        }

    }


    public void showToast(String message) {

        LayoutInflater inflater = activity.getLayoutInflater();
        View layouttoast = inflater.inflate(R.layout.toastcustom, (ViewGroup) activity.findViewById(R.id.toastcustom));
        ((TextView) layouttoast.findViewById(R.id.texttoast)).setText(message);

        Toast mytoast = new Toast(activity);
        mytoast.setView(layouttoast);
        mytoast.setGravity(Gravity.CENTER, 0, 0);
        mytoast.setDuration(Toast.LENGTH_LONG);
        mytoast.show();
    }

    public class PinnedItemAdapter extends RecyclerView.Adapter<PinnedItemAdapter.ItemViewHolder> {
        public static final String EXTRA_ITEM_TRANSITION_NAME = "item_transition_name";

        ViewGroup relativeLayout;
        private Activity mActivity;
        private ArrayList<Item> arraylist;
        String defaullt = "Default";

        public PinnedItemAdapter(Activity mActivity, ArrayList<Item> mDataset) {//, PlannerPinnedItemAdapter.Callback callback
            this.mActivity = mActivity;
            this.arraylist = new ArrayList<Item>();
            this.arraylist.addAll(mDataset);
//            this.callback = callback;
        }

        @Override
        public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_notes_list, parent, false);
            return new ItemViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ItemViewHolder holder, int position1) {

            holder.llImages.setVisibility(View.GONE);
            holder.llImages.removeAllViews();
            Item items = arraylist.get(position1);

            relativeLayout = (ViewGroup) View.inflate(mActivity, R.layout.list_item_colleborate_list_users, null);
            int imageHeight, imageWidth;

            if (TextUtils.isEmpty(PlannerTabActivity.strCategoryTypeId) && (TextUtils.isEmpty(PlannerTabActivity.strCategoryId) && (TextUtils.isEmpty(PlannerTabActivity.strSubCategoryId)))) {
                holder.tvLabel.setText(items.getStrCategoryTypeName());
                holder.tvLabel.setVisibility(View.VISIBLE);
            } else if (!TextUtils.isEmpty(PlannerTabActivity.strCategoryTypeId)) {
                holder.tvLabel.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(PlannerTabActivity.strSubCategoryId)) {
                    holder.tvLabel.setVisibility(View.GONE);
                    holder.tvLabel.setText(items.getStrCategoryTypeName());
                } else if (!TextUtils.isEmpty(PlannerTabActivity.strCategoryId)) {
                    String subCat = items.getStrDefaultSubName();
                    if (TextUtils.isEmpty(subCat)) {
                        holder.tvLabel.setText(defaullt.toUpperCase());
                    } else {
                        holder.tvLabel.setText(items.getStrDefaultSubName());
                    }
                } else {
                    holder.tvLabel.setText(items.getStrCategoryTypeName());
                }
            } else {
                holder.tvLabel.setVisibility(View.GONE);
            }

            Typeface font4 = Typeface.createFromAsset(mActivity.getAssets(), "fonts/roboto_medium.ttf");
            holder.tvLabel.setTypeface(font4);

            String noteValue = items.getStrValue();
            String title = items.getStrTitle();

            if (title.length() != 0) {
                holder.tvTitle.setVisibility(View.VISIBLE);
                holder.tvTitle.setText(items.getIntPlannerId() + "");
            } else {
                if (noteValue.length() > 10) {
                    holder.tvTitle.setVisibility(View.INVISIBLE);
                } else {
                    holder.tvTitle.setVisibility(View.INVISIBLE);//GONE
                }
            }

            Typeface font1 = Typeface.createFromAsset(mActivity.getAssets(), "fonts/roboto_regular.ttf");
            holder.tvTitle.setTypeface(font1);

            ArrayList<TodoListItem> arrayToDoList = new ArrayList<>(items.getArryObjstrTodoList());
            ArrayList<String> toSingleStringList = new ArrayList<>();

            if (noteValue.length() > 0) {

                String val = items.getStrValue();
                String newVal = val.trim();
                String trimmedString = "";//= val.trim();
                holder.tvText.setVisibility(View.VISIBLE);
                holder.rlCheckbox.setVisibility(View.GONE);
                holder.tvText.applyCustomFont(mActivity, mActivity.getResources().getString(R.string.roboto_light));
                String[] text = noteValue.split("\\r?\\n");
                for (String line : text) {

                    toSingleStringList.add(line);
                }

                int size = toSingleStringList.size();
                if (toSingleStringList.size() >= 7) {
                    for (int i = size - 1; i > 7; i--) {
                        toSingleStringList.remove(i);
                    }
                }

                if (toSingleStringList.get(0).length() > 80) {// noteValue.length()
                    holder.tvText.setText(toSingleStringList.get(0).substring(0, 80));
                } else {
                    for (String s : toSingleStringList) {
                        trimmedString += s + "\n";
                    }
                }

                holder.tvText.setText(trimmedString);

                if (noteValue.length() < 14) {
                    holder.tvText.setTextSize(20);
                } else {
                    holder.tvText.setTextSize(13);
                }

            } else if ((arrayToDoList.size() != 0)) {

                holder.tvText.setVisibility(View.GONE);
                holder.rlCheckbox.setVisibility(View.VISIBLE);
                holder.rlCheckbox.removeAllViews();
                int size = arrayToDoList.size();
                if (arrayToDoList.size() >= 6) {
                    try {
                        if (items.getImgNoteList().size() != 0) {
                            size = 3;
                        } else {
                            size = 6;
                        }
                    } catch (NullPointerException e) {
                        Log.e("NullPoineter", e + "...in image");
                    }

                } else {
                    size = arrayToDoList.size();
                }
                holder.tvText.setVisibility(View.GONE);
                holder.rlCheckbox.setVisibility(View.VISIBLE);
                holder.rlCheckbox.removeAllViews();

                for (int i = 0; i < size; i++) {

                    Boolean checked = arrayToDoList.get(i).getStrNoteListFlage();
                    CheckBox checkBox = new CheckBox(mActivity);
                    checkBox.setClickable(false);
                    if (checked) {
                        checkBox.setChecked(true);
                    } else {
                        checkBox.setChecked(false);
                    }
                    Typeface font = Typeface.createFromAsset(mActivity.getAssets(), "fonts/roboto_light.ttf");
                    checkBox.setTypeface(font);
                    int checkboxValLength = arrayToDoList.get(i).getStrNoteListValue().length();
                    if (checkboxValLength > 50) {
                        char[] val = arrayToDoList.get(i).getStrNoteListValue().toCharArray();
                        checkBox.setText(val, 0, 50);
                    } else {
                        checkBox.setText(arrayToDoList.get(i).getStrNoteListValue());
                    }
                    checkBox.setTextSize(13);
                    checkBox.setTextColor(mActivity.getResources().getColor(R.color.black));//text_note
                    checkBox.setPadding(10, 10, 10, 10);
                    checkBox.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    holder.rlCheckbox.addView(checkBox);

                }

            } else {

                holder.tvText.setVisibility(View.GONE);
                holder.rlCheckbox.setVisibility(View.GONE);

            }
            ArrayList<ImgDetails> arrayisImage = new ArrayList<>(items.getImgNoteList());
//            String isImage = items.getImgNote().getFilename();//error
            if (arrayisImage.size() == 0) {

                holder.ivMultiImage.setVisibility(View.GONE);
                holder.ivImage.setVisibility(View.GONE);
                holder.ivImage.setImageDrawable(null);

            } else {

                if (arrayisImage.size() >= 2) {
                    holder.ivMultiImage.setVisibility(View.VISIBLE);
                } else holder.ivMultiImage.setVisibility(View.GONE);

                imageHeight = arrayisImage.get(0).getIntHeight();
                imageWidth = arrayisImage.get(0).getIntWeight();
                holder.ivImage.setVisibility(View.VISIBLE);

                String imagePath = URL_ImageUpload_ROOT + items.getImgNoteList().get(0).getFilename();

                Display display = mActivity.getWindowManager().getDefaultDisplay();
                int phnWidth = Utils.getScreenWidth(mActivity) / 2;
                double height = ((imageHeight * phnWidth) / imageWidth);
                int pHeightNew = ((display.getHeight() * 70) / 100);

                if (height >= pHeightNew) {
                    height = ((display.getHeight() * 50) / 100);
                } else if (height == 0) {
                    height = ((display.getHeight() * 40) / 100);
                }
                if (height > 1000) {
                    height = ((height * 80) / 100);
                }
                Glide.with(mActivity)
                        .load(imagePath)
                        .dontAnimate()
//                            .override(imageWidth, imageHeight) // resizes the image to these dimensions (in pixel)
                        .into(holder.ivImage);

                ViewGroup.LayoutParams params = holder.ivImage.getLayoutParams();
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                params.height = (int) height;
                holder.ivImage.setLayoutParams(params);

            }

            RealmList<AudioDetails> audioDetails = new RealmList<AudioDetails>();
            audioDetails.addAll(items.getFileAudioList());
//            String isAudio = items.getFileAudio().getFilename();//error
            if ((title.length() == 0) && (noteValue.length() == 0) && (items.getArryObjstrTodoList().size() == 0) && (arrayisImage.size() == 0) && (audioDetails.size() == 0) && (items.getArryCollaboratUserId().size() != 0)) {
                if ((items.getArryCollaboratUserId().size() != 0)) {
                    holder.llImages.setVisibility(View.VISIBLE);
                    holder.llImages.removeAllViews();
                    int totCount = items.getArryCollaboratUserId().size();

                    FrameLayout frMain = relativeLayout.findViewById(R.id.frMain);
                    ImageView cvImage1 = relativeLayout.findViewById(R.id.cvImage1);
                    ImageView cvImage2 = relativeLayout.findViewById(R.id.cvImage2);
                    ImageView cvImage3 = relativeLayout.findViewById(R.id.cvImage3);

                    TextView text = relativeLayout.findViewById(R.id.text);
                    Typeface font3 = Typeface.createFromAsset(mActivity.getAssets(), "fonts/roboto_light.ttf");
                    text.setTypeface(font3);

                    cvImage1.setVisibility(View.VISIBLE);
                    with(mActivity)
                            .load(R.drawable.ic_share)
                            .dontAnimate()
                            .into(cvImage1);

                    holder.llImages.addView(relativeLayout);
                }
            } else if ((title.length() == 0) && (noteValue.length() == 0) && (items.getArryObjstrTodoList().size() == 0) && (arrayisImage.size() == 0) && (items.getArryCollaboratUserId().size() == 0) && (audioDetails.size() != 0)) {
                Typeface font2 = Typeface.createFromAsset(mActivity.getAssets(), "fonts/roboto_light.ttf");
                holder.flAudio.setVisibility(View.VISIBLE);
                holder.tvAudio.setTypeface(font2);
            } else {
                holder.flAudio.setVisibility(View.GONE);//Condition if all note fields are empty
            }


            holder.setClickListener(new ItemClickListener() {
                @Override
                public void onClick(View view, int position, boolean isLongClick) {
                    if (isLongClick) {
                        callback.multiSelect(activity, position, 0, "OnLongClick");
                        holder.ivImage.setAlpha(0.5f);
                        holder.llItemClickDelete.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.list_item_selected_state));
                    } else {
                        if (PlannerFragment.selectionList.size() == 0) {
                            Item items1 = arraylist.get(position1);
                            RealmList<ImgDetails> imgDetails = items1.getImgNoteList();
                            int REQUEST_CODE = 1;

                            boolean isConnected = ConnectivityReceiver.isConnected();

                            Intent intent = new Intent(mActivity, NoteDetailActivity.class);//NoteDetailActivity NoteDummyActivity
                            intent.putExtra("type", "activity");
                            intent.putExtra("position", position1);
                            intent.putExtra("plannerId", String.valueOf(items1.getIntPlannerId()));
                            intent.putExtra("status", isConnected);

                            if (imgDetails.size() != 0) {
                                ViewCompat.setTransitionName(holder.llImgs, "Item");
                                intent.putExtra("isImage", "yes");
                                intent.putExtra(EXTRA_ITEM_TRANSITION_NAME, ViewCompat.getTransitionName(holder.llImgs));
                                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                                        mActivity,
                                        holder.llImgs,
                                        ViewCompat.getTransitionName(holder.llImgs));
                                mActivity.startActivityForResult(intent, REQUEST_CODE, options.toBundle());
                            } else {
                                intent.putExtra("isImage", "no");
                                ViewCompat.setTransitionName(holder.rlMain, "Item");
                                intent.putExtra(EXTRA_ITEM_TRANSITION_NAME, ViewCompat.getTransitionName(holder.rlMain));
                                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                                        mActivity,
                                        holder.rlMain,
                                        ViewCompat.getTransitionName(holder.rlMain));
                                mActivity.startActivityForResult(intent, REQUEST_CODE, options.toBundle());
                            }

                        } else {

                            callback.multiSelect(activity, position, 0, "OnClick");
//                            callback.editVal(mActivity, position, 0, "OnClick");
                            if (PlannerFragment.selectionList.contains(arraylist.get(position))) {
                                holder.ivImage.setAlpha(0.5f);
                                holder.llItemClickDelete.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.list_item_selected_state));
                            } else {
                                holder.ivImage.setAlpha(1f);
                                holder.llItemClickDelete.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.list_item_normal_state));
                            }
                        }

                    }

                }
            });

        }

        public void restoreItem(Item item, int position) {//restorePinnedItem
            arraylist.remove(position);
            arraylist.add(position, item);
            notifyItemChanged(position);
//            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            return arraylist.size();
        }

        class ItemViewHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener, View.OnLongClickListener {
            CustomTextView tvTitle;
            RelativeLayout rlMain;
            ImageView ivImage, ivMultiImage;
            LinearLayout rlCheckbox;
            CardView cvTasks;
            LinearLayout llImages;
            FrameLayout flAudio;
            TextView tvAudio;
            TextView tvLabel;
            CustomTextView tvText;
            LinearLayout llItemClickDelete;
            FrameLayout llImgs;
            private ItemClickListener clickListener;

            public ItemViewHolder(View itemView) {
                super(itemView);
                this.tvTitle = itemView.findViewById(R.id.tvTitle);
                this.rlMain = itemView.findViewById(R.id.rlMain);
                this.ivImage = itemView.findViewById(R.id.ivImage);
                this.ivMultiImage = itemView.findViewById(R.id.ivMultiImage);
                this.rlCheckbox = itemView.findViewById(R.id.rlCheckbox);
                this.cvTasks = itemView.findViewById(R.id.cvTasks);
                this.llImages = itemView.findViewById(R.id.llImages);
                this.flAudio = itemView.findViewById(R.id.flAudio);
                this.tvAudio = itemView.findViewById(R.id.tvAudio);
                this.tvLabel = itemView.findViewById(R.id.tvLabel);
                this.tvText = itemView.findViewById(R.id.tvText);

                this.llItemClickDelete = itemView.findViewById(R.id.llItemClickDelete);
                this.llImgs = itemView.findViewById(R.id.llImgs);

                itemView.setTag(itemView);
                itemView.setOnClickListener(this);
                itemView.setOnLongClickListener(this);
            }

            public void setClickListener(ItemClickListener itemClickListener) {
                this.clickListener = itemClickListener;
            }

            @Override
            public void onClick(View view) {
                clickListener.onClick(view, getPosition(), false);
            }

            @Override
            public boolean onLongClick(View view) {
                clickListener.onClick(view, getPosition(), true);
                return true;
            }


        }
    }

    public class OthersItemAdapter extends RecyclerView.Adapter<OthersItemAdapter.ItemViewHolder> {
        public static final String EXTRA_ITEM_TRANSITION_NAME = "item_transition_name";

        ViewGroup relativeLayout;
        Context context;
        private Activity mActivity;
        private ArrayList<Item> arraylist;
        Point size;
        float density;

        String defaullt = "Default";

        public OthersItemAdapter(Activity mActivity, ArrayList<Item> mDataset) {//, PlannerItemAdapter.Callback callback) {

            this.mActivity = mActivity;
            this.context = mActivity;
            this.arraylist = new ArrayList<Item>();
            this.arraylist.addAll(mDataset);

        }

        @Override
        public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_notes_list, parent, false);
            return new ItemViewHolder(view);
        }

        @Override
        public int getItemCount() {
            return arraylist.size();
        }

        @Override
        public void onBindViewHolder(ItemViewHolder holder, int position) {

            holder.llImages.setVisibility(View.GONE);
            holder.llImages.removeAllViews();
            Item items = arraylist.get(position);

            Display display1 = mActivity.getWindowManager().getDefaultDisplay();
            size = new Point();
            DisplayMetrics dm = new DisplayMetrics();
            display1.getMetrics(dm);
            density = dm.density;
            display1.getSize(size);

            relativeLayout = (ViewGroup) View.inflate(mActivity, R.layout.list_item_colleborate_list_users, null);
            int imageHeight, imageWidth;

            if (TextUtils.isEmpty(PlannerTabActivity.strCategoryTypeId) && (TextUtils.isEmpty(PlannerTabActivity.strCategoryId) && (TextUtils.isEmpty(PlannerTabActivity.strSubCategoryId)))) {
                holder.tvLabel.setText(items.getStrCategoryTypeName());
                holder.tvLabel.setVisibility(View.VISIBLE);
            } else if (!TextUtils.isEmpty(PlannerTabActivity.strCategoryTypeId)) {
                holder.tvLabel.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(PlannerTabActivity.strSubCategoryId)) {
                    holder.tvLabel.setVisibility(View.GONE);
                    holder.tvLabel.setText(items.getStrCategoryTypeName());
                } else if (!TextUtils.isEmpty(PlannerTabActivity.strCategoryId)) {
                    String subCat = items.getStrDefaultSubName();
                    if (TextUtils.isEmpty(subCat)) {
                        holder.tvLabel.setText(defaullt.toUpperCase());
                    } else {
                        holder.tvLabel.setText(items.getStrDefaultSubName());
                    }
                } else {
                    holder.tvLabel.setText(items.getStrCategoryTypeName());
                }
            } else {
                holder.tvLabel.setVisibility(View.GONE);
            }

            Typeface font4 = Typeface.createFromAsset(mActivity.getAssets(), "fonts/roboto_medium.ttf");
            holder.tvLabel.setTypeface(font4);

            if (PlannerFragment.selectionList.contains(arraylist.get(position))) {
                holder.ivImage.setAlpha(0.5f);
                holder.llItemClickDelete.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.list_item_selected_state));
            } else {
                holder.ivImage.setAlpha(1f);
                holder.llItemClickDelete.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.list_item_normal_state));
            }

            String noteValue = items.getStrValue();
            String title = items.getStrTitle();

            if (title.length() != 0) {
                holder.tvTitle.setVisibility(View.VISIBLE);
                holder.tvTitle.setText(items.getIntPlannerId() + "");
            } else {
                if (noteValue.length() > 10) {
                    holder.tvTitle.setVisibility(View.INVISIBLE);
                } else {
                    holder.tvTitle.setVisibility(View.INVISIBLE);//GONE
                }
            }
            Typeface font = Typeface.createFromAsset(mActivity.getAssets(), "fonts/roboto_regular.ttf");
            holder.tvTitle.setTypeface(font);
            ArrayList<TodoListItem> arrayToDoList = new ArrayList<>(items.getArryObjstrTodoList());
            ArrayList<String> toSingleStringList = new ArrayList<>();

            try {
                if (noteValue.length() > 0) {

                    String val = items.getStrValue();
                    String newVal = val.trim();
                    String trimmedString = "";//= val.trim();
                    holder.tvText.setVisibility(View.VISIBLE);
                    holder.rlCheckbox.setVisibility(View.GONE);
                    holder.tvText.applyCustomFont(mActivity, mActivity.getResources().getString(R.string.roboto_light));
                    String[] text = noteValue.split("\\r?\\n");
                    for (String line : text) {
                        toSingleStringList.add(line);
                    }

                    int size = toSingleStringList.size();
                    if (toSingleStringList.size() >= 7) {
                        for (int i = size - 1; i > 7; i--) {
                            toSingleStringList.remove(i);
                        }
                    }

                    // Log.e("note",toSingleStringList.size()+"....");
                    // Log.e("note_val",toSingleStringList.get(0)+"....");
                    try {
                        if (toSingleStringList.size() != 0 && toSingleStringList.get(0).length() > 80) {// noteValue.length()
                            holder.tvText.setText(toSingleStringList.get(0).substring(0, 80));
                        } else {
                            for (String s : toSingleStringList) {
                                trimmedString += s + "\n";
                            }
                            holder.tvText.setText(trimmedString);
                        }

                        if (noteValue.length() <= 15) {
                            holder.tvText.setTextSize(20);
                        } else {
                            holder.tvText.setTextSize(13);
                        }

                    } catch (NullPointerException e) {
                        Log.e("NullPointer", e + "..");
                    }

                } else if ((arrayToDoList.size() != 0)) {
                    int size = arrayToDoList.size();
                    if (arrayToDoList.size() >= 6) {
                        try {
                            if (items.getImgNoteList().size() != 0) {
                                size = 3;
                            } else {
                                size = 6;
                            }
                        } catch (NullPointerException e) {
                            Log.e("NullPoineter", e + "...in image");
                        }

                    } else {
                        size = arrayToDoList.size();
                    }
                    holder.tvText.setVisibility(View.GONE);
                    holder.rlCheckbox.setVisibility(View.VISIBLE);
                    holder.rlCheckbox.removeAllViews();

                    for (int i = 0; i < size; i++) {
                        CheckBox checkBox = new CheckBox(mActivity);
                        Boolean checked = arrayToDoList.get(i).getStrNoteListFlage();
                        checkBox.setClickable(false);
                        if (checked) {
                            checkBox.setChecked(true);
                        } else {
                            checkBox.setChecked(false);
                        }

                        int checkboxValLength = arrayToDoList.get(i).getStrNoteListValue().length();
                        if (checkboxValLength > 50) {
                            char[] val = arrayToDoList.get(i).getStrNoteListValue().toCharArray();
                            checkBox.setText(val, 0, 50);
                        } else {
                            checkBox.setText(arrayToDoList.get(i).getStrNoteListValue());
                        }

                        Typeface font1 = Typeface.createFromAsset(mActivity.getAssets(), "fonts/roboto_light.ttf");
                        checkBox.setTypeface(font1);
                        checkBox.setTextSize(13);
                        checkBox.setGravity(0);
                        checkBox.setTextColor(ContextCompat.getColor(mActivity, R.color.text_color));
//                    checkBox.setTextAppearance(mActivity, R.style.Roboto_light);

                        checkBox.setPadding(0, 10, 5, 10);
                        checkBox.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                        holder.rlCheckbox.addView(checkBox);
                    }

                } else {
                    holder.tvText.setVisibility(View.GONE);
                    holder.rlCheckbox.setVisibility(View.GONE);
                }
            } catch (NullPointerException e) {
                Log.e("NullPoineter", e + "...in notevale");
            }

            try {
//                String isImage = items.getImgNoteList().get(0).getFilename();//error
                ArrayList<ImgDetails> arrayisImage = new ArrayList<>(items.getImgNoteList());
                if (arrayisImage.size() == 0) {

                    holder.ivMultiImage.setVisibility(View.GONE);
                    holder.ivImage.setVisibility(View.GONE);
                    holder.ivImage.setImageDrawable(null);

                } else {
                    if (arrayisImage.size() >= 2) {
                        holder.ivMultiImage.setVisibility(View.VISIBLE);
                    } else holder.ivMultiImage.setVisibility(View.GONE);

                    imageHeight = arrayisImage.get(0).getIntHeight();
                    imageWidth = arrayisImage.get(0).getIntWeight();
                    holder.ivImage.setVisibility(View.VISIBLE);
                    String imagePath = URL_ImageUpload_ROOT + items.getImgNoteList().get(0).getFilename();

                    Display display = mActivity.getWindowManager().getDefaultDisplay();
                    int phnWidth = Utils.getScreenWidth(mActivity) / 2;
                    double height = ((imageHeight * phnWidth) / imageWidth);
                    int pHeightNew = ((display.getHeight() * 70) / 100);

                    if (height >= pHeightNew) {
                        height = ((display.getHeight() * 50) / 100);
                    } else if (height == 0) {
                        height = ((display.getHeight() * 40) / 100);
                    }

                    if (height > 1000) {

                        height = ((height * 80) / 100);
                    }

                    Glide.with(mActivity)
                            .load(imagePath)
                            .dontAnimate()
//                            .override(imageWidth, imageHeight) // resizes the image to these dimensions (in pixel)
                            .into(holder.ivImage);

                    ViewGroup.LayoutParams params = holder.ivImage.getLayoutParams();
                    params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                    params.height = (int) height;
                    holder.ivImage.setLayoutParams(params);

                }
            } catch (NullPointerException e) {
                Log.e("NullPoineter", e + "...in image");
            }
            holder.llImages.removeAllViews();

            try {
                RealmList<ImgDetails> imgDetails = new RealmList<>();
                imgDetails.addAll(items.getImgNoteList());

                RealmList<AudioDetails> audioDetails = new RealmList<AudioDetails>();
                audioDetails.addAll(items.getFileAudioList());
                if ((title.length() == 0) && (noteValue.length() == 0) && (items.getArryObjstrTodoList().size() == 0) && (imgDetails.size() == 0) && (audioDetails.size() == 0) && (items.getArryCollaboratUserId().size() != 0)) {
                    if ((items.getArryCollaboratUserId().size() != 0)) {
                        holder.llImages.setVisibility(View.VISIBLE);
                        holder.llImages.removeAllViews();
                        int totCount = items.getArryCollaboratUserId().size();

                        FrameLayout frMain = relativeLayout.findViewById(R.id.frMain);
                        ImageView cvImage1 = relativeLayout.findViewById(R.id.cvImage1);
                        ImageView cvImage2 = relativeLayout.findViewById(R.id.cvImage2);
                        ImageView cvImage3 = relativeLayout.findViewById(R.id.cvImage3);
                        TextView text = relativeLayout.findViewById(R.id.text);
                        Typeface font3 = Typeface.createFromAsset(mActivity.getAssets(), "fonts/roboto_light.ttf");
                        text.setTypeface(font3);

                        cvImage1.setVisibility(View.VISIBLE);
                        with(mActivity)
                                .load(R.drawable.ic_share)
                                .dontAnimate()
                                .into(cvImage1);

                        holder.llImages.addView(relativeLayout);
                    }
                } else if ((title.length() == 0) && (noteValue.length() == 0) && (items.getArryObjstrTodoList().size() == 0) && (imgDetails.size() == 0) && (items.getArryCollaboratUserId().size() == 0) && (audioDetails.size() != 0)) {
                    holder.flAudio.setVisibility(View.VISIBLE);
                    Typeface font2 = Typeface.createFromAsset(mActivity.getAssets(), "fonts/roboto_light.ttf");
                    holder.tvAudio.setTextAppearance(mActivity, R.style.Roboto_light);
                } else {
                    holder.flAudio.setVisibility(View.GONE);//Condition if all note fields are empty
                }

            } catch (NullPointerException e) {
                Log.e("NullPoineter", e + "...in audio");
            }

            try {
                RealmList<ImgDetails> imgDetails = new RealmList<>();
                imgDetails.addAll(items.getImgNoteList());

                RealmList<AudioDetails> audioDetails = new RealmList<AudioDetails>();
                audioDetails.addAll(items.getFileAudioList());
                if ((title.length() == 0) && (noteValue.length() == 0) && (items.getArryObjstrTodoList().size() == 0) && (imgDetails.size() == 0) && (audioDetails.size() == 0) && (items.getArryCollaboratUserId().size() != 0)) {
                    if ((items.getArryCollaboratUserId().size() != 0)) {
                        holder.llImages.setVisibility(View.VISIBLE);
                        holder.llImages.removeAllViews();
                        int totCount = items.getArryCollaboratUserId().size();

                        FrameLayout frMain = relativeLayout.findViewById(R.id.frMain);
                        ImageView cvImage1 = relativeLayout.findViewById(R.id.cvImage1);
                        ImageView cvImage2 = relativeLayout.findViewById(R.id.cvImage2);
                        ImageView cvImage3 = relativeLayout.findViewById(R.id.cvImage3);
                        TextView text = relativeLayout.findViewById(R.id.text);
                        Typeface font3 = Typeface.createFromAsset(mActivity.getAssets(), "fonts/roboto_light.ttf");
                        text.setTypeface(font3);

                        cvImage1.setVisibility(View.VISIBLE);
                        with(mActivity)
                                .load(R.drawable.ic_share)
                                .dontAnimate()
                                .into(cvImage1);

                        holder.llImages.addView(relativeLayout);
                    }
                } else if ((title.length() == 0) && (noteValue.length() == 0) && (items.getArryObjstrTodoList().size() == 0) && (imgDetails.size() == 0) && (items.getArryCollaboratUserId().size() == 0) && (audioDetails.size() != 0)) {
                    holder.flAudio.setVisibility(View.VISIBLE);
                    Typeface font2 = Typeface.createFromAsset(mActivity.getAssets(), "fonts/roboto_light.ttf");
                    holder.tvAudio.setTextAppearance(mActivity, R.style.Roboto_light);
                } else {
                    holder.flAudio.setVisibility(View.GONE);//Condition if all note fields are empty
                }

            } catch (NullPointerException e) {
                Log.e("NullPoineter", e + "...in audio");
            }

            holder.setClickListener(new ItemClickListener() {
                @Override
                public void onClick(View view, int position, boolean isLongClick) {
                    if (isLongClick) {
                        callback.multiSelect(activity, position, 2, "OnLongClick");
                        holder.ivImage.setAlpha(0.5f);
                        holder.llItemClickDelete.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.list_item_selected_state));
                    } else {
                        if (PlannerFragment.selectionList.size() == 0) {
                            Item items1 = arraylist.get(position);
                            RealmList<ImgDetails> imgDetails = items1.getImgNoteList();
                            int REQUEST_CODE = 1;

                            Intent intent = new Intent(mActivity, NoteDetailActivity.class);
                            intent.putExtra("type", "activity");
                            intent.putExtra("position", position);
                            intent.putExtra("plannerId", String.valueOf(items1.getIntPlannerId()));

                            if (imgDetails.size() != 0) {
                                ViewCompat.setTransitionName(holder.llImgs, "Item");
                                intent.putExtra("isImage", "yes");
                                intent.putExtra(EXTRA_ITEM_TRANSITION_NAME, ViewCompat.getTransitionName(holder.llImgs));
                                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                                        mActivity,
                                        holder.llImgs,
                                        ViewCompat.getTransitionName(holder.llImgs));
                                mActivity.startActivityForResult(intent, REQUEST_CODE, options.toBundle());
                            } else {
                                ViewCompat.setTransitionName(holder.rlMain, "Item");//DetailActivtiy
                                intent.putExtra("isImage", "no");
                                intent.putExtra(EXTRA_ITEM_TRANSITION_NAME, ViewCompat.getTransitionName(holder.rlMain));
                                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                                        mActivity,
                                        holder.rlMain,
                                        ViewCompat.getTransitionName(holder.rlMain));
                                mActivity.startActivityForResult(intent, REQUEST_CODE, options.toBundle());
                            }

                        } else {
                            callback.multiSelect(activity, position, 2, "OnClick");
//                            callback.editVal(mActivity, position, 1, "OnClick");
                            if (PlannerFragment.selectionList.contains(arraylist.get(position))) {
                                holder.ivImage.setAlpha(0.5f);
                                holder.llItemClickDelete.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.list_item_selected_state));
                            } else {
                                holder.ivImage.setAlpha(1f);
                                holder.llItemClickDelete.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.list_item_normal_state));
                            }
                        }

                    }
                }
            });

        }


        public void restoreItem(Item item, int position) {//restoreOthersItem
            arraylist.remove(position);
            arraylist.add(position, item);
            notifyItemChanged(position);
//            notifyDataSetChanged();
        }


        class ItemViewHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener, View.OnLongClickListener {
            CustomTextView tvTitle;
            RelativeLayout rlMain;
            ImageView ivImage, ivMultiImage;
            LinearLayout rlCheckbox;
            CardView cvTasks;
            LinearLayout llImages;
            FrameLayout flAudio;
            TextView tvAudio;
            TextView tvLabel;
            CustomTextView tvText;
            LinearLayout llItemClickDelete;
            FrameLayout llImgs;
            private ItemClickListener clickListener;

            public ItemViewHolder(View itemView) {
                super(itemView);
                this.tvTitle = itemView.findViewById(R.id.tvTitle);
                this.rlMain = itemView.findViewById(R.id.rlMain);
                this.ivImage = itemView.findViewById(R.id.ivImage);
                this.ivMultiImage = itemView.findViewById(R.id.ivMultiImage);
                this.rlCheckbox = itemView.findViewById(R.id.rlCheckbox);
                this.cvTasks = itemView.findViewById(R.id.cvTasks);
                this.llImages = itemView.findViewById(R.id.llImages);
                this.flAudio = itemView.findViewById(R.id.flAudio);
                this.tvAudio = itemView.findViewById(R.id.tvAudio);
                this.tvLabel = itemView.findViewById(R.id.tvLabel);
                this.tvText = itemView.findViewById(R.id.tvText);

                this.llItemClickDelete = itemView.findViewById(R.id.llItemClickDelete);
                this.llImgs = itemView.findViewById(R.id.llImgs);

                itemView.setTag(itemView);
                itemView.setOnClickListener(this);
                itemView.setOnLongClickListener(this);

            }

            public void setClickListener(ItemClickListener itemClickListener) {
                this.clickListener = itemClickListener;
            }

            @Override
            public void onClick(View view) {
                clickListener.onClick(view, getPosition(), false);
            }

            @Override
            public boolean onLongClick(View view) {
                clickListener.onClick(view, getPosition(), true);
                return true;
            }

        }
    }

    public class SharedItemAdapter extends RecyclerView.Adapter<SharedItemAdapter.ItemViewHolder> {
        public static final String EXTRA_ITEM_TRANSITION_NAME = "item_transition_name";

        ViewGroup relativeLayout;
        String defaullt = "Default";
        private Activity mActivity;
        private ArrayList<Item> arraylist;

        public SharedItemAdapter(Activity mActivity, ArrayList<Item> mDataset) {//, PlannerPinnedItemAdapter.Callback callback
            this.mActivity = mActivity;
            this.arraylist = new ArrayList<Item>();
            this.arraylist.addAll(mDataset);
//            this.callback = callback;
        }

        @Override
        public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_notes_list, parent, false);
            return new ItemViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ItemViewHolder holder, int position1) {

            holder.llImages.setVisibility(View.GONE);
            holder.llImages.removeAllViews();
            Item items = arraylist.get(position1);

            relativeLayout = (ViewGroup) View.inflate(mActivity, R.layout.list_item_colleborate_list_users, null);
            int imageHeight, imageWidth;

            if (TextUtils.isEmpty(PlannerTabActivity.strCategoryTypeId) && (TextUtils.isEmpty(PlannerTabActivity.strCategoryId) && (TextUtils.isEmpty(PlannerTabActivity.strSubCategoryId)))) {
                holder.tvLabel.setText(items.getStrCategoryTypeName());
                holder.tvLabel.setVisibility(View.VISIBLE);
            } else if (!TextUtils.isEmpty(PlannerTabActivity.strCategoryTypeId)) {
                holder.tvLabel.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(PlannerTabActivity.strSubCategoryId)) {
                    holder.tvLabel.setVisibility(View.GONE);
                    holder.tvLabel.setText(items.getStrCategoryTypeName());
                } else if (!TextUtils.isEmpty(PlannerTabActivity.strCategoryId)) {
                    String subCat = items.getStrDefaultSubName();
                    if (TextUtils.isEmpty(subCat)) {
                        holder.tvLabel.setText(defaullt.toUpperCase());
                    } else {
                        holder.tvLabel.setText(items.getStrDefaultSubName());
                    }
                } else {
                    holder.tvLabel.setText(items.getStrCategoryTypeName());
                }
            } else {
                holder.tvLabel.setVisibility(View.GONE);
            }

            Typeface font4 = Typeface.createFromAsset(mActivity.getAssets(), "fonts/roboto_medium.ttf");
            holder.tvLabel.setTypeface(font4);

            String noteValue = items.getStrValue();
            String title = items.getStrTitle();

            if (title.length() != 0) {
                holder.tvTitle.setVisibility(View.VISIBLE);
                holder.tvTitle.setText(items.getIntPlannerId() + "");
            } else {
                if (noteValue.length() > 10) {
                    holder.tvTitle.setVisibility(View.INVISIBLE);
                } else {
                    holder.tvTitle.setVisibility(View.INVISIBLE);//GONE
                }
            }

            Typeface font1 = Typeface.createFromAsset(mActivity.getAssets(), "fonts/roboto_regular.ttf");
            holder.tvTitle.setTypeface(font1);

            ArrayList<TodoListItem> arrayToDoList = new ArrayList<>(items.getArryObjstrTodoList());
            ArrayList<String> toSingleStringList = new ArrayList<>();

            if (noteValue.length() > 0) {

                String val = items.getStrValue();
                String newVal = val.trim();
                String trimmedString = "";//= val.trim();
                holder.tvText.setVisibility(View.VISIBLE);
                holder.rlCheckbox.setVisibility(View.GONE);
                holder.tvText.applyCustomFont(mActivity, mActivity.getResources().getString(R.string.roboto_light));
                String[] text = noteValue.split("\\r?\\n");
                for (String line : text) {

                    toSingleStringList.add(line);
                }

                int size = toSingleStringList.size();
                if (toSingleStringList.size() >= 7) {
                    for (int i = size - 1; i > 7; i--) {
                        toSingleStringList.remove(i);
                    }
                }

                if (toSingleStringList.get(0).length() > 80) {// noteValue.length()
                    holder.tvText.setText(toSingleStringList.get(0).substring(0, 80));
                } else {
                    for (String s : toSingleStringList) {
                        trimmedString += s + "\n";
                    }
                }

                holder.tvText.setText(trimmedString);

                if (noteValue.length() < 14) {
                    holder.tvText.setTextSize(20);
                } else {
                    holder.tvText.setTextSize(13);
                }

            } else if ((arrayToDoList.size() != 0)) {

                holder.tvText.setVisibility(View.GONE);
                holder.rlCheckbox.setVisibility(View.VISIBLE);
                holder.rlCheckbox.removeAllViews();
                int size = arrayToDoList.size();
                if (arrayToDoList.size() >= 6) {
                    try {
                        if (items.getImgNoteList().size() != 0) {
                            size = 3;
                        } else {
                            size = 6;
                        }
                    } catch (NullPointerException e) {
                        Log.e("NullPoineter", e + "...in image");
                    }

                } else {
                    size = arrayToDoList.size();
                }
                holder.tvText.setVisibility(View.GONE);
                holder.rlCheckbox.setVisibility(View.VISIBLE);
                holder.rlCheckbox.removeAllViews();

                for (int i = 0; i < size; i++) {

                    Boolean checked = arrayToDoList.get(i).getStrNoteListFlage();
                    CheckBox checkBox = new CheckBox(mActivity);
                    checkBox.setClickable(false);
                    if (checked) {
                        checkBox.setChecked(true);
                    } else {
                        checkBox.setChecked(false);
                    }
                    Typeface font = Typeface.createFromAsset(mActivity.getAssets(), "fonts/roboto_light.ttf");
                    checkBox.setTypeface(font);
                    int checkboxValLength = arrayToDoList.get(i).getStrNoteListValue().length();
                    if (checkboxValLength > 50) {
                        char[] val = arrayToDoList.get(i).getStrNoteListValue().toCharArray();
                        checkBox.setText(val, 0, 50);
                    } else {
                        checkBox.setText(arrayToDoList.get(i).getStrNoteListValue());
                    }
                    checkBox.setTextSize(13);
                    checkBox.setTextColor(mActivity.getResources().getColor(R.color.black));//text_note
                    checkBox.setPadding(10, 10, 10, 10);
                    checkBox.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                    holder.rlCheckbox.addView(checkBox);

                }

            } else {

                holder.tvText.setVisibility(View.GONE);
                holder.rlCheckbox.setVisibility(View.GONE);

            }

            RealmList<ImgDetails> arrarImage = new RealmList<ImgDetails>();
            arrarImage.addAll(items.getImgNoteList());

            if (arrarImage.size() == 0) {
                holder.ivMultiImage.setVisibility(View.GONE);
                holder.ivImage.setVisibility(View.GONE);
                holder.ivImage.setImageDrawable(null);
            } else {
                if (arrarImage.size() >= 2) {
                    holder.ivMultiImage.setVisibility(View.VISIBLE);
                } else holder.ivMultiImage.setVisibility(View.GONE);

                imageHeight = arrarImage.get(0).getIntHeight();
                imageWidth = arrarImage.get(0).getIntWeight();
                holder.ivImage.setVisibility(View.VISIBLE);

                String imagePath = URL_ImageUpload_ROOT + items.getImgNoteList().get(0).getFilename();

                Display display = mActivity.getWindowManager().getDefaultDisplay();
                int phnWidth = Utils.getScreenWidth(mActivity) / 2;
                double height = ((imageHeight * phnWidth) / imageWidth);
                int pHeightNew = ((display.getHeight() * 70) / 100);

                if (height >= pHeightNew) {
                    height = ((display.getHeight() * 50) / 100);
                } else if (height == 0) {
                    height = ((display.getHeight() * 40) / 100);
                }
                if (height > 1000) {
                    height = ((height * 80) / 100);
                }
                Glide.with(mActivity)
                        .load(imagePath)
                        .dontAnimate()
//                            .override(imageWidth, imageHeight) // resizes the image to these dimensions (in pixel)
                        .into(holder.ivImage);


                ViewGroup.LayoutParams params = holder.ivImage.getLayoutParams();
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                params.height = (int) height;
                holder.ivImage.setLayoutParams(params);

            }

            holder.setClickListener(new ItemClickListener() {
                @Override
                public void onClick(View view, int position, boolean isLongClick) {
                    if (isLongClick) {

                        showToast("You can't delete shared note");
//                        callback.multiSelect(activity, position, 0, "OnLongClick");
//                        holder.ivImage.setAlpha(0.5f);
//                        holder.llItemClickDelete.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.list_item_selected_state));
                    } else {
                        if (PlannerFragment.selectionList.size() == 0) {
                            Item items1 = arraylist.get(position1);
                            RealmList<ImgDetails> arrarImage = items.getImgNoteList();
//                            ImgDetails imgDetails = items1.getImgNote();
                            int REQUEST_CODE = 1;

                            boolean isConnected = ConnectivityReceiver.isConnected();

                            Intent intent = new Intent(mActivity, NoteDetailActivity.class);//NoteDetailActivity
                            intent.putExtra("type", "activity");
                            intent.putExtra("position", position1);
                            intent.putExtra("plannerId", String.valueOf(items1.getIntPlannerId()));
                            intent.putExtra("status", isConnected);

                            if (arrarImage.size() != 0) {
                                ViewCompat.setTransitionName(holder.llImgs, "Item");
                                intent.putExtra("isImage", "yes");
                                intent.putExtra(EXTRA_ITEM_TRANSITION_NAME, ViewCompat.getTransitionName(holder.llImgs));
                                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                                        mActivity,
                                        holder.llImgs,
                                        ViewCompat.getTransitionName(holder.llImgs));
                                mActivity.startActivityForResult(intent, REQUEST_CODE, options.toBundle());
                            } else {
                                intent.putExtra("isImage", "no");
                                ViewCompat.setTransitionName(holder.rlMain, "Item");
                                intent.putExtra(EXTRA_ITEM_TRANSITION_NAME, ViewCompat.getTransitionName(holder.rlMain));
                                ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                                        mActivity,
                                        holder.rlMain,
                                        ViewCompat.getTransitionName(holder.rlMain));
                                mActivity.startActivityForResult(intent, REQUEST_CODE, options.toBundle());
                            }

                        } else {
                            showToast("You can't delete shared note");
//                            callback.multiSelect(activity, position, 0, "OnClick");
//                            callback.editVal(mActivity, position, 0, "OnClick");
                            if (PlannerFragment.selectionList.contains(arraylist.get(position))) {
                                holder.ivImage.setAlpha(0.5f);
                                holder.llItemClickDelete.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.list_item_selected_state));
                            } else {
                                holder.ivImage.setAlpha(1f);
                                holder.llItemClickDelete.setBackgroundColor(ContextCompat.getColor(mActivity, R.color.list_item_normal_state));
                            }
                        }

                    }

                }
            });

        }


        @Override
        public int getItemCount() {
            return arraylist.size();
        }

        class ItemViewHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener, View.OnLongClickListener {
            CustomTextView tvTitle;
            RelativeLayout rlMain;
            ImageView ivImage, ivMultiImage;
            LinearLayout rlCheckbox;
            CardView cvTasks;
            LinearLayout llImages;
            FrameLayout flAudio;
            TextView tvAudio;
            TextView tvLabel;
            CustomTextView tvText;
            LinearLayout llItemClickDelete;
            FrameLayout llImgs;
            private ItemClickListener clickListener;

            public ItemViewHolder(View itemView) {
                super(itemView);
                this.tvTitle = itemView.findViewById(R.id.tvTitle);
                this.rlMain = itemView.findViewById(R.id.rlMain);
                this.ivImage = itemView.findViewById(R.id.ivImage);
                this.ivMultiImage = itemView.findViewById(R.id.ivMultiImage);
                this.rlCheckbox = itemView.findViewById(R.id.rlCheckbox);
                this.cvTasks = itemView.findViewById(R.id.cvTasks);
                this.llImages = itemView.findViewById(R.id.llImages);
                this.flAudio = itemView.findViewById(R.id.flAudio);
                this.tvAudio = itemView.findViewById(R.id.tvAudio);
                this.tvLabel = itemView.findViewById(R.id.tvLabel);
                this.tvText = itemView.findViewById(R.id.tvText);

                this.llItemClickDelete = itemView.findViewById(R.id.llItemClickDelete);
                this.llImgs = itemView.findViewById(R.id.llImgs);

                itemView.setTag(itemView);
                itemView.setOnClickListener(this);
                itemView.setOnLongClickListener(this);
            }

            public void setClickListener(ItemClickListener itemClickListener) {
                this.clickListener = itemClickListener;
            }

            @Override
            public void onClick(View view) {
                clickListener.onClick(view, getPosition(), false);
            }

            @Override
            public boolean onLongClick(View view) {
                clickListener.onClick(view, getPosition(), true);
                return true;
            }


        }
    }
}
