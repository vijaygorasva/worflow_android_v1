package com.example.user.communicator;

public class UpdaterApplication extends Application {
        public void onCreate() {
            super.onCreate();
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    loadDataFromServer();
                }});
            thread.start();
        }

        private void loadDataFromServer() {
            // ...
            // load data from server
            // this could take some time
            // ...

            // How do we update the current activity?

        }
    }
