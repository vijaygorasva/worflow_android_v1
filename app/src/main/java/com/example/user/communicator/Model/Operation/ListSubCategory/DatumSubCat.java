
package com.example.user.communicator.Model.Operation.ListSubCategory;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class DatumSubCat extends RealmObject implements Parcelable {

    public static final Parcelable.Creator<DatumSubCat> CREATOR = new Parcelable.Creator<DatumSubCat>() {
        @Override
        public DatumSubCat createFromParcel(Parcel in) {
            return new DatumSubCat(in);
        }

        @Override
        public DatumSubCat[] newArray(int size) {
            return new DatumSubCat[size];
        }
    };
    @SerializedName("fkIntCategoryTypeId")
    @Expose
    private String fkIntCategoryTypeId;

    @SerializedName("fkIntCategoryId")
    @Expose
    private String fkIntCategoryId;

    @SerializedName("fkIntSubCategoryId")
    @Expose
    private String fkIntSubCategoryId;

    @SerializedName("strTitle")
    @Expose
    private String strTitle;
    @SerializedName("strOperationName")
    @Expose
    private String strOperationSubCategoryName;
    @SerializedName("intCreateUserId")
    @Expose
    private String intCreateUserId;
    @SerializedName("datCreateDateAndTime")
    @Expose
    private String datCreateDateAndTime;
    @SerializedName("strGoalAchieve")//strGoalAchive
    @Expose
    private String strGoalAchive;

    @SerializedName("strOperationMainCategoryName")
    @Expose
    private String strOperationMainCategoryName;

    @SerializedName("intProjectId")
    @Expose
    private String intProjectId;

    @SerializedName("isTaken")
    @Expose
    private Boolean isTaken;
    @PrimaryKey
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("intOrganisationId")
    @Expose
    private String intOrganisationId;
    @SerializedName("intCountSteps")
    @Expose
    private Integer intCountSteps;
    @SerializedName("arryAchieveSteps")
    @Expose
    private RealmList<AchieveStepsCategory> achieveStepsCategories = null;
    @SerializedName("arryNotAchieveSteps")
    @Expose
    private RealmList<NotAchieveStepsCategory> notAchieveStepsCategories = null;

    @SerializedName("durtaion")
    @Expose
    private String durtaion;

    public static Creator<DatumSubCat> getCREATOR() {
        return CREATOR;
    }

    protected DatumSubCat(Parcel in) {

        fkIntCategoryTypeId = in.readString();
        fkIntCategoryId = in.readString();
        fkIntSubCategoryId = in.readString();

        id = in.readString();
        strTitle = in.readString();
        strOperationSubCategoryName = in.readString();
        intCreateUserId = in.readString();
        datCreateDateAndTime = in.readString();
        strGoalAchive = in.readString();
        strOperationMainCategoryName = in.readString();
        intProjectId = in.readString();
        intOrganisationId = in.readString();
        intCountSteps = in.readInt();
        intOrganisationId = in.readString();
        in.readTypedList(achieveStepsCategories, AchieveStepsCategory.CREATOR);
        in.readTypedList(notAchieveStepsCategories, NotAchieveStepsCategory.CREATOR);
        durtaion = in.readString();


    }

    public String getDurtaion() {
        return durtaion;
    }

    public String getFkIntCategoryTypeId() {
        return fkIntCategoryTypeId;
    }

    public void setFkIntCategoryTypeId(String fkIntCategoryTypeId) {
        this.fkIntCategoryTypeId = fkIntCategoryTypeId;
    }

    public String getFkIntCategoryId() {
        return fkIntCategoryId;
    }

    public void setFkIntCategoryId(String fkIntCategoryId) {
        this.fkIntCategoryId = fkIntCategoryId;
    }

    public String getFkIntSubCategoryId() {
        return fkIntSubCategoryId;
    }

    public void setFkIntSubCategoryId(String fkIntSubCategoryId) {
        this.fkIntSubCategoryId = fkIntSubCategoryId;
    }

    public Boolean getTaken() {
        return isTaken;
    }

    public void setTaken(Boolean taken) {
        isTaken = taken;
    }

    public DatumSubCat() {
    }

    public Boolean getIsTaken() {
        return isTaken;
    }

    public void setIsTaken(Boolean isTaken) {
        this.isTaken = isTaken;
    }

    public void setDurtaion(String durtaion) {
        this.durtaion = durtaion;
    }
    public Integer getIntCountSteps() {
        return intCountSteps;
    }


    public String getStrOperationMainCategoryName() {
        return strOperationMainCategoryName;
    }

    public void setStrOperationMainCategoryName(String strOperationMainCategoryName) {
        this.strOperationMainCategoryName = strOperationMainCategoryName;
    }

    public String getIntProjectId() {
        return intProjectId;
    }

    public void setIntProjectId(String intProjectId) {
        this.intProjectId = intProjectId;
    }

    public String getIntOrganisationId() {
        return intOrganisationId;
    }

    public void setIntOrganisationId(String intOrganisationId) {
        this.intOrganisationId = intOrganisationId;
    }



    public String getStrTitle() {
        return strTitle;
    }

    public void setStrTitle(String strTitle) {
        this.strTitle = strTitle;
    }

    public String getStrOperationSubCategoryName() {
        return strOperationSubCategoryName;
    }

    public void setStrOperationSubCategoryName(String strOperationSubCategoryName) {
        this.strOperationSubCategoryName = strOperationSubCategoryName;
    }

    public String getIntCreateUserId() {
        return intCreateUserId;
    }

    public void setIntCreateUserId(String intCreateUserId) {
        this.intCreateUserId = intCreateUserId;
    }

    public String getDatCreateDateAndTime() {
        return datCreateDateAndTime;
    }

    public void setDatCreateDateAndTime(String datCreateDateAndTime) {
        this.datCreateDateAndTime = datCreateDateAndTime;
    }

    public String getStrGoalAchive() {
        return strGoalAchive;
    }

    public void setStrGoalAchive(String strGoalAchive) {
        this.strGoalAchive = strGoalAchive;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setIntCountSteps(Integer intCountSteps) {
        this.intCountSteps = intCountSteps;
    }

    public RealmList<AchieveStepsCategory> getAchieveStepsCategories() {
        return achieveStepsCategories;
    }

    public void setAchieveStepsCategories(RealmList<AchieveStepsCategory> achieveStepsCategories) {
        this.achieveStepsCategories = achieveStepsCategories;
    }

    public RealmList<NotAchieveStepsCategory> getNotAchieveStepsCategories() {
        return notAchieveStepsCategories;
    }

    public void setNotAchieveStepsCategories(RealmList<NotAchieveStepsCategory> notAchieveStepsCategories) {
        this.notAchieveStepsCategories = notAchieveStepsCategories;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(fkIntCategoryTypeId);
        parcel.writeString(fkIntCategoryId);
        parcel.writeString(fkIntSubCategoryId);

        parcel.writeString(id);
        parcel.writeString(strTitle);
        parcel.writeString(strOperationSubCategoryName);
        parcel.writeString(intCreateUserId);
        parcel.writeString(datCreateDateAndTime);
        parcel.writeString(strGoalAchive);
        parcel.writeString(strOperationMainCategoryName);
        parcel.writeString(intProjectId);
        parcel.writeString(intOrganisationId);
        parcel.writeInt(intCountSteps);
        parcel.writeString(intOrganisationId);
        parcel.writeTypedList(achieveStepsCategories);
        parcel.writeTypedList(notAchieveStepsCategories);
        parcel.writeString(durtaion);
    }
}

