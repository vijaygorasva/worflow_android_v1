package com.example.user.communicator.Utility;

import android.app.Activity;
import android.widget.TextView;

import com.example.user.communicator.Model.TaskModels.TaskItems;

import java.util.Timer;
import java.util.TimerTask;


public class Constants {

    public static int seconds, minutes, hours;
    public static TaskItems finalTaskItems;
    public static TextView textView;
    public static Timer mTimer;
    public static Activity mActivity;
    public static boolean isRunning = false;

    public static void startTimer() {
        isRunning = true;
        final int[] second = {0};
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                second[0]++;
                seconds = second[0] % 60;
                int totalMinutes = second[0] / 60;
                minutes = totalMinutes % 60;
                hours = totalMinutes / 60;
                if (textView != null) {
                    if (mActivity != null) {
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                textView.setText((hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds));
                            }
                        });
                    }
                }
            }
        }, 0, 1000);
    }


    public static void setAllClear() {
        isRunning = false;
        if (textView != null) {
            textView.setText("00:00:00");
        }
        seconds = 0;
        minutes = 0;
        hours = 0;
        finalTaskItems = null;
        textView = null;
        mTimer = null;
    }

    public static void setTextView(TextView txtView) {
        textView = txtView;
    }

    public static void setActivity(Activity activity) {
        mActivity = activity;
    }
}
