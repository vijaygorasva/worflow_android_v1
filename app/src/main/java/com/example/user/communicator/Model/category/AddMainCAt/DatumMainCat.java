
package com.example.user.communicator.Model.category.AddMainCAt;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DatumMainCat {

    @SerializedName("fkIntCategoryTypeId")
    @Expose
    private String fkIntCategoryTypeId;
    @SerializedName("strCategoryName")
    @Expose
    private String strCategoryName;
    @SerializedName("intCreateUserId")
    @Expose
    private String intCreateUserId;
    @SerializedName("datCreateDateAndTime")
    @Expose
    private String datCreateDateAndTime;
    @SerializedName("intOrganisationId")
    @Expose
    private String intOrganisationId;
    @SerializedName("intProjectId")
    @Expose
    private String intProjectId;
    @SerializedName("intLastModifiedUserId")
    @Expose
    private Object intLastModifiedUserId;
    @SerializedName("datLastModifiedDateAndTime")
    @Expose
    private Object datLastModifiedDateAndTime;
    @SerializedName("strStatus")
    @Expose
    private String strStatus;
    @SerializedName("strDefaultSub")
    @Expose
    private Boolean strDefaultSub;
    @SerializedName("_id")
    @Expose
    private String id;

    public String getFkIntCategoryTypeId() {
        return fkIntCategoryTypeId;
    }

    public void setFkIntCategoryTypeId(String fkIntCategoryTypeId) {
        this.fkIntCategoryTypeId = fkIntCategoryTypeId;
    }

    public String getStrCategoryName() {
        return strCategoryName;
    }

    public void setStrCategoryName(String strCategoryName) {
        this.strCategoryName = strCategoryName;
    }

    public String getIntCreateUserId() {
        return intCreateUserId;
    }

    public void setIntCreateUserId(String intCreateUserId) {
        this.intCreateUserId = intCreateUserId;
    }

    public String getDatCreateDateAndTime() {
        return datCreateDateAndTime;
    }

    public void setDatCreateDateAndTime(String datCreateDateAndTime) {
        this.datCreateDateAndTime = datCreateDateAndTime;
    }

    public String getIntOrganisationId() {
        return intOrganisationId;
    }

    public void setIntOrganisationId(String intOrganisationId) {
        this.intOrganisationId = intOrganisationId;
    }

    public String getIntProjectId() {
        return intProjectId;
    }

    public void setIntProjectId(String intProjectId) {
        this.intProjectId = intProjectId;
    }

    public Object getIntLastModifiedUserId() {
        return intLastModifiedUserId;
    }

    public void setIntLastModifiedUserId(Object intLastModifiedUserId) {
        this.intLastModifiedUserId = intLastModifiedUserId;
    }

    public Object getDatLastModifiedDateAndTime() {
        return datLastModifiedDateAndTime;
    }

    public void setDatLastModifiedDateAndTime(Object datLastModifiedDateAndTime) {
        this.datLastModifiedDateAndTime = datLastModifiedDateAndTime;
    }

    public String getStrStatus() {
        return strStatus;
    }

    public void setStrStatus(String strStatus) {
        this.strStatus = strStatus;
    }

    public Boolean getStrDefaultSub() {
        return strDefaultSub;
    }

    public void setStrDefaultSub(Boolean strDefaultSub) {
        this.strDefaultSub = strDefaultSub;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
