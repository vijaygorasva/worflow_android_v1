package com.example.user.communicator.Model.Notes;

import java.io.File;

public class AudFile {
    File file;
    String duration;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
