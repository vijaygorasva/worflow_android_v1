package com.example.user.communicator.Adapter.BrodcastsAdapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.BrodcastsModels.ReplyItem;
import com.example.user.communicator.R;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ReplyAdapter extends RecyclerView.Adapter<ReplyAdapter.ViewHolder> {


    private final String BASE_URL = "http://13.232.124.188:9999/";
    private ArrayList<ReplyItem> items;
    private Context context;

    public ReplyAdapter(ArrayList<ReplyItem> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_reply, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(items.get(position));
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    private String getMillsToDate(long mills) {
        SimpleDateFormat df = new SimpleDateFormat("dd,MMM yyyy hh:mm a", Locale.US);
        return df.format(new Date(mills));
    }

    private String getDiffrenece(long startDate, long endDate) {

        long difference = (endDate - startDate);
        long sec = difference / 1000 % 60;
        long min = difference / (60 * 1000) % 60;
        long hours = difference / (60 * 60 * 1000) % 24;
        long diffDays = difference / (24 * 60 * 60 * 1000);

        return (diffDays == 0 ? ((hours == 0 ? (min == 0 ? (sec + " sec") : (min + " min")) : (hours + " hrs"))) : (getMillsToDate(startDate)));
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.prof_img)
        ImageView prof_img;

        @BindView(R.id.name)
        CustomTextView name;

        @BindView(R.id.time)
        CustomTextView time;

        @BindView(R.id.replyValue)
        RecyclerView replyValue;

        RelpyValuesAdapter valuesAdapter;
        LinearLayoutManager linearLayoutManager;
        ArrayList<String> valuesList = new ArrayList<>();

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @SuppressLint("SetTextI18n")
        public void bind(final ReplyItem item) {

            String diff = getDiffrenece(item.getAddTime(), System.currentTimeMillis());
            if (!TextUtils.isEmpty(diff)) {
                time.setText(diff);
            } else {
                time.setText("");
            }
            if (item.getIntUserId().getImg() != null) {
                if (!TextUtils.isEmpty(item.getIntUserId().getImg().getPath())) {
                    Picasso.get().load(BASE_URL + item.getIntUserId().getImg().getPath()).placeholder(R.drawable.ic_error_male).error(R.drawable.ic_error_male).into(prof_img);
                }
            }
            name.setText(item.getIntUserId().getStrFirstName() + " " + item.getIntUserId().getStrLastName());
            valuesList.addAll(item.getReplyValue());
            linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            replyValue.setLayoutManager(linearLayoutManager);
            valuesAdapter = new RelpyValuesAdapter(valuesList, context);
            replyValue.setAdapter(valuesAdapter);

        }
    }
}
