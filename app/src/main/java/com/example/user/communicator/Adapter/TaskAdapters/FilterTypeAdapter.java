package com.example.user.communicator.Adapter.TaskAdapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.TaskModels.FilterType;
import com.example.user.communicator.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilterTypeAdapter extends RecyclerView.Adapter<FilterTypeAdapter.ViewHolder> {

    private Context context;
    private ArrayList<FilterType> taskItems;
    private OnClickListner onClick;
//    public final String BASE_URL = "http://52.66.249.75:9999/";

    public final String BASE_URL_SEVER = "http://52.66.249.75:9999/";
    public final String BASE_URL_local = "http://:9999/";
    public final String BASE_URL_TEST = "http://13.232.37.104:9999/";
    public final String BASE_URL = BASE_URL_TEST;

    private ArrayList<FilterType> allTaskItems = new ArrayList<>();

    public FilterTypeAdapter(Context context, ArrayList<FilterType> taskItems) {
        this.taskItems = taskItems;
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_filter_dialog, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        FilterType item = taskItems.get(holder.getAdapterPosition());

        if (position == taskItems.size()-1){
            holder.view.setVisibility(View.GONE);
        }else {
            holder.view.setVisibility(View.VISIBLE);
        }

        holder.name.setText(item.getName().toUpperCase());
//        holder.btn.setChecked(item.isSelected());
//        holder.btn.setOnCheckedChangeListener((buttonView, isChecked) -> {
//            if (onClick != null) {
//                onClick.OnClick(holder.getAdapterPosition());
//            }
//        });

        holder.mHolder.setOnClickListener(v -> {
            if (onClick != null) {
                onClick.OnClick(holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return taskItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setOnClickListner(OnClickListner onClickListner) {
        this.onClick = onClickListner;
    }

    public interface OnClickListner {
        void OnClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.view)
        View view;

        @BindView(R.id.name)
        CustomTextView name;

        @BindView(R.id.holder)
        RelativeLayout mHolder;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}