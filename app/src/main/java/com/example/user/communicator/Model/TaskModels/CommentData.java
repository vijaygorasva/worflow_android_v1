package com.example.user.communicator.Model.TaskModels;


import io.realm.RealmList;
import io.realm.RealmObject;

public class CommentData extends RealmObject {

    private CreateUser intUserId;
    private String _id;
    private String comment;
    private long addTime;
    private String fileType;
    private String filePath;
    private String originalName;
    private int isHaveAttachment;
    private RealmList<Replies> replies = new RealmList<>();

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public CreateUser getIntUserId() {
        return intUserId;
    }

    public void setIntUserId(CreateUser intUserId) {
        this.intUserId = intUserId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public long getAddTime() {
        return addTime;
    }

    public void setAddTime(long addTime) {
        this.addTime = addTime;
    }

    public RealmList<Replies> getReplies() {
        return replies;
    }

    public void setReplies(RealmList<Replies> replies) {
        this.replies = replies;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public int getIsHaveAttachment() {
        return isHaveAttachment;
    }

    public void setIsHaveAttachment(int isHaveAttachment) {
        this.isHaveAttachment = isHaveAttachment;
    }
}
