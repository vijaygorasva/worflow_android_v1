package com.example.user.communicator.Utility;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class ImagePicker {

    public static final int IMAGE_PICKER_REQUEST_CODE = 10001;

    public static Uri generateImageUri(Context context) {
//        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "MyDir" + File.separator);
        final File root = context.getCacheDir();
        if (!root.exists() || !root.isDirectory()) {
            root.mkdirs();
        }

        final String name = Calendar.getInstance().getTimeInMillis() + ".jpg";
        final File sdImageMainDirectory = new File(root, name);
        Uri uri = Uri.fromFile(sdImageMainDirectory);
        return uri;
    }

    public static Intent createImageIntent(Context context) {
        // Camera.
        final List<Intent> cameraIntents = new ArrayList<>();
        final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = context.getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            cameraIntents.add(intent);
        }

        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        photoPickerIntent.setType("image/*");

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(photoPickerIntent, "Profile photo");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));

        return chooserIntent;
    }

    public static void openImageIntent(Activity activity) {
        Intent chooserIntent = createImageIntent(activity);
        activity.startActivityForResult(chooserIntent, IMAGE_PICKER_REQUEST_CODE);
    }

    public static void openImageIntent(Context context, Fragment fragment) {
        Intent chooserIntent = createImageIntent(context);
        fragment.startActivityForResult(chooserIntent, IMAGE_PICKER_REQUEST_CODE);
    }

    public static void openImageIntent(Context context, android.app.Fragment fragment) {
        Intent chooserIntent = createImageIntent(context);
        fragment.startActivityForResult(chooserIntent, IMAGE_PICKER_REQUEST_CODE);
    }

    public static boolean saveBitmap(Bitmap bmp, Uri uri) {
        File file = new File(uri.getPath());
        if (file.exists()) {
            file.delete();
        }

        FileOutputStream out = null;
        boolean result = false;
        try {
            out = new FileOutputStream(uri.getPath());
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public static Uri getImageUri(Activity activity, Intent data) {
        if (data != null) {
            String action = data.getAction();
            Uri uri = data.getData();

            if (uri != null) {
                return uri;
            } else if ("inline-data".equals(action)) {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                if (bitmap != null) {
                    Uri outputFileUri = generateImageUri(activity);
                    if (saveBitmap(bitmap, outputFileUri)) {
                        return outputFileUri;
                    } else {
                        File file = new File(outputFileUri.getPath());
                        if (file.exists()) {
                            file.delete();
                        }
                    }
                }
            }
        }

        return null;
    }

}
