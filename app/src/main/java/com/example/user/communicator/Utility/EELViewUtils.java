package com.example.user.communicator.Utility;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.communicator.R;

import pl.droidsonroids.gif.GifImageView;


public class EELViewUtils {

    Context mContext;

    ViewGroup mMainLayout;
    GifImageView ivLoader;
    View mContentLayout;

    View vLoading;
    View vEmpty;
    View vError;

    TextView tvLoadingText1, tvLoadingText2;
    TextView tvEmptyTitle, tvEmptySubtitle;
    TextView tvErrorText1, tvErrorText2;
    ImageView ivEmptyImage, ivErrorIcon;

    Button btnEmptyButton, btnErrorButton;

    String sLoadingText1 = "Loading...", sLoadingText2;
    String sEmptyTitle = "No data", sEmptySubtitle;
    String sErrorText1 = "Error", sErrorText2;
    String sEmptyButton, sErrorButton = "Retry";

    public EELViewUtils(ViewGroup mainLayout, View contentLayout) {
        mContext = mainLayout.getContext();
        mMainLayout = mainLayout;
        mContentLayout = contentLayout;
        init();
    }

    private void init() {

        vLoading = LayoutInflater.from(mContext).inflate(R.layout.include_loading, mMainLayout, false);
        vEmpty = LayoutInflater.from(mContext).inflate(R.layout.include_empty, mMainLayout, false);
        vError = LayoutInflater.from(mContext).inflate(R.layout.include_error, mMainLayout, false);

        vLoading.setVisibility(View.GONE);
        vEmpty.setVisibility(View.GONE);
        vError.setVisibility(View.GONE);
        ivLoader = (GifImageView) vLoading.findViewById(R.id.ivLoader);
        tvLoadingText1 = (TextView) vLoading.findViewById(R.id.tvLoadingText1);
        tvLoadingText2 = (TextView) vLoading.findViewById(R.id.tvLoadingText2);

        ivEmptyImage = (ImageView) vEmpty.findViewById(R.id.ivEmptyImage);
        ivEmptyImage.setColorFilter(Color.LTGRAY);
        tvEmptyTitle = (TextView) vEmpty.findViewById(R.id.tvEmptyTitle);
        tvEmptySubtitle = (TextView) vEmpty.findViewById(R.id.tvEmptySubtitle);

        ivErrorIcon = (ImageView) vError.findViewById(R.id.ivErrorIcon);
        tvErrorText1 = (TextView) vError.findViewById(R.id.tvErrorText1);
        tvErrorText2 = (TextView) vError.findViewById(R.id.tvErrorText2);

        btnEmptyButton = (Button) vEmpty.findViewById(R.id.btnEmptyButton);
        btnErrorButton = (Button) vError.findViewById(R.id.btnErrorButton);

        setLoadingText1(sLoadingText1);
        setLoadingText2(sLoadingText2);
        setEmptyTitle(sEmptyTitle);
        setEmptySubtitle(sEmptySubtitle);
        setEmptyButtonText(sEmptyButton, null);
        setErrorText1(sErrorText1);
        setErrorText2(sErrorText2);
        setEmptyButtonText(sEmptyButton, null);

        mMainLayout.addView(vLoading);
        mMainLayout.addView(vEmpty);
        mMainLayout.addView(vError);
    }

    public void setEmptyTitlePosition(){
        int padding = 100;
        float density = mContext.getResources().getDisplayMetrics().density;
        int paddingDp = (int) (padding * density);
        LinearLayout.LayoutParams params = new
                LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.setMargins(0,0,0,paddingDp);
        tvEmptyTitle.setLayoutParams(params);
        tvEmptyTitle.setPadding(0,0,0,paddingDp);

    }

    public void setLoadingText1(String text){
        sLoadingText1 = text;
        if (tvLoadingText1 != null) {
            if (TextUtils.isEmpty(text)) {
                tvLoadingText1.setVisibility(View.GONE);
                return;
            }

            tvLoadingText1.setVisibility(View.GONE);//VISIBLE
            tvLoadingText1.setText(sLoadingText1);
        }
    }

    public void setLoadingText2(String text){
        sLoadingText2 = text;
        if (tvLoadingText2 != null) {
            if (TextUtils.isEmpty(text)) {
                tvLoadingText2.setVisibility(View.GONE);
                return;
            }

            tvLoadingText2.setVisibility(View.VISIBLE);
            tvLoadingText2.setText(sLoadingText2);
        }
    }

    public void setEmptyImage(int resId){
        if (ivEmptyImage != null) {
            ivEmptyImage.setVisibility(View.VISIBLE);
            ivEmptyImage.setImageResource(resId);
        }
    }

    public void setEmptyImageVisibility(boolean visible){
        if (ivEmptyImage != null) {
            ivEmptyImage.setVisibility(visible ? View.VISIBLE : View.GONE);
        }
    }

    public void setEmptyTitle(String text){
        sEmptyTitle = text;
        if (tvEmptyTitle != null) {
            if (TextUtils.isEmpty(text)) {
                tvEmptyTitle.setVisibility(View.GONE);
                return;
            }

            tvEmptyTitle.setVisibility(View.VISIBLE);
            tvEmptyTitle.setText(sEmptyTitle);
        }
    }

    public void setEmptySubtitle(String text){
        sEmptySubtitle = text;
        if (tvEmptySubtitle != null) {
            if (TextUtils.isEmpty(text)) {
                tvEmptySubtitle.setVisibility(View.GONE);
                return;
            }

            tvEmptySubtitle.setVisibility(View.VISIBLE);
            tvEmptySubtitle.setText(sEmptySubtitle);
        }
    }

    public void setErrorImageVisibility(boolean visible){
        if (ivErrorIcon != null) {
            ivErrorIcon.setVisibility(visible ? View.VISIBLE : View.GONE);
        }
    }

    public void setErrorText1(String text){
        sErrorText1 = text;
        if (tvErrorText1 != null) {
            if (TextUtils.isEmpty(text)) {
                tvErrorText1.setVisibility(View.GONE);
                return;
            }

            tvErrorText1.setVisibility(View.VISIBLE);
            tvErrorText1.setText(sErrorText1);
        }
    }

    public void setErrorText2(String text){
        sErrorText2 = text;
        if (tvErrorText2 != null) {
            if (TextUtils.isEmpty(text)) {
                tvErrorText2.setVisibility(View.GONE);
                return;
            }

            tvErrorText2.setVisibility(View.VISIBLE);
            tvErrorText2.setText(sErrorText2);
        }
    }

    public void setEmptyButtonText(View.OnClickListener listener){
        setEmptyButtonText(sEmptyButton, listener);
    }

    public void setEmptyButtonText(String text, View.OnClickListener listener){
        sEmptyButton = text;
        if (btnEmptyButton != null) {
            if (TextUtils.isEmpty(text)) {
                btnEmptyButton.setVisibility(View.GONE);
                return;
            }

            btnEmptyButton.setVisibility(View.VISIBLE);
            btnEmptyButton.setText(sEmptyButton);
            btnEmptyButton.setOnClickListener(listener);
        }
    }

    public void setErrorButtonText(View.OnClickListener listener){
        setErrorButtonText(sErrorButton, listener);
    }

    public void setErrorButtonText(String text, View.OnClickListener listener){
        sErrorButton = text;
        if (btnErrorButton != null) {
            if (TextUtils.isEmpty(text)) {
                btnErrorButton.setVisibility(View.GONE);
                return;
            }

            btnErrorButton.setVisibility(View.VISIBLE);
            btnErrorButton.setText(sErrorButton);
            btnErrorButton.setOnClickListener(listener);
        }
    }

    public void showContentViewWhite() {
        ivLoader.setColorFilter(ContextCompat.getColor(mContext, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);
        ivLoader.setVisibility(View.VISIBLE);
        mContentLayout.setVisibility(View.VISIBLE);
        vLoading.setVisibility(View.GONE);
        vEmpty.setVisibility(View.GONE);
        vError.setVisibility(View.GONE);
    }

    public void showContentViewColorPrimary() {
        ivLoader.setColorFilter(ContextCompat.getColor(mContext, R.color.colorPrimary), android.graphics.PorterDuff.Mode.MULTIPLY);
        mContentLayout.setVisibility(View.VISIBLE);
        ivLoader.setVisibility(View.VISIBLE);
        vLoading.setVisibility(View.GONE);
        vEmpty.setVisibility(View.GONE);
        vError.setVisibility(View.GONE);
    }

    public void showLoadingView() {
        mContentLayout.setVisibility(View.GONE);
        vLoading.setVisibility(View.VISIBLE);
        vEmpty.setVisibility(View.GONE);
        vError.setVisibility(View.GONE);
    }

    public void showEmptyView() {
        mContentLayout.setVisibility(View.GONE);
        vLoading.setVisibility(View.GONE);
        vEmpty.setVisibility(View.VISIBLE);
        vError.setVisibility(View.GONE);
    }

    public void showErrorView() {
        mContentLayout.setVisibility(View.GONE);
        vLoading.setVisibility(View.GONE);
        vEmpty.setVisibility(View.GONE);
        vError.setVisibility(View.VISIBLE);
    }

}
