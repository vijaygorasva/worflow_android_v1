
package com.example.user.communicator.Model.Notes;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class ObjRemainder extends RealmObject implements Parcelable {

    //
    public static final Creator<ObjRemainder> CREATOR = new Creator<ObjRemainder>() {
        @Override
        public ObjRemainder createFromParcel(Parcel in) {
            return new ObjRemainder(in);
        }

        @Override
        public ObjRemainder[] newArray(int size) {
            return new ObjRemainder[size];
        }
    };

    @SerializedName("strRemainderDate")
    @Expose
    private String strRemainderDate;
    @SerializedName("strRemainderTime")
    @Expose
    private String strRemainderTime;
    @SerializedName("strType")
    @Expose
    private String strType;
    @SerializedName("blnFlage")
    @Expose
    private Boolean blnFlage;

    public ObjRemainder() {

    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getStrRemainderDate() {
        return strRemainderDate;
    }

    public void setStrRemainderDate(String strRemainderDate) {
        this.strRemainderDate = strRemainderDate;
    }

    public String getStrRemainderTime() {
        return strRemainderTime;
    }

    public void setStrRemainderTime(String strRemainderTime) {
        this.strRemainderTime = strRemainderTime;
    }

    public String getStrType() {
        return strType;
    }

    public void setStrType(String strType) {
        this.strType = strType;
    }

    public Boolean getBlnFlage() {
        return blnFlage;
    }

    public void setBlnFlage(Boolean blnFlage) {
        this.blnFlage = blnFlage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    //    @PrimaryKey
    @SerializedName("intId")
    @Expose
    private int _id;
//
//    public ObjRemainder(){
//
//    }

    public ObjRemainder(Parcel in) {
        strRemainderDate = in.readString();
        strRemainderTime = in.readString();
        strType = in.readString();
    }



    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(strRemainderDate);
        parcel.writeString(strRemainderTime);
        parcel.writeString(strType);

    }
}
