package com.example.user.communicator.Activity.TaskModule;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.communicator.Adapter.TaskAdapters.FilterTypeAdapter;
import com.example.user.communicator.Adapter.TaskAdapters.TaskListAdapter;
import com.example.user.communicator.CustomViews.CustomEditText;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Dialog.CategoryDialog;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.TaskModels.FilterType;
import com.example.user.communicator.Model.TaskModels.SelectionItem;
import com.example.user.communicator.Model.TaskModels.SelectionObject;
import com.example.user.communicator.Model.TaskModels.TaskItems;
import com.example.user.communicator.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class TasksActivity extends BaseActivity {

    @BindView(R.id.tvToolbar)
    TextView tvToolbar;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.rvTaskList)
    RecyclerView rvTaskList;

    @BindView(R.id.etAddTask)
    CustomEditText etAddTask;

    @BindView(R.id.searchBox)
    CustomEditText mSearchBox;

    @BindView(R.id.ivBack1)
    ImageView ivBack1;

    @BindView(R.id.ivSearch1)
    ImageView ivSearch1;

//    @BindView(R.id.SearchView)
//    RelativeLayout searchView;

    @BindView(R.id.tvDone)
    CustomTextView tvDone;

    @BindView(R.id.usersCount)
    CustomTextView usersCount;

    @BindView(R.id.myTasks)
    CustomTextView myTasks;

    @BindView(R.id.assignedTasks)
    CustomTextView assignedTasks;

    @BindView(R.id.otherCount)
    CustomTextView otherCount;

    @BindView(R.id.filterType)
    CustomTextView filterType;

    @BindView(R.id.spinner)
    MaterialSpinner spinner;

    TaskListAdapter taskListAdapter;
    ArrayList<TaskItems> taskItems = new ArrayList<>();

    @BindView(R.id.fabtnAdd)
    FloatingActionButton fabtnAdd;

    @BindView(R.id.fabSearch11)
    FloatingActionButton fabSearch1;

    @BindView(R.id.tvCategory)
    CustomTextView tvCategory;

    @BindView(R.id.SearchView)
    LinearLayout searchView;

    @BindView(R.id.line)
    View mLine;

    Context context;

    String mPriority;

    SelectionObject mObject;
    Datum mUserDetails;

    ArrayList<SelectionItem> selectionItemsUsers = new ArrayList<>();
    ArrayList<SelectionItem> selectionItemsLevels = new ArrayList<>();
    ArrayList<SelectionItem> selectionItemsMembers = new ArrayList<>();

    ArrayList<FilterType> filterTypes = new ArrayList<>();

    @BindView(R.id.cvTasks)
    CardView mAddView;

    boolean isMyTaskItems = true;
    boolean isSearch = false;
    Realm mRealm;
    String categoryID = "0";
    String mFilterType = "ADD_TIME";
    String BROADCAST_ACTION = "com.example.user.communicator";

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks);
        ButterKnife.bind(this);

        context = this;
        mRealm = Realm.getDefaultInstance();

        mUserDetails = getLoginData();

        rvTaskList.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvTaskList.setLayoutManager(mLayoutManager);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        rvTaskList.setLayoutManager(layoutManager);

        taskListAdapter = new TaskListAdapter(this, taskItems, rvTaskList);
        taskListAdapter.setIsAssignedTask(false);
        rvTaskList.setAdapter(taskListAdapter);

        tvDone.setOnClickListener(view -> taskAddCall());
        ivBack1.setOnClickListener(v -> {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(mSearchBox.getWindowToken(), 0);
            }
            if (isMyTaskItems) {
                setListOfTasks("My Tasks");
            } else {
                setListOfTasks("Assigned Tasks");
            }
        });
        ivSearch1.setOnClickListener(v -> performSearch());

        tvCategory.setOnClickListener(v -> {
            CategoryDialog.newIntance((dialog, categotyid, categotyName) -> {
                tvCategory.setText(categotyName);
                categoryID = categotyid;
                if (categotyName.equalsIgnoreCase("All")) {
                    fabtnAdd.setVisibility(View.GONE);
                    filterType.setVisibility(View.GONE);
                    mLine.setVisibility(View.GONE);
                } else {
                    if (!isMyTaskItems) {
                        if (!isSearch) {
                            fabtnAdd.setVisibility(View.VISIBLE);
                        } else {
                            fabtnAdd.setVisibility(View.GONE);
                        }
                    }
                    filterType.setVisibility(View.VISIBLE);
                    mLine.setVisibility(View.VISIBLE);
                }
                if (isMyTaskItems) {
                    setListOfTasks("My Tasks");
                } else {
                    setListOfTasks("Assigned Tasks");
                }
            }).show(getSupportFragmentManager().beginTransaction(), "CategoryDialog");
        });
        fabtnAdd.setVisibility(View.GONE);
        filterType.setVisibility(View.GONE);
        mLine.setVisibility(View.GONE);

        fabSearch1.setOnClickListener(v -> {
            isSearch = true;
            searchView.setVisibility(View.VISIBLE);
            fabSearch1.setVisibility(View.GONE);
            fabtnAdd.setVisibility(View.GONE);
            mAddView.setVisibility(View.GONE);
        });

        mSearchBox.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                performSearch();
                return true;
            }
            return false;
        });
        filterType.setOnClickListener(v -> {
            showFilterTypeDialog();
        });

        filterType.setText(isMyTaskItems ? "Newly Assigned" : "Newly Added");

        FilterType filterType = new FilterType();
        filterType.setName(isMyTaskItems ? "Newly Assigned" : "Newly Added");
        filterType.setKey("ADD_TIME");
        filterType.setSelected(true);
        filterTypes.add(filterType);

        FilterType filterType1 = new FilterType();
        filterType1.setName("Recently Updated");
        filterType1.setKey("UPDATE_TIME");
        filterTypes.add(filterType1);

        FilterType filterType2 = new FilterType();
        filterType2.setName("Priority Low to High");
        filterType2.setKey("PRIORITY_LOW_TO_HIGH");
        filterTypes.add(filterType2);

        FilterType filterType3 = new FilterType();
        filterType3.setName("Priority High to Low");
        filterType3.setKey("PRIORITY_HIGH_TO_LOW");
        filterTypes.add(filterType3);


        spinner.setItems("My Tasks", "Assigned Tasks");
        spinner.setOnItemSelectedListener((view, position, id, item) -> setListOfTasks(item.toString()));

        myTasks.setOnClickListener(v -> setListOfTasks("My Tasks"));
        assignedTasks.setOnClickListener(v -> setListOfTasks("Assigned Tasks"));


        mObject = getSelectionItem();
        setAddView();
        setListOfTasks("My Tasks");
        IntentFilter filter = new IntentFilter();
        filter.addAction(BROADCAST_ACTION);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(notificationBroadcaster, filter);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    BroadcastReceiver notificationBroadcaster = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String intUserId = intent.getStringExtra("intUserId");
            String intTaskDocumentNo = intent.getStringExtra("intTaskDocumentNo");
            if (!isSearch && isMyTaskItems) {
                getData(intTaskDocumentNo);
            }
        }
    };

    public void getData(String id) {
        RequestParams params = new RequestParams();
        params.put("intTaskDocumentNo", id);
        showProgressDialog(this, "Please wait...");
        ApiCallWithToken(getTaskData, ApiCall.GET_TASK_DATA, params);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(notificationBroadcaster);
    }

    @SuppressLint("RestrictedApi")
    @OnClick({R.id.ivBack, R.id.ivSearch, R.id.fabtnAdd})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.ivSearch:
                isSearch = true;
                searchView.setVisibility(View.VISIBLE);
                fabtnAdd.setVisibility(View.GONE);
                mAddView.setVisibility(View.GONE);
                break;
            case R.id.fabtnAdd:
                Intent i = new Intent(TasksActivity.this, SearchUserActivity.class);
                startActivityForResult(i, 2020);
                break;
        }
    }

    private void showFilterTypeDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dilogue_filter_type);

        RecyclerView recyclerView = dialog.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        filterTypes.get(0).setName(isMyTaskItems ? "Newly Assigned" : "Newly Added");

        FilterTypeAdapter adapter = new FilterTypeAdapter(this, filterTypes);
        recyclerView.setAdapter(adapter);
        adapter.setOnClickListner(position -> {
            for (int i = 0; i < filterTypes.size(); i++) {
                filterTypes.get(i).setSelected(false);
            }
            filterTypes.get(position).setSelected(true);
            mFilterType = filterTypes.get(position).getKey();
            filterType.setText(filterTypes.get(position).getName());
            adapter.notifyDataSetChanged();
            dialog.dismiss();
            if (isSearch) {
                performSearch();
            } else {
                if (isMyTaskItems) {
                    setListOfTasks("My Tasks");
                } else {
                    setListOfTasks("Assigned Tasks");
                }
            }
        });
        dialog.show();
    }

    public void performSearch() {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(mSearchBox.getWindowToken(), 0);
        }
        if (isNetworkAvailable()) {
            String txt = mSearchBox.getText().toString().trim();
            if (!TextUtils.isEmpty(txt)) {
                RequestParams params = new RequestParams();
                if (isMyTaskItems) {
                    params.put("intUserId", mUserDetails.getIntUserId());
                    params.put("intMemberTypeId", mUserDetails.getIntMemberTypeId());
                    params.put("intLevelTypeId", mUserDetails.getIntLevelTypeId());
                    params.put("intSkipCount", 0);
                    params.put("intPagelimit", 1);
                    params.put("searchText", txt);
                    params.put("sortBy", mFilterType);
                    params.put("strCategory", categoryID);
                    showProgressDialog(TasksActivity.this, "Please Wait!");
                    ApiCallWithToken(searchMyTask, ApiCall.SEARCH_MY_TASKS, params);
                } else {
                    params.put("intUserId", mUserDetails.getIntUserId());
                    params.put("searchText", txt);
                    params.put("sortBy", mFilterType);
                    params.put("strCategory", categoryID);
                    showProgressDialog(TasksActivity.this, "Please Wait!");
                    ApiCallWithToken(searchAssignedTask, ApiCall.SEARCH_ASSIGNED_TASK, params);
                }
            } else {
                if (isMyTaskItems) {
                    setListOfTasks("My Tasks");
                } else {
                    setListOfTasks("Assigned Tasks");
                }
            }
        } else {
            showNetworkAlert();
        }
    }

    @SuppressLint("RestrictedApi")
    public void setListOfTasks(String item) {
        isSearch = false;
        mSearchBox.setText("");
        searchView.setVisibility(View.GONE);
        fabSearch1.setVisibility(View.VISIBLE);
        RequestParams params = new RequestParams();
        if (item.equals("My Tasks")) {
            myTasks.setTextColor(ContextCompat.getColor(this, R.color.white));
            assignedTasks.setTextColor(ContextCompat.getColor(this, R.color.unselected));
            isMyTaskItems = true;
            fabtnAdd.setVisibility(View.GONE);
            taskItems.clear();
            taskListAdapter.notifyDataSetChanged();
            if (isNetworkAvailable()) {
                params.put("intUserId", mUserDetails.getIntUserId());
                params.put("intMemberTypeId", mUserDetails.getIntMemberTypeId());
                params.put("intLevelTypeId", mUserDetails.getIntLevelTypeId());
                params.put("intSkipCount", 0);
                params.put("intPagelimit", 1);
                params.put("sortBy", mFilterType);
                params.put("strCategory", categoryID);
                showProgressDialog(TasksActivity.this, "Please Wait!");
                ApiCallWithToken(getMyTask, ApiCall.GET_MYTASK, params);
            } else {
                taskItems.clear();
                RealmResults<TaskItems> itemsRealmResults = mRealm.where(TaskItems.class).equalTo("isMyTask", true).sort("createdDate", Sort.DESCENDING).findAll();
                if (itemsRealmResults != null && itemsRealmResults.size() != 0) {
                    taskItems.addAll(itemsRealmResults);
                    taskListAdapter.notifyDataSetChanged();
                }
            }
        } else {
            assignedTasks.setTextColor(ContextCompat.getColor(this, R.color.white));
            myTasks.setTextColor(ContextCompat.getColor(this, R.color.unselected));
            taskItems.clear();
            taskListAdapter.notifyDataSetChanged();
            isMyTaskItems = false;
            if (tvCategory.getText().toString().trim().equalsIgnoreCase("All")) {
                fabtnAdd.setVisibility(View.GONE);
            } else {
                fabtnAdd.setVisibility(View.VISIBLE);
            }
            if (isNetworkAvailable()) {
                params.put("intUserId", mUserDetails.getIntUserId());
                params.put("sortBy", mFilterType);
                params.put("strCategory", categoryID);
                showProgressDialog(TasksActivity.this, "Please Wait!");
                ApiCallWithToken(getAssignedTask, ApiCall.GET_ASSIGNED_TASK, params);
            } else {
                taskItems.clear();
                RealmResults<TaskItems> itemsRealmResults = mRealm.where(TaskItems.class).equalTo("isMyTask", false).sort("createdDate", Sort.DESCENDING).findAll();
                if (itemsRealmResults != null && itemsRealmResults.size() != 0) {
                    taskItems.addAll(itemsRealmResults);
                    Log.e("taskItems", itemsRealmResults.size() + "....." + taskItems.size() + "....");
                    for (int i = 0; i < taskItems.size(); i++) {
                        Log.e("taskItems_item", taskItems.get(i) + ">..." + "....");
                    }
                    taskListAdapter.notifyDataSetChanged();
                }
            }
        }
        if (filterType.getText().toString().trim().equalsIgnoreCase("Newly Assigned") ||
                filterType.getText().toString().trim().equalsIgnoreCase("Newly Added")) {
            filterType.setText(isMyTaskItems ? "Newly Assigned" : "Newly Added");
        }
        setAddView();
    }

    public void taskAddCall() {
        String task = etAddTask.getText().toString().trim();
        if (TextUtils.isEmpty(task)) {
            Toast.makeText(context, "Please write something about task.", Toast.LENGTH_SHORT).show();
            return;
        }

        RequestParams params = new RequestParams();
        params.put("strDescription", task);
        params.put("intCreateUserId", mUserDetails.getIntUserId());
        params.put("strCategoryName", tvCategory.getText().toString().trim());
        params.put("strCategory", categoryID);
        if (mObject.isUser()) {
            if (mObject.getUsers() != null && mObject.getUsers().size() != 0) {
                JSONArray array = new JSONArray();
                for (SelectionItem item : mObject.getUsers()) {
                    try {
                        JSONObject object = new JSONObject();
                        object.put("id", item.getId());
                        array.put(object);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if (array.length() != 0) {
                    params.put("arryOfobjstrUserCollectionData", array.toString());
                } else {
                    Toast.makeText(context, "Please select users or levels and members for task.", Toast.LENGTH_SHORT).show();
                    return;
                }
            } else {
                Toast.makeText(context, "Please select users or levels and members for task.", Toast.LENGTH_SHORT).show();
                return;
            }
        } else {
            if (mObject.getLevels() != null && mObject.getMemberTypes() != null && mObject.getLevels().size() != 0 && mObject.getMemberTypes().size() != 0) {
                JSONArray array = new JSONArray();
                for (SelectionItem item : mObject.getLevels()) {
                    try {
                        JSONObject object = new JSONObject();
                        object.put("id", item.getId());
                        array.put(object);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                JSONArray array1 = new JSONArray();
                for (SelectionItem item : mObject.getMemberTypes()) {
                    try {
                        JSONObject object = new JSONObject();
                        object.put("id", item.getId());
                        array1.put(object);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if (array.length() != 0 && array1.length() != 0) {
                    params.put("arryOfobjstrLevelCollection", array);
                    params.put("arryOfobjstrMemberCollection", array1);
                } else {
                    Toast.makeText(context, "Please select users or levels and members for task.", Toast.LENGTH_SHORT).show();
                    return;
                }
            } else {
                Toast.makeText(context, "Please select users or levels and members for task.", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        ApiCallWithToken(addTask, ApiCall.ADD_TASK, params);
    }


    public void setAddView() {
        if (mObject == null || mObject.isUser()) {
            if (mObject == null || mObject.getUsers().size() == 0) {
                mAddView.setVisibility(View.GONE);
            } else {
                usersCount.setVisibility(View.VISIBLE);
                otherCount.setVisibility(View.GONE);
                usersCount.setText(mObject.getUsers().size() + " Users");
                if (!isMyTaskItems) {
                    if (tvCategory.getText().toString().trim().equalsIgnoreCase("All")) {
                        mAddView.setVisibility(View.GONE);
                    } else {
                        mAddView.setVisibility(View.VISIBLE);
                    }
                } else {
                    mAddView.setVisibility(View.GONE);
                }
            }
        } else {
            if (mObject.getLevels() != null && mObject.getMemberTypes() != null && mObject.getLevels().size() != 0 && mObject.getMemberTypes().size() != 0) {
                usersCount.setVisibility(View.GONE);
                otherCount.setVisibility(View.VISIBLE);
                otherCount.setText(mObject.getLevels().size() + " Levels and " + mObject.getMemberTypes().size() + "  Member Types");
                if (!isMyTaskItems) {
                    if (tvCategory.getText().toString().trim().equalsIgnoreCase("All")) {
                        mAddView.setVisibility(View.GONE);
                    } else {
                        mAddView.setVisibility(View.VISIBLE);
                    }
                } else {
                    mAddView.setVisibility(View.GONE);
                }
            } else {
                mAddView.setVisibility(View.GONE);
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 5252 || requestCode == 5555) {
            taskListAdapter.onAttachmentResult(requestCode, resultCode, data);
        } else if (requestCode == 2020) {
            if (resultCode == RESULT_OK) {
                Gson gson = new Gson();
                String userData = data.getStringExtra("data");
                mObject = gson.fromJson(userData, SelectionObject.class);
                if (mObject != null) {
                    setSelectionItem(mObject);
                    setAddView();
                }
            }
        } else if (requestCode == 123) {
            boolean isUpdated = data.getBooleanExtra("isUpdated", false);
            int _id = data.getIntExtra("_id", 0);
            int position = data.getIntExtra("position", 0);
//            if (isUpdated) {
//                if (isMyTaskItems) {
//                    setListOfTasks("My Tasks");
//                } else {
//                    setListOfTasks("Assigned Tasks");
//                }
//            }
            TaskItems tItem = mRealm.where(TaskItems.class).equalTo("intTaskDocumentNo", _id).findFirst();
            taskItems.set(position, tItem);
            taskListAdapter.notifyItemChanged(position);

        }
    }


    @SuppressLint("RestrictedApi")
    @Override
    public void OnResponce(JSONObject data, ApiCall type) {

        try {
            Gson gson = new GsonBuilder().create();
            JSONArray array;
            switch (type) {
                case GET_MYTASK:
                case GET_ASSIGNED_TASK:
                    taskItems.clear();
                    array = data.getJSONObject("data").getJSONArray("tasksData");
                    if (array.length() != 0) {
                        mRealm.beginTransaction();
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            TaskItems items = gson.fromJson(object.toString(), TaskItems.class);
                            items.setMyTask(isMyTaskItems);
                            mRealm.copyToRealmOrUpdate(items);
                            this.taskItems.add(0, items);
                        }
                        mRealm.commitTransaction();
                    }
//                    Collections.sort(taskItems, (s1, s2) -> {
//                        long rollno1 = s1.getCreatedDate();
//                        long rollno2 = s2.getCreatedDate();
//                        Date date1 = new Date(rollno1);
//                        Date date2 = new Date(rollno2);
//                        return date2.compareTo(date1);
//                    });
                    taskListAdapter.setIsAssignedTask(!isMyTaskItems);
                    taskListAdapter.notifyDataSetChanged();
//                    setMyTaskItems(this.taskItems);
                    etAddTask.setText("");
                    break;

                case SEARCH_ASSIGNED_TASK:
                case SEARCH_MY_TASKS:
                    taskItems.clear();
                    array = data.getJSONObject("data").getJSONArray("tasksData");
                    if (array.length() != 0) {
                        mRealm.beginTransaction();
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            TaskItems items = gson.fromJson(object.toString(), TaskItems.class);
                            items.setMyTask(isMyTaskItems);
                            mRealm.copyToRealmOrUpdate(items);
                            this.taskItems.add(0, items);
                        }
                        mRealm.commitTransaction();
                    }
//                    Collections.sort(taskItems, (s1, s2) -> {
//                        long rollno1 = s1.getCreatedDate();
//                        long rollno2 = s2.getCreatedDate();
//                        Date date1 = new Date(rollno1);
//                        Date date2 = new Date(rollno2);
//                        return date2.compareTo(date1);
//                    });
                    taskListAdapter.setIsAssignedTask(!isMyTaskItems);
                    taskListAdapter.notifyDataSetChanged();
                    break;
                case GET_TASK_DATA:
                    TaskItems items1 = gson.fromJson(data.getJSONObject("data").getJSONObject("taskData").toString(), TaskItems.class);
                    mRealm.beginTransaction();
                    mRealm.copyToRealmOrUpdate(items1);
                    mRealm.commitTransaction();
                    for (int i = 0; i < taskItems.size(); i++) {
                        if (taskItems.get(i).getIntTaskDocumentNo() == items1.getIntTaskDocumentNo()) {
                            taskItems.remove(i);
                        }
                    }
                    taskItems.add(0, items1);
                    taskListAdapter.notifyDataSetChanged();
                    break;

                case ADD_TASK:
                    if (!isMyTaskItems) {
                        JSONObject object = data.getJSONObject("data").getJSONObject("taskData");
                        mRealm.beginTransaction();
                        TaskItems items = gson.fromJson(object.toString(), TaskItems.class);
                        items.setMyTask(false);
                        mRealm.copyToRealmOrUpdate(items);
                        this.taskItems.add(0, items);
                        mRealm.commitTransaction();
                        taskListAdapter.notifyDataSetChanged();
//                        if (isMyTaskItems) {
//                            setMyTaskItems(this.taskItems);
//                        } else {
//                            setAssignedTaskItems(this.taskItems);
//                        }
                    }
                    etAddTask.setText("");
                    break;

                case GET_USERS_DATA:
                    array = data.getJSONArray("data");
                    if (array.length() != 0) {
                        selectionItemsUsers.clear();
                        for (int i = 0; i < array.length(); i++) {
                            try {
                                JSONObject object = array.getJSONObject(i);
                                SelectionItem item = gson.fromJson(object.toString(), SelectionItem.class);
                                selectionItemsUsers.add(item);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    break;

                case GET_LEVELS:
                    array = data.getJSONArray("data");
                    if (array.length() != 0) {
                        selectionItemsLevels.clear();
                        for (int i = 0; i < array.length(); i++) {
                            try {
                                JSONObject object = array.getJSONObject(i);
                                SelectionItem item = gson.fromJson(object.toString(), SelectionItem.class);
                                selectionItemsLevels.add(item);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    break;

                case GET_MEMBER_TYPES:
                    array = data.getJSONArray("data");
                    if (array.length() != 0) {
                        selectionItemsMembers.clear();
                        for (int i = 0; i < array.length(); i++) {
                            try {
                                JSONObject object = array.getJSONObject(i);
                                SelectionItem item = gson.fromJson(object.toString(), SelectionItem.class);
                                selectionItemsMembers.add(item);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    break;

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        hideProgressDialog(TasksActivity.this);
    }
}
