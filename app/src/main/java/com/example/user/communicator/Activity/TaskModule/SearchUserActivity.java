package com.example.user.communicator.Activity.TaskModule;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.user.communicator.Adapter.TaskAdapters.SearchUserAdapter;
import com.example.user.communicator.CustomViews.CustomEditText;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.TaskModels.SelectionItem;
import com.example.user.communicator.Model.TaskModels.SelectionObject;
import com.example.user.communicator.Model.TaskModels.TaskDetailItem;
import com.example.user.communicator.R;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;


public class SearchUserActivity extends BaseActivity {

    @BindView(R.id.search_user)
    CustomEditText mSearchEdit;

//    @BindView(R.id.search_level)
//    CustomEditText mSearchLevel;

    @BindView(R.id.search_memberType)
    CustomEditText mSearchMemberType;

    @BindView(R.id.ivBack)
    ImageView mBack;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerViewUser;

    @BindView(R.id.normalSearch)
    LinearLayout mNormalSearch;

    @BindView(R.id.advancedSearch)
    LinearLayout mAdvancedSearch;

    @BindView(R.id.tvToolbar)
    CustomTextView tvToolbar;

    @BindView(R.id.recyclerViewLevel)
    RecyclerView mRecyclerViewLevel;

    @BindView(R.id.recyclerViewMemberType)
    RecyclerView mRecyclerViewMemberType;

    @BindView(R.id.fabtnAdd)
    FloatingActionButton fabtnAdd;

    LinearLayoutManager linearLayoutManager, linearLayoutManager1, linearLayoutManager2;
    ArrayList<SelectionItem> selectionItemsUsers = new ArrayList<>();
    ArrayList<SelectionItem> selectionItemsLevels = new ArrayList<>();
    ArrayList<SelectionItem> selectionItemsMembers = new ArrayList<>();


    ArrayList<SelectionItem> defaultUsers = new ArrayList<>();
    ArrayList<SelectionItem> defaultLevels = new ArrayList<>();
    ArrayList<SelectionItem> defaultMembers = new ArrayList<>();

    SearchUserAdapter adapterUser, adapterLevel, adapterMemberType;
    boolean isUser = false;

    @BindView(R.id.ivAdvanced)
    CustomTextView ivAdvanced;
    boolean isFromEditTask = false;
    Datum mUserDetails;
    Realm mRealm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_user);
        ButterKnife.bind(this);
        isFromEditTask = getIntent().getBooleanExtra("isFromEditTask", false);
        mUserDetails = getLoginData();
        mRealm = Realm.getDefaultInstance();
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        linearLayoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        linearLayoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerViewUser.setLayoutManager(linearLayoutManager);
        mRecyclerViewLevel.setLayoutManager(linearLayoutManager1);
        mRecyclerViewMemberType.setLayoutManager(linearLayoutManager2);
        adapterUser = new SearchUserAdapter(this, selectionItemsUsers);
        adapterLevel = new SearchUserAdapter(this, selectionItemsLevels);
        adapterMemberType = new SearchUserAdapter(this, selectionItemsMembers);
        mRecyclerViewUser.setAdapter(adapterUser);
        mRecyclerViewLevel.setAdapter(adapterLevel);
        mRecyclerViewMemberType.setAdapter(adapterMemberType);
//        adapterUser.setOnClickListner(onClickListner);
//        adapterLevel.setOnClickListner(onClickListner1);
//        adapterMemberType.setOnClickListner(onClickListner2);

        getData();

        mSearchEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String txt = mSearchEdit.getText().toString().trim();
                if (isUser) {
                    if (TextUtils.isEmpty(txt)) {
                        txt = "";
                    }
                    adapterUser.filter(txt);
                } else {
                    if (TextUtils.isEmpty(txt)) {
                        txt = "";
                    }
                    adapterLevel.filter(txt);
                }
            }
        });

//        mSearchLevel.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                String txt = mSearchLevel.getText().toString().trim();
//                if (TextUtils.isEmpty(txt)) {
//                    txt = "";
//                }
//                adapterLevel.filter(txt);
//            }
//        });


        mSearchMemberType.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String txt = mSearchMemberType.getText().toString().trim();
                if (TextUtils.isEmpty(txt)) {
                    txt = "";
                }
                adapterMemberType.filter(txt);
            }
        });


        mBack.setOnClickListener(v -> finish());
        ivAdvanced.setOnClickListener(v -> setSearchData());
        fabtnAdd.setOnClickListener(v -> {
                    if (isFromEditTask) {
                        alert();
                    } else {
                        onBack();
                    }
                }
        );
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }


    public void setSearchData() {
        if (isUser) {
            isUser = false;
            mNormalSearch.setVisibility(View.GONE);
            mAdvancedSearch.setVisibility(View.VISIBLE);
            mSearchEdit.setHint("Search Level");
            mSearchEdit.setText("");
            tvToolbar.setText("Member Types and Levels");
            ivAdvanced.setText("Normal");
        } else {
            isUser = true;
            mNormalSearch.setVisibility(View.VISIBLE);
            mAdvancedSearch.setVisibility(View.GONE);
            mSearchEdit.setHint("Search Users");
            mSearchEdit.setText("");
            tvToolbar.setText("Users");
            ivAdvanced.setText("Advance");
        }
    }


    public void getData() {
        RequestParams params = new RequestParams();

        params.put("intLevelTypeId", mUserDetails.getIntLevelTypeId());
        params.put("intMemberTypeId", mUserDetails.getIntMemberTypeId());
//        params.put("intLevelTypeId", "5a673111239b610be398930d");
//        params.put("intMemberTypeId", "5a90f40c8deb341eca718220");
        String id = "";
        for (int i = 0; i < mUserDetails.getArryObjModuleUser().get(0).getObjModuledetails().size(); i++) {
            if (mUserDetails.getArryObjModuleUser().get(0).getObjModuledetails().get(i).getStrModuleName().equalsIgnoreCase("TASK")) {
                id = mUserDetails.getArryObjModuleUser().get(0).getObjModuledetails().get(i).getId();
            }
        }
        params.put("intModuleId", id);


        showProgressDialog(this, "Please Wait...");
        ApiCallWithToken(getUsers, ApiCall.GET_USERS_DATA, params);

    }

    @Override
    public void OnResponce(JSONObject data, ApiCall type) {
        JSONArray array = null;
        try {
            array = data.getJSONArray("data");

            if (type == ApiCall.GET_USERS_DATA) {
                if (array.length() != 0) {
                    Gson gson = new Gson();
                    selectionItemsUsers.clear();
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        SelectionItem item = gson.fromJson(object.toString(), SelectionItem.class);
                        if (!item.getItemName().contains(mUserDetails.getStrFirstName())) {
                            selectionItemsUsers.add(item);
                        }
                    }

                    adapterUser.setAllTaskItems(selectionItemsUsers);
                    adapterUser.notifyDataSetChanged();
                    RequestParams params = new RequestParams();
//                    params.put("intLevelTypeId", mUserDetails.getData().get(0).getIntLevelTypeId());
                    String id = "";
                    for (int i = 0; i < mUserDetails.getArryObjModuleUser().get(0).getObjModuledetails().size(); i++) {
                        if (mUserDetails.getArryObjModuleUser().get(0).getObjModuledetails().get(i).getStrModuleName().equalsIgnoreCase("TASK")) {
                            id = mUserDetails.getArryObjModuleUser().get(0).getObjModuledetails().get(i).getId();
                        }
                    }
                    params.put("intModuleId", id);

                    params.put("intLevelTypeId", mUserDetails.getIntLevelTypeId());
                    params.put("intMemberTypeId", mUserDetails.getIntMemberTypeId());
//                    params.put("intLevelTypeId", "5a673111239b610be398930d");
//                    params.put("intMemberTypeId", "5a90f40c8deb341eca718220");
//                    params.put("intMemberTypeId", mUserDetails.getData().get(0).getIntMemberTypeId());
                    ApiCallWithToken(getLevels, ApiCall.GET_LEVELS, params);
                }
            }

            if (type == ApiCall.GET_LEVELS) {
                if (array.length() != 0) {
                    Gson gson = new Gson();
                    selectionItemsLevels.clear();
                    for (int i = 0; i < array.length(); i++) {

                        JSONObject object = array.getJSONObject(i);
                        SelectionItem item = gson.fromJson(object.toString(), SelectionItem.class);
                        selectionItemsLevels.add(item);

                    }
                    adapterLevel.setAllTaskItems(selectionItemsLevels);
                    adapterLevel.notifyDataSetChanged();
                    RequestParams params = new RequestParams();
//                    params.put("intLevelTypeId", mUserDetails.getData().get(0).getIntLevelTypeId());
                    String id = "";
                    for (int i = 0; i < mUserDetails.getArryObjModuleUser().get(0).getObjModuledetails().size(); i++) {
                        if (mUserDetails.getArryObjModuleUser().get(0).getObjModuledetails().get(i).getStrModuleName().equalsIgnoreCase("Task")) {
                            id = mUserDetails.getArryObjModuleUser().get(0).getObjModuledetails().get(i).getId();
                        }
                    }
                    params.put("intModuleId", id);
                    params.put("intLevelTypeId", "5a673111239b610be398930d");
                    params.put("intMemberTypeId", "5a90f40c8deb341eca718220");
//                    params.put("intMemberTypeId", mUserDetails.getIntMemberTypeId());
                    ApiCallWithToken(getMembers, ApiCall.GET_MEMBER_TYPES, params);
                }
            }

            if (type == ApiCall.GET_MEMBER_TYPES) {
                hideProgressDialog(SearchUserActivity.this);
                if (array.length() != 0) {
                    Gson gson = new Gson();
                    selectionItemsMembers.clear();
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        SelectionItem item = gson.fromJson(object.toString(), SelectionItem.class);
                        selectionItemsMembers.add(item);
                    }

                    adapterMemberType.setAllTaskItems(selectionItemsMembers);
                    adapterMemberType.notifyDataSetChanged();
                    setDefultData();
                }
            }
        } catch (JSONException e) {
            hideProgressDialog(this);
            e.printStackTrace();
        }
    }

    @Override
    public void OnError(JSONObject object, ApiCall type) {
        hideProgressDialog(this);
    }

    public void setDefultData() {
        isUser = false;
        if (isFromEditTask && getSelectionEditItem() == null) {
            String task_Id = getIntent().getStringExtra("_id");
            TaskDetailItem items = mRealm.where(TaskDetailItem.class).equalTo("intTaskDocumentNo",
                    Integer.valueOf(task_Id)).findFirst();
            if (items.getUsers() != null && items.getUsers().size() != 0) {
                isUser = false;
                for (int i = 0; i < items.getUsers().size(); i++) {
                    boolean isF = false;
                    for (int j = 0; j < selectionItemsUsers.size() && !isF; j++) {
                        if (items.getUsers().get(i).get_id().equalsIgnoreCase(selectionItemsUsers.get(j).getId())) {
                            isF = true;
                            selectionItemsUsers.get(j).setSelected(true);
                        }
                    }
                }
            } else {
                if (items.getLevelsOne().size() != 0) {
                    for (int i = 0; i < items.getLevelsOne().size(); i++) {
                        boolean isF = false;
                        for (int j = 0; j < selectionItemsLevels.size() && !isF; j++) {
                            if (items.getLevelsOne().get(i).get_id().equalsIgnoreCase(selectionItemsLevels.get(j).getId())) {
                                isF = true;
                                isUser = true;
                                selectionItemsLevels.get(j).setSelected(true);
                            }
                        }
                    }
                }
                if (items.getLevelsTwo().size() != 0) {
                    for (int i = 0; i < items.getLevelsTwo().size(); i++) {
                        boolean isF = false;
                        for (int j = 0; j < selectionItemsLevels.size() && !isF; j++) {
                            if (items.getLevelsTwo().get(i).get_id().equalsIgnoreCase(selectionItemsLevels.get(j).getId())) {
                                isF = true;
                                isUser = true;
                                selectionItemsLevels.get(j).setSelected(true);
                            }
                        }
                    }
                }
                if (items.getLevelsThree().size() != 0) {
                    for (int i = 0; i < items.getLevelsThree().size(); i++) {
                        boolean isF = false;
                        for (int j = 0; j < selectionItemsLevels.size() && !isF; j++) {
                            if (items.getLevelsThree().get(i).get_id().equalsIgnoreCase(selectionItemsLevels.get(j).getId())) {
                                isF = true;
                                isUser = true;
                                selectionItemsLevels.get(j).setSelected(true);
                            }
                        }
                    }
                }
                if (items.getLevelsFour().size() != 0) {
                    for (int i = 0; i < items.getLevelsFour().size(); i++) {
                        boolean isF = false;
                        for (int j = 0; j < selectionItemsLevels.size() && !isF; j++) {
                            if (items.getLevelsFour().get(i).get_id().equalsIgnoreCase(selectionItemsLevels.get(j).getId())) {
                                isF = true;
                                isUser = true;
                                selectionItemsLevels.get(j).setSelected(true);
                            }
                        }
                    }
                }
                if (items.getMemberTypes().size() != 0) {
                    for (int i = 0; i < items.getMemberTypes().size(); i++) {
                        boolean isF = false;
                        for (int j = 0; j < selectionItemsMembers.size() && !isF; j++) {
                            if (items.getMemberTypes().get(i).get_id().equalsIgnoreCase(selectionItemsMembers.get(j).getId())) {
                                isF = true;
                                isUser = true;
                                selectionItemsMembers.get(j).setSelected(true);
                            }
                        }
                    }
                }

            }
        } else {
            SelectionObject object1;
            if (isFromEditTask) {
                object1 = getSelectionEditItem();
            } else {
                object1 = getSelectionItem();//removed becz once the user selected it remainsd same  //new SelectionObject();//
            }
            if (object1 == null || object1.isUser()) {
                isUser = false;
                if (object1 != null && object1.getUsers().size() != 0) {
                    for (int i = 0; i < object1.getUsers().size(); i++) {
                        boolean isF = false;
                        for (int j = 0; j < selectionItemsUsers.size() && !isF; j++) {
                            if (object1.getUsers().get(i).getItemName().equalsIgnoreCase(selectionItemsUsers.get(j).getItemName())) {
                                isF = true;
                                selectionItemsUsers.get(j).setSelected(true);
                            }
                        }
                    }
                }
            } else {
                if (object1.getLevels().size() != 0 && object1.getMemberTypes().size() != 0) {
                    for (int i = 0; i < object1.getLevels().size(); i++) {
                        boolean isF = false;
                        for (int j = 0; j < selectionItemsLevels.size() && !isF; j++) {
                            if (object1.getLevels().get(i).getItemName().equalsIgnoreCase(selectionItemsLevels.get(j).getItemName())) {
                                isF = true;
                                isUser = true;
                                selectionItemsLevels.get(j).setSelected(true);
                            }
                        }
                    }
                    for (int i = 0; i < object1.getMemberTypes().size(); i++) {
                        boolean isF = false;
                        for (int j = 0; j < selectionItemsMembers.size() && !isF; j++) {
                            if (object1.getMemberTypes().get(i).getItemName().equalsIgnoreCase(selectionItemsMembers.get(j).getItemName())) {
                                isF = true;
                                isUser = true;
                                selectionItemsMembers.get(j).setSelected(true);
                            }
                        }
                    }
                }
            }
        }
        adapterUser.notifyDataSetChanged();
        adapterLevel.notifyDataSetChanged();
        adapterMemberType.notifyDataSetChanged();
        setSearchData();
    }


    public void alert() {
        String task_Id = getIntent().getStringExtra("_id");
        TaskDetailItem item = mRealm.where(TaskDetailItem.class).equalTo("intTaskDocumentNo",
                Integer.valueOf(task_Id)).findFirst();
        boolean isChanged = false;
        boolean isBreak = false;
        if (isUser) {
            ArrayList<SelectionItem> selectionItems = adapterUser.getItems();
            if (item.getUsers() != null && item.getUsers().size() != 0) {
                for (int z = 0; z < item.getUsers().size() && !isBreak; z++) {
                    for (int k = 0; k < selectionItems.size() && !isChanged; k++) {
                        if (selectionItems.get(k).isSelected()) {
                            if (item.getUsers().get(z).get_id().equalsIgnoreCase(selectionItems.get(k).getId())) {
                                isChanged = true;
                            }
                        }
                    }
                    if (!isChanged) {
                        isBreak = true;
                    }
                }
            }
            if (isBreak) {
                showAlert();
            } else {
                onBack();
            }
        } else {
            ArrayList<SelectionItem> selectionItemsLevels = adapterLevel.getItems();
            ArrayList<SelectionItem> selectionItemsMemberTypes = adapterMemberType.getItems();

            if (item.getLevelsOne() != null && item.getLevelsOne().size() != 0) {
                for (int z = 0; z < item.getLevelsOne().size() && !isBreak; z++) {
                    for (int k = 0; k < selectionItemsLevels.size() && !isChanged; k++) {
                        if (selectionItemsLevels.get(k).isSelected()) {
                            if (item.getLevelsOne().get(z).get_id().equalsIgnoreCase(selectionItemsLevels.get(k).getId())) {
                                isChanged = true;
                            }
                        }
                    }
                    if (!isChanged) {
                        isBreak = true;
                    }
                }
            }
            if (item.getLevelsTwo() != null && item.getLevelsTwo().size() != 0) {
                for (int z = 0; z < item.getLevelsTwo().size() && !isBreak; z++) {
                    for (int k = 0; k < selectionItemsLevels.size() && !isChanged; k++) {
                        if (selectionItemsLevels.get(k).isSelected()) {
                            if (item.getLevelsTwo().get(z).get_id().equalsIgnoreCase(selectionItemsLevels.get(k).getId())) {
                                isChanged = true;
                            }
                        }
                    }
                    if (!isChanged) {
                        isBreak = true;
                    }
                }
            }
            if (item.getLevelsThree() != null && item.getLevelsThree().size() != 0) {
                for (int z = 0; z < item.getLevelsThree().size() && !isBreak; z++) {
                    for (int k = 0; k < selectionItemsLevels.size() && !isChanged; k++) {
                        if (selectionItemsLevels.get(k).isSelected()) {
                            if (item.getLevelsThree().get(z).get_id().equalsIgnoreCase(selectionItemsLevels.get(k).getId())) {
                                isChanged = true;
                            }
                        }
                    }
                    if (!isChanged) {
                        isBreak = true;
                    }
                }
            }
            if (item.getLevelsFour() != null && item.getLevelsFour().size() != 0) {
                for (int z = 0; z < item.getLevelsFour().size() && !isBreak; z++) {
                    for (int k = 0; k < selectionItemsLevels.size() && !isChanged; k++) {
                        if (selectionItemsLevels.get(k).isSelected()) {
                            if (item.getLevelsFour().get(z).get_id().equalsIgnoreCase(selectionItemsLevels.get(k).getId())) {
                                isChanged = true;
                            }
                        }
                    }
                    if (!isChanged) {
                        isBreak = true;
                    }
                }
            }
            if (item.getMemberTypes() != null && item.getMemberTypes().size() != 0) {
                for (int z = 0; z < item.getMemberTypes().size() && !isBreak; z++) {
                    for (int k = 0; k < selectionItemsMemberTypes.size() && !isChanged; k++) {
                        if (selectionItemsMemberTypes.get(k).isSelected()) {
                            if (item.getMemberTypes().get(z).get_id().equalsIgnoreCase(selectionItemsMemberTypes.get(k).getId())) {
                                isChanged = true;
                            }
                        }
                    }
                    if (!isChanged) {
                        isBreak = true;
                    }
                }
            }
            if (isBreak) {
                showAlert();
            } else {
                onBack();
            }
        }
    }


    public void showAlert() {
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Will lost all the activities relevant to the user you had removed , Are you sure to continue?");
        alertDialogBuilder.setPositiveButton("Yes", (dialog, which) -> {
            dialog.dismiss();
            onBack();
        });
        alertDialogBuilder.setNegativeButton("No", (d, w) -> {
            d.dismiss();
        });
        AlertDialog dialog = alertDialogBuilder.create();
        dialog.show();
    }

    public void onBack() {
        SelectionObject object = new SelectionObject();
        Gson gson = new Gson();
        Intent i = new Intent();
        if (isUser) {
            ArrayList<SelectionItem> items = new ArrayList<>();
            ArrayList<SelectionItem> selectionItems = adapterUser.getItems();
            for (int j = 0; j < selectionItems.size(); j++) {
                if (selectionItems.get(j).isSelected()) {
                    items.add(selectionItems.get(j));
                }
            }
            object.setUser(true);
            object.setUsers(items);

        } else {
            object.setUser(false);
            ArrayList<SelectionItem> itemsLevels = new ArrayList<>();
            ArrayList<SelectionItem> itemsMemberTypes = new ArrayList<>();
            ArrayList<SelectionItem> selectionItemsLevels = adapterLevel.getItems();
            ArrayList<SelectionItem> selectionItemsMemberTypes = adapterMemberType.getItems();

            for (int j = 0; j < selectionItemsLevels.size(); j++) {
                if (selectionItemsLevels.get(j).isSelected()) {
                    itemsLevels.add(selectionItemsLevels.get(j));
                }
            }

            for (int j = 0; j < selectionItemsMemberTypes.size(); j++) {
                if (selectionItemsMemberTypes.get(j).isSelected()) {
                    itemsMemberTypes.add(selectionItemsMemberTypes.get(j));
                }
            }
            object.setLevels(itemsLevels);
            object.setMemberTypes(itemsMemberTypes);
        }
        Log.e("OnBack_data22..", "..." + gson.toJson(object));
        i.putExtra("data", gson.toJson(object));
        setResult(RESULT_OK, i);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
