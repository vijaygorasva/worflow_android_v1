package com.example.user.communicator.Activity.TaskModule.Task;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.user.communicator.Activity.TaskModule.BaseActivity;
import com.example.user.communicator.CustomViews.CustomEditText;
import com.example.user.communicator.CustomViews.CustomeViewPager;
import com.example.user.communicator.Fragment.DynamicCategoryFragment;
import com.example.user.communicator.Model.Filter;
import com.example.user.communicator.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

import static com.example.user.communicator.Activity.TaskModule.Task.Interface.onChangeText;
import static com.example.user.communicator.Activity.TaskModule.Task.Interface.onPerformSearch;

public class TaskTabActivity extends BaseActivity implements DynamicCategoryFragment.OnCategorySelectedListener {

    @BindView(R.id.searchBox)
    CustomEditText mSearchBox;

    @BindView(R.id.ivBack1)
    ImageView ivBack1;

    @BindView(R.id.ivSearch1)
    ImageView ivSearch1;

    @BindView(R.id.SearchView)
    LinearLayout searchView;

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    @BindView(R.id.pager)
    CustomeViewPager viewPager;


    MyTaskFragment myTaskFragment;
    AssignedTaskFragmnet assignedTaskFragmnet;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_tab);
        ButterKnife.bind(this);


        tabLayout.addTab(tabLayout.newTab().setText("My Tasks"));
        tabLayout.addTab(tabLayout.newTab().setText("Assign Tasks"));

        myTaskFragment = MyTaskFragment.newInstance(viewPager);
        assignedTaskFragmnet = AssignedTaskFragmnet.newInstance(viewPager);

        setViewPager();

        ivBack1.setOnClickListener(v -> {
            performSearch(false);
        });
        ivSearch1.setOnClickListener(v -> performSearch(true));

        mSearchBox.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                performSearch(true);
                return true;
            }
            return false;
        });
        mSearchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (onChangeText.size() != 0) {
                    for (Interface.OnSearchTextListner onSearchTextListner : onChangeText) {
                        onSearchTextListner.OnChangeText(mSearchBox.getText().toString());
                    }
                }
            }
        });
        Interface.setOnSearchViewVisibilityListner(onSearchViewVisibilityListner);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() == 1) {
            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                if (fragment instanceof AssignedTaskFragmnet) {
                    if (!((IOnBackPressed) fragment).onBackPressed()) {
                        super.onBackPressed();
                    }
                } else {
                    super.onBackPressed();
                }
            }
        } else {
            super.onBackPressed();
        }
    }

    Interface.OnSearchViewVisibilityListner onSearchViewVisibilityListner = visibility -> {
        searchView.setVisibility(visibility);
        if (visibility == View.GONE) {
            mSearchBox.setText("");
        }
    };

    public void performSearch(boolean search) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(mSearchBox.getWindowToken(), 0);
        }
        if (onPerformSearch.size() != 0) {
            for (Interface.OnPerformSearchListner onPerformSearch : onPerformSearch) {
                onPerformSearch.OnPerformSearch(search);
            }
        }
    }

    public void setViewPager() {
        final PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                RealmResults<Filter> filterRealmResults = realm.where(Filter.class).findAll();
                filterRealmResults.deleteAllFromRealm();
                realm.commitTransaction();
                performSearch(false);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public class PagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, int NumOfTabs) {
            super(fm);
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return myTaskFragment;
                case 1:
                    return assignedTaskFragmnet;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }

    @Override
    public void onCategorySelected(Boolean isActive, String tabName, String tabID, String mainCat, String mainCatID, String subCatID, String subCatName) {
        if (viewPager.getCurrentItem() == 0) {
            myTaskFragment.getSelectedCategory(tabName, tabID, mainCat, mainCatID, subCatID, subCatName);
        } else if (viewPager.getCurrentItem() == 1) {
            assignedTaskFragmnet.getSelectedCategory(tabName, tabID, mainCat, mainCatID, subCatID, subCatName);
        }
    }
}
