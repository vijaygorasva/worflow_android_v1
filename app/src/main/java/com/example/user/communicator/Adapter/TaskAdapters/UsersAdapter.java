package com.example.user.communicator.Adapter.TaskAdapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.TaskModels.Duration;
import com.example.user.communicator.Model.TaskModels.SelectionItems.User;
import com.example.user.communicator.Model.TaskModels.TaskDetailItem;
import com.example.user.communicator.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;


public class UsersAdapter extends RecyclerView.Adapter<UsersAdapter.ViewHolder> {

    private Context context;
    private ArrayList<User> taskItems;
    TaskDetailItem mItem;
    private OnClickListner onClick;


    public UsersAdapter(Context context, ArrayList<User> taskItems, TaskDetailItem mItem) {
        this.taskItems = taskItems;
        this.context = context;
        this.mItem = mItem;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_duration_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(taskItems.get(position));
    }

    @Override
    public int getItemCount() {
        return taskItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setOnClickListner(OnClickListner onClickListner) {
        this.onClick = onClickListner;
    }

    private String getMillsToDate(long mills) {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
        return df.format(new Date(mills));
    }

    private String getMillsToDate1(long mills) {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        return df.format(new Date(mills));
    }


    private String getDiffrenece(long startDate, long endDate) {

//        long difference = (endDate - startDate) + 321153;
        long difference = (endDate - startDate);
        long sec = difference / 1000 % 60;
        long min = difference / (60 * 1000) % 60;
        long hours = difference / (60 * 60 * 1000) % 24;
        long days = difference / (24 * 60 * 60 * 1000);
        hours = (hours < 0 ? -hours : hours);

        return (days != 0 ? (days != 1 ? days + " days " : days + " day ") : "") +
                (hours != 0 ? (hours != 1 ? hours + " hours " : hours + " hour ") : "") +
                (min != 0 ? (min != 1 ? min + " minutes " : min + " minute ") : "") +
                (sec != 0 ? (sec != 1 ? sec + " seconds " : sec + " second ") : " ");
    }


    public interface OnClickListner {
        void OnClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.timeLine)
        CustomTextView timeline;

        @BindView(R.id.name)
        CustomTextView name;

        @BindView(R.id.totaltime)
        CustomTextView totalTime;

        @BindView(R.id.d)
        CustomTextView dTxt;

        @BindView(R.id.day_count)
        CustomTextView day_count;

        @BindView(R.id.hours_count)
        CustomTextView hours_count;

        @BindView(R.id.minutes_count)
        CustomTextView minutes_count;

        @BindView(R.id.days)
        LinearLayout mDays;

        @BindView(R.id.line)
        View line;

        @BindView(R.id.hours)
        LinearLayout mHours;

        @BindView(R.id.minutes)
        LinearLayout mMinutes;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        @SuppressLint("SetTextI18n")
        public void bind(final User item) {

            name.setText(item.getStrFirstName() + " " + item.getStrLastName());

            if (item.getDurations().size() != 0) {
                Duration duration = item.getDurations().get(0);
                if (duration.getEndTime() != 0 && !TextUtils.isEmpty(getMillsToDate(duration.getEndTime()))) {
                    timeline.setText(getMillsToDate(duration.getStartTime()) + " to " + getMillsToDate(duration.getEndTime()));
                } else {
                    timeline.setVisibility(View.GONE);
                }

                long difference = (duration.getEndTime() - duration.getStartTime());
                long sec = difference / 1000 % 60;
                long min = difference / (60 * 1000) % 60;
                long hours = difference / (60 * 60 * 1000) % 24;
                long days = difference / (24 * 60 * 60 * 1000);

                String d = (days != 0 ? (days != 1 ? "days " : "day ") : "");

                if (days != 0) {
                    mDays.setVisibility(View.VISIBLE);
                    day_count.setText(String.valueOf(days));
                    dTxt.setText(d);
                } else {
                    mDays.setVisibility(View.GONE);
                }
                if (hours != 0) {
                    mHours.setVisibility(View.VISIBLE);
                    hours_count.setText(String.valueOf(hours));
                } else {
                    mHours.setVisibility(View.GONE);
                }
                if (min != 0) {
                    mMinutes.setVisibility(View.VISIBLE);
                    minutes_count.setText(String.valueOf(min));
                } else {
                    mMinutes.setVisibility(View.GONE);
                }
                if (days == 0 && hours == 0 && min == 0) {
                    if (sec != 0) {
                        mDays.setVisibility(View.VISIBLE);
                        day_count.setText(String.valueOf(sec));
                        dTxt.setText("sec");
                    }
                }

            } else {
                mDays.setVisibility(View.GONE);
                mHours.setVisibility(View.GONE);
                mMinutes.setVisibility(View.GONE);
                timeline.setVisibility(View.GONE);
            }

//            if (mItem.getDatStartToDueDate().size() != 0) {
//                totalTime.setText(getMillsToDate1(mItem.getDatStartToDueDate().get(0)) + " to " +
//                        getMillsToDate1(mItem.getDatStartToDueDate().get(1)));
//            } else {
//                totalTime.setVisibility(View.GONE);
//            }
            if (getAdapterPosition() == taskItems.size() - 1) {
                line.setVisibility(View.GONE);
            } else {
                line.setVisibility(View.VISIBLE);
            }


        }
    }
}
