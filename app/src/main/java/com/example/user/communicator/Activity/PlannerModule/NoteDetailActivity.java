package com.example.user.communicator.Activity.PlannerModule;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.icu.util.TimeZone;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.user.communicator.Activity.TaskModule.ImageShowActivity;
import com.example.user.communicator.Adapter.PlannerAdapter.PlannerSectionAdapter;
import com.example.user.communicator.Application;
import com.example.user.communicator.CustomViews.CustomEditText;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Dialog.AddAudioDialog;
import com.example.user.communicator.Dialog.ReminderDialog;
import com.example.user.communicator.Model.Colleborate.ColleborateDatum;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.Notes.AudFile;
import com.example.user.communicator.Model.Notes.AudioDetails;
import com.example.user.communicator.Model.Notes.ImgDetails;
import com.example.user.communicator.Model.Notes.ImgFile;
import com.example.user.communicator.Model.Notes.Item;
import com.example.user.communicator.Model.Notes.ObjRemainder;
import com.example.user.communicator.Model.Notes.TodoListItem;
import com.example.user.communicator.Model.Notes.Undo;
import com.example.user.communicator.Model.Notes.UndoCheckboxModel;
import com.example.user.communicator.Model.Notes.UndoCheckboxes;
import com.example.user.communicator.Model.Notes.UndoTexts;
import com.example.user.communicator.Model.Notes.UndoTextsModel;
import com.example.user.communicator.Nammu.Nammu;
import com.example.user.communicator.R;
import com.example.user.communicator.Service.MyService;
import com.example.user.communicator.Utility.DateUtils;
import com.example.user.communicator.Utility.Functions;
import com.example.user.communicator.Utility.ImagePicker;
import com.example.user.communicator.Utility.RealPathUtil;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import net.ralphpina.permissionsmanager.PermissionsManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

//import java.util.TimeZone;

public class NoteDetailActivity extends BaseActivity {//} implements ConnectivityReceiver.ConnectivityReceiverListener {


    //4421
    @BindView(R.id.ivPin)
    ImageView ivPin;
    @BindView(R.id.tvEdited)
    TextView tvEdited;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.rlCheckbox)
    LinearLayout rlCheckbox;
    @BindView(R.id.ivConvert)
    ImageView ivConvert;
    @BindView(R.id.ivAdd)
    ImageView ivAdd;
    @BindView(R.id.ivImageOne)
    ImageView ivImageOne;
    @BindView(R.id.ivImageTwo)
    ImageView ivImageTwo;
    @BindView(R.id.ivImageThree)
    ImageView ivImageThree;
    @BindView(R.id.flImage)
    FrameLayout flImage;
    @BindView(R.id.tvToolbar)
    TextView tvToolbar;
    @BindView(R.id.toolbar)
    RelativeLayout toolbar;
    @BindView(R.id.tvTitle)
    EditText tvTitle;
    @BindView(R.id.tvCheckbox)
    TextView tvCheckbox;
    @BindView(R.id.llUndos)
    LinearLayout llUndos;
    @BindView(R.id.llBottom)
    RelativeLayout llBottom;
    @BindView(R.id.tvTimer)
    TextView tvTimer;
    @BindView(R.id.llFileUpload)
    LinearLayout llFileUpload;
    @BindView(R.id.rlMain)
    RelativeLayout rlMain;
    @BindView(R.id.llColleborate)
    LinearLayout llColleborate;
    @BindView(R.id.ivRedo)
    ImageView ivRedo;
    @BindView(R.id.ivUndo)
    ImageView ivUndo;
    MediaPlayer mediaPlayerOne, mediaPlayerTwo, mediaPlayerThree;
    Boolean isAudioPlaying = false;


    @BindView(R.id.llAudioOne)
    LinearLayout llAudioOne;
    @BindView(R.id.ivPlayOne)
    ImageView ivPlayOne;
    @BindView(R.id.seekBarOne)
    SeekBar seekBarOne;
    @BindView(R.id.tvAudioOne)
    TextView tvAudioOne;
    @BindView(R.id.ivAudioDeleteOne)
    ImageView ivAudioDeleteOne;

    @BindView(R.id.tvMoreAudio)
    CustomTextView tvMoreAudio;

    @BindView(R.id.llAudioTwo)
    LinearLayout llAudioTwo;
    @BindView(R.id.ivPlayTwo)
    ImageView ivPlayTwo;
    @BindView(R.id.seekBarTwo)
    SeekBar seekBarTwo;
    @BindView(R.id.tvAudioTwo)
    TextView tvAudioTwo;
    @BindView(R.id.ivAudioDeleteTwo)
    ImageView ivAudioDeleteTwo;

    @BindView(R.id.llAudioThree)
    LinearLayout llAudioThree;
    @BindView(R.id.ivPlayThree)
    ImageView ivPlayThree;
    @BindView(R.id.seekBarThree)
    SeekBar seekBarThree;
    @BindView(R.id.tvAudioThree)
    TextView tvAudioThree;
    @BindView(R.id.ivAudioDeleteThree)
    ImageView ivAudioDeleteThree;
    int kCount = 0;
    Boolean isAudioExpanded = false;

//    @BindView(R.id.llImages)
//    FrameLayout llImages;

    CustomEditText editText;
    Boolean isCheckboxChanged = false;
    String noteValue;
    Boolean isPinned = false;
    String id;
    StringBuffer toSingleString;//checkbox string to single string
    String imgPath = "NoData", imgStatus = "Online", audStatus = "Online";// audPath = "NoData",
    Boolean isCam = true;//true= camera,false=gallery
    String typeItem = "delete";
    String isNewImage = "no", isNewAudio = "no";
    String activityType, isImage = "no";
    //    int height, weight;
    int colorGrey = Color.parseColor("#A1A1A1"); //The color u want
    int colorBlack = Color.parseColor("#000000"); //The color u want
    String strUserId;
    String isCheckBox = "Checkboxes";
    int width;
    String plannerID;
    int positionItem;
    Uri file;
    File fileProfile = new File("null");
    ArrayList<Item> oneItemServer = new ArrayList<>();
    ArrayList<TodoListItem> arrayToDoList = new ArrayList<>();

    ObjRemainder objRemainder = new ObjRemainder();
    ObjRemainder objRemainderOld = new ObjRemainder();
    List<EditText> alleditText = new ArrayList<EditText>();
    List<EditText> alleditTextCheckbox = new ArrayList<EditText>();
    ArrayList<TodoListItem> todoCheckbxText = new ArrayList<>();
    ArrayList<String> toSingleStringList = new ArrayList<>();
    ArrayList<ColleborateDatum> arryCollaboratUserData = new ArrayList<>();
    RealmList<String> collUsers = new RealmList<>();
    int REQUEST_CODE = 4;
    Display display;
    Datum mUserDetails;
    CustomEditText etTextCheckBox = null;
    java.text.SimpleDateFormat sdf;
    ViewGroup mLinearLayout1;
    View view1;
    ViewGroup mLinearLayout;
    LayoutInflater inflater;
    Typeface font_roboto_regular;
    RealmList<ImgDetails> imgDetailss = new RealmList<>();
    RealmList<AudioDetails> arrAudioDetails = new RealmList<>();
    RealmList<AudioDetails> audDeletUpdated = new RealmList<>();
    String dateModified = null;
    TimeZone istTime = TimeZone.getTimeZone("IST");
    private double startTime = 0;
    private Handler myHandler = new Handler();

    ArrayList<AudFile> arrFileAudio = new ArrayList<>();
    ClipData mClipData;
    ArrayList<String> mArrayUri = new ArrayList<String>();//Uri
    private double finalTime = 0;
    private boolean internetConnected = true;
    public static String isChangedCheckbox = "No", isChanged = "No", isActiveCheckbox = "No", isCheckbox = "No";
    static String isType = "Checkboxes";
    static int checkboxCount = 0;
    static String isActiveText = "No";
    static String titleText = "";
    static List<EditText> UndoalleditText = new ArrayList<EditText>();
    static RealmList<Item> noteUpdatedList = new RealmList();
    static RealmList<TodoListItem> todoCheckboxItems = new RealmList<>();
    static Realm mRealm;
    static ArrayList<Undo> undoList = new ArrayList<>();
    static ArrayList<UndoCheckboxes> undoCheckboxesArraylist = new ArrayList<>();
    static ArrayList<UndoCheckboxes> redoCheckboxesArraylist = new ArrayList<>();
    static ArrayList<UndoTexts> undoTextsArraylist = new ArrayList<>();
    static ArrayList<UndoTexts> redoTextsArraylist = new ArrayList<>();
    private final int MY_PERMISSIONS_RECORD_AUDIO = 1;
    ArrayList<ImgFile> fileArrayList = new ArrayList<>();
    int imageHeight, imageWidth;
    private Runnable UpdateSongTimeOne = new Runnable() {
        public void run() {
            startTime = mediaPlayerOne.getCurrentPosition();
            seekBarOne.setProgress((int) startTime);
            myHandler.postDelayed(this, 100);
        }
    };
    private Runnable UpdateSongTimeTwo = new Runnable() {
        public void run() {
            startTime = mediaPlayerTwo.getCurrentPosition();
            seekBarTwo.setProgress((int) startTime);
            myHandler.postDelayed(this, 100);
        }
    };
    private Runnable UpdateSongTimeThree = new Runnable() {
        public void run() {
            startTime = mediaPlayerThree.getCurrentPosition();
            seekBarThree.setProgress((int) startTime);
            myHandler.postDelayed(this, 100);
        }
    };

    public static void storeData() {

        if ((undoList.size() < 6)) {

            if (isType.equals("Checkboxes")) {

                ArrayList<UndoCheckboxModel> NewundoCheckboxesFinal = new ArrayList<>();
                ArrayList<UndoCheckboxModel> undoCheckboxesFinal = new ArrayList<>();

                if (isChangedCheckbox.equals("Yes") && (isActiveCheckbox.equals("No"))) {

                    if (UndoalleditText.size() == 0) {
                        UndoCheckboxModel undoCheckboxes1 = new UndoCheckboxModel();
                        undoCheckboxes1.setId("0");
                        undoCheckboxes1.setStrNoteListValue("");
                        undoCheckboxes1.setStrNoteListFlage(false);
                        undoCheckboxesFinal.add(undoCheckboxes1);
                        NewundoCheckboxesFinal.add(0, undoCheckboxesFinal.get(0));
                    } else {

                        for (int i = 0; i < UndoalleditText.size(); i++) {
                            UndoCheckboxModel undoCheckboxes1 = new UndoCheckboxModel();
                            undoCheckboxes1.setId(String.valueOf(i));
                            undoCheckboxes1.setStrNoteListValue(UndoalleditText.get(i).getText().toString());
                            undoCheckboxes1.setStrNoteListFlage(false);
                            undoCheckboxesFinal.add(undoCheckboxes1);
                            NewundoCheckboxesFinal.add(i, undoCheckboxesFinal.get(i));
                        }

                    }

                    int count = undoCheckboxesArraylist.size();
                    if (count == 0) {
                        UndoCheckboxes undos = new UndoCheckboxes();
                        undos.setId(String.valueOf(count));
                        undos.setTitle(titleText);
                        undos.setUndoCheckboxModel(NewundoCheckboxesFinal);
                        undoCheckboxesArraylist.add(0, undos);
                    } else if (count == 1) {
                        UndoCheckboxes undos = new UndoCheckboxes();
                        undos.setId(String.valueOf(count));
                        undos.setTitle(titleText);
                        undos.setUndoCheckboxModel(NewundoCheckboxesFinal);
                        undoCheckboxesArraylist.add(1, undos);
                    } else if (count == 2) {
                        UndoCheckboxes undos = new UndoCheckboxes();
                        undos.setId(String.valueOf(count));
                        undos.setTitle(titleText);
                        undos.setUndoCheckboxModel(NewundoCheckboxesFinal);
                        undoCheckboxesArraylist.add(count, undos);
                    } else if (count == 3) {
                        UndoCheckboxes undos = new UndoCheckboxes();
                        undos.setId(String.valueOf(count));
                        undos.setTitle(titleText);
                        undos.setUndoCheckboxModel(NewundoCheckboxesFinal);
                        undoCheckboxesArraylist.add(count, undos);
                    } else if (count == 4) {
                        UndoCheckboxes undos = new UndoCheckboxes();
                        undos.setId(String.valueOf(count));
                        undos.setTitle(titleText);
                        undos.setUndoCheckboxModel(NewundoCheckboxesFinal);
                        undoCheckboxesArraylist.add(count, undos);
                    } else if (count == 5) {
                        UndoCheckboxes undos = new UndoCheckboxes();
                        undos.setId(String.valueOf(count));
                        undos.setTitle(titleText);
                        undos.setUndoCheckboxModel(NewundoCheckboxesFinal);
                        undoCheckboxesArraylist.add(count, undos);
                    } else {
                        undoCheckboxesArraylist.remove(1);
                        UndoCheckboxes undos = new UndoCheckboxes();
                        undos.setId(String.valueOf(count));
                        undos.setTitle(titleText);
                        undos.setUndoCheckboxModel(NewundoCheckboxesFinal);
                        undoCheckboxesArraylist.add(5, undos);
                    }

                    isChangedCheckbox = "No";
                    undoCheckboxesFinal.clear();
                }

            } else {

                ArrayList<UndoTextsModel> NewundoTextsFinal = new ArrayList<>();
                ArrayList<UndoTextsModel> undoTextsFinal = new ArrayList<>();
                if (isChanged.equals("Yes") && (isActiveText.equals("No"))) {//isActive used to prevent adding the data again in the list
//undoTextsArraylist redoTextsArraylist
                    if (UndoalleditText.size() == 0) {

                        UndoTextsModel undoTextsModel = new UndoTextsModel();
                        undoTextsModel.setId("0");
                        undoTextsModel.setStrNoteListValue("");
                        undoTextsFinal.add(undoTextsModel);
                        NewundoTextsFinal.add(0, undoTextsFinal.get(0));
                    } else {
                        for (int i = 0; i < UndoalleditText.size(); i++) {//+ new Gson().toJson(UndoalleditText.get(i))
                            UndoTextsModel undoTextsModel = new UndoTextsModel();
                            undoTextsModel.setId(String.valueOf(i));
                            undoTextsModel.setStrNoteListValue(UndoalleditText.get(i).getText().toString());
                            undoTextsFinal.add(undoTextsModel);
                            NewundoTextsFinal.add(i, undoTextsFinal.get(i));

//                            Log.e("VAl_ELSE",UndoalleditText.size()+"..."+NewundoTextsFinal);
                        }
                    }
                    int count = undoTextsArraylist.size();


                    if (count == 0) {
                        UndoTexts undoTextsModel = new UndoTexts();
                        undoTextsModel.setId(String.valueOf(count));
                        undoTextsModel.setTitle(titleText);
                        undoTextsModel.setUndoTextsModel(NewundoTextsFinal);
                        undoTextsArraylist.add(0, undoTextsModel);
//                        Log.e("VAl_count.1..", count + "..." + undoTextsArraylist.size());
                    } else if (count == 1) {
                        UndoTexts undoTextsModel = new UndoTexts();
                        undoTextsModel.setId(String.valueOf(count));
                        undoTextsModel.setTitle(titleText);
                        undoTextsModel.setUndoTextsModel(NewundoTextsFinal);
                        undoTextsArraylist.add(1, undoTextsModel);
//                        Log.e("VAl_count.1..", count + "..." + undoTextsArraylist.size());
                    } else if (count == 2) {
                        UndoTexts undoTextsModel = new UndoTexts();
                        undoTextsModel.setId(String.valueOf(count));
                        undoTextsModel.setTitle(titleText);
                        undoTextsModel.setUndoTextsModel(NewundoTextsFinal);
                        undoTextsArraylist.add(count, undoTextsModel);
//                        Log.e("VAl_count.2..", count + "..." + undoTextsArraylist.size());
                    } else if (count == 3) {
                        UndoTexts undoTextsModel = new UndoTexts();
                        undoTextsModel.setId(String.valueOf(count));
                        undoTextsModel.setTitle(titleText);
                        undoTextsModel.setUndoTextsModel(NewundoTextsFinal);
                        undoTextsArraylist.add(count, undoTextsModel);
//                        Log.e("VAl_count.3..", count + "..." + undoTextsArraylist.size());
                    } else if (count == 4) {
                        UndoTexts undoTextsModel = new UndoTexts();
                        undoTextsModel.setId(String.valueOf(count));
                        undoTextsModel.setTitle(titleText);
                        undoTextsModel.setUndoTextsModel(NewundoTextsFinal);
                        undoTextsArraylist.add(count, undoTextsModel);
//                        Log.e("VAl_count.4..", count + "..." + undoTextsArraylist.size());
                    } else if (count == 5) {
                        UndoTexts undoTextsModel = new UndoTexts();
                        undoTextsModel.setId(String.valueOf(count));
                        undoTextsModel.setTitle(titleText);
                        undoTextsModel.setUndoTextsModel(NewundoTextsFinal);
                        undoTextsArraylist.add(count, undoTextsModel);
//                        Log.e("VAl_count.5..", count + "..." + undoTextsArraylist.size());
                    } else {
                        undoTextsArraylist.remove(1);
                        UndoTexts undoTextsModel = new UndoTexts();
                        undoTextsModel.setId(String.valueOf(count));
                        undoTextsModel.setTitle(titleText);
                        undoTextsModel.setUndoTextsModel(NewundoTextsFinal);
//                        Log.e("VAl_count>5..", count + "..." + undoTextsArraylist.size());
                        undoTextsArraylist.add(5, undoTextsModel);
//                        Log.e("VAl_count>5>>..", count + "..." + undoTextsArraylist.size());
                    }

                    isActiveText = "Yes";
                }
                undoTextsFinal.clear();

            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_detail);
        ButterKnife.bind(this);
        mRealm = Realm.getDefaultInstance();
//        titleText = "";
        undoCheckboxesArraylist.clear();
        undoTextsArraylist.clear();
        scrollView.fullScroll(View.FOCUS_DOWN);
        Intent intent = new Intent(NoteDetailActivity.this, MyService.class);
        intent.putExtra("activity", "NoteDetailActivity");
        startService(intent);
//        supportPostponeEnterTransition();

        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mLinearLayout = findViewById(R.id.rlCheckbox);

        sdf = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");//yyyy-MM-dd'T'HH:mm:ss'Z'");

        SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences("Login", MODE_PRIVATE);
        String data = sharedpreferences.getString("UserData", null);
        Gson gson = new Gson();
        mUserDetails = gson.fromJson(data, Datum.class);
        strUserId = mUserDetails.getIntUserId();

        Nammu.init(this);
        display = getWindowManager().getDefaultDisplay();
        width = display.getWidth();

        font_roboto_regular = Typeface.createFromAsset(getAssets(), "fonts/roboto_regular.ttf");
        Typeface font_sfp_regular = Typeface.createFromAsset(getAssets(), "fonts/SFProText_Regular.ttf");
        editText = new CustomEditText(NoteDetailActivity.this);
        editText.setPadding(10, 10, 10, 10);
        tvToolbar.setTypeface(font_roboto_regular);
        tvTitle.setTypeface(font_roboto_regular);
        tvEdited.setTypeface(font_sfp_regular);

        activityType = getIntent().getStringExtra("type");
        plannerID = getIntent().getStringExtra("plannerId");
        llFileUpload.setVisibility(View.VISIBLE);
        llColleborate.removeAllViews();
        if (activityType.equals("activity")) {
            isImage = getIntent().getStringExtra("isImage");
            positionItem = getIntent().getIntExtra("position", -1);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                String transitionName = getIntent().getStringExtra(PlannerSectionAdapter.EXTRA_ITEM_TRANSITION_NAME);
                if (isImage.equals("yes")) {
                    supportPostponeEnterTransition();
//                    ivImageOne.setTransitionName(transitionName);
                    flImage.setTransitionName(transitionName);//flImage
                } else {
//                    supportStartPostponedEnterTransition();
//                    supportFinishAfterTransition();
//                    supportPostponeEnterTransition();
                    scrollView.setTransitionName(transitionName);//rlMain.setTransitionName(transitionName);
                }
            }

            RealmResults<Item> itemsRealmResults = mRealm.where(Item.class).equalTo("intPlannerId", Integer.parseInt(plannerID)).findAll();
            if (itemsRealmResults.size() != 0) {

                oneItemServer.clear();
                oneItemServer.addAll(itemsRealmResults);

                objRemainderOld.set_id(oneItemServer.get(0).getObjRemainder().get_id());
                objRemainderOld.setBlnFlage(oneItemServer.get(0).getObjRemainder().getBlnFlage());
                objRemainderOld.setStrRemainderDate(oneItemServer.get(0).getObjRemainder().getStrRemainderDate());
                objRemainderOld.setStrRemainderTime(oneItemServer.get(0).getObjRemainder().getStrRemainderTime());
                objRemainderOld.setStrType(oneItemServer.get(0).getObjRemainder().getStrType());

                Log.e("oneItemServer", plannerID + "...." + oneItemServer.get(0));
                dataServer(oneItemServer.get(0));

            }

        } else {
            RealmResults<Item> itemsRealmResults = mRealm.where(Item.class).equalTo("intPlannerId", Integer.parseInt(plannerID)).findAll();
            if (itemsRealmResults != null && itemsRealmResults.size() != 0) {

                oneItemServer.addAll(itemsRealmResults);
                dataServer(oneItemServer.get(0));
//                getColleborateUsers();
            } else {
                getSingleNote(plannerID);

            }
        }

        toolbar.setVisibility(View.VISIBLE);
        titleText = tvTitle.getText().toString();
        tvTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                isCheckboxChanged = true;
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void afterTextChanged(Editable editable) {

                llBottom.setVisibility(View.VISIBLE);
                llUndos.setVisibility(View.VISIBLE);
                if (isType.equals("Checkboxes")) {

                    if (undoCheckboxesArraylist.size() == 0) {
                        ivUndo.setEnabled(false);
                        ivUndo.setColorFilter(colorGrey);
                    } else {
                        ivUndo.setEnabled(true);
                        ivUndo.setColorFilter(colorBlack);
                    }

                } else {
                    if (undoTextsArraylist.size() == 0) {
                        ivUndo.setEnabled(false);
                        ivUndo.setColorFilter(colorGrey);

                    } else {
                        ivUndo.setEnabled(true);
                        ivUndo.setColorFilter(colorBlack);
                    }
                }

                isChangedCheckbox = "Yes";
                isChanged = "Yes";
                isActiveText = "No";
                isActiveCheckbox = "No";
                titleText = tvTitle.getText().toString();

                Calendar mcurrentTime = Calendar.getInstance();
                Date date = mcurrentTime.getTime();

                DateFormat gmtFormat = new SimpleDateFormat("MMM dd");//, h:mm a
                DateFormat gmtTimeFormat = new SimpleDateFormat("h:mm a");
                gmtFormat.setTimeZone(istTime);
                gmtTimeFormat.setTimeZone(istTime);

                SimpleDateFormat mdformat = new SimpleDateFormat("h:mm a");
                mdformat.setTimeZone(istTime);
                dateModified = mdformat.format(date);
                tvEdited.setVisibility(View.VISIBLE);
                llBottom.setVisibility(View.VISIBLE);
                tvEdited.setText("Today " + dateModified);

            }
        });


        tvTitle.setOnClickListener(view -> {
            tvTitle.requestFocus();
            tvTitle.setCursorVisible(true);
            tvTitle.setFocusableInTouchMode(true);
        });

        tvTitle.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                editText.setSelection(editText.getText().length());
                editText.requestFocus();
                editText.setCursorVisible(true);

                return true;
            }
            return false;
        });

    }

    @Override
    protected void onStop() {
        super.onStop();
        Intent intent = new Intent(NoteDetailActivity.this, MyService.class);
        intent.putExtra("close", true);
        stopService(intent);
    }

    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = Application.getConnectivityStatusString(context);
            setSnackbarMessage(status, false);
        }
    };


    public void getSingleNote(String id) {

        RequestParams params = new RequestParams();

        params.put("intPlannerId", id);
        params.put("strUserId", strUserId);

        ApiCallWithToken(singlePlanner, ApiCall.SINGLEPLANNER, params);
    }

    public void getColleborateUsers() {

        if (isNetworkAvailable()) {
            llColleborate.removeAllViews();
            RequestParams params = new RequestParams();
            params.put("strUserId", strUserId);
            params.put("intPlannerId", String.valueOf(plannerID));
            ApiCallWithToken(urlColUser, ApiCall.COLLABORATEUSERS, params);

        } else {

            arryCollaboratUserData.clear();
            RealmResults<ColleborateDatum> itemsRealmResults = mRealm.where(ColleborateDatum.class).findAll();

            if (itemsRealmResults != null && itemsRealmResults.size() != 0) {
                arryCollaboratUserData.addAll(itemsRealmResults);
                if (arryCollaboratUserData.size() == 0) {

                    int count = oneItemServer.get(0).getArryCollaboratUserId().size();
                    if (count != 0) {
                        ColleborateDatum colleborateDatum = new ColleborateDatum();
                        for (int i = 0; i < count; i++) {
                            arryCollaboratUserData.add(colleborateDatum);
                        }
                    }
                    setColleborateUsers(arryCollaboratUserData);
                }
            }

        }
    }


    public void loadImage() {

        int SPLASH_TIME_OUT = 3000;

        if (fileArrayList.size() > 0) {

            new Handler().postDelayed(() -> {
                Log.e("LoadImage", fileArrayList.get(0).getFile() + "..." + fileArrayList.get(0).getHeight() + "..." + fileArrayList.get(0).getWeight());
                saveImage(fileArrayList.get(0).getFile(), fileArrayList.get(0).getHeight(), fileArrayList.get(0).getWeight());
                fileArrayList.remove(0);
            }, SPLASH_TIME_OUT);

        }
    }

    public void saveImage(File fileProfile, int imageHeight, int imageWidth) {
//    public void saveImage() {
        showToast("bfr.." + imgDetailss.size());
        RequestParams params = new RequestParams();
        try {
            params.put("file", fileProfile);//fileProfile = new File("")
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        params.put("intPlannerId", String.valueOf(oneItemServer.get(0).getIntPlannerId()));
        params.put("strModifiedUserId", strUserId);
        params.put("intHeight", String.valueOf(imageHeight));
        params.put("intWeight", String.valueOf(imageWidth));

        params.put("strCategoryTypeId", oneItemServer.get(0).getfkIntCategoryTypeId());
        params.put("strCategoryId", oneItemServer.get(0).getfkIntCategoryId());
        params.put("strSubCategoryId", oneItemServer.get(0).getfkIntSubCategoryId());

        Log.e("saveImage", fileProfile + "..." + params);

        showProgressDialog(this, "Uploading...");
        ApiCallWithToken(urlImage, ApiCall.IMAGE, params);
    }

    public void saveAudio(File audFile, String time) {

        RequestParams params = new RequestParams();
        try {
            params.put("file", audFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        params.put("intPlannerId", String.valueOf(oneItemServer.get(0).getIntPlannerId()));
        params.put("strModifiedUserId", strUserId);
        params.put("strDuration", time);

        params.put("strCategoryTypeId", oneItemServer.get(0).getfkIntCategoryTypeId());
        params.put("strCategoryId", oneItemServer.get(0).getfkIntCategoryId());
        params.put("strSubCategoryId", oneItemServer.get(0).getfkIntSubCategoryId());

        showProgressDialog(this, "Uploading...");
        ApiCallWithToken(urlAudio, ApiCall.AUDIO, params);

    }

    /*
    {
	"strCategoryTypeId":"5d168aaf3905710a216f4a0c",
	"strCategoryId":"5d17108c3905710a216f4b0d",
	"intOrganisationId":"5be144b78dd6c908037cf3b8",
	"intProjectId":"5bfc098c4d70683a16451708",
	"intPlannerId":"339",
	"strModifiedUserId":"000000013905710a216f49c5",
	 "strAudioId":"5d304e78be6d5d612f6edbe1"


}
     */
    public void deleteAudio(AudioDetails arrAudioDetail) {
//        File fileProfile = new File("null");
        RequestParams params = new RequestParams();
//        try {
////            params.put("file", fileProfile);//fileAudio = new File("")
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }

        params.put("strAudioId", arrAudioDetail.getIntAudioId());
        params.put("intPlannerId", String.valueOf(oneItemServer.get(0).getIntPlannerId()));
        params.put("strModifiedUserId", strUserId);

        params.put("strCategoryTypeId", oneItemServer.get(0).getfkIntCategoryTypeId());
        params.put("strCategoryId", oneItemServer.get(0).getfkIntCategoryId());
        params.put("strSubCategoryId", oneItemServer.get(0).getfkIntSubCategoryId());

        Log.e("deleteAudio", urldeleteNoteAud + "..." + params);

        showProgressDialog(this, "Removing...");
        ApiCallWithToken(urldeleteNoteAud, ApiCall.DELETEAUDIO, params);

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint("ResourceType")
    public void setValues(Item listItem) {

        arrayToDoList.clear();
        arrayToDoList.addAll(oneItemServer.get(0).getArryObjstrTodoList());
        String title = listItem.getStrTitle();

        if (title.length() > 0) {
            tvTitle.setText(listItem.getStrTitle());
        } else {
            tvTitle.setHint("Title");
        }

        tvTitle.setOnTouchListener((view, motionEvent) -> {
            tvTitle.setCursorVisible(true);
            return false;
        });

        noteValue = listItem.getStrValue();

        if (noteValue.length() != 0) {

            ivConvert.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_checkbox_grey));
            String[] val = noteValue.split("\\r?\\n");
            for (String line : val) {
                toSingleStringList.add(line);
            }

            todoCheckboxItems.clear();
            toSingleString = new StringBuffer(toSingleStringList.size());
            toSingleString.append(noteValue);
            rlCheckbox.setVisibility(View.VISIBLE);

            isCheckBox = "TextFormat";
            isType = "TextFormat";
            alleditText.clear();
            alleditTextCheckbox.clear();
            rlCheckbox.removeAllViews();

            for (int i = 0; i < toSingleStringList.size(); i++) {
                isChanged = "Yes";
                isActiveText = "No";
                editText = new CustomEditText(NoteDetailActivity.this);
                alleditText.add(editText);
                editText.setText(toSingleStringList.get(i));
                textStyle(editText);
                editText.setId(i);
                rlCheckbox.addView(editText);

                editText.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        isCheckboxChanged = true;
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        ivUndo.setEnabled(true);
                        ivUndo.setColorFilter(colorBlack);
                        redoTextsArraylist.clear();
                        ivRedo.setEnabled(false);
                        ivRedo.setColorFilter(colorGrey);
                        llBottom.setVisibility(View.VISIBLE);
                        llUndos.setVisibility(View.VISIBLE);
                        if (undoTextsArraylist.size() == 0) {
                            ivUndo.setEnabled(false);
                            ivUndo.setColorFilter(colorGrey);
                        } else {
                            ivUndo.setEnabled(true);
                            ivUndo.setColorFilter(colorBlack);
                        }
                        UndoalleditText = alleditText;
                        isActiveText = "No";

                        Calendar mcurrentTime = Calendar.getInstance();
                        Date date = mcurrentTime.getTime();
                        DateFormat gmtFormat = new SimpleDateFormat("MMM dd");//, h:mm a
                        DateFormat gmtTimeFormat = new SimpleDateFormat("h:mm a");
                        gmtFormat.setTimeZone(istTime);
                        gmtTimeFormat.setTimeZone(istTime);

                        SimpleDateFormat mdformat = new SimpleDateFormat("h:mm a");
                        mdformat.setTimeZone(istTime);
                        dateModified = mdformat.format(date);
                        tvEdited.setVisibility(View.VISIBLE);
                        llBottom.setVisibility(View.VISIBLE);
                        tvEdited.setText("Today " + dateModified);

                    }

                });

            }

            loopViews(rlCheckbox);
            List<EditText> alledits = new ArrayList<EditText>();
            for (int i = 0; i < alleditText.size(); i++) {
                EditText editText = new EditText(NoteDetailActivity.this);
                editText.setText(alleditText.get(i).getText().toString());
                alledits.add(editText);
                UndoalleditText = alledits;
            }

        } else if (arrayToDoList.size() != 0) {
            ivConvert.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_checkbox_colorprimary));
            isCheckbox = "Yes";
            alleditText.clear();
            alleditTextCheckbox.clear();
            todoCheckboxItems.clear();
            isCheckBox = "Checkboxes";
            isType = "Checkboxes";
            tvCheckbox.setText("Checkboxes");
            rlCheckbox.setVisibility(View.VISIBLE);
            rlCheckbox.removeAllViews();
            ivAdd.setVisibility(View.GONE);//VISIBLE
            checkboxCount = arrayToDoList.size();
            toSingleString = new StringBuffer(1);
            toSingleString.append("null");

            assignCheckBoxe();//show in checkboxes

        } else {

            isCheckbox = "No";
            toSingleStringList.clear();
            isCheckBox = "TextFormat";
            isType = "TextFormat";
            tvCheckbox.setText("TextFormat");
            rlCheckbox.setVisibility(View.VISIBLE);
            rlCheckbox.removeAllViews();

            ivAdd.setVisibility(View.GONE);

            LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View rowView = inflater.inflate(R.layout.add_customedittext, null);
            rlCheckbox.addView(rowView, rlCheckbox.getChildCount() - 1);

            editText = rowView.findViewById(R.id.etNote);
            textStyle(editText);
            ImageView ivdelete = rowView.findViewById(R.id.ivdelete);
            editText.setHint("");
            alleditText.add(editText);
            editText.setBackgroundResource(getResources().getColor(android.R.color.transparent));
            toSingleString = new StringBuffer(1);
            toSingleString.append(editText.getText().toString());

            ivdelete.setOnClickListener(view -> rlCheckbox.removeView((View) view.getParent()));

            editText.setOnTouchListener((view, motionEvent) -> {
                editText.setFocusableInTouchMode(true);
                editText.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                editText.setCursorVisible(true);
                return false;
            });

            editText.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    isCheckboxChanged = true;
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {

                    Calendar mcurrentTime = Calendar.getInstance();
                    Date date = mcurrentTime.getTime();
                    DateFormat gmtFormat = new SimpleDateFormat("MMM dd");//, h:mm a
                    DateFormat gmtTimeFormat = new SimpleDateFormat("h:mm a");

                    gmtFormat.setTimeZone(istTime);
                    gmtTimeFormat.setTimeZone(istTime);

                    SimpleDateFormat mdformat = new SimpleDateFormat("h:mm a");
                    mdformat.setTimeZone(istTime);
                    dateModified = mdformat.format(date);
                    tvEdited.setVisibility(View.VISIBLE);
                    llBottom.setVisibility(View.VISIBLE);
                    tvEdited.setText("Today " + dateModified);

                }

            });

        }
    }

    @SuppressLint("ResourceType")
    public void textStyle(CustomEditText customEditText) {//},int i) {

        Typeface font_roboto_regular = Typeface.createFromAsset(getAssets(), "fonts/roboto_regular.ttf");
        customEditText.applyCustomFont(this, getResources().getString(R.string.roboto_light));//@style/Roboto_light
        customEditText.setBackgroundResource(getResources().getColor(android.R.color.transparent));
        customEditText.setTextColor(ContextCompat.getColor(this, R.color.text_color));
//        customEditText.setLineSpacing((float) 1.50, 2);
        customEditText.setTextSize(14);

        customEditText.setPadding(10, 10, 10, 10);
        customEditText.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        customEditText.setSelection(customEditText.getText().length());
//        rlCheckbox.addView(customEditText, i);

    }

    @SuppressLint("ResourceType")
    public void textStyleCheckbox(CustomEditText customEditText) {

        customEditText.applyCustomFont(this, getResources().getString(R.string.roboto_light));
        customEditText.setBackgroundResource(getResources().getColor(android.R.color.transparent));
        customEditText.setTextColor(ContextCompat.getColor(this, R.color.text_color));
        customEditText.setTextSize(14);
        customEditText.setSelection(customEditText.getText().length());

    }

    @SuppressLint("ResourceType")
    public void assignCheckBoxe() {

        alleditTextCheckbox.clear();
        alleditText.clear();

        for (int i = 0; i < arrayToDoList.size(); i++) {

            final View view1 = inflater.inflate(R.layout.checkbox_layout, null);
            rlCheckbox.addView(view1, i);//rlCheckbox.getChildCount()
            RelativeLayout llMain = view1.findViewById(R.id.llMain);
            final CustomEditText etTextCheckBox = view1.findViewById(R.id.etText);
            etTextCheckBox.setFocusable(true);
            final ImageView ivCheckbox = view1.findViewById(R.id.ivCheckbox);
            final ImageView ivClose = view1.findViewById(R.id.ivClose);

            Boolean isChk = arrayToDoList.get(i).getStrNoteListFlage();

            if (isChk) {
                etTextCheckBox.setTag(true);
                ivCheckbox.setImageDrawable(getResources().getDrawable(R.drawable.ic_checked));
                etTextCheckBox.setPaintFlags(etTextCheckBox.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else {
                etTextCheckBox.setTag(false);
                ivCheckbox.setImageDrawable(getResources().getDrawable(R.drawable.ic_unchecked));
                etTextCheckBox.setPaintFlags(etTextCheckBox.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            }
//            etTextCheckBox.applyCustomFont(this, getResources().getString(R.string.roboto_light));
            etTextCheckBox.setText(arrayToDoList.get(i).getStrNoteListValue());
            textStyleCheckbox(etTextCheckBox);
//            etTextCheckBox.setTextSize(14);
            etTextCheckBox.setId(i);
            alleditTextCheckbox.add(etTextCheckBox);
//            etTextCheckBox.setTextAppearance(this, R.style.Roboto_light);
//            etTextCheckBox.setTextColor(getResources().getColor(R.color.text_note));
//            etTextCheckBox.setBackgroundResource(getResources().getColor(android.R.color.transparent));
//
//            (alleditTextCheckbox.get(alleditTextCheckbox.size() - 1)).setCursorVisible(true);
////            (alleditTextCheckbox.get(alleditTextCheckbox.size() - 1)).requestFocus();
//            (alleditTextCheckbox.get(alleditTextCheckbox.size() - 1)).setFocusable(true);
//            (alleditTextCheckbox.get(alleditTextCheckbox.size() - 1)).setSelection(alleditTextCheckbox.get(alleditTextCheckbox.size() - 1).getText().length());


            UndoalleditText = alleditTextCheckbox;
            int finalI = i;


            TodoListItem todoListItem = new TodoListItem();
            todoListItem.setStrNoteListFlage(isChk);
            todoListItem.setStrNoteListValue(arrayToDoList.get(i).getStrNoteListValue());
            todoCheckbxText.add(todoListItem);

            etTextCheckBox.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    isCheckboxChanged = true;
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @RequiresApi(api = Build.VERSION_CODES.N)
                @Override
                public void afterTextChanged(Editable s) {

                    llUndos.setVisibility(View.VISIBLE);
                    llBottom.setVisibility(View.VISIBLE);

                    if (undoCheckboxesArraylist.size() == 0) {

                        ivUndo.setEnabled(false);
                        ivUndo.setColorFilter(colorGrey);

                    } else {

                        ivUndo.setEnabled(true);
                        ivUndo.setColorFilter(colorBlack);

                    }
                    isActiveCheckbox = "No";
                    isChangedCheckbox = "Yes";

                    String _text = etTextCheckBox.getText().toString();
                    if (_text.isEmpty()) {

                    } else {
                        UndoalleditText = alleditTextCheckbox;
                    }

                    Calendar mcurrentTime = Calendar.getInstance();
                    Date date = mcurrentTime.getTime();
                    DateFormat gmtFormat = new SimpleDateFormat("MMM dd");//, h:mm a
                    DateFormat gmtTimeFormat = new SimpleDateFormat("h:mm a");
                    gmtFormat.setTimeZone(istTime);
                    gmtTimeFormat.setTimeZone(istTime);

                    SimpleDateFormat mdformat = new SimpleDateFormat("h:mm a");
                    mdformat.setTimeZone(istTime);
                    dateModified = mdformat.format(date);
                    tvEdited.setVisibility(View.VISIBLE);
                    llBottom.setVisibility(View.VISIBLE);
                    tvEdited.setText("Today " + dateModified);

                }
            });

//            scrollView.setOnTouchListener((view, motionEvent) -> {
//                etTextCheckBox.setSelection(etTextCheckBox.getText().length());
//                etTextCheckBox.requestFocus();
//                etTextCheckBox.setCursorVisible(false);
//                etTextCheckBox.setFocusableInTouchMode(false);
//                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                imm.showSoftInput(etTextCheckBox, InputMethodManager.SHOW_IMPLICIT);
//                return false;
//            });

            llMain.setOnTouchListener((view, motionEvent) -> {
                ivClose.setVisibility(View.GONE);//VISIBLE
                etTextCheckBox.setCursorVisible(true);
                return false;
            });

            etTextCheckBox.setOnTouchListener((view, motionEvent) -> {
                ivClose.setVisibility(View.GONE);//VISIBLE
                etTextCheckBox.setCursorVisible(true);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(etTextCheckBox, InputMethodManager.SHOW_IMPLICIT);
                return false;
            });

            ivCheckbox.setOnTouchListener((view, motionEvent) -> {

                String isCheck = String.valueOf(etTextCheckBox.getTag());
                isCheckboxChanged = true;
                if (isCheck.equals("true")) {
                    etTextCheckBox.setTag(false);
                    ivClose.setAlpha(0f);
                    ivCheckbox.setImageDrawable(getResources().getDrawable(R.drawable.ic_unchecked));
                    etTextCheckBox.setPaintFlags(etTextCheckBox.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                } else {
                    etTextCheckBox.setTag(true);
                    ivCheckbox.setImageDrawable(getResources().getDrawable(R.drawable.ic_checked));
                    etTextCheckBox.setPaintFlags(etTextCheckBox.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                }
                return false;
            });

            ivClose.setOnClickListener(view -> rlCheckbox.removeView((View) view.getParent()));
        }

        loopViewsCheckbox(rlCheckbox);

        if (!alleditTextCheckbox.isEmpty()) {
            isChanged = "No";
            ArrayList<UndoCheckboxModel> NewundoCheckboxesFinal = new ArrayList<>();
            ArrayList<UndoCheckboxModel> undoCheckboxesFinal = new ArrayList<>();
            for (int i = 0; i < alleditTextCheckbox.size(); i++) {
                UndoCheckboxModel undoCheckboxes1 = new UndoCheckboxModel();
                undoCheckboxes1.setId(String.valueOf(i));
                undoCheckboxes1.setStrNoteListValue(alleditTextCheckbox.get(i).getText().toString());
                undoCheckboxes1.setStrNoteListFlage(false);
                undoCheckboxesFinal.add(undoCheckboxes1);
                NewundoCheckboxesFinal.add(i, undoCheckboxesFinal.get(i));
            }

            UndoCheckboxes undos1 = new UndoCheckboxes();
            undos1.setId(String.valueOf(0));
            undos1.setTitle(tvTitle.getText().toString());
            undos1.setUndoCheckboxModel(NewundoCheckboxesFinal);
            undoCheckboxesArraylist.add(0, undos1);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint("ResourceType")
    public void convertDatas() {

        if (isCheckBox.equals("Checkboxes")) {//converting  checkbox to  TextFormat
            isCheckBox = "TextFormat";
            isType = "TextFormat";
            ivConvert.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_checkbox_grey));
            convertToTextFormat();
        } else if (isCheckBox.equals("TextFormat")) {//converting  TextFormat to checkbox
            isCheckBox = "Checkboxes";
            isType = "Checkboxes";
            ivConvert.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_checkbox_colorprimary));
            convertToCheckBoxes();
        }

    }

    @SuppressLint("ResourceType")
    public void convertToCheckBoxes() {

        ivAdd.setVisibility(View.GONE);//VISIBLE
        checkboxCount = alleditText.size();

        alleditTextCheckbox.clear();
        rlCheckbox.removeAllViews();
        toSingleStringList.clear();

        if (!alleditText.isEmpty()) {

            for (int j = 0; j < alleditText.size(); j++) {
                String nixSampleLine = alleditText.get(j).getText().toString();
                if (nixSampleLine.length() != 0) {
                    String[] lines = nixSampleLine.split("\\r?\\n");
                    for (String line : lines) {
                        toSingleStringList.add(line);
                    }
                }
            }
            alleditText.clear();

        }

        if (toSingleStringList.size() != 0) {//toSingleStringList contains value

            for (int i = 0; i < toSingleStringList.size(); i++) {

                rlCheckbox.setVisibility(View.VISIBLE);
                mLinearLayout1 = findViewById(R.id.rlCheckbox);
                view1 = View.inflate(NoteDetailActivity.this, R.layout.checkbox_layout, null);
                final RelativeLayout llMain = view1.findViewById(R.id.llMain);
                etTextCheckBox = view1.findViewById(R.id.etText);
                final ImageView ivCheckbox = view1.findViewById(R.id.ivCheckbox);
                final ImageView ivClose = view1.findViewById(R.id.ivClose);

                textStyleCheckbox(etTextCheckBox);
//                etTextCheckBox.requestFocus();
                etTextCheckBox.setText(toSingleStringList.get(i));
                etTextCheckBox.setId(i);
                etTextCheckBox.setTag(false);
                mLinearLayout1.addView(view1, i);

                loopViewsCheckbox(mLinearLayout1);

                ivClose.setOnClickListener(view -> rlCheckbox.removeView((View) view.getParent()));
                alleditTextCheckbox.add(etTextCheckBox);

                etTextCheckBox.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        isCheckboxChanged = true;
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                        llUndos.setVisibility(View.VISIBLE);
                        llBottom.setVisibility(View.VISIBLE);

                        if (undoCheckboxesArraylist.size() == 0) {

                            ivUndo.setEnabled(false);
                            ivUndo.setColorFilter(colorGrey);

                        } else {

                            ivUndo.setEnabled(true);
                            ivUndo.setColorFilter(colorBlack);

                        }

                        isActiveCheckbox = "No";
                        isChangedCheckbox = "Yes";
                        String _text = etTextCheckBox.getText().toString();
                        if (_text.isEmpty()) {

                        } else {
                            UndoalleditText = alleditTextCheckbox;
                        }
                    }
                });

                rlMain.setOnTouchListener((view, motionEvent) -> {
                    etTextCheckBox.setSelection(etTextCheckBox.getText().length());
                    etTextCheckBox.requestFocus();
                    etTextCheckBox.setCursorVisible(true);
                    return false;
                });

                llMain.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        ivClose.setVisibility(View.GONE);//VISIBLE
                        etTextCheckBox.requestFocus();
                        etTextCheckBox.setCursorVisible(true);
                        return false;
                    }
                });

                etTextCheckBox.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        ivClose.setVisibility(View.GONE);//VISIBLE
                        etTextCheckBox.setCursorVisible(true);
                        return false;
                    }

                });

                ivClose.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {

                        ivClose.setAlpha(0f);
                        return false;
                    }
                });
            }
            for (int i = 0; i < rlCheckbox.getChildCount(); i++) {
                View nextChild = (rlCheckbox).getChildAt(i);
                String val = ((CustomEditText) ((RelativeLayout) nextChild).getChildAt(1)).getText().toString();

                ((RelativeLayout) nextChild).getChildAt(0).setOnTouchListener((view, motionEvent) -> {

                    ((CustomEditText) ((RelativeLayout) nextChild).getChildAt(1)).setCursorVisible(true);
                    String isCheck = String.valueOf(((RelativeLayout) nextChild).getChildAt(1).getTag());

                    if (isCheck.equals("true")) {
                        ((RelativeLayout) nextChild).getChildAt(1).setTag(false);
                        ((ImageView) ((RelativeLayout) nextChild).getChildAt(0)).setImageDrawable(getResources().getDrawable(R.drawable.ic_unchecked));
                        ((CustomEditText) ((RelativeLayout) nextChild).getChildAt(1)).setPaintFlags(etTextCheckBox.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                    } else {
                        ((RelativeLayout) nextChild).getChildAt(1).setTag(true);
                        ((ImageView) ((RelativeLayout) nextChild).getChildAt(0)).setImageDrawable(getResources().getDrawable(R.drawable.ic_checked));
                        ((CustomEditText) ((RelativeLayout) nextChild).getChildAt(1)).setPaintFlags(etTextCheckBox.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    }

                    return false;

                });
            }
            toSingleStringList.clear();

        } else {//toSingleStringList contains no value ie todoCheckboxItems contains value

            rlCheckbox.setVisibility(View.VISIBLE);

            mLinearLayout1 = findViewById(R.id.rlCheckbox);
            view1 = View.inflate(NoteDetailActivity.this, R.layout.checkbox_layout, null);
            final RelativeLayout llMain = view1.findViewById(R.id.llMain);
            etTextCheckBox = view1.findViewById(R.id.etText);
            final ImageView ivCheckbox = view1.findViewById(R.id.ivCheckbox);
            final ImageView ivClose = view1.findViewById(R.id.ivClose);
            textStyleCheckbox(etTextCheckBox);
            etTextCheckBox.requestFocus();
            etTextCheckBox.setCursorVisible(true);
            etTextCheckBox.setId(1);
            etTextCheckBox.setSelection(etTextCheckBox.getText().length());

            ivClose.setOnClickListener(view -> rlCheckbox.removeView((View) view.getParent()));
            alleditTextCheckbox.add(etTextCheckBox);
            mLinearLayout1.addView(view1);

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint({"ResourceType", "ClickableViewAccessibility"})
    public void convertToTextFormat() {

        rlCheckbox.removeAllViews();
        alleditText.clear();
        ivAdd.setVisibility(View.GONE);

        if (!alleditTextCheckbox.isEmpty()) {
            todoCheckboxItems.clear();

            for (int j = 0; j < alleditTextCheckbox.size(); j++) {

                String val = alleditTextCheckbox.get(j).getText().toString();

                if (val.length() != 0) {//only add checkbox which contains values and remove if it is null
                    TodoListItem todoListItem = new TodoListItem();
                    todoListItem.setStrNoteListFlage(false);
                    todoListItem.setStrNoteListValue(val);
                    todoCheckboxItems.add(todoListItem);
                }

            }
            UndoalleditText = alleditTextCheckbox;
            alleditTextCheckbox.clear();
        }

        if (todoCheckboxItems.size() != 0) {

            for (int i = 0; i < todoCheckboxItems.size(); i++) {
                editText = new CustomEditText(NoteDetailActivity.this);
                alleditText.add(editText);
                editText.setText(todoCheckboxItems.get(i).getStrNoteListValue());
                editText.setLeft(10);
                editText.setWidth(width);
                editText.setId(i);
                textStyle(editText);
                rlCheckbox.addView(editText, i);

                int finalI = i;

                editText.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                        isCheckboxChanged = true;
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        redoTextsArraylist.clear();
                        ivRedo.setEnabled(false);
                        ivRedo.setColorFilter(colorGrey);
                        llUndos.setVisibility(View.VISIBLE);
                        llBottom.setVisibility(View.VISIBLE);
                        if (undoTextsArraylist.size() == 0) {

                            ivUndo.setEnabled(false);
                            ivUndo.setColorFilter(colorGrey);

                        } else {
                            ivUndo.setEnabled(true);
                            ivUndo.setColorFilter(colorBlack);
                        }

                        isChanged = "Yes";
                        isActiveText = "No";
                        UndoalleditText = alleditText;

                    }
                });


            }
            loopViews(rlCheckbox);

        } else {//toSingleStringList contains no value ie todoCheckboxItems contains value

            editText = new CustomEditText(NoteDetailActivity.this);
            editText.setTextSize(14);
            editText.setWidth(width);
            editText.requestFocus();
            editText.setCursorVisible(true);
            alleditText.add(editText);
//            editText.setPadding(10, 10, 10, 10);
            editText.setId(1);
            textStyle(editText);
            editText.setLeft(10);
            rlCheckbox.addView(editText);
            toSingleString = new StringBuffer(1);
            toSingleString.append(editText.getText().toString());

            rlMain.setOnTouchListener((view, motionEvent) -> {
                editText.setSelection(editText.getText().length());
                editText.requestFocus();
                editText.setCursorVisible(true);
                return false;
            });

        }
    }

    @SuppressLint("ResourceType")
    public void addEnterKeyPressed(int position, String newVal) {

        alleditText.clear();
        final int[] checked = {1};
        checkboxCount = checkboxCount + 1;


        final View view = inflater.inflate(R.layout.checkbox_layout, null);
        rlCheckbox.addView(view, position);

        RelativeLayout llMain1 = view.findViewById(R.id.llMain);
        final CustomEditText etTextCheckBox1 = view.findViewById(R.id.etText);
        final ImageView ivCheckbox1 = view.findViewById(R.id.ivCheckbox);
        final ImageView ivClose1 = view.findViewById(R.id.ivClose);
        textStyleCheckbox(etTextCheckBox1);
        etTextCheckBox1.setTag(false);
        etTextCheckBox1.setId(position);
        etTextCheckBox1.setText(newVal);
        etTextCheckBox1.requestFocus();
        etTextCheckBox1.setCursorVisible(true);

        ivClose1.setOnClickListener(view1 -> {
//                ivClose1.setAlpha(0f);
            rlCheckbox.removeView((View) view1.getParent());
        });

        loopViewsCheckbox(rlCheckbox);

        etTextCheckBox1.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                isCheckboxChanged = true;
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                redoCheckboxesArraylist.clear();
                ivRedo.setEnabled(false);
                ivRedo.setColorFilter(colorGrey);
                llUndos.setVisibility(View.VISIBLE);
                llBottom.setVisibility(View.VISIBLE);
                if (undoCheckboxesArraylist.size() == 0) {

                    ivUndo.setEnabled(false);
                    ivUndo.setColorFilter(colorGrey);

                } else {
                    ivUndo.setEnabled(true);
                    ivUndo.setColorFilter(colorBlack);
                }
                isChangedCheckbox = "Yes";
                isActiveCheckbox = "No";
                UndoalleditText = alleditTextCheckbox;

            }
        });

        rlMain.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                etTextCheckBox1.setSelection(etTextCheckBox1.getText().length());
                etTextCheckBox1.requestFocus();
                etTextCheckBox1.setCursorVisible(true);
                return false;

            }
        });

        final Boolean[] isChecked = {false};
        llMain1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                ivClose1.setVisibility(View.GONE);//VISIBLE
                etTextCheckBox1.requestFocus();
                etTextCheckBox1.setCursorVisible(true);
                return false;
            }
        });

        etTextCheckBox1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                ivClose1.setVisibility(View.GONE);//VISIBLE
                etTextCheckBox1.setCursorVisible(true);
                return false;
            }
        });

        ivCheckbox1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                ivClose1.setAlpha(0f);

                if (checked[0] == 0) {

                    etTextCheckBox1.setTag(false);
                    isChecked[0] = false;
                    ivCheckbox1.setImageDrawable(getResources().getDrawable(R.drawable.ic_unchecked));
                    etTextCheckBox1.setPaintFlags(etTextCheckBox1.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                    checked[0] = 1;

                } else {

                    etTextCheckBox1.setTag(true);
                    isChecked[0] = true;
                    ivCheckbox1.setImageDrawable(getResources().getDrawable(R.drawable.ic_checked));
                    etTextCheckBox1.setPaintFlags(etTextCheckBox1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    checked[0] = 0;

                }
                return false;
            }
        });

        ivClose1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                rlCheckbox.removeView((View) view.getParent());
                ivClose1.setAlpha(0f);
                return false;
            }
        });

        TodoListItem todoListItem1 = new TodoListItem();
        String isChk = String.valueOf(etTextCheckBox1.getTag());
        if (isChk.equals("true")) {
            todoListItem1.setStrNoteListFlage(true);
        } else {
            todoListItem1.setStrNoteListFlage(false);
        }

        todoListItem1.setStrNoteListValue(etTextCheckBox1.getText().toString());
        todoCheckboxItems.add(todoListItem1);
        alleditTextCheckbox.add(position, etTextCheckBox1);

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint("ResourceType")
    public void addEditTextEnterKeyPressed(int position, String newVal) {
        editText = new CustomEditText(NoteDetailActivity.this);
        editText.setHint("");
        textStyle(editText);
        editText.setLeft(10);
        editText.setText(newVal);
        editText.setId(position);
        editText.requestFocus();
        editText.setCursorVisible(true);
        alleditText.add(position, editText);
        rlCheckbox.addView(editText, position);
        loopViews(rlCheckbox);

        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                isCheckboxChanged = true;
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                redoTextsArraylist.clear();
                ivRedo.setEnabled(false);
                ivRedo.setColorFilter(colorGrey);
                llUndos.setVisibility(View.VISIBLE);
                llBottom.setVisibility(View.VISIBLE);

                if (undoTextsArraylist.size() == 0) {
                    ivUndo.setEnabled(false);
                    ivUndo.setColorFilter(colorGrey);
                } else {
                    ivUndo.setEnabled(true);
                    ivUndo.setColorFilter(colorBlack);
                }

                isActiveText = "No";
                isChanged = "Yes";
                List<EditText> alledits = new ArrayList<EditText>();
                for (int i = 0; i < alleditText.size(); i++) {
                    EditText editText = new EditText(NoteDetailActivity.this);
                    editText.setText(alleditText.get(i).getText().toString());
                    alledits.add(editText);
                    UndoalleditText = alledits;
                }

            }
        });

        rlMain.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                editText.setSelection(editText.getText().length());
                editText.requestFocus();
                editText.setCursorVisible(true);
                return false;
            }
        });

    }


    @SuppressLint("ResourceType")
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void dataServer(Item listItem) {

        String dateModify = listItem.getDatModifiedDateAndTime();
        dateModified = listItem.getDatModifiedDateAndTime();
        imgDetailss.addAll(listItem.getImgNoteList());
        ObjRemainder objRemainderVal = listItem.getObjRemainder();
        final ArrayList<String> colleborators = new ArrayList<>();
        colleborators.addAll(listItem.getArryCollaboratUserId());

        if (colleborators.size() > 0) {
            getColleborateUsers();
            isPinned = false;
        }
        objRemainder = objRemainderVal;
        isPinned = listItem.getBlnPinNote();
        if (isPinned) {
            int color = Color.parseColor("#F1C40F"); //The color u want
            ivPin.setColorFilter(color);
        } else {
            int color = Color.parseColor("#FFFFFF"); //The color u want
            ivPin.setColorFilter(color);
        }

        if (TextUtils.isEmpty(dateModify)) {
            tvEdited.setVisibility(View.GONE);
            llBottom.setVisibility(View.GONE);
        } else {
            tvEdited.setVisibility(View.VISIBLE);
            llBottom.setVisibility(View.VISIBLE);

            SimpleDateFormat inputPattern = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
            Date inputDate = null;
            try {
                inputDate = inputPattern.parse(listItem.getDatModifiedDateAndTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            DateFormat gmtFormat = new SimpleDateFormat("MMM dd");//, h:mm a
            DateFormat gmtTimeFormat = new SimpleDateFormat("h:mm a");
            gmtFormat.setTimeZone(TimeZone.getTimeZone("IST"));
            gmtTimeFormat.setTimeZone(TimeZone.getTimeZone("IST"));

            if (inputDate != null) {
                Boolean check_today = DateUtils.isToday(inputDate);
                Boolean check_yesterday = DateUtils.isYesterday(inputDate);
                if (check_today) {
                    tvEdited.setText("" + "Edited: Today " + gmtTimeFormat.format(inputDate));
                } else if (check_yesterday) {
                    tvEdited.setText("" + "Edited: Yesterday " + gmtTimeFormat.format(inputDate));
                } else {
                    tvEdited.setText("" + "Edited: " + gmtFormat.format(inputDate));
                }
            } else {
                tvEdited.setText("" + "Edited: " + gmtFormat.format(inputDate));
            }

        }

        setValues(listItem);

        setImages(imgDetailss);

        arrAudioDetails.clear();
        audDeletUpdated.clear();
        Log.e("audio_dataserver", listItem.getFileAudioList().size() + "....");
        arrAudioDetails.addAll(listItem.getFileAudioList());
        audDeletUpdated.addAll(listItem.getFileAudioList());

        if (arrAudioDetails.size() == 0) {
            llAudioOne.setVisibility(View.GONE);
            tvMoreAudio.setVisibility(View.GONE);
        } else {

            tvMoreAudio.setVisibility(View.VISIBLE);
            tvMoreAudio.setText("+ (" + (arrAudioDetails.size() - 1) + " )");
            isNewAudio = "yes";

            AudFile audFile = new AudFile();

            for (int i = 0; i < arrAudioDetails.size(); i++) {

                if (i == 0) {
                    final int[] playStatus = {0};
                    llAudioOne.setVisibility(View.VISIBLE);
                    llAudioTwo.setVisibility(View.GONE);
                    llAudioThree.setVisibility(View.GONE);

                    String uri = URL_ImageUpload_ROOT + arrAudioDetails.get(0).getFilename();
                    Log.e("Aud_uri_detail", arrAudioDetails.size() + "...." + uri);
//                seekBarOne = findViewById(R.id.seekBar);
                    tvAudioOne.setText(arrAudioDetails.get(0).getStrDuration());
                    mediaPlayerOne = MediaPlayer.create(NoteDetailActivity.this, Uri.parse(uri));

                    audFile.setDuration(arrAudioDetails.get(0).getStrDuration());
                    audFile.setFile(new File(uri));

                    ivPlayOne.setOnClickListener(v -> {

                        isAudioPlaying = true;
                        if (playStatus[0] == 0) {//play

                            Glide.with(NoteDetailActivity.this)
                                    .load(R.drawable.ic_pause_blue)
                                    .dontAnimate()
                                    .into(ivPlayOne);


                            mediaPlayerOne.start();
                            int oneTimeOnly = 0;

                            finalTime = mediaPlayerOne.getDuration();
                            startTime = mediaPlayerOne.getCurrentPosition();
                            if (oneTimeOnly == 0) {
                                seekBarOne.setMax((int) finalTime);
                                oneTimeOnly = 1;
                            }
                            seekBarOne.setProgress((int) startTime);
                            myHandler.postDelayed(UpdateSongTimeOne, 100);
                            playStatus[0] = 1;

                        } else {//pause

                            Glide.with(NoteDetailActivity.this)
                                    .load(R.drawable.ic_play_blue)
                                    .dontAnimate()
                                    .into(ivPlayOne);
                            playStatus[0] = 0;
                            finalTime = mediaPlayerOne.getDuration();
                            startTime = mediaPlayerOne.getCurrentPosition();

                            mediaPlayerOne.pause();

                        }

                    });
//                    mediaPlayerOne.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                        @Override
//                        public void onCompletion(MediaPlayer mp) {
//                            isAudioPlaying = false;
//                            mediaPlayerOne.seekTo(0);
//                            Glide.with(NoteDetailActivity.this)
//                                    .load(R.drawable.ic_play_blue)
//                                    .dontAnimate()
//                                    .into(ivPlayOne);
//                        }
//                    });
                    seekBarOne.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                            if (fromUser) {
                                mediaPlayerOne.seekTo(progress);
                                seekBar.setProgress(progress);
                                mediaPlayerOne.start();
                                Glide.with(NoteDetailActivity.this)
                                        .load(R.drawable.ic_pause_blue)
                                        .dontAnimate()
                                        .into(ivPlayOne);
                            }
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {

                        }

                    });


                } else if (i == 1) {
                    final int[] playStatus = {0};
//                    llAudioOne.setVisibility(View.VISIBLE);
//                    llAudioTwo.setVisibility(View.VISIBLE);
                    llAudioThree.setVisibility(View.GONE);

                    String uri = URL_ImageUpload_ROOT + arrAudioDetails.get(1).getFilename();
//                seekBarTwo = findViewById(R.id.seekBar);
                    tvAudioTwo.setText(arrAudioDetails.get(1).getStrDuration());
                    mediaPlayerTwo = MediaPlayer.create(NoteDetailActivity.this, Uri.parse(uri));

                    audFile.setDuration(arrAudioDetails.get(1).getStrDuration());
                    audFile.setFile(new File(uri));
                    ivPlayTwo.setOnClickListener(v -> {

                        isAudioPlaying = true;
                        if (playStatus[0] == 0) {//play

                            Glide.with(NoteDetailActivity.this)
                                    .load(R.drawable.ic_pause_blue)
                                    .dontAnimate()
                                    .into(ivPlayTwo);


                            mediaPlayerTwo.start();
                            int oneTimeOnly = 0;

                            finalTime = mediaPlayerTwo.getDuration();
                            startTime = mediaPlayerTwo.getCurrentPosition();
                            if (oneTimeOnly == 0) {
                                seekBarTwo.setMax((int) finalTime);
                                oneTimeOnly = 1;
                            }
                            seekBarTwo.setProgress((int) startTime);
                            myHandler.postDelayed(UpdateSongTimeTwo, 100);
                            playStatus[0] = 1;

                        } else {//pause

                            Glide.with(NoteDetailActivity.this)
                                    .load(R.drawable.ic_play_blue)
                                    .dontAnimate()
                                    .into(ivPlayTwo);
                            playStatus[0] = 0;
                            finalTime = mediaPlayerTwo.getDuration();
                            startTime = mediaPlayerTwo.getCurrentPosition();

                            mediaPlayerTwo.pause();

                        }

                    });

                } else if (i == 2) {
                    final int[] playStatus = {0};
                    String uri = URL_ImageUpload_ROOT + arrAudioDetails.get(2).getFilename();
                    tvAudioThree.setText(arrAudioDetails.get(2).getStrDuration());
                    mediaPlayerThree = MediaPlayer.create(NoteDetailActivity.this, Uri.parse(uri));

                    audFile.setDuration(arrAudioDetails.get(2).getStrDuration());
                    audFile.setFile(new File(uri));
                    ivPlayThree.setOnClickListener(v -> {

                        isAudioPlaying = true;
                        if (playStatus[0] == 0) {//play

                            Glide.with(NoteDetailActivity.this)
                                    .load(R.drawable.ic_pause_blue)
                                    .dontAnimate()
                                    .into(ivPlayThree);


                            mediaPlayerThree.start();
                            int oneTimeOnly = 0;

                            finalTime = mediaPlayerThree.getDuration();
                            startTime = mediaPlayerThree.getCurrentPosition();
                            if (oneTimeOnly == 0) {
                                seekBarThree.setMax((int) finalTime);
                                oneTimeOnly = 1;
                            }
                            seekBarThree.setProgress((int) startTime);
                            myHandler.postDelayed(UpdateSongTimeThree, 100);
                            playStatus[0] = 1;

                        } else {//pause

                            Glide.with(NoteDetailActivity.this)
                                    .load(R.drawable.ic_play_blue)
                                    .dontAnimate()
                                    .into(ivPlayThree);
                            playStatus[0] = 0;
                            finalTime = mediaPlayerThree.getDuration();
                            startTime = mediaPlayerThree.getCurrentPosition();

                            mediaPlayerThree.pause();

                        }

                    });

//                    mediaPlayerThree.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                        @Override
//                        public void onCompletion(MediaPlayer mp) {
//                            isAudioPlaying = false;
//                            mediaPlayerThree.seekTo(0);
//                            Glide.with(NoteDetailActivity.this)
//                                    .load(R.drawable.ic_play_blue)
//                                    .dontAnimate()
//                                    .into(ivPlayThree);
//                        }
//                    });
                    seekBarThree.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                            if (fromUser) {
                                mediaPlayerThree.seekTo(progress);
                                seekBar.setProgress(progress);
                                mediaPlayerThree.start();
                                Glide.with(NoteDetailActivity.this)
                                        .load(R.drawable.ic_pause_blue)
                                        .dontAnimate()
                                        .into(ivPlayThree);
                            }
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {

                        }

                    });

                }

            }

            arrFileAudio.add(audFile);

        }

        if (objRemainderVal.getStrRemainderTime() == null) {

            tvTimer.setVisibility(View.GONE);

        } else {

            tvTimer.setVisibility(View.VISIBLE);
            Date date1 = null;
            String date = Functions.formatDate("yyyy-MM-dd", "MMMM dd", objRemainderVal.getStrRemainderDate());
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            try {
                date1 = format.parse(objRemainderVal.getStrRemainderDate());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Boolean check_today = DateUtils.isToday(date1);
            Boolean check_yesterday = DateUtils.isYesterday(date1);
            Boolean check_tomorrow = DateUtils.isTomorrow(date1);

            if (check_today) {
                tvTimer.setText("Today " + objRemainderVal.getStrRemainderTime());
            } else if (check_yesterday) {
                tvTimer.setText("Yesterday " + objRemainderVal.getStrRemainderTime());
            } else if (check_tomorrow) {
                tvTimer.setText("Tomorrow " + objRemainderVal.getStrRemainderTime());
            } else {
                tvTimer.setText(date + ", " + objRemainderVal.getStrRemainderTime());
            }

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint("ResourceType")
    public void textFieldValue(ArrayList<UndoTextsModel> undoTexts) {

        rlCheckbox.removeAllViews();
        rlCheckbox.setVisibility(View.VISIBLE);
        ivAdd.setVisibility(View.GONE);

        if (!undoTexts.isEmpty()) {

            alleditText.clear();
            alleditTextCheckbox.clear();
            String toPrint = "";

            for (int i = 0; i < undoTexts.size(); i++) {
                editText = new CustomEditText(NoteDetailActivity.this);
                editText.setText(undoTexts.get(i).getStrNoteListValue());
                editText.setLeft(10);
                editText.setId(i);
                editText.setHint("");
                textStyle(editText);
                alleditText.add(editText);
                rlCheckbox.addView(editText, i);
                isActiveText = "Yes";
                int finalI = i;

                editText.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                        isCheckboxChanged = true;
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        redoTextsArraylist.clear();
                        ivRedo.setEnabled(false);
                        ivRedo.setColorFilter(colorGrey);
                        llUndos.setVisibility(View.VISIBLE);
                        llBottom.setVisibility(View.VISIBLE);
                        if (undoTextsArraylist.size() == 0) {

                            ivUndo.setEnabled(false);
                            ivUndo.setColorFilter(colorGrey);

                        } else {
                            ivUndo.setEnabled(true);
                            ivUndo.setColorFilter(colorBlack);
                        }

                        isChanged = "Yes";
                        isActiveText = "No";
                        UndoalleditText = alleditText;

                    }
                });

                rlMain.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        editText.setSelection(editText.getText().length());
                        editText.requestFocus();
                        editText.setCursorVisible(true);
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                        return false;
                    }
                });

            }
            loopViews(rlCheckbox);
        }
    }

    public void setImages(RealmList<ImgDetails> imgDetail) {

        if (imgDetail.size() == 0) {
            flImage.setVisibility(View.GONE);
        } else {
            flImage.setVisibility(View.VISIBLE);
            if (imgDetail.size() == 1) {
                ivImageOne.setVisibility(View.VISIBLE);
                ivImageTwo.setVisibility(View.GONE);
                ivImageThree.setVisibility(View.GONE);
            } else if (imgDetail.size() == 2) {
                ivImageOne.setVisibility(View.VISIBLE);
                ivImageTwo.setVisibility(View.VISIBLE);
                ivImageThree.setVisibility(View.GONE);
            } else if (imgDetail.size() == 3) {
                ivImageOne.setVisibility(View.VISIBLE);
                ivImageTwo.setVisibility(View.VISIBLE);
                ivImageThree.setVisibility(View.VISIBLE);

            }
            mArrayUri.clear();
            imgPath = String.valueOf(fileProfile);
            for (int i = 0; i < imgDetail.size(); i++) {
                isNewImage = "yes";
                int imageHeight = imgDetail.get(i).getIntHeight();
                int imageWidth = imgDetail.get(i).getIntWeight();
                String imagePath = URL_ImageUpload_ROOT + imgDetail.get(i).getFilename();
                mArrayUri.add(imagePath);

                ImageView img;
                if (i == 0) {
                    img = ivImageOne;
                } else if (i == 1) {
                    img = ivImageTwo;
                } else {
                    img = ivImageThree;
                }
                Log.e("Img_Hgt", "..." + imagePath + "..." + imageWidth + "..." + imageHeight);
                if (imageWidth != 0 && imageHeight != 0) {

                    double height = getImageHeight(this, imageWidth, imageHeight);
                    fileProfile = new File(imagePath);
                    ViewGroup.LayoutParams params = img.getLayoutParams();
                    params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                    params.height = (int) height;
                    img.setLayoutParams(params);
                    Picasso.get()
                            .load(imagePath)
                            .noFade()
                            .into(img, new Callback() {
                                @Override
                                public void onSuccess() {
                                    supportStartPostponedEnterTransition();
                                }

                                @Override
                                public void onError(Exception e) {
                                    supportStartPostponedEnterTransition();
                                }

                            });

                    imgPath = imagePath;

                }

            }

            imgPath = String.valueOf(fileProfile);
        }
    }

    @SuppressLint({"ResourceType", "ClickableViewAccessibility"})
    public void checkBoxeValue(ArrayList<UndoCheckboxModel> undoCheckboxes) {
        alleditText.clear();
        rlCheckbox.removeAllViews();
        rlCheckbox.setVisibility(View.VISIBLE);
        alleditTextCheckbox.clear();
        checkboxCount = undoCheckboxes.size() - 1;
        for (int i = 0; i < undoCheckboxes.size(); i++) {

            rlCheckbox = findViewById(R.id.rlCheckbox);
            final View view1 = inflater.inflate(R.layout.checkbox_layout, null);
            RelativeLayout llMain = view1.findViewById(R.id.llMain);
            final CustomEditText etTextCheckBox = view1.findViewById(R.id.etText);
            final ImageView ivCheckbox = view1.findViewById(R.id.ivCheckbox);
            final ImageView ivClose = view1.findViewById(R.id.ivClose);

            etTextCheckBox.setId(i);
            etTextCheckBox.setSelection(etTextCheckBox.getText().length());
            textStyleCheckbox(etTextCheckBox);
            etTextCheckBox.setText(undoCheckboxes.get(i).getStrNoteListValue());
            rlCheckbox.addView(view1, i);

            isActiveCheckbox = "Yes";
            alleditTextCheckbox.add(etTextCheckBox);
            final int finalI = i;

            etTextCheckBox.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    isCheckboxChanged = true;
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {

                    redoCheckboxesArraylist.clear();
                    ivRedo.setEnabled(false);
                    ivRedo.setColorFilter(colorGrey);
                    llUndos.setVisibility(View.VISIBLE);
                    llBottom.setVisibility(View.VISIBLE);
                    if (undoCheckboxesArraylist.size() == 0) {
                        ivUndo.setEnabled(false);
                        ivUndo.setColorFilter(colorGrey);
                    } else {
                        ivUndo.setEnabled(true);
                        ivUndo.setColorFilter(colorBlack);
                    }

                    isChangedCheckbox = "Yes";
                    isActiveCheckbox = "No";
                    UndoalleditText = alleditTextCheckbox;

                }
            });

            int finalI1 = i;

            rlMain.setOnTouchListener((view, motionEvent) -> {

                etTextCheckBox.setSelection(etTextCheckBox.getText().length());
                etTextCheckBox.requestFocus();
                etTextCheckBox.setCursorVisible(true);
                return false;

            });

            llMain.setOnTouchListener((view, motionEvent) -> {
                ivClose.setVisibility(View.GONE);//VISIBLE
                etTextCheckBox.setCursorVisible(true);
                return false;
            });

            etTextCheckBox.setOnTouchListener((view, motionEvent) -> {
                ivClose.setVisibility(View.GONE);//VISIBLE
                etTextCheckBox.setCursorVisible(true);
                return false;
            });

            ivClose.setOnTouchListener((view, motionEvent) -> {

                ivClose.setAlpha(0f);
                return false;
            });
        }
        loopViewsCheckbox(rlCheckbox);
    }


    private boolean mayRequest() {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestAudioPermissions() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {

            //When permission is not granted by user, show them message why this permission is needed.
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.RECORD_AUDIO)) {
                Toast.makeText(this, "Please grant permissions to record audio", Toast.LENGTH_LONG).show();

                //Give user option to still opt-in the permissions
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        MY_PERMISSIONS_RECORD_AUDIO);

            } else {
                // Show user dialog to grant permission to record audio
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        MY_PERMISSIONS_RECORD_AUDIO);
            }
        }
        //If permission is granted, then go ahead recording audio
        else if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO)
                == PackageManager.PERMISSION_GRANTED) {

            //Go ahead with recording audio now
            if (arrAudioDetails.size() <= 3) {//checking if audio is presnt
                recordAudio();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_RECORD_AUDIO: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay!
                    recordAudio();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "Permissions Denied to record audio", Toast.LENGTH_LONG).show();
                }
                return;
            }
            case IMAGE_PICKER_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (isCam) {////true= camera,false=gallery
                        Intent intent5 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent5, IMAGE_PICKER_REQUEST_CODE);
                    } else {
//                        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                        startActivityForResult(i, IMAGE_PICKER_REQUEST_CODE);
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), IMAGE_PICKER_REQUEST_CODE);

                    }
                } else {
                    Toast.makeText(this, "Permissions Denied to upload image", Toast.LENGTH_LONG).show();
                }
            }
            case 0: {
                if (requestCode == 0) {
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                            && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
//                    takePictureButton.setEnabled(true);
                    }
                }
            }

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("IMAGE_det", IMAGE_PICKER_REQUEST_CODE + "..." + data);
        if (requestCode == IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {

            if (data != null && data.getData() != null) {
                Bitmap bitmap = null;
                int tot = imgDetailss.size();

                Uri uri = getImageUri(this, data);
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String path = storeImage(bitmap);
                fileProfile = new File(path);

                BitmapFactory.Options options = new BitmapFactory.Options();
                imageHeight = bitmap.getHeight();
                imageWidth = bitmap.getWidth();

                if (tot == 0) {//when no img is presnt in note
                    flImage.setVisibility(View.VISIBLE);
                    ivImageOne.setVisibility(View.VISIBLE);
                    ivImageTwo.setVisibility(View.GONE);
                    ivImageThree.setVisibility(View.GONE);
                    Glide.with(getApplicationContext())
                            .load(path)
                            .into(ivImageOne);
                } else if (tot == 1) {//when one image is already present
                    ivImageOne.setVisibility(View.VISIBLE);
                    ivImageTwo.setVisibility(View.VISIBLE);
                    Glide.with(getApplicationContext())
                            .load(path)
                            .into(ivImageTwo);
                } else if (tot == 2) {//when 2 image is already present
                    ivImageTwo.setVisibility(View.VISIBLE);
                    ivImageThree.setVisibility(View.VISIBLE);
                    Glide.with(getApplicationContext())
                            .load(path)
                            .into(ivImageThree);
                }

                saveImage(fileProfile, imageHeight, imageWidth);

            } else if (data != null && getImageUri(this, data) != null) {
                Bitmap bitmap = null;
                Uri uri = getImageUri(this, data);
                try {
                    isNewImage = "new";
                    Bitmap bitmap1 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                    String path = storeImage(bitmap1);
                    File newfile = new File(RealPathUtil.getPath(getApplicationContext(), uri));
                    fileProfile = new File(path);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    bitmap = BitmapFactory.decodeFile(newfile.getAbsolutePath(), options);
                    imageHeight = options.outHeight;
                    imageWidth = options.outWidth;
                    double height = getImageHeight(this, imageWidth, imageHeight);
//                    ViewGroup.LayoutParams params = ivImage.getLayoutParams();
//                    params.width = ViewGroup.LayoutParams.MATCH_PARENT;
//                    params.height = (int) height;

                    flImage.setVisibility(View.VISIBLE);

                    int tot = imgDetailss.size();
                    if (tot == 0) {//when no img is presnt in note
                        flImage.setVisibility(View.VISIBLE);
                        ivImageOne.setVisibility(View.VISIBLE);
                        ivImageTwo.setVisibility(View.GONE);
                        ivImageThree.setVisibility(View.GONE);
                        Glide.with(getApplicationContext())
                                .load(path)
                                .into(ivImageOne);
                    } else if (tot == 1) {//when one image is already present
                        ivImageOne.setVisibility(View.VISIBLE);
                        ivImageTwo.setVisibility(View.VISIBLE);
                        Glide.with(getApplicationContext())
                                .load(path)
                                .into(ivImageTwo);
                    } else if (tot == 2) {//when 2 image is already present
                        ivImageTwo.setVisibility(View.VISIBLE);
                        ivImageThree.setVisibility(View.VISIBLE);
                        Glide.with(getApplicationContext())
                                .load(path)
                                .into(ivImageThree);
                    }
                    saveImage(fileProfile, imageHeight, imageWidth);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (data != null && data.getClipData() != null) {
                mClipData = data.getClipData();
                Log.e("IMAGE_all", "..." + mClipData);

                if (mClipData.getItemCount() != 0) {
                    int totCount = imgDetailss.size() + mClipData.getItemCount();

                    if ((mClipData.getItemCount() > 3) || totCount > 3) {
                        showToast("Can't select more than 3 images");
                    } else {
                        flImage.setVisibility(View.VISIBLE);

                        for (int i = 0; i < mClipData.getItemCount(); i++) {
                            ClipData.Item item = mClipData.getItemAt(i);
                            Uri uri = item.getUri();

//                        mArrayUr.add(uri.toString());

                            try {
                                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mClipData.getItemAt(i).getUri());
                                String selectedFilePath = getPath(getApplicationContext(), mClipData.getItemAt(i).getUri());
//                            String path = storeImage(bitmap);

                                File fileNew = new File(selectedFilePath);

                                ImgFile imgFile = new ImgFile();
                                imgFile.setFile(fileNew);

                                imageHeight = bitmap.getHeight();
                                imageWidth = bitmap.getWidth();

                                imgFile.setHeight(imageHeight);
                                imgFile.setWeight(imageWidth);
                                fileArrayList.add(imgFile);

                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            //double height = getReducedImageHeight(this, imageWidth, imageHeight);

                        }

                        if (isNetworkAvailable()) {
                            loadImage();
                        }
                    }
                }

            }
        } else if (requestCode == REQUEST_CODE) {
            Calendar mcurrentTime = Calendar.getInstance();
            Date date = mcurrentTime.getTime();
            DateFormat gmtFormat = new SimpleDateFormat("MMM dd");//, h:mm a
            DateFormat gmtTimeFormat = new SimpleDateFormat("h:mm a");
            gmtFormat.setTimeZone(istTime);
            gmtTimeFormat.setTimeZone(istTime);
            SimpleDateFormat mdformat = new SimpleDateFormat("h:mm a");
            mdformat.setTimeZone(istTime);
            dateModified = mdformat.format(date);
            tvEdited.setVisibility(View.VISIBLE);
            llBottom.setVisibility(View.VISIBLE);
            tvEdited.setText("Today " + dateModified);
            plannerID = data.getStringExtra("ID");
            final ArrayList<ColleborateDatum> Colleborators = data.getParcelableArrayListExtra("Colleborators");

            if (isNetworkAvailable()) {
                arryCollaboratUserData.clear();
                llColleborate.removeAllViews();
                getColleborateUsers();
            }

        } else if (requestCode == 1001) {

            int plannerId = Integer.parseInt(data.getStringExtra("plannerId"));

            RealmResults<Item> itemsRealmResults = mRealm.where(Item.class).equalTo("intPlannerId", plannerId).findAll();
            Log.e("onActivity", plannerId + "..." + itemsRealmResults.size() + "...");
//            mRealm.copyToRealmOrUpdate(itemsRealmResults.get(0));
//            mRealm.commitTransaction();
            imgDetailss.clear();
            imgDetailss.addAll(itemsRealmResults.get(0).getImgNoteList());
            setImages(imgDetailss);

        }

    }

    public void recordAudio() {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int displayWidth = displayMetrics.widthPixels;
        int displayHeight = displayMetrics.heightPixels;

        AddAudioDialog.newIntance(displayWidth, displayHeight, new AddAudioDialog.Callback() {

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void edit(AddAudioDialog dialog, File audioFile, String time) {
                Calendar mcurrentTime = Calendar.getInstance();
                Date date = mcurrentTime.getTime();
                DateFormat gmtFormat = new SimpleDateFormat("MMM dd");//, h:mm a
                DateFormat gmtTimeFormat = new SimpleDateFormat("h:mm a");
                gmtFormat.setTimeZone(istTime);
                gmtTimeFormat.setTimeZone(istTime);

                SimpleDateFormat mdformat = new SimpleDateFormat("h:mm a");
                mdformat.setTimeZone(istTime);
                dateModified = mdformat.format(date);
                tvEdited.setVisibility(View.VISIBLE);
                tvEdited.setText("Today " + dateModified);

                Boolean isONetwork = isOnline(NoteDetailActivity.this);
                audStatus = "Update";
                isNewAudio = "new";
                if (isONetwork) {
//                    fileAudio = audioFile;
                    audStatus = "Update";
                    if (audioFile.exists()) {//not >arrAudioDetails.size()
                        saveAudio(audioFile, time);
                    }
                } else {
                    setAudioDatas();
                }

            }
        }).show(getSupportFragmentManager(), "AddNewTaskDialog");
    }

    public void setAudioDatas() {

        Log.e("audio_setAudioDatas", arrAudioDetails.size() + "...");
//        if (arrAudioDetails.size() > 1) {
//            tvMoreAudio.setVisibility(View.VISIBLE);
//        } else tvMoreAudio.setVisibility(View.GONE);

        if (isAudioExpanded) {
//            tvMoreAudio.setVisibility(View.GONE);
        } else {
            if (arrAudioDetails.size() == 1) tvMoreAudio.setVisibility(View.GONE);
            else if (arrAudioDetails.size() > 1) {
                tvMoreAudio.setVisibility(View.VISIBLE);
                tvMoreAudio.setText("+ (" + (arrAudioDetails.size() - 1) + " )");
            }
        }
//

        AudFile audFile = new AudFile();
        for (int i = 0; i < arrAudioDetails.size(); i++) {

            if (i == 0) {
                final int[] playStatus = {0};
                llAudioOne.setVisibility(View.VISIBLE);
                llAudioTwo.setVisibility(View.GONE);
                llAudioThree.setVisibility(View.GONE);

                String uri = URL_ImageUpload_ROOT + arrAudioDetails.get(0).getFilename();
//                seekBarOne = findViewById(R.id.seekBar);
                tvAudioOne.setText(arrAudioDetails.get(0).getStrDuration());
                mediaPlayerOne = MediaPlayer.create(NoteDetailActivity.this, Uri.parse(uri));

                audFile.setDuration(arrAudioDetails.get(0).getStrDuration());
                audFile.setFile(new File(uri));

                ivPlayOne.setOnClickListener(v -> {

                    isAudioPlaying = true;
                    if (playStatus[0] == 0) {//play

                        Glide.with(NoteDetailActivity.this)
                                .load(R.drawable.ic_pause_blue)
                                .dontAnimate()
                                .into(ivPlayOne);


                        mediaPlayerOne.start();
                        int oneTimeOnly = 0;

                        finalTime = mediaPlayerOne.getDuration();
                        startTime = mediaPlayerOne.getCurrentPosition();
                        if (oneTimeOnly == 0) {
                            seekBarOne.setMax((int) finalTime);
                            oneTimeOnly = 1;
                        }
                        seekBarOne.setProgress((int) startTime);
                        myHandler.postDelayed(UpdateSongTimeOne, 100);
                        playStatus[0] = 1;

                    } else {//pause

                        Glide.with(NoteDetailActivity.this)
                                .load(R.drawable.ic_play_blue)
                                .dontAnimate()
                                .into(ivPlayOne);
                        playStatus[0] = 0;
                        finalTime = mediaPlayerOne.getDuration();
                        startTime = mediaPlayerOne.getCurrentPosition();

                        mediaPlayerOne.pause();

                    }

                });

                mediaPlayerOne.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        isAudioPlaying = false;
                        mediaPlayerOne.seekTo(0);
                        Glide.with(NoteDetailActivity.this)
                                .load(R.drawable.ic_play_blue)
                                .dontAnimate()
                                .into(ivPlayOne);
                    }
                });
                seekBarOne.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (fromUser) {
                            mediaPlayerOne.seekTo(progress);
                            seekBar.setProgress(progress);
                            mediaPlayerOne.start();
                            Glide.with(NoteDetailActivity.this)
                                    .load(R.drawable.ic_pause_blue)
                                    .dontAnimate()
                                    .into(ivPlayOne);
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }

                });


            } else if (i == 1) {
                final int[] playStatus = {0};
//                    llAudioOne.setVisibility(View.VISIBLE);
//                    llAudioTwo.setVisibility(View.VISIBLE);
                llAudioThree.setVisibility(View.GONE);

                String uri = URL_ImageUpload_ROOT + arrAudioDetails.get(1).getFilename();
//                seekBarTwo = findViewById(R.id.seekBar);
                tvAudioTwo.setText(arrAudioDetails.get(1).getStrDuration());
                mediaPlayerTwo = MediaPlayer.create(NoteDetailActivity.this, Uri.parse(uri));

                audFile.setDuration(arrAudioDetails.get(1).getStrDuration());
                audFile.setFile(new File(uri));
                ivPlayTwo.setOnClickListener(v -> {

                    isAudioPlaying = true;
                    if (playStatus[0] == 0) {//play

                        Glide.with(NoteDetailActivity.this)
                                .load(R.drawable.ic_pause_blue)
                                .dontAnimate()
                                .into(ivPlayTwo);


                        mediaPlayerTwo.start();
                        int oneTimeOnly = 0;

                        finalTime = mediaPlayerTwo.getDuration();
                        startTime = mediaPlayerTwo.getCurrentPosition();
                        if (oneTimeOnly == 0) {
                            seekBarTwo.setMax((int) finalTime);
                            oneTimeOnly = 1;
                        }
                        seekBarTwo.setProgress((int) startTime);
                        myHandler.postDelayed(UpdateSongTimeTwo, 100);
                        playStatus[0] = 1;

                    } else {//pause

                        Glide.with(NoteDetailActivity.this)
                                .load(R.drawable.ic_play_blue)
                                .dontAnimate()
                                .into(ivPlayTwo);
                        playStatus[0] = 0;
                        finalTime = mediaPlayerTwo.getDuration();
                        startTime = mediaPlayerTwo.getCurrentPosition();

                        mediaPlayerTwo.pause();

                    }

                });

            } else if (i == 2) {
                final int[] playStatus = {0};
                String uri = URL_ImageUpload_ROOT + arrAudioDetails.get(2).getFilename();
//                seekBarThree = findViewById(R.id.seekBar);
                tvAudioThree.setText(arrAudioDetails.get(2).getStrDuration());
                mediaPlayerThree = MediaPlayer.create(NoteDetailActivity.this, Uri.parse(uri));

                audFile.setDuration(arrAudioDetails.get(2).getStrDuration());
                audFile.setFile(new File(uri));
                ivPlayThree.setOnClickListener(v -> {

                    isAudioPlaying = true;
                    if (playStatus[0] == 0) {//play

                        Glide.with(NoteDetailActivity.this)
                                .load(R.drawable.ic_pause_blue)
                                .dontAnimate()
                                .into(ivPlayThree);


                        mediaPlayerThree.start();
                        int oneTimeOnly = 0;

                        finalTime = mediaPlayerThree.getDuration();
                        startTime = mediaPlayerThree.getCurrentPosition();
                        if (oneTimeOnly == 0) {
                            seekBarThree.setMax((int) finalTime);
                            oneTimeOnly = 1;
                        }
                        seekBarThree.setProgress((int) startTime);
                        myHandler.postDelayed(UpdateSongTimeThree, 100);
                        playStatus[0] = 1;

                    } else {//pause

                        Glide.with(NoteDetailActivity.this)
                                .load(R.drawable.ic_play_blue)
                                .dontAnimate()
                                .into(ivPlayThree);
                        playStatus[0] = 0;
                        finalTime = mediaPlayerThree.getDuration();
                        startTime = mediaPlayerThree.getCurrentPosition();

                        mediaPlayerThree.pause();

                    }

                });

                mediaPlayerThree.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        isAudioPlaying = false;
                        mediaPlayerThree.seekTo(0);
                        Glide.with(NoteDetailActivity.this)
                                .load(R.drawable.ic_play_blue)
                                .dontAnimate()
                                .into(ivPlayThree);
                    }
                });
                seekBarThree.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (fromUser) {
                            mediaPlayerThree.seekTo(progress);
                            seekBar.setProgress(progress);
                            mediaPlayerThree.start();
                            Glide.with(NoteDetailActivity.this)
                                    .load(R.drawable.ic_pause_blue)
                                    .dontAnimate()
                                    .into(ivPlayThree);
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }

                });

            }

        }
    }

    private void SaveImageToFolder(Bitmap bm) {

        File folder = new File(Environment.getExternalStorageDirectory() + File.separator + "WorFlow");
        File newfolder = new File(folder, "WorFlow Images");

        boolean success = true;

        if (!newfolder.exists()) {

            success = newfolder.mkdirs();
        }
        if (success) {
            // Do something on success
        } else {
            // Do something else on failure
        }
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        File f = new File(newfolder + File.separator + "IMG_" + timestamp + ".jpg");
        try {

            f.createNewFile();

            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @TargetApi(Build.VERSION_CODES.N)
    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ResourceType")
    @OnClick({R.id.ivBack, R.id.tvMoreAudio, R.id.ivAudioDeleteOne, R.id.ivAudioDeleteTwo, R.id.ivAudioDeleteThree, R.id.ivAdd, R.id.llPin, R.id.llRemainder, R.id.tvTimer, R.id.ivArchieve, R.id.llCamera, R.id.llGallery, R.id.llColleborateIcon, R.id.llRecord, R.id.llCheckbox, R.id.llGmail, R.id.ivUndo, R.id.ivRedo, R.id.ivImageOne, R.id.ivImageTwo, R.id.ivImageThree})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.ivBack:

                onBackPressed();

                break;

//            case R.id.ivImgDelet:
//
//                if (isNetworkAvailable()) {
//                    fileProfile = new File("");
//                    imageHeight = 0;
//                    imageWidth = 0;
//                    saveImage(fileProfile, imageHeight, imageWidth);
//                } else {
//                    String status = Application.getConnectivityStatusString(this);
//                    setSnackbarMessage(status, false);
//                }
//
//                break;

            case R.id.tvMoreAudio:

                if (kCount == 0) {
                    isAudioExpanded = true;
                    llAudioOne.setVisibility(View.VISIBLE);
                    if (arrAudioDetails.size() == 3) {
                        llAudioTwo.setVisibility(View.VISIBLE);
                        llAudioThree.setVisibility(View.VISIBLE);
                    } else if (arrAudioDetails.size() == 2) {
                        llAudioTwo.setVisibility(View.VISIBLE);
                    }
                    tvMoreAudio.setText("+ less");
                    kCount = 1;
                } else {
                    isAudioExpanded = false;
                    llAudioOne.setVisibility(View.VISIBLE);
                    if (arrAudioDetails.size() == 3) {
                        llAudioTwo.setVisibility(View.GONE);
                        llAudioThree.setVisibility(View.GONE);
                    } else if (arrAudioDetails.size() == 2) {
                        llAudioTwo.setVisibility(View.GONE);
                    }
                    tvMoreAudio.setText("+ (" + (arrAudioDetails.size() - 1) + " )");
                    kCount = 0;
                }
                break;
            case R.id.ivAudioDeleteOne:
                if (isNetworkAvailable()) {
                    llAudioOne.setVisibility(View.GONE);
                    deleteAudio(audDeletUpdated.get(0));
                } else {
                    String status = Application.getConnectivityStatusString(this);
                    setSnackbarMessage(status, false);
                }
                break;

            case R.id.ivAudioDeleteTwo:
                if (isNetworkAvailable()) {
                    llAudioTwo.setVisibility(View.GONE);
                    deleteAudio(audDeletUpdated.get(1));
                } else {
                    String status = Application.getConnectivityStatusString(this);
                    setSnackbarMessage(status, false);
                }
                break;
            case R.id.ivAudioDeleteThree:

                if (isNetworkAvailable()) {
                    llAudioThree.setVisibility(View.GONE);
                    deleteAudio(audDeletUpdated.get(2));
                } else {
                    String status = Application.getConnectivityStatusString(this);
                    setSnackbarMessage(status, false);
                }

                break;

            case R.id.ivAdd:

//                addEnterKeyPressed();

                break;

            case R.id.llPin:
                if (arryCollaboratUserData.size() > 0) {
                    showToast("Shared Note cannot be pinned");
                } else {
                    notePin();
                }
                isCheckboxChanged = true;

                break;

            case R.id.llRemainder:
            case R.id.tvTimer:

                if (isNetworkAvailable()) {
                    String isThere = tvTimer.getText().toString();

                    if (isThere.length() != 0) {

                        String oldType = oneItemServer.get(0).getObjRemainder().getStrType();
                        String oldDate = oneItemServer.get(0).getObjRemainder().getStrRemainderDate();
                        String oldTime = oneItemServer.get(0).getObjRemainder().getStrRemainderTime();

                        ReminderDialog.newIntance(oldType, oldDate, oldTime, (dialog, newDate, time, type) -> {

                            if (newDate.length() != 0) {

                                Calendar mcurrentTime = Calendar.getInstance();
                                Date dateEdited = mcurrentTime.getTime();
                                DateFormat gmtFormat = new SimpleDateFormat("MMM dd");//, h:mm a
                                DateFormat gmtTimeFormat = new SimpleDateFormat("h:mm a");
                                gmtFormat.setTimeZone(istTime);
                                gmtTimeFormat.setTimeZone(istTime);

                                SimpleDateFormat mdformat = new SimpleDateFormat("h:mm a");
                                mdformat.setTimeZone(istTime);
                                dateModified = mdformat.format(dateEdited);
                                tvEdited.setVisibility(View.VISIBLE);
                                tvEdited.setText("Today " + dateModified);

                                try (Realm r = Realm.getDefaultInstance()) {
                                    r.executeTransaction((realm) -> {
                                        Item user = realm.where(Item.class).equalTo("intPlannerId", Integer.parseInt(plannerID)).findFirst();
                                        if (user != null) {
                                            ObjRemainder obj = new ObjRemainder();
                                            objRemainder.set_id(Integer.parseInt(plannerID));
                                            objRemainder.setBlnFlage(true);
                                            objRemainder.setStrRemainderDate(newDate);
                                            objRemainder.setStrRemainderTime(time);
                                            objRemainder.setStrType(type);
                                            user.setObjRemainder(objRemainder);
                                        }
                                    });
                                }

                                isCheckboxChanged = true;
                                tvTimer.setVisibility(View.VISIBLE);
                                Date date1 = null;
                                String date = Functions.formatDate("yyyy-MM-dd", "MMMM dd", newDate);
                                java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd");
                                try {
                                    date1 = format.parse(newDate);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                Boolean check_today = DateUtils.isToday(date1);
                                Boolean check_yesterday = DateUtils.isYesterday(date1);
                                Boolean check_tomorrow = DateUtils.isTomorrow(date1);
                                if (check_today) {
                                    tvTimer.setText("Today " + time);
                                } else if (check_yesterday) {
                                    tvTimer.setText("Yesterday " + time);
                                } else if (check_tomorrow) {
                                    tvTimer.setText("Tomorrow " + time);
                                } else {
                                    tvTimer.setText(date + ", " + time);
                                }
                            } else {
                                ObjRemainder obj = new ObjRemainder();
                                objRemainder = obj;
                                tvTimer.setVisibility(View.GONE);
                                tvTimer.setText("");
                            }
                        }).show(getSupportFragmentManager(), "AddNewTaskDialog");

                    } else {

                        ReminderDialog.newIntance("null", "null", "null", (dialog, newDate, time, type) -> {

                            Calendar mcurrentTime = Calendar.getInstance();
                            Date dateEdited = mcurrentTime.getTime();
                            DateFormat gmtFormat = new SimpleDateFormat("MMM dd");//, h:mm a
                            DateFormat gmtTimeFormat = new SimpleDateFormat("h:mm a");
                            gmtFormat.setTimeZone(istTime);
                            gmtTimeFormat.setTimeZone(istTime);

                            SimpleDateFormat mdformat = new SimpleDateFormat("h:mm a");
                            mdformat.setTimeZone(istTime);
                            dateModified = mdformat.format(dateEdited);
                            tvEdited.setVisibility(View.VISIBLE);
                            tvEdited.setText("Today " + dateModified);

                            try (Realm r = Realm.getDefaultInstance()) {
                                r.executeTransaction((realm) -> {
                                    Item user = realm.where(Item.class).equalTo("intPlannerId", Integer.parseInt(plannerID)).findFirst();
                                    if (user != null) {
                                        ObjRemainder obj = new ObjRemainder();
                                        objRemainder.set_id(Integer.parseInt(plannerID));
                                        objRemainder.setBlnFlage(true);
                                        objRemainder.setStrRemainderDate(newDate);
                                        objRemainder.setStrRemainderTime(time);
                                        objRemainder.setStrType(type);
                                        user.setObjRemainder(objRemainder);
                                    }
                                });
                            }
                            tvTimer.setVisibility(View.VISIBLE);

                            isCheckboxChanged = true;

                            Date date1 = null;
                            String date = Functions.formatDate("yyyy-MM-dd", "MMMM dd", newDate);
                            java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd");

                            try {
                                date1 = format.parse(newDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            Boolean check_today = DateUtils.isToday(date1);
                            Boolean check_yesterday = DateUtils.isYesterday(date1);
                            Boolean check_tomorrow = DateUtils.isTomorrow(date1);

                            if (check_today) {
                                tvTimer.setText("Today " + time);
                            } else if (check_yesterday) {
                                tvTimer.setText("Yesterday " + time);
                            } else if (check_tomorrow) {
                                tvTimer.setText("Tomorrow " + time);
                            } else {
                                tvTimer.setText(date + ", " + time);
                            }

                        }).show(getSupportFragmentManager(), "AddNewTaskDialog");

                    }
                } else {
                    showToast("No Internet Connection");
                }

                break;

            case R.id.ivArchieve:
                break;

            case R.id.llCamera:

                if (isNetworkAvailable()) {
                    isCam = true;//true= camera,false=gallery
//                    if (!imgPath.equals("NoData")) {

                    if (imgDetailss.size() >= 3) {
                        final BottomSheetDialog dialog = new BottomSheetDialog(NoteDetailActivity.this);
                        View sheetView = NoteDetailActivity.this.getLayoutInflater().inflate(R.layout.new_layout, null);
                        dialog.setContentView(sheetView);
                        dialog.setCanceledOnTouchOutside(true);
                        dialog.show();
                        TextView tvTitle = sheetView.findViewById(R.id.tvTitle);
                        tvTitle.setText("Can't upload more than 3 images");
                        Button btnProceed = sheetView.findViewById(R.id.btnProceed);
                        Button btnCancel = sheetView.findViewById(R.id.btnCancel);
                        btnProceed.setText("Ok");
                        btnProceed.setOnClickListener(view24 -> {

                            ArrayList<String> perms = new ArrayList<String>();
                            perms.add(Manifest.permission.CAMERA);
                            perms.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                perms.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                            }

                            final String[] permissions = perms.toArray(new String[perms.size()]);
                            if (Nammu.hasPermission(NoteDetailActivity.this, permissions)) {
                                ImagePicker.openImageIntent(NoteDetailActivity.this);
                                Intent intent5 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                file = Uri.fromFile(getOutputMediaFile());
                                startActivityForResult(intent5, IMAGE_PICKER_REQUEST_CODE);
//                            }

                            } else {

                                if (Nammu.shouldShowRequestPermissionRationale(NoteDetailActivity.this, permissions[0])
                                        || Nammu.shouldShowRequestPermissionRationale(NoteDetailActivity.this, permissions[1])
                                        || Nammu.shouldShowRequestPermissionRationale(NoteDetailActivity.this, permissions[2])
                                ) {
                                    ActivityCompat.requestPermissions(NoteDetailActivity.this,
                                            new String[]{Manifest.permission.CAMERA},
                                            IMAGE_PICKER_REQUEST_CODE);
                                } else {
                                    ActivityCompat.requestPermissions(NoteDetailActivity.this,
                                            new String[]{Manifest.permission.CAMERA},
                                            IMAGE_PICKER_REQUEST_CODE);
                                }
                            }
                            dialog.dismiss();
                        });

                        btnCancel.setOnClickListener(view23 -> dialog.dismiss());

                    } else {

                        ArrayList<String> perms = new ArrayList<String>();
                        perms.add(Manifest.permission.CAMERA);
                        perms.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            perms.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                        }

                        final String[] permissions = perms.toArray(new String[perms.size()]);
                        if (Nammu.hasPermission(NoteDetailActivity.this, permissions)) {

                            Intent intent5 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            file = Uri.fromFile(getOutputMediaFile());
                            startActivityForResult(intent5, IMAGE_PICKER_REQUEST_CODE);

                        } else {

                            if (Nammu.shouldShowRequestPermissionRationale(NoteDetailActivity.this, permissions[0])
                                    || Nammu.shouldShowRequestPermissionRationale(NoteDetailActivity.this, permissions[1])
                                    || Nammu.shouldShowRequestPermissionRationale(NoteDetailActivity.this, permissions[2])
                            ) {
                                ActivityCompat.requestPermissions(this,
                                        new String[]{Manifest.permission.CAMERA},
                                        IMAGE_PICKER_REQUEST_CODE);

                            } else {
                                ActivityCompat.requestPermissions(this,
                                        new String[]{Manifest.permission.CAMERA},
                                        IMAGE_PICKER_REQUEST_CODE);
                            }
                        }
                    }
                } else {
                    showToast("No Internet Connection");
                }

                break;

            case R.id.llGallery:

                showToast(imgDetailss.size() + "...." + imgDetailss.size());

                if (isNetworkAvailable()) {
                    isCam = false;//true= camera,false=gallery
//                    if (!imgPath.equals("NoData")) {

                    if (imgDetailss.size() >= 3) {
                        final BottomSheetDialog dialog = new BottomSheetDialog(NoteDetailActivity.this);
                        View sheetView = NoteDetailActivity.this.getLayoutInflater().inflate(R.layout.new_layout, null);
                        dialog.setContentView(sheetView);
                        dialog.setCanceledOnTouchOutside(true);
                        dialog.show();
                        TextView tvTitle = sheetView.findViewById(R.id.tvTitle);
                        tvTitle.setText("Can't upload more than 3 images");
                        Button btnProceed = sheetView.findViewById(R.id.btnProceed);
                        Button btnCancel = sheetView.findViewById(R.id.btnCancel);
                        btnProceed.setVisibility(View.GONE);
                        btnCancel.setText("Ok");
                        btnProceed.setOnClickListener(view24 -> {

                            ArrayList<String> perms = new ArrayList<String>();
                            perms.add(Manifest.permission.CAMERA);
                            perms.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                perms.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                            }

                            final String[] permissions = perms.toArray(new String[perms.size()]);
                            if (Nammu.hasPermission(NoteDetailActivity.this, permissions)) {

                                if (mayRequest()) {
//                                    Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                                    startActivityForResult(i, IMAGE_PICKER_REQUEST_CODE);
                                    Intent intent = new Intent();
                                    intent.setType("image/*");
                                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                                    intent.setAction(Intent.ACTION_GET_CONTENT);
                                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), IMAGE_PICKER_REQUEST_CODE);


//                                createImageIntent(NoteDetailActivity.this);
                                }
                            } else {
                                if (Nammu.shouldShowRequestPermissionRationale(NoteDetailActivity.this, permissions[0])
                                        || Nammu.shouldShowRequestPermissionRationale(NoteDetailActivity.this, permissions[1])
                                        || Nammu.shouldShowRequestPermissionRationale(NoteDetailActivity.this, permissions[2])
                                ) {
                                    ActivityCompat.requestPermissions(NoteDetailActivity.this,
                                            new String[]{Manifest.permission.CAMERA},
                                            IMAGE_PICKER_REQUEST_CODE);
                                } else {
                                    ActivityCompat.requestPermissions(NoteDetailActivity.this,
                                            new String[]{Manifest.permission.CAMERA},
                                            IMAGE_PICKER_REQUEST_CODE);
                                }
                            }
                            dialog.dismiss();
                        });

                        btnCancel.setOnClickListener(view23 -> dialog.dismiss());

                    } else {

                        ArrayList<String> perms = new ArrayList<String>();
                        perms.add(Manifest.permission.CAMERA);
                        perms.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            perms.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                        }

                        final String[] permissions = perms.toArray(new String[perms.size()]);
                        if (Nammu.hasPermission(NoteDetailActivity.this, permissions)) {
//                        openImageIntent(this);

//                            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                            startActivityForResult(i, IMAGE_PICKER_REQUEST_CODE);
//                            Intent intent = new Intent();
//                            intent.setType("image/*");
//                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//                            intent.setAction(Intent.ACTION_GET_CONTENT);
//                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), IMAGE_PICKER_REQUEST_CODE);

                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), IMAGE_PICKER_REQUEST_CODE);

                        } else {

                            if (Nammu.shouldShowRequestPermissionRationale(NoteDetailActivity.this, permissions[0])
                                    || Nammu.shouldShowRequestPermissionRationale(NoteDetailActivity.this, permissions[1])
                                    || Nammu.shouldShowRequestPermissionRationale(NoteDetailActivity.this, permissions[2])
                            ) {
                                ActivityCompat.requestPermissions(this,
                                        new String[]{Manifest.permission.CAMERA},
                                        IMAGE_PICKER_REQUEST_CODE);


                            } else {
                                ActivityCompat.requestPermissions(this,
                                        new String[]{Manifest.permission.CAMERA},
                                        IMAGE_PICKER_REQUEST_CODE);
                            }
                        }
                    }
                } else {
                    showToast("No Internet Connection");
                }


                break;

            case R.id.llColleborateIcon:

                Intent intent = new Intent(NoteDetailActivity.this, ColleborateActivity.class);
                intent.putExtra("Type", "NoteDetail");
                intent.putParcelableArrayListExtra("Colleborators", arryCollaboratUserData);
                intent.putExtra("strCategoryTypeId", oneItemServer.get(0).getfkIntCategoryTypeId());
                intent.putExtra("strCategoryId", oneItemServer.get(0).getfkIntCategoryId());
                intent.putExtra("strSubCategoryId", oneItemServer.get(0).getfkIntSubCategoryId());
                intent.putExtra("intPlannerId", String.valueOf(oneItemServer.get(0).getIntPlannerId()));
                intent.putExtra("createdBy", oneItemServer.get(0).getIntCreateUserId());
                startActivityForResult(intent, 4);

                break;

            case R.id.llRecord:

                if (isNetworkAvailable()) {
                    audStatus = "Update";

                    if (arrAudioDetails.size() >= 3) {//checking if audio is presnt

                        final BottomSheetDialog dialog = new BottomSheetDialog(NoteDetailActivity.this);
                        View sheetView = NoteDetailActivity.this.getLayoutInflater().inflate(R.layout.new_layout, null);
                        dialog.setContentView(sheetView);
                        dialog.setCanceledOnTouchOutside(true);
                        dialog.show();
                        TextView tvTitle = sheetView.findViewById(R.id.tvTitle);
                        tvTitle.setText("Can't upload more than 3 audio");

                        Button btnProceed = sheetView.findViewById(R.id.btnProceed);
                        Button btnCancel = sheetView.findViewById(R.id.btnCancel);
                        btnProceed.setVisibility(View.GONE);
                        btnCancel.setText("Ok");
                        btnProceed.setOnClickListener(view22 -> {

                            String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
                            int res = getApplicationContext().checkCallingOrSelfPermission(permission);

                            if (res == PackageManager.PERMISSION_GRANTED) {
                                requestAudioPermissions();
                            } else {

                                Snackbar snackbar = Snackbar
                                        .make(llBottom, "Allow permission for recording", Snackbar.LENGTH_LONG);
                                snackbar.show();
                                PermissionsManager.get()
                                        .intentToAppSettings(NoteDetailActivity.this);
                            }

                            dialog.dismiss();
                        });
                        btnCancel.setOnClickListener(view2 -> dialog.dismiss());

                    } else {

                        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
                        int res = getApplicationContext().checkCallingOrSelfPermission(permission);

                        if (res == PackageManager.PERMISSION_GRANTED) {
                            requestAudioPermissions();
                        } else {
                            Snackbar snackbar = Snackbar
                                    .make(llBottom, "Allow permission for recording", Snackbar.LENGTH_LONG);
                            snackbar.show();
                            PermissionsManager.get()
                                    .intentToAppSettings(NoteDetailActivity.this);
                        }
                    }
                } else {
                    showToast("No Internet Connection");
                }

                break;

            case R.id.llCheckbox:

                Calendar mcurrentTime = Calendar.getInstance();
                Date date = mcurrentTime.getTime();
                DateFormat gmtFormat = new SimpleDateFormat("MMM dd");//, h:mm a
                DateFormat gmtTimeFormat = new SimpleDateFormat("h:mm a");
                gmtFormat.setTimeZone(istTime);
                gmtTimeFormat.setTimeZone(istTime);

                SimpleDateFormat mdformat = new SimpleDateFormat("h:mm a");
                mdformat.setTimeZone(istTime);
                dateModified = mdformat.format(date);
                llBottom.setVisibility(View.VISIBLE);
                tvEdited.setVisibility(View.VISIBLE);
                tvEdited.setText("Today " + dateModified);

                if (isCheckBox.equals("Checkboxes")) {

///                    UndoalleditText.clear();
                    undoTextsArraylist.clear();
                    redoTextsArraylist.clear();

                    ArrayList<UndoTextsModel> NewundoTextsFinal = new ArrayList<>();
                    ArrayList<UndoTextsModel> undoTextsFinal = new ArrayList<>();

                    if (!alleditTextCheckbox.isEmpty()) {

                        UndoalleditText = alleditTextCheckbox;//issue
                        for (int i = 0; i < UndoalleditText.size(); i++) {
                            UndoTextsModel undoCheckboxes1 = new UndoTextsModel();
                            undoCheckboxes1.setId(String.valueOf(i));
                            undoCheckboxes1.setStrNoteListValue(UndoalleditText.get(i).getText().toString());
                            undoTextsFinal.add(undoCheckboxes1);
                            NewundoTextsFinal.add(i, undoTextsFinal.get(i));
                        }

                        UndoTexts undos1 = new UndoTexts();
                        undos1.setId(String.valueOf(0));
                        undos1.setTitle(tvTitle.getText().toString());
                        undos1.setUndoTextsModel(NewundoTextsFinal);
                        undoTextsArraylist.add(0, undos1);

                    }

                } else {

//                    UndoalleditText.clear();//check it later
                    undoCheckboxesArraylist.clear();
                    redoCheckboxesArraylist.clear();

                    if (!alleditText.isEmpty()) {

                        UndoalleditText = alleditText;

                        ArrayList<UndoCheckboxModel> NewundoCheckboxesFinal = new ArrayList<>();
                        ArrayList<UndoCheckboxModel> undoCheckboxesFinal = new ArrayList<>();
                        for (int i = 0; i < UndoalleditText.size(); i++) {
                            UndoCheckboxModel undoCheckboxes1 = new UndoCheckboxModel();
                            undoCheckboxes1.setId(String.valueOf(i));
                            undoCheckboxes1.setStrNoteListValue(UndoalleditText.get(i).getText().toString());
                            undoCheckboxes1.setStrNoteListFlage(false);
                            undoCheckboxesFinal.add(undoCheckboxes1);
                            NewundoCheckboxesFinal.add(i, undoCheckboxesFinal.get(i));
                        }

                        UndoCheckboxes undos1 = new UndoCheckboxes();
                        undos1.setId(String.valueOf(0));
                        undos1.setTitle(tvTitle.getText().toString());
                        undos1.setUndoCheckboxModel(NewundoCheckboxesFinal);
                        undoCheckboxesArraylist.add(0, undos1);

                    }

                }

                ivUndo.setEnabled(false);
                ivUndo.setColorFilter(colorGrey);
                ivRedo.setColorFilter(colorGrey);
                ivRedo.setEnabled(false);

                convertDatas();
                break;

            case R.id.llGmail:

                if (isNetworkAvailable()) {
                    String list_text = "";
                    for (int i = 0; i < rlCheckbox.getChildCount(); i++) {
                        View child = rlCheckbox.getChildAt(i);

                        if (child instanceof CustomEditText) {
                            String val = ((CustomEditText) child).getText().toString();
                            if (i == 0) {
                                list_text = list_text + val;
                            } else {
                                list_text = list_text + "\n" + val;
                            }
                        } else if (child instanceof RelativeLayout) {
                            String val = ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getText().toString();
                            if (i == 0) {
                                list_text = list_text + val;
                            } else {
                                list_text = list_text + "\n" + val;
                            }
                        }
                    }

                    Intent i = new Intent(Intent.ACTION_SENDTO);
                    i.setType("message/rfc822");
                    i.setData(Uri.parse("mailto:"));
                    i.putExtra(Intent.EXTRA_EMAIL, new String[]{""});
                    i.putExtra(Intent.EXTRA_SUBJECT, "Planner: " + tvTitle.getText().toString());
                    i.putExtra(Intent.EXTRA_TEXT, list_text);

                    try {
                        startActivity(Intent.createChooser(i, "Send mail..."));
                    } catch (ActivityNotFoundException ex) {
                        Toast.makeText(NoteDetailActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    showToast("No Internet Connection");
                }


                break;

            case R.id.ivUndo:

                if (isCheckBox.equals("TextFormat")) { //convert textformat to checkboxes


                    if (undoTextsArraylist.size() == 0) {
                        ivUndo.setColorFilter(colorGrey);
                        ivUndo.setEnabled(false);
                    } else {
                        ivUndo.setColorFilter(colorBlack);
                        ivUndo.setEnabled(true);
                    }

                    int k;
                    if (undoTextsArraylist.size() - 2 == -1) {
                        k = 0;
                    } else {
                        k = undoTextsArraylist.size() - 2;
                    }

                    isActiveText = "Yes";
                    isChanged = "No";
                    tvTitle.setText(undoTextsArraylist.get(k).getTitle());

                    ArrayList<UndoTextsModel> undoTexts = new ArrayList<>(undoTextsArraylist.get(k).getUndoTextsModel());
                    textFieldValue(undoTexts);

                    redoTextsArraylist.add(undoTextsArraylist.get(undoTextsArraylist.size() - 1));
                    undoTextsArraylist.remove(undoTextsArraylist.size() - 1);
                    if (k == 0) {
                        ivUndo.setColorFilter(colorGrey);
                        ivUndo.setEnabled(false);
                    } else {
                        ivUndo.setColorFilter(colorBlack);
                        ivUndo.setEnabled(true);
                    }
                    if (redoTextsArraylist.size() == 0) {
                        ivRedo.setColorFilter(colorGrey);
                        ivRedo.setEnabled(false);
                    } else {
                        ivRedo.setColorFilter(colorBlack);
                        ivRedo.setEnabled(true);
                    }

                } else {
                    isActiveCheckbox = "Yes";
                    int k;
                    if (undoCheckboxesArraylist.size() - 2 == -1) {
                        k = 0;
                    } else {
                        k = undoCheckboxesArraylist.size() - 2;
                    }

                    isChangedCheckbox = "No";
                    tvTitle.setText(undoCheckboxesArraylist.get(k).getTitle());
                    if (k == 0) {
                        ivUndo.setColorFilter(colorGrey);
                        ivUndo.setEnabled(false);
                    } else {
                        ivUndo.setColorFilter(colorBlack);
                        ivUndo.setEnabled(true);
                    }

                    ArrayList<UndoCheckboxModel> undoCheckboxes = new ArrayList<>(undoCheckboxesArraylist.get(k).getUndoCheckboxModel());
                    checkBoxeValue(undoCheckboxes);

                    redoCheckboxesArraylist.add(undoCheckboxesArraylist.get(undoCheckboxesArraylist.size() - 1));
                    undoCheckboxesArraylist.remove(undoCheckboxesArraylist.size() - 1);

                    if (redoCheckboxesArraylist.size() == 0) {
                        ivRedo.setColorFilter(colorGrey);
                        ivRedo.setEnabled(false);
                    } else {
                        ivRedo.setColorFilter(colorBlack);
                        ivRedo.setEnabled(true);
                    }
                }
                break;

            case R.id.ivRedo:

                if (isCheckBox.equals("TextFormat")) { //convert textformat to checkboxes

                    if (redoTextsArraylist.size() == 0) {
                        ivRedo.setColorFilter(colorGrey);
                        ivRedo.setEnabled(false);
                    } else {
                        ivRedo.setColorFilter(colorBlack);
                        ivRedo.setEnabled(true);
                    }

                    int k1 = redoTextsArraylist.size() - 1;
                    tvTitle.setText(redoTextsArraylist.get(k1).getTitle());
                    ArrayList<UndoTextsModel> undoTextsModels = new ArrayList<>(redoTextsArraylist.get(k1).getUndoTextsModel());
                    textFieldValue(undoTextsModels);

                    undoTextsArraylist.add(redoTextsArraylist.get(redoTextsArraylist.size() - 1));
                    redoTextsArraylist.remove(redoTextsArraylist.size() - 1);

                    if (k1 == 0) {
                        ivRedo.setColorFilter(colorGrey);
                        ivRedo.setEnabled(false);
                    }

                    if (undoTextsArraylist.size() != 0) {
                        ivUndo.setColorFilter(colorBlack);
                        ivUndo.setEnabled(true);
                    }

                } else {

                    if (redoCheckboxesArraylist.size() == 0) {
                        ivRedo.setColorFilter(colorGrey);
                        ivRedo.setEnabled(false);
                    } else {
                        ivRedo.setColorFilter(colorBlack);
                        ivRedo.setEnabled(true);
                    }

                    int k1 = redoCheckboxesArraylist.size() - 1;
                    tvTitle.setText(redoCheckboxesArraylist.get(k1).getTitle());

                    ArrayList<UndoCheckboxModel> undoCheckboxes = new ArrayList<>(redoCheckboxesArraylist.get(k1).getUndoCheckboxModel());
                    checkBoxeValue(undoCheckboxes);

                    undoCheckboxesArraylist.add(redoCheckboxesArraylist.get(redoCheckboxesArraylist.size() - 1));
                    redoCheckboxesArraylist.remove(redoCheckboxesArraylist.size() - 1);

                    if (k1 == 0) {
                        ivRedo.setColorFilter(colorGrey);
                        ivRedo.setEnabled(false);
                    }

                    if (undoCheckboxesArraylist.size() != 0) {
                        ivUndo.setColorFilter(colorBlack);
                        ivUndo.setEnabled(true);
                    }
                }

                break;
            case R.id.ivImageOne:
                ArrayList<ImgDetails> listOne = new ArrayList();
                listOne.addAll(imgDetailss);

                Intent img = new Intent(NoteDetailActivity.this, ImageShowActivity.class);
                img.putExtra("type", "Detail");
                img.putExtra("img_pos", 0);

                img.putExtra("strImageId", imgDetailss.get(0).getIntImageId());
                img.putExtra("intPlannerId", plannerID);
                img.putExtra("strCategoryTypeId", oneItemServer.get(0).getfkIntCategoryTypeId());
                img.putExtra("strCategoryId", oneItemServer.get(0).getfkIntCategoryId());
                img.putExtra("strSubCategoryId", oneItemServer.get(0).getfkIntSubCategoryId());

                img.putParcelableArrayListExtra("img_list", listOne);
                startActivityForResult(img, 1001);
                break;
            case R.id.ivImageTwo:
                ArrayList<ImgDetails> list = new ArrayList();
                list.addAll(imgDetailss);

                Intent img2 = new Intent(NoteDetailActivity.this, ImageShowActivity.class);
                img2.putExtra("type", "Detail");
                img2.putExtra("img_pos", 1);

                img2.putExtra("strImageId", imgDetailss.get(1).getIntImageId());
                img2.putExtra("intPlannerId", plannerID);
                img2.putExtra("strCategoryTypeId", oneItemServer.get(0).getfkIntCategoryTypeId());
                img2.putExtra("strCategoryId", oneItemServer.get(0).getfkIntCategoryId());
                img2.putExtra("strSubCategoryId", oneItemServer.get(0).getfkIntSubCategoryId());

                img2.putParcelableArrayListExtra("img_list", list);
                startActivityForResult(img2, 1001);
                break;
            case R.id.ivImageThree:
                ArrayList<ImgDetails> list3 = new ArrayList();
                list3.addAll(imgDetailss);

                Intent img3 = new Intent(NoteDetailActivity.this, ImageShowActivity.class);
                img3.putExtra("type", "Detail");
                img3.putExtra("img_pos", 2);

                img3.putExtra("strImageId", imgDetailss.get(2).getIntImageId());
                img3.putExtra("intPlannerId", plannerID);
                img3.putExtra("strCategoryTypeId", oneItemServer.get(0).getfkIntCategoryTypeId());
                img3.putExtra("strCategoryId", oneItemServer.get(0).getfkIntCategoryId());
                img3.putExtra("strSubCategoryId", oneItemServer.get(0).getfkIntSubCategoryId());

                img3.putParcelableArrayListExtra("img_list", list3);
                startActivityForResult(img3, 1001);
                break;
        }
    }

    public void updateData() {


        if (isNetworkAvailable()) {
            String val = String.valueOf(toSingleString);
            String trimmedString = val.trim();

            RequestParams params = new RequestParams();
            params.put("strTitle", tvTitle.getText().toString());
            params.put("strColor", "#ffffff");
            params.put("strCreateUserId", oneItemServer.get(0).getIntCreateUserId());//initial creater of note
            params.put("intPlannerId", String.valueOf(oneItemServer.get(0).getIntPlannerId()));//)
            params.put("blnPinNote", isPinned);//)

//        params.put("intProjectId", mUserDetails.getArryselectedProjectAllItems().get(0).getId());//)
//        params.put("intOrganisationId", mUserDetails.getArryselectedOrganisationAllItems().get(0).getId());//)

            params.put("strModifiedUserId", strUserId);//intModifiedUserId
            params.put("datCreateDateAndTime", oneItemServer.get(0).getDatCreateDateAndTime());//initial note created date and time

            params.put("strValue", trimmedString);
//            params.put("arryCollaboratUserId", collUser.toString());

            params.put("strCategoryTypeId", oneItemServer.get(0).getfkIntCategoryTypeId());
            params.put("strCategoryId", oneItemServer.get(0).getfkIntCategoryId());
            params.put("strSubCategoryId", oneItemServer.get(0).getfkIntSubCategoryId());

            JSONArray array = new JSONArray();
            for (TodoListItem todo : todoCheckboxItems) {
                try {
                    JSONObject object = new JSONObject();
                    object.put("id", todo.getId());
                    object.put("strNoteListValue", todo.getStrNoteListValue());
                    object.put("strNoteListFlage", todo.getStrNoteListFlage());
                    array.put(object);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            params.put("arryObjstrTodoList", array);

            try {
                JSONObject object = new JSONObject();
                object.put("intId", objRemainderOld.get_id());
                object.put("strRemainderDate", objRemainderOld.getStrRemainderDate());
                object.put("strRemainderTime", objRemainderOld.getStrRemainderTime());
                object.put("strType", objRemainderOld.getStrType());
                object.put("blnFlage", objRemainderOld.getBlnFlage());
                params.put("objRemainderOld", object);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                JSONObject object = new JSONObject();
                object.put("intId", objRemainder.get_id());
                object.put("strRemainderDate", objRemainder.getStrRemainderDate());
                object.put("strRemainderTime", objRemainder.getStrRemainderTime());
                object.put("strType", objRemainder.getStrType());
                object.put("blnFlage", objRemainder.getBlnFlage());
                params.put("objRemainder", object);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            ApiCallWithToken(urlSaveAllNotes, ApiCall.UPDATEPLANNER, params);

        } else {
            showToast("No Internet Connection");
        }

    }

    public void notePin() {
        if (isNetworkAvailable()) {
            RequestParams params = new RequestParams();
            params.put("blnPinNote", !isPinned);
            params.put("intPlannerId", oneItemServer.get(0).getIntPlannerId());
            params.put("strModifiedUserId", strUserId);
            params.put("strCategoryTypeId", oneItemServer.get(0).getfkIntCategoryTypeId());
            params.put("strCategoryId", oneItemServer.get(0).getfkIntCategoryId());
            params.put("strSubCategoryId", oneItemServer.get(0).getfkIntSubCategoryId());
            ApiCallWithToken(urlNotePinned, ApiCall.PINNED, params);

        } else {
            showToast("No Internet Connection");
        }
    }

    public void deletePlanner(final int plannerId) {

        if (isNetworkAvailable()) {
            RequestParams params = new RequestParams();
            params.put("intPlannerId", String.valueOf(plannerId));
            params.put("strModifiedUserId", strUserId);
            ApiCallWithToken(url_noteDelete, ApiCall.DELETEPLANNER, params);
        } else {
            showToast("No Internet Connection");
        }

    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void OnResponce(JSONObject data, ApiCall type) {
        JSONArray array = null;
        try {
            if (type == ApiCall.COLLABORATEUSERS) {
                hideProgressDialog(NoteDetailActivity.this);
                array = data.getJSONArray("data");
                if (data.length() != 0) {
                    Gson gson = new Gson();
                    mRealm.beginTransaction();
                    final ArrayList<ColleborateDatum> colleborators = new ArrayList<>();

                    for (int i = 0; i < array.length(); i++) {
                        try {
                            JSONObject object = array.getJSONObject(i);
                            ColleborateDatum users = gson.fromJson(object.toString(), ColleborateDatum.class);
                            colleborators.add(users);


                            mRealm.copyToRealmOrUpdate(users);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    collUsers.clear();
                    for (int j = 0; j < colleborators.size(); j++) {
                        collUsers.add(colleborators.get(j).getId());
                    }

                    mRealm.commitTransaction();
                    if (colleborators.size() != 0) {

                        int color = Color.parseColor("#FFFFFF");
                        ivPin.setColorFilter(color);
                        isPinned = false;
                        mRealm.beginTransaction();
                        oneItemServer.get(0).setBlnPinNote(false);
                        mRealm.copyToRealmOrUpdate(oneItemServer.get(0));
                        mRealm.commitTransaction();

                        arryCollaboratUserData.clear();
                        arryCollaboratUserData.addAll(colleborators);

                        setColleborateUsers(arryCollaboratUserData);
                    }

                }
            } else if (type == ApiCall.IMAGE) {
                hideProgressDialog(NoteDetailActivity.this);

                array = data.getJSONArray("data");
                Log.e("ImageSave", array.length() + "...." + data + "...");

                if (array.length() != 0) {
                    mRealm.beginTransaction();
                    imgDetailss.clear();
                    mRealm.commitTransaction();
                    Gson gson = new Gson();
                    for (int i = 0; i < array.length(); i++) {
                        try {
                            JSONObject object = array.getJSONObject(i);
                            Item img = gson.fromJson(object.toString(), Item.class);

                            mRealm.beginTransaction();
                            mRealm.copyToRealmOrUpdate(img);
                            imgDetailss.addAll(img.getImgNoteList());
                            mRealm.commitTransaction();

                            plannerID = String.valueOf(img.getIntPlannerId());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        showToast("Successfully updated..");
                    }

                    if (fileProfile.length() != 0) {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeFile(fileProfile.getAbsolutePath(), options);
                        SaveImageToFolder(bitmap);
                    }

                }

                if (fileArrayList.size() != 0) {
                    for (int j = 0; j < fileArrayList.size(); j++) {
                        showToast(j + "......" + fileArrayList.size());
                        saveImage(fileArrayList.get(j).getFile(), fileArrayList.get(j).getHeight(), fileArrayList.get(j).getWeight());
                        fileArrayList.remove(j);
                    }
                }


                setImages(imgDetailss);

            } else if (type == ApiCall.AUDIO) {

                hideProgressDialog(NoteDetailActivity.this);
                array = data.getJSONArray("data");

                mRealm.beginTransaction();
                arrAudioDetails.clear();
                audDeletUpdated.clear();
                mRealm.commitTransaction();
                Log.e("Audio_data", data + "");
                if (array.length() != 0) {
                    Gson gson = new Gson();

                    for (int i = 0; i < array.length(); i++) {
                        try {
                            JSONObject object = array.getJSONObject(i);
                            Item img = gson.fromJson(object.toString(), Item.class);
                            Log.e("Audiosss", img.getFileAudioList().size() + "..." + arrAudioDetails.size() + "..." + audDeletUpdated.size() + "..." + img.getFileAudioList());

                            mRealm.beginTransaction();
                            arrAudioDetails.addAll(img.getFileAudioList());
                            audDeletUpdated.addAll(img.getFileAudioList());
                            mRealm.commitTransaction();
                            Log.e("Audiosss_2_//", img.getFileAudioList().size() + "..." + arrAudioDetails.size() + "..." + audDeletUpdated.size() + "..." + img.getFileAudioList());

                            plannerID = String.valueOf(img.getIntPlannerId());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    showToast("Successfully uploaded....." + audDeletUpdated.size() + "..." + arrAudioDetails.size());
                    setAudioDatas();

                }
            } else if (type == ApiCall.UPDATEPLANNER) {
                if (data.length() != 0) {
                    noteUpdatedList.clear();
                    array = data.getJSONArray("data");
                    if (array.length() != 0) {
                        try {
                            JSONObject object = array.getJSONObject(0);
                            int planer_ID = Integer.parseInt(object.getString("intPlannerId"));
                            mRealm.beginTransaction();
                            RealmResults<Item> itemsRealmResults = mRealm.where(Item.class).equalTo("intPlannerId", planer_ID).findAll();
                            mRealm.copyToRealmOrUpdate(itemsRealmResults.get(0));
                            mRealm.commitTransaction();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
            } else if (type == ApiCall.DELETEPLANNER) {
                if (data.length() != 0) {
                    array = data.getJSONArray("data");
                    if (array.length() != 0) {
                        try {
                            JSONObject object = array.getJSONObject(0);
                            int planer_ID = Integer.parseInt(object.getString("intPlannerId"));
                            mRealm.beginTransaction();
                            RealmResults<Item> itemsRealmResults = mRealm.where(Item.class).equalTo("intPlannerId", planer_ID).findAll();
                            itemsRealmResults.deleteAllFromRealm();
                            mRealm.commitTransaction();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    showToast("Successfully removed");
                }
            } else if (type == ApiCall.DELETEAUDIO) {

                Log.e("deleteAduio_data", data + "");
                hideProgressDialog(NoteDetailActivity.this);

                array = data.getJSONArray("data");

                mRealm.beginTransaction();
                arrAudioDetails.clear();
                mRealm.commitTransaction();

                if (array.length() != 0) {
                    showToast("Successfully removed.." + audDeletUpdated.size() + "..." + arrAudioDetails.size());
                    Gson gson = new Gson();

                    for (int i = 0; i < array.length(); i++) {
                        try {
                            JSONObject object = array.getJSONObject(i);
                            Item img = gson.fromJson(object.toString(), Item.class);

                            mRealm.beginTransaction();
                            arrAudioDetails.addAll(img.getFileAudioList());
                            mRealm.commitTransaction();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    if (isAudioExpanded) {
//                        tvMoreAudio.setVisibility(View.GONE);
                    } else {
                        if (arrAudioDetails.size() == 1) tvMoreAudio.setVisibility(View.GONE);
                        else if (arrAudioDetails.size() > 1) {
                            tvMoreAudio.setVisibility(View.VISIBLE);
                            tvMoreAudio.setText("+ (" + (arrAudioDetails.size() - 1) + " )");
                        }
                    }

                }

            } else if (type == ApiCall.PINNED) {
                array = data.getJSONArray("data");

                if (array.length() != 0) {
                    try {
                        JSONObject object = array.getJSONObject(0);
                        int planer_ID = Integer.parseInt(object.getString("intPlannerId"));
                        Boolean isPin = Boolean.valueOf(object.getString("blnPinNote"));
                        mRealm.beginTransaction();
                        RealmResults<Item> itemsRealmResults = mRealm.where(Item.class).equalTo("intPlannerId", planer_ID).findAll();

                        Item item = itemsRealmResults.get(0);
                        isPinned = isPin;
                        if (isPin) {
                            int color = Color.parseColor("#F1C40F"); //The color u want
                            ivPin.setColorFilter(color);
                        } else {
                            int color = Color.parseColor("#FFFFFF"); //The color u want
                            ivPin.setColorFilter(color);
                        }

                        item.setBlnPinNote(isPin);
                        mRealm.copyToRealmOrUpdate(item);
                        mRealm.commitTransaction();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else if (type == ApiCall.SINGLEPLANNER) {
                showToast("SINGLEPLANNER");
                Gson gson = new Gson();
                hideProgressDialog(NoteDetailActivity.this);

                if (data.length() != 0) {
                    array = data.getJSONArray("data");
                    oneItemServer.clear();

                    mRealm.beginTransaction();

                    for (int i = 0; i < array.length(); i++) {
                        try {
                            JSONObject object = array.getJSONObject(i);
                            Item item = gson.fromJson(object.toString(), Item.class);

                            oneItemServer.add(item);
                            mRealm.copyToRealmOrUpdate(item);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    mRealm.commitTransaction();
//                    oneItemServer.addAll(itemsRealmResults);
                }
                dataServer(oneItemServer.get(0));

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        super.OnResponce(data, type);
    }


    @Override
    public void OnError(JSONObject object, ApiCall type) {

        if (type == ApiCall.COLLABORATEUSERS) {
            hideProgressDialog(NoteDetailActivity.this);
            try {
                String message = object.getString("message");
                if (message.equals("No data fount")) {
                    collUsers.clear();
                    arryCollaboratUserData.clear();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        super.OnError(object, type);
    }

    public void setColleborateUsers(ArrayList<ColleborateDatum> colleborators) {

        if (isPinned) {
            int color = Color.parseColor("#FFFFFF"); //The color u want
            ivPin.setColorFilter(color);
            isPinned = false;
            isCheckboxChanged = true;
            notePin();
        }

        if (colleborators.size() > 0) {
            llColleborate.setVisibility(View.VISIBLE);
            int totCount = colleborators.size();

            final String lastName, firstName, fullName;
            RelativeLayout linearLayout = (RelativeLayout) View.inflate(NoteDetailActivity.this, R.layout.list_item_colleborate_list_users, null);

            TextView text = linearLayout.findViewById(R.id.text);
            LinearLayout llShare = linearLayout.findViewById(R.id.llShare);
            ImageView cvImage1 = linearLayout.findViewById(R.id.cvImage1);

            String name = colleborators.get(0).getItemName();
            final String[] split = name.split(":");
            firstName = split[0];
            if (split.length == 2) {
                lastName = split[1];
                fullName = firstName + " " + lastName;
            } else {
                fullName = firstName;
            }

            if (totCount == 1) {
                text.setText(fullName);
                cvImage1.setVisibility(View.VISIBLE);
                Glide.with(NoteDetailActivity.this)
                        .load(R.drawable.ic_share)
                        .dontAnimate()
                        .into(cvImage1);
            } else {

                text.setText(fullName + " +others");
                cvImage1.setVisibility(View.VISIBLE);
                Glide.with(NoteDetailActivity.this)
                        .load(R.drawable.ic_share)
                        .dontAnimate()
                        .into(cvImage1);
            }


            llShare.setOnClickListener(view -> {
                Intent intent = new Intent(NoteDetailActivity.this, ColleborateActivity.class);
                intent.putExtra("Type", "NoteDetail");
                intent.putParcelableArrayListExtra("Colleborators", arryCollaboratUserData);
                intent.putExtra("strCategoryTypeId", oneItemServer.get(0).getfkIntCategoryTypeId());
                intent.putExtra("strCategoryId", oneItemServer.get(0).getfkIntCategoryId());
                intent.putExtra("strSubCategoryId", oneItemServer.get(0).getfkIntSubCategoryId());
                intent.putExtra("intPlannerId", plannerID);
                intent.putExtra("createdBy", oneItemServer.get(0).getIntCreateUserId());
                startActivityForResult(intent, 4);
                if (isNetworkAvailable()) {
                    linearLayout.removeAllViews();
                }
            });

            llColleborate.addView(linearLayout);
        }
    }


    private void loopViews(ViewGroup parent) {

        for (int i = 0; i < parent.getChildCount(); i++) {
            View child = parent.getChildAt(i);
            if (child instanceof CustomEditText) {
                int finalI = i;

                child.setOnKeyListener(new View.OnKeyListener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {

                        if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                            // Perform action on key press

                            String val = ((CustomEditText) child).getText().toString();
                            String newVal = val.substring(((CustomEditText) child).getSelectionStart());
                            ((CustomEditText) child).setText(val.substring(0, ((CustomEditText) child).getSelectionStart()));
                            ((CustomEditText) child).setSelection(((CustomEditText) child).getText().length());
                            addEditTextEnterKeyPressed(finalI + 1, newVal);

                            return true;
                        } else if (keyCode == KeyEvent.KEYCODE_DEL) {

                            String modifyText = null;
                            modifyText = ((CustomEditText) child).getText().toString();
                            int curPos = ((CustomEditText) child).getSelectionStart();

                            if (curPos == 0) {
                                if (finalI == 0) {
                                    parent.getChildAt(0).requestFocus();
                                } else {
                                    rlCheckbox.removeView(parent.getChildAt(finalI));
                                    parent.getChildAt(finalI - 1).requestFocus();
                                    alleditText.remove(finalI);
                                    String newTextVal = ((CustomEditText) parent.getChildAt(finalI - 1)).getText().toString();
                                    modifyText = newTextVal + modifyText;

                                    ((CustomEditText) parent.getChildAt(finalI - 1)).setText(modifyText);
                                    ((CustomEditText) parent.getChildAt(finalI - 1)).setSelection(modifyText.length());

                                }
                            }
                            loopViews(rlCheckbox);
                        }
                        return false;
                    }
                });
            }
        }
    }

    private void loopViewsCheckbox(ViewGroup parent) {

        int k = 9;
        for (int i = 0; i < parent.getChildCount(); i++) {

            View child = parent.getChildAt(i);
            if (child instanceof RelativeLayout) {

                int finalI = i;
                CustomEditText editText = (CustomEditText) ((RelativeLayout) child).getChildAt(1);
//                editText.setImeActionLabel("Enter", KeyEvent.KEYCODE_ENTER);

//                int curPos = ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getSelectionStart();
//                int m = editText.getImeActionId();
//                editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//                    @Override
//                    public void onFocusChange(View v, boolean hasFocus) {
//                        editText.setImeActionLabel("Enter", 1004);
//                        Log.e("editText_onFocusChan", m + "..." + "...." + curPos + "..." + editText.getId());
//                    }
//                });

//                Log.e("editText", m + "..." + "...." + curPos + "..." + editText.getId());

//                if (i == parent.getChildCount()) {
//                    k = 0;
//                    editText.setImeActionLabel("Enter", 1004);
//                }

//                int finalK = k;
//                editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//                    @Override
//                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                        boolean handled = false;
//                        Toast.makeText(getApplicationContext(), finalK + "..You entered....." + actionId + "....." + event, Toast.LENGTH_LONG).show();
//                        if ((actionId == 1004) || (actionId == 5)) {//5
//                            handled = true;
//                            String val = ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getText().toString();
//                            String newVal = val.substring((((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getSelectionStart()), val.length());
//                            ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).setText(val.substring(0, (((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getSelectionStart())));
//                            ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).setSelection((((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getText().length()));
//                            addEnterKeyPressed(finalI + 1, newVal);
//                        }
//                        return handled;
//                    }
//                });
//
//                editText.setOnKeyListene((v, keyCode, event) -> {
//                    Toast.makeText(getApplicationContext(), "setOnKeyListene...22222" + event+"..."+keyCode, Toast.LENGTH_LONG).show();
//                    if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
//
//                        String val = ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getText().toString();
//                        String newVal = val.substring((((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getSelectionStart()), val.length());
//                        ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).setText(val.substring(0, (((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getSelectionStart())));
//                        ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).setSelection((((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getText().length()));
//                        addEnterKeyPressed(finalI + 1, newVal);
//                        return true;
//
//                    } else if (keyCode == KeyEvent.KEYCODE_DEL) {
//
//                        String modifyText = null;
//                        modifyText = ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getText().toString();
////                        int curPos = ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getSelectionStart();
//
//                        if (curPos == 0) {
//                            if (finalI == 0) {
//                                parent.getChildAt(0).requestFocus();
//                            } else {
//                                rlCheckbox.removeView(parent.getChildAt(finalI));
//                                parent.getChildAt(finalI - 1).requestFocus();
//                                alleditTextCheckbox.remove(finalI);
//                                String newTextVal = ((CustomEditText) ((RelativeLayout) parent.getChildAt(finalI - 1)).getChildAt(1)).getText().toString();
//                                modifyText = newTextVal + modifyText;
//                                ((CustomEditText) ((RelativeLayout) parent.getChildAt(finalI - 1)).getChildAt(1)).setText(modifyText);
//                                ((CustomEditText) ((RelativeLayout) parent.getChildAt(finalI - 1)).getChildAt(1)).setSelection(modifyText.length());
//                            }
//                        }
//
//                        loopViewsCheckbox(rlCheckbox);
//
//                    }
//                    return false;
//                });

//                editText.addTextChangedListener(new TextWatcher() {
//
//                    @Override
//                    public void onTextChanged(CharSequence s, int start, int before, int count) {
//                        if (editText.getText().length() == 0){
//                            Toast.makeText(getApplicationContext(), "onTextChanged..1111." + s + "..."+start+"..."+before+"..."+count , Toast.LENGTH_LONG).show();
//                            String modifyText = null;
//                            modifyText = ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getText().toString();
//                            int curPos = ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getSelectionStart();
//
//                            if (curPos == 0) {
//                                if (finalI == 0) {
//                                    parent.getChildAt(0).requestFocus();
//                                } else {
//                                    rlCheckbox.removeView(parent.getChildAt(finalI));
//                                    parent.getChildAt(finalI - 1).requestFocus();
//                                    alleditTextCheckbox.remove(finalI);
//                                    String newTextVal = ((CustomEditText) ((RelativeLayout) parent.getChildAt(finalI - 1)).getChildAt(1)).getText().toString();
//                                    modifyText = newTextVal + modifyText;
//
//                                    ((CustomEditText) ((RelativeLayout) parent.getChildAt(finalI - 1)).getChildAt(1)).setText(modifyText);
//                                    ((CustomEditText) ((RelativeLayout) parent.getChildAt(finalI - 1)).getChildAt(1)).setSelection(modifyText.length());
//                                }
//                            }
//
//                            loopViewsCheckbox(rlCheckbox);
//                        }
//
//                    }
//
//                    @Override
//                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                    }
//
//                    @Override
//                    public void afterTextChanged(Editable s) {
//
//                    }
//                });

////new////

//                editText.setOnKeyListene(new View.OnKeyListener() {
//                    @Override
//                    public boolean onKey(View v, int keyCode, KeyEvent event) {
//
//                        if (event.getUnicodeChar() ==
//                                (int) EditableAccomodatingLatinIMETypeNullIssues.ONE_UNPROCESSED_CHARACTER.charAt(0)) {
//                            Toast.makeText(getApplicationContext(), "else...ACTION_DOWN" + keyCode + "..." + event, Toast.LENGTH_LONG).show();
//                            //We are ignoring this character, and we want everyone else to ignore it, too, so
//                            // we return true indicating that we have handled it (by ignoring it).
//                            return true;
//                        }
//                        if ((event.getAction() == KeyEvent.ACTION_UP)&& (keyCode == KeyEvent.KEYCODE_DEL)) {
//                            Toast.makeText(getApplicationContext(), "if...ACTION_UP" + keyCode + "..." + event, Toast.LENGTH_LONG).show();
//                            //We only look at ACTION_DOWN in this code, assuming that ACTION_UP is redundant.
//                            // If not, adjust accordingly.
//                            return false;
//                        }
//                        //Now, just do your event handling as usual...
//                        if (keyCode == KeyEvent.KEYCODE_ENTER) {
//
//                            Toast.makeText(getApplicationContext(), "KEYCODE_ENTER" + keyCode + "..." + event, Toast.LENGTH_LONG).show();
//                            //Trap the Done key and close the keyboard if it is pressed (if that's what you want to do)
//                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//                            imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
//
//                            String val = ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getText().toString();
//                            String newVal = val.substring((((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getSelectionStart()), val.length());
//                            ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).setText(val.substring(0, (((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getSelectionStart())));
//                            ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).setSelection((((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getText().length()));
//                            addEnterKeyPressed(finalI + 1, newVal);
//                            return true;
//                        } else if (keyCode == KeyEvent.KEYCODE_DEL) {
//
//                            //Backspace key processing goes here...
//                            String modifyText = null;
//                            modifyText = ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getText().toString();
//                            int curPos = ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getSelectionStart();
//
//                            if (curPos == 0) {
//                                Toast.makeText(getApplicationContext(), "if...KEYCODE_DEL" + keyCode + "..." + event, Toast.LENGTH_LONG).show();
//                                if (finalI == 0) {
//                                    parent.getChildAt(0).requestFocus();
//                                } else {
//                                    rlCheckbox.removeView(parent.getChildAt(finalI));
//                                    parent.getChildAt(finalI - 1).requestFocus();
//                                    alleditTextCheckbox.remove(finalI);
//                                    String newTextVal = ((CustomEditText) ((RelativeLayout) parent.getChildAt(finalI - 1)).getChildAt(1)).getText().toString();
//                                    modifyText = newTextVal + modifyText;
//                                    ((CustomEditText) ((RelativeLayout) parent.getChildAt(finalI - 1)).getChildAt(1)).setText(modifyText);
//                                    ((CustomEditText) ((RelativeLayout) parent.getChildAt(finalI - 1)).getChildAt(1)).setSelection(modifyText.length());
//                                }
//                            }
////                            else {
////                                Toast.makeText(getApplicationContext(), "else...KEYCODE_DEL" + keyCode + "..." + event, Toast.LENGTH_LONG).show();
////                                String val = editText.getText().toString();
////                                Log.e("curPos", val.substring(0, curPos) + "..." + val + "...");
////
////                                Log.e("curPos_New", charRemoveAt(val, curPos) + "..." + val + "...");
////                                ((CustomEditText) ((RelativeLayout) parent.getChildAt(finalI)).getChildAt(1)).setText(charRemoveAt(val, curPos));
////                                ((CustomEditText) ((RelativeLayout) parent.getChildAt(finalI)).getChildAt(1)).setSelection(2);
////                            }
//                            loopViewsCheckbox(rlCheckbox);
//                            return true;
//                        } else {
//                            Toast.makeText(getApplicationContext(), "KEYCODE_BACK" + keyCode + "..." + event, Toast.LENGTH_LONG).show();
//                        }
//
////                        if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_DEL)) {
////                            Toast.makeText(getApplicationContext(), "setOnKeyListene..del." + keyCode + "..." + event, Toast.LENGTH_LONG).show();
////                            String modifyText = null;
////                            modifyText = ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getText().toString();
////                            int curPos = ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getSelectionStart();
////
////                            if (curPos == 0) {
////                                if (finalI == 0) {
////                                    parent.getChildAt(0).requestFocus();
////                                } else {
////                                    rlCheckbox.removeView(parent.getChildAt(finalI));
////                                    parent.getChildAt(finalI - 1).requestFocus();
////                                    alleditTextCheckbox.remove(finalI);
////                                    String newTextVal = ((CustomEditText) ((RelativeLayout) parent.getChildAt(finalI - 1)).getChildAt(1)).getText().toString();
////                                    modifyText = newTextVal + modifyText;
////
////                                    ((CustomEditText) ((RelativeLayout) parent.getChildAt(finalI - 1)).getChildAt(1)).setText(modifyText);
////                                    ((CustomEditText) ((RelativeLayout) parent.getChildAt(finalI - 1)).getChildAt(1)).setSelection(modifyText.length());
////                                }
////                            }
////
////                            loopViewsCheckbox(rlCheckbox);
////                        } else if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
////                            Toast.makeText(getApplicationContext(), "setOnKeyListene..enter." + keyCode + "..." + event, Toast.LENGTH_LONG).show();
////                            String val = ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getText().toString();
////                            String newVal = val.substring((((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getSelectionStart()), val.length());
////                            ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).setText(val.substring(0, (((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getSelectionStart())));
////                            ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).setSelection((((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getText().length()));
////                            addEnterKeyPressed(finalI + 1, newVal);
////                            return true;
////
////                        }
//                        return false;
//                    }
//                });
////new////

                ///orgnl/////
                ((RelativeLayout) child).getChildAt(1).setOnKeyListener((v, keyCode, event) -> {
//                    Toast.makeText(getApplicationContext(), "setOnKeyListene...22222" + event + "..." + keyCode, Toast.LENGTH_LONG).show();
                    if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {

                        String val = ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getText().toString();
                        String newVal = val.substring((((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getSelectionStart()));
                        ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).setText(val.substring(0, (((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getSelectionStart())));
                        ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).setSelection((((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getText().length()));
                        addEnterKeyPressed(finalI + 1, newVal);
                        return true;

                    } else if (keyCode == KeyEvent.KEYCODE_DEL) {

                        String modifyText = null;
                        modifyText = ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getText().toString();
                        int curPos = ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getSelectionStart();

                        if (curPos == 0) {
                            if (finalI == 0) {
                                parent.getChildAt(0).requestFocus();
                            } else {
                                rlCheckbox.removeView(parent.getChildAt(finalI));
                                parent.getChildAt(finalI - 1).requestFocus();
                                alleditTextCheckbox.remove(finalI);
                                String newTextVal = ((CustomEditText) ((RelativeLayout) parent.getChildAt(finalI - 1)).getChildAt(1)).getText().toString();
                                modifyText = newTextVal + modifyText;

                                ((CustomEditText) ((RelativeLayout) parent.getChildAt(finalI - 1)).getChildAt(1)).setText(modifyText);
                                ((CustomEditText) ((RelativeLayout) parent.getChildAt(finalI - 1)).getChildAt(1)).setSelection(modifyText.length());

                            }
                        }

                        loopViewsCheckbox(rlCheckbox);

                    } else {

                    }
                    return false;
                });
////orgnl////
            }
        }
    }

    @Override
    public void onBackPressed() {

        if (activityType.equals("activity")) {

            if (isAudioPlaying) {
                if (mediaPlayerOne.isPlaying()) {
                    mediaPlayerTwo.stop();
                }
                if (mediaPlayerTwo.isPlaying()) {
                    mediaPlayerTwo.stop();
                }
                if (mediaPlayerThree.isPlaying()) {
                    mediaPlayerTwo.stop();
                }
            }

            toSingleStringList.clear();
            todoCheckboxItems.clear();

            if (!alleditText.isEmpty()) {
                for (int j = 0; j < alleditText.size(); j++) {
                    toSingleStringList.add(alleditText.get(j).getText().toString());
                }
            }

            if (!alleditTextCheckbox.isEmpty()) {

                for (int j = 0; j < alleditTextCheckbox.size(); j++) {
                    TodoListItem todoListItem = new TodoListItem();
                    String isChk = String.valueOf(alleditTextCheckbox.get(j).getTag());
                    if (isChk.equals("true")) {
                        todoListItem.setStrNoteListFlage(true);
                    } else {
                        todoListItem.setStrNoteListFlage(false);
                    }
                    todoListItem.setStrNoteListValue(alleditTextCheckbox.get(j).getText().toString());

                    todoCheckboxItems.add(todoListItem);
                }
            }

            if (toSingleStringList.size() != 0) {

                toSingleString.delete(0, toSingleString.length());
                for (String s : toSingleStringList) {
                    if (toSingleStringList.size() == 1) {
                        if (s.equals("")) {
                        } else if (s != null) {
                            toSingleString.append(s).append('\n');
                        }
                    } else if (s != null) {
                        toSingleString.append(s).append('\n');
                    }
                }
            } else {
                toSingleString = new StringBuffer(0);
                toSingleString.delete(0, toSingleString.length());
                toSingleString.append("");
            }

            String title = tvTitle.getText().toString();
            String val = toSingleString.toString();
            int k = 0;
            for (int i = 0; i < todoCheckboxItems.size(); i++) {
                if (todoCheckboxItems.get(i).getStrNoteListValue().trim().length() == 0) {
                    k++;
                }
            }

            if ((title.trim().length() == 0) && (val.trim().length() == 0) && (collUsers.size() == 0) && (todoCheckboxItems.size() == k) && (isNewImage.equals("no")) && (isNewAudio.equals("no"))) {
                deletePlanner(oneItemServer.get(0).getIntPlannerId());
            } else {
                typeItem = "update";
                if ((title.length() != 0) || (toSingleString.length() != 0) || (todoCheckboxItems.size() != 0) || (collUsers.size() != 0)) {

                    if ((isCheckboxChanged)) {//check if strvalue or todolist item is changed or not
                        updateData();
                    }
                }
            }

            ivAdd.setVisibility(View.GONE);
            llAudioOne.setVisibility(View.GONE);
            llAudioTwo.setVisibility(View.GONE);
            llAudioThree.setVisibility(View.GONE);
            llFileUpload.setVisibility(View.GONE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setExitTransition(null);
            }
            llColleborate.setVisibility(View.GONE);
            llColleborate.removeAllViews();
            llColleborate.setVisibility(View.GONE);
            tvTitle.setVisibility(View.GONE);
            rlCheckbox.setVisibility(View.GONE);
            toolbar.setVisibility(View.GONE);
            supportFinishAfterTransition();
        } else {

        }
        super.onBackPressed();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void finish() {

        if (activityType.equals("activity")) {
            Boolean pinChanged = false;

            if (isAudioPlaying) {
                if (mediaPlayerOne.isPlaying()) {
                    mediaPlayerTwo.stop();
                }
                if (mediaPlayerTwo.isPlaying()) {
                    mediaPlayerTwo.stop();
                }
                if (mediaPlayerThree.isPlaying()) {
                    mediaPlayerTwo.stop();
                }
            }

            if (isNetworkAvailable()) {

                if ((isCheckboxChanged)) {
                    Calendar mcurrentTime = Calendar.getInstance();
                    Date date = mcurrentTime.getTime();
                    String pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";//"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
                    SimpleDateFormat gmtFormat = new SimpleDateFormat(pattern);
                    gmtFormat.setTimeZone(istTime);
                    dateModified = gmtFormat.format(date);
                } else {
                    String valDate = oneItemServer.get(0).getDatModifiedDateAndTime();
                    if (!TextUtils.isEmpty(valDate)) {
                        dateModified = oneItemServer.get(0).getDatModifiedDateAndTime();
                    } else {
                        dateModified = oneItemServer.get(0).getDatCreateDateAndTime();
                    }
                }

                if (!oneItemServer.get(0).getBlnPinNote().equals(isPinned)) {
                    pinChanged = true;
                }
                Log.e("typeItemitem_bfr", typeItem + "..." + oneItemServer.get(0));
                Item item = new Item();
                item.setStrTitle(tvTitle.getText().toString());
                item.setStrValue(toSingleString.toString().trim());
                item.setArryObjstrTodoList(todoCheckboxItems);

                item.setImgNoteList(imgDetailss);
                item.setFileAudioList(arrAudioDetails);
//                item.setIntHeight(height);
//                item.setIntWeight(weight);
                item.setArryCollaboratUserId(collUsers);
                item.setBlnPinNote(isPinned);

                item.setDatModifiedDateAndTime(dateModified);
                item.setIntModifiedUserId(strUserId);

                item.setfkIntCategoryTypeId(oneItemServer.get(0).getfkIntCategoryTypeId());
                item.setfkIntCategoryId(oneItemServer.get(0).getfkIntCategoryId());
                item.setfkIntSubCategoryId(oneItemServer.get(0).getfkIntSubCategoryId());

                item.setStrCategoryTypeName(oneItemServer.get(0).getStrCategoryTypeName());
                item.setStrCategoryName(oneItemServer.get(0).getStrCategoryName());
                item.setStrDefaultSubName(oneItemServer.get(0).getStrDefaultSubName());

                item.setDatCreateDateAndTime(oneItemServer.get(0).getDatCreateDateAndTime());
                item.setIntCreateUserId(oneItemServer.get(0).getIntCreateUserId());
                item.setIntPlannerMainId(oneItemServer.get(0).getIntPlannerMainId());
                item.setIntPlannerId(oneItemServer.get(0).getIntPlannerId());

                item.setIntProjectId(mUserDetails.getArryselectedProjectAllItems().get(0).getId());
                item.setIntOrganisationId(mUserDetails.getArryselectedOrganisationAllItems().get(0).getId());
                item.setObjRemainder(objRemainder);
//                item.setStrDuration(tvAudio.getText().toString());

                noteUpdatedList.clear();
                noteUpdatedList.add(item);
                Gson gson = new Gson();
//            String val = gson.toJson(noteUpdatedList.get(0));
                Log.e("typeItemitem", typeItem + "...");
                if (typeItem.equals("update")) {

                    mRealm.beginTransaction();
                    mRealm.copyToRealmOrUpdate(noteUpdatedList.get(0));
                    mRealm.commitTransaction();

                    Intent returnIntent = new Intent();//NoteDetailActivity.this, PlannerFragment.class
//                    returnIntent.putExtra("item",oneItemServer.get(0));//noteUpdatedList.get(0));
                    returnIntent.putExtra("position", positionItem);
                    returnIntent.putExtra("message", "update");
                    returnIntent.putExtra("pinChanged", pinChanged);
                    this.setResult(1, returnIntent); //By not passing the intent in the result, the calling activity will get null data.
                    super.finish();

                } else {

                    RealmResults<Item> deleteFromRealm = mRealm.where(Item.class).equalTo("intPlannerId", oneItemServer.get(0).getIntPlannerId()).findAll();
                    mRealm.beginTransaction();
                    deleteFromRealm.deleteAllFromRealm();
                    mRealm.commitTransaction();

                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("item", noteUpdatedList.get(0));
                    returnIntent.putExtra("position", positionItem);
                    returnIntent.putExtra("message", "delete");
                    returnIntent.putExtra("pinChanged", pinChanged);
                    setResult(1, returnIntent); //By not passing the intent in the result, the calling activity will get null data.
                    super.finish();

                }


            } else {

                Intent returnIntent = new Intent();//NoteDetailActivity.this, PlannerFragment.class
                returnIntent.putExtra("plannerId", oneItemServer.get(0).getIntPlannerId());
                returnIntent.putExtra("position", positionItem);
                returnIntent.putExtra("message", "offline");
                returnIntent.putExtra("pinChanged", pinChanged);
                this.setResult(1, returnIntent); //By not passing the intent in the result, the calling activity will get null data.
                super.finish();

            }
        } else {
            super.finish();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerInternetCheckReceiver();
    }

    private void registerInternetCheckReceiver() {
        IntentFilter internetFilter = new IntentFilter();
        internetFilter.addAction("android.net.wifi.STATE_CHANGE");
        internetFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(broadcastReceiver, internetFilter);
    }

    private void setSnackbarMessage(String status, boolean showBar) {
        String internetStatus = "";
        Snackbar snackbar;
        if (status.equalsIgnoreCase("Wifi enabled") || status.equalsIgnoreCase("Mobile data enabled")) {
            internetStatus = "Connecting..";
        } else {
            internetStatus = "No Internet Connection";
        }
        if (internetStatus.equalsIgnoreCase("No Internet Connection")) {
            if (internetConnected) {
                snackbar = Snackbar.make(findViewById(R.id.llBottom), internetStatus, Snackbar.LENGTH_INDEFINITE);

                snackbar.setActionTextColor(Color.WHITE);
                View sbView = snackbar.getView();
                TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbar.show();
                internetConnected = false;
            }
        } else {
            if (!internetConnected) {
                internetConnected = true;
                snackbar = Snackbar.make(findViewById(R.id.llBottom), internetStatus, Snackbar.LENGTH_LONG);
                View sbView = snackbar.getView();
                TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbar.setActionTextColor(Color.WHITE);
                snackbar.show();
            }
        }
    }

}

