package com.example.user.communicator.Mqtt;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;

import com.example.user.communicator.Model.ChatModels.Messages;
import com.google.gson.Gson;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class MqttUtil {
    private static boolean published;
    @SuppressLint("StaticFieldLeak")
    public static MqttAndroidClient client;

    private Context mCon;
    private String clienttopic;
    public static String curruntChatUser = "";


    public MqttUtil() {
        Interfaces.setgetConnectMqttClient(getConnectMqttClient);
        Interfaces.setgetPublishMessage(getPublishMessage);
    }

    Interfaces.getConnectMqttClient getConnectMqttClient = this::getClient;

    public MqttAndroidClient getClient(Context context, String address, String clientId, String clientName) {
        try {
            if (client == null) {
                client = new MqttAndroidClient(context, address, clientId);
            }
            mCon = context;
            clienttopic = clientName;
            if (!client.isConnected()) {
                connect(context, clientName);
            } else {
                subscribeMessage(clientName);
            }


            return client;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void connect(final Context context, final String _topic) {
        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setCleanSession(true);
        mqttConnectOptions.setKeepAliveInterval(10000);

        try {
            client.connect(mqttConnectOptions, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
//                    Toast.makeText(context, "Connection created Successfully .", Toast.LENGTH_LONG).show();
                    if (!TextUtils.isEmpty(_topic)) {
                        subscribeMessage(_topic);
                    }
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
//                    Toast.makeText(context, "Connection problem occur .", Toast.LENGTH_LONG).show();
//                    connect(context, _topic);

                }
            });
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public void publishMessage(String topic, final String payload) {
        published = false;
        try {
            byte[] encodedpayload = payload.getBytes();
            MqttMessage message = new MqttMessage(encodedpayload);
            client.publish(topic, message);
            published = true;

        } catch (Exception e) {

            e.printStackTrace();
        }
    }


    public void unsubscribeMessage(final String payload) {
        try {
            client.unsubscribe(payload);
            disconnect(mCon);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }


    public void subscribeMessage(final String payload) {
        published = false;
        try {
            byte[] encodedpayload = payload.getBytes();
            MqttMessage message = new MqttMessage(encodedpayload);
            client.subscribe(payload, 0);
            published = true;
            getMsg();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void close() {
        if (client != null) {
            client.unregisterResources();
            client.close();
        }
    }

    public void getMsg() {
        try {
            client.setCallback(new MqttCallback() {
                @Override
                public void connectionLost(Throwable cause) {
//                    if (client != null) {
//                        connect(mCon, clienttopic);
//                    }
                }

                @Override
                public void messageArrived(String topic, MqttMessage message) {
                    Gson g = new Gson();
                    Messages msg = g.fromJson(new String(message.getPayload()), Messages.class);
                    if (!TextUtils.isEmpty(msg.getMessage()) || msg.isHaveMedia()) {
                        if (!TextUtils.isEmpty(curruntChatUser)) {
                            if (curruntChatUser.equals(msg.getGroupId())) {
                                Interfaces.mNewMessageListener.getNewMessage(new String(message.getPayload()));
                            }
                        }
                    }
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void disconnect(final Context contex) {
        try {
            IMqttToken disconToken = null;
            try {
                disconToken = client.disconnect();
            } catch (MqttException e) {
                e.printStackTrace();
            }
            if (disconToken != null) {
                disconToken.setActionCallback(new IMqttActionListener() {
                    @Override
                    public void onSuccess(IMqttToken asyncActionToken) {
                        client = null;
                    }

                    @Override
                    public void onFailure(IMqttToken asyncActionToken,
                                          Throwable exception) {

                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


//    @Override
//    public void ConnectMqttClient(Context context, String address, String clientName) {
//        getClient(context, address, clientName);
//    }

    Interfaces.getPublishMessage getPublishMessage = new Interfaces.getPublishMessage() {
        @Override
        public void PublishMessage(String topic, String payload) {
            publishMessage(topic, payload);
        }
    };
}
