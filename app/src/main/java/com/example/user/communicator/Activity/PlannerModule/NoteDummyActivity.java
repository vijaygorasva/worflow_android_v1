package com.example.user.communicator.Activity.PlannerModule;

import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.user.communicator.Adapter.PlannerAdapter.DataSetAdapter;
import com.example.user.communicator.Adapter.PlannerAdapter.PlannerSectionAdapter;
import com.example.user.communicator.CustomViews.CustomEditText;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.Notes.Item;
import com.example.user.communicator.Model.Notes.ObjRemainder;
import com.example.user.communicator.Model.Notes.TodoListItem;
import com.example.user.communicator.R;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

public class NoteDummyActivity extends AppCompatActivity {

    @BindView(R.id.ivPin)
    ImageView ivPin;
    @BindView(R.id.tvEdited)
    TextView tvEdited;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.rlCheckbox)
    LinearLayout rlCheckbox;
    @BindView(R.id.ivConvert)
    ImageView ivConvert;
    @BindView(R.id.ivAdd)
    ImageView ivAdd;
    @BindView(R.id.ivImage)
    ImageView ivImage;
    @BindView(R.id.flImage)
    FrameLayout flImage;
    @BindView(R.id.tvToolbar)
    TextView tvToolbar;
    @BindView(R.id.toolbar)
    RelativeLayout toolbar;
    @BindView(R.id.tvTitle)
    EditText tvTitle;
    @BindView(R.id.tvCheckbox)
    TextView tvCheckbox;
    @BindView(R.id.llUndos)
    LinearLayout llUndos;
    @BindView(R.id.llBottom)
    RelativeLayout llBottom;
    @BindView(R.id.tvTimer)
    TextView tvTimer;
    @BindView(R.id.tvAudio)
    TextView tvAudio;
    @BindView(R.id.llFileUpload)
    LinearLayout llFileUpload;
    @BindView(R.id.llAudio)
    LinearLayout llAudio;
    @BindView(R.id.rlMain)
    RelativeLayout rlMain;
    @BindView(R.id.llColleborate)
    LinearLayout llColleborate;
    @BindView(R.id.ivRedo)
    ImageView ivRedo;
    @BindView(R.id.ivUndo)
    ImageView ivUndo;
    MediaPlayer mediaPlayer;
    Boolean isAudioPlaying = false;
    SeekBar seekbar;
    @BindView(R.id.ivPlay)
    ImageView ivPlay;

    Realm mRealm;
    Datum mUserDetails;
    String strUserId;
    Typeface font_roboto_regular;

    CustomEditText editText;
    String activityType, plannerID, isImage = "no";
    int positionItem;
    ArrayList<TodoListItem> arrayToDoList = new ArrayList<>();
    ArrayList<Item> oneItemServer = new ArrayList<>();


    ObjRemainder objRemainderOld = new ObjRemainder();


    ////

    @BindView(R.id.rvDatas)
    RecyclerView rvDatas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_dummy);
        ButterKnife.bind(this);
        mRealm = Realm.getDefaultInstance();
        SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences("Login", MODE_PRIVATE);
        String data = sharedpreferences.getString("UserData", null);
        Gson gson = new Gson();
        mUserDetails = gson.fromJson(data, Datum.class);
        strUserId = mUserDetails.getIntUserId();
        rvDatas.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvDatas.setLayoutManager(linearLayoutManager);
//        rvDatas.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(this, R.drawable.divider)));

        font_roboto_regular = Typeface.createFromAsset(getAssets(), "fonts/roboto_regular.ttf");
        Typeface font_sfp_regular = Typeface.createFromAsset(getAssets(), "fonts/SFProText_Regular.ttf");
        editText = new CustomEditText(NoteDummyActivity.this);
        editText.setPadding(10, 10, 10, 10);
        tvToolbar.setTypeface(font_roboto_regular);
        tvTitle.setTypeface(font_roboto_regular);
        tvEdited.setTypeface(font_sfp_regular);

        activityType = getIntent().getStringExtra("type");
        plannerID = getIntent().getStringExtra("plannerId");

        if (activityType.equals("activity")) {
            isImage = getIntent().getStringExtra("isImage");
            positionItem = getIntent().getIntExtra("position", -1);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                String transitionName = getIntent().getStringExtra(PlannerSectionAdapter.EXTRA_ITEM_TRANSITION_NAME);
                if (isImage.equals("yes")) {
                    supportPostponeEnterTransition();
                    ivImage.setTransitionName(transitionName);
                } else {
//                    supportStartPostponedEnterTransition();
//                    supportFinishAfterTransition();
//                    supportPostponeEnterTransition();
                    scrollView.setTransitionName(transitionName);//rlMain.setTransitionName(transitionName);
                }
            }

            RealmResults<Item> itemsRealmResults = mRealm.where(Item.class).equalTo("intPlannerId", Integer.parseInt(plannerID)).findAll();
            if (itemsRealmResults.size() != 0) {

                oneItemServer.clear();
                oneItemServer.addAll(itemsRealmResults);

                objRemainderOld.set_id(oneItemServer.get(0).getObjRemainder().get_id());
                objRemainderOld.setBlnFlage(oneItemServer.get(0).getObjRemainder().getBlnFlage());
                objRemainderOld.setStrRemainderDate(oneItemServer.get(0).getObjRemainder().getStrRemainderDate());
                objRemainderOld.setStrRemainderTime(oneItemServer.get(0).getObjRemainder().getStrRemainderTime());
                objRemainderOld.setStrType(oneItemServer.get(0).getObjRemainder().getStrType());
                setDatas();
//                dataServer(oneItemServer.get(0));

            }
        } else {
            RealmResults<Item> itemsRealmResults = mRealm.where(Item.class).equalTo("intPlannerId", Integer.parseInt(plannerID)).findAll();
            if (itemsRealmResults != null && itemsRealmResults.size() != 0) {

                oneItemServer.addAll(itemsRealmResults);
//                dataServer(oneItemServer.get(0));
//                getColleborateUsers();
            } else {
//                getSingleNote(plannerID);

            }
        }

    }

    public void setDatas() {
        arrayToDoList.clear();
        arrayToDoList.addAll(oneItemServer.get(0).getArryObjstrTodoList());
        DataSetAdapter dataSetAdapter = new DataSetAdapter(NoteDummyActivity.this, "CHECKBOX", arrayToDoList);
        rvDatas.setAdapter(dataSetAdapter);
    }

}
