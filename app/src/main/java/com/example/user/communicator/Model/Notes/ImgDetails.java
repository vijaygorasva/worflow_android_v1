
package com.example.user.communicator.Model.Notes;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class ImgDetails extends RealmObject implements Parcelable {

    @SerializedName("intImageId")
    @Expose
    private String intImageId;


    protected ImgDetails(Parcel in) {
        intImageId = in.readString();
        fieldname = in.readString();
        originalname = in.readString();
        encoding = in.readString();
        mimetype = in.readString();
        destination = in.readString();
        filename = in.readString();
        path = in.readString();
//        size = in.readInt();
    }
    @SerializedName("fieldname")
    @Expose
    private String fieldname;
    @SerializedName("originalname")
    @Expose
    private String originalname;
    @SerializedName("encoding")
    @Expose
    private String encoding;
    @SerializedName("mimetype")
    @Expose
    private String mimetype;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("filename")
    @Expose
    private String filename;
    @SerializedName("path_img")
    @Expose
    private String path;
//    @SerializedName("size")
//    @Expose
//    private int size;
    @SerializedName("intHeight")
    @Expose
    private int intHeight;
    @SerializedName("intWeight")
    @Expose
    private int intWeight;

    public String getIntImageId() {
        return intImageId;
    }

    public void setIntImageId(String intImageId) {
        this.intImageId = intImageId;
    }

    public int getIntHeight() {
        return intHeight;
    }

    public void setIntHeight(int intHeight) {
        this.intHeight = intHeight;
    }

    public int getIntWeight() {
        return intWeight;
    }

    public void setIntWeight(int intWeight) {
        this.intWeight = intWeight;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getOriginalname() {
        return originalname;
    }

    public void setOriginalname(String originalname) {
        this.originalname = originalname;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }


    @Override
    public int describeContents() {
        return 0;
    }
    public static final Creator<ImgDetails> CREATOR = new Creator<ImgDetails>() {
        @Override
        public ImgDetails createFromParcel(Parcel in) {
            return new ImgDetails(in);
        }

        @Override
        public ImgDetails[] newArray(int size) {
            return new ImgDetails[size];
        }
    };


    public ImgDetails() {

    }

//    public void setSize(int size) {
//        this.size = size;
//    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(intImageId);
        parcel.writeString(fieldname);
        parcel.writeString(originalname);
        parcel.writeString(encoding);
        parcel.writeString(mimetype);
        parcel.writeString(destination);
        parcel.writeString(filename);
        parcel.writeString(path);
//        parcel.writeInt(size);

    }

}
