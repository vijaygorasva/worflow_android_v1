package com.example.user.communicator.Model.TaskModels;

import io.realm.RealmObject;

public class Replies extends RealmObject {
    private String _id;
    private int intTaskDocumentNo;
    private String intCommentId;
    private String comment;
    private int isHaveAttachment;
    private int status;
    private int isReply;
    private long addTime;
    private CreateUser intUserId;

    public CreateUser getIntUserId() {
        return intUserId;
    }

    public void setIntUserId(CreateUser intUserId) {
        this.intUserId = intUserId;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public int getIntTaskDocumentNo() {
        return intTaskDocumentNo;
    }

    public void setIntTaskDocumentNo(int intTaskDocumentNo) {
        this.intTaskDocumentNo = intTaskDocumentNo;
    }

    public String getIntCommentId() {
        return intCommentId;
    }

    public void setIntCommentId(String intCommentId) {
        this.intCommentId = intCommentId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getIsHaveAttachment() {
        return isHaveAttachment;
    }

    public void setIsHaveAttachment(int isHaveAttachment) {
        this.isHaveAttachment = isHaveAttachment;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getIsReply() {
        return isReply;
    }

    public void setIsReply(int isReply) {
        this.isReply = isReply;
    }

    public long getAddTime() {
        return addTime;
    }

    public void setAddTime(long addTime) {
        this.addTime = addTime;
    }
}
