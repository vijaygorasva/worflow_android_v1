package com.example.user.communicator.Adapter.PlannerAdapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.communicator.Model.Login.Login;
import com.example.user.communicator.Model.NoteCategory.Datum;
import com.example.user.communicator.R;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

public class PlannerCategorylistListAdapter extends RecyclerView.Adapter<PlannerCategorylistListAdapter.ViewHolder> {

    int btnVisible = 0;
    public final String BASE_URL = "http://52.66.249.75:9999/";
    private final String URL_ROOT = "http://52.66.249.75:9999/api/";
    public final String deletePlanner = URL_ROOT + "planner/deletemaincategory";
    public final String updatePlanner = URL_ROOT + "planner/updatemaincategory";
    Callback callback;
    Activity context;
    private List<Datum> mainDataList = null;
    String strUserId;
    private int expandPos = 9999;
    com.example.user.communicator.Model.Login.Datum mUserDetails;
    private ProgressDialog pDialog;
    Boolean isExpandedSave = false;
    private RecyclerView recyclerView;

    public PlannerCategorylistListAdapter(Activity context, ArrayList<Datum> mainDataList, Callback callback, RecyclerView recyclerView) {
        this.mainDataList = mainDataList;
        this.callback = callback;
        this.context = context;
        this.recyclerView = recyclerView;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_catgeory, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position1) {
        holder.bind(mainDataList.get(position1), holder,position1);
    }

    @Override
    public int getItemCount() {
        return mainDataList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void showProgressDialog() {
        if (pDialog == null || !pDialog.isShowing()) {
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Please Wait!");
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }

    public void hideProgressDialog() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    public void showToast(String message) {
        LayoutInflater inflater = context.getLayoutInflater();
        View layouttoast = inflater.inflate(R.layout.toastcustom, (ViewGroup) context.findViewById(R.id.toastcustom));
        ((TextView) layouttoast.findViewById(R.id.texttoast)).setText(message);

        Toast mytoast = new Toast(context);
        mytoast.setView(layouttoast);
        mytoast.setDuration(Toast.LENGTH_LONG);
        mytoast.setGravity(Gravity.CENTER, 0, 0);
        mytoast.show();
    }

    public void insertItem(Datum model, int position) {
        mainDataList.add(position, model);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvCategory)
        TextView tvCategory;
        @BindView(R.id.etCategory)
        EditText etCategory;
        @BindView(R.id.ivSave)
        ImageView ivSave;
        @BindView(R.id.ivDelete)
        ImageView ivDelete;
        @BindView(R.id.llMain)
        LinearLayout llMain;
        @BindView(R.id.flIcons)
        FrameLayout flIcons;

        public ViewHolder(View itemView) {

            super(itemView);
            ButterKnife.bind(this, itemView);

        }

        public void bind(final Datum items, final ViewHolder holder, int position) {
            final boolean isExpanded = holder.getAdapterPosition() == expandPos;
            SharedPreferences sharedpreferences = context.getApplicationContext().getSharedPreferences("Login", Context.MODE_PRIVATE);
            String data = sharedpreferences.getString("UserData", null);
            Gson gson = new Gson();
            mUserDetails = gson.fromJson(data, com.example.user.communicator.Model.Login.Datum.class);
            strUserId = mUserDetails.getIntUserId();

            Typeface font_roboto_regular = Typeface.createFromAsset(context.getAssets(), "fonts/roboto_regular.ttf");
            Typeface font_roboto_light = Typeface.createFromAsset(context.getAssets(), "fonts/roboto_light.ttf");
            Typeface font_sfp_regular = Typeface.createFromAsset(context.getAssets(), "fonts/SFProText_Regular.ttf");

            tvCategory.setTypeface(font_roboto_regular);
            tvCategory.setText(items.getStrMainCategoryName());

            llMain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (btnVisible == 0) {
//                        Intent intent = new Intent();
//                        intent.putExtra("ID", items.getId());
//                        intent.putExtra("NAME", items.getStrMainCategoryName());
//                        (context).setResult(2, intent);
                        callback.getCategorySelceted(PlannerCategorylistListAdapter.this, items.getId(), items.getStrMainCategoryName());
                    } else {
                        etCategory.setCursorVisible(true);
                        etCategory.requestFocus();
                        etCategory.setSelection(etCategory.getText().toString().length());
                    }

                }
            });

            llMain.setActivated(isExpanded);
            etCategory.setCursorVisible(isExpanded);
            flIcons.setVisibility(View.GONE);
            if ((isExpanded) && (!isExpandedSave)) {
                flIcons.setVisibility(View.VISIBLE);
                tvCategory.setVisibility(View.GONE);
                etCategory.setVisibility(View.VISIBLE);
                isExpandedSave = false;
            } else {
                flIcons.setVisibility(View.GONE);
                tvCategory.setVisibility(View.VISIBLE);
                etCategory.setVisibility(View.GONE);

            }
            if (position != 0){
                llMain.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {

                        if (isNetworkAvailable()) {
                            expandPos = isExpanded ? -1 : holder.getAdapterPosition();
                            isExpandedSave = false;
//                            if ((isExpanded) && (isExpandedSave)) {
//                                notifyItemChanged(holder.getAdapterPosition());
//                                etCategory.setCursorVisible(false);
//                            } else {
//                                TransitionManager.beginDelayedTransition(recyclerView);
//                            }

                            btnVisible = 1;
                            tvCategory.setVisibility(View.GONE);
                            etCategory.setVisibility(View.VISIBLE);
                            etCategory.setCursorVisible(true);
                            etCategory.requestFocus();

                            etCategory.setText(tvCategory.getText().toString());
                            etCategory.setSelection(etCategory.getText().toString().length());
                            notifyItemChanged(getItemViewType());
                            notifyItemRangeChanged(0, mainDataList.size());
                            notifyDataSetChanged();

                        } else {
                            showToast(context.getString(R.string.noConnection));
                        }
                        return false;

                    }
                });
            }



//
//            llMain.setActivated(isExpanded);
//            ivSave.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
//            Log.e("OnLongClick_no..",isExpanded+"..."+expandPos);

//            llMain.setOnTouchListener(new View.OnTouchListener() {
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//                    expandPos = isExpanded ? -1 : holder.getAdapterPosition();
//                    Log.e("OnLongClick_note.","..."+isExpanded);
//                    if (isExpanded) {
//                        notifyItemChanged(holder.getAdapterPosition());
//                        notifyDataSetChanged();
//                    } else {
//                        TransitionManager.beginDelayedTransition(recyclerView);
//                        notifyDataSetChanged();
//                    }
//                    return false;
//                }
//            });

            ivSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (isNetworkAvailable()) {
                        String catID = items.getId();
                        String Name = items.getStrMainCategoryName();
                        int position = getItemViewType();
                        btnVisible = 0;
                        ivSave.setVisibility(View.GONE);
                        String catName = etCategory.getText().toString();
                        if (catName.length() > 15) {
                            showToast("Category name should be within 15 characters");
                        } else {

                            updateCategory(catID, catName);
                        }
//                        isExpanded=false;
                    } else {
                        showToast(context.getString(R.string.noConnection));
                    }
                }
            });

            ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (isNetworkAvailable()) {
                        int pos = getItemViewType();
                        Log.e("Datum_deleteItem", pos + "..." + new Gson().toJson(items) + "");
                        mainDataList.remove(pos);
                        notifyItemRemoved(pos);
                        deletemaincategory(items);
                        ivDelete.setVisibility(View.GONE);
                        ivSave.setVisibility(View.GONE);
                    } else {
                        showToast(context.getString(R.string.noConnection));
                    }

                }
            });

        }

        private void deletemaincategory(final Datum items) {

            RequestParams params = new RequestParams();
            params.put("strMainCategoryId", items.getId());
            params.put("strModifiedUserId",strUserId);
            Log.e("Datum_params", params + "..." + deletePlanner + "");
            SharedPreferences sharedpreferences = context.getApplicationContext().getSharedPreferences("Login", Context.MODE_PRIVATE);
            String data = sharedpreferences.getString("UserData", null);
            String token = sharedpreferences.getString("Token", null);
            Gson gson = new Gson();
            Login userData = gson.fromJson(data, Login.class);
//            showProgressDialog();
            try {
                AsyncHttpClient client = new AsyncHttpClient();
                client.setTimeout(60 * 1000);
                if (userData != null) {
                    client.addHeader("Authorization", "Bearer " + token);
                }
                client.setURLEncodingEnabled(true);
                client.post(context, deletePlanner, params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                        hideProgressDialog();
                        try {
                            if (response.getBoolean("success")) {

                                notifyDataSetChanged();
                                showToast(response.getString("message"));
                                isExpandedSave = true;
                                flIcons.setVisibility(View.GONE);
                            } else {
                                showToast(response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        hideProgressDialog();
                        showToast("Oops!!! Please try again");
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        hideProgressDialog();
                        showToast("Oops!!! Please try again");
                    }

                });
            } catch (Exception e) {
                hideProgressDialog();
                e.printStackTrace();
            }
        }

        private void updateCategory(final String catId, final String catName) {

            RequestParams params = new RequestParams();
            params.put("strMainCategoryId", catId);
            params.put("strMainCategoryName", catName);
            params.put("strLastModifiedUserId", strUserId);

            SharedPreferences sharedpreferences = context.getApplicationContext().getSharedPreferences("Login", Context.MODE_PRIVATE);
            String data = sharedpreferences.getString("UserData", null);
            String token = sharedpreferences.getString("Token", null);
            Gson gson = new Gson();
            Login userData = gson.fromJson(data, Login.class);
//            showProgressDialog();
            try {
                AsyncHttpClient client = new AsyncHttpClient();
                client.setTimeout(60 * 1000);
                if (userData != null) {
                    client.addHeader("Authorization", "Bearer " + token);
                }
                client.setURLEncodingEnabled(true);

                client.post(context, updatePlanner, params, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
//                        hideProgressDialog();
                        try {
                            if (response.getBoolean("success")) {
                                final int position = getItemViewType();
                                notifyItemChanged(position);
                                mainDataList.get(position).setStrMainCategoryName(catName);
                                notifyItemChanged(position);
                                tvCategory.setText(catName);
//                                notifyItemRangeChanged(0, mainDataList.size());
//                                showToast(response.getString("message"));
                                isExpandedSave = true;
                                flIcons.setVisibility(View.GONE);
                            } else {
                                showToast(response.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        hideProgressDialog();
                        showToast("Oops!!! Please try again");
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        hideProgressDialog();
                        showToast("Oops!!! Please try again");
                    }
                });
            } catch (Exception e) {
                hideProgressDialog();
                e.printStackTrace();
            }
        }

        public boolean isNetworkAvailable() {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }

    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public interface Callback {
        void getCategorySelceted(PlannerCategorylistListAdapter dialog, String id, String catName);
    }
}
