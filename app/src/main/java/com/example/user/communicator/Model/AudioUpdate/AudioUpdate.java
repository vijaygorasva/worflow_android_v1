
package com.example.user.communicator.Model.AudioUpdate;

import com.example.user.communicator.Model.Notes.Item;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AudioUpdate {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Item data;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Item getData() {
        return data;
    }

    public void setData(Item data) {
        this.data = data;
    }

}
