
package com.example.user.communicator.Model.Colleborate;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataSave {

    @SerializedName("intPlannerId")
    @Expose
    private Integer intPlannerId;
    @SerializedName("arryCollaboratUserId")
    @Expose
    private List<String> arryCollaboratUserId = null;
    @SerializedName("intPlannerMainId")
    @Expose
    private String intPlannerMainId;

    public Integer getIntPlannerId() {
        return intPlannerId;
    }

    public void setIntPlannerId(Integer intPlannerId) {
        this.intPlannerId = intPlannerId;
    }

    public List<String> getArryCollaboratUserId() {
        return arryCollaboratUserId;
    }

    public void setArryCollaboratUserId(List<String> arryCollaboratUserId) {
        this.arryCollaboratUserId = arryCollaboratUserId;
    }

    public String getIntPlannerMainId() {
        return intPlannerMainId;
    }

    public void setIntPlannerMainId(String intPlannerMainId) {
        this.intPlannerMainId = intPlannerMainId;
    }

}
