package com.example.user.communicator.Model.ChatModels;

import java.util.ArrayList;

public class Group {

    private String _id;
    private ArrayList<String> memberIds = new ArrayList<>();

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public ArrayList<String> getMemberIds() {
        return memberIds;
    }

    public void setMemberIds(ArrayList<String> memberIds) {
        this.memberIds = memberIds;
    }
}
