package com.example.user.communicator.Activity.TaskModule.DetailsActivity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.user.communicator.Adapter.TaskAdapters.FilesAdapter;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.TaskModels.SelectionItems.FileData;
import com.example.user.communicator.Model.TaskModels.TaskDetailItem;
import com.example.user.communicator.R;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

public class FilesPageFrgament extends BaseFragment {


    @BindView(R.id.filesLayout)
    LinearLayout filesLayout;

    @BindView(R.id.filesView)
    RecyclerView filesView;

    @BindView(R.id.noData)
    CustomTextView noData;

    LinearLayoutManager linearLayoutManager2;
    FilesAdapter attachmentAdapter;
    ArrayList<FileData> attachmentItemArrayList = new ArrayList<>();

    TaskDetailItem items;
    Context context;
    String task_Id;
    Realm mRealm;
    Datum mUserData;


    public static FilesPageFrgament newInstance(String _id, boolean isFromNotification) {
        FilesPageFrgament fragmentFirst = new FilesPageFrgament();
        Bundle args = new Bundle();
        args.putString("_id", _id);
        args.putBoolean("isFromNotification", isFromNotification);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.layout_files_fragment, container, false);
        ButterKnife.bind(this, rootView);

        context = getActivity();
        mRealm = Realm.getDefaultInstance();
        mUserData = getLoginData();
        task_Id = String.valueOf(getArguments().getString("_id"));
        if (isNetworkAvailable()) {
            if (!TextUtils.isEmpty(task_Id)) {
                getData(task_Id);
            }
        } else {
            if (!TextUtils.isEmpty(task_Id)) {
                items = mRealm.where(TaskDetailItem.class).equalTo("intTaskDocumentNo", Integer.valueOf(task_Id)).findFirst();
                setData();
            }
        }


        return rootView;
    }

    public void setData() {
        linearLayoutManager2 = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        filesView.setLayoutManager(linearLayoutManager2);
        attachmentItemArrayList.clear();
        if (items.getObjFileData() != null && items.getObjFileData().size() != 0) {
            attachmentItemArrayList.addAll(items.getObjFileData());
        }
        if (items.getCommentFiles() != null && items.getCommentFiles().size() != 0) {
            attachmentItemArrayList.addAll(items.getCommentFiles());
        }

        if (attachmentItemArrayList.size() != 0) {
            noData.setVisibility(View.GONE);
        } else {
            noData.setVisibility(View.VISIBLE);
        }
        attachmentAdapter = new FilesAdapter(context, attachmentItemArrayList);
        filesView.setAdapter(attachmentAdapter);
    }


    @Override
    public void OnResponce(JSONObject object, ApiCall type) {
        hideProgressDialog();
        try {
            if (type == ApiCall.GET_TASK_DATA) {
                Gson gson = new Gson();
                TaskDetailItem items = gson.fromJson(object.getJSONObject("data").getJSONObject("taskData").toString(), TaskDetailItem.class);
                items.setDetailAvailable(true);
                mRealm.beginTransaction();
                mRealm.copyToRealmOrUpdate(items);
                mRealm.commitTransaction();
                this.items = mRealm.where(TaskDetailItem.class).equalTo("_id", items.get_id()).findFirst();
                setData();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnError(JSONObject object, ApiCall type) {
        try {
            if (type == ApiCall.GET_TASK_DATA) {
                showToast(object.getString("message"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getData(String id) {
        RequestParams params = new RequestParams();
        params.put("intTaskDocumentNo", id);
        showProgressDialog("Please wait...");
        ApiCallWithToken(getTaskData, ApiCall.GET_TASK_DATA, params);
    }


}
