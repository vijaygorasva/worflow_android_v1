package com.example.user.communicator.Model.TaskModels;

import java.util.ArrayList;


public class SelectionObject {


    private boolean isUser = false;
    private ArrayList<SelectionItem> users = new ArrayList<>();
    private ArrayList<SelectionItem> levels = new ArrayList<>();
    private ArrayList<SelectionItem> memberTypes = new ArrayList<>();


    public boolean isUser() {
        return isUser;
    }

    public void setUser(boolean user) {
        isUser = user;
    }

    public ArrayList<SelectionItem> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<SelectionItem> users) {
        this.users = users;
    }

    public ArrayList<SelectionItem> getLevels() {
        return levels;
    }

    public void setLevels(ArrayList<SelectionItem> levels) {
        this.levels = levels;
    }

    public ArrayList<SelectionItem> getMemberTypes() {
        return memberTypes;
    }

    public void setMemberTypes(ArrayList<SelectionItem> memberTypes) {
        this.memberTypes = memberTypes;
    }
}
