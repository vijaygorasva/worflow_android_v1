
package com.example.user.communicator.Model.NoteUpdate;

import java.util.List;

import com.example.user.communicator.Model.Notes.Item;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NoteUpdate {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Item> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Item> getData() {
        return data;
    }

    public void setData(List<Item> data) {
        this.data = data;
    }

}
