package com.example.user.communicator.Service;


import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import com.example.user.communicator.Model.TaskModels.TaskItems;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class OnClearFromRecentService extends Service {

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("ClearFromRecentService", "Service Started");
        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("ClearFromRecentService", "Service Destroyed");
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Log.e("ClearFromRecentService", "END");

//        if (Constants.finalTaskItems != null) {
//            if (Constants.mTimer != null) {
//                Constants.mTimer.cancel();
//                TimeCounter.finalTaskItems.getWorkHistories().get(TimeCounter.finalTaskItems.getWorkHistories().size() - 1).setEndTime(getCurruntTime());
//                TimeCounter.finalTaskItems.getWorkHistories().get(TimeCounter.finalTaskItems.getWorkHistories().size() - 1).
//                        setTotalTime((hours < 10 ? (hours != 0 ? "0" + hours + " hours " : "") : hours + " hours ")
//                                + (minutes < 10 ? (minutes != 0 ? "0" + minutes + " minutes " : "") : minutes + " minutes ")
//                                + (seconds < 10 ? (seconds != 0 ? "0" + seconds + " seconds " : "") : seconds + " seconds "));
//                ArrayList<TaskItems> itemsArrayList = getTaskItems();
//                boolean isF = false;
//                for (int i = 0; i < itemsArrayList.size() && !isF; i++) {
//                    if (itemsArrayList.get(i).get_id().equalsIgnoreCase(Constants.finalTaskItems.get_id())) {
//                        isF = true;
//                        Gson gson = new Gson();
//                        String data = gson.toJson(Constants.finalTaskItems);
//                        TaskItems items = gson.fromJson(data, TaskItems.class);
//                        itemsArrayList.set(i, items);
//                        setTaskItems(itemsArrayList);
//                    }
//                }
//            }
//        }
        stopSelf();
    }

    public String getCurruntTime() {
        SimpleDateFormat df = new SimpleDateFormat("hh:mm a", Locale.US);
        return df.format(Calendar.getInstance().getTime());
    }


    public SharedPreferences getSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(this);
    }

    public ArrayList<TaskItems> getTaskItems() {
        String appInfo = getSharedPreferences().getString("TaskItems", "");
        Gson gson = new Gson();
        if (!TextUtils.isEmpty(appInfo)) {
            Type type = new TypeToken<ArrayList<TaskItems>>() {
            }.getType();
            return gson.fromJson(appInfo, type);
        }
        return new ArrayList<TaskItems>();
    }

    public void setTaskItems(ArrayList<TaskItems> appInfo) {
        SharedPreferences.Editor editor1 = getSharedPreferences().edit();
        Gson gson = new Gson();
        String data = gson.toJson(appInfo);
        editor1.putString("TaskItems", data);
        editor1.apply();
    }
}
