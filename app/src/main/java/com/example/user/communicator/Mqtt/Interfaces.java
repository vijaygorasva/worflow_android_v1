package com.example.user.communicator.Mqtt;

import android.content.Context;


public class Interfaces {


    public static getConnectMqttClient mConnectMqttClient;
    public static getPublishMessage mPublishMessage;
    public static getNewMessageListener mNewMessageListener;
    public static OnBackPressedListener onBackPressedListener;
    public static OnContactSyncedListener onContactSyncedListener;


    public interface getConnectMqttClient {
        void ConnectMqttClient(Context context, String address, String clientId,String clientName);
    }

    public static void setgetConnectMqttClient(getConnectMqttClient connectMqttClient) {
        mConnectMqttClient = connectMqttClient;
    }


    public interface getPublishMessage {
        void PublishMessage(String topic, final String payload);
    }

    public static void setgetPublishMessage(getPublishMessage publishMessage) {
        mPublishMessage = publishMessage;
    }


    public interface getNewMessageListener {
        void getNewMessage(String s);
    }

    public static void setgetNewMessageListener(getNewMessageListener newMessageListener) {
        mNewMessageListener = newMessageListener;
    }

    public interface OnBackPressedListener {
        void onBackPressed(boolean isBack);
    }

    public static void setOnBackPressedListener(OnBackPressedListener onBackPressedListener1) {
        onBackPressedListener = onBackPressedListener1;
    }

    public interface OnContactSyncedListener {
        void onContactSynced(boolean isContactSynced);
    }

    public static void setOnContactSyncedListener(OnContactSyncedListener onContactSyncedListener1) {
        onContactSyncedListener = onContactSyncedListener1;
    }
}
