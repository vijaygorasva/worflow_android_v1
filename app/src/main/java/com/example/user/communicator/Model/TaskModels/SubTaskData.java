package com.example.user.communicator.Model.TaskModels;

import io.realm.RealmObject;

public class SubTaskData extends RealmObject {

    private String _id;
    private int intTaskDocumentNo;
//    private String title;
    private String description;
    private long addTime;


    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public int getIntTaskDocumentNo() {
        return intTaskDocumentNo;
    }

    public void setIntTaskDocumentNo(int intTaskDocumentNo) {
        this.intTaskDocumentNo = intTaskDocumentNo;
    }

//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getAddTime() {
        return addTime;
    }

    public void setAddTime(long addTime) {
        this.addTime = addTime;
    }
}
