package com.example.user.communicator.Activity.TaskModule.DetailsActivity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.icu.text.DateFormat;
import android.icu.util.TimeZone;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.example.user.communicator.Activity.TaskModule.EditTaskActivity;
import com.example.user.communicator.Adapter.TaskAdapters.CommentsAdapter;
import com.example.user.communicator.Adapter.TaskAdapters.FilterTypeAdapter;
import com.example.user.communicator.Adapter.TaskAdapters.SubTasksAdapter;
import com.example.user.communicator.CustomViews.CustomEditText;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.TaskModels.CommentData;
import com.example.user.communicator.Model.TaskModels.Duration;
import com.example.user.communicator.Model.TaskModels.FilterType;
import com.example.user.communicator.Model.TaskModels.Replies;
import com.example.user.communicator.Model.TaskModels.SubTaskData;
import com.example.user.communicator.Model.TaskModels.TaskDetailItem;
import com.example.user.communicator.Model.TaskModels.TaskItems;
import com.example.user.communicator.R;
import com.example.user.communicator.Utility.EELViewUtils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;
import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.Sort;

import static android.app.Activity.RESULT_OK;

public class DetailsPageFragment extends BaseFragment {

    @BindView(R.id.vLoading)
    LinearLayout vLoading;

    @BindView(R.id.tvTitle)
    CustomTextView tvTitle;

    @BindView(R.id.etTitle)
    CustomEditText etTitle;

    @BindView(R.id.commentEdtTxt)
    CustomEditText mCommentEdtTxt;

    @BindView(R.id.send)
    ImageView mSend;

    @BindView(R.id.cmnt_file)
    ImageView mCmntFile;

    @BindView(R.id.cmnt_img)
    ImageView mCmntImgFile;

    @BindView(R.id.tvHeaderType)
    CustomTextView tvHeaderType;

    @BindView(R.id.tvPriorType)
    CustomTextView tvPriorType;

    @BindView(R.id.tvProgress)
    CustomTextView tvProgress;

    @BindView(R.id.tvStartDate)
    CustomTextView tvStartDate;

    @BindView(R.id.tvDash)
    CustomTextView tvDash;

    @BindView(R.id.tvEndDate)
    CustomTextView tvEndDate;

    @BindView(R.id.tvDuration)
    CustomTextView tvDuration;

    @BindView(R.id.viewMore)
    CustomTextView viewMore;

    @BindView(R.id.viewMore1)
    CustomTextView viewMoreComments;

    @BindView(R.id.ns)
    NestedScrollView ns;

    @BindView(R.id.rl)
    RelativeLayout rl;

    EELViewUtils mEelViewUtils;
//    @BindView(R.id.attachmentView)
//    RecyclerView attachmentView;

    @BindView(R.id.commentsView)
    RecyclerView commentsView;

    @BindView(R.id.subTasksView)
    RecyclerView subTasksView;

    @BindView(R.id.subTasksLayout)
    LinearLayout subTasksLayout;

    @BindView(R.id.prof_img)
    ImageView prof_img;

    @BindView(R.id.name)
    CustomTextView name;

    @BindView(R.id.tvDate)
    CustomTextView tvDate;

    @BindView(R.id.noOfComments)
    CustomTextView noOfComments;

    @BindView(R.id.taskStatus)
    CustomTextView taskStatus;

    @BindView(R.id.addCommentsView)
    RelativeLayout addCommentsView;

    @BindView(R.id.cmnt_file_name)
    CustomTextView cmnt_file_name;

    @BindView(R.id.prior_dot)
    ImageView dot;

    @BindView(R.id.priority)
    LinearLayout mPriorityLayout;

    @BindView(R.id.tvAddPrior)
    CustomTextView tvAddPrior;

    @BindView(R.id.update)
    ImageView update;

    //    AttachmentAdapter adapter;
    CommentsAdapter commentsAdapter;
    SubTasksAdapter subTasksAdapter;

    LinearLayoutManager linearLayoutManager, linearLayoutManager1, linearLayoutManager3;
    //    ArrayList<FileData> attachmentItemArrayList = new ArrayList<>();
    ArrayList<CommentData> commentMainList = new ArrayList<>();
    ArrayList<CommentData> commentDataArrayList = new ArrayList<>();
    ArrayList<SubTaskData> subTaskMainList = new ArrayList<>();
    ArrayList<SubTaskData> subTaskDataArrayList = new ArrayList<>();
    TaskDetailItem items;
    Context context;
    String task_Id;
    String task_status, tv_prior_type;
    Realm mRealm;

    Datum mUserData;
    boolean isFromNotification = false;
    String subTaskId;
    int mComPos;

    @BindView(R.id.addSubTask)
    FloatingActionButton mAddSubTasks;
    boolean isImage = false;
    boolean isFile = false;
    boolean isViewSubTask = false;
    boolean isViewCmnts = false;
    boolean isUpdating = false;
    String mUpdateType = "";
    String mainPath = "";
    String BROADCAST_ACTION = "com.example.user.communicator";

    @BindView(R.id.root)
    ViewGroup root;

    public static DetailsPageFragment newInstance(String _id, boolean isFromNotification) {
        DetailsPageFragment fragmentFirst = new DetailsPageFragment();
        Bundle args = new Bundle();
        args.putString("_id", _id);
        args.putBoolean("isFromNotification", isFromNotification);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    SubTasksAdapter.OnClickListner onClickListner = position -> {
        if (items.getIsClosed() == 0) {
            subTaskId = subTaskDataArrayList.get(position).get_id();
            RequestParams params = new RequestParams();
            params.put("intTaskDocumentNo", items.getIntTaskDocumentNo());
            params.put("intUserId", mUserData.getIntUserId());
            params.put("subTaskId", subTaskDataArrayList.get(position).get_id());
//            showProgressDialog("Please wait...");
            vLoading.setVisibility(View.VISIBLE);
            ApiCallWithToken(removeSubTaskData, ApiCall.REMOVE_SUB_TASK_DATA, params);
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().unregisterReceiver(notificationBroadcaster);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        root.getViewTreeObserver().removeOnGlobalLayoutListener(keyboardLayoutListener);

    }

    private void AddPrior() {
        String prior1 = items.getStrPriorityValue();
        if (!TextUtils.isEmpty(prior1)) {
            if (prior1.equalsIgnoreCase("4")) {
                Toast.makeText(context, "Task is already complete!", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        if (!isNetworkAvailable()) {
            Toast.makeText(context, context.getString(R.string.noConnection), Toast.LENGTH_SHORT).show();
            return;
        }
        showPriorityDialog(prior1);
    }

    private void showPriorityDialog(String prior1) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dilogue_filter_type);

        CustomTextView textView = dialog.findViewById(R.id.title);
        textView.setText("Change priority");
        RecyclerView recyclerView = dialog.findViewById(R.id.recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        ArrayList<FilterType> filterTypes = new ArrayList<>();
        FilterType filterType = new FilterType();
        filterType.setName("Low");
        filterType.setKey("1");
        filterTypes.add(filterType);

        FilterType filterType1 = new FilterType();
        filterType1.setName("Medium");
        filterType1.setKey("2");
        filterTypes.add(filterType1);

        FilterType filterType2 = new FilterType();
        filterType2.setName("High");
        filterType2.setKey("3");
        filterTypes.add(filterType2);
        if (!TextUtils.isEmpty(prior1)) {
            filterTypes.get(Integer.parseInt(prior1) - 1).setSelected(true);
        }

        FilterTypeAdapter adapter = new FilterTypeAdapter(context, filterTypes);
        recyclerView.setAdapter(adapter);
        adapter.setOnClickListner(position -> {
            for (int i = 0; i < filterTypes.size(); i++) {
                filterTypes.get(i).setSelected(false);
            }
            filterTypes.get(position).setSelected(true);
            String prior = filterTypes.get(position).getKey();
            adapter.notifyDataSetChanged();
            dialog.dismiss();
            if (!TextUtils.isEmpty(prior)) {
                RequestParams params = new RequestParams();
                params.put("intTaskDocumentNo", items.getIntTaskDocumentNo());
                params.put("intCreateUserId", mUserData.getIntUserId());
                params.put("strPriorityValue", prior);
                vLoading.setVisibility(View.VISIBLE);
//                showProgressDialog("Please wait...");
                tv_prior_type = prior;
                ApiCallWithToken(updateTask, ApiCall.UPDATE_PRIORITY, params);
            }
        });
        dialog.show();
    }

    @SuppressLint("RestrictedApi")
    ViewTreeObserver.OnGlobalLayoutListener keyboardLayoutListener = () -> {
        int heightDiff = root.getRootView().getHeight() - root.getHeight();
//        int contentViewTop = getActivity().getWindow().findViewById(Window.ID_ANDROID_CONTENT).getTop();
//            if (heightDiff <= contentViewTop) {
//                mAddSubTasks.setVisibility(View.VISIBLE);
//
//            } else {
//                int keyboardHeight = heightDiff - contentViewTop;
//                mAddSubTasks.setVisibility(View.GONE);
//            }
        if (items != null) {
            if (items.getIntCreateUserId().equalsIgnoreCase(mUserData.getIntUserId())) {
                if (mAddSubTasks != null) {
                    if (heightDiff >= 600) {
                        mAddSubTasks.setVisibility(View.GONE);
                    } else {
                        if (items.getIsClosed() == 1) {
                            mAddSubTasks.setVisibility(View.GONE);
                        } else {
                            mAddSubTasks.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null && data.getData() != null) {
            if (requestCode == 5555) {
                Uri uri = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
                    bitmap = compressedBitmap(bitmap);
                    mainPath = storeImage(bitmap);
                    isImage = true;
                    isFile = false;
                    cmnt_file_name.setText(mainPath.substring(mainPath.lastIndexOf("/") + 1));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (data != null && data.getData() != null) {
            if (requestCode == 5252 && resultCode == RESULT_OK) {
                Uri selectedPdf = data.getData();
                mainPath = getPath(context, selectedPdf);
                if (selectedPdf.getLastPathSegment().endsWith("pdf")) {
                    isFile = true;
                    isImage = false;
                    cmnt_file_name.setText(mainPath.substring(mainPath.lastIndexOf("/") + 1));
                } else if (mainPath.endsWith("pdf")) {
                    isFile = true;
                    isImage = false;
                    cmnt_file_name.setText(mainPath.substring(mainPath.lastIndexOf("/") + 1));
                } else if (isGoogleDriveDocument(selectedPdf)) {
                    isFile = true;
                    isImage = false;
                    cmnt_file_name.setText(mainPath.substring(mainPath.lastIndexOf("/") + 1));
                }
            }
        }
//        if (data != null) {
//            if (requestCode == 4521 && resultCode == RESULT_OK) {
//                boolean isUpdated = data.getBooleanExtra("isUpdated", false);
//                if (isUpdated) {
//                    DetailsTabActivity.isUpdated = true;
//                    if (!TextUtils.isEmpty(task_Id)) {
//                        items = mRealm.where(TaskDetailItem.class).equalTo("intTaskDocumentNo", Integer.valueOf(task_Id)).findFirst();
//                        setData();
//                    }
//                }
//            }
//        }
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressLint({"SetTextI18n", "RestrictedApi"})
    public void setData() {

//        if (items.getObjFileData() != null && items.getObjFileData().size() != 0) {
//            attachmentItemArrayList.clear();
//            attachmentItemArrayList.addAll(items.getObjFileData());
//        }

        if (!items.getIntCreateUserId().equalsIgnoreCase(mUserData.getIntUserId())) {
            if (!TextUtils.isEmpty(items.getIsTaskUpdated())) {
                if (items.getIsTaskUpdated().equalsIgnoreCase("1")) {
                    RequestParams params = new RequestParams();
                    params.put("intTaskDocumentNo", items.getIntTaskDocumentNo());
                    ApiCallWithToken(getTaskviewed, ApiCall.Get_TASK_VIEWED, params);
                    TaskItems tItem = mRealm.where(TaskItems.class).equalTo("intTaskDocumentNo", items.getIntTaskDocumentNo()).findFirst();
                    mRealm.beginTransaction();
                    items.setIsTaskUpdated("0");
                    tItem.setIsTaskUpdated("0");
                    mRealm.copyToRealmOrUpdate(items);
                    mRealm.copyToRealmOrUpdate(tItem);
                    mRealm.commitTransaction();
                }
            }
        }

        if (!TextUtils.isEmpty(items.getIntProgress())) {
            tvProgress.setVisibility(View.VISIBLE);
            tvProgress.setText("( " + items.getIntProgress() + " % )");
        } else {
            tvProgress.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(items.getStrSubCategoryName())) {
            tvHeaderType.setText(items.getStrCategoryName() + " > " + items.getStrSubCategoryName());
        } else {
            tvHeaderType.setText(items.getStrCategoryName());
        }
        if (items.getTaskComments() != null && items.getTaskComments().size() != 0) {
            commentDataArrayList.clear();
            commentMainList.clear();
            commentMainList.addAll(items.getTaskComments().sort("addTime", Sort.DESCENDING));
            if (commentMainList.size() > 5) {
                for (int i = 0; i < 5; i++) {
                    commentDataArrayList.add(commentMainList.get(i));
                }
                viewMoreComments.setVisibility(View.VISIBLE);
            } else {
                viewMoreComments.setVisibility(View.GONE);
                commentDataArrayList.addAll(commentMainList);
            }
        }
        if (items.getSubTasksData() != null && items.getSubTasksData().size() != 0) {
            subTaskDataArrayList.clear();
            subTaskMainList.clear();
            subTaskMainList.addAll(items.getSubTasksData().sort("addTime", Sort.ASCENDING));
            if (subTaskMainList.size() > 5) {
                for (int i = 0; i < 5; i++) {
                    subTaskDataArrayList.add(subTaskMainList.get(i));
                }
                viewMore.setVisibility(View.VISIBLE);
            } else {
                viewMore.setVisibility(View.GONE);
                subTaskDataArrayList.addAll(subTaskMainList);
            }
        }
//        linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        linearLayoutManager1 = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);

        linearLayoutManager3 = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
//        attachmentView.setLayoutManager(linearLayoutManager);
        commentsView.setLayoutManager(linearLayoutManager1);
        subTasksView.setLayoutManager(linearLayoutManager3);
//        adapter = new AttachmentAdapter(context, attachmentItemArrayList, false);
        commentsAdapter = new CommentsAdapter(context, commentDataArrayList);
        commentsAdapter.setOnClickListner(onClickListner1);
        subTasksAdapter = new SubTasksAdapter(context, subTaskDataArrayList);
        subTasksAdapter.setOnClickListner(onClickListner);

//        attachmentView.setAdapter(adapter);
        commentsView.setAdapter(commentsAdapter);
        subTasksView.setAdapter(subTasksAdapter);
        if (subTaskDataArrayList.size() == 0) {
            subTasksLayout.setVisibility(View.GONE);
        } else {
            subTasksLayout.setVisibility(View.VISIBLE);
        }

        Log.e("getCreateUser", items.getCreateUser() + "...");
        if (items.getCreateUser() != null) {

            if (items.getCreateUser().getImg() != null) {
                if (!TextUtils.isEmpty(items.getCreateUser().getImg().getPath())) {
                    Picasso.get().load(BASE_URL + items.getCreateUser().getImg().getPath()).placeholder(R.drawable.ic_error_male).error(R.drawable.ic_error_male).into(prof_img);
                }
            }
            if (!TextUtils.isEmpty(items.getCreateUser().getStrFirstName()) || !TextUtils.isEmpty(items.getCreateUser().getStrLastName())) {
                String names = "";
                if (items.getUsers().size() > 1) {
                    names = "( + " + (items.getUsers().size() - 1) + " More)";
                }
                if (items.getCreateUser().get_id().equals(mUserData.getIntUserId())) {
                    name.setText("Me " + names);
                } else
                    name.setText(items.getCreateUser().getStrFirstName() + " " + items.getCreateUser().getStrLastName() + names);
            }
        }
        if (commentDataArrayList.size() != 0) {
            noOfComments.setText(" " + String.valueOf(commentDataArrayList.size()));
        } else {
            noOfComments.setVisibility(View.GONE);
        }
//        if (items.getCreatedDate() != 0) {

        if (!TextUtils.isEmpty(items.getCreatedDate())) {
            tvDate.setText("Created on " + convertDate(items.getCreatedDate()));
//            tvDate.setText("Created on " + getMillsToDate(items.getCreatedDate()));
        }
        if (!TextUtils.isEmpty(items.getTaskStatus())) {
            taskStatus.setText(items.getTaskStatus());
            setStatusBKG(items.getTaskStatus());
        } else {
            taskStatus.setVisibility(View.GONE);
        }

        if (items.getDatStartToDueDate().size() != 0) {
            tvDuration.setVisibility(View.VISIBLE);
            tvDash.setVisibility(View.VISIBLE);
            tvStartDate.setVisibility(View.VISIBLE);
            tvEndDate.setVisibility(View.VISIBLE);
            tvStartDate.setText(getMillsToDate(items.getDatStartToDueDate().get(0).getStartDate()));
            tvEndDate.setText(getMillsToDate(items.getDatStartToDueDate().get(0).getDueDate()));
            long duration = getDaysBetweenDates(getMillsToDate(items.getDatStartToDueDate().get(0).getStartDate()), getMillsToDate(items.getDatStartToDueDate().get(0).getDueDate()));

            tvDuration.setText(duration == 1 ? (" " + duration + " day") : (" " + duration + " days"));

        } else if (!TextUtils.isEmpty(items.getIntHour()) && !TextUtils.isEmpty(items.getIntMinut())) {
            String duration = (items.getIntMinut().equalsIgnoreCase("1") ? (items.getIntHour() + " hour ") : (items.getIntHour() + " hours ")) +
                    (items.getIntHour().equalsIgnoreCase("1") ? (items.getIntMinut() + " minute") : (items.getIntMinut() + " minutes"));

            tvDuration.setText(duration);
            tvStartDate.setVisibility(View.GONE);
            tvEndDate.setVisibility(View.GONE);
            tvDash.setVisibility(View.GONE);

        } else {
            tvDash.setVisibility(View.GONE);
            tvDuration.setVisibility(View.GONE);
            tvStartDate.setVisibility(View.GONE);
            tvEndDate.setVisibility(View.GONE);
        }


        String prior = items.getStrPriorityValue();

        if (!TextUtils.isEmpty(prior)) {
            tvAddPrior.setVisibility(View.GONE);
            mPriorityLayout.setVisibility(View.VISIBLE);
            switch (prior) {
                case "1":
                    tvPriorType.setText("Low Priority");
                    tvPriorType.setTextColor(context.getResources().getColor(R.color.blue));
                    dot.setColorFilter(ContextCompat.getColor(context, R.color.blue), android.graphics.PorterDuff.Mode.MULTIPLY);
                    break;
                case "2":
                    tvPriorType.setText("Medium Priority");
                    tvPriorType.setTextColor(context.getResources().getColor(R.color.orange));
                    dot.setColorFilter(ContextCompat.getColor(context, R.color.orange), android.graphics.PorterDuff.Mode.MULTIPLY);
                    break;
                case "3":
                    tvPriorType.setText("High Priority");
                    tvPriorType.setTextColor(context.getResources().getColor(R.color.red));
                    dot.setColorFilter(ContextCompat.getColor(context, R.color.red), android.graphics.PorterDuff.Mode.MULTIPLY);
                    break;
                case "4":
                    tvPriorType.setText("Done");
                    tvPriorType.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    dot.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), android.graphics.PorterDuff.Mode.MULTIPLY);
                    break;
            }
        } else {
            if (items.getIntCreateUserId().equalsIgnoreCase(mUserData.getIntUserId())) {
                tvAddPrior.setVisibility(View.VISIBLE);
            } else {
                tvAddPrior.setVisibility(View.GONE);
            }
            mPriorityLayout.setVisibility(View.GONE);
        }

        tvTitle.setText(items.getStrDescription());

        mSend.setOnClickListener(v -> {
            if (!isNetworkAvailable()) {
                showToast(context.getString(R.string.noConnection));
                return;
            }
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mSend.getWindowToken(), 0);
            String txt = mCommentEdtTxt.getText().toString().trim();
            if (!TextUtils.isEmpty(txt)) {
                sendComment(txt);
            } else {
                showToast("Please write comment.");
            }
        });


        if (items.getIsClosed() == 1) {
            addCommentsView.setVisibility(View.GONE);
            mAddSubTasks.setVisibility(View.GONE);
            update.setVisibility(View.GONE);
        } else {
            addCommentsView.setVisibility(View.VISIBLE);
            if (items.getIntCreateUserId().equalsIgnoreCase(mUserData.getIntUserId())) {
                mAddSubTasks.setVisibility(View.VISIBLE);
                update.setVisibility(View.VISIBLE);
            } else {
                mAddSubTasks.setVisibility(View.GONE);
                update.setVisibility(View.GONE);
            }
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public String convertDate(String dat) {
        Log.e("convertDate", "...." + dat);
        Date inputDate = null;
        DateFormat gmtTimeFormat = null, gmtFormat = null;
        android.icu.text.SimpleDateFormat inputPattern = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            inputPattern = new android.icu.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

            try {
                inputDate = inputPattern.parse(dat);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            gmtFormat = new android.icu.text.SimpleDateFormat("MMM dd");//, h:mm a
            gmtTimeFormat = new android.icu.text.SimpleDateFormat("h:mm a");
            gmtFormat.setTimeZone(TimeZone.getTimeZone("IST"));
            gmtTimeFormat.setTimeZone(TimeZone.getTimeZone("IST"));
        }

        return gmtFormat.format(inputDate) + " " + gmtTimeFormat.format(inputDate);
    }

    @SuppressLint("RestrictedApi")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.layout_detailspage_fragment, container, false);
        ButterKnife.bind(this, rootView);

        mEelViewUtils = new EELViewUtils(rl, ns);
        context = getActivity();
        mRealm = Realm.getDefaultInstance();
        mUserData = getLoginData();
        task_Id = String.valueOf(getArguments().getString("_id"));
        isFromNotification = getArguments().getBoolean("isFromNotification", false);
        if (isNetworkAvailable()) {
            if (!TextUtils.isEmpty(task_Id)) {
                getData(task_Id);
            }
        } else {
            if (!TextUtils.isEmpty(task_Id)) {
                items = mRealm.where(TaskDetailItem.class).equalTo("intTaskDocumentNo", Integer.valueOf(task_Id)).findFirst();
                setData();
            }
        }
        mAddSubTasks.setOnClickListener(view -> {
            showSubTaskAddDialog();
        });
        tvPriorType.setOnClickListener(view -> {
            if (items.getIntCreateUserId().equalsIgnoreCase(mUserData.getIntUserId())) {
                if (items.getIsClosed() == 0) {
                    AddPrior();
                }
            }
        });
        tvAddPrior.setOnClickListener(view -> {
            if (items.getIntCreateUserId().equalsIgnoreCase(mUserData.getIntUserId())) {
                if (items.getIsClosed() == 0) {
                    AddPrior();
                }
            }
        });
        mCmntImgFile.setOnClickListener(v -> {
            if (!isNetworkAvailable()) {
                Toast.makeText(context, context.getString(R.string.noConnection), Toast.LENGTH_SHORT).show();
                return;
            }
            if (mayRequest()) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, 5555);
            }
        });
        mCmntFile.setOnClickListener(v -> {
            if (!isNetworkAvailable()) {
                Toast.makeText(context, context.getString(R.string.noConnection), Toast.LENGTH_SHORT).show();
                return;
            }
            if (mayRequest()) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("application/pdf");
                startActivityForResult(intent, 5252);
            }
        });
        taskStatus.setOnClickListener(v -> {
            Log.e("taskStatus", items.getIntCreateUserId() + "...." + mUserData.getIntUserId());//000000013905710a216f49c4....000000013905710a216f49c5
//            if (items.getIntCreateUserId().equalsIgnoreCase(mUserData.getIntUserId())) {
            if (!items.getTaskStatus().equalsIgnoreCase("Closed")) {
                if (!isNetworkAvailable()) {
                    Toast.makeText(context, context.getString(R.string.noConnection), Toast.LENGTH_SHORT).show();
                    return;
                }
                showStatusDialog(items.getTaskStatus());
            }
//            }
        });

        update.setOnClickListener(v -> {
            if (items.getIsClosed() == 0) {
                Intent i = new Intent(getActivity(), EditTaskActivity.class);
                i.putExtra("_id", items.getIntTaskDocumentNo());
                getActivity().startActivityForResult(i, 4521);
            }
        });
        root.getViewTreeObserver().addOnGlobalLayoutListener(keyboardLayoutListener);

        viewMore.setOnClickListener(v -> {
            isViewSubTask = true;
            viewMore.setVisibility(View.GONE);
            for (int i = 5; i < subTaskMainList.size(); i++) {
                subTaskDataArrayList.add(subTaskMainList.get(i));
            }
            subTasksAdapter.notifyDataSetChanged();
        });
        viewMoreComments.setOnClickListener(v -> {
            isViewCmnts = true;
            viewMoreComments.setVisibility(View.GONE);
            for (int i = 5; i < commentMainList.size(); i++) {
                commentDataArrayList.add(commentMainList.get(i));
            }
            commentsAdapter.notifyDataSetChanged();
        });
        IntentFilter filter = new IntentFilter();
        filter.addAction(BROADCAST_ACTION);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        getActivity().registerReceiver(notificationBroadcaster, filter);
        return rootView;
    }

    @Override
    public void OnError(JSONObject object, ApiCall type) {
        try {
            if (type == ApiCall.ADD_COMMENT) {
                showToast(object.getString("message"));
            }
            if (type == ApiCall.GET_TASK_DATA) {
                showToast(object.getString("message"));
            }
            if (type == ApiCall.ADD_SUBTASK_DATA) {

                vLoading.setVisibility(View.GONE);
                hideProgressDialog();
                showToast(object.getString("message"));
            }
            if (type == ApiCall.REMOVE_SUB_TASK_DATA) {
                subTaskId = "";
                vLoading.setVisibility(View.GONE);
                hideProgressDialog();
                showToast(object.getString("message"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnResponce(JSONObject object, ApiCall type) {
        hideProgressDialog();
        vLoading.setVisibility(View.GONE);
        try {
            Gson gson = new GsonBuilder().create();
            if (type == ApiCall.ADD_COMMENT) {
                CommentData commentData = gson.fromJson(object.getJSONObject("data").getJSONObject("commentData").toString(), CommentData.class);
                if (commentData != null) {
//                    commentMainList.add(0, commentData);
//                    commentDataArrayList.add(0, commentData);
//                    if (commentMainList.size() > 5) {
//                        if (!isViewCmnts) {
//                            viewMoreComments.setVisibility(View.VISIBLE);
//                            commentDataArrayList.remove(commentDataArrayList.size() - 1);
//                        } else {
//                            viewMoreComments.setVisibility(View.GONE);
//                        }
//                    } else {
//                        viewMoreComments.setVisibility(View.GONE);
//                    }
                    mRealm.beginTransaction();
//                    TaskDetailItem items = mRealm.where(TaskDetailItem.class).equalTo("_id", this.items.get_id()).findFirst();
                    items.getTaskComments().add(commentData);
                    mRealm.commitTransaction();

                    if (items.getTaskComments() != null && items.getTaskComments().size() != 0) {
                        commentDataArrayList.clear();
                        commentMainList.clear();
                        commentMainList.addAll(items.getTaskComments().sort("addTime", Sort.DESCENDING));
                        if (commentMainList.size() > 5) {
                            for (int i = 0; i < 5; i++) {
                                commentDataArrayList.add(commentMainList.get(i));
                            }
                            viewMoreComments.setVisibility(View.VISIBLE);
                        } else {
                            viewMoreComments.setVisibility(View.GONE);
                            commentDataArrayList.addAll(commentMainList);
                        }
                    }
                    commentsAdapter.notifyDataSetChanged();
                    DetailsTabActivity.isUpdated = true;
                    isImage = false;
                    isFile = false;
                    mainPath = "";
                    cmnt_file_name.setText(mainPath);
                }
                mCommentEdtTxt.setText("");
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
            if (type == ApiCall.ADD_REPLY) {
                mRealm.beginTransaction();
                Replies replies = gson.fromJson(object.getJSONObject("data").getJSONObject("commentData").toString(), Replies.class);
                int size = commentDataArrayList.get(mComPos).getReplies().size();
                TaskDetailItem items = mRealm.where(TaskDetailItem.class).equalTo("_id", this.items.get_id()).findFirst();
                items.getTaskComments().get(mComPos).getReplies().add(replies);
                int newSize = commentDataArrayList.get(mComPos).getReplies().size();
                if (size == newSize) {
                    commentDataArrayList.get(mComPos).getReplies().add(replies);
                }
                mRealm.commitTransaction();
                commentsAdapter.notifyItemChanged(mComPos);
                DetailsTabActivity.isUpdated = true;
                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
            if (type == ApiCall.GET_TASK_DATA) {
                mEelViewUtils.showContentViewColorPrimary();
                TaskDetailItem items = gson.fromJson(object.getJSONObject("data").getJSONObject("taskData").toString(), TaskDetailItem.class);
                items.setDetailAvailable(true);
                mRealm.beginTransaction();
                mRealm.copyToRealmOrUpdate(items);
                mRealm.commitTransaction();
                this.items = mRealm.where(TaskDetailItem.class).equalTo("_id", items.get_id()).findFirst();
                if (isUpdating) {
                    isUpdating = false;
                    updateData();
                } else {
                    setData();
                }
            }

            if (type == ApiCall.CHANGE_STATUS) {
                DetailsTabActivity.isUpdated = true;
                taskStatus.setText(task_status);
                setStatusBKG(task_status);
                mRealm.beginTransaction();
                TaskItems item = mRealm.where(TaskItems.class).equalTo("intTaskDocumentNo", items.getIntTaskDocumentNo()).findFirst();
                item.setTaskStatus(task_status);
                items.setTaskStatus(task_status);
                if (task_status.equalsIgnoreCase("Closed")) {
                    item.setIsClosed(1);
                    items.setIsClosed(1);
                }
                mRealm.commitTransaction();
                if (task_status.equalsIgnoreCase("Running")) {
                    if (!isNetworkAvailable()) {
                        Toast.makeText(context, context.getString(R.string.noConnection), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    startWork();
                } else if (task_status.equalsIgnoreCase("Pause")) {
                    if (!isNetworkAvailable()) {
                        Toast.makeText(context, context.getString(R.string.noConnection), Toast.LENGTH_SHORT).show();
                        return;
                    }
                    endWork();
                }
            }

            if (type == ApiCall.START_TASK) {
                Duration duration = gson.fromJson(object.getJSONObject("data").getJSONObject("durationData").toString(), Duration.class);
                mRealm.beginTransaction();
                TaskItems item1 = mRealm.where(TaskItems.class).equalTo("isRunning", 1).findFirst();
                TaskItems item2 = mRealm.where(TaskItems.class).equalTo("intTaskDocumentNo",
                        items.getIntTaskDocumentNo()).findFirst();
                if (item1 != null) {
                    item1.getTaskDurationsData().get(item1.getTaskDurationsData().size() - 1).setEndTime(System.currentTimeMillis());
                    item1.setIsRunning(0);
                }
                item2.getTaskDurationsData().add(0, duration);
                item2.setIsRunning(1);
                mRealm.commitTransaction();

                mRealm.beginTransaction();
                items.getTaskDurationsData().add(0, duration);
                items.setIsRunning(1);
                mRealm.commitTransaction();
            }
            if (type == ApiCall.END_TASK) {
                mRealm.beginTransaction();
                items.setIsClosed(1);
                TaskItems item = mRealm.where(TaskItems.class).equalTo("intTaskDocumentNo", items.getIntTaskDocumentNo()).findFirst();
                item.setIsClosed(1);
                mRealm.commitTransaction();
                showToast(object.getString("message"));
            }

            if (type == ApiCall.UPDATE_PRIORITY) {
                DetailsTabActivity.isUpdated = true;
                tvAddPrior.setVisibility(View.GONE);
                mPriorityLayout.setVisibility(View.VISIBLE);
                switch (tv_prior_type) {
                    case "1":
                        tvPriorType.setText("Low Priority");
                        tvPriorType.setTextColor(context.getResources().getColor(R.color.blue));
                        dot.setColorFilter(ContextCompat.getColor(context, R.color.blue), android.graphics.PorterDuff.Mode.MULTIPLY);
                        break;
                    case "2":
                        tvPriorType.setText("Medium Priority");
                        tvPriorType.setTextColor(context.getResources().getColor(R.color.orange));
                        dot.setColorFilter(ContextCompat.getColor(context, R.color.orange), android.graphics.PorterDuff.Mode.MULTIPLY);
                        break;
                    case "3":
                        tvPriorType.setText("High Priority");
                        tvPriorType.setTextColor(context.getResources().getColor(R.color.red));
                        dot.setColorFilter(ContextCompat.getColor(context, R.color.red), android.graphics.PorterDuff.Mode.MULTIPLY);
                        break;
                    case "4":
                        tvPriorType.setText("Done");
                        tvPriorType.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                        dot.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), android.graphics.PorterDuff.Mode.MULTIPLY);
                        break;
                }
                mRealm.beginTransaction();
                items.setStrPriorityValue(tv_prior_type);
                TaskItems item = mRealm.where(TaskItems.class).equalTo("intTaskDocumentNo", items.getIntTaskDocumentNo()).findFirst();
                item.setStrPriorityValue(tv_prior_type);
                mRealm.commitTransaction();
            }

            if (type == ApiCall.ADD_SUBTASK_DATA) {
                SubTaskData data = gson.fromJson(object.getJSONObject("data").getJSONObject("subTaskData").toString(), SubTaskData.class);
                mRealm.beginTransaction();
                this.items.getSubTasksData().add(data);
                mRealm.commitTransaction();
                subTaskDataArrayList.clear();
                subTaskMainList.clear();
                subTaskMainList.addAll(items.getSubTasksData().sort("addTime", Sort.ASCENDING));
                if (subTaskMainList.size() > 5) {
                    for (int i = 0; i < 5; i++) {
                        subTaskDataArrayList.add(subTaskMainList.get(i));
                    }
                    viewMore.setVisibility(View.VISIBLE);
                } else {
                    viewMore.setVisibility(View.GONE);
                    subTaskDataArrayList.addAll(subTaskMainList);
                }
                subTasksAdapter.notifyDataSetChanged();
                if (subTaskDataArrayList.size() == 0) {
                    subTasksLayout.setVisibility(View.GONE);
                } else {
                    subTasksLayout.setVisibility(View.VISIBLE);
                }
            }
            if (type == ApiCall.REMOVE_SUB_TASK_DATA) {
                SubTaskData data = mRealm.where(SubTaskData.class).equalTo("_id", subTaskId).findFirst();
                mRealm.beginTransaction();
                if (data != null) {
                    data.deleteFromRealm();
                }
                mRealm.commitTransaction();
                subTaskDataArrayList.clear();
                subTaskMainList.clear();
                subTaskMainList.addAll(items.getSubTasksData().sort("addTime", Sort.ASCENDING));
                if (subTaskMainList.size() > 5) {
                    for (int i = 0; i < 5; i++) {
                        subTaskDataArrayList.add(subTaskMainList.get(i));
                    }
                    viewMore.setVisibility(View.VISIBLE);
                } else {
                    viewMore.setVisibility(View.GONE);
                    subTaskDataArrayList.addAll(subTaskMainList);
                }
                subTasksAdapter.notifyDataSetChanged();
                if (subTaskDataArrayList.size() == 0) {
                    subTasksLayout.setVisibility(View.GONE);
                } else {
                    subTasksLayout.setVisibility(View.VISIBLE);
                }
                subTaskId = "";
            }
            if (type == ApiCall.CHANGE_PROGRESS_STATUS) {

                String val = object.getJSONObject("data").getString("intProgress");
                if (val.length() != 0) {
                    tvProgress.setVisibility(View.VISIBLE);
                    tvProgress.setText("( " + val + " % )");
                    mRealm.beginTransaction();
                    items.setIntProgress(val);
                    mRealm.copyToRealmOrUpdate(items);
                    mRealm.commitTransaction();
                }
            }
        } catch (JSONException e) {
            mCommentEdtTxt.setText("");
            e.printStackTrace();
        }
    }

    CommentsAdapter.OnClickListner onClickListner1 = position -> {
        if (items.getIsClosed() == 0) {
            mComPos = position;
            showAddCommentReplyDialog(commentDataArrayList.get(position));
        }
    };

    public void sendComment(String comment) {
        RequestParams params = new RequestParams();
        params.put("intTaskDocumentNo", items.getIntTaskDocumentNo());
        params.put("intUserId", mUserData.getIntUserId());
        params.put("comment", comment);

        params.put("intTaskDocumentNo", items.getIntTaskDocumentNo());
        if (isImage || isFile) {
            if (isImage) {
                params.put("fileType", "IMAGE");
            } else if (isFile) {
                params.put("fileType", "DOC");
            } else {
                showToast("Not valid data!");
                return;
            }
            if (TextUtils.isEmpty(mainPath)) {
                showToast("Not valid path!");
                return;
            }
            File file = new File(mainPath);
            if (file.exists()) {
                try {
                    params.put("file", file);
                } catch (FileNotFoundException e) {
                    showToast("Not valid path!");
                    e.printStackTrace();
                    return;
                }
            } else {
                showToast("Not valid path!");
                return;
            }
        }

        vLoading.setVisibility(View.VISIBLE);
//        showProgressDialog("Please wait...");
        ApiCallWithToken(addComment, ApiCall.ADD_COMMENT, params);
    }

    public long getDaysBetweenDates(String start, String end) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
        Date startDate, endDate;
        long numberOfDays = 0;
        try {
            startDate = dateFormat.parse(start);
            endDate = dateFormat.parse(end);
            numberOfDays = getUnitBetweenDates(startDate, endDate, TimeUnit.DAYS);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return numberOfDays;
    }

    private long getUnitBetweenDates(Date startDate, Date endDate, TimeUnit unit) {
        long timeDiff = endDate.getTime() - startDate.getTime();
        return unit.convert(timeDiff, TimeUnit.MILLISECONDS);
    }

    public void getData(String id) {
        RequestParams params = new RequestParams();
        params.put("intTaskDocumentNo", id);

        vLoading.setVisibility(View.VISIBLE);
//        showProgressDialog("Please wait...");
        mEelViewUtils.showLoadingView();
        Log.e("getData", getTaskData + "..." + params);
        ApiCallWithToken(getTaskData, ApiCall.GET_TASK_DATA, params);
    }


    public void showAddCommentReplyDialog(final CommentData item) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_add_cmnt_replies);
        CustomEditText mDesc = dialog.findViewById(R.id.commentEdtTxt);
        CustomTextView btOk = dialog.findViewById(R.id.submit_sub_task);
        CustomTextView close = dialog.findViewById(R.id.close);


        btOk.setOnClickListener(v -> {
            String desc = mDesc.getText().toString().trim();
            if (TextUtils.isEmpty(desc)) {
                showToast("Please enter Reply");
                return;
            }

            RequestParams params = new RequestParams();
            params.put("intTaskDocumentNo", items.getIntTaskDocumentNo());
            params.put("intUserId", getLoginData().getIntUserId());
            params.put("comment", desc);
            params.put("intCommentId", item.get_id());

            vLoading.setVisibility(View.VISIBLE);
//            showProgressDialog("Please wait...");
            ApiCallWithToken(add_reply, ApiCall.ADD_REPLY, params);

            dialog.dismiss();
        });
        close.setOnClickListener(v -> {
            dialog.dismiss();
        });

        dialog.show();
    }


    public void showSubTaskAddDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dialog_add_sub_task);
//        CustomEditText mTitle = dialog.findViewById(R.id.sub_task_title);
        CustomEditText mDesc = dialog.findViewById(R.id.desc);
        CustomTextView btOk = dialog.findViewById(R.id.submit_sub_task);
        CustomTextView close = dialog.findViewById(R.id.close);

        btOk.setOnClickListener(v -> {
//            String title = mTitle.getText().toString().trim();
            String desc = mDesc.getText().toString().trim();
//            if (TextUtils.isEmpty(title)) {
//                showToast("Please enter Title");
//                return;
//            }
            if (TextUtils.isEmpty(desc)) {
                showToast("Please enter Description");
                return;
            }

            RequestParams params = new RequestParams();
            params.put("intTaskDocumentNo", items.getIntTaskDocumentNo());
            params.put("intUserId", mUserData.getIntUserId());
//            params.put("title", title);
            params.put("description", desc);
//            showProgressDialog("Please wait...");

            vLoading.setVisibility(View.VISIBLE);
            ApiCallWithToken(addSubTaskData, ApiCall.ADD_SUBTASK_DATA, params);

            dialog.dismiss();
        });
        close.setOnClickListener(v -> {
            dialog.dismiss();
        });
        dialog.show();
    }


    public void updateData() {
        if (!items.getIntCreateUserId().equalsIgnoreCase(mUserData.getIntUserId())) {
            if (!TextUtils.isEmpty(items.getIsTaskUpdated())) {
                if (items.getIsTaskUpdated().equalsIgnoreCase("1")) {
                    RequestParams params = new RequestParams();
                    params.put("intTaskDocumentNo", items.getIntTaskDocumentNo());
                    ApiCallWithToken(getTaskviewed, ApiCall.Get_TASK_VIEWED, params);
                    TaskItems tItem = mRealm.where(TaskItems.class).equalTo("intTaskDocumentNo", items.getIntTaskDocumentNo()).findFirst();
                    mRealm.beginTransaction();
                    items.setIsTaskUpdated("0");
                    tItem.setIsTaskUpdated("0");
                    mRealm.copyToRealmOrUpdate(items);
                    mRealm.copyToRealmOrUpdate(tItem);
                    mRealm.commitTransaction();
                }
            }
        }
        if (mUpdateType.equalsIgnoreCase("task_add_comment") || mUpdateType.equalsIgnoreCase("task_add_reply")) {
            if (items.getTaskComments() != null && items.getTaskComments().size() != 0) {
                commentDataArrayList.clear();
                commentMainList.clear();
                commentMainList.addAll(items.getTaskComments().sort("addTime", Sort.DESCENDING));
                if (isViewCmnts) {
                    viewMoreComments.setVisibility(View.GONE);
                    commentDataArrayList.addAll(commentMainList);
                } else {
                    if (commentMainList.size() > 5) {
                        for (int i = 0; i < 5; i++) {
                            commentDataArrayList.add(commentMainList.get(i));
                        }
                        viewMoreComments.setVisibility(View.VISIBLE);
                    } else {
                        viewMoreComments.setVisibility(View.GONE);
                        commentDataArrayList.addAll(commentMainList);
                    }
                }
                commentsAdapter.notifyDataSetChanged();
            }
        } else if (mUpdateType.equalsIgnoreCase("add_sub_task") || mUpdateType.equalsIgnoreCase("remove_sub_task")) {
            subTaskDataArrayList.clear();
            subTaskMainList.clear();
            if (items.getSubTasksData() != null && items.getSubTasksData().size() != 0) {
                subTaskMainList.addAll(items.getSubTasksData().sort("addTime", Sort.ASCENDING));
                if (isViewSubTask) {
                    viewMore.setVisibility(View.GONE);
                    subTaskDataArrayList.addAll(subTaskMainList);
                } else {
                    if (subTaskMainList.size() > 5) {
                        for (int i = 0; i < 5; i++) {
                            subTaskDataArrayList.add(subTaskMainList.get(i));
                        }
                        viewMore.setVisibility(View.VISIBLE);
                    } else {
                        viewMore.setVisibility(View.GONE);
                        subTaskDataArrayList.addAll(subTaskMainList);
                    }
                }
                if (subTaskDataArrayList.size() == 0) {
                    subTasksLayout.setVisibility(View.GONE);
                } else {
                    subTasksLayout.setVisibility(View.VISIBLE);
                }
            }
            subTasksAdapter.notifyDataSetChanged();
        }
    }

    private void setStatusBKG(String status) {
        GradientDrawable bgShape = (GradientDrawable) taskStatus.getBackground();
        switch (status) {
            case "Pending":
                taskStatus.setTextColor(ContextCompat.getColor(context, R.color.sub_text_color));
                bgShape.setColor(ContextCompat.getColor(context, R.color.white));
                bgShape.setStroke(1, ContextCompat.getColor(context, R.color.sub_text_color));
                break;
            case "Running":
                taskStatus.setTextColor(ContextCompat.getColor(context, R.color.white));
                bgShape.setColor(ContextCompat.getColor(context, R.color.color_running));
                bgShape.setStroke(1, ContextCompat.getColor(context, R.color.transparent));
                break;
            case "Pause":
                taskStatus.setTextColor(ContextCompat.getColor(context, R.color.white));
                bgShape.setColor(ContextCompat.getColor(context, R.color.color_pending));
                bgShape.setStroke(1, ContextCompat.getColor(context, R.color.transparent));
                break;
            case "Due":
                taskStatus.setTextColor(ContextCompat.getColor(context, R.color.white));
                bgShape.setColor(ContextCompat.getColor(context, R.color.color_due));
                bgShape.setStroke(1, ContextCompat.getColor(context, R.color.transparent));
                break;
            case "Completed":
                taskStatus.setTextColor(ContextCompat.getColor(context, R.color.white));
                bgShape.setColor(ContextCompat.getColor(context, R.color.color_completed));
                bgShape.setStroke(1, ContextCompat.getColor(context, R.color.transparent));
                break;
            case "Closed":
                taskStatus.setTextColor(ContextCompat.getColor(context, R.color.white));
                bgShape.setColor(ContextCompat.getColor(context, R.color.color_closed));
                bgShape.setStroke(1, ContextCompat.getColor(context, R.color.transparent));
                break;
        }
    }

    private void showStatusDialog(String status) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dilogue_filter_type);

        CustomTextView textView = dialog.findViewById(R.id.title);
        textView.setText("Change Status");
        RecyclerView recyclerView = dialog.findViewById(R.id.recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        ArrayList<FilterType> filterTypes = new ArrayList<>();

        if (!items.getIntCreateUserId().equalsIgnoreCase(mUserData.getIntUserId())) {

            FilterType filterType = new FilterType();
            filterType.setName("Pending");
            filterType.setKey("Pending");
            filterTypes.add(filterType);

            FilterType filterType1 = new FilterType();
            filterType1.setName("Running");
            filterType1.setKey("Running");
            filterTypes.add(filterType1);

            FilterType filterType2 = new FilterType();
            filterType2.setName("Pause");
            filterType2.setKey("Pause");
            filterTypes.add(filterType2);

            FilterType filterType5 = new FilterType();
            filterType5.setName("Completed");
            filterType5.setKey("Completed");
            filterTypes.add(filterType5);

            FilterType filterType6 = new FilterType();
            filterType6.setName("Progress");
            filterType6.setKey("Progress");
            filterTypes.add(filterType6);

        } else {
            FilterType filterType3 = new FilterType();
            filterType3.setName("Due");
            filterType3.setKey("Due");
            filterTypes.add(filterType3);

            FilterType filterType4 = new FilterType();
            filterType4.setName("Closed");
            filterType4.setKey("Closed");
            filterTypes.add(filterType4);
        }


        if (!TextUtils.isEmpty(status)) {
            for (int i = 0; i < filterTypes.size(); i++) {
                if (filterTypes.get(i).getKey().equalsIgnoreCase(status)) {
                    filterTypes.get(i).setSelected(true);
                }
            }
        }

        FilterTypeAdapter adapter = new FilterTypeAdapter(context, filterTypes);
        recyclerView.setAdapter(adapter);
        adapter.setOnClickListner(position -> {
            for (int i = 0; i < filterTypes.size(); i++) {
                filterTypes.get(i).setSelected(false);
            }
            filterTypes.get(position).setSelected(true);
            String prior = filterTypes.get(position).getKey();
            adapter.notifyDataSetChanged();

            if (!TextUtils.isEmpty(prior)) {
                if (prior.equalsIgnoreCase("Closed")) {
                    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    alertDialogBuilder.setMessage("Are you sure , you want to Close this Task?");
                    alertDialogBuilder.setPositiveButton("Ok", (dialog1, which) -> {
                        dialog1.dismiss();
                        dialog.dismiss();
                        RequestParams params = new RequestParams();
                        params.put("intTaskDocumentNo", items.getIntTaskDocumentNo());
                        params.put("intUserId", mUserData.getIntUserId());
                        params.put("taskStatus", prior);
//                        showProgressDialog("Please wait...");
                        vLoading.setVisibility(View.VISIBLE);
                        task_status = prior;
                        ApiCallWithToken(changeStatus, ApiCall.CHANGE_STATUS, params);
                    });
                    alertDialogBuilder.setNegativeButton("Cancel", (d, w) -> {
                        d.dismiss();
                    });
                    AlertDialog dialog1 = alertDialogBuilder.create();
                    dialog1.show();
                } else if (prior.equalsIgnoreCase("Progress")) {

                    dialog.dismiss();
                    final Dialog dialogProgress = new Dialog(context);
                    dialogProgress.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialogProgress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    dialogProgress.setCancelable(true);
                    dialogProgress.setCanceledOnTouchOutside(true);
                    dialogProgress.setContentView(R.layout.dialog_progress_type);

                    CustomTextView textViewProgress = dialogProgress.findViewById(R.id.title);
                    textViewProgress.setText("Progress");
                    int per = 0;
                    if (!TextUtils.isEmpty(items.getIntProgress())) {
                        per = Integer.parseInt(items.getIntProgress());
                    }
                    IndicatorSeekBar listenerSeekBar = dialogProgress.findViewById(R.id.listener);
                    listenerSeekBar.setProgress(per);

                    listenerSeekBar.setOnSeekChangeListener(new OnSeekChangeListener() {
                        @Override
                        public void onSeeking(SeekParams seekParams) {//after changed
                            tvProgress.setVisibility(View.VISIBLE);
                            tvProgress.setText("( " + seekParams.progress + " % )");
                            RequestParams params = new RequestParams();
                            params.put("intTaskDocumentNo", items.getIntTaskDocumentNo());
                            params.put("intUserId", mUserData.getIntUserId());
                            params.put("intProgress", seekParams.progress);
                            vLoading.setVisibility(View.VISIBLE);
//                            task_status = prior;
                            ApiCallWithToken(changeProgressStatus, ApiCall.CHANGE_PROGRESS_STATUS, params);

                        }

                        @Override
                        public void onStartTrackingTouch(IndicatorSeekBar seekBar) {//initally

                        }

                        @Override
                        public void onStopTrackingTouch(IndicatorSeekBar seekBar) {//after changed

                        }

                    });
                    dialogProgress.show();

                } else {
                    dialog.dismiss();
                    RequestParams params = new RequestParams();
                    params.put("intTaskDocumentNo", items.getIntTaskDocumentNo());
                    params.put("intUserId", mUserData.getIntUserId());
                    params.put("taskStatus", prior);
                    vLoading.setVisibility(View.VISIBLE);
//                    showProgressDialog("Please wait...");
                    task_status = prior;
                    ApiCallWithToken(changeStatus, ApiCall.CHANGE_STATUS, params);
                }
            }
        });
        dialog.show();
    }

    private void startWork() {
        RequestParams params = new RequestParams();
        params.put("intTaskDocumentNo", items.getIntTaskDocumentNo());
        params.put("intUserId", mUserData.getIntUserId());
        ApiCallWithToken(startTask, ApiCall.START_TASK, params);
    }

    private void endWork() {
        RequestParams params = new RequestParams();
        String _id = "";
        boolean isF = false;
        for (int i = 0; i < items.getTaskDurationsData().size() && !isF; i++) {
            if (items.getTaskDurationsData().get(i).getEndTime() == 0) {
                _id = items.getTaskDurationsData().get(i).get_id();
                isF = true;
            }
        }
        params.put("durationId", _id);
        ApiCallWithToken(endTask, ApiCall.END_TASK, params);
    }

    BroadcastReceiver notificationBroadcaster = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mUpdateType = intent.getStringExtra("type");
            if (mUpdateType.equalsIgnoreCase("task_add_comment")
                    || mUpdateType.equalsIgnoreCase("task_add_reply")
                    || mUpdateType.equalsIgnoreCase("add_sub_task")
                    || mUpdateType.equalsIgnoreCase("remove_sub_task")) {
                isUpdating = true;
                getData(task_Id);
            }

        }
    };
}
