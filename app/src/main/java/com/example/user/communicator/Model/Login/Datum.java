
package com.example.user.communicator.Model.Login;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("intUserId")
    @Expose
    private String intUserId;
    @SerializedName("intUserLevelId")
    @Expose
    private String intUserLevelId;
    @SerializedName("intLevelTypeId")
    @Expose
    private String intLevelTypeId;
    @SerializedName("strFirstName")
    @Expose
    private String strFirstName;
    @SerializedName("strLastName")
    @Expose
    private String strLastName;
    @SerializedName("intMemberTypeId")
    @Expose
    private String intMemberTypeId;
    @SerializedName("strEmail")
    @Expose
    private String strEmail;
    @SerializedName("strPhoneNo")
    @Expose
    private String strPhoneNo;
    @SerializedName("intMemberNumber")
    @Expose
    private Integer intMemberNumber;
    @SerializedName("arryObjModuleUser")
    @Expose
    private List<ArryObjModuleUser> arryObjModuleUser = null;
    @SerializedName("arryObjModuleLevel")
    @Expose
    private List<ArryObjModuleLevel> arryObjModuleLevel = null;
    @SerializedName("strAddress")
    @Expose
    private String strAddress;
    @SerializedName("strGender")
    @Expose
    private String strGender;
    @SerializedName("datBirthDate")
    @Expose
    private String datBirthDate;
    @SerializedName("strPincode")
    @Expose
    private Object strPincode;
    @SerializedName("strPlace")
    @Expose
    private Object strPlace;
    @SerializedName("arryselectedOrganisationAllItems")
    @Expose
    private List<ArryselectedOrganisationAllItem> arryselectedOrganisationAllItems = null;
    @SerializedName("arryselectedProjectAllItems")
    @Expose
    private List<ArryselectedProjectAllItem> arryselectedProjectAllItems = null;

    public String getIntUserId() {
        return intUserId;
    }

    public void setIntUserId(String intUserId) {
        this.intUserId = intUserId;
    }

    public String getIntUserLevelId() {
        return intUserLevelId;
    }

    public void setIntUserLevelId(String intUserLevelId) {
        this.intUserLevelId = intUserLevelId;
    }

    public String getIntLevelTypeId() {
        return intLevelTypeId;
    }

    public void setIntLevelTypeId(String intLevelTypeId) {
        this.intLevelTypeId = intLevelTypeId;
    }

    public String getStrFirstName() {
        return strFirstName;
    }

    public void setStrFirstName(String strFirstName) {
        this.strFirstName = strFirstName;
    }

    public String getStrLastName() {
        return strLastName;
    }

    public void setStrLastName(String strLastName) {
        this.strLastName = strLastName;
    }

    public String getIntMemberTypeId() {
        return intMemberTypeId;
    }

    public void setIntMemberTypeId(String intMemberTypeId) {
        this.intMemberTypeId = intMemberTypeId;
    }

    public String getStrEmail() {
        return strEmail;
    }

    public void setStrEmail(String strEmail) {
        this.strEmail = strEmail;
    }

    public String getStrPhoneNo() {
        return strPhoneNo;
    }

    public void setStrPhoneNo(String strPhoneNo) {
        this.strPhoneNo = strPhoneNo;
    }

    public Integer getIntMemberNumber() {
        return intMemberNumber;
    }

    public void setIntMemberNumber(Integer intMemberNumber) {
        this.intMemberNumber = intMemberNumber;
    }

    public List<ArryObjModuleUser> getArryObjModuleUser() {
        return arryObjModuleUser;
    }

    public void setArryObjModuleUser(List<ArryObjModuleUser> arryObjModuleUser) {
        this.arryObjModuleUser = arryObjModuleUser;
    }

    public List<ArryObjModuleLevel> getArryObjModuleLevel() {
        return arryObjModuleLevel;
    }

    public void setArryObjModuleLevel(List<ArryObjModuleLevel> arryObjModuleLevel) {
        this.arryObjModuleLevel = arryObjModuleLevel;
    }

    public String getStrAddress() {
        return strAddress;
    }

    public void setStrAddress(String strAddress) {
        this.strAddress = strAddress;
    }

    public String getStrGender() {
        return strGender;
    }

    public void setStrGender(String strGender) {
        this.strGender = strGender;
    }

    public String getDatBirthDate() {
        return datBirthDate;
    }

    public void setDatBirthDate(String datBirthDate) {
        this.datBirthDate = datBirthDate;
    }

    public Object getStrPincode() {
        return strPincode;
    }

    public void setStrPincode(Object strPincode) {
        this.strPincode = strPincode;
    }

    public Object getStrPlace() {
        return strPlace;
    }

    public void setStrPlace(Object strPlace) {
        this.strPlace = strPlace;
    }

    public List<ArryselectedOrganisationAllItem> getArryselectedOrganisationAllItems() {
        return arryselectedOrganisationAllItems;
    }

    public void setArryselectedOrganisationAllItems(List<ArryselectedOrganisationAllItem> arryselectedOrganisationAllItems) {
        this.arryselectedOrganisationAllItems = arryselectedOrganisationAllItems;
    }

    public List<ArryselectedProjectAllItem> getArryselectedProjectAllItems() {
        return arryselectedProjectAllItems;
    }

    public void setArryselectedProjectAllItems(List<ArryselectedProjectAllItem> arryselectedProjectAllItems) {
        this.arryselectedProjectAllItems = arryselectedProjectAllItems;
    }

}
