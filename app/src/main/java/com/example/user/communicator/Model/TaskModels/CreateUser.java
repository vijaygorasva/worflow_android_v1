package com.example.user.communicator.Model.TaskModels;


import io.realm.RealmObject;

public class CreateUser extends RealmObject {

    private String _id;
    private String strFirstName;
    private String strLastName;
    private ImageData img;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getStrFirstName() {
        return strFirstName;
    }

    public void setStrFirstName(String strFirstName) {
        this.strFirstName = strFirstName;
    }

    public String getStrLastName() {
        return strLastName;
    }

    public void setStrLastName(String strLastName) {
        this.strLastName = strLastName;
    }

    public ImageData getImg() {
        return img;
    }

    public void setImg(ImageData img) {
        this.img = img;
    }


}
