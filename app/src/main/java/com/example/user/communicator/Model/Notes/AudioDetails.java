package com.example.user.communicator.Model.Notes;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class AudioDetails extends RealmObject implements Parcelable {

    @SerializedName("intAudioId")
    @Expose
    private String intAudioId;
    @SerializedName("fieldname")
    @Expose
    private String fieldname;
    @SerializedName("originalname")
    @Expose
    private String originalname;
    @SerializedName("encoding")
    @Expose
    private String encoding;

    @SerializedName("mimetype")
    @Expose
    private String mimetype;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("filename")
    @Expose
    private String filename;
    @SerializedName("path")
    @Expose
    private String path;
    @SerializedName("size")
    @Expose
    private Integer size;

    public static final Creator<AudioDetails> CREATOR = new Creator<AudioDetails>() {
        @Override
        public AudioDetails createFromParcel(Parcel in) {
            return new AudioDetails(in);
        }

        @Override
        public AudioDetails[] newArray(int size) {
            return new AudioDetails[size];
        }
    };

    protected AudioDetails(Parcel in) {
        intAudioId = in.readString();
        fieldname = in.readString();
        originalname = in.readString();
        encoding = in.readString();
        mimetype = in.readString();
        destination = in.readString();
        filename = in.readString();
        path = in.readString();
        strDuration = in.readString();
//        size = in.readInt();
    }

    public String getIntAudioId() {
        return intAudioId;
    }

    public String getFieldname() {
        return fieldname;
    }

    public void setFieldname(String fieldname) {
        this.fieldname = fieldname;
    }

    public String getOriginalname() {
        return originalname;
    }

    public void setOriginalname(String originalname) {
        this.originalname = originalname;
    }

    public String getEncoding() {
        return encoding;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    @SerializedName("strDuration")
    @Expose
    private String strDuration;

    public AudioDetails() {

    }

    public void setIntAudioId(String intAudioId) {
        this.intAudioId = intAudioId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getStrDuration() {
        return strDuration;
    }

    public void setStrDuration(String strDuration) {
        this.strDuration = strDuration;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(intAudioId);
        parcel.writeString(fieldname);
        parcel.writeString(originalname);
        parcel.writeString(encoding);
        parcel.writeString(mimetype);
        parcel.writeString(destination);
        parcel.writeString(filename);
        parcel.writeString(path);
        parcel.writeString(strDuration);
//        parcel.writeInt(size);

    }

}
