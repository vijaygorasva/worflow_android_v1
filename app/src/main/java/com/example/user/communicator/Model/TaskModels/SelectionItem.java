package com.example.user.communicator.Model.TaskModels;


import io.realm.RealmObject;

public class SelectionItem extends RealmObject {


    private String id;
    private String itemName;
    private String imgPic;
    private String category;
    private boolean isSelected = false;


    public String getUrl() {
        return imgPic;
    }

    public void setUrl(String url) {
        this.imgPic = url;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
