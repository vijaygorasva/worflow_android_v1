package com.example.user.communicator.Dialog;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.communicator.Activity.PlannerModule.BaseActivity;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.Colleborate.ColleborateDatum;
import com.example.user.communicator.Model.Filter;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.Login.Login;
import com.example.user.communicator.Model.Login.ObjModuledetailLevel;
import com.example.user.communicator.Model.TaskModels.TaskItems;
import com.example.user.communicator.R;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import io.realm.RealmResults;

public class FilterDialog extends DialogFragment {

//    public final String URL_ROOT = "http://52.66.249.75:9999/api/";

    public final String URL_ImageUpload_SERVER = "http://52.66.249.75:9999/uploads/";
    public final String URL_ROOT_SERVER = "http://52.66.249.75:9999/api/";//52.66.249.75
    public final String BASE_URL_SEVER = "http://52.66.249.75:9999/";

    public final String URL_ImageUpload_LOCAL = "http://52.66.249.75:9999/uploads/";
    public final String URL_ROOT_local = "http://52.66.249.75:9999/api/";//52.66.249.75
    public final String BASE_URL_local = "http://52.66.249.75:9999/";

    public final String URL_ROOT_TEST = "http://13.232.37.104:9999/api/";//http://13.232.37.104:9999
    public final String URL_ImageUpload_ROOT_TEST = "http://52.66.249.75:9999/uploads/";
    public final String BASE_URL_TEST = "http://192.168.50.100:9999/";

    public final String URL_ROOT = URL_ROOT_TEST;
    public final String URL_ImageUpload_ROOT = URL_ImageUpload_ROOT_TEST;
    public final String BASE_URL = BASE_URL_TEST;

    public final String urlColList = URL_ROOT + "connection/getconnectionuser";
    public final String urlFilter = URL_ROOT + "task/get_filter_task_search_details";//api/task/get_filter_task_search_details
    Callback callback;
    @BindView(R.id.btnApply)
    AppCompatButton btnApply;
    @BindView(R.id.llMain)
    LinearLayout llMain;

    @BindView(R.id.llName)
    LinearLayout llName;
    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.ivArrowName)
    ImageView ivArrowName;
    @BindView(R.id.rlNameLists)
    LinearLayout rlNameLists;
    RealmResults<Filter> filterRealmResults;
    String strUserId;
    Realm mRealm;
    ArrayList<TaskItems> taskItems = new ArrayList<>();
    ArrayList<ColleborateDatum> colleborateData = new ArrayList<>();
    Datum mUserDetails;

    String categoryTypeID, categoryID, subCategoryID;
    @BindView(R.id.rlName)
    RelativeLayout rlName;
    @BindView(R.id.tvStatus)
    TextView tvStatus;
    @BindView(R.id.ivArrowStatus)
    ImageView ivArrowStatus;
    @BindView(R.id.rlStatus)
    RelativeLayout rlStatus;
    @BindView(R.id.rlStatusLists)
    LinearLayout rlStatusLists;
    @BindView(R.id.llStatus)
    LinearLayout llStatus;
    @BindView(R.id.tvPriority)
    TextView tvPriority;
    @BindView(R.id.ivArrowPriority)
    ImageView ivArrowPriority;
    @BindView(R.id.rlPriority)
    RelativeLayout rlPriority;
    @BindView(R.id.rlPriorityLists)
    LinearLayout rlPriorityLists;
    @BindView(R.id.llPriority)
    LinearLayout llPriority;
    @BindView(R.id.tvSortBy)
    TextView tvSortBy;
    @BindView(R.id.ivArrowSortBy)
    ImageView ivArrowSortBy;
    @BindView(R.id.rlSortBy)
    RelativeLayout rlSortBy;
    @BindView(R.id.rlSortByLists)
    LinearLayout rlSortByLists;
    @BindView(R.id.llSortBy)
    LinearLayout llSortBy;

    String moduleType, prioVal, sortByVal;
    String nameIDSelected, nameSelected, sortBySelected, prioritySelected, statusSelected;
    Boolean isExpanded = false;

    public FilterDialog() {

    }

    public static FilterDialog newIntance(String type, String categoryTypeID, String categoryID, String subCategoryID, Callback callback) {
        Bundle args = new Bundle();
        args.putString("moduleType", type);
        args.putString("categoryTypeID", categoryTypeID);
        args.putString("categoryID", categoryID);
        args.putString("subCategoryID", subCategoryID);

        FilterDialog dialog = new FilterDialog();
        dialog.setArguments(args);
        dialog.setCallback(callback);
        return dialog;
        //getArguments().getString("_id")
    }


    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, getTheme());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_filter, container, false);
        ButterKnife.bind(this, view);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.border_roundeed);
        moduleType = getArguments().getString("moduleType");
        categoryTypeID = getArguments().getString("categoryTypeID");
        categoryTypeID = getArguments().getString("categoryTypeID");
        categoryID = getArguments().getString("categoryID");
        subCategoryID = getArguments().getString("subCategoryID");
        /*
        Bundle args = getArguments();
        displayWidth = args.getInt("displayWidth");
         */
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int displayWidth = displayMetrics.widthPixels;
        int displayHeight = displayMetrics.heightPixels;
        view.setMinimumWidth((int) (displayWidth * 0.8f));
//        view.setMinimumHeight((int) (displayHeight * 0.4f));
//        llMain.setLayoutParams(MATCH_PARENT, MATCH_PARENT);
//        llMain.set

//        view.setMinimumHeight((int) (displayMetrics.heightPixels * 50) / 100);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mRealm = Realm.getDefaultInstance();

        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("Login", getActivity().MODE_PRIVATE);
        String data = sharedpreferences.getString("UserData", null);
        Gson gson = new Gson();
        mUserDetails = gson.fromJson(data, Datum.class);
        strUserId = mUserDetails.getIntUserId();

        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        filterRealmResults = realm.where(Filter.class).findAll();
        realm.commitTransaction();
        if (filterRealmResults.size() != 0) {//to show the filter selected already
            for (int i = 0; i < filterRealmResults.size(); i++) {
                if (!TextUtils.isEmpty(filterRealmResults.get(i).getNameSelected())) {
                    tvName.setVisibility(View.VISIBLE);
                    etName.setVisibility(View.GONE);
                    tvName.setText(filterRealmResults.get(i).getNameSelected());

                    nameSelected = filterRealmResults.get(i).getNameSelected();
                    nameIDSelected = filterRealmResults.get(i).getNameIDSelected();
                }
                if (!TextUtils.isEmpty(filterRealmResults.get(i).getPrioritySelected())) {
                    tvPriority.setText("Priority" + " - " + filterRealmResults.get(i).getPrioritySelected());
                    prioritySelected = filterRealmResults.get(i).getPrioritySelected();
                }
                if (!TextUtils.isEmpty(filterRealmResults.get(i).getSortBySelected())) {
                    tvSortBy.setText("Sort By" + " - " + filterRealmResults.get(i).getSortBySelected());
                    sortBySelected = filterRealmResults.get(i).getSortBySelected();
                }
                if (!TextUtils.isEmpty(filterRealmResults.get(i).getStatusSelected())) {
                    tvStatus.setText("Status" + " - " + filterRealmResults.get(i).getStatusSelected());
                    statusSelected = filterRealmResults.get(i).getStatusSelected();
                }
            }
        }

        getColleborateData();

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getFilterUsers();
                dismiss();
            }
        });
/*
2019-07-12 12:34:59.025 8413-8413/com.example.user.communicator E/Apply_parms:
 http://52.66.249.75:9999/api/task/get_filter_task_search_details.....
 intUserId=000000013905710a216f49c5
 &strSubCategoryId=
 &strCategoryId=0
 &intProgress=
 &intTaskDocumentNo=
 &intAssignedUserId=000000013905710a216f49c5
 &strTaskStatus=Pending
 &strCategoryTypeId=
 &strSearchText=

 */
    }

    @OnClick({R.id.llName, R.id.llStatus, R.id.llPriority, R.id.llSortBy})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llName:

                tvName.setText("");
                tvName.setVisibility(View.GONE);
                etName.setVisibility(View.VISIBLE);
                etName.setText("");
                if (isExpanded) {

                    rlSortByLists.removeAllViews();
                    rlStatusLists.removeAllViews();
                    rlSortByLists.removeAllViews();

                    rlSortByLists.setVisibility(View.GONE);
                    rlStatusLists.setVisibility(View.GONE);
                    rlPriorityLists.setVisibility(View.GONE);

                    ivArrowSortBy.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_keyboard_arrow_right));
                    ivArrowStatus.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_keyboard_arrow_right));
                    ivArrowPriority.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_keyboard_arrow_right));
                }

                ivArrowName.setVisibility(View.VISIBLE);
                ivArrowName.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down_grey));

                if (rlNameLists.getChildCount() > 0) {//works when  multiple time clicked same llStatus
                    rlNameLists.removeAllViews();
                    rlNameLists.setVisibility(View.GONE);
                    ivArrowName.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_keyboard_arrow_right));
                } else {
                    isExpanded = true;
                    etName.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            rlNameLists.setVisibility(View.VISIBLE);
                            rlNameLists.removeAllViews();
                            ArrayList<ColleborateDatum> datas = filter(etName.getText().toString());
                            for (int j = 0; j < datas.size(); j++) {
                                final String lastName, firstName, fullName;
                                LinearLayout linearLayout = (LinearLayout) View.inflate(getActivity(), R.layout.add_sub_cat, null);
                                CustomTextView tvSubCat = linearLayout.findViewById(R.id.tvSubCat);
                                LinearLayout llSub = linearLayout.findViewById(R.id.llSub);
                                String name = datas.get(j).getItemName();
                                final String[] split = name.split(":");
                                firstName = split[0];
                                if (split.length == 2) {
                                    lastName = split[1];
                                    fullName = firstName + " " + lastName;
                                } else {
                                    fullName = firstName;
                                }
                                ((CustomTextView) linearLayout.findViewById(R.id.tvSubCat)).setText(fullName);

                                int finalJ = j;
                                llSub.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        nameIDSelected = datas.get(finalJ).getId();
                                        nameSelected = fullName;
                                        rlNameLists.setVisibility(View.GONE);
                                        tvName.setVisibility(View.VISIBLE);
                                        etName.setVisibility(View.GONE);
                                        tvName.setText(fullName);
                                        ivArrowName.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_keyboard_arrow_right));
                                    }
                                });
                                rlNameLists.addView(linearLayout);
                            }
                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                        }
                    });
                }

                break;
            case R.id.llStatus:
                Toast.makeText(getActivity(), "Status..Count..." + rlStatusLists.getChildCount(), Toast.LENGTH_LONG).show();
                if (isExpanded) {

                    etName.setVisibility(View.GONE);
                    tvName.setVisibility(View.VISIBLE);

                    rlNameLists.removeAllViews();
                    rlPriorityLists.removeAllViews();
                    rlSortByLists.removeAllViews();

                    rlNameLists.setVisibility(View.GONE);
                    rlPriorityLists.setVisibility(View.GONE);
                    rlSortByLists.setVisibility(View.GONE);

                    ivArrowName.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_keyboard_arrow_right));
                    ivArrowPriority.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_keyboard_arrow_right));
                    ivArrowSortBy.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_keyboard_arrow_right));
                }
                if (rlStatusLists.getChildCount() > 0) {//works when  multiple time clicked same llStatus
                    rlStatusLists.removeAllViews();
                    rlStatusLists.setVisibility(View.GONE);
                    ivArrowStatus.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_keyboard_arrow_right));
                } else {
                    isExpanded = true;
                    ivArrowStatus.setVisibility(View.VISIBLE);
                    ivArrowStatus.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down_grey));
                    ArrayList<String> statusArray = new ArrayList<String>();
                    statusArray.add("Pending");
                    statusArray.add("Running");
                    statusArray.add("Pause");
                    statusArray.add("Due");
                    statusArray.add("Closed");
                    statusArray.add("Completed");

                    rlStatusLists.removeAllViews();
                    rlStatusLists.setVisibility(View.VISIBLE);
                    for (int j = 0; j < statusArray.size(); j++) {

                        LinearLayout linearLayout = (LinearLayout) View.inflate(getActivity(), R.layout.add_sub_cat, null);
                        CustomTextView tvSubCat = linearLayout.findViewById(R.id.tvSubCat);
                        LinearLayout llSub = linearLayout.findViewById(R.id.llSub);
                        ((CustomTextView) linearLayout.findViewById(R.id.tvSubCat)).setText(statusArray.get(j));
                        int finalJ = j;
                        llSub.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                tvStatus.setText("Status" + " - " + statusArray.get(finalJ));

                                statusSelected = statusArray.get(finalJ);
                                ivArrowStatus.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_keyboard_arrow_right));
                                rlStatusLists.setVisibility(View.GONE);
                            }
                        });
                        rlStatusLists.addView(linearLayout);
                    }
                }
                break;
            case R.id.llPriority:
                Toast.makeText(getActivity(), "Priority..Count..." + rlPriorityLists.getChildCount(), Toast.LENGTH_LONG).show();
                if (isExpanded) {
                    etName.setVisibility(View.GONE);
                    tvName.setVisibility(View.VISIBLE);

                    rlNameLists.removeAllViews();
                    rlStatusLists.removeAllViews();
                    rlSortByLists.removeAllViews();

                    rlNameLists.setVisibility(View.GONE);
                    rlStatusLists.setVisibility(View.GONE);
                    rlSortByLists.setVisibility(View.GONE);

                    ivArrowName.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_keyboard_arrow_right));
                    ivArrowStatus.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_keyboard_arrow_right));
                    ivArrowSortBy.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_keyboard_arrow_right));
                }
                if (rlPriorityLists.getChildCount() > 0) {//works when  multiple time clicked same llStatus
                    rlPriorityLists.removeAllViews();
                    rlPriorityLists.setVisibility(View.GONE);
                    ivArrowPriority.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_keyboard_arrow_right));
                } else {
                    isExpanded = true;
                    ivArrowPriority.setVisibility(View.VISIBLE);
                    ivArrowPriority.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down_grey));

                    ArrayList<String> priorityArray = new ArrayList<String>();
                    priorityArray.add("Low");
                    priorityArray.add("Medium");
                    priorityArray.add("High");

                    rlPriorityLists.removeAllViews();
                    rlPriorityLists.setVisibility(View.VISIBLE);
                    for (int j = 0; j < priorityArray.size(); j++) {

                        LinearLayout linearLayout = (LinearLayout) View.inflate(getActivity(), R.layout.add_sub_cat, null);
                        LinearLayout llSub = linearLayout.findViewById(R.id.llSub);
                        CustomTextView tvSubCat = linearLayout.findViewById(R.id.tvSubCat);
                        ((CustomTextView) linearLayout.findViewById(R.id.tvSubCat)).setText(priorityArray.get(j));
                        int finalJ = j;
                        llSub.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                tvPriority.setText("Priority" + " - " + priorityArray.get(finalJ));

                                prioritySelected = priorityArray.get(finalJ);
                                ivArrowPriority.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_keyboard_arrow_right));
                                rlPriorityLists.setVisibility(View.GONE);
                            }
                        });
                        rlPriorityLists.addView(linearLayout);
                    }
                }
                break;
            case R.id.llSortBy:
                Toast.makeText(getActivity(), "SortBy..Count..." + rlSortByLists.getChildCount(), Toast.LENGTH_LONG).show();
                if (isExpanded) {

                    etName.setVisibility(View.GONE);
                    tvName.setVisibility(View.VISIBLE);

                    rlNameLists.removeAllViews();
                    rlStatusLists.removeAllViews();
                    rlPriorityLists.removeAllViews();

                    rlNameLists.setVisibility(View.GONE);
                    rlStatusLists.setVisibility(View.GONE);
                    rlPriorityLists.setVisibility(View.GONE);

                    ivArrowName.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_keyboard_arrow_right));
                    ivArrowStatus.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_keyboard_arrow_right));
                    ivArrowPriority.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_keyboard_arrow_right));
                }
                if (rlSortByLists.getChildCount() > 0) {//works when  multiple time clicked same llStatus
                    rlSortByLists.removeAllViews();
                    rlSortByLists.setVisibility(View.GONE);
                    ivArrowSortBy.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_keyboard_arrow_right));
                } else {
                    isExpanded = true;
                    ivArrowSortBy.setVisibility(View.VISIBLE);
                    ivArrowSortBy.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down_grey));
                    ArrayList<String> sortByArray = new ArrayList<String>();
                    sortByArray.add("Newly Added");
                    sortByArray.add("Recently Updated");
                    sortByArray.add("Priority Low - High");
                    sortByArray.add("Priority High - Low");

                    rlSortByLists.removeAllViews();
                    rlSortByLists.setVisibility(View.VISIBLE);
                    for (int j = 0; j < sortByArray.size(); j++) {
                        LinearLayout linearLayout = (LinearLayout) View.inflate(getActivity(), R.layout.add_sub_cat, null);
                        LinearLayout llSub = linearLayout.findViewById(R.id.llSub);
                        CustomTextView tvSubCat = linearLayout.findViewById(R.id.tvSubCat);
                        ((CustomTextView) linearLayout.findViewById(R.id.tvSubCat)).setText(sortByArray.get(j));
                        int finalJ = j;
                        llSub.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                tvSortBy.setText("Sort By" + " - " + sortByArray.get(finalJ));

                                sortBySelected = sortByArray.get(finalJ);
                                ivArrowSortBy.setImageDrawable(ContextCompat.getDrawable(getActivity(), R.drawable.ic_keyboard_arrow_right));
                                rlSortByLists.setVisibility(View.GONE);
                            }
                        });
                        rlSortByLists.addView(linearLayout);
                    }
                }
                break;
        }
    }


    public ArrayList<ColleborateDatum> filter(String charText) {//ArrayList<ColleborateDatum> arrayList
        ArrayList<ColleborateDatum> mainDataList = new ArrayList<ColleborateDatum>();
        charText = charText.toLowerCase(Locale.getDefault());
        mainDataList.clear();

        if (charText.length() == 0) {
            mainDataList.clear();
        } else {
            for (ColleborateDatum wp : colleborateData) {

                if (!mainDataList.contains(wp)) {
                    if (wp.getItemName().toLowerCase(Locale.getDefault()).startsWith(charText)) {
                        mainDataList.add(wp);
                    }
                } else {
                    mainDataList.remove(wp);
                }
            }
        }
        return mainDataList;
    }

    /*
    {
	"intUserId":"000000014d70683a1645170b",
	"intAssignedUserId":"000000014d70683a1645170c",
	"strSearchText":"",
	"strCategoryTypeId":"",
	"strCategoryId":"",
	"strSubCategoryId":"",
	"strTaskStatus":"",
	"strPriority":"",  ...
	"intProgress":"0",
	"intTaskDocumentNo":"",
	"sortBy":"UPATE_TIME"  ....


	intUserId=000000013905710a216f49c5
	&intAssignedUserId=000000013905710a216f49c5
	&strSearchText=
	&strCategoryTypeId=
	&strCategoryId=0
	&strSubCategoryId=
	&strTaskStatus=Pending
	&intProgress=
	&intTaskDocumentNo=

}

     */

    public void getFilterUsers() {
        if (isNetworkAvailable()) {

            Filter filter = new Filter();
            filter.setNameSelected(nameSelected);
            filter.setNameIDSelected(nameIDSelected);
            filter.setStatusSelected(statusSelected);
            filter.setPrioritySelected(prioritySelected);
            filter.setSortBySelected(sortBySelected);

            Realm realm = Realm.getDefaultInstance();
            realm.beginTransaction();
            mRealm.copyToRealmOrUpdate(filter);
            realm.commitTransaction();

            if (!TextUtils.isEmpty(prioritySelected)) {

                switch (prioritySelected) {
                    case "Low":
                        prioVal = "1";
                        break;
                    case "Medium":
                        prioVal = "2";
                        break;
                    case "High":
                        prioVal = "3";
                        break;
                }
            }
            if (categoryID.equals("0")) {
                categoryID = "";
            }


            if (!TextUtils.isEmpty(sortBySelected)) {
                switch (sortBySelected) {
                    case "Newly Added":
                        sortByVal = "ADD_TIME";
                        break;
                    case "Recently Updated":
                        sortByVal = "UPATE_TIME";
                        break;
                    case "Priority Low - High":
                        sortByVal = "PRIORITY_LOW_TO_HIGH";
                        break;
                    case "Priority High - Low":
                        sortByVal = "PRIORITY_HIGH_TO_LOW";
                        break;
                }
            }
            RequestParams params = new RequestParams();

            params.put("intUserId", strUserId);

            params.put("intAssignedUserId", nameIDSelected);
            params.put("strSearchText", "");
            params.put("sortBy", sortByVal);
            params.put("strTaskStatus", statusSelected);
            params.put("strPriority", prioVal);

            params.put("strCategoryTypeId", categoryTypeID);
            params.put("strCategoryId", categoryID);
            params.put("strSubCategoryId", subCategoryID);

            params.put("intProgress", "");
            params.put("intTaskDocumentNo", "");
            params.put("strSearchType", moduleType);//ASSIGNED or MYTASK

            params.put("intProjectId", mUserDetails.getArryselectedProjectAllItems().get(0).getId());
            params.put("intOrganisationId", mUserDetails.getArryselectedOrganisationAllItems().get(0).getId());

            Log.e("Apply_parms", urlFilter + "....." + params);
            ApiCallWithToken(urlFilter, BaseActivity.ApiCall.TASKFILTER, params);


        }
    }

    public void getColleborateData() {
//        mRealm = Realm.getDefaultInstance();
//
//        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("Login", getActivity().MODE_PRIVATE);
//        String data = sharedpreferences.getString("UserData", null);
//        Gson gson = new Gson();
//        mUserDetails = gson.fromJson(data, com.example.user.communicator.Model.Login.Datum.class);
//        strUserId = mUserDetails.getIntUserId();

        String intModuleId = null;
        ArrayList<ObjModuledetailLevel> objModuledetailLevels = new ArrayList<ObjModuledetailLevel>(mUserDetails.getArryObjModuleLevel().get(0).getObjModuledetails());

        for (ObjModuledetailLevel objDetailLevel : objModuledetailLevels) {
            if (objDetailLevel.getStrModuleName().equals("PLANNERNOTE")) {
                intModuleId = objDetailLevel.getId();
            }
        }
        if (intModuleId != null) {

            if (isNetworkAvailable()) {
                RequestParams params = new RequestParams();
                params.put("intModuleId", intModuleId);
                params.put("intLevelTypeId", mUserDetails.getIntLevelTypeId());
                params.put("intMemberTypeId", mUserDetails.getIntMemberTypeId());

                ApiCallWithToken(urlColList, BaseActivity.ApiCall.COLLEBORATELIST, params);
            } else {
                colleborateData.clear();
                RealmResults<ColleborateDatum> itemsRealmResults = mRealm.where(ColleborateDatum.class).findAll();
                if (itemsRealmResults.size() != 0) {
                    colleborateData.addAll(itemsRealmResults);
//                    filter(arrayList, text);
                }
            }
        }
    }

    public void ApiCallWithToken(String url, final BaseActivity.ApiCall type,
                                 final RequestParams requestParams) {

        try {
            SharedPreferences sharedpreferences = getActivity().getSharedPreferences("Login", 0);
            String data = sharedpreferences.getString("UserData", null);
            String token = sharedpreferences.getString("Token", null);
            Gson gson = new Gson();
            Login login = gson.fromJson(data, Login.class);
            Datum userData = gson.fromJson(data, Datum.class);
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(30 * 1000);
            if (userData != null) {
                client.addHeader("Authorization", "Bearer " + token);
                requestParams.put("intProjectId", userData.getArryselectedProjectAllItems().get(0).getId());
                requestParams.put("intOrganisationId", userData.getArryselectedOrganisationAllItems().get(0).getId());
            }

            client.setURLEncodingEnabled(true);
            client.post(getActivity(), url, requestParams, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        if (response.getBoolean("success")) {
                            OnResponce(response, type);
                        } else {
                            OnError(response, type);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
//                    hideProgressDialog(context);
                    String errorType = throwable.getMessage();
                    if (errorType.equals("Read timed out")) {
                        showToast("Oops!!! Server Time-out.Please try again");
                    } else {
                        showToast("Oops!!! Please try again");
                        OnError(errorResponse, type);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                    hideProgressDialog(context.geta);
                    showToast("Oops!!! Please try again");
                }

            });

        } catch (Exception e) {
//            hideProgressDialog(context);
            e.printStackTrace();
        }

    }

    public void OnResponce(JSONObject data, BaseActivity.ApiCall type) {
        String s = data.toString();

        JSONArray array = null;
        try {
            if (type == BaseActivity.ApiCall.COLLEBORATELIST) {
                array = data.getJSONArray("data");
                if (array.length() != 0) {

                    Gson gson = new Gson();
                    colleborateData.clear();
                    mRealm.beginTransaction();
                    for (int i = 0; i < array.length(); i++) {
                        try {
                            JSONObject object = array.getJSONObject(i);
                            ColleborateDatum item = gson.fromJson(object.toString(), ColleborateDatum.class);
                            colleborateData.add(item);
                            mRealm.copyToRealmOrUpdate(item);
//                            filter(colleborateData, text);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    mRealm.commitTransaction();
                }
            } else if (type == BaseActivity.ApiCall.TASKFILTER) {
                Log.e("taskItems_call", data + "...");

                taskItems.clear();
                Gson gson = new Gson();
                array = data.getJSONObject("data").getJSONArray("tasksData");
                if (array.length() != 0) {
                    mRealm.beginTransaction();
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject object = array.getJSONObject(i);
                        TaskItems items = gson.fromJson(object.toString(), TaskItems.class);
                        Log.e("taskItems_call", new Gson().toJson(items) + "...");
                        items.setMyTask(true);
                        mRealm.copyToRealmOrUpdate(items);
                        taskItems.add(items);
                    }
                    mRealm.commitTransaction();
                }

//                if (taskItems.size() == 0) {
//                    Toast.makeText(getContext(),"No task found in this category",Toast.LENGTH_LONG).show();
//                }
//                else {
//                    taskListAdapter.setIsAssignedTask(false);
//                    taskListAdapter.notifyDataSetChanged();
//                }
                Log.e("taskItems_size", taskItems.size() + "..." + array.length());

                callback.setFilterData(taskItems);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void OnError(JSONObject object, BaseActivity.ApiCall type) {

        if (type == BaseActivity.ApiCall.COLLEBORATELIST) {

        }
        try {
            showToast(object.getString("message"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void showToast(String message) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View layouttoast = inflater.inflate(R.layout.toastcustom, (ViewGroup) getActivity().findViewById(R.id.toastcustom));
        ((TextView) layouttoast.findViewById(R.id.texttoast)).setText(message);

        Toast mytoast = new Toast(getActivity());
        mytoast.setView(layouttoast);
        mytoast.setGravity(Gravity.CENTER, 0, 0);
        mytoast.setDuration(Toast.LENGTH_LONG);
        mytoast.show();
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }


    public interface Callback {
        void setFilterData(ArrayList<TaskItems> taskItems);
    }

}
