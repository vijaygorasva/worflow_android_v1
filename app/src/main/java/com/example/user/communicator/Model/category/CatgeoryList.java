
package com.example.user.communicator.Model.category;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class CatgeoryList extends RealmObject implements Parcelable {


    public static final Parcelable.Creator<CatgeoryList> CREATOR = new Parcelable.Creator<CatgeoryList>() {
        @Override
        public CatgeoryList createFromParcel(Parcel in) {
            return new CatgeoryList(in);
        }

        @Override
        public CatgeoryList[] newArray(int size) {
            return new CatgeoryList[size];
        }
    };
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @PrimaryKey
    @SerializedName("position")
    @Expose
    private int position;
    @SerializedName("data")
    @Expose
    private RealmList<DatumCategory> data = null;

    protected CatgeoryList(Parcel in) {
        position = in.readInt();
        success = in.readByte() != 0;
//        intCaregoryTypeId = in.readString();
        message = in.readString();
        in.readTypedList(data, DatumCategory.CREATOR);
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CatgeoryList() {
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public RealmList<DatumCategory> getData() {
        return data;
    }

    public void setData(RealmList<DatumCategory> data) {
        this.data = data;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {

        parcel.writeInt(position);
//        parcel.writeString(intCaregoryTypeId);
        parcel.writeByte((byte) (success ? 1 : 0));
        parcel.writeString(message);
        parcel.writeTypedList(data);
    }

    @Override
    public int describeContents() {
        return 0;
    }

}
