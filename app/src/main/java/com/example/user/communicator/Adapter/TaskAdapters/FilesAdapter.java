package com.example.user.communicator.Adapter.TaskAdapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.user.communicator.Activity.TaskModule.ImageShowActivity;
import com.example.user.communicator.Activity.TaskModule.PDFViewerActivity;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.TaskModels.SelectionItems.FileData;
import com.example.user.communicator.R;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FilesAdapter extends RecyclerView.Adapter<FilesAdapter.ViewHolder> {

    private Context context;
    private ArrayList<FileData> taskItems;
    private OnClickListner onClick;
//    public final String BASE_URL = "http://52.66.249.75:9999/";

    public final String BASE_URL_SEVER = "http://52.66.249.75:9999/";
    public final String BASE_URL_local = "http://:9999/";
    public final String BASE_URL_TEST = "http://13.232.37.104:9999/";
    public final String BASE_URL = BASE_URL_TEST;
    private ArrayList<FileData> allTaskItems = new ArrayList<>();

    public FilesAdapter(Context context, ArrayList<FileData> taskItems) {
        this.taskItems = taskItems;
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_files, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(taskItems.get(position));
    }

    @Override
    public int getItemCount() {
        return taskItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setOnClickListner(OnClickListner onClickListner) {
        this.onClick = onClickListner;
    }

    public interface OnClickListner {
        void OnClick(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.ivImg)
        ImageView mImg;


        @BindView(R.id.tvDocName)
        CustomTextView tvDocName;

        @BindView(R.id.holder)
        RelativeLayout mHolder;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        public void bind(final FileData items) {

            if (!TextUtils.isEmpty(items.getOriginalName())) {
                tvDocName.setText(items.getOriginalName());
            }
            if (items.getFileType().equalsIgnoreCase("DOC")) {
                mImg.setImageResource(R.drawable.ic_pdf);
            } else if (items.getFileType().equalsIgnoreCase("IMAGE")) {
                mImg.setImageResource(R.drawable.ic_gallery);
            }

            mHolder.setOnClickListener(v -> {
                if (items.getFileType().equalsIgnoreCase("IMAGE")) {
                    if (!TextUtils.isEmpty(items.getFilePath())) {
                        Intent i = new Intent(context, ImageShowActivity.class);
                        i.putExtra("img_url", items.getFilePath());
                        context.startActivity(i);
                    }
                } else {
                    if (!TextUtils.isEmpty(items.getFilePath())) {
                        File imgFile = new File(items.getFilePath());
                        if (imgFile.exists()) {
                            Intent i = new Intent(context, PDFViewerActivity.class);
                            i.putExtra("path", items.getFilePath());
                            context.startActivity(i);
                        } else {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(BASE_URL + items.getFilePath()));
                            context.startActivity(browserIntent);
                        }
                    }
                }
            });

        }
    }
}