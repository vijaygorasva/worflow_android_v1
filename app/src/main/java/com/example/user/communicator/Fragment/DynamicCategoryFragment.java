package com.example.user.communicator.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.user.communicator.Adapter.PlannerAdapter.CategoryAdapter;
import com.example.user.communicator.CustomViews.CustomEditText;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.category.ArryCategoryDetail;
import com.example.user.communicator.Model.category.ArryObjStrDefaultSubCategory;
import com.example.user.communicator.R;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

public class DynamicCategoryFragment extends BasicFragment implements CategoryAdapter.Callback {
    @BindView(R.id.rvCategoryList)
    RecyclerView rvCategoryList;
    @BindView(R.id.llNodata)
    LinearLayout llNodata;
    @BindView(R.id.llAddNew)
    LinearLayout llAddNew;
    @BindView(R.id.ivAddMainCat)
    ImageView ivAddMainCat;
    @BindView(R.id.etCategory)
    CustomEditText etCategory;
    View view;
    int val;
    Boolean isFabActive;
    String tabCatID, tabName;
    Realm mRealm;
    String strUserId;
    Datum mUserDetails;
    OnCategorySelectedListener mListenerCat = (OnCategorySelectedListener) getActivity();
    ArrayList<ArryCategoryDetail> arryMainCategoryDetails = new ArrayList<>();
    ArrayList<ArryObjStrDefaultSubCategory> defaultSubCategories = new ArrayList<>();
    CategoryAdapter operationAdapter;


    public static DynamicCategoryFragment newInstance(Fragment activity, Boolean isFabActive, int position, String tabName, String tabCatID, ArrayList<ArryObjStrDefaultSubCategory> subCategory, ArrayList<ArryCategoryDetail> arryMainCategoryDetails) {

        DynamicCategoryFragment fragment = new DynamicCategoryFragment();
        Bundle args = new Bundle();
        args.putInt("someInt", position);
        args.putString("tabName", tabName);
        args.putBoolean("isFabActive", isFabActive);

        args.putString("tabCatID", tabCatID);
        args.putParcelableArrayList("mainCat", arryMainCategoryDetails);
        args.putParcelableArrayList("subCat", subCategory);
        fragment.setArguments(args);
        return fragment;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_catgeory_dynamiclist, container, false);
        ButterKnife.bind(this, view);
        SharedPreferences sharedpreferences = getContext().getApplicationContext().getSharedPreferences("Login", 0);
        String data = sharedpreferences.getString("UserData", null);
        Gson gson = new Gson();
        mUserDetails = gson.fromJson(data, Datum.class);
        strUserId = mUserDetails.getIntUserId();
        mRealm = Realm.getDefaultInstance();

        tabName = getArguments().getString("tabName");
        val = getArguments().getInt("someInt", 0);
        tabCatID = getArguments().getString("tabCatID");
        isFabActive = getArguments().getBoolean("isFabActive");
        arryMainCategoryDetails.addAll(getArguments().getParcelableArrayList("mainCat"));
        defaultSubCategories.addAll(getArguments().getParcelableArrayList("subCat"));

        if (arryMainCategoryDetails.size() > 0) {
            ArryCategoryDetail arryCategoryDetail = new ArryCategoryDetail();
            arryCategoryDetail.setStrCategoryName("All");
            arryMainCategoryDetails.add(0, arryCategoryDetail);
        }

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvCategoryList.setLayoutManager(linearLayoutManager);
        rvCategoryList.setHasFixedSize(true);

        operationAdapter = new CategoryAdapter(getActivity(), rvCategoryList, defaultSubCategories, arryMainCategoryDetails, this);
        rvCategoryList.setAdapter(operationAdapter);

        if (arryMainCategoryDetails.size() > 0) {

        } else {
            llNodata.setVisibility(View.VISIBLE);
        }

        ivAddMainCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llAddNew.setVisibility(View.VISIBLE);
                ivAddMainCat.setVisibility(View.GONE);
            }
        });

        etCategory.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                // Perform action on key press
                String val = etCategory.getText().toString();
                if (val.length() > 15) {
                    showToast("Category name should be within 15 characters");
                } else {
                    if (isNetworkAvailable()) {
                        saveMainCat(val);
                        etCategory.setText("");
                    } else {
                        showToast(getString(R.string.noConnection));
                    }
                }
                return true;
            }
            return false;
        });

        return view;
    }

    @Override
    public View provideYourFragmentView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    public void saveMainCat(String val) {

        if (isNetworkAvailable()) {
            RequestParams params = new RequestParams();
            params.put("strCategoryTypeId", tabCatID);
            params.put("strCategoryName", val);
            params.put("strCreateUserId", strUserId);
            params.put("intProjectId", mUserDetails.getArryselectedProjectAllItems().get(0).getId());
            params.put("intOrganisationId", mUserDetails.getArryselectedOrganisationAllItems().get(0).getId());
            /*

            params.put("strCategoryTypeId", strCategoryTypeId);
            params.put("strCategoryId", strCategoryId);
            params.put("strSubCategoryId", strSubCategoryId);
             */

            ApiCallWithToken(url_saveCategory, BasicFragment.ApiCall.SAVEMAINCATEGORY, params);

        } else {
            showToast(getString(R.string.noConnection));
        }

    }

    @Override
    public void getCatgeory(Context activity, String mainCatName, String mainCatID, String subCatID, String subCatName) {
        mListenerCat.onCategorySelected(isFabActive, tabName, tabCatID, mainCatName, mainCatID, subCatID, subCatName);
    }


    @Override
    public void onAttach(Context context) {

        if (getActivity() instanceof OnCategorySelectedListener) {
            mListenerCat = (OnCategorySelectedListener) getActivity();
        } else {
            throw new ClassCastException(getActivity().toString()
                    + getResources().getString(R.string.exception_message));
        }

        operationAdapter = new CategoryAdapter(getActivity(), rvCategoryList, defaultSubCategories, arryMainCategoryDetails, this);
        super.onAttach(context);
    }

    @Override
    public void OnResponce(JSONObject data, ApiCall type) {

        JSONArray array = null;
        Gson gson = new Gson();

        try {
            if (type == ApiCall.SAVEMAINCATEGORY) {
                hideProgressDialog(getActivity());
                array = data.getJSONArray("data");

                mRealm.beginTransaction();
                for (int i = 0; i < array.length(); i++) {
                    try {
                        llNodata.setVisibility(View.GONE);
                        JSONObject object = array.getJSONObject(i);
                        ArryCategoryDetail arryCategoryDetail = new ArryCategoryDetail();
                        arryCategoryDetail.setFkIntCategoryTypeId("fkIntCategoryTypeId");
                        arryCategoryDetail.setId(object.getString("_id"));
                        arryCategoryDetail.setStrCategoryName(object.getString("strCategoryName"));
                        arryCategoryDetail.setStrDefaultSub(object.getBoolean("strDefaultSub"));

                        if (operationAdapter == null){
                            operationAdapter = new CategoryAdapter(getActivity(), rvCategoryList, defaultSubCategories, arryMainCategoryDetails, this);
                            rvCategoryList.setAdapter(operationAdapter);
                        }
                        if (operationAdapter.getItemCount() > 0){
                            operationAdapter.insertCatItem(arryCategoryDetail, 1);
                        }else {
                            operationAdapter.insertCatItem(arryCategoryDetail, 0);
                        }
                        operationAdapter.notifyDataSetChanged();
                        mRealm.copyToRealmOrUpdate(arryCategoryDetail);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                mRealm.commitTransaction();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        super.OnResponce(data, type);
    }

    public interface OnCategorySelectedListener {
        void onCategorySelected(Boolean isFabActive, String tabName, String tabID, String mainCat, String mainCatID, String subCatID, String subCatName);
    }

}
