package com.example.user.communicator.Activity.PlannerModule;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.user.communicator.Adapter.PlannerAdapter.ViewPagerAdapter;
import com.example.user.communicator.ConnectivityReceiver;
import com.example.user.communicator.CustomViews.CustomEditText;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Fragment.DynamicCategoryFragment;
import com.example.user.communicator.Fragment.OperationsFragment;
import com.example.user.communicator.Fragment.PlannerFragment;
import com.example.user.communicator.Model.Notes.Item;
import com.example.user.communicator.Model.Reminder.Reminder;
import com.example.user.communicator.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

public class Main4Activity extends BaseActivity implements PlannerFragment.OnSearchSelectedListener, DynamicCategoryFragment.OnCategorySelectedListener {//}, ConnectivityReceiver.ConnectivityReceiverListener {


    public static final int REQUEST_CODE_ALARM = 0;
    public static String strCategoryTypeId,
            strCategoryId,
            strSubCategoryId;
    public static String strCategoryTypeName, strCategoryName, strSubCategoryName;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.llMain)
    RelativeLayout llMain;

    @BindView(R.id.llToolbar)
    LinearLayout llToolbar;

    @BindView(R.id.etSearch)
    CustomEditText etSearch;
    @BindView(R.id.etTitle)
    CustomEditText etTitle;

    @BindView(R.id.tvNetwrokConnetion)
    CustomTextView tvNetwrokConnetion;

    @BindView(R.id.ivClose)
    ImageView ivClose;

    Boolean isSelected = false;
    //Fragments
    PlannerFragment plannerFragment;
    OperationsFragment operationsFragment;
    InputMethodManager imm;
    ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
    //This is our tablayout
    private TabLayout tabLayout;
    //This is our viewPager
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);
        ButterKnife.bind(this);
        showSoftKeyboard(etSearch);
        toolbar.setTitle("");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//        imm = (InputMethodManager)Main4Activity.this. getSystemService(Activity.INPUT_METHOD_SERVICE);
//        imm.showSoftInput(etSearch, InputMethodManager.SHOW_IMPLICIT);
        setupViewPager(viewPager);
        llToolbar.setVisibility(View.VISIBLE);
        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                toolbar.setVisibility(View.GONE);
                viewPager.setCurrentItem(position, false);

//                tabLayout.setSelectedTabIndicatorColor(0xFFFFFFFF);
                if (position == 0) {
                    plannerFragment.getNewNoteList();
                } else {
                    operationsFragment.getAllSubCategory();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });

        if (isNetworkAvailable()) {
//            getReminders();
        } else {
            Realm mRealm = Realm.getDefaultInstance();
            RealmResults<Item> itemList = mRealm.where(Item.class).findAll();//.equalTo("intPlannerId",28)

            RealmResults<Reminder> apiPlannerList = mRealm.where(Reminder.class).findAll();//.equalTo("intId",28)
            Log.e("ALL_LISt", apiPlannerList.size() + "...alarm...." + itemList.size());
        }
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        tabLayout.setupWithViewPager(viewPager);

    }

    @Override
    public void onCategorySelected(Boolean isActive, String tabName, String tabID, String mainCat, String mainCatID, String subCatID, String subCatName) {

    }

    @Override
    public void onSearchSelected(Boolean isClicked) {
        if (isClicked) {

            etSearch.setFocusable(true);
            etSearch.setFocusableInTouchMode(true);
            etSearch.setCursorVisible(true);
            showToast("isClicked..." + isClicked);
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
            llToolbar.setVisibility(View.VISIBLE);
            toolbar.setVisibility(View.VISIBLE);
            etSearch.setVisibility(View.VISIBLE);
            etSearch.setClickable(true);

//            if (imm != null) {
//            imm.showSoftInput(etSearch, InputMethodManager.SHOW_FORCED);
//            }

            etSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    String val = etSearch.getText().toString();
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    String val = etSearch.getText().toString();
                    if (!TextUtils.isEmpty(val)) {
//                        plannerFragment.filter(val);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                    String val = etSearch.getText().toString();
                    if (!TextUtils.isEmpty(val)) {
                        plannerFragment.filter(val);
                    } else plannerFragment.filter("");
                }
            });

            ivClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    llToolbar.setVisibility(View.GONE);
                    toolbar.setVisibility(View.GONE);
                    etSearch.setText("");
                    plannerFragment.filter("");
                }
            });

        } else {
            llToolbar.setVisibility(View.GONE);
            toolbar.setVisibility(View.GONE);
            etSearch.setText("");
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//            if (imm != null) {
//                imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
//            }
        }

    }

    private void setupViewPager(ViewPager viewPager) {
//        etSearch.setFocusable(true);
//        etSearch.setCursorVisible(true);
//        etSearch.setClickable(true);

        toolbar.setVisibility(View.GONE);
        boolean isConnected = ConnectivityReceiver.isConnected();
        Bundle bundle = new Bundle();
        bundle.putBoolean("isConnected", isConnected);

        plannerFragment = new PlannerFragment();
        plannerFragment.putArguments(bundle);

        operationsFragment = new OperationsFragment();
        operationsFragment.putArguments(bundle);

        adapter.addFragment(plannerFragment, "Notes");
        adapter.addFragment(operationsFragment, "Operations");
//        plannerFragment.putArguments(bundle);

        viewPager.setAdapter(adapter);

    }
}
