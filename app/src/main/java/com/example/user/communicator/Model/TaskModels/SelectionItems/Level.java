package com.example.user.communicator.Model.TaskModels.SelectionItems;


public class Level {

    private String _id;
    private String strLevelValue;
    private String intLevelTypeId;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getStrLevelValue() {
        return strLevelValue;
    }

    public void setStrLevelValue(String strLevelValue) {
        this.strLevelValue = strLevelValue;
    }

    public String getIntLevelTypeId() {
        return intLevelTypeId;
    }

    public void setIntLevelTypeId(String intLevelTypeId) {
        this.intLevelTypeId = intLevelTypeId;
    }
}
