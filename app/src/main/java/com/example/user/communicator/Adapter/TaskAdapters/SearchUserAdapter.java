package com.example.user.communicator.Adapter.TaskAdapters;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.TaskModels.SelectionItem;
import com.example.user.communicator.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class SearchUserAdapter extends RecyclerView.Adapter<SearchUserAdapter.ViewHolder> {

    private Context context;
    private ArrayList<SelectionItem> taskItems;
    private OnClickListner onClick1;

    private ArrayList<SelectionItem> allTaskItems = new ArrayList<>();

    public SearchUserAdapter(Context context, ArrayList<SelectionItem> taskItems) {
        this.taskItems = taskItems;
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_user, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(taskItems.get(position));

    }

    @Override
    public int getItemCount() {
        return taskItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public void setAllTaskItems(ArrayList<SelectionItem> allTaskItems) {
        this.allTaskItems.clear();
        this.allTaskItems.addAll(allTaskItems);
    }

    public ArrayList<SelectionItem> getItems() {
        return allTaskItems;
    }

    public void filter(String date) {

        try {
            taskItems.clear();
            if (TextUtils.isEmpty(date)) {
                taskItems.addAll(allTaskItems);
            } else {
                for (SelectionItem item : allTaskItems) {
                    if (item.getItemName().toLowerCase().contains(date.toLowerCase())) {
                        taskItems.add(item);
                    }
                }
            }
            ((Activity) context).runOnUiThread(this::notifyDataSetChanged);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setOnClickListner(OnClickListner onClickListner) {
        this.onClick1 = onClickListner;
    }


    public interface OnClickListner {
        void OnClick1(int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img)
        ImageView mImg;

        @BindView(R.id.select)
        ImageView mSelect;

        @BindView(R.id.txtName)
        CustomTextView mTxtName;

        @BindView(R.id.txtDesignation)
        CustomTextView mTxtDesignation;

        @BindView(R.id.holder)
        RelativeLayout mHolder;

        @BindView(R.id.imgSelect)
        RelativeLayout mImgSelect;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        public void bind(final SelectionItem items) {
            final String lastName, firstName;
            String name = items.getItemName();
            final String[] split = name.split(":");
            firstName = split[0];
            if (split.length == 2) {
                lastName = split[1];
                mTxtName.setText(firstName + " " + lastName);
            } else {
                mTxtName.setText(firstName);
            }

            mTxtDesignation.setText(items.getCategory());
            if (!TextUtils.isEmpty(items.getUrl())) {
                mImg.setVisibility(View.VISIBLE);
                Picasso.get().load(items.getUrl()).placeholder(R.drawable.ic_placeholder).error(R.drawable.ic_placeholder).into(mImg);
            }

            if (items.isSelected()) {
                mImgSelect.setVisibility(View.VISIBLE);
            } else {
                mImgSelect.setVisibility(View.GONE);
            }

            mHolder.setOnClickListener(v -> {
                boolean isVisible = false;
                if (mImgSelect.getVisibility() == View.VISIBLE) {
                    isVisible = false;
                    mImgSelect.setVisibility(View.GONE);
                } else {
                    isVisible = true;
                    mImgSelect.setVisibility(View.VISIBLE);
                }

                boolean isF = false;
                for (int i = 0; i < allTaskItems.size() && !isF; i++) {
                    if (taskItems.get(getAdapterPosition()).getItemName().equalsIgnoreCase(allTaskItems.get(i).getItemName())) {
                        isF = true;
                        allTaskItems.get(i).setSelected(isVisible);
                    }
                }
                if (onClick1 != null) {
                    onClick1.OnClick1(getAdapterPosition());
                }
            });

        }
    }


}
