
package com.example.user.communicator.Model.Login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.annotations.PrimaryKey;

public class ArryObjModuleLevel {

    @PrimaryKey
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("intLevelTypeId")
    @Expose
    private String intLevelTypeId;
    @SerializedName("objModuledetails")
    @Expose
    private List<ObjModuledetailLevel> objModuledetails = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIntLevelTypeId() {
        return intLevelTypeId;
    }

    public void setIntLevelTypeId(String intLevelTypeId) {
        this.intLevelTypeId = intLevelTypeId;
    }

    public List<ObjModuledetailLevel> getObjModuledetails() {
        return objModuledetails;
    }

    public void setObjModuledetails(List<ObjModuledetailLevel> objModuledetails) {
        this.objModuledetails = objModuledetails;
    }

}
