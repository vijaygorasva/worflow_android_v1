package com.example.user.communicator.Model.Notes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class UndoCheckboxModel extends RealmObject {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("strNoteListValue")
    @Expose
    private String strNoteListValue;
    @SerializedName("strNoteListFlage")
    @Expose
    private Boolean strNoteListFlage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStrNoteListValue() {
        return strNoteListValue;
    }

    public void setStrNoteListValue(String strNoteListValue) {
        this.strNoteListValue = strNoteListValue;
    }

    public Boolean getStrNoteListFlage() {
        return strNoteListFlage;
    }

    public void setStrNoteListFlage(Boolean strNoteListFlage) {
        this.strNoteListFlage = strNoteListFlage;
    }


}
