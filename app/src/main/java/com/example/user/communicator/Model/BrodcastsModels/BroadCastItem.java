package com.example.user.communicator.Model.BrodcastsModels;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class BroadCastItem extends RealmObject {

    @PrimaryKey
    private String _id;

    private String senderUserId;
    private RealmList<String> receiverUserIds = new RealmList<>();
    private RealmList<String> userLevelIds = new RealmList<>();
    private RealmList<String> memberTypeIds = new RealmList<>();
    private String message;
    private RealmList<InBoxReplyItem> reply = new RealmList<>();
    private String broadcastType;
    private String replyType;
    private RealmList<String> replyOptions = new RealmList<>();
    private long addTime;
    private int status;
    private int replyCount;
    private boolean isMyBroadCast = false;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public boolean isMyBroadCast() {
        return isMyBroadCast;
    }

    public void setMyBroadCast(boolean myBroadCast) {
        isMyBroadCast = myBroadCast;
    }

    public String getSenderUserId() {
        return senderUserId;
    }

    public void setSenderUserId(String senderUserId) {
        this.senderUserId = senderUserId;
    }

    public RealmList<String> getReceiverUserIds() {
        return receiverUserIds;
    }

    public void setReceiverUserIds(RealmList<String> receiverUserIds) {
        this.receiverUserIds = receiverUserIds;
    }

    public RealmList<String> getUserLevelIds() {
        return userLevelIds;
    }

    public void setUserLevelIds(RealmList<String> userLevelIds) {
        this.userLevelIds = userLevelIds;
    }

    public RealmList<String> getMemberTypeIds() {
        return memberTypeIds;
    }

    public void setMemberTypeIds(RealmList<String> memberTypeIds) {
        this.memberTypeIds = memberTypeIds;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public RealmList<InBoxReplyItem> getReply() {
        return reply;
    }

    public void setReply(RealmList<InBoxReplyItem> reply) {
        this.reply = reply;
    }

    public String getBroadcastType() {
        return broadcastType;
    }

    public void setBroadcastType(String broadcastType) {
        this.broadcastType = broadcastType;
    }

    public String getReplyType() {
        return replyType;
    }

    public void setReplyType(String replyType) {
        this.replyType = replyType;
    }

    public RealmList<String> getReplyOptions() {
        return replyOptions;
    }

    public void setReplyOptions(RealmList<String> replyOptions) {
        this.replyOptions = replyOptions;
    }

    public long getAddTime() {
        return addTime;
    }

    public void setAddTime(long addTime) {
        this.addTime = addTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getReplyCount() {
        return replyCount;
    }

    public void setReplyCount(int replyCount) {
        this.replyCount = replyCount;
    }
}
