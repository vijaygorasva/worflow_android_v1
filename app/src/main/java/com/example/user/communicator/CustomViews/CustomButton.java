package com.example.user.communicator.CustomViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.example.user.communicator.R;


public class CustomButton extends android.support.v7.widget.AppCompatButton {

    String fontStyle;

    public CustomButton(Context context) {
        super(context);
        applyCustomFont(context, "");
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomButton);
        final int N = a.getIndexCount();
        for (int i = 0; i < N; ++i) {
            int attr = a.getIndex(i);
            switch (attr) {
                case R.styleable.CustomButton_customFont:
                    fontStyle = a.getString(attr);
                    applyCustomFont(context, fontStyle);
                    break;

            }
        }
        a.recycle();
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context, "");
    }

    private void applyCustomFont(Context context, String fontStyle) {
        Typeface customFont = Typeface.createFromAsset(context.getAssets(), "fonts/" + fontStyle);
        setTypeface(customFont);
    }
}
