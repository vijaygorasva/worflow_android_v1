package com.example.user.communicator.Utility;


import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;

public class CamPreview extends SurfaceView implements SurfaceHolder.Callback
{
    SurfaceHolder holder;
    Camera camera;
    Parameters params;

    int width, height;

    public CamPreview(Context context, Camera camera, int height)
    {
        super(context);
        this.camera = camera;

        params = camera.getParameters();

        holder = this.getHolder();
        holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        this.height = height;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        Size size = params.getPreviewSize();
        float ratio = (float) size.width / (float) size.height;

        int newWidth = (int) (ratio * height);
        setMeasuredDimension(newWidth, height);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
    {
        if (holder.getSurface() == null) return;

        // stop preview before making changes
        try
        {
            camera.stopPreview();
        }
        catch (Exception e)
        {
        }

        try
        {
            params.setPreviewSize(width, height);
            camera.setPreviewDisplay(holder);
            camera.startPreview();
        }
        catch (Exception e)
        {
            Log.d("FrontCam", "Error starting camera preview: " + e.getMessage());
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder)
    {
        try
        {
            camera.setPreviewDisplay(holder);
            camera.startPreview();
        }
        catch (IOException e)
        {
            Log.d("FrontCam", "Error creating surface: " + e.getMessage());
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder)
    {

    }
}