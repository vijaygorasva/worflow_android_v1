package com.example.user.communicator.Adapter.PlannerAdapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.user.communicator.Model.Notes.TodoListItem;
import com.example.user.communicator.R;
import com.example.user.communicator.Utility.ItemClickListener;

import java.util.ArrayList;

public class DataSetAdapter extends RecyclerView.Adapter<DataSetAdapter.VIHolder> {
    Context context;
    String type;
    ArrayList<TodoListItem> arrayToDoList = new ArrayList<>();

    public DataSetAdapter(Context context, String type, ArrayList<TodoListItem> arrayToDoList) {
        this.context = context;
        this.type = type;
        this.arrayToDoList = arrayToDoList;
    }

    @NonNull
    @Override
    public VIHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        if (type.equals("CHECKBOX")) {

            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.checkbox_layout, viewGroup, false);
        } else {
            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        }
        return new VIHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VIHolder holder, int i) {
        TodoListItem todoListItem = arrayToDoList.get(i);
        holder.etTextCheckBox.setText(todoListItem.getStrNoteListValue());

        holder.etTextCheckBox.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Toast.makeText(context, "key.." + keyCode, Toast.LENGTH_LONG).show();
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    Toast.makeText(context, "Enter key.." + keyCode, Toast.LENGTH_LONG).show();
                } else if (keyCode == KeyEvent.KEYCODE_DEL) {

                    Toast.makeText(context, "delete key.." + keyCode, Toast.LENGTH_LONG).show();
                }
                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrayToDoList.size();
    }

    public void insertData() {

    }

    class VIHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        RelativeLayout llMain;
        ImageView ivCheckbox, ivClose;
        EditText etTextCheckBox;
        private ItemClickListener clickListener;

        public VIHolder(@NonNull View itemView) {
            super(itemView);
            this.llMain = itemView.findViewById(R.id.llMain);
            etTextCheckBox = itemView.findViewById(R.id.etText);
            ivCheckbox = itemView.findViewById(R.id.ivCheckbox);
            ivClose = itemView.findViewById(R.id.ivClose);
            itemView.setTag(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }

        @Override
        public boolean onLongClick(View v) {
            return false;
        }
    }
}
