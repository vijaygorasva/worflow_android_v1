package com.example.user.communicator.Model.TaskModels;

import com.example.user.communicator.Model.TaskModels.SelectionItems.FileData;
import com.example.user.communicator.Model.TaskModels.SelectionItems.User;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class TaskItems extends RealmObject {


    private String _id;
    private CreateUser createUser;
    private RealmList<CommentData> taskComments = new RealmList<>();
    private int isRunning;

    @PrimaryKey
    private int intTaskDocumentNo;

    private int isClosed;
    private String intCreateUserId;
    private String strTackName;
    private String intHour;
    private String intMinut;
    private String strPriorityValue;
    private String strDescription;
    private String strStatus;
    private String intProgress;
    private String strCategory;
    private String strCategoryName;

    private String strSubCategory;
    private String strSubCategoryName;

    private String strCategoryType;
    private String strCategoryTypeName;

    private String isTaskUpdated;
    private String taskStatus;
    private RealmList<DueDate> datStartToDueDate = new RealmList<>();
    private RealmList<FileData> objFileData = new RealmList<>();
    private RealmList<String> strTaskListName = new RealmList<>();
    private RealmList<String> strTaskListNameId = new RealmList<>();
    private boolean isMyTask = false;
    private boolean isDetailAvailable = false;
    private boolean isSelected = false;

    @SerializedName("arryOfObjStrUser")
    private RealmList<User> users = new RealmList<>();

    @SerializedName("arryOfObjStrLevelOne")
    private RealmList<LevelObject> levelsOne = new RealmList<>();


    @SerializedName("arryOfObjStrLevelTwo")
    private RealmList<LevelObject> levelsTwo = new RealmList<>();


    @SerializedName("arryOfObjStrLevelThree")
    private RealmList<LevelObject> levelsThree = new RealmList<>();


    @SerializedName("arryOfObjStrLevelFour")
    private RealmList<LevelObject> levelsFour = new RealmList<>();

    @SerializedName("arryOfObjStrMmber")
    private RealmList<MemberObject> memberTypes = new RealmList<>();

    private RealmList<Duration> taskDurationsData = new RealmList<>();

    @SerializedName("datCreateDateAndTime")
    private String createdDate;

    public int getIsClosed() {
        return isClosed;
    }

    public void setIsClosed(int isClosed) {
        this.isClosed = isClosed;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }


    public CreateUser getCreateUser() {
        return createUser;
    }

    public void setCreateUser(CreateUser createUser) {
        this.createUser = createUser;
    }

    public boolean isMyTask() {
        return isMyTask;
    }

    public void setMyTask(boolean myTask) {
        isMyTask = myTask;
    }

    public int getIsRunning() {
        return isRunning;
    }

    public void setIsRunning(int isRunning) {
        this.isRunning = isRunning;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public RealmList<User> getUsers() {
        return users;
    }

    public void setUsers(RealmList<User> users) {
        this.users = users;
    }

    public RealmList<LevelObject> getLevelsOne() {
        return levelsOne;
    }

    public void setLevelsOne(RealmList<LevelObject> levelsOne) {
        this.levelsOne = levelsOne;
    }

    public RealmList<LevelObject> getLevelsTwo() {
        return levelsTwo;
    }

    public void setLevelsTwo(RealmList<LevelObject> levelsTwo) {
        this.levelsTwo = levelsTwo;
    }

    public RealmList<LevelObject> getLevelsThree() {
        return levelsThree;
    }

    public void setLevelsThree(RealmList<LevelObject> levelsThree) {
        this.levelsThree = levelsThree;
    }

    public RealmList<LevelObject> getLevelsFour() {
        return levelsFour;
    }

    public void setLevelsFour(RealmList<LevelObject> levelsFour) {
        this.levelsFour = levelsFour;
    }

    public RealmList<MemberObject> getMemberTypes() {
        return memberTypes;
    }

    public void setMemberTypes(RealmList<MemberObject> memberTypes) {
        this.memberTypes = memberTypes;
    }

    public String getStrCategory() {
        return strCategory;
    }

    public void setStrCategory(String strCategory) {
        this.strCategory = strCategory;
    }

    public String getStrCategoryName() {
        return strCategoryName;
    }

    public void setStrCategoryName(String strCategoryName) {
        this.strCategoryName = strCategoryName;
    }

    public RealmList<Duration> getTaskDurationsData() {
        return taskDurationsData;
    }

    public void setTaskDurationsData(RealmList<Duration> taskDurationsData) {
        this.taskDurationsData = taskDurationsData;
    }

    public int getIntTaskDocumentNo() {
        return intTaskDocumentNo;
    }

    public void setIntTaskDocumentNo(int intTaskDocumentNo) {
        this.intTaskDocumentNo = intTaskDocumentNo;
    }

    public String getIntCreateUserId() {
        return intCreateUserId;
    }

    public void setIntCreateUserId(String intCreateUserId) {
        this.intCreateUserId = intCreateUserId;
    }

    public String getStrTackName() {
        return strTackName;
    }

    public void setStrTackName(String strTackName) {
        this.strTackName = strTackName;
    }

    public String getIntHour() {
        return intHour;
    }

    public void setIntHour(String intHour) {
        this.intHour = intHour;
    }

    public String getIntMinut() {
        return intMinut;
    }

    public void setIntMinut(String intMinut) {
        this.intMinut = intMinut;
    }

    public String getStrPriorityValue() {
        return strPriorityValue;
    }

    public void setStrPriorityValue(String strPriorityValue) {
        this.strPriorityValue = strPriorityValue;
    }

    public String getStrDescription() {
        return strDescription;
    }

    public void setStrDescription(String strDescription) {
        this.strDescription = strDescription;
    }

    public String getStrStatus() {
        return strStatus;
    }

    public void setStrStatus(String strStatus) {
        this.strStatus = strStatus;
    }

    public String getIntProgress() {
        return intProgress;
    }

    public void setIntProgress(String intProgress) {
        this.intProgress = intProgress;
    }

    public RealmList<CommentData> getTaskComments() {
        return taskComments;
    }

    public void setTaskComments(RealmList<CommentData> taskComments) {
        this.taskComments = taskComments;
    }

    public RealmList<DueDate> getDatStartToDueDate() {
        return datStartToDueDate;
    }

    public void setDatStartToDueDate(RealmList<DueDate> datStartToDueDate) {
        this.datStartToDueDate = datStartToDueDate;
    }

    public RealmList<FileData> getObjFileData() {
        return objFileData;
    }

    public void setObjFileData(RealmList<FileData> objFileData) {
        this.objFileData = objFileData;
    }

    public RealmList<String> getStrTaskListName() {
        return strTaskListName;
    }

    public void setStrTaskListName(RealmList<String> strTaskListName) {
        this.strTaskListName = strTaskListName;
    }

    public RealmList<String> getStrTaskListNameId() {
        return strTaskListNameId;
    }

    public void setStrTaskListNameId(RealmList<String> strTaskListNameId) {
        this.strTaskListNameId = strTaskListNameId;
    }

    public boolean isDetailAvailable() {
        return isDetailAvailable;
    }

    public void setDetailAvailable(boolean detailAvailable) {
        isDetailAvailable = detailAvailable;
    }

    public String getIsTaskUpdated() {
        return isTaskUpdated;
    }

    public void setIsTaskUpdated(String isTaskUpdated) {
        this.isTaskUpdated = isTaskUpdated;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus) {
        this.taskStatus = taskStatus;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getStrSubCategory() {
        return strSubCategory;
    }

    public void setStrSubCategory(String strSubCategory) {
        this.strSubCategory = strSubCategory;
    }

    public String getStrSubCategoryName() {
        return strSubCategoryName;
    }

    public void setStrSubCategoryName(String strSubCategoryName) {
        this.strSubCategoryName = strSubCategoryName;
    }

    public String getStrCategoryType() {
        return strCategoryType;
    }

    public void setStrCategoryType(String strCategoryType) {
        this.strCategoryType = strCategoryType;
    }

    public String getStrCategoryTypeName() {
        return strCategoryTypeName;
    }

    public void setStrCategoryTypeName(String strCategoryTypeName) {
        this.strCategoryTypeName = strCategoryTypeName;
    }
}
