package com.example.user.communicator.Model.TaskModels;


import io.realm.RealmObject;

public class DateTime extends RealmObject {

    private String _id;
    private String strFirstName;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getStrFirstName() {
        return strFirstName;
    }

    public void setStrFirstName(String strFirstName) {
        this.strFirstName = strFirstName;
    }

}
