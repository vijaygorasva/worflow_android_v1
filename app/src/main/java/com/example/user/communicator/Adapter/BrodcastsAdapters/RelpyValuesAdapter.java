package com.example.user.communicator.Adapter.BrodcastsAdapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class RelpyValuesAdapter extends RecyclerView.Adapter<RelpyValuesAdapter.ViewHolder> {


    private final String BASE_URL = "http://13.232.124.188:9999/";
    private ArrayList<String> items;
    private Context context;

    public RelpyValuesAdapter(ArrayList<String> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_reply_value, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(items.get(position));
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.reply)
        CustomTextView reply;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final String item) {
            reply.setText(item);
        }
    }
}
