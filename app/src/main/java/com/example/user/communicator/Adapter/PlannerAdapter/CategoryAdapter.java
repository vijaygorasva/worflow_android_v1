package com.example.user.communicator.Adapter.PlannerAdapter;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.category.ArryCategoryDetail;
import com.example.user.communicator.Model.category.ArryObjStrDefaultSubCategory;
import com.example.user.communicator.R;
import com.example.user.communicator.Utility.DividerItemDecoration;
import com.example.user.communicator.Utility.ItemClickListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ItemViewHolder> {

    RecyclerView rvCategoryList;
    Context context;
    Callback callback;
    private int expandPos = 9999;
    private ArrayList<ArryCategoryDetail> categoryDetails;
    private ArrayList<ArryObjStrDefaultSubCategory> subCategory;

    public CategoryAdapter(Context context, RecyclerView rvCategoryList, ArrayList<ArryObjStrDefaultSubCategory> subCategory, ArrayList<ArryCategoryDetail> categoryDetails, Callback callback) {

        this.context = context;
        this.rvCategoryList = rvCategoryList;
        this.categoryDetails = new ArrayList<ArryCategoryDetail>();
        this.categoryDetails.addAll(categoryDetails);
        this.subCategory = new ArrayList<ArryObjStrDefaultSubCategory>();
        this.subCategory.addAll(subCategory);
        this.callback = callback;

    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.bind(holder, position);
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_operation_category, parent, false);
//        ButterKnife.bind(this, view);
        return new ItemViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return categoryDetails.size();
    }

    public boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());
    }

    public interface Callback {
        void getCatgeory(Context activity, String mainCatName, String mainCatID, String subCatID, String subCatName);
    }

    public void insertCatItem(ArryCategoryDetail item, int position) {
        categoryDetails.add(position, item);
        notifyDataSetChanged();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, View.OnLongClickListener {

        @BindView(R.id.tvCategory)
        TextView tvCategory;
        @BindView(R.id.etCategory)
        EditText etCategory;
        @BindView(R.id.ivSave)
        ImageView ivSave;
        @BindView(R.id.ivDelete)
        ImageView ivDelete;
        @BindView(R.id.llMain)
        LinearLayout llMain;
        @BindView(R.id.flIcons)
        FrameLayout flIcons;
        @BindView(R.id.llFirst)
        RelativeLayout llFirst;
        @BindView(R.id.llSubCat)
        LinearLayout llSubCat;
        @BindView(R.id.rvSubCatList)
        RecyclerView rvSubCatList;
        @BindView(R.id.ivArrow)
        ImageView ivArrow;

        private ItemClickListener clickListener;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setTag(itemView);
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);

        }

        public void setClickListener(ItemClickListener itemClickListener) {
            this.clickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            clickListener.onClick(view, getPosition(), false);
        }

        @Override
        public boolean onLongClick(View view) {
            clickListener.onClick(view, getPosition(), true);
            return true;
        }


        public void bind(final ItemViewHolder holder, int position) {
            ArryCategoryDetail categoryDetail = categoryDetails.get(position);

            flIcons.setVisibility(View.GONE);

            if (position == 0) {

                ivArrow.setVisibility(View.GONE);
                tvCategory.setText(categoryDetail.getStrCategoryName());
                holder.setClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {

                        if (isLongClick) {
                        } else {
                            callback.getCatgeory(context, "All", categoryDetail.getId(), "", "");
                        }

                    }
                });
            } else {
                tvCategory.setText(categoryDetail.getStrCategoryName());
                SubCategoryAdpter operationAdapter = new SubCategoryAdpter(context, categoryDetail.getStrCategoryName(), categoryDetail.getId(), subCategory);

                final boolean isExpanded = holder.getAdapterPosition() == expandPos;
                if (isExpanded) {
                    if (holder.getAdapterPosition() == subCategory.size() - 1) {
                        LinearLayoutManager manager = (LinearLayoutManager) rvCategoryList.getLayoutManager();
                        int distance;
                        View first = rvCategoryList.getChildAt(0);
                        if (first != null) {
                            int height = first.getHeight();
                            int current = rvCategoryList.getChildAdapterPosition(first);
                            int p = Math.abs(holder.getAdapterPosition() - current);
                            if (p > 5) {
                                distance = (p - (p - 5)) * height;
                            } else {
                                distance = p * height;
                            }
                            manager.scrollToPositionWithOffset(holder.getAdapterPosition(), distance);
                        }
                    }
                }

                if (subCategory.size() > 0) {
                    holder.ivArrow.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_keyboard_arrow_right));
                    holder.ivArrow.setVisibility(View.VISIBLE);
                }
                rvSubCatList.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
                rvSubCatList.setActivated(isExpanded);

                if (isExpanded) {
                    holder.ivArrow.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_arrow_down_grey));
                }

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
                rvSubCatList.setLayoutManager(linearLayoutManager);
                rvSubCatList.setHasFixedSize(true);
                rvSubCatList.setAdapter(operationAdapter);

                holder.setClickListener(new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {

                        if (isLongClick) {
                        } else {
//                            holder.ivArrow.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_arrow_down_grey));
                            expandPos = isExpanded ? -1 : holder.getAdapterPosition();

                            if (isExpanded) {
                                notifyItemChanged(holder.getAdapterPosition());
                                operationAdapter.notifyDataSetChanged();
                            }

                            callback.getCatgeory(context, categoryDetail.getStrCategoryName(), categoryDetail.getId(), "", "");
//                            else {
//                                if (subCategory.size() == 0) {
//                                    callback.getCatgeory(context, categoryDetail.getStrCategoryName(), categoryDetail.getId(), "", "");
//                                } else {
//
//                                }
//                                rvSubCatList.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(context, R.drawable.divider)));
//                                notifyDataSetChanged();
//                            }

                        }
                    }
                });

                ivArrow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.ivArrow.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_arrow_down_grey));
                        expandPos = isExpanded ? -1 : holder.getAdapterPosition();

                        if (isExpanded) {
                            notifyItemChanged(holder.getAdapterPosition());
                            operationAdapter.notifyDataSetChanged();
                        } else {
                            if (subCategory.size() == 0) {
                                callback.getCatgeory(context, categoryDetail.getStrCategoryName(), categoryDetail.getId(), "", "");
                            } else {

                            }
                            rvSubCatList.addItemDecoration(new DividerItemDecoration(ContextCompat.getDrawable(context, R.drawable.divider)));
                            notifyDataSetChanged();
                        }

                    }
                });


            }
        }
    }

    public class SubCategoryAdpter extends RecyclerView.Adapter<SubCategoryAdpter.SubCatItemViewHolder> {

        Context context;
        private ArrayList<ArryObjStrDefaultSubCategory> arraylist;
        String mainCatName, mainCatID;

        public SubCategoryAdpter(Context context, String mainCatName, String mainCatID, ArrayList<ArryObjStrDefaultSubCategory> mDataset) {

            this.context = context;
            this.mainCatName = mainCatName;
            this.mainCatID = mainCatID;
            this.arraylist = new ArrayList<ArryObjStrDefaultSubCategory>();
            this.arraylist.addAll(mDataset);
        }

        @Override
        public SubCatItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_sub_cat, parent, false);
            return new SubCatItemViewHolder(view);
        }

        @Override
        public void onBindViewHolder(SubCatItemViewHolder holder, int position1) {
            ArryObjStrDefaultSubCategory subCategory = arraylist.get(position1);
            holder.tvSubCat.setText(subCategory.getStrDefaultSubName());

            holder.setClickListener(new ItemClickListener() {
                @Override
                public void onClick(View view, int position, boolean isLongClick) {
                    if (isLongClick) {

                    } else {
                        callback.getCatgeory(context, mainCatName, mainCatID, subCategory.getId(), subCategory.getStrDefaultSubName());
                    }

                }
            });

        }


        public void removeItem(int position) {
            arraylist.remove(position);
        }

        public void insertItem(ArryObjStrDefaultSubCategory item, int position) {
            arraylist.add(position, item);
            notifyItemChanged(position);
        }

        public void restoreItem(ArryObjStrDefaultSubCategory item, int position) {
            arraylist.remove(position);
            arraylist.add(position, item);
            notifyItemChanged(position);
//            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            return arraylist.size();
        }

        class SubCatItemViewHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener, View.OnLongClickListener {
            CustomTextView tvSubCat;
            private ItemClickListener clickListener;

            public SubCatItemViewHolder(View itemView) {
                super(itemView);
                this.tvSubCat = itemView.findViewById(R.id.tvSubCat);

                itemView.setTag(itemView);
                itemView.setOnClickListener(this);
                itemView.setOnLongClickListener(this);
            }

            public void setClickListener(ItemClickListener itemClickListener) {
                this.clickListener = itemClickListener;
            }

            @Override
            public void onClick(View view) {
                clickListener.onClick(view, getPosition(), false);

            }

            @Override
            public boolean onLongClick(View view) {
                clickListener.onClick(view, getPosition(), true);
                return true;
            }


        }
    }

}
