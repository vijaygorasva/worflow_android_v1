package com.example.user.communicator.Dialog;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.communicator.Activity.PlannerModule.BaseActivity;
import com.example.user.communicator.Activity.PlannerModule.PlannerTabActivity;
import com.example.user.communicator.Fragment.DynamicCategoryFragment;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.Login.Login;
import com.example.user.communicator.Model.category.ArryCategoryDetail;
import com.example.user.communicator.Model.category.ArryObjStrDefaultSubCategory;
import com.example.user.communicator.Model.category.DatumCategory;
import com.example.user.communicator.R;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

public class AllCategoryListDialog extends DialogFragment {//} implements DynamicCategoryFragment.OnCategorySelectedListener {


    public final String URL_ImageUpload_SERVER = "http://52.66.249.75:9999/uploads/";
    public final String URL_ROOT_SERVER = "http://52.66.249.75:9999/api/";//52.66.249.75
    public final String BASE_URL_SEVER = "http://52.66.249.75:9999/";

    public final String URL_ImageUpload_LOCAL = "http://52.66.249.75:9999/uploads/";
    public final String URL_ROOT_local = "http://52.66.249.75:9999/api/";//52.66.249.75
    public final String BASE_URL_local = "http://52.66.249.75:9999/";

    public final String URL_ROOT_TEST = "http://13.232.37.104:9999/api/";//http://13.232.37.104:9999
    public final String URL_ImageUpload_ROOT_TEST = "http://52.66.249.75:9999/uploads/";
    public final String BASE_URL_TEST = "http://192.168.50.100:9999/";

    public final String URL_ROOT = URL_ROOT_TEST;
    public final String URL_ImageUpload_ROOT = URL_ImageUpload_ROOT_TEST;
    public final String BASE_URL = BASE_URL_TEST;


    //    public final String URL_ROOT = "http://52.66.249.75:9999/api/";//52.66.249.75
    public final String url_allCategoryList = URL_ROOT + "category/autoCompleteCategory";
    @BindView(R.id.tablayout)
    TabLayout tabLayout;
    @BindView(R.id.viewpager)
    ViewPager viewPager;

    String strUserId, moduleType;
    Datum mUserDetails;
    Realm mRealm;
    Boolean isFabActive;
    ArrayList<String> tabNames = new ArrayList<>();
    ArrayList<String> tabCatId = new ArrayList<>();

    ArrayList<ArryObjStrDefaultSubCategory> arryObjStrDefaultSubCategories = new ArrayList<>();

//    public static AllCategoryListDialog newIntance() {
//            Bundle args = new Bundle();
//            AllCategoryListDialog dialog = new AllCategoryListDialog();
//            dialog.setArguments(args);
//        return dialog;
//    }

    public static AllCategoryListDialog newIntance(String planner, Boolean isFabActive) {
        Bundle args = new Bundle();
        args.putString("module", planner);
        args.putBoolean("isFabActive", isFabActive);

        Log.e("AllCategoryListDialog", planner + "...");
        AllCategoryListDialog dialog = new AllCategoryListDialog();
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public void onAttachFragment(Fragment childFragment) {
        super.onAttachFragment(childFragment);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_all_category, container, false);
        ButterKnife.bind(this, view);
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle args = getArguments();
        moduleType = args.getString("module");
        isFabActive = args.getBoolean("isFabActive");

        SharedPreferences sharedpreferences = getContext().getApplicationContext().getSharedPreferences("Login", 0);
        String data = sharedpreferences.getString("UserData", null);
        Gson gson = new Gson();
        mUserDetails = gson.fromJson(data, Datum.class);
        strUserId = mUserDetails.getIntUserId();
        mRealm = Realm.getDefaultInstance();

        getAllCategory();

    }

    public void showToast(String message) {
        LayoutInflater inflater = getLayoutInflater();
        View layouttoast = inflater.inflate(R.layout.toastcustom, (ViewGroup) getActivity().findViewById(R.id.toastcustom));
        ((TextView) layouttoast.findViewById(R.id.texttoast)).setText(message);

        Toast mytoast = new Toast(getActivity());
        mytoast.setView(layouttoast);
        mytoast.setDuration(Toast.LENGTH_LONG);
        mytoast.setGravity(Gravity.CENTER, 0, 0);
        mytoast.show();
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public void getAllCategory() {

        if (isNetworkAvailable()) {
            RequestParams params = new RequestParams();
            params.put("intProjectId", mUserDetails.getArryselectedProjectAllItems().get(0).getId());//5bfc09844d70683a16451707
            params.put("intOrganisationId", mUserDetails.getArryselectedOrganisationAllItems().get(0).getId());//5be144b78dd6c908037cf3b8

            ApiCallWithToken(url_allCategoryList, BaseActivity.ApiCall.ALLCATEGORYLIST, params);

        } else {
            arryObjStrDefaultSubCategories.clear();
            RealmResults<DatumCategory> itemResults = mRealm.where(DatumCategory.class).findAll();
            List<DatumCategory> arrayListOfUnmanagedObjects = mRealm.copyFromRealm(itemResults);

            RealmResults<ArryObjStrDefaultSubCategory> itemLists = mRealm.where(ArryObjStrDefaultSubCategory.class).findAll();

            if (arrayListOfUnmanagedObjects != null && arrayListOfUnmanagedObjects.size() != 0) {
                for (int i = 0; i < arrayListOfUnmanagedObjects.size(); i++) {
                    DatumCategory fragName = arrayListOfUnmanagedObjects.get(i);

//                    ArrayList<ArryObjStrDefaultSubCategory> list = (ArrayList<ArryObjStrDefaultSubCategory>) mRealm.copyFromRealm(fragName.getArryObjStrDefaultSubCategory());
//                    if (list.size() != 0) {
//                        arryObjStrDefaultSubCategories.addAll(list);
//                    }
                    dynamicTabCreation(fragName);
                }
            }
        }
    }

    public void ApiCallWithToken(String url, final BaseActivity.ApiCall type, final RequestParams requestParams) {

        requestParams.put("strCategoryTypeId", PlannerTabActivity.strCategoryTypeId);
        requestParams.put("strCategoryId", PlannerTabActivity.strCategoryId);
        requestParams.put("strSubCategoryId", PlannerTabActivity.strSubCategoryId);

        try {
            SharedPreferences sharedpreferences = getActivity().getSharedPreferences("Login", 0);
            String data = sharedpreferences.getString("UserData", null);
            String token = sharedpreferences.getString("Token", null);
            Gson gson = new Gson();
            Login login = gson.fromJson(data, Login.class);
            com.example.user.communicator.Model.Login.Datum userData = gson.fromJson(data, Datum.class);
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(30 * 1000);
            if (userData != null) {
                client.addHeader("Authorization", "Bearer " + token);
            }

            client.setURLEncodingEnabled(true);
            client.post(getActivity(), url, requestParams, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    try {
                        if (response.getBoolean("success")) {
                            OnResponce(response, type);
                        } else {
                            OnError(response, type);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                    try {
                        String errorType = throwable.getMessage();
                        showToast(errorResponse.getString("message"));
//                        if (errorType.equals("Read timed out")) {
//                            showToast("Oops!!! Server Time-out.Please try again");
//                        } else {
//                            showToast("Oops!!! Please try again");
                        OnError(errorResponse, type);
//                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.e("ALLCATEGORYLIST_onFaile", responseString + ".." + statusCode);
                    showToast("Oops!!! Please try again");
                }

            });

        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    public void OnResponce(JSONObject data, BaseActivity.ApiCall type) {

        String s = data.toString();
        JSONArray array = null;
        try {
            if (type == BaseActivity.ApiCall.ALLCATEGORYLIST) {
                array = data.getJSONArray("data");
                arryObjStrDefaultSubCategories.clear();
                if (array.length() != 0) {
                    Gson gson = new Gson();
                    mRealm.beginTransaction();

                    for (int k = 0; k < array.length(); k++) {
                        JSONObject object = array.getJSONObject(k);
                        DatumCategory fragName = gson.fromJson(object.toString(), DatumCategory.class);
                        mRealm.copyToRealmOrUpdate(fragName);
                        if (fragName.getArryObjStrDefaultSubCategory().size() != 0) {
                            arryObjStrDefaultSubCategories.addAll(fragName.getArryObjStrDefaultSubCategory());
                        }

                        dynamicTabCreation(fragName);
                    }
                    mRealm.commitTransaction();
                } else {
                    dismiss();
                    showToast("You don't have any project");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void dynamicTabCreation(DatumCategory fragName) {

        List<String> listModules = fragName.getArryStrIncludeModules();
        for (int j = 0; j < listModules.size(); j++) {
            assert listModules.get(j) != null;
            if (listModules.get(j).equals(moduleType)) {
                tabNames.add(fragName.getStrCategoryType());
                tabCatId.add(fragName.getIntCaregoryTypeId());
                tabLayout.addTab(tabLayout.newTab().setText(fragName.getStrCategoryType()));

                FragmentManager manager = getFragmentManager();

                if (manager != null) {

                    PlansPagerAdapter adapter = new PlansPagerAdapter(getChildFragmentManager(), tabLayout.getTabCount(), fragName.getStrCategoryType());
                    viewPager.setAdapter(adapter);

                    tabLayout.setOnTabSelectedListener(onTabSelectedListener(viewPager));
                    viewPager.setOffscreenPageLimit(1);
                    viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

                    if (tabLayout.getTabCount() < 3) {
                        tabLayout.setTabMode(TabLayout.MODE_FIXED);
                    } else {
                        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
                    }
                }

            }
        }
    }

    private TabLayout.OnTabSelectedListener onTabSelectedListener(final ViewPager pager) {
        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        };
    }

    public void OnError(JSONObject object, BaseActivity.ApiCall type) {

        if (type == BaseActivity.ApiCall.OPERATIONSLIST) {

            try {
                showToast(object.getString("message"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public class PlansPagerAdapter extends FragmentStatePagerAdapter {
        int mNumOfTabs;
        String tabName, tabCatID;
        RealmList<ArryObjStrDefaultSubCategory> realsubCategory;
        RealmList<ArryCategoryDetail> realcategoryDetails;
        ArrayList<ArryObjStrDefaultSubCategory> subCategory;
        ArrayList<ArryCategoryDetail> detailmainCategory;

        public PlansPagerAdapter(FragmentManager fm, int NumOfTabs, String tabName) {

            super(fm);

            this.mNumOfTabs = NumOfTabs;
            this.tabName = tabName;
        }

        @Override
        public Fragment getItem(int position) {

            tabCatID = tabCatId.get(position);
            tabName = tabNames.get(position);
            detailmainCategory = new ArrayList<>();
            subCategory = new ArrayList<>();
            Realm realm = Realm.getDefaultInstance();
            RealmResults<DatumCategory> itemList1 = realm.where(DatumCategory.class).findAll();

            Bundle bundle = new Bundle();

            for (int i = 0; i < itemList1.size(); i++) {
                RealmResults<DatumCategory> apiPlannerList = realm.where(DatumCategory.class).equalTo("strCategoryType", tabName).findAll();
                realsubCategory = apiPlannerList.get(0).getArryObjStrDefaultSubCategory();
                realcategoryDetails = apiPlannerList.get(0).getArryCategoryDetails();
            }

            for (int i = 0; i < realsubCategory.size(); i++) {
                subCategory.add(realsubCategory.get(i));
            }

            for (int i = 0; i < realcategoryDetails.size(); i++) {
                detailmainCategory.add(realcategoryDetails.get(i));
            }

            bundle.putParcelableArrayList("values", subCategory);

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i1) {

                }

                @Override
                public void onPageSelected(int i) {
                    DynamicCategoryFragment.newInstance(getParentFragment(), isFabActive, position, tabName, tabCatID, subCategory, detailmainCategory);
                }

                @Override
                public void onPageScrollStateChanged(int i) {

                }

            });

            return DynamicCategoryFragment.newInstance(getParentFragment(), isFabActive, position, tabName, tabCatID, subCategory, detailmainCategory);
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }

    }

}