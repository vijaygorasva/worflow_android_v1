package com.example.user.communicator.Dialog;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.user.communicator.R;
import com.jaredrummler.materialspinner.MaterialSpinner;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddNewTaskDialog extends DialogFragment {

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    @BindView(R.id.btnCancel)
    Button btnCancel;
    Callback callback;
    @BindView(R.id.spinner)
    MaterialSpinner spinner;

    String memType;

    public static AddNewTaskDialog newIntance(Callback callback) {

        AddNewTaskDialog dialog = new AddNewTaskDialog();
//        dialog.setArguments(args);
        dialog.setCallback(callback);
        return dialog;
    }

//    private void setArguments(ArrayList<DependencyList> dependencyListArrayList) {
//        dependencyListArrayList.addAll(dependencyListArrayList);
//    }

    public AddNewTaskDialog() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, getTheme());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_input_dialog_add_task, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        spinner.setItems("All Staff", "All Head");
        int index=spinner.getSelectedIndex();
        if (index==0) memType="All Staff";
        else memType="All Head";
        spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {

            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
//                Snackbar.make(view, "Clicked " + item, Snackbar.LENGTH_LONG).show();

                memType=item;

            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {

        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.btnCancel)
    public void onCancelClick() {
        dismiss();
    }

    @Override
    public void onDestroyView() {

        super.onDestroyView();
//        ButterKnife.un(this);
    }

    @OnClick(R.id.btnSubmit)
    public void onSubmitClick() {

        callback.edit(AddNewTaskDialog.this,memType);
        dismiss();

    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }


    public interface Callback {
        void edit(AddNewTaskDialog dialog, String name);
    }

}