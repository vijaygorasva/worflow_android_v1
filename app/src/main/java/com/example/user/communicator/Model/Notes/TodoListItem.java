
package com.example.user.communicator.Model.Notes;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

public class TodoListItem extends RealmObject implements Parcelable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("strNoteListValue")
    @Expose
    private String strNoteListValue;
    @SerializedName("strNoteListFlage")
    @Expose
    private Boolean strNoteListFlage;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public TodoListItem(String id, String strNoteListValue, Boolean strNoteListFlage) {
        this.id=id;
        this.strNoteListValue = strNoteListValue;
        this.strNoteListFlage = strNoteListFlage;
    }

    public TodoListItem() {

    }

    public String getStrNoteListValue() {
        return strNoteListValue;
    }

    public void setStrNoteListValue(String strNoteListValue) {
        this.strNoteListValue = strNoteListValue;
    }

    public Boolean getStrNoteListFlage() {
        return strNoteListFlage;
    }

    public void setStrNoteListFlage(Boolean strNoteListFlage) {
        this.strNoteListFlage = strNoteListFlage;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TodoListItem> CREATOR = new Creator<TodoListItem>() {
        @Override
        public TodoListItem createFromParcel(Parcel in) {
            return new TodoListItem(in);
        }

        @Override
        public TodoListItem[] newArray(int size) {
            return new TodoListItem[size];
        }
    };

    public TodoListItem(Parcel in) {
        strNoteListValue = in.readString();
        strNoteListFlage = Boolean.valueOf(in.readString());
    }


    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(strNoteListValue);
        parcel.writeString(String.valueOf(strNoteListFlage));

    }


}
