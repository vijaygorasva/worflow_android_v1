package com.example.user.communicator.Activity.TaskModule.Task;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.user.communicator.Activity.TaskModule.DetailsActivity.BaseFragment;
import com.example.user.communicator.Adapter.TaskAdapters.FilterTypeAdapter;
import com.example.user.communicator.Adapter.TaskAdapters.TaskListAdapter;
import com.example.user.communicator.CustomViews.CustomEditText;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.CustomViews.CustomeViewPager;
import com.example.user.communicator.Dialog.AllCategoryListDialog;
import com.example.user.communicator.Dialog.FilterDialog;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.TaskModels.FilterType;
import com.example.user.communicator.Model.TaskModels.SelectionItem;
import com.example.user.communicator.Model.TaskModels.SelectionObject;
import com.example.user.communicator.Model.TaskModels.TaskItems;
import com.example.user.communicator.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

public class MyTaskFragment extends BaseFragment {

    @BindView(R.id.etAddTask)
    CustomEditText etAddTask;

    @BindView(R.id.tvDone)
    CustomTextView tvDone;

    @BindView(R.id.rvTaskList)
    RecyclerView rvTaskList;

    @BindView(R.id.filterType)
    ImageView filterType;

    TaskListAdapter taskListAdapter;
    ArrayList<TaskItems> taskItems = new ArrayList<>();

    @BindView(R.id.fabSearch11)
    FloatingActionButton fabSearch1;

    @BindView(R.id.tvTypeCategory)
    CustomTextView tvTypeCategory;

    @BindView(R.id.tvMainCategory)
    CustomTextView tvMainCategory;

    @BindView(R.id.tvSubCategory)
    CustomTextView tvSubCategory;

//    @BindView(R.id.line)
//    View mLine;

    Context context;

    SelectionObject mObject;
    Datum mUserDetails;

    ArrayList<SelectionItem> selectionItemsUsers = new ArrayList<>();
    ArrayList<SelectionItem> selectionItemsLevels = new ArrayList<>();
    ArrayList<SelectionItem> selectionItemsMembers = new ArrayList<>();

    ArrayList<FilterType> filterTypes = new ArrayList<>();

    boolean isSearch = false;
    Realm mRealm;
    String categoryID = "0";
    String categoryName = "";
    String categoryTypeID = "";
    String categoryTypeName = "";
    String subCategoryID = "";
    String subCategoryName = "";
    String mFilterType = "ADD_TIME";
    String BROADCAST_ACTION = "com.example.user.communicator";

    public String mSeacrhText = "";
    ViewPager viewPager;

    @BindView(R.id.vLoading)
    LinearLayout vLoading;

    AllCategoryListDialog allCategoryListDialog;

    public static MyTaskFragment newInstance(CustomeViewPager viewPager) {
        MyTaskFragment fragmentFirst = new MyTaskFragment();
        Bundle args = new Bundle();
        args.putSerializable("viewPager", viewPager);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }


    @SuppressLint("RestrictedApi")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_my_task, container, false);
        ButterKnife.bind(this, rootView);

        context = getActivity();
        mRealm = Realm.getDefaultInstance();
        viewPager = (ViewPager) getArguments().getSerializable("viewPager");

        mUserDetails = getLoginData();

        rvTaskList.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        rvTaskList.setLayoutManager(mLayoutManager);

        final LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        rvTaskList.setLayoutManager(layoutManager);

        taskListAdapter = new TaskListAdapter(getActivity(), taskItems, rvTaskList);
        taskListAdapter.setIsAssignedTask(false);
        rvTaskList.setAdapter(taskListAdapter);

        tvDone.setOnClickListener(view -> taskAddCall());

        tvTypeCategory.setOnClickListener(v -> {

            if (TextUtils.isEmpty(subCategoryName) && TextUtils.isEmpty(categoryName)) {
                allCategoryListDialog = AllCategoryListDialog.newIntance("TASK", false);

                if (allCategoryListDialog != null) {
                    allCategoryListDialog.show(getActivity().getSupportFragmentManager().beginTransaction(), "my-dialog");
                }
            } else {
                getSelectedCategory("", "", "", "", "", "");

            }
            tvMainCategory.setVisibility(View.GONE);
            tvSubCategory.setVisibility(View.GONE);
        });

        tvMainCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvSubCategory.setVisibility(View.GONE);

                if (TextUtils.isEmpty(subCategoryName)) {
                    allCategoryListDialog = AllCategoryListDialog.newIntance("TASK", false);

                    if (allCategoryListDialog != null) {
                        allCategoryListDialog.show(getActivity().getSupportFragmentManager().beginTransaction(), "my-dialog");
                    }
                } else {
                    getSelectedCategory(categoryTypeName, categoryTypeID, categoryName, categoryID, "", "");
                }
            }
        });

        tvSubCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSelectedCategory(categoryTypeName, categoryTypeID, categoryName, categoryID, subCategoryID, subCategoryName);
            }
        });

        filterType.setVisibility(View.VISIBLE);
//        mLine.setVisibility(View.VISIBLE);

        fabSearch1.setOnClickListener(v -> {
            if (!isSearch) {
                isSearch = true;
                Interface.onChangeVisibility.OnChangevisibility(View.VISIBLE);
//            searchView.setVisibility(View.VISIBLE);
//                fabSearch1.setVisibility(View.GONE);
            } else {
                isSearch = false;
                Interface.onChangeVisibility.OnChangevisibility(View.GONE);
                setListOfTasks();
            }
        });


        /*
        2019-07-15 11:30:06.314 5475-5475/com.example.user.communicator E/Apply_parms:
        http://52.66.249.75:9999/api/task/get_filter_task_search_details.....
        intUserId=000000013905710a216f49c5
        &strSubCategoryId=
        &strCategoryId=
        &intProgress=
        &intProjectId=5d162c7c3905710a216f49ac
        &intTaskDocumentNo=
        &intAssignedUserId=000000013905710a216f49c4
        &intOrganisationId=5d162c513905710a216f49ab
        &strCategoryTypeId=
        &strSearchType=MYTASK
        &strSearchText=
         */

        filterType.setOnClickListener(v -> {
//            showFilterTypeDialog();

            FilterDialog.newIntance("MYTASK", categoryTypeID, categoryID, subCategoryID, new FilterDialog.Callback() {
                @Override
                public void setFilterData(ArrayList<TaskItems> taskItems) {
                    taskListAdapter = new TaskListAdapter(getActivity(), taskItems, rvTaskList);
                    if (taskItems.size() == 0) {
                        showToast("No task found in this category");
                    } else {
                        taskListAdapter.setIsAssignedTask(false);
                    }
                    rvTaskList.setAdapter(taskListAdapter);
                }

            }).show(getActivity().getSupportFragmentManager(), "FilterDialog");

        });

//        filterType.setText("Newly Assigned");

        FilterType filterType = new FilterType();
        filterType.setName("Newly Assigned");
        filterType.setKey("ADD_TIME");
        filterType.setSelected(true);
        filterTypes.add(filterType);

        FilterType filterType1 = new FilterType();
        filterType1.setName("Recently Updated");
        filterType1.setKey("UPDATE_TIME");
        filterTypes.add(filterType1);

        FilterType filterType2 = new FilterType();
        filterType2.setName("Priority Low to High");
        filterType2.setKey("PRIORITY_LOW_TO_HIGH");
        filterTypes.add(filterType2);

        FilterType filterType3 = new FilterType();
        filterType3.setName("Priority High to Low");
        filterType3.setKey("PRIORITY_HIGH_TO_LOW");
        filterTypes.add(filterType3);


        mObject = getSelectionItem();
        Interface.setOnSearchTextListner(onSearchTextListner);
        Interface.setOnPerformSearchListner(onPerformSearchListner);
        setListOfTasks();

        IntentFilter filter = new IntentFilter();
        filter.addAction(BROADCAST_ACTION);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        getActivity().registerReceiver(notificationBroadcaster, filter);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        return rootView;
    }

    Interface.OnSearchTextListner onSearchTextListner = text -> {
        if (viewPager.getCurrentItem() == 0) {
            mSeacrhText = text;
            if (mSeacrhText.length() > 3) {
                performSearch();
            }
        }
    };

    Interface.OnPerformSearchListner onPerformSearchListner = search -> {
        if (viewPager.getCurrentItem() == 0) {
            if (search) {
                performSearch();
            } else {
                setListOfTasks();
            }
        }
    };


    BroadcastReceiver notificationBroadcaster = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String intUserId = intent.getStringExtra("intUserId");
            String intTaskDocumentNo = intent.getStringExtra("intTaskDocumentNo");
            if (!isSearch) {
                getData(intTaskDocumentNo);
            }
        }
    };

    public void getData(String id) {
        RequestParams params = new RequestParams();
        params.put("intTaskDocumentNo", id);
//        showProgressDialog("Please wait...");
//        vLoading.setVisibility(View.VISIBLE);
        ApiCallWithToken(getTaskData, ApiCall.GET_TASK_DATA, params);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().unregisterReceiver(notificationBroadcaster);
    }

//    @SuppressLint("RestrictedApi")
//    @OnClick({R.id.ivBack, R.id.ivSearch, R.id.fabtnAdd})
//    public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.ivBack:
//                getActivity().finish();
//                break;
//            case R.id.ivSearch:
//                isSearch = true;
//                Interface.onChangeVisibility.OnChangevisibility(View.VISIBLE);
////                searchView.setVisibility(View.VISIBLE);
//                break;
//            case R.id.fabtnAdd:
//                Intent i = new Intent(getActivity(), SearchUserActivity.class);
//                startActivityForResult(i, 2020);
//                break;
//        }
//    }


    private void showFilterTypeDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.dilogue_filter_type);

        RecyclerView recyclerView = dialog.findViewById(R.id.recyclerView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);

        filterTypes.get(0).setName("Newly Assigned");

        FilterTypeAdapter adapter = new FilterTypeAdapter(getActivity(), filterTypes);
        recyclerView.setAdapter(adapter);
        adapter.setOnClickListner(position -> {
            for (int i = 0; i < filterTypes.size(); i++) {
                filterTypes.get(i).setSelected(false);
            }
            filterTypes.get(position).setSelected(true);
            mFilterType = filterTypes.get(position).getKey();
//            filterType.setText(filterTypes.get(position).getName());
            adapter.notifyDataSetChanged();
            dialog.dismiss();
            if (isSearch) {
                performSearch();
            } else {
                setListOfTasks();

            }
        });
        dialog.show();
    }

    public void performSearch() {

        if (isNetworkAvailable()) {
            if (!TextUtils.isEmpty(mSeacrhText)) {
                RequestParams params = new RequestParams();
                params.put("intUserId", mUserDetails.getIntUserId());
                params.put("intMemberTypeId", mUserDetails.getIntMemberTypeId());
                params.put("intLevelTypeId", mUserDetails.getIntLevelTypeId());
                params.put("intSkipCount", 0);
                params.put("intPagelimit", 1);
                params.put("searchText", mSeacrhText);
                params.put("sortBy", mFilterType);
                params.put("strCategory", categoryID);
                if (!TextUtils.isEmpty(subCategoryID)) {
                    params.put("strSubCategory", subCategoryID);
                }
//        showProgressDialog("Please wait...");
                vLoading.setVisibility(View.VISIBLE);
                ApiCallWithToken(searchMyTask, ApiCall.SEARCH_MY_TASKS, params);
            } else {
                setListOfTasks();
            }
        } else {
            showNetworkAlert();
        }
    }

    @SuppressLint("RestrictedApi")
    public void setListOfTasks() {
        isSearch = false;
//        mSearchBox.setText("");
        Interface.onChangeVisibility.OnChangevisibility(View.GONE);
//        searchView.setVisibility(View.GONE);
        fabSearch1.setVisibility(View.VISIBLE);
        RequestParams params = new RequestParams();
        taskItems.clear();
        taskListAdapter.notifyDataSetChanged();
        if (isNetworkAvailable()) {
            params.put("intUserId", mUserDetails.getIntUserId());
            params.put("intMemberTypeId", mUserDetails.getIntMemberTypeId());
            params.put("intLevelTypeId", mUserDetails.getIntLevelTypeId());
            params.put("intSkipCount", 0);
            params.put("intPagelimit", 1);
            params.put("sortBy", mFilterType);
            params.put("strCategory", categoryID);
            if (!TextUtils.isEmpty(subCategoryID)) {
                params.put("strSubCategory", subCategoryID);
            }
//        showProgressDialog("Please wait...");
            vLoading.setVisibility(View.VISIBLE);
            Log.e("getMyTask", getMyTask + "..." + params);
            ApiCallWithToken(getMyTask, ApiCall.GET_MYTASK, params);
        } else {
            taskItems.clear();
            RealmResults<TaskItems> itemsRealmResults = mRealm.where(TaskItems.class).equalTo("isMyTask", true).sort("createdDate", Sort.DESCENDING).findAll();
            if (itemsRealmResults != null && itemsRealmResults.size() != 0) {
                taskItems.addAll(itemsRealmResults);
                taskListAdapter.notifyDataSetChanged();
            }
        }
//        if (filterType.getText().toString().trim().equalsIgnoreCase("Newly Assigned") ||
//                filterType.getText().toString().trim().equalsIgnoreCase("Newly Added")) {
//            filterType.setText("Newly Assigned");
//        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (viewPager.getCurrentItem() == 0) {
            if (requestCode == 5252 || requestCode == 5555) {
                taskListAdapter.onAttachmentResult(requestCode, resultCode, data);
            } else if (requestCode == 123) {
                boolean isUpdated = data.getBooleanExtra("isUpdated", false);
                int _id = data.getIntExtra("_id", 0);
                int position = data.getIntExtra("position", 0);
//            if (isUpdated) {
//                if (isMyTaskItems) {
//                    setListOfTasks("My Tasks");
//                } else {
//                    setListOfTasks("Assigned Tasks");
//                }
//            }
                TaskItems tItem = mRealm.where(TaskItems.class).equalTo("intTaskDocumentNo", _id).findFirst();
                taskItems.set(position, tItem);
                taskListAdapter.notifyItemChanged(position);

            }
        }
    }


    public void taskAddCall() {
        String task = etAddTask.getText().toString().trim();
        if (TextUtils.isEmpty(task)) {
            Toast.makeText(context, "Please write something about task.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(categoryTypeID)) {
            Toast.makeText(context, "Please select category.", Toast.LENGTH_SHORT).show();
            return;
        }

        RequestParams params = new RequestParams();
        params.put("strDescription", task);
        params.put("intCreateUserId", mUserDetails.getIntUserId());
        params.put("strCategoryName", categoryName);
        params.put("strCategory", categoryID);
        params.put("strSubCategory", subCategoryID);
        params.put("strSubCategoryName", subCategoryName);
        params.put("strCategoryType", categoryTypeID);
        params.put("strCategoryTypeName", categoryTypeName);

        JSONArray array = new JSONArray();
        try {
            JSONObject object = new JSONObject();
            object.put("id", mUserDetails.getIntUserId());
            array.put(object);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (array.length() != 0) {
            params.put("arryOfobjstrUserCollectionData", array.toString());
        }

        ApiCallWithToken(addTask, ApiCall.ADD_TASK, params);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void OnResponce(JSONObject data, ApiCall type) {

        try {
            Gson gson = new GsonBuilder().create();
            JSONArray array;
            switch (type) {
                case ADD_TASK:
                    JSONObject object1 = data.getJSONObject("data").getJSONObject("taskData");
                    mRealm.beginTransaction();
                    TaskItems items1 = gson.fromJson(object1.toString(), TaskItems.class);
                    items1.setMyTask(false);
                    Log.e("items1", new Gson().toJson(items1) + "...");

                    mRealm.copyToRealmOrUpdate(items1);
                    this.taskItems.add(0, items1);
                    mRealm.commitTransaction();
                    taskListAdapter.notifyDataSetChanged();
                    etAddTask.setText("");
//                    mObject = new SelectionObject();
//                    usersCount.setText("");
//                    usersCount.setVisibility(View.GONE);
                    break;

                case GET_MYTASK:
                case GET_ASSIGNED_TASK:
                    taskItems.clear();
                    array = data.getJSONObject("data").getJSONArray("tasksData");
                    if (array.length() != 0) {
                        mRealm.beginTransaction();
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            TaskItems items = gson.fromJson(object.toString(), TaskItems.class);
                            items.setMyTask(true);
                            mRealm.copyToRealmOrUpdate(items);
                            this.taskItems.add(items);
                        }
                        mRealm.commitTransaction();
                    }
//                    Collections.sort(taskItems, (s1, s2) -> {
//                        long rollno1 = s1.getCreatedDate();
//                        long rollno2 = s2.getCreatedDate();
//                        Date date1 = new Date(rollno1);
//                        Date date2 = new Date(rollno2);
//                        return date2.compareTo(date1);
//                    });

                    if (taskItems.size() == 0) {
                        showToast("No task found in this category");
                    } else {
//                        taskListAdapter.setIsAssignedTask(false);

                        taskListAdapter = new TaskListAdapter(getActivity(), taskItems, rvTaskList);
                        taskListAdapter.setIsAssignedTask(false);
                        rvTaskList.setAdapter(taskListAdapter);
                        taskListAdapter.notifyDataSetChanged();

                    }
                    break;

                case SEARCH_ASSIGNED_TASK:
                case SEARCH_MY_TASKS:
                    taskItems.clear();
                    array = data.getJSONObject("data").getJSONArray("tasksData");
                    if (array.length() != 0) {
                        mRealm.beginTransaction();
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject object = array.getJSONObject(i);
                            TaskItems items = gson.fromJson(object.toString(), TaskItems.class);
                            items.setMyTask(true);
                            mRealm.copyToRealmOrUpdate(items);
                            this.taskItems.add(items);
                        }
                        mRealm.commitTransaction();
                    }
//                    Collections.sort(taskItems, (s1, s2) -> {
//                        long rollno1 = s1.getCreatedDate();
//                        long rollno2 = s2.getCreatedDate();
//                        Date date1 = new Date(rollno1);
//                        Date date2 = new Date(rollno2);
//                        return date2.compareTo(date1);
//                    });
                    taskListAdapter.setIsAssignedTask(false);
                    taskListAdapter.notifyDataSetChanged();
                    break;
                case GET_TASK_DATA:

                    TaskItems taskItems1 = gson.fromJson(data.getJSONObject("data").getJSONObject("taskData").toString(), TaskItems.class);
                    if (!taskItems1.getCreateUser().get_id().equalsIgnoreCase(mUserDetails.getIntUserId())) {
                        mRealm.beginTransaction();
                        mRealm.copyToRealmOrUpdate(taskItems1);
                        mRealm.commitTransaction();
                        if (tvTypeCategory.getText().toString().equalsIgnoreCase(taskItems1.getStrCategoryName()) ||
                                tvTypeCategory.getText().toString().equalsIgnoreCase("All")) {
                            for (int i = 0; i < taskItems.size(); i++) {
                                if (taskItems.get(i).getIntTaskDocumentNo() == taskItems1.getIntTaskDocumentNo()) {
                                    taskItems.remove(i);
                                }
                            }
                            taskItems.add(0, taskItems1);
                            taskListAdapter.notifyDataSetChanged();
                        }
                    }
                    break;
                case GET_USERS_DATA:
                    array = data.getJSONArray("data");
                    if (array.length() != 0) {
                        selectionItemsUsers.clear();
                        for (int i = 0; i < array.length(); i++) {
                            try {
                                JSONObject object = array.getJSONObject(i);
                                SelectionItem item = gson.fromJson(object.toString(), SelectionItem.class);
                                selectionItemsUsers.add(item);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    break;

                case GET_LEVELS:
                    array = data.getJSONArray("data");
                    if (array.length() != 0) {
                        selectionItemsLevels.clear();
                        for (int i = 0; i < array.length(); i++) {
                            try {
                                JSONObject object = array.getJSONObject(i);
                                SelectionItem item = gson.fromJson(object.toString(), SelectionItem.class);
                                selectionItemsLevels.add(item);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    break;

                case GET_MEMBER_TYPES:
                    array = data.getJSONArray("data");
                    if (array.length() != 0) {
                        selectionItemsMembers.clear();
                        for (int i = 0; i < array.length(); i++) {
                            try {
                                JSONObject object = array.getJSONObject(i);
                                SelectionItem item = gson.fromJson(object.toString(), SelectionItem.class);
                                selectionItemsMembers.add(item);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        hideProgressDialog();
        new Handler().postDelayed(() -> {
            if (getActivity() != null) {
                getActivity().runOnUiThread(() -> vLoading.setVisibility(View.GONE));
            }
        }, 500);
    }

    public void getSelectedCategory(String tabName, String tabID, String mainCat, String mainCatID, String subCatID, String subCatName) {
        if (allCategoryListDialog != null) {
            allCategoryListDialog.dismiss();
        }

        if (!TextUtils.isEmpty(mainCat)) {
            tvMainCategory.setVisibility(View.VISIBLE);
            if (mainCat.equals("All")) {
                tvMainCategory.setText(tabName);
            } else {
                tvMainCategory.setText(mainCat);
            }
        }

        if (!TextUtils.isEmpty(subCatName)) {
            tvSubCategory.setVisibility(View.VISIBLE);
            tvSubCategory.setText(subCatName);
        }
        categoryName = mainCat;
        categoryID = mainCatID;
        categoryTypeID = tabID;
        categoryTypeName = tabName;
        subCategoryID = subCatID;
        subCategoryName = subCatName;
        setListOfTasks();
    }

}
