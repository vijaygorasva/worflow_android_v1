
package com.example.user.communicator.Service;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.text.format.DateFormat;
import android.util.Log;

import com.example.user.communicator.Activity.PlannerModule.NoteDetailActivity;
import com.example.user.communicator.Model.Notes.Item;
import com.example.user.communicator.Model.Notes.ObjRemainder;
import com.example.user.communicator.Model.Reminder.Reminder;
import com.example.user.communicator.R;
import com.example.user.communicator.Utility.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import io.realm.Realm;
import io.realm.RealmResults;

public class WorkerService extends Service {


    Realm mRealm;

    public WorkerService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    private NotificationManager notifManager;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mRealm = Realm.getDefaultInstance();
        Log.e("onStartCommand", "-----------------....hererererereereereer....... " + intent);
        RealmResults<Reminder> itemsRealmResults = mRealm.where(Reminder.class).findAll();

//        String type = intent.getStringExtra("json");
        Log.e("itemsRealmResults", itemsRealmResults.size() + ".....");//+ type);

        if (itemsRealmResults.size() != 0) {
            for (int i = 0; i < itemsRealmResults.size(); i++) {


                Item item = itemsRealmResults.get(i).getItem();
                try {
                    String plannerId = itemsRealmResults.get(i).getPlannerId();
                    Log.e("Workersevie", plannerId + "....");
                    Log.e("Workersevie_obj", "..." + itemsRealmResults.get(0));
                    ObjRemainder objRemainder = item.getObjRemainder();
                    String reminderType = objRemainder.getStrType();
                    String reminderDate = objRemainder.getStrRemainderDate();

                    Date objDate = new Date(); // Current System Date and time is assigned to objDate
                    String strDateFormat = "hh:mm a"; //Date format is Specified
                    SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat); //Date format string is passed as an argument to the Date format object
                    String CurrntTime = objSDF.format(objDate);

                    Calendar mcurrentTime = Calendar.getInstance();
                    Date date = mcurrentTime.getTime();

                    String pattern = "yyyy-MM-dd";
                    android.icu.text.SimpleDateFormat gmtFormat = new android.icu.text.SimpleDateFormat(pattern);
                    String CurrentDate = gmtFormat.format(date);

                    if ((objRemainder.getStrRemainderTime().length() != 0) && (objRemainder.getStrRemainderDate().length() != 0)) {
                        String reminderTime = objRemainder.getStrRemainderTime().toUpperCase();

                        Date date1 = null;
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            date1 = format.parse(reminderDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        Boolean check_today = DateUtils.isToday(date1);

                        switch (reminderType) {

                            case "Daily":
                                Log.e("Daily", plannerId + "...." + reminderType + "....." + reminderTime + "..." + CurrntTime);
                                if (reminderTime.equals(CurrntTime)) {
                                    Log.e("Daily", "EQUAL");
                                    checkReminderTime(item);
                                }
                                break;

                            case "Weekly":
                                SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
                                Date d = new Date();
                                String day_cur = sdf.format(d);
                                String day_rem = sdf.format(date1);
                                Log.e("Weekly", plannerId + "...." + reminderType + ".." + day_cur + "..." + "..." + day_rem);

                                if (day_cur.equals(day_rem)) {
                                    if (reminderTime.equals(CurrntTime)) {
                                        Log.e("Weekly", "EQUAL_day.and time." + CurrntTime + "...." + reminderTime);
                                        createNotification("Note Reminder" + reminderTime, this, reminderTime, item.getIntPlannerId(), item.getStrTitle());
                                    }
                                }

                                break;

                            case "Monthly":
                                SimpleDateFormat month_date = new SimpleDateFormat("dd");
                                String dat_cur = month_date.format(mcurrentTime.getTime());
                                String dat_rem = (String) DateFormat.format("dd", date1);

                                Log.e("Monthly", plannerId + "...." + reminderType + ".." + dat_rem + "..." + "..." + dat_cur);

                                if (dat_cur.equals(dat_rem)) {
                                    if (reminderTime.equals(CurrntTime)) {
                                        Log.e("Monthly", "EQUAL_days...time equal");
                                        createNotification("Note Reminder" + reminderTime, this, reminderTime, item.getIntPlannerId(), item.getStrTitle());
                                    }
                                }

                                break;

                            case "Do not repeat":
                                Log.e("Do not repeat", plannerId + "...." + reminderType + "..");
                                getReminderDate(plannerId, check_today, item);
                                break;

                        }

                    } else {
                        Realm realm = Realm.getDefaultInstance();
                        final RealmResults<Reminder> realmResults = realm.where(Reminder.class).findAll();
                        Reminder reminder = realmResults.where().equalTo("plannerId", String.valueOf(plannerId)).findFirst();
                        Log.e("delete_reminder", "....." + reminder);
                        if (reminder != null) {
                            if (!realm.isInTransaction()) {
                                realm.beginTransaction();
                            }
                            reminder.deleteFromRealm();
                            realm.commitTransaction();
                        }
                    }
                } catch (NullPointerException e) {
                    Log.e("NullPointer Exception", e + "..");
                }

            }

        } else {
            Log.e("itemsReal_else", "else.....");
        }
        return super.onStartCommand(intent, flags, startId);

    }

    public void getReminderDate(String plannerId, Boolean check_today, Item item) {
        ObjRemainder objRemainder = item.getObjRemainder();
        String reminderDate = objRemainder.getStrRemainderDate();
        String reminderTime = objRemainder.getStrRemainderTime();

        if (check_today) {

            String newTime = getNewFormatedTime(reminderTime);
            Boolean isSame = DateUtils.getCurrentTimeUsingDate(newTime);

            if (isSame) {
                Log.e("date1_if", isSame + "....." + newTime + "..." + item.getIntPlannerId());
                createNotification("Note Reminder " + reminderTime, this, reminderTime, item.getIntPlannerId(), item.getStrTitle());

                Realm realm = Realm.getDefaultInstance();
                final RealmResults<Reminder> realmResults = realm.where(Reminder.class).findAll();

                Reminder reminder = realmResults.where().equalTo("plannerId", String.valueOf(plannerId)).findFirst();
                if (reminder != null) {
                    if (!realm.isInTransaction()) {
                        realm.beginTransaction();
                    }
                    reminder.deleteFromRealm();
                    realm.commitTransaction();
                }

            } else {
                Log.e("date1_else", isSame + "....." + newTime + "..." + item.getIntPlannerId());
            }

        }
    }

    public void checkReminderTime(Item item) {

        String reminderTime = item.getObjRemainder().getStrRemainderTime();
        String[] splitSpace = reminderTime.split(" ");
        String[] split1 = splitSpace[0].split(":");

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        int time1 = Integer.parseInt(split1[0]);
        if (splitSpace[0].equals("pm")) {
            time1 = time1 + 10;
        }

        calendar.set(Calendar.HOUR_OF_DAY, time1);
        calendar.set(Calendar.MINUTE, Integer.parseInt(split1[1]));

        createNotification("Note Reminder " + reminderTime, this, reminderTime, item.getIntPlannerId(), item.getStrTitle());

    }

    public void createNotification(String aMessage, Context context, String time, int plannerId, String titleNote) {

        Boolean isOnline = isOnline(getApplicationContext());
        final int NOTIFY_ID = 0; // ID of notification
        String id = context.getString(R.string.default_notification_channel_id_reminder); // default_channel_id
        String title = "Reminder"; // Default Channel
        Intent intent;
        PendingIntent pendingIntent;
        NotificationCompat.Builder builder;
        if (notifManager == null) {
            notifManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = notifManager.getNotificationChannel(id);
            if (mChannel == null) {
                mChannel = new NotificationChannel(id, title, importance);
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                notifManager.createNotificationChannel(mChannel);
            }

            builder = new NotificationCompat.Builder(context, id);
            intent = new Intent(context, NoteDetailActivity.class);
            intent.putExtra("type", "Notification");
            intent.putExtra("plannerId", String.valueOf(plannerId));

            if (isOnline) {
                intent.putExtra("status", "Online");
            } else intent.putExtra("status", "Offline");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
            builder.setContentTitle(aMessage)                            // required
                    .setSmallIcon(android.R.drawable.ic_popup_reminder)   // required
                    .setContentText(titleNote) // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setTicker(aMessage)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
        } else {

            builder = new NotificationCompat.Builder(context, id);
            intent = new Intent(context, NoteDetailActivity.class);
            intent.putExtra("type", "Notification");
//            intent.putExtra("listItem", "");// gson.toJson(item)

            intent.putExtra("plannerId", String.valueOf(plannerId));
            if (isOnline) {
                intent.putExtra("status", "Online");
            } else intent.putExtra("status", "Offline");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            int importance = NotificationCompat.PRIORITY_HIGH;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                importance = NotificationManager.IMPORTANCE_HIGH;
            }

            builder.setContentTitle(aMessage)                            // required
                    .setSmallIcon(android.R.drawable.ic_popup_reminder)   // required
                    .setContentText(titleNote) // required  context.getString(R.string.app_name)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setPriority(importance)
                    .setContentIntent(pendingIntent)
                    .setTicker(aMessage)
                    .setSound(defaultSoundUri)
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                    .setPriority(Notification.PRIORITY_HIGH);
        }

        Notification notification = builder.build();
        notifManager.notify(NOTIFY_ID, notification);


    }

    public String getNewFormatedTime(String oldDateString) {
        final String NEW_FORMAT = "HH:mm ";
        final String OLD_FORMAT = "hh:mm a";

//        String oldDateString = h + ":" + m;
        String newDateString = "";

        try {
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(OLD_FORMAT, getCurrentLocale());
            Date d = sdf.parse(oldDateString);
            sdf.applyPattern(NEW_FORMAT);
            newDateString = sdf.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return newDateString;
    }

    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
//        NetworkInfo netInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        //should check null because in airplane mode it will be null
        return (netInfo != null && netInfo.isConnected());
    }

    @TargetApi(Build.VERSION_CODES.N)
    public Locale getCurrentLocale() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return getResources().getConfiguration().getLocales().get(0);
        } else {
            //noinspection deprecation
            return getResources().getConfiguration().locale;
        }
    }

}
