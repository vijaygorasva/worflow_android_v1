package com.example.user.communicator.Model.Notes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import io.realm.annotations.PrimaryKey;

public class UndoCheckboxes {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("checkbox")
    @Expose
    private ArrayList<UndoCheckboxModel> undoCheckboxModel;

    public ArrayList<UndoCheckboxModel> getUndoCheckboxModel() {
        return undoCheckboxModel;
    }

    public void setUndoCheckboxModel(ArrayList<UndoCheckboxModel> undoCheckboxModel) {
        this.undoCheckboxModel = undoCheckboxModel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
