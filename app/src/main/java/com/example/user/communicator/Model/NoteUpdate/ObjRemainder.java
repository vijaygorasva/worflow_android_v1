
package com.example.user.communicator.Model.NoteUpdate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ObjRemainder {

    @SerializedName("blnFlage")
    @Expose
    private Boolean blnFlage;
    @SerializedName("strRemainderDate")
    @Expose
    private String strRemainderDate;
    @SerializedName("strRemainderTime")
    @Expose
    private String strRemainderTime;
    @SerializedName("strType")
    @Expose
    private String strType;

    public Boolean getBlnFlage() {
        return blnFlage;
    }

    public void setBlnFlage(Boolean blnFlage) {
        this.blnFlage = blnFlage;
    }

    public String getStrRemainderDate() {
        return strRemainderDate;
    }

    public void setStrRemainderDate(String strRemainderDate) {
        this.strRemainderDate = strRemainderDate;
    }

    public String getStrRemainderTime() {
        return strRemainderTime;
    }

    public void setStrRemainderTime(String strRemainderTime) {
        this.strRemainderTime = strRemainderTime;
    }

    public String getStrType() {
        return strType;
    }

    public void setStrType(String strType) {
        this.strType = strType;
    }

}
