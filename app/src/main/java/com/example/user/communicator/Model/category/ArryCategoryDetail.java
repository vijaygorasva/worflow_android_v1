
package com.example.user.communicator.Model.category;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ArryCategoryDetail extends RealmObject implements Parcelable, Comparable<ArryCategoryDetail> {


    public static final Parcelable.Creator<ArryCategoryDetail> CREATOR = new Parcelable.Creator<ArryCategoryDetail>() {
        @Override
        public ArryCategoryDetail createFromParcel(Parcel in) {
            return new ArryCategoryDetail(in);
        }

        @Override
        public ArryCategoryDetail[] newArray(int size) {
            return new ArryCategoryDetail[size];
        }
    };
    @PrimaryKey
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("strCategoryName")
    @Expose
    private String strCategoryName;
    @SerializedName("fkIntCategoryTypeId")
    @Expose
    private String fkIntCategoryTypeId;
    @SerializedName("strDefaultSub")
    @Expose
    private Boolean strDefaultSub;

    @SerializedName("intCreateUserId")
    @Expose
    private String intCreateUserId;
    @SerializedName("datCreateDateAndTime")
    @Expose
    private String datCreateDateAndTime;
    @SerializedName("intOrganisationId")
    @Expose
    private String intOrganisationId;
    @SerializedName("intProjectId")
    @Expose
    private String intProjectId;
    @SerializedName("intLastModifiedUserId")
    @Expose
    private String intLastModifiedUserId;
    @SerializedName("datLastModifiedDateAndTime")
    @Expose
    private String datLastModifiedDateAndTime;
    @SerializedName("strStatus")
    @Expose
    private String strStatus;


    protected ArryCategoryDetail(Parcel in) {
        id = in.readString();
        strCategoryName = in.readString();
        strDefaultSub = in.readByte() != 0;
        fkIntCategoryTypeId = in.readString();

        intCreateUserId = in.readString();
        datCreateDateAndTime = in.readString();
        intOrganisationId = in.readString();
        intProjectId = in.readString();
        intLastModifiedUserId = in.readString();
        datLastModifiedDateAndTime = in.readString();
        strStatus = in.readString();
    }

    @Override
    public int compareTo(ArryCategoryDetail o) {
        if (getDatCreateDateAndTime() == null || o.getDatCreateDateAndTime() == null)
            return 0;
        return getDatCreateDateAndTime().compareTo(o.getDatCreateDateAndTime());


//        return 0;
    }

    public static Creator<ArryCategoryDetail> getCREATOR() {
        return CREATOR;
    }

    public String getIntCreateUserId() {
        return intCreateUserId;
    }

    public void setIntCreateUserId(String intCreateUserId) {
        this.intCreateUserId = intCreateUserId;
    }

    public String getDatCreateDateAndTime() {
        return datCreateDateAndTime;
    }

    public void setDatCreateDateAndTime(String datCreateDateAndTime) {
        this.datCreateDateAndTime = datCreateDateAndTime;
    }

    public String getIntOrganisationId() {
        return intOrganisationId;
    }

    public void setIntOrganisationId(String intOrganisationId) {
        this.intOrganisationId = intOrganisationId;
    }

    public String getIntProjectId() {
        return intProjectId;
    }

    public void setIntProjectId(String intProjectId) {
        this.intProjectId = intProjectId;
    }

    public String getIntLastModifiedUserId() {
        return intLastModifiedUserId;
    }

    public void setIntLastModifiedUserId(String intLastModifiedUserId) {
        this.intLastModifiedUserId = intLastModifiedUserId;
    }

    public String getDatLastModifiedDateAndTime() {
        return datLastModifiedDateAndTime;
    }

    public void setDatLastModifiedDateAndTime(String datLastModifiedDateAndTime) {
        this.datLastModifiedDateAndTime = datLastModifiedDateAndTime;
    }

    public String getStrStatus() {
        return strStatus;
    }

    public void setStrStatus(String strStatus) {
        this.strStatus = strStatus;
    }

    public String getFkIntCategoryTypeId() {
        return fkIntCategoryTypeId;
    }

    public void setFkIntCategoryTypeId(String fkIntCategoryTypeId) {
        this.fkIntCategoryTypeId = fkIntCategoryTypeId;
    }

    public ArryCategoryDetail() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStrCategoryName() {
        return strCategoryName;
    }

    public void setStrCategoryName(String strCategoryName) {
        this.strCategoryName = strCategoryName;
    }

    public Boolean getStrDefaultSub() {
        return strDefaultSub;
    }

    public void setStrDefaultSub(Boolean strDefaultSub) {
        this.strDefaultSub = strDefaultSub;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {

        parcel.writeString(id);
        parcel.writeString(strCategoryName);
        parcel.writeByte((byte) (strDefaultSub ? 1 : 0));
        parcel.writeString(fkIntCategoryTypeId);
        parcel.writeString(intCreateUserId);
        parcel.writeString(datCreateDateAndTime);
        parcel.writeString(intOrganisationId);
        parcel.writeString(intProjectId);
        parcel.writeString(intLastModifiedUserId);
        parcel.writeString(datLastModifiedDateAndTime);
        parcel.writeString(strStatus);
    }

}
