package com.example.user.communicator.Activity.ChatModule;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.example.user.communicator.Activity.TaskModule.BaseActivity;
import com.example.user.communicator.R;

public class ImageScreenActivity extends BaseActivity {

    ImageView mImg, mSend;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_screen);

        mImg = findViewById(R.id.imgSelected);
        mSend = findViewById(R.id.send);

        final Uri uri = Uri.parse(getIntent().getStringExtra("uri"));
        mImg.setImageURI(uri);
        mSend.setOnClickListener(v -> {
            Intent i = new Intent();
            i.putExtra("uri", String.valueOf(uri));
            setResult(RESULT_OK, i);
            finish();
        });


    }
}
