package com.example.user.communicator.Utility;

import android.net.ParseException;
import android.text.TextUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


public class DateUtils extends android.text.format.DateUtils {

    public static String SQL_DATE_TIME_FORMAT_PATTERN = "yyyy-MM-dd HH:mm:ss";
    public static String SQL_DATE_FORMAT_PATTERNS = "dd MMM yyyy";
    public static String SQL_DATE_FORMAT_PATTERN = "yyyy-MM-dd";
    public static String SQL_TIME_FORMAT_PATTERN = "HH:mm:ss";
    public static String DIS_DATE_TIME_FORMAT_PATTERN = "MMM dd, yyyy hh:mm a";
    public static String DIS_DATE_FORMAT_PATTERN = "MMM/dd/yyyy";
    public static String DIS_TIME_FORMAT_PATTERN = "hh:mm a";

    public static String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                SQL_DATE_TIME_FORMAT_PATTERN, Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static String getFormattedDate(Date date, String format) {
        if (date == null || format == null) return null;

        String dateString = null;
        try {
            dateString = new SimpleDateFormat(format, Locale.getDefault()).format(date);
            // Log.v( TAG, "#parseDate dateDT: " + dateDT );
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dateString;
    }

    public static Date parseDate(String date) {

        if (TextUtils.isEmpty(date)) {
            return null;
        }

        StringBuffer sbDate = new StringBuffer();
        sbDate.append(date);
        String newDate = null;
        Date dateDT = null;

        try {
            newDate = sbDate.substring(0, 19).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String rDate = newDate.replace("T", " ");
        String nDate = rDate.replaceAll("-", "/");

        try {
            dateDT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.getDefault()).parse(nDate);
            // Log.v( TAG, "#parseDate dateDT: " + dateDT );
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dateDT;
    }

    public static Date parseDate(String date, String format) {
        if (TextUtils.isEmpty(date)) {
            return null;
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat(format, Locale.getDefault());
        try {
            Date date1 = dateFormat.parse(date);
            return date1;
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String formatDate(String inputFormat, String outputFormat, String inputDate) {

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, Locale.getDefault());


        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (java.text.ParseException e) {

        }

        return outputDate;

    }

    public static boolean isToday(Date date) {

        Calendar smsTime = Calendar.getInstance();
        smsTime.setTime(date);

        Calendar now = Calendar.getInstance();
        if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE)) {
//            Log.e("isToday",now.get(Calendar.DATE)+"....."+date);
            return true;
        }

        return false;
    }

    public static long getDaysBetweenDates(String start, String end) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
        Date startDate, endDate;
        long numberOfDays = 0;
        try {
            startDate = dateFormat.parse(start);
            endDate = dateFormat.parse(end);
            numberOfDays = getUnitBetweenDates(startDate, endDate, TimeUnit.DAYS);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return numberOfDays;
    }

    public static long getUnitBetweenDates(Date startDate, Date endDate, TimeUnit unit) {
        long timeDiff = endDate.getTime() - startDate.getTime();
        return unit.convert(timeDiff, TimeUnit.MILLISECONDS);
    }

    public static String currentDate() {
        String newDateStr = new SimpleDateFormat("dd MMM yyyy").format(new Date());
        return newDateStr;
    }
    public static Boolean getCurrentTimeUsingDate(String time) {
//        long elapsed=1;
        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        String formattedDate = dateFormat.format(date);

        Date d1 = null;
        try {
            d1 = dateFormat.parse(formattedDate);
            Date d2 = dateFormat.parse(time);
            long elapsed = d2.getTime() - d1.getTime();

            if (elapsed == 0) {
                return true;
            } else {
            }

        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        System.out.println("Current time of the day using Date - 12 hour format: " + formattedDate);
        return false;

    }

    public static boolean isYesterday(Date date) {

        Calendar smsTime = Calendar.getInstance();// your date
        smsTime.setTime(date);

        Calendar yes = Calendar.getInstance();// today
        yes.add(Calendar.DATE, -1);

        if (yes.get(Calendar.YEAR) == smsTime.get(Calendar.YEAR)
                && yes.get(Calendar.DATE) == smsTime.get(Calendar.DATE)) {
            return true;
        }
        return false;

    }

    public static boolean isTomorrow(Date date) {

        Calendar smsTime = Calendar.getInstance();// your date
        smsTime.setTime(date);

        Calendar yes = Calendar.getInstance();// today
        yes.add(Calendar.DATE, +1);

        if (yes.get(Calendar.YEAR) == smsTime.get(Calendar.YEAR)
                && yes.get(Calendar.DATE) == smsTime.get(Calendar.DATE)) {
            return true;
        }
        return false;
    }

    private String getMillsToDate(long mills) {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
        return df.format(new Date(mills));
    }

    private String getDiffrenece(long startDate, long endDate) {
        long difference = (endDate - startDate);
        long sec = difference / 1000 % 60;
        long min = difference / (60 * 1000) % 60;
        long hours = difference / (60 * 60 * 1000) % 24;
        long diffDays = difference / (24 * 60 * 60 * 1000);
        return (diffDays == 0 ? ((hours == 0 ? (min == 0 ? ("Just now") : (min + " min")) : (hours + " hrs"))) : (getMillsToDate(startDate)));
    }

    public static String totalTimeTaken(ArrayList<String> timestampsList) {
        long tm = 0;
        for (String tmp : timestampsList) {
            String[] arr = tmp.split(":");
            tm += Integer.parseInt(arr[2]);
            tm += 60 * Integer.parseInt(arr[1]);
            tm += 3600 * Integer.parseInt(arr[0]);
        }

        long hh = tm / 3600;
        tm %= 3600;
        long mm = tm / 60;
        tm %= 60;
        long ss = tm;
        System.out.println(format(hh) + ":" + format(mm) + ":" + format(ss));
        return format(hh) + ":" + format(mm) + ":" + format(ss);
    }

    private static String format(long s) {
        if (s < 10) return "0" + s;
        else return "" + s;
    }

}
