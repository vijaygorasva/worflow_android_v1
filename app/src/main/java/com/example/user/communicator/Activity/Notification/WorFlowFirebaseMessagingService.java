package com.example.user.communicator.Activity.Notification;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.example.user.communicator.Activity.BrodcastsModule.BroadCastDetailActivity;
import com.example.user.communicator.Activity.BrodcastsModule.BroadCastsActivity;
import com.example.user.communicator.Activity.TaskModule.DetailsActivity.DetailsTabActivity;
import com.example.user.communicator.Activity.TaskModule.Task.TaskTabActivity;
import com.example.user.communicator.Activity.TaskModule.TaskDetailsActivity;
import com.example.user.communicator.Activity.TaskModule.TasksActivity;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.Notes.Item;
import com.example.user.communicator.Model.Notes.ObjRemainder;
import com.example.user.communicator.Model.Reminder.Reminder;
import com.example.user.communicator.R;
import com.example.user.communicator.Service.WorkerService;
import com.example.user.communicator.Utility.Constant;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;

public class WorFlowFirebaseMessagingService extends FirebaseMessagingService {
    public static final int REQUEST_CODE_ALARM = 0;
    private final String TAG = "MyFirebaseMsgService";

    //    private WorkerService broadcaster;
    @Override
    public void onNewToken(String s) {
        Constant.DEVICE_TOKEN = s;
        super.onNewToken(s);
    }

//    @Override
//    public void onCreate() {
//        super.onCreate();
//        broadcaster = WorkerService();
//    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Realm mRealm = Realm.getDefaultInstance();
        if (remoteMessage.getData().size() > 0) {

            Log.e(TAG, "Message data payload: " + remoteMessage.getData());
//            RemoteMessage.Notification notification = remoteMessage.getNotification();
//            String s = remoteMessage.getData().toString();
//                JSONObject json = new JSONObject(stringStringMap.get("timelockData"));
//                String type = stringStringMap.get("type");

            Map<String, String> stringStringMap = remoteMessage.getData();
            String type = stringStringMap.get("type");

            Log.e("Message_Type", type + "");

            if (isTaskNotification(type)) {
                String intUserId = stringStringMap.get("intUserId");
                Datum mUserDetails = getLoginData();
                if (intUserId.equalsIgnoreCase(mUserDetails.getIntUserId())) {
                    sendNotification(type, intUserId, stringStringMap);
                }
            } else if (type.equalsIgnoreCase("broadcast") || type.equalsIgnoreCase("broadcastReply")) {
                String broadcastId = stringStringMap.get("broadcastId");
                sendNotification(type, broadcastId, stringStringMap);
            } else if (type.equalsIgnoreCase("note_remainder")) {

                String palnnerId = stringStringMap.get("plannerId");
                String action = stringStringMap.get("action");

                RealmResults<Reminder> itemsRealmResults = mRealm.where(Reminder.class).findAll();
                Log.e("itemsRealmResults", itemsRealmResults.size() + ".....");

                if (action.equals("add")) {

                    try {

                        JSONObject body = new JSONObject(stringStringMap.get("body"));
                        Gson gson = new Gson();
                        Item item = gson.fromJson(body.toString(), Item.class);
                        String planer_ID = String.valueOf(item.getIntPlannerId());
                        ObjRemainder obj = item.getObjRemainder();
                        obj.set_id(itemsRealmResults.size() + 1);

                        Reminder reminderNew = new Reminder();
                        reminderNew.set_id(itemsRealmResults.size() + 1);
                        reminderNew.setPlannerId(String.valueOf(planer_ID));
                        Item itemNew = item;
                        itemNew.setObjRemainder(obj);
                        reminderNew.setItem(itemNew);

                        Log.e("reminderNe ", new Gson().toJson(reminderNew) + "......." + planer_ID);
                        mRealm.beginTransaction();
                        mRealm.copyToRealmOrUpdate(reminderNew);
                        mRealm.commitTransaction();

                        RealmResults<Reminder> itemsRealmResults11 = mRealm.where(Reminder.class).findAll();
//                            RealmResults<Reminder> itemsRealmResults11 = mRealm.where(Reminder.class).equalTo("plannerId", planer_ID).findAll();
                        Log.e("itemsRealmResults6 ", itemsRealmResults11.size() + ".." + planer_ID);

                        Intent intent = new Intent(this, WorkerService.class);
                        intent.setAction(Intent.ACTION_MAIN);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startService(intent);

                        PendingIntent pendingIntent = PendingIntent.getService(this, REQUEST_CODE_ALARM, intent, 0);
                        int alarmType = AlarmManager.ELAPSED_REALTIME;
                        final int FIFTEEN_SEC_MILLIS = 15000;
                        AlarmManager alarmManager = (AlarmManager) this.getSystemService(this.ALARM_SERVICE);
//                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, ti,
//                            1000 * 60 * 2, pendingIntent);
                        alarmManager.setRepeating(alarmType, SystemClock.elapsedRealtime(), 30 * 1000, pendingIntent);//1*60*1000

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if (action.equals("update")) {

                    Log.e("action2", action + "..");
                    try {

                        JSONObject body = new JSONObject(stringStringMap.get("body"));
                        Log.e("body", body + "..");
                        Gson gson = new Gson();
                        Item item = gson.fromJson(body.toString(), Item.class);
                        String planer_ID = String.valueOf(item.getIntPlannerId());
                        ObjRemainder obj = item.getObjRemainder();
                        obj.set_id(itemsRealmResults.size() + 1);

                        Reminder reminderNew = new Reminder();
                        reminderNew.set_id(itemsRealmResults.size() + 1);
                        reminderNew.setPlannerId(planer_ID);
                        Item itemNew = item;
                        itemNew.setObjRemainder(obj);
                        reminderNew.setItem(itemNew);
                        Log.e("reminderNe ", new Gson().toJson(reminderNew) + "......." + planer_ID);
                        mRealm.beginTransaction();
                        mRealm.copyToRealmOrUpdate(reminderNew);
                        mRealm.commitTransaction();
                        RealmResults<Reminder> itemsRealmResults22 = mRealm.where(Reminder.class).equalTo("plannerId", planer_ID).findAll();
                        Log.e("itemsRealmResults22 ", itemsRealmResults22.size() + ".." + planer_ID + "..." + itemsRealmResults22);

                        RealmResults<Reminder> itemsRealmResults111 = mRealm.where(Reminder.class).findAll();
                        Log.e("itemsRealmResults111", itemsRealmResults111.size() + ".....");

                        Intent intent = new Intent(this, WorkerService.class);
                        intent.setAction(Intent.ACTION_MAIN);
                        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        startService(intent);

                        PendingIntent pendingIntent = PendingIntent.getService(this, REQUEST_CODE_ALARM, intent, 0);
                        int alarmType = AlarmManager.ELAPSED_REALTIME;
                        final int FIFTEEN_SEC_MILLIS = 15000;
                        AlarmManager alarmManager = (AlarmManager) this.getSystemService(this.ALARM_SERVICE);
//                    alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, ti,
//                            1000 * 60 * 2, pendingIntent);
                        alarmManager.setRepeating(alarmType, SystemClock.elapsedRealtime(), 30 * 1000, pendingIntent);//1*60*1000

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if (action.equals("delete")) {
                    Log.e("action3", action + "..");
//                    RealmResults<Reminder> itemsRealmResults33 = mRealm.where(Reminder.class).findAll();
//                    RealmResults<Reminder> itemsRealmResults22 = mRealm.where(Reminder.class).equalTo("_id", id).findAll();
//                    Log.e("itemsRealmResults22", itemsRealmResults22.size() + ".." + itemsRealmResults33.size());
//
//                    for (int i = 0; i < itemsRealmResults22.size(); i++) {
//                        Reminder reminder = itemsRealmResults22.get(i);
//                        String plannerId = reminder.getPlannerId();
//                        if (plannerId.equals(id)) {
//                            mRealm.beginTransaction();
//                            mRealm.copyToRealmOrUpdate(reminderNew);
//                            mRealm.commitTransaction();
//                        }
//                    }

                }

            }
        }

        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

    }

    private boolean isTaskNotification(String type) {
        return type.equalsIgnoreCase("task") || type.equalsIgnoreCase("task_update")
                || type.equalsIgnoreCase("task_add")
                || type.equalsIgnoreCase("task_add_comment")
                || type.equalsIgnoreCase("task_add_reply")
                || type.equalsIgnoreCase("add_sub_task")
                || type.equalsIgnoreCase("task_start")
                || type.equalsIgnoreCase("task_end")
                || type.equalsIgnoreCase("task_status_changed")
                || type.equalsIgnoreCase("remove_sub_task");
    }

    private void sendNotification(String type, String messageBody, Map<String, String> stringStringMap) {

        Intent intent = null;
        if (isTaskNotification(type)) {
            String intUserId = stringStringMap.get("intUserId");
            String intTaskDocumentNo = stringStringMap.get("intTaskDocumentNo");
            intent = new Intent(this, DetailsTabActivity.class);
            int _id = Integer.parseInt(intTaskDocumentNo);
            intent.putExtra("isAppIsInBackground", isAppIsInBackground(this));
            intent.putExtra("isFromNotification", true);
            intent.putExtra("intUserId", intUserId);
            intent.putExtra("_id", _id);
            String BROADCAST_ACTION = "com.example.user.communicator";
            Intent broadcast = new Intent();
            broadcast.setAction(BROADCAST_ACTION);
            broadcast.addCategory(Intent.CATEGORY_DEFAULT);
            broadcast.putExtra("intUserId", intUserId);
            broadcast.putExtra("intTaskDocumentNo", intTaskDocumentNo);
            broadcast.putExtra("type", type);
            sendBroadcast(broadcast);
        } else if (type.equalsIgnoreCase("broadcast") || type.equalsIgnoreCase("broadcastReply")) {
            if (type.equalsIgnoreCase("broadcast")) {
                intent = new Intent(this, BroadCastsActivity.class);
            }
            if (type.equalsIgnoreCase("broadcastReply")) {
                intent = new Intent(this, BroadCastDetailActivity.class);
            }
            intent.putExtra("broadcastId", messageBody);
            if (isAppIsInBackground(this)) {
                intent.putExtra("isFromNotification", true);
            } else {
                intent.putExtra("isFromNotification", false);
            }
        }

        if (intent == null) {
            intent = new Intent();
        }

//        String message = (type.equalsIgnoreCase("broadcast") ? "New Broadcast Received!" : (type.equalsIgnoreCase("broadcastReply") ? "New Reply Received!" : ""));
        String title = stringStringMap.get("title");
        String message = stringStringMap.get("body");

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        int num = (int) System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getActivity(this, num /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        int importance = NotificationCompat.PRIORITY_HIGH;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            importance = NotificationManager.IMPORTANCE_HIGH;
        }
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setPriority(importance)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_HIGH);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }

        if (notificationManager != null) {
            notificationManager.notify(num /* ID of notification */, notificationBuilder.build());
        }


    }

    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

    public Datum getLoginData() {
        SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences("Login", MODE_PRIVATE);
        String data = sharedpreferences.getString("UserData", null);
        if (!TextUtils.isEmpty(data)) {
            Gson gson = new Gson();
//            return gson.fromJson(data, Login.class);
            return gson.fromJson(data, Datum.class);
        }
        return null;
    }

}
