
package com.example.user.communicator.Model.Colleborate;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ColleborateDatum extends RealmObject implements Parcelable {
    public static final Creator<ColleborateDatum> CREATOR = new Creator<ColleborateDatum>() {
        @Override
        public ColleborateDatum createFromParcel(Parcel in) {
            return new ColleborateDatum(in);
        }

        @Override
        public ColleborateDatum[] newArray(int size) {
            return new ColleborateDatum[size];
        }
    };
    @PrimaryKey
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("itemName")
    @Expose
    private String itemName;
    @SerializedName("imgPic")
    @Expose
    private String imgPic;
    @SerializedName("category")
    @Expose
    private String category;
    private boolean isSelected = false;

    public ColleborateDatum() {
    }

    public ColleborateDatum(Parcel in) {
        id = in.readString();
        itemName = in.readString();
        imgPic = in.readString();
        category = in.readString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getImgPic() {
        return imgPic;
    }

    public void setImgPic(String imgPic) {
        this.imgPic = imgPic;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(id);
        parcel.writeString(itemName);
        parcel.writeString(imgPic);
        parcel.writeString(category);

    }
}
