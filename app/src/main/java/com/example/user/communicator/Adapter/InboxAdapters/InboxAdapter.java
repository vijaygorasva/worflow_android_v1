package com.example.user.communicator.Adapter.InboxAdapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.InboxModel.Inbox;
import com.example.user.communicator.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InboxAdapter extends RecyclerView.Adapter<InboxAdapter.ViewHolder> {


    private ArrayList<Inbox> inboxArrayList;
    Context context;
    private OnClickListner onClick;
    private OnBottomListner onBottom;

    private final String URL_ROOT = "http://52.66.249.75:9999/api/";


    public InboxAdapter(Context context, ArrayList<Inbox> taskItems) {
        this.inboxArrayList = taskItems;
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_item_inbox, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (position == inboxArrayList.size() - 1) {
            if (onBottom != null) {
                onBottom.OnBottom();
            }
        }

        holder.bind(inboxArrayList.get(position), holder);
    }

    @Override
    public int getItemCount() {
        return inboxArrayList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cv)
        CardView cardView;

        @BindView(R.id.title)
        CustomTextView mTitle;

        @BindView(R.id.body)
        CustomTextView mBody;

        @BindView(R.id.time)
        CustomTextView time;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(Inbox item, ViewHolder holder) {

            if (item.getAdd_time() != 0) {
                String diff = getDiffrenece(item.getAdd_time(), System.currentTimeMillis());
                if (!TextUtils.isEmpty(diff)) {
                    time.setText(diff);
                } else {
                    time.setText("");
                }
            } else {
                time.setText("");
            }
            if (!TextUtils.isEmpty(item.getNotificationData().getTitle())) {
                mTitle.setText(item.getNotificationData().getTitle());
            }
            if (!TextUtils.isEmpty(item.getNotificationData().getBody())) {
                mBody.setText(item.getNotificationData().getBody());
            }

            cardView.setOnClickListener(v -> {
                if (onClick != null) {
                    onClick.OnClick(getAdapterPosition());
                }
            });
        }
    }

    private String getMillsToDate(long mills) {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy hh:mm a", Locale.US);
        return df.format(new Date(mills));
    }

    private String getDiffrenece(long startDate, long endDate) {
        long difference = (endDate - startDate);
        long sec = difference / 1000 % 60;
        long min = difference / (60 * 1000) % 60;
        long hours = difference / (60 * 60 * 1000) % 24;
        long diffDays = difference / (24 * 60 * 60 * 1000);
        return (diffDays == 0 ? ((hours == 0 ? (min == 0 ? ("Just now") : (min + " min")) : (hours + " hrs"))) : (getMillsToDate(startDate)));
    }

    public interface OnClickListner {
        void OnClick(int position);
    }

    public void setOnClickListner(OnClickListner onClickListner) {
        this.onClick = onClickListner;
    }

    public interface OnBottomListner {
        void OnBottom();

    }

    public void setOnBottomListner(OnBottomListner onBottomListner) {
        this.onBottom = onBottomListner;
    }

}
