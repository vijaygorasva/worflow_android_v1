package com.example.user.communicator.Model.Notes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class UndoTextsModel extends RealmObject {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("Value")
    @Expose
    private String strNoteListValue;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStrNoteListValue() {
        return strNoteListValue;
    }

    public void setStrNoteListValue(String strNoteListValue) {
        this.strNoteListValue = strNoteListValue;
    }


}
