package com.example.user.communicator.Activity.TaskModule.DetailsActivity;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.TaskModels.SelectionObject;
import com.example.user.communicator.R;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.icu.util.ULocale.getName;

public class BaseFragment extends Fragment {

    //    public final String URL_ROOT = "http://worflow.com:9999/api/";
//    public final String URL_ROOT = "http://52.66.249.75:9999/api/";
//    public final String BASE_URL = "http://52.66.249.75:9999/";

    public final String URL_ImageUpload_SERVER = "http://52.66.249.75:9999/uploads/";
    public final String URL_ROOT_SERVER = "http://52.66.249.75:9999/api/";//52.66.249.75
    public final String BASE_URL_SEVER = "http://52.66.249.75:9999/";

    public final String URL_ImageUpload_LOCAL = "http://:9999/uploads/";
    public final String URL_ROOT_local = "http://:9999/api/";//52.66.249.75
    public final String BASE_URL_local = "http://:9999/";

    public final String URL_ROOT_TEST = "http://13.232.37.104:9999/api/";//http://13.232.37.104:9999
    public final String URL_ImageUpload_ROOT_TEST = "http://13.232.37.104:9999/uploads/";
    public final String BASE_URL_TEST = "http://13.232.37.104:9999/";

    public final String URL_ROOT = URL_ROOT_TEST;
    public final String URL_ImageUpload_ROOT = URL_ImageUpload_ROOT_TEST;
    public final String BASE_URL = BASE_URL_TEST;


    public final String getUsers = URL_ROOT + "connection/getconnectionuser";
    public final String getLevels = URL_ROOT + "connection/getconnectionlevel";
    public final String getMembers = URL_ROOT + "connection/getconnectionmembers";
    public final String addTask = URL_ROOT + "task/savetaskdata";
    public final String deleteTask = URL_ROOT + "task/delete";
    public final String getAssignedTask = URL_ROOT + "task/getAssignedTasksData";
    public final String getMyTask = URL_ROOT + "task/getMyTasksData";
    public final String getTaskData = URL_ROOT + "task/getTaskData";
    public final String getTaskviewed = URL_ROOT + "task/viewed";
    public final String startTask = URL_ROOT + "task/start";
    public final String addComment = URL_ROOT + "task/add_comment";
    public final String endTask = URL_ROOT + "task/end";
    public final String updateTask = URL_ROOT + "task/updatetaskdata";
    public final String changeStatus = URL_ROOT + "task/change_status";
    public final String getBrodcastsList = URL_ROOT + "broadcast/list";
    public final String getBroadcastsInboxList = URL_ROOT + "broadcast/inbox";
    public final String saveBrodcast = URL_ROOT + "broadcast/save";
    public final String getReplies = URL_ROOT + "broadcast/reply/list";
    public final String getBroadcastsDetails = URL_ROOT + "broadcast/details";
    public final String addSubTaskData = URL_ROOT + "task/add_subtask";
    public final String removeSubTaskData = URL_ROOT + "task/remove_subtask";
    public final String add_reply = URL_ROOT + "task/add_reply";
    public final String logout = URL_ROOT + "log/logout";
    public final String searchMyTask = URL_ROOT + "task/my_task_search";
    public final String searchAssignedTask = URL_ROOT + "task/assigned_task_search";

    public final String changeProgressStatus = URL_ROOT + "task/onChangeProgress";
    public final String ANDROID = "ANDROID";

    private String TAG = "BaseActivity";
    public ProgressDialog pDialog;
    public final int IMAGE_PICKER_REQUEST_CODE = 10001;
    public final String DOCUMENTS_DIR = "documents";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public void showToast1(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public void showToast(String message) {
        LayoutInflater inflater = getLayoutInflater();
        View layouttoast = inflater.inflate(R.layout.toastcustom, (ViewGroup) getActivity().findViewById(R.id.toastcustom));
        ((TextView) layouttoast.findViewById(R.id.texttoast)).setText(message);

        Toast mytoast = new Toast(getActivity());
        mytoast.setView(layouttoast);
        mytoast.setDuration(Toast.LENGTH_LONG);
        mytoast.setGravity(Gravity.CENTER, 0, 0);
        mytoast.show();
//        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public enum ApiCall {
        GET_USERS_DATA, GET_LEVELS, GET_MEMBER_TYPES, ADD_TASK, GET_ASSIGNED_TASK, GET_MYTASK,
        GET_BRODCASTS_LIST, GET_BROADCASTS_INBOX_LIST, SAVE_BRODCAST, ADD_COMMENT, GET_TASK_DATA,
        GET_BROADCASTS_DETAILS, GET_REPLIES, LOGOUT, ADD_SUBTASK_DATA, REMOVE_SUB_TASK_DATA, ADD_REPLY, UPDATE_PRIORITY,
        Get_TASK_VIEWED, SEARCH_MY_TASKS, SEARCH_ASSIGNED_TASK, REMOVE_TASK, CHANGE_STATUS, START_TASK, END_TASK,
        CHANGE_PROGRESS_STATUS
    }

    public String getCurruntDate() {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        return df.format(Calendar.getInstance().getTime());

    }

    public String getMillsToDate(String mills) {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        return df.format(new Date(mills));
    }


    public long stringToMills(String mDate) {
        SimpleDateFormat df = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        Date date = null;
        try {
            date = df.parse(mDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long millis = 0;
        if (date != null) {
            millis = date.getTime();
        }
        return millis;
    }

    public Datum getLoginData() {
        SharedPreferences sharedpreferences = getActivity().getApplicationContext().getSharedPreferences("Login", getActivity().MODE_PRIVATE);
        String data = sharedpreferences.getString("UserData", null);
        if (!TextUtils.isEmpty(data)) {
            Gson gson = new Gson();
//            return gson.fromJson(data, Login.class);
            return gson.fromJson(data, Datum.class);
        }
        return null;
    }

    public boolean isNetworkAvailable() {
        if (getActivity() != null) {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        } else {
            return true;
        }
    }

    public void showProgressDialog(String message) {
        try {
            if (pDialog == null || !pDialog.isShowing()) {
                pDialog = new ProgressDialog(getActivity());
                pDialog.setMessage(message);
                pDialog.setCancelable(false);
                pDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideProgressDialog() {
        try {
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.dismiss();
                pDialog = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public SharedPreferences getSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(getActivity());
    }

    public SelectionObject getSelectionItem() {
        String appInfo = getSharedPreferences().getString("SelectionObject", "");
        Gson gson = new Gson();
        if (!TextUtils.isEmpty(appInfo)) {
            return gson.fromJson(appInfo, SelectionObject.class);
        }
        return new SelectionObject();
    }

    public void setSelectionItem(SelectionObject object) {
        SharedPreferences.Editor editor1 = getSharedPreferences().edit();
        Gson gson = new Gson();
        editor1.putString("SelectionObject", gson.toJson(object));
        editor1.apply();
    }


    public void showNetworkAlert() {
        alert(getActivity(), "Network Error", "No network connected");
    }

    public void alert(Context context, String title, String message) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    public void ApiCall(String url, final ApiCall type, RequestParams requestParams) {

        try {

            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(60 * 1000);
            client.setURLEncodingEnabled(true);
            client.post(getActivity(), url, requestParams, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        if (response.getBoolean("success")) {
                            OnResponce(response, type);
                        } else {
                            OnError(response, type);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    hideProgressDialog();
                    try {
                        showToast(errorResponse.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    OnError(errorResponse, type);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    hideProgressDialog();
                    OnError(null, type);
                    showToast(responseString);
                }
            });
        } catch (Exception e) {
            hideProgressDialog();
            e.printStackTrace();
        }
    }

    public void ApiCallWithToken(String url, final ApiCall type, RequestParams requestParams) {

        try {

            SharedPreferences sharedpreferences = getActivity().getSharedPreferences("Login", getActivity().MODE_PRIVATE);
            String data = sharedpreferences.getString("UserData", null);
            String token = sharedpreferences.getString("Token", null);
            Gson gson = new Gson();
//            Login userData = gson.fromJson(data, Login.class);

            Datum userData = gson.fromJson(data, Datum.class);
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(60 * 1000);
            if (userData != null) {
                client.addHeader("Authorization", "Bearer " + token);

                requestParams.put("intProjectId", userData.getArryselectedProjectAllItems().get(0).getId());
                requestParams.put("intOrganisationId", userData.getArryselectedOrganisationAllItems().get(0).getId());
            }

//            ByteArrayEntity entity = new ByteArrayEntity(requestParams.toString().getBytes("UTF-8"));
//            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

            client.setURLEncodingEnabled(true);
            client.post(getActivity(), url, requestParams, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        if (response.getBoolean("success")) {
                            OnResponce(response, type);
                        } else {
                            OnError(response, type);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    hideProgressDialog();
                    try {
                        showToast(errorResponse.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    OnError(errorResponse, type);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    hideProgressDialog();
                    showToast(responseString);
                }
            });
        } catch (Exception e) {
            hideProgressDialog();
            e.printStackTrace();
        }
    }

    public void OnResponce(JSONObject object, ApiCall type) {
        String s = object.toString();
    }

    public void OnError(JSONObject object, ApiCall type) {
        String s1 = object.toString();
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public String getPath(final Context context, final Uri uri) {
        if (uri == null) return null;

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {

            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
//                final Uri contentUri = ContentUris.withAppendedId(
//                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
//
//                return getDataColumn(context, contentUri, null, null);

                if (id != null && id.startsWith("raw:")) {
                    return id.substring(4);
                }

                String[] contentUriPrefixesToTry = new String[]{
                        "content://downloads/public_downloads",
                        "content://downloads/my_downloads",
                        "content://downloads/all_downloads"
                };

                for (String contentUriPrefix : contentUriPrefixesToTry) {
                    Uri contentUri = ContentUris.withAppendedId(Uri.parse(contentUriPrefix), Long.valueOf(id));
                    try {
                        String path = getDataColumn(context, contentUri, null, null);
                        if (path != null) {
                            return path;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                // path could not be retrieved using ContentResolver, therefore copy file to accessible cache using streams
                String fileName = getFileName(context, uri);
                File cacheDir = getDocumentCacheDir(context);
                File file = generateFileName(fileName, cacheDir);
                String destinationPath = null;
                if (file != null) {
                    destinationPath = file.getAbsolutePath();
                    saveFileFromUri(context, uri, destinationPath);
                }

                return destinationPath;
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            } else if (isGoogleDriveDocument(uri)) {
                return getDriveFilePath(uri, context);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private static void saveFileFromUri(Context context, Uri uri, String destinationPath) {
        InputStream is = null;
        BufferedOutputStream bos = null;
        try {
            is = context.getContentResolver().openInputStream(uri);
            bos = new BufferedOutputStream(new FileOutputStream(destinationPath, false));
            byte[] buf = new byte[1024];
            is.read(buf);
            do {
                bos.write(buf);
            } while (is.read(buf) != -1);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (is != null) is.close();
                if (bos != null) bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Nullable
    public static File generateFileName(@Nullable String name, File directory) {
        if (name == null) {
            return null;
        }

        File file = new File(directory, name);

        if (file.exists()) {
            String fileName = name;
            String extension = "";
            int dotIndex = name.lastIndexOf('.');
            if (dotIndex > 0) {
                fileName = name.substring(0, dotIndex);
                extension = name.substring(dotIndex);
            }

            int index = 0;

            while (file.exists()) {
                index++;
                name = fileName + '(' + index + ')' + extension;
                file = new File(directory, name);
            }
        }

        try {
            if (!file.createNewFile()) {
                return null;
            }
        } catch (IOException e) {
            return null;
        }


        return file;
    }

    public File getDocumentCacheDir(@NonNull Context context) {
        File dir = new File(context.getCacheDir(), DOCUMENTS_DIR);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dir;
    }

    public String getPath2(final Context context, final Uri uri) {
        String absolutePath = getPath(context, uri);
        return absolutePath != null ? absolutePath : uri.toString();
    }

    public String getFileName(@NonNull Context context, Uri uri) {
        String mimeType = context.getContentResolver().getType(uri);
        String filename = null;

        if (mimeType == null) {
            String path = getPath2(context, uri);
            if (path == null) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    filename = getName(uri.toString());
                }
            } else {
                File file = new File(path);
                filename = file.getName();
            }
        } else {
            Cursor returnCursor = context.getContentResolver().query(uri, null,
                    null, null, null);
            if (returnCursor != null) {
                int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                returnCursor.moveToFirst();
                filename = returnCursor.getString(nameIndex);
                returnCursor.close();
            }
        }

        return filename;
    }

    private String getDataColumn(Context context, Uri uri, String selection,
                                 String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    private String getDriveFilePath(Uri uri, Context context) {
        showProgressDialog("Please wait...");
        Cursor returnCursor = context.getContentResolver().query(uri, null, null, null, null);
        /*
         * Get the column indexes of the data in the Cursor,
         *     * move to the first row in the Cursor, get the data,
         *     * and display it.
         * */
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
        returnCursor.moveToFirst();
        String name = (returnCursor.getString(nameIndex));
        if (!name.endsWith("pdf")) {
            name = name + ".pdf";
        }
        String size = (Long.toString(returnCursor.getLong(sizeIndex)));
        File file = generateFileName(name, context.getCacheDir());
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(uri);
            FileOutputStream outputStream = new FileOutputStream(file);
            int read = 0;
            int maxBufferSize = 1024 * 1024;
            int bytesAvailable = inputStream.available();

            //int bufferSize = 1024;
            int bufferSize = Math.min(bytesAvailable, maxBufferSize);

            final byte[] buffers = new byte[bufferSize];
            while ((read = inputStream.read(buffers)) != -1) {
                outputStream.write(buffers, 0, read);
            }
            Log.e("File Size", "Size " + file.length());
            inputStream.close();
            outputStream.close();
            Log.e("File Path", "Path " + file.getPath());
            Log.e("File Size", "Size " + file.length());
        } catch (Exception e) {
            Log.e("Exception", e.getMessage());
        }
        hideProgressDialog();
        return file.getAbsolutePath();
    }

    public boolean isGoogleDriveDocument(Uri uri) {
        return "com.google.android.apps.docs.storage".equals(uri.getAuthority()) ||
                "com.google.android.apps.docs.storage.legacy".equals(uri.getAuthority());
    }

    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    public Bitmap compressedBitmap(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] BYTE;
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        BYTE = byteArrayOutputStream.toByteArray();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;
        return BitmapFactory.decodeByteArray(BYTE, 0, BYTE.length, options);
    }

    public String storeImage(Bitmap image) {

        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            return null;
        }
        try {
            String s = getSize(String.valueOf(image.getWidth()), String.valueOf(image.getHeight()));
            String size = getSize(s.substring(0, s.indexOf(",")), s.substring(s.indexOf(",") + 1));
            image = Bitmap.createScaledBitmap(image, Integer.parseInt(size.substring(0, size.indexOf(","))), Integer.parseInt(size.substring(size.indexOf(",") + 1)), true);
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.JPEG, 70, fos);
            fos.close();
            return pictureFile.getAbsolutePath();
        } catch (FileNotFoundException e) {
            return null;
        } catch (IOException e) {
            return null;
        }
    }

    public String getSize(String _width, String _height) {
        int width = Integer.parseInt(_width);
        int height = Integer.parseInt(_height);
        if (width > height) {
            if (width > 720) {
                height = (height * 720) / width;
                width = 720;
            }
            return width + "," + height;
        } else {
            if (height > 720) {
                width = (width * 720) / height;
                height = 720;
            }
            return width + "," + height;
        }
    }

    public File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getActivity().getPackageName()
                + "/Files");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss", Locale.US).format(new Date());
        File mediaFile;
        String mImageName = "MI_" + timeStamp + ".jpeg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public boolean mayRequest() {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || getActivity().checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }
}
