package com.example.user.communicator.Fragment;

import android.app.Activity;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.user.communicator.Adapter.PlannerAdapter.OperationAdapter;
import com.example.user.communicator.Application;
import com.example.user.communicator.CustomViews.CustomEditText;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Dialog.AllCategoryListDialog;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.Operation.ListSubCategory.DatumSubCat;
import com.example.user.communicator.R;
import com.example.user.communicator.Utility.EELViewUtils;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

public class OperationsFragment extends BasicFragment implements AdapterView.OnItemSelectedListener, OperationAdapter.Callback {//}, DynamicCategoryFragment.OnCategorySelectedListener  {

    @BindView(R.id.flMainContent)
    LinearLayout flMainContent;
    @BindView(R.id.rvOperationList)
    RecyclerView rvOperationList;
    @BindView(R.id.cvTasks)
    CardView cvTasks;
    public static ArrayList<DatumSubCat> deleteList = new ArrayList<>();
    @BindView(R.id.tvTypeCategory)
    CustomTextView tvTypeCategory;
    @BindView(R.id.tvMainCategory)
    CustomTextView tvMainCategory;
    @BindView(R.id.tvSubCategory)
    CustomTextView tvSubCategory;
    @BindView(R.id.llNodata)
    LinearLayout llNodata;
    private static int itemPosition;

    OperationAdapter operationAdapter;
    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;
    ArrayList<DatumSubCat> arrayList = new ArrayList<>();
    EELViewUtils mEelViewUtils;
    Realm mRealm;
    String strUserId;
    Datum mUserDetails;
    @BindView(R.id.etTitle)
    CustomEditText etTitle;
    @BindView(R.id.fabtnDelete)
    FloatingActionButton fabtnDelete;

    public static Boolean isConnected = false;
    private boolean internetConnected = true;


    public static String strCategoryTypeId,
            strCategoryId,
            strSubCategoryId;

    public String strCategoryTypeName, strCategoryName, strSubCategoryName;
    AllCategoryListDialog allCategoryListDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_operations, container, false);
        ButterKnife.bind(this, view);
        mEelViewUtils = new EELViewUtils(flMainContent, rvOperationList);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        arrayList = new ArrayList<>();
        operationAdapter = new OperationAdapter(getActivity(), arrayList, rvOperationList, this);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (!isConnected) {
            String status = Application.getConnectivityStatusString(getActivity());
            setSnackbarMessage(status, false);
        }

        SharedPreferences sharedpreferences = getContext().getApplicationContext().getSharedPreferences("Login", 0);
        String data = sharedpreferences.getString("UserData", null);
        Gson gson = new Gson();
        mUserDetails = gson.fromJson(data, Datum.class);
        strUserId = mUserDetails.getIntUserId();
        mRealm = Realm.getDefaultInstance();
        cvTasks.setVisibility(View.GONE);

        strCategoryTypeId = "";
        strCategoryId = "";
        strSubCategoryId = "";

        tvTypeCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvMainCategory.setVisibility(View.GONE);
                tvSubCategory.setVisibility(View.GONE);
                catChange(1);
            }
        });

        tvMainCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvSubCategory.setVisibility(View.GONE);
                catChange(2);
            }
        });

        tvSubCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                catChange(3);
            }
        });

        cvTasks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etTitle.setFocusable(true);
                etTitle.setFocusableInTouchMode(true);
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvOperationList.setLayoutManager(linearLayoutManager);
        rvOperationList.setHasFixedSize(true);
        arrayList.clear();
        RealmResults<DatumSubCat> itemsRealmResults = mRealm.where(DatumSubCat.class).findAll();

        if (TextUtils.isEmpty(strCategoryTypeId)) {
            arrayList.addAll(itemsRealmResults);
        }
        for (int i = 0; i < itemsRealmResults.size(); i++) {
            String categoryTypeId = itemsRealmResults.get(i).getFkIntCategoryTypeId();
            String intCategoryId = itemsRealmResults.get(i).getFkIntCategoryId();
            String intSubCategoryId = itemsRealmResults.get(i).getFkIntSubCategoryId();

            if (categoryTypeId.equals(strCategoryTypeId)) {
                arrayList.add(itemsRealmResults.get(i));
            } else {
                arrayList.add(itemsRealmResults.get(i));
            }
        }
        if (itemsRealmResults.size() > 0) {

            onItemsLoadComplete();
        }

//        getAllSubCategory(strCategoryTypeId, strCategoryId, strSubCategoryId);

        etTitle.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    String val = etTitle.getText().toString();
                    if (TextUtils.isEmpty(strCategoryTypeId)) {
                        if (!TextUtils.isEmpty(val)) {
                            showToast("Select category");
                        }
                    } else {
                        if (!TextUtils.isEmpty(val)) {
                            saveProjectTitle(val);
                        }
                    }

                }
                return false;
            }
        });

    }

    public void putArguments(Bundle args) {
        isConnected = args.getBoolean("isConnected");
    }

    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = Application.getConnectivityStatusString(context);
            setSnackbarMessage(status, false);
        }
    };

    @Override
    public View provideYourFragmentView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return null;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        menu.findItem(R.id.search).setVisible(true);
        menu.findItem(R.id.item_delete).setVisible(false);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");
        super.onPrepareOptionsMenu(menu);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_search, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }

        if (searchView != null) {

            searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
            queryTextListener = new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextChange(String newText) {
//                    filter(newText);
                    return true;
                }

                @Override
                public boolean onQueryTextSubmit(String query) {

                    return true;
                }
            };
            searchView.setOnQueryTextListener(queryTextListener);

        }
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        strCategoryTypeId = arrayList.get(position).getFkIntCategoryTypeId();
        strCategoryId = arrayList.get(position).getFkIntCategoryId();
        strSubCategoryId = arrayList.get(position).getFkIntSubCategoryId();
        getAllSubCategory();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void catChange(int catPos) {

        if (catPos == 1) {

            if (TextUtils.isEmpty(strCategoryTypeName) && TextUtils.isEmpty(strCategoryName)) {
                allCategoryListDialog = AllCategoryListDialog.newIntance("OPERATION", false);
                allCategoryListDialog.show(getFragmentManager().beginTransaction(), "my-dialog");
            } else {
                getCategory("", "", "", "", "", "");
            }
        } else if (catPos == 2) {
            if (TextUtils.isEmpty(strSubCategoryName)) {
                allCategoryListDialog = AllCategoryListDialog.newIntance("OPERATION", false);
                allCategoryListDialog.show(getFragmentManager().beginTransaction(), "my-dialog");
            } else {
                getCategory(strCategoryTypeName, strCategoryTypeId, strCategoryName, strCategoryId, "", "");
            }
        } else if (catPos == 3) {
            getCategory(strCategoryTypeName, strCategoryTypeId, strCategoryName, strCategoryId, strSubCategoryId, strSubCategoryName);
        }

    }

    public void getCategory(String tabNames, String tabIDs, String mainCatNams, String mainCatId, String subCatId, String subCatNames) {

        strCategoryTypeName = tabNames;
        strCategoryTypeId = tabIDs;

        strCategoryName = mainCatNams;
        strCategoryId = mainCatId;

        strSubCategoryId = subCatId;
        strSubCategoryName = subCatNames;

        Log.e("getCatgeory_recvData", strCategoryTypeName + " = " + strCategoryTypeId + "..."
                + strCategoryName + " = " + strCategoryId + "...."
                + strSubCategoryName + " = " + strSubCategoryId + "....");

        if (!TextUtils.isEmpty(strCategoryName)) {
            if (strCategoryName.equals("All")) {
                tvMainCategory.setVisibility(View.VISIBLE);
                tvMainCategory.setText(strCategoryTypeName);
            } else {
                tvMainCategory.setVisibility(View.VISIBLE);
                tvMainCategory.setText(strCategoryName);
            }
        }

        if (!TextUtils.isEmpty(strSubCategoryName)) {
            tvSubCategory.setVisibility(View.VISIBLE);
            tvSubCategory.setText(strSubCategoryName);
        }

        if (allCategoryListDialog != null) {
            allCategoryListDialog.dismiss();
        }

        if (TextUtils.isEmpty(strCategoryName)) {
            cvTasks.setVisibility(View.GONE);
        } else if (strCategoryName.equalsIgnoreCase("All")) {
            cvTasks.setVisibility(View.GONE);
        } else {
            cvTasks.setVisibility(View.VISIBLE);
        }

        getAllSubCategory();

    }

    public void getAllSubCategory() {

        if (isNetworkAvailable()) {
            RequestParams params = new RequestParams();
            params.put("strCreateUserId", strUserId);

            params.put("strCategoryTypeId", strCategoryTypeId);
            params.put("strCategoryId", strCategoryId);
//            params.put("strSubCategoryId", strSubCategoryId);

            mEelViewUtils.showLoadingView();

            Log.e("allLists", url_subCatList + "..." + params);

            ApiCallWithToken(url_subCatList, ApiCall.GETSUBCATlIST, params);
        } else {
            arrayList.clear();
            RealmResults<DatumSubCat> itemsRealmResults = mRealm.where(DatumSubCat.class).findAll();

            if (TextUtils.isEmpty(strCategoryTypeId)) {
                arrayList.addAll(itemsRealmResults);
            }
            for (int i = 0; i < itemsRealmResults.size(); i++) {
                String categoryTypeId = itemsRealmResults.get(i).getFkIntCategoryTypeId();

                if (categoryTypeId.equals(strCategoryTypeId)) {
                    arrayList.add(itemsRealmResults.get(i));
                }
            }
            onItemsLoadComplete();

        }

    }

    public void saveProjectTitle(String val) {

        if (isNetworkAvailable()) {
            RequestParams params = new RequestParams();
            params.put("strOperationName", val);
            params.put("strCreateUserId", strUserId);

            params.put("intProjectId", mUserDetails.getArryselectedProjectAllItems().get(0).getId());
            params.put("intOrganisationId", mUserDetails.getArryselectedOrganisationAllItems().get(0).getId());

            params.put("strTitle", "");
            params.put("strGoalAchieve", false);

            params.put("strCategoryTypeId", strCategoryTypeId);
            params.put("strCategoryId", strCategoryId);
//            params.put("strSubCategoryId", strSubCategoryId);

            showProgressDialog(getActivity(), "Please Wait...");
            ApiCallWithToken(url_saveTitleProject, ApiCall.SAVEPROJECTTITLE, params);

        } else {
            showToast(getString(R.string.noConnection));
        }

    }

    @Override
    public void OnResponce(JSONObject data, ApiCall type) {

        JSONArray array = null;
        Gson gson = new Gson();

        try {
            if (type == ApiCall.SAVEPROJECTTITLE) {
                hideProgressDialog(getActivity());
                array = data.getJSONArray("data");
                rvOperationList.setVisibility(View.VISIBLE);
                llNodata.setVisibility(View.GONE);
                mRealm.beginTransaction();
                for (int i = 0; i < array.length(); i++) {
                    try {
                        JSONObject object = array.getJSONObject(i);
                        DatumSubCat item = gson.fromJson(object.toString(), DatumSubCat.class);
                        mRealm.copyToRealmOrUpdate(item);
                        operationAdapter.insertItem(item, 0);
                        operationAdapter.notifyDataSetChanged();
                        etTitle.setText("");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                mRealm.commitTransaction();
            } else if (type == ApiCall.GETSUBCATlIST) {

                mEelViewUtils.showContentViewWhite();
                arrayList.clear();
                mRealm.beginTransaction();
                array = data.getJSONArray("data");

                llNodata.setVisibility(View.GONE);

                for (int i = 0; i < array.length(); i++) {
                    try {
                        JSONObject object = array.getJSONObject(i);
                        DatumSubCat item = gson.fromJson(object.toString(), DatumSubCat.class);
                        arrayList.add(item);
                        mRealm.copyToRealmOrUpdate(item);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                mRealm.commitTransaction();
                onItemsLoadComplete();
            } else if (type == ApiCall.GOALDELETE) {
                array = data.getJSONArray("data");
                Boolean success = data.getBoolean("success");
                String mesg = data.getString("message");

                if (success) {
                    showToast("Succesfully deleted");
                    operationAdapter.removeItem(itemPosition);
                    fabtnDelete.setVisibility(View.GONE);
                } else {
                    showToast("Unable to delete this goal");
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        super.OnResponce(data, type);
    }

    public void onItemsLoadComplete() {
        if (arrayList.size() != 0) {
            operationAdapter = new OperationAdapter(getActivity(), arrayList, rvOperationList, this);
            rvOperationList.setAdapter(operationAdapter);
        } else {
            llNodata.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void longClick(Activity activity, int pos, DatumSubCat item, String clickType) {
        if (clickType.equals("clearDeleteSelectedItems")) {
            deleteList.clear();
            fabtnDelete.setVisibility(View.GONE);
        } else {
            itemPosition = pos;
            if (deleteList.size() > 0) {
                deleteList.clear();
                operationAdapter.notifyDataSetChanged();
            }
            fabtnDelete.setVisibility(View.VISIBLE);
            deleteList.clear();
            deleteList.add(0, item);
            fabtnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteGoal(item);
                }
            });
        }
    }

    public void onBackPressed() {
        deleteList.clear();
        clearActionMode();
        getActivity().invalidateOptionsMenu();
    }

    public void clearActionMode() {
        fabtnDelete.setVisibility(View.GONE);
        deleteList.clear();
        operationAdapter.notifyDataSetChanged();
        getActivity().invalidateOptionsMenu();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("");
        if (((AppCompatActivity) getActivity()).getSupportActionBar() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
    }

    public void deleteGoal(DatumSubCat datumSubCat) {

        if (isNetworkAvailable()) {
            RequestParams params = new RequestParams();
            params.put("strOperationId", datumSubCat.getId());
            params.put("fkintOperationId", datumSubCat.getFkIntCategoryId());
            params.put("strModifiedUserId", strUserId);

//            params.put("strCategoryTypeId", strCategoryTypeId);
//            params.put("strCategoryId", strCategoryId);
//            params.put("strSubCategoryId", strSubCategoryId);

            ApiCallWithToken(url_deleteList, ApiCall.GOALDELETE, params);

        } else {
            showToast(getString(R.string.noConnection));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(broadcastReceiver);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        registerInternetCheckReceiver();
    }

    private void registerInternetCheckReceiver() {
        IntentFilter internetFilter = new IntentFilter();
        internetFilter.addAction("android.net.wifi.STATE_CHANGE");
        internetFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        getActivity().registerReceiver(broadcastReceiver, internetFilter);
    }

    private void setSnackbarMessage(String status, boolean showBar) {
        String internetStatus = "";
        Snackbar snackbar;
        if (status.equalsIgnoreCase("Wifi enabled") || status.equalsIgnoreCase("Mobile data enabled")) {
            internetStatus = "Connecting..";
        } else {
            internetStatus = "No Internet Connection";
        }
        if (internetStatus.equalsIgnoreCase("No Internet Connection")) {
            if (internetConnected) {
                snackbar = Snackbar.make(getActivity().findViewById(R.id.llMain), internetStatus, Snackbar.LENGTH_INDEFINITE);

                snackbar.setActionTextColor(Color.WHITE);
                View sbView = snackbar.getView();
                TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
//                Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout)snackbar;
                sbView.setMinimumHeight(0);
                Typeface font_sfp_regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/roboto_light.ttf");
                textView.setTypeface(font_sfp_regular);
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(12);
                snackbar.show();
                internetConnected = false;
            }
        } else {

            if (!internetConnected) {
                internetConnected = true;
                snackbar = Snackbar.make(getActivity().findViewById(R.id.llMain), internetStatus, Snackbar.LENGTH_LONG);
                View sbView = snackbar.getView();
                TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                sbView.setMinimumHeight(0);
                Typeface font_sfp_regular = Typeface.createFromAsset(getActivity().getAssets(), "fonts/roboto_light.ttf");
                textView.setTypeface(font_sfp_regular);
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(12);
                snackbar.setActionTextColor(Color.WHITE);
                snackbar.show();
            }

        }
    }
}