package com.example.user.communicator.Model.category;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class DatumCategory extends RealmObject implements Parcelable {

    public static final Parcelable.Creator<DatumCategory> CREATOR = new Parcelable.Creator<DatumCategory>() {
        @Override
        public DatumCategory createFromParcel(Parcel in) {
            return new DatumCategory(in);
        }

        @Override
        public DatumCategory[] newArray(int size) {
            return new DatumCategory[size];
        }
    };
    @PrimaryKey
    @SerializedName("intCaregoryTypeId")
    @Expose
    private String intCaregoryTypeId;
    @SerializedName("strCategoryType")
    @Expose
    private String strCategoryType;
    @SerializedName("intDefaultSubCategoryCount")
    @Expose
    private Integer intDefaultSubCategoryCount;
    @SerializedName("arryObjStrDefaultSubCategory")
    @Expose
    private RealmList<ArryObjStrDefaultSubCategory> arryObjStrDefaultSubCategory = null;
    @SerializedName("arryCategoryDetails")
    @Expose
    private RealmList<ArryCategoryDetail> arryCategoryDetails = null;

    @SerializedName("arryStrIncludeModules")
    @Expose
    private RealmList<String> arryStrIncludeModules = null;

    protected DatumCategory(Parcel in) {
        intCaregoryTypeId = in.readString();
        strCategoryType = in.readString();
        intDefaultSubCategoryCount = in.readInt();
        in.readTypedList(arryObjStrDefaultSubCategory, ArryObjStrDefaultSubCategory.CREATOR);
        in.readStringList(arryStrIncludeModules);
    }

    public DatumCategory() {
    }

    public String getIntCaregoryTypeId() {
        return intCaregoryTypeId;
    }

    public void setIntCaregoryTypeId(String intCaregoryTypeId) {
        this.intCaregoryTypeId = intCaregoryTypeId;
    }

    public String getStrCategoryType() {
        return strCategoryType;
    }

    public void setStrCategoryType(String strCategoryType) {
        this.strCategoryType = strCategoryType;
    }

    public Integer getIntDefaultSubCategoryCount() {
        return intDefaultSubCategoryCount;
    }

    public void setIntDefaultSubCategoryCount(Integer intDefaultSubCategoryCount) {
        this.intDefaultSubCategoryCount = intDefaultSubCategoryCount;
    }

    public RealmList<ArryObjStrDefaultSubCategory> getArryObjStrDefaultSubCategory() {
        return arryObjStrDefaultSubCategory;
    }

    public void setArryObjStrDefaultSubCategory(RealmList<ArryObjStrDefaultSubCategory> arryObjStrDefaultSubCategory) {
        this.arryObjStrDefaultSubCategory = arryObjStrDefaultSubCategory;
    }

    public RealmList<ArryCategoryDetail> getArryCategoryDetails() {
        return arryCategoryDetails;
    }

    public void setArryCategoryDetails(RealmList<ArryCategoryDetail> arryCategoryDetails) {
        this.arryCategoryDetails = arryCategoryDetails;
    }

    public RealmList<String> getArryStrIncludeModules() {
        return arryStrIncludeModules;
    }

    public void setArryStrIncludeModules(RealmList<String> arryStrIncludeModules) {
        this.arryStrIncludeModules = arryStrIncludeModules;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {

        parcel.writeString(intCaregoryTypeId);
        parcel.writeString(strCategoryType);
        parcel.writeInt(intDefaultSubCategoryCount);
        parcel.writeTypedList(arryObjStrDefaultSubCategory);
        parcel.writeList(arryStrIncludeModules);
    }

    @Override
    public int describeContents() {
        return 0;
    }


}
