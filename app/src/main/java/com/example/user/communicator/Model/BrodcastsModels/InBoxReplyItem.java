package com.example.user.communicator.Model.BrodcastsModels;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class InBoxReplyItem extends RealmObject {

    @PrimaryKey
    private String _id;

    private int status;
    private String broadcastId;
    private String intUserId;
    private RealmList<String> replyValue;
    private long addTime;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getBroadcastId() {
        return broadcastId;
    }

    public void setBroadcastId(String broadcastId) {
        this.broadcastId = broadcastId;
    }

    public String getIntUserId() {
        return intUserId;
    }

    public void setIntUserId(String intUserId) {
        this.intUserId = intUserId;
    }

    public RealmList<String> getReplyValue() {
        return replyValue;
    }

    public void setReplyValue(RealmList<String> replyValue) {
        this.replyValue = replyValue;
    }

    public long getAddTime() {
        return addTime;
    }

    public void setAddTime(long addTime) {
        this.addTime = addTime;
    }
}