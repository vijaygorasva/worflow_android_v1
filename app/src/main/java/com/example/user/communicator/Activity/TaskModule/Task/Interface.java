package com.example.user.communicator.Activity.TaskModule.Task;

import java.util.ArrayList;
import java.util.List;

public class Interface {

    public static OnSearchViewVisibilityListner onChangeVisibility;
    public static List<OnSearchTextListner> onChangeText = new ArrayList<>();
    public static  List<OnPerformSearchListner> onPerformSearch = new ArrayList<>();

    public interface OnSearchViewVisibilityListner {
        void OnChangevisibility(int visibility);
    }

    public static void setOnSearchViewVisibilityListner(OnSearchViewVisibilityListner onClickListner) {
        onChangeVisibility = onClickListner;
    }

    public interface OnSearchTextListner {
        void OnChangeText(String text);
    }

    public static void setOnSearchTextListner(OnSearchTextListner onClickListner) {
        onChangeText.add(onClickListner);
    }

    public interface OnPerformSearchListner {
        void OnPerformSearch(boolean search);
    }

    public static void setOnPerformSearchListner(OnPerformSearchListner onClickListner) {
        onPerformSearch.add(onClickListner);
    }
}
