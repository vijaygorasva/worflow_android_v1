
package com.example.user.communicator.Model.Notes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllNote {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Item> doc = null;
    @SerializedName("strPlannerMainId")
    @Expose
    private String strPlannerMainId;
    @SerializedName("strMainCategoryname")
    @Expose
    private String strMainCategoryname;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public List<Item> getAllNotes() {
        return doc;
    }

    public void setAllNotes(List<Item> doc) {
        this.doc = doc;
    }

    public String getStrPlannerMainId() {
        return strPlannerMainId;
    }

    public void setStrPlannerMainId(String strPlannerMainId) {
        this.strPlannerMainId = strPlannerMainId;
    }

    public String getStrMainCategoryname() {
        return strMainCategoryname;
    }

    public void setStrMainCategoryname(String strMainCategoryname) {
        this.strMainCategoryname = strMainCategoryname;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
