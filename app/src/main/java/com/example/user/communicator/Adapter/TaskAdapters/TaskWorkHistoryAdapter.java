package com.example.user.communicator.Adapter.TaskAdapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.TaskModels.TaskWorkHistory;
import com.example.user.communicator.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class TaskWorkHistoryAdapter extends RecyclerView.Adapter<TaskWorkHistoryAdapter.ViewHolder> {

    private Context context;
    private ArrayList<TaskWorkHistory> taskItems;
    private ArrayList<TaskWorkHistory> allTaskItems = new ArrayList<>();

    public TaskWorkHistoryAdapter(Context context, ArrayList<TaskWorkHistory> taskItems) {
        this.taskItems = taskItems;
        if (taskItems != null) {
            if (taskItems.size() != 0) {
                allTaskItems.addAll(taskItems);
            }
        }
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_task_work_history, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(taskItems.get(position));
    }

    @Override
    public int getItemCount() {
        return taskItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.timeLine)
        CustomTextView timeline;

        @BindView(R.id.totaltime)
        CustomTextView totalTime;

        @BindView(R.id.cvTasks_Two)
        CardView mCardView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @SuppressLint("SetTextI18n")
        public void bind(final TaskWorkHistory item) {

            if (!TextUtils.isEmpty(item.getEndTime())) {
                timeline.setText(item.getStartTime() + " to " + item.getEndTime());
                totalTime.setText(item.getTotalTime());
            } else {
                itemView.setVisibility(View.GONE);
            }

        }
    }

    public void setAllTaskItems(ArrayList<TaskWorkHistory> allTaskItems) {
        this.allTaskItems.clear();
        this.allTaskItems.addAll(allTaskItems);

    }

    public void filter(String date) {

        try {
            taskItems.clear();
            if (TextUtils.isEmpty(date)) {
                taskItems.addAll(allTaskItems);
            } else {
                for (TaskWorkHistory item : allTaskItems) {
                    if (item.getWorkDate().toLowerCase().contains(date.toLowerCase())) {
                        taskItems.add(item);
                    }
                }
            }
            ((Activity) context).
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            notifyDataSetChanged();
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
