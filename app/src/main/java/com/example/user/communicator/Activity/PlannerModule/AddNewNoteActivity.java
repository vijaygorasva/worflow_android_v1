package com.example.user.communicator.Activity.PlannerModule;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.user.communicator.Activity.TaskModule.ImageShowActivity;
import com.example.user.communicator.Application;
import com.example.user.communicator.ConnectivityReceiver;
import com.example.user.communicator.CustomViews.CustomEditText;
import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Dialog.AddAudioDialog;
import com.example.user.communicator.Dialog.ReminderDialog;
import com.example.user.communicator.Fragment.PlannerFragment;
import com.example.user.communicator.Model.Colleborate.ColleborateDatum;
import com.example.user.communicator.Model.NoteUpdate.NoteUpdate;
import com.example.user.communicator.Model.Notes.AudFile;
import com.example.user.communicator.Model.Notes.AudioDetails;
import com.example.user.communicator.Model.Notes.ImgDetails;
import com.example.user.communicator.Model.Notes.ImgFile;
import com.example.user.communicator.Model.Notes.Item;
import com.example.user.communicator.Model.Notes.ObjRemainder;
import com.example.user.communicator.Model.Notes.TodoListItem;
import com.example.user.communicator.Model.Notes.UndoCheckboxModel;
import com.example.user.communicator.Model.Notes.UndoCheckboxes;
import com.example.user.communicator.Model.Notes.UndoTexts;
import com.example.user.communicator.Model.Notes.UndoTextsModel;
import com.example.user.communicator.Nammu.Nammu;
import com.example.user.communicator.R;
import com.example.user.communicator.Service.MyService;
import com.example.user.communicator.Utility.DateUtils;
import com.example.user.communicator.Utility.Functions;
import com.example.user.communicator.Utility.RealPathUtil;
import com.google.gson.Gson;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import net.ralphpina.permissionsmanager.PermissionsManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import pl.droidsonroids.gif.GifImageView;

//        minetesting419@gmail.com
public class AddNewNoteActivity extends BaseActivity {//} implements ConnectivityReceiver.ConnectivityReceiverListener {
    int planer_ID = 0;
    public static String isChangedCheckbox = "No", isChanged = "No";
    static String isType = "Checkboxes";
    static TodoListItem todoListItem = new TodoListItem();
    static List<EditText> UndoalleditText = new ArrayList<EditText>();
    static int checkboxCount = 0;
    static RealmList<Item> noteUpdatedList = new RealmList();
    static RealmList<TodoListItem> todoCheckboxItems = new RealmList<>();
    static String isActiveCheckbox = "No", isActiveText = "No";
    static String titleText = "";
    static ArrayList<UndoCheckboxes> undoCheckboxesArraylist = new ArrayList<>();
    static ArrayList<UndoCheckboxes> redoCheckboxesArraylist = new ArrayList<>();
    static ArrayList<UndoTexts> undoTextsArraylist = new ArrayList<>();
    static ArrayList<UndoTexts> redoTextsArraylist = new ArrayList<>();
    private final int MY_PERMISSIONS_RECORD_AUDIO = 1;
    String isCheckBox = "Checkboxes";

    Boolean isCam = true;//true= camera,false=gallery
    @BindView(R.id.ivConvert)
    ImageView ivConvert;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.llPin)
    LinearLayout llPin;
    @BindView(R.id.ivRemainder)
    ImageView ivRemainder;
    @BindView(R.id.ivArchieve)
    ImageView ivArchieve;
    @BindView(R.id.ivRedo)
    ImageView ivRedo;
    @BindView(R.id.ivUndo)
    ImageView ivUndo;
    @BindView(R.id.tvToolbar)
    TextView tvToolbar;
    @BindView(R.id.flImage)
    FrameLayout flImage;
    @BindView(R.id.ivImageOne)
    ImageView ivImageOne;
    @BindView(R.id.ivImageTwo)
    ImageView ivImageTwo;
    @BindView(R.id.ivImageThree)
    ImageView ivImageThree;
    static Realm mRealm = Realm.getDefaultInstance();
    @BindView(R.id.ivLoader)
    GifImageView ivLoader;
    @BindView(R.id.tvTitle)
    EditText tvTitle;
    @BindView(R.id.tvText)
    EditText tvText;
    @BindView(R.id.tvCheckbox)
    TextView tvCheckbox;
    @BindView(R.id.tvTimer)
    TextView tvTimer;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.rlCheckbox)
    LinearLayout rlCheckbox;
    @BindView(R.id.ivAdd)
    ImageView ivAdd;
    @BindView(R.id.llBottom)
    RelativeLayout llBottom;
    @BindView(R.id.rlMain)
    RelativeLayout rlMain;
    @BindView(R.id.llColleborate)
    LinearLayout llColleborate;
    @BindView(R.id.ivPin)
    ImageView ivPin;
    @BindView(R.id.llFirst)
    LinearLayout llFirst;
    @BindView(R.id.llAudioOne)
    LinearLayout llAudioOne;
    @BindView(R.id.ivPlayOne)
    ImageView ivPlayOne;
    @BindView(R.id.seekBarOne)
    SeekBar seekBarOne;
    @BindView(R.id.tvAudioOne)
    TextView tvAudioOne;
    @BindView(R.id.tvMoreAudio)
    CustomTextView tvMoreAudio;
    @BindView(R.id.llAudioTwo)
    LinearLayout llAudioTwo;
    @BindView(R.id.ivPlayTwo)
    ImageView ivPlayTwo;
    @BindView(R.id.seekBarTwo)
    SeekBar seekBarTwo;
    @BindView(R.id.tvAudioTwo)
    TextView tvAudioTwo;
    @BindView(R.id.llAudioThree)
    LinearLayout llAudioThree;
    @BindView(R.id.ivPlayThree)
    ImageView ivPlayThree;
    @BindView(R.id.seekBarThree)
    SeekBar seekBarThree;
    @BindView(R.id.tvAudioThree)
    TextView tvAudioThree;
    int kCount = 0;
    Boolean isAudioExpanded = false;
    private boolean internetConnected = true;
    Boolean isPinned = false;
    String strUserId;
    ArrayList<ColleborateDatum> arryCollaboratUserData = new ArrayList<>();
    File fileProfile = new File("null");
    StringBuffer toSingleString;
    ClipData mClipData;
    Bitmap bitmap;
    int imageHeight, imageWidth;
    //    MediaPlayer mediaPlayer;
    MediaPlayer mediaPlayerOne, mediaPlayerTwo, mediaPlayerThree;

    Boolean isAudioPlaying = false;
    Typeface font_roboto_regular, font_roboto_light;
    RealmList<String> collUsers = new RealmList<>();
    ViewGroup mLinearLayout1;
    String audStatus = "Online";
    RealmList<AudioDetails> arrAudioDetails = new RealmList<>();
    RealmList<AudioDetails> audDeletUpdated = new RealmList<>();

    com.example.user.communicator.Model.Login.Datum mUserDetails;
    ArrayList<String> mArrayUri = new ArrayList<String>();//Uri
    ArrayList<ImgFile> fileArrayList = new ArrayList<>();
    int REQUEST_CODE = 3;
    ObjRemainder objRemainder = new ObjRemainder();
    List<EditText> alleditText = new ArrayList<EditText>();
    List<EditText> alleditTextCheckbox = new ArrayList<EditText>();
    CustomEditText editText, etTextCheckBox = null;
    View view1;
    String categoryName;
    int colorGrey = Color.parseColor("#A1A1A1"); //The color u want
    int colorBlack = Color.parseColor("#000000"); //The color u want
    private double startTime = 0;
    private double finalTime = 0;
    private Handler myHandler = new Handler();

    RealmList<ImgDetails> imgDetailss = new RealmList<>();

    private Runnable UpdateSongTimeOne = new Runnable() {
        public void run() {
            startTime = mediaPlayerOne.getCurrentPosition();
            seekBarOne.setProgress((int) startTime);
            myHandler.postDelayed(this, 100);
        }
    };

    private Runnable UpdateSongTimeTwo = new Runnable() {
        public void run() {
            startTime = mediaPlayerTwo.getCurrentPosition();
            seekBarTwo.setProgress((int) startTime);
            myHandler.postDelayed(this, 100);
        }
    };

    private Runnable UpdateSongTimeThree = new Runnable() {
        public void run() {
            startTime = mediaPlayerThree.getCurrentPosition();
            seekBarThree.setProgress((int) startTime);
            myHandler.postDelayed(this, 100);
        }
    };

    String strCategoryTypeId,
            strCategoryId,
            strSubCategoryId;


    public static void storeData() {

        if ((undoTextsArraylist.size() < 6)) {
            if (isType.equals("TextFormat")) {

                ArrayList<UndoCheckboxModel> NewundoCheckboxesFinal = new ArrayList<>();
                ArrayList<UndoCheckboxModel> undoCheckboxesFinal = new ArrayList<>();


                if (isChangedCheckbox.equals("Yes") && (isActiveCheckbox.equals("No"))) {

                    if (UndoalleditText.size() == 0) {
                        UndoCheckboxModel undoCheckboxes1 = new UndoCheckboxModel();
                        undoCheckboxes1.setId("0");
                        undoCheckboxes1.setStrNoteListValue("");
                        undoCheckboxes1.setStrNoteListFlage(false);
                        undoCheckboxesFinal.add(undoCheckboxes1);
                        NewundoCheckboxesFinal.add(0, undoCheckboxesFinal.get(0));
                    } else {
                        for (int i = 0; i < UndoalleditText.size(); i++) {
                            UndoCheckboxModel undoCheckboxes1 = new UndoCheckboxModel();
                            undoCheckboxes1.setId(String.valueOf(i));
                            undoCheckboxes1.setStrNoteListValue(UndoalleditText.get(i).getText().toString());
                            undoCheckboxes1.setStrNoteListFlage(false);
                            undoCheckboxesFinal.add(undoCheckboxes1);
                            NewundoCheckboxesFinal.add(i, undoCheckboxesFinal.get(i));
                        }
                    }

                    int count = undoCheckboxesArraylist.size();

                    if (count == 1) {
                        UndoCheckboxes undos = new UndoCheckboxes();
                        undos.setId(String.valueOf(count));
                        undos.setTitle(titleText);
                        undos.setUndoCheckboxModel(NewundoCheckboxesFinal);
                        undoCheckboxesArraylist.add(1, undos);
                    } else if (count == 2) {
                        UndoCheckboxes undos = new UndoCheckboxes();
                        undos.setId(String.valueOf(count));
                        undos.setTitle(titleText);
                        undos.setUndoCheckboxModel(NewundoCheckboxesFinal);
                        undoCheckboxesArraylist.add(count, undos);
                    } else if (count == 3) {
                        UndoCheckboxes undos = new UndoCheckboxes();
                        undos.setId(String.valueOf(count));
                        undos.setTitle(titleText);
                        undos.setUndoCheckboxModel(NewundoCheckboxesFinal);
                        undoCheckboxesArraylist.add(count, undos);
                    } else if (count == 4) {
                        UndoCheckboxes undos = new UndoCheckboxes();
                        undos.setId(String.valueOf(count));
                        undos.setTitle(titleText);
                        undos.setUndoCheckboxModel(NewundoCheckboxesFinal);
                        undoCheckboxesArraylist.add(count, undos);
                    } else {
                        UndoCheckboxes undos = new UndoCheckboxes();
                        undos.setId(String.valueOf(count));
                        undos.setTitle(titleText);
                        undos.setUndoCheckboxModel(NewundoCheckboxesFinal);
                        undoCheckboxesArraylist.add(count, undos);
                    }
                    isChangedCheckbox = "No";
                    undoCheckboxesFinal.clear();
                }

            } else {
                ArrayList<UndoTextsModel> NewundoTextsFinal = new ArrayList<>();
                ArrayList<UndoTextsModel> undoTextsFinal = new ArrayList<>();

                if (isChanged.equals("Yes") && (isActiveText.equals("No"))) {//isActiveText used to prevent adding the data again in the list

                    if (UndoalleditText.size() == 0) {
                        UndoTextsModel undoTextsModel = new UndoTextsModel();
                        undoTextsModel.setId("0");
                        undoTextsModel.setStrNoteListValue("");
                        undoTextsFinal.add(undoTextsModel);
                        NewundoTextsFinal.add(0, undoTextsFinal.get(0));
                    } else {
                        for (int i = 0; i < UndoalleditText.size(); i++) {
                            UndoTextsModel undoTextsModel = new UndoTextsModel();
                            undoTextsModel.setId(String.valueOf(i));

                            undoTextsModel.setStrNoteListValue(UndoalleditText.get(i).getText().toString());
                            undoTextsFinal.add(undoTextsModel);
                            NewundoTextsFinal.add(i, undoTextsFinal.get(i));
                        }
                    }
                    int count = undoTextsArraylist.size();
//                    Log.e("VAl_count...", count + "...");
//                    for (int i = 0; i < NewundoTextsFinal.size(); i++) {
//                        Log.e("SERvicetyText", count + "...." + new Gson().toJson(undoTextsFinal.get(i)) + ".." + new Gson().toJson(NewundoTextsFinal.get(i)));
//                    }

                    if (count == 0) {
                        UndoTexts undoTextsModel = new UndoTexts();
                        undoTextsModel.setId(String.valueOf(count));
                        undoTextsModel.setTitle(titleText);
                        undoTextsModel.setUndoTextsModel(NewundoTextsFinal);
                        undoTextsArraylist.add(0, undoTextsModel);
//                        Log.e("VAl_count.0..", count + "..." + undoTextsArraylist.size() + "...." + new Gson().toJson(undoTextsArraylist.get(0)));
                    } else if (count == 1) {
                        UndoTexts undoTextsModel = new UndoTexts();
                        undoTextsModel.setId(String.valueOf(count));
                        undoTextsModel.setTitle(titleText);
                        undoTextsModel.setUndoTextsModel(NewundoTextsFinal);
                        undoTextsArraylist.add(1, undoTextsModel);
//                        Log.e("VAl_count.1..", count + "..." + undoTextsArraylist.size() + "...." + new Gson().toJson(undoTextsArraylist.get(1)));
                    } else if (count == 2) {
                        UndoTexts undoTextsModel = new UndoTexts();
                        undoTextsModel.setId(String.valueOf(count));
                        undoTextsModel.setTitle(titleText);
                        undoTextsModel.setUndoTextsModel(NewundoTextsFinal);
                        undoTextsArraylist.add(2, undoTextsModel);
//                        Log.e("VAl_count.2..", count + "..." + new Gson().toJson(undoTextsModel) + "...." + new Gson().toJson(undoTextsArraylist.get(2)));
                    } else if (count == 3) {
                        UndoTexts undoTextsModel = new UndoTexts();
                        undoTextsModel.setId(String.valueOf(count));
                        undoTextsModel.setTitle(titleText);
                        undoTextsModel.setUndoTextsModel(NewundoTextsFinal);
                        undoTextsArraylist.add(3, undoTextsModel);
//                        Log.e("VAl_count.3..", count + "..." + undoTextsArraylist.size() + "....");
//                        for (int i = 0; i < undoTextsArraylist.size(); i++) {
//                            Log.e("VAl_", count + "...." + isChanged + "..." + i + "..." + new Gson().toJson(undoTextsArraylist.get(i)));
//                        }
                    } else if (count == 4) {
                        UndoTexts undoTextsModel = new UndoTexts();
                        undoTextsModel.setId(String.valueOf(count));
                        undoTextsModel.setTitle(titleText);
                        undoTextsModel.setUndoTextsModel(NewundoTextsFinal);
                        undoTextsArraylist.add(4, undoTextsModel);
//                        Log.e("VAl_count.4..", count + "..." + undoTextsArraylist.size() + "...." + new Gson().toJson(undoTextsArraylist.get(4)));
                    } else {
//                        Log.e("SERvicetyTextFormat", isChanged + "..." + isActiveText + "...." + count);
                        UndoTexts undoTextsModel = new UndoTexts();
                        undoTextsModel.setId(String.valueOf(count));
                        undoTextsModel.setTitle(titleText);
                        undoTextsModel.setUndoTextsModel(NewundoTextsFinal);
                        undoTextsArraylist.add(count, undoTextsModel);
//                        Log.e("VAl_count>4..", count + "..." + undoTextsArraylist.size() + "...." + new Gson().toJson(undoTextsArraylist.get(0)));

                    }
//                    for (int i = 0; i < count; i++) {
//                        Log.e("VAl_isVal", count + "...." + isChanged + "..." + i + "..." + new Gson().toJson(undoTextsArraylist.get(i)));
//                    }

                }
                isActiveText = "Yes";
                undoTextsFinal.clear();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Intent intent = new Intent(AddNewNoteActivity.this, MyService.class);
        intent.putExtra("close", true);
        stopService(intent);
    }

    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = Application.getConnectivityStatusString(context);
            setSnackbarMessage(status, false);
        }
    };
    InputMethodManager imm;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint({"ResourceType", "ClickableViewAccessibility"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_note);
        ButterKnife.bind(this);
        imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        titleText = "";
        undoCheckboxesArraylist.clear();
        undoTextsArraylist.clear();
        Intent intent = new Intent(AddNewNoteActivity.this, MyService.class);
        intent.putExtra("activity", "AddNewNoteActivity");
        startService(intent);
        Nammu.init(this);
        SharedPreferences sharedpreferences = getApplicationContext().getSharedPreferences("Login", MODE_PRIVATE);
        String data = sharedpreferences.getString("UserData", null);
        Gson gson = new Gson();
        mUserDetails = gson.fromJson(data, com.example.user.communicator.Model.Login.Datum.class);
        strUserId = mUserDetails.getIntUserId();

        strCategoryTypeId = PlannerTabActivity.strCategoryTypeId;
        strCategoryId = PlannerTabActivity.strCategoryId;
        strSubCategoryId = PlannerTabActivity.strSubCategoryId;
        categoryName = getIntent().getStringExtra("plannerName");
        planer_ID = 0;

        font_roboto_regular = Typeface.createFromAsset(getAssets(), "fonts/roboto_regular.ttf");
        font_roboto_light = Typeface.createFromAsset(getAssets(), "fonts/roboto_light.ttf");
        Typeface font_sfp_regular = Typeface.createFromAsset(getAssets(), "fonts/SFProText_Regular.ttf");
        tvToolbar.setTypeface(font_roboto_regular);
        tvTitle.setTypeface(font_roboto_regular);
        tvText.setTypeface(font_roboto_light);
        tvTitle.setHint("Title");
        tvText.setVisibility(View.GONE);
        noteUpdatedList.clear();
        rlCheckbox.setVisibility(View.VISIBLE);

        boolean isConnected = ConnectivityReceiver.isConnected();
        if (!isConnected) {
            String status = Application.getConnectivityStatusString(this);
            setSnackbarMessage(status, false);
        }

        if (undoTextsArraylist.size() == 0) {

            ArrayList<UndoTextsModel> NewundoTextsFinal = new ArrayList<>();
            ArrayList<UndoTextsModel> undoTextsFinal = new ArrayList<>();
            UndoTextsModel undoCheckboxes1 = new UndoTextsModel();
            undoCheckboxes1.setId("0");
            undoCheckboxes1.setStrNoteListValue("");
            undoTextsFinal.add(undoCheckboxes1);
            NewundoTextsFinal.add(0, undoTextsFinal.get(0));
            UndoTexts undos1 = new UndoTexts();
            undos1.setId(String.valueOf(0));
            undos1.setTitle("");
            undos1.setUndoTextsModel(NewundoTextsFinal);
            undoTextsArraylist.add(0, undos1);

        }
        editText = new CustomEditText(AddNewNoteActivity.this);
        textStyle(editText);
        editText.setLeft(10);
        editText.requestFocus();
        editText.setCursorVisible(true);
        editText.setPadding(16, 16, 16, 16);
        editText.applyCustomFont(this, getResources().getString(R.string.roboto_light));
        editText.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        rlCheckbox.addView(editText);
        isChanged = "Yes";
        alleditText.add(editText);
        titleText = tvTitle.getText().toString();
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        tvTitle.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                llBottom.setVisibility(View.VISIBLE);
                if (isType.equals("Checkboxes")) {
                    if (undoTextsArraylist.size() == 0) {
                        ivUndo.setEnabled(false);
                        ivUndo.setColorFilter(colorGrey);
                    } else {
                        ivUndo.setEnabled(true);
                        ivUndo.setColorFilter(colorBlack);
                    }
                } else {
                    if (undoCheckboxesArraylist.size() == 0) {
                        ivUndo.setEnabled(false);
                        ivUndo.setColorFilter(colorGrey);
                    } else {
                        ivUndo.setEnabled(true);
                        ivUndo.setColorFilter(colorBlack);
                    }

                }
                isChangedCheckbox = "Yes";
                isChanged = "Yes";
                isActiveText = "No";
                titleText = tvTitle.getText().toString();
            }
        });

        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                isChanged = "Yes";
                isActiveText = "No";

                redoTextsArraylist.clear();
                ivRedo.setEnabled(false);
                ivRedo.setColorFilter(colorGrey);
                llBottom.setVisibility(View.VISIBLE);

                if (undoTextsArraylist.size() == 0) {
                    ivUndo.setEnabled(false);
                    ivUndo.setColorFilter(colorGrey);
                } else {
                    ivUndo.setEnabled(true);
                    ivUndo.setColorFilter(colorBlack);
                }

                UndoalleditText = alleditText;
            }
        });

        editText.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                addEditTextEnterKeyPressed(1, "");
                return true;
            }
            return false;
        });

        tvTitle.setOnClickListener(view -> {
            tvTitle.requestFocus();
            tvTitle.setCursorVisible(true);
            tvTitle.setFocusableInTouchMode(true);
        });

        tvTitle.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                // Perform action on key press
                editText.setSelection(editText.getText().length());
                editText.requestFocus();
                editText.setCursorVisible(true);
                return true;
            }
            return false;
        });
    }


    @SuppressLint("ResourceType")
    public void textStyle(CustomEditText customEditText) {

        customEditText.applyCustomFont(this, getResources().getString(R.string.roboto_light));
        customEditText.setBackgroundResource(getResources().getColor(android.R.color.transparent));
        customEditText.setTextColor(ContextCompat.getColor(this, R.color.text_color));
        customEditText.setTextSize(14);
//        customEditText.setLineSpacing((float) 0.75, 2);
    }

    @SuppressLint("ResourceType")
    public void textFieldValue(ArrayList<UndoTextsModel> undoTexts) {

        rlCheckbox.removeAllViews();
        rlCheckbox.setVisibility(View.VISIBLE);
        tvText.setVisibility(View.GONE);
        ivAdd.setVisibility(View.GONE);

        if (!undoTexts.isEmpty()) {
            alleditText.clear();
            alleditTextCheckbox.clear();
            for (int i = 0; i < undoTexts.size(); i++) {
                editText = new CustomEditText(AddNewNoteActivity.this);
                editText.setText(undoTexts.get(i).getStrNoteListValue());
                textStyle(editText);
                alleditText.add(editText);
                editText.setLeft(10);
                editText.setId(i);
                editText.setHint("");
                editText.setSelection(editText.getText().length());
                editText.requestFocus();
                editText.setCursorVisible(true);
                editText.setPadding(16, 16, 16, 16);
                editText.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                rlCheckbox.addView(editText, i);
                alleditTextCheckbox.add(editText);
                isActiveText = "Yes";
                int finalI = i;
//                editText.setOnKeyListener((v, keyCode, event) -> {
//                    if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
//                        addEditTextEnterKeyPressed(finalI + 1, "");
//                        return true;
//                    }
//                    return false;
//                });

                editText.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        redoTextsArraylist.clear();
                        ivRedo.setEnabled(false);
                        ivRedo.setColorFilter(colorGrey);
                        llBottom.setVisibility(View.VISIBLE);
                        if (undoTextsArraylist.size() == 0) {
                            ivUndo.setEnabled(false);
                            ivUndo.setColorFilter(colorGrey);
                        } else {
                            ivUndo.setEnabled(true);
                            ivUndo.setColorFilter(colorBlack);
                        }
                        isChanged = "Yes";
                        isActiveText = "No";
                        UndoalleditText = alleditTextCheckbox;
                    }
                });

                rlMain.setOnTouchListener((view, motionEvent) -> {
                    editText.setSelection(editText.getText().length());
                    editText.requestFocus();
                    editText.setCursorVisible(true);
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                    return false;
                });
            }
            loopViews(rlCheckbox);
        }
    }

    @SuppressLint({"ResourceType", "ClickableViewAccessibility"})
    public void checkBoxeValue(ArrayList<UndoCheckboxModel> undoCheckboxes) {

        for (int i = 0; i < undoCheckboxes.size(); i++) {
            EditText editText = new EditText(AddNewNoteActivity.this);
            editText.setText(undoCheckboxes.get(i).getStrNoteListValue());
            alleditText.add(editText);
        }
        rlCheckbox.removeAllViews();
        rlCheckbox.setVisibility(View.VISIBLE);
        alleditTextCheckbox.clear();
        checkboxCount = undoCheckboxes.size() - 1;

        for (int i = 0; i < undoCheckboxes.size(); i++) {

            mLinearLayout1 = findViewById(R.id.rlCheckbox);
            view1 = View.inflate(AddNewNoteActivity.this, R.layout.checkbox_layout, null);
            final RelativeLayout llMain = view1.findViewById(R.id.llMain);
            etTextCheckBox = view1.findViewById(R.id.etText);
            final ImageView ivCheckbox = view1.findViewById(R.id.ivCheckbox);
            final ImageView ivClose = view1.findViewById(R.id.ivClose);
//            alleditText.add(etTextCheckBox);
            textStyle(etTextCheckBox);
            etTextCheckBox.requestFocus();
            etTextCheckBox.setSelection(etTextCheckBox.getText().length());
            etTextCheckBox.setCursorVisible(true);
            etTextCheckBox.setId(i);
            etTextCheckBox.setText(undoCheckboxes.get(i).getStrNoteListValue());
            mLinearLayout1.addView(view1, i);

            alleditTextCheckbox.add(etTextCheckBox);
            final int finalI = i;

            etTextCheckBox.addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {

                    redoCheckboxesArraylist.clear();
                    ivRedo.setEnabled(false);
                    ivRedo.setColorFilter(colorGrey);
                    llBottom.setVisibility(View.VISIBLE);
                    if (undoCheckboxesArraylist.size() == 0) {
                        ivUndo.setEnabled(false);
                        ivUndo.setColorFilter(colorGrey);
                    } else {
                        ivUndo.setEnabled(true);
                        ivUndo.setColorFilter(colorBlack);
                    }
                    isChangedCheckbox = "Yes";
                    isActiveCheckbox = "No";
                    String _text = etTextCheckBox.getText().toString();
                    if (_text.isEmpty()) {
                    } else {
                        UndoalleditText = alleditTextCheckbox;
                    }

                }
            });

            int finalI1 = i;
//            etTextCheckBox.setOnKeyListener((v, keyCode, event) -> {
//                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
//                    addEnterKeyPressed(finalI1 + 1, "");
//                    return true;
//                }
//                return false;
//            });

            rlMain.setOnTouchListener((view, motionEvent) -> {
                etTextCheckBox.setSelection(etTextCheckBox.getText().length());
                etTextCheckBox.requestFocus();
                etTextCheckBox.setCursorVisible(true);
                return false;
            });

            llMain.setOnTouchListener((view, motionEvent) -> {
                ivClose.setVisibility(View.GONE);//VISIBLE
                etTextCheckBox.setCursorVisible(true);
                return false;
            });

            etTextCheckBox.setOnTouchListener((view, motionEvent) -> {
                ivClose.setVisibility(View.GONE);//VISIBLE
                etTextCheckBox.setCursorVisible(true);
                return false;
            });

            final int[] checked1 = {1};
            ivCheckbox.setOnTouchListener((view, motionEvent) -> {
                ivClose.setAlpha(0f);
                if (checked1[0] == 0) {
                    ivCheckbox.setTag(false);
                    ivCheckbox.setImageDrawable(getResources().getDrawable(R.drawable.ic_unchecked));
                    etTextCheckBox.setPaintFlags(etTextCheckBox.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                    checked1[0] = 1;
                } else {
                    ivCheckbox.setTag(true);
                    ivCheckbox.setImageDrawable(getResources().getDrawable(R.drawable.ic_checked));
                    etTextCheckBox.setPaintFlags(etTextCheckBox.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    checked1[0] = 0;
                }
                return false;
            });

            ivClose.setOnTouchListener((view, motionEvent) -> {
                ivClose.setAlpha(0f);
                return false;
            });

        }
        loopViewsCheckbox(rlCheckbox);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_RECORD_AUDIO: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay!
                    recordAudio();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "Permissions Denied to record audio", Toast.LENGTH_LONG).show();
                }
                return;
            }
            case IMAGE_PICKER_REQUEST_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (isCam) {////true= camera,false=gallery
                        Intent intent5 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent5, IMAGE_PICKER_REQUEST_CODE);
                    } else {
//                        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                        startActivityForResult(i, IMAGE_PICKER_REQUEST_CODE);
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), IMAGE_PICKER_REQUEST_CODE);

                    }
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "Permissions Denied to upload image", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private boolean mayRequest() {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestAudioPermissions() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {

            //When permission is not granted by user, show them message why this permission is needed.
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.RECORD_AUDIO)) {
                Toast.makeText(this, "Please grant permissions to record audio", Toast.LENGTH_LONG).show();

                //Give user option to still opt-in the permissions
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        MY_PERMISSIONS_RECORD_AUDIO);

            } else {
                // Show user dialog to grant permission to record audio
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECORD_AUDIO},
                        MY_PERMISSIONS_RECORD_AUDIO);
            }
        }
        //If permission is granted, then go ahead recording audio
        else if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECORD_AUDIO)
                == PackageManager.PERMISSION_GRANTED) {

            //Go ahead with recording audio now
            if (arrAudioDetails.size() <= 3) {//checking if audio is presnt
                recordAudio();
            }
        }
    }

    public void recordAudio() {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int displayWidth = displayMetrics.widthPixels;
        int displayHeight = displayMetrics.heightPixels;

        AddAudioDialog.newIntance(displayWidth, displayHeight, new AddAudioDialog.Callback() {

            @Override
            public void edit(AddAudioDialog dialog, File audioFile, String time) {
                audStatus = "Add";
                if (isNetworkAvailable()) {
//                    tvAudio.setVisibility(View.VISIBLE);
//                    llAudio.setVisibility(View.VISIBLE);
//                    saveAudio(time);
                    if (audioFile.exists()) {//not >arrAudioDetails.size()
                        saveAudio(audioFile, time);
                    }
                } else {
                    setAudioDatas();
//                    audPath = String.valueOf(audioFile);
//                    seekbar = findViewById(R.id.seekBar);
//                    tvAudio.setVisibility(View.VISIBLE);
//                    tvAudio.setText(time);
//                    llAudio.setVisibility(View.VISIBLE);
//                    mediaPlayer = new MediaPlayer();
//                    try {
//
//                        mediaPlayer.setDataSource(fileAudo.getPath());
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                    final int[] playStatus = {0};
//                    ivPlay.setOnClickListener(v -> {
//
//                        isAudioPlaying = true;
//                        if (playStatus[0] == 0) {//play
//
//                            Glide.with(AddNewNoteActivity.this)
//                                    .load(R.drawable.ic_pause_blue)
//                                    .dontAnimate()
//                                    .into(ivPlay);
//
//                            int oneTimeOnly = 0;
//                            mediaPlayer.start();
//
//                            finalTime = mediaPlayer.getDuration();
//                            startTime = mediaPlayer.getCurrentPosition();
//                            if (oneTimeOnly == 0) {
//                                seekbar.setMax((int) finalTime);
//                                oneTimeOnly = 1;
//                            }
//                            seekbar.setProgress((int) startTime);
//                            myHandler.postDelayed(UpdateSongTime, 100);
//                            playStatus[0] = 1;
//
//                        } else {//pause
//
//                            Glide.with(AddNewNoteActivity.this)
//                                    .load(R.drawable.ic_play_blue)
//                                    .dontAnimate()
//                                    .into(ivPlay);
//                            playStatus[0] = 0;
//
//                            finalTime = mediaPlayer.getDuration();
//                            startTime = mediaPlayer.getCurrentPosition();
//
//                            mediaPlayer.pause();
//
//                        }
//                    });
//
//                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//                        @Override
//                        public void onCompletion(MediaPlayer mp) {
//                            isAudioPlaying = false;
//                            mediaPlayer.seekTo(0);
//                            Glide.with(AddNewNoteActivity.this)
//                                    .load(R.drawable.ic_play_blue)
//                                    .dontAnimate()
//                                    .into(ivPlay);
//                        }
//                    });
//
//                    seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//                        @Override
//                        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                            if (fromUser) {
//                                mediaPlayer.seekTo(progress);
//                                seekBar.setProgress(progress);
//                                mediaPlayer.start();
//                                Glide.with(AddNewNoteActivity.this)
//                                        .load(R.drawable.ic_pause_blue)
//                                        .dontAnimate()
//                                        .into(ivPlay);
//                            }
//                        }
//
//                        @Override
//                        public void onStartTrackingTouch(SeekBar seekBar) {
//                        }
//
//                        @Override
//                        public void onStopTrackingTouch(SeekBar seekBar) {
//
//                        }
//
//                    });
                }

            }
        }).show(getSupportFragmentManager(), "AddNewTaskDialog");

    }

    public void setAudioDatas() {

        if (isAudioExpanded) {
//            tvMoreAudio.setVisibility(View.GONE);
        } else {
            if (arrAudioDetails.size() == 1) tvMoreAudio.setVisibility(View.GONE);
            else if (arrAudioDetails.size() > 1) {
                tvMoreAudio.setVisibility(View.VISIBLE);
                tvMoreAudio.setText("+ (" + (arrAudioDetails.size() - 1) + " )");
            }
        }
//
//        if (arrAudioDetails.size() > 1) {
//            tvMoreAudio.setVisibility(View.VISIBLE);
//        } else tvMoreAudio.setVisibility(View.GONE);

        AudFile audFile = new AudFile();
        for (int i = 0; i < arrAudioDetails.size(); i++) {

            if (i == 0) {
                final int[] playStatus = {0};
                llAudioOne.setVisibility(View.VISIBLE);
                llAudioTwo.setVisibility(View.GONE);
                llAudioThree.setVisibility(View.GONE);

                String uri = URL_ImageUpload_ROOT + arrAudioDetails.get(0).getFilename();
                Log.e("Aud_uri", arrAudioDetails.size() + "...." + uri);
//                seekBarOne = findViewById(R.id.seekBar);
                tvAudioOne.setText(arrAudioDetails.get(0).getStrDuration());
                mediaPlayerOne = MediaPlayer.create(AddNewNoteActivity.this, Uri.parse(uri));

                audFile.setDuration(arrAudioDetails.get(0).getStrDuration());
                audFile.setFile(new File(uri));

                ivPlayOne.setOnClickListener(v -> {

                    isAudioPlaying = true;
                    if (playStatus[0] == 0) {//play

                        Glide.with(AddNewNoteActivity.this)
                                .load(R.drawable.ic_pause_blue)
                                .dontAnimate()
                                .into(ivPlayOne);


                        mediaPlayerOne.start();
                        int oneTimeOnly = 0;

                        finalTime = mediaPlayerOne.getDuration();
                        startTime = mediaPlayerOne.getCurrentPosition();
                        if (oneTimeOnly == 0) {
                            seekBarOne.setMax((int) finalTime);
                            oneTimeOnly = 1;
                        }
                        seekBarOne.setProgress((int) startTime);
                        myHandler.postDelayed(UpdateSongTimeOne, 100);
                        playStatus[0] = 1;

                    } else {//pause

                        Glide.with(AddNewNoteActivity.this)
                                .load(R.drawable.ic_play_blue)
                                .dontAnimate()
                                .into(ivPlayOne);
                        playStatus[0] = 0;
                        finalTime = mediaPlayerOne.getDuration();
                        startTime = mediaPlayerOne.getCurrentPosition();

                        mediaPlayerOne.pause();

                    }

                });
                mediaPlayerOne.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        isAudioPlaying = false;
                        mediaPlayerOne.seekTo(0);
                        Glide.with(AddNewNoteActivity.this)
                                .load(R.drawable.ic_play_blue)
                                .dontAnimate()
                                .into(ivPlayOne);
                    }
                });
                seekBarOne.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (fromUser) {
                            mediaPlayerOne.seekTo(progress);
                            seekBar.setProgress(progress);
                            mediaPlayerOne.start();
                            Glide.with(AddNewNoteActivity.this)
                                    .load(R.drawable.ic_pause_blue)
                                    .dontAnimate()
                                    .into(ivPlayOne);
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }

                });


            } else if (i == 1) {
                final int[] playStatus = {0};
//                    llAudioOne.setVisibility(View.VISIBLE);
//                    llAudioTwo.setVisibility(View.VISIBLE);
                llAudioThree.setVisibility(View.GONE);

                String uri = URL_ImageUpload_ROOT + arrAudioDetails.get(1).getFilename();
//                seekBarTwo = findViewById(R.id.seekBar);
                tvAudioTwo.setText(arrAudioDetails.get(1).getStrDuration());
                mediaPlayerTwo = MediaPlayer.create(AddNewNoteActivity.this, Uri.parse(uri));

                audFile.setDuration(arrAudioDetails.get(1).getStrDuration());
                audFile.setFile(new File(uri));
                ivPlayTwo.setOnClickListener(v -> {

                    isAudioPlaying = true;
                    if (playStatus[0] == 0) {//play

                        Glide.with(AddNewNoteActivity.this)
                                .load(R.drawable.ic_pause_blue)
                                .dontAnimate()
                                .into(ivPlayTwo);


                        mediaPlayerTwo.start();
                        int oneTimeOnly = 0;

                        finalTime = mediaPlayerTwo.getDuration();
                        startTime = mediaPlayerTwo.getCurrentPosition();
                        if (oneTimeOnly == 0) {
                            seekBarTwo.setMax((int) finalTime);
                            oneTimeOnly = 1;
                        }
                        seekBarTwo.setProgress((int) startTime);
                        myHandler.postDelayed(UpdateSongTimeTwo, 100);
                        playStatus[0] = 1;

                    } else {//pause

                        Glide.with(AddNewNoteActivity.this)
                                .load(R.drawable.ic_play_blue)
                                .dontAnimate()
                                .into(ivPlayTwo);
                        playStatus[0] = 0;
                        finalTime = mediaPlayerTwo.getDuration();
                        startTime = mediaPlayerTwo.getCurrentPosition();

                        mediaPlayerTwo.pause();

                    }

                });

            } else if (i == 2) {
                final int[] playStatus = {0};
                String uri = URL_ImageUpload_ROOT + arrAudioDetails.get(2).getFilename();
//                seekBarThree = findViewById(R.id.seekBar);
                tvAudioThree.setText(arrAudioDetails.get(2).getStrDuration());
                mediaPlayerThree = MediaPlayer.create(AddNewNoteActivity.this, Uri.parse(uri));

                audFile.setDuration(arrAudioDetails.get(2).getStrDuration());
                audFile.setFile(new File(uri));
                ivPlayThree.setOnClickListener(v -> {

                    isAudioPlaying = true;
                    if (playStatus[0] == 0) {//play

                        Glide.with(AddNewNoteActivity.this)
                                .load(R.drawable.ic_pause_blue)
                                .dontAnimate()
                                .into(ivPlayThree);


                        mediaPlayerThree.start();
                        int oneTimeOnly = 0;

                        finalTime = mediaPlayerThree.getDuration();
                        startTime = mediaPlayerThree.getCurrentPosition();
                        if (oneTimeOnly == 0) {
                            seekBarThree.setMax((int) finalTime);
                            oneTimeOnly = 1;
                        }
                        seekBarThree.setProgress((int) startTime);
                        myHandler.postDelayed(UpdateSongTimeThree, 100);
                        playStatus[0] = 1;

                    } else {//pause

                        Glide.with(AddNewNoteActivity.this)
                                .load(R.drawable.ic_play_blue)
                                .dontAnimate()
                                .into(ivPlayThree);
                        playStatus[0] = 0;
                        finalTime = mediaPlayerThree.getDuration();
                        startTime = mediaPlayerThree.getCurrentPosition();

                        mediaPlayerThree.pause();

                    }

                });

                mediaPlayerThree.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        isAudioPlaying = false;
                        mediaPlayerThree.seekTo(0);
                        Glide.with(AddNewNoteActivity.this)
                                .load(R.drawable.ic_play_blue)
                                .dontAnimate()
                                .into(ivPlayThree);
                    }
                });
                seekBarThree.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        if (fromUser) {
                            mediaPlayerThree.seekTo(progress);
                            seekBar.setProgress(progress);
                            mediaPlayerThree.start();
                            Glide.with(AddNewNoteActivity.this)
                                    .load(R.drawable.ic_pause_blue)
                                    .dontAnimate()
                                    .into(ivPlayThree);
                        }
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }

                });

            }

        }
    }

    @androidx.annotation.RequiresApi(api = Build.VERSION_CODES.KITKAT)
    protected void onActivityResult(final int requestCode, int resultCode, Intent data) {

        Log.e("IMAGE_file", resultCode + "..." + data);
        if (requestCode == IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {

            if (data != null && data.getData() != null) {
                llFirst.setVisibility(View.GONE);

                Uri uri = getImageUri(this, data);
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String path = storeImage(bitmap);
                fileProfile = new File(path);
                flImage.setVisibility(View.VISIBLE);
                Glide.with(getApplicationContext())
                        .load(path)
                        .into(ivImageThree);
                BitmapFactory.Options options = new BitmapFactory.Options();
                imageHeight = bitmap.getHeight();
                imageWidth = bitmap.getWidth();

                saveImage(fileProfile, imageHeight, imageWidth);

            } else if (data != null && getImageUri(this, data) != null) {
                Bitmap bitmap = null;
                Uri uri = getImageUri(this, data);
                try {
                    Bitmap bitmap1 = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                    String path = storeImage(bitmap1);
                    File newfile = new File(RealPathUtil.getPath(getApplicationContext(), uri));
                    fileProfile = new File(path);
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    bitmap = BitmapFactory.decodeFile(newfile.getAbsolutePath(), options);
                    imageHeight = options.outHeight;
                    imageWidth = options.outWidth;
                    double height = getImageHeight(this, imageWidth, imageHeight);
//                    ViewGroup.LayoutParams params = ivImage.getLayoutParams();
//                    params.width = ViewGroup.LayoutParams.MATCH_PARENT;
//                    params.height = (int) height;

                    flImage.setVisibility(View.VISIBLE);

                    int tot = imgDetailss.size();
                    if (tot == 0) {//when no img is presnt in note
                        flImage.setVisibility(View.VISIBLE);
                        ivImageOne.setVisibility(View.VISIBLE);
                        ivImageTwo.setVisibility(View.GONE);
                        ivImageThree.setVisibility(View.GONE);
                        Glide.with(getApplicationContext())
                                .load(path)
                                .into(ivImageOne);
                    } else if (tot == 1) {//when one image is already present
                        ivImageOne.setVisibility(View.VISIBLE);
                        ivImageTwo.setVisibility(View.VISIBLE);
                        Glide.with(getApplicationContext())
                                .load(path)
                                .into(ivImageTwo);
                    } else if (tot == 2) {//when 2 image is already present
                        ivImageTwo.setVisibility(View.VISIBLE);
                        ivImageThree.setVisibility(View.VISIBLE);
                        Glide.with(getApplicationContext())
                                .load(path)
                                .into(ivImageThree);
                    }
                    saveImage(fileProfile, imageHeight, imageWidth);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (data.getClipData() != null) {
                mClipData = data.getClipData();

                if (mClipData.getItemCount() == 0) {
                    flImage.setVisibility(View.GONE);
                } else if (mClipData.getItemCount() > 3) {
                    flImage.setVisibility(View.GONE);
                    showToast("Can't select more than 3 images");
                } else {

                    int count = 100;
                    for (int i = 0; i < mClipData.getItemCount(); i++) {
                        Log.e("Dataa is ", count + i + "...");
                        ClipData.Item item = mClipData.getItemAt(i);
                        Uri uri = item.getUri();

                        /*
  Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
                    bitmap = compressedBitmap(bitmap);
                    mainPath = storeImage(bitmap);
 */

//                        String selectedFilePath = getPath(getApplicationContext(), uri);

                        Log.e("ClipData", i + "..." + uri);
                        int SPLASH_TIME_OUT = 0;//3000;

                        int finalI = i;
//                        new Handler().postDelayed(() -> {

                        ImgFile imgFile = new ImgFile();
                        try {
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mClipData.getItemAt(finalI).getUri());
                            bitmap = compressedBitmap(bitmap);
//                                    File fileNew = null;
//                                    if (i==0) {
//                                        String pathOne  = storeImage(bitmap);
//                                        File  fileNew = new File(pathOne);
//                                        imgFile.setFile(fileNew);
//                                    }else   if (i==0) {
//                                        String pathTwo  = storeImage(bitmap);
//                                        File fileNew = new File(pathTwo);
//                                        imgFile.setFile(fileNew);
//                                    }else if(i==2){
//                                        String pathThree  = storeImage(bitmap);
//                                        File fileNew = new File(pathThree);
//                                        imgFile.setFile(fileNew);
//                                    }

                            String pathOne = storeImage(bitmap);
                            File fileNew = new File(pathOne);
                            imgFile.setFile(fileNew);
                            Log.e("path------------------", finalI + "..." + imgFile.getFile());

                            imageHeight = bitmap.getHeight();
                            imageWidth = bitmap.getWidth();

                            imgFile.setHeight(imageHeight);
                            imgFile.setWeight(imageWidth);
                            fileArrayList.add(imgFile);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
//                            }, SPLASH_TIME_OUT);


                    }
                    if (isNetworkAvailable()) {
                        loadImage();
                    }
                }

            }
        } else if (requestCode == REQUEST_CODE) {
            planer_ID = Integer.parseInt(data.getStringExtra("ID"));
            llColleborate.removeAllViews();
            if (planer_ID != 0) {
                arryCollaboratUserData.clear();
                final ArrayList<ColleborateDatum> Colleborators = data.getParcelableArrayListExtra("Colleborators");
                getColleborateUsers();
            }
        } else if (requestCode == 1001) {

            int plannerId = Integer.parseInt(data.getStringExtra("plannerId"));

            RealmResults<Item> itemsRealmResults = mRealm.where(Item.class).equalTo("intPlannerId", plannerId).findAll();
            Log.e("onActivity", plannerId + "..." + itemsRealmResults.size() + "..." + itemsRealmResults.get(0).getImgNoteList().size());
//            mRealm.copyToRealmOrUpdate(itemsRealmResults.get(0));
//            mRealm.commitTransaction();
            setImages(itemsRealmResults.get(0).getImgNoteList());

        }
    }


    public void setImages(RealmList<ImgDetails> imgDetailss) {
        if (imgDetailss.size() == 0) {
            flImage.setVisibility(View.GONE);
        } else {
            flImage.setVisibility(View.VISIBLE);
            if (imgDetailss.size() == 1) {
                ivImageOne.setVisibility(View.VISIBLE);
                ivImageTwo.setVisibility(View.GONE);
                ivImageThree.setVisibility(View.GONE);
            } else if (imgDetailss.size() == 2) {
                ivImageOne.setVisibility(View.VISIBLE);
                ivImageTwo.setVisibility(View.VISIBLE);
                ivImageThree.setVisibility(View.GONE);
            } else if (imgDetailss.size() == 3) {
                ivImageOne.setVisibility(View.VISIBLE);
                ivImageTwo.setVisibility(View.VISIBLE);
                ivImageThree.setVisibility(View.VISIBLE);

            }
            mArrayUri.clear();
            for (int i = 0; i < imgDetailss.size(); i++) {

                int imageHeight = imgDetailss.get(i).getIntHeight();
                int imageWidth = imgDetailss.get(i).getIntWeight();
                String imagePath = URL_ImageUpload_ROOT + imgDetailss.get(i).getFilename();
                mArrayUri.add(imagePath);

                ImageView img;
                if (i == 0) {
                    img = ivImageOne;
                } else if (i == 1) {
                    img = ivImageTwo;
                } else {
                    img = ivImageThree;
                }
                Log.e("Img_Hgt", "..." + imagePath + "..." + imageWidth + "..." + imageHeight);
                if (imageWidth != 0 && imageHeight != 0) {

                    double height = getImageHeight(this, imageWidth, imageHeight);
                    fileProfile = new File(imagePath);
                    ViewGroup.LayoutParams params = img.getLayoutParams();
                    params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                    params.height = (int) height;
                    img.setLayoutParams(params);
                    Picasso.get()
                            .load(imagePath)
                            .noFade()
                            .into(img, new Callback() {
                                @Override
                                public void onSuccess() {
                                    supportStartPostponedEnterTransition();
                                }

                                @Override
                                public void onError(Exception e) {
                                    supportStartPostponedEnterTransition();
                                }

                            });

                }
            }
        }
    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(AddNewNoteActivity.this, MyService.class);
        intent.putExtra("activity", "AddNewNoteActivity");
        intent.putExtra("close", true);
        startService(intent);

        if (isAudioPlaying) {
            if (mediaPlayerOne.isPlaying()) {
                mediaPlayerOne.stop();
            }
            if (mediaPlayerTwo.isPlaying()) {
                mediaPlayerTwo.stop();
            }
            if (mediaPlayerThree.isPlaying()) {
                mediaPlayerThree.stop();
            }
        }

        ArrayList<String> toSingleStringList = new ArrayList<>();
        if (isCheckBox.equals("Checkboxes")) {
            todoCheckboxItems.clear();
            if (!alleditText.isEmpty()) {
                for (int j = 0; j < alleditText.size(); j++) {
                    toSingleStringList.add(alleditText.get(j).getText().toString());
                }
            } else {
                toSingleStringList.add(tvText.getText().toString());
            }

        } else {
            todoCheckboxItems.clear();
            if (!alleditTextCheckbox.isEmpty()) {

                for (int j = 0; j < alleditTextCheckbox.size(); j++) {

                    TodoListItem todoListItem = new TodoListItem();
                    String isChk = String.valueOf(alleditTextCheckbox.get(j).getTag());
                    if (isChk.equals("true")) {
                        todoListItem.setStrNoteListFlage(true);
                    } else {
                        todoListItem.setStrNoteListFlage(false);
                    }
                    todoListItem.setStrNoteListValue(alleditTextCheckbox.get(j).getText().toString());
                    todoCheckboxItems.add(todoListItem);
                }
            }
        }

        if (toSingleStringList.size() != 0) {
            if (toSingleStringList.size() == 1) {
                toSingleString = new StringBuffer(toSingleStringList.size());
                toSingleString.delete(0, toSingleString.length());
                toSingleString.append(toSingleStringList.get(0));
            } else {

                toSingleString = new StringBuffer(toSingleStringList.size());
                toSingleString.delete(0, toSingleString.length());

                for (String s : toSingleStringList) {
                    if (s != null) {
                        toSingleString.append(s).append('\n');
                    }
                }
                String itemList = toSingleString.toString();
            }

        } else {
            toSingleString = new StringBuffer(toSingleStringList.size());
            toSingleString.delete(0, toSingleString.length());
            toSingleString.append("");
        }
        if (isNetworkAvailable()) {
            String title = tvTitle.getText().toString();
            String valString = toSingleString.toString();
            int k = 0;
            for (int i = 0; i < todoCheckboxItems.size(); i++) {
                if (todoCheckboxItems.get(i).getStrNoteListValue().trim().length() == 0) {
                    k++;
                }
            }
            if ((title.trim().length() == 0) && (valString.trim().length() == 0) && (todoCheckboxItems.size() == k)) {//&& (arryCollaboratUserData.size() != 0)) {// && (fileProfile.length() == 0) && (fileAudo.length() == 0)
                if (arryCollaboratUserData.size() != 0) {
                    finish();
                } else {
                    Intent returnIntent = new Intent();
                    setResult(Activity.RESULT_CANCELED, returnIntent);
                    super.finish();
                }
            } else {
                if (planer_ID == 0) {
                    addNote();
                } else {
                    updateData();
                }
            }

        } else {
            Intent returnIntent = new Intent();
            setResult(Activity.RESULT_CANCELED, returnIntent);
            super.finish();
        }
        super.onBackPressed();

    }

    public void finish() {
        if (isAudioPlaying) {
            if (mediaPlayerOne.isPlaying()) {
                mediaPlayerOne.stop();
            }
            if (mediaPlayerTwo.isPlaying()) {
                mediaPlayerTwo.stop();
            }
            if (mediaPlayerThree.isPlaying()) {
                mediaPlayerThree.stop();
            }
        }
        if (planer_ID != 0) {
            Item item = new Item();
            item.setStrTitle(tvTitle.getText().toString());
            item.setStrValue(toSingleString.toString());
            item.setArryObjstrTodoList(todoCheckboxItems);
            item.setImgNoteList(imgDetailss);
            item.setFileAudioList(arrAudioDetails);
//            item.setIntHeight(height);
//            item.setIntWeight(weight);
            item.setArryCollaboratUserId(collUsers);
            item.setIntCreateUserId(strUserId);

            item.setDatModifiedDateAndTime("");
            item.setfkIntCategoryTypeId(strCategoryTypeId);
            item.setfkIntCategoryId(strCategoryId);
            item.setfkIntSubCategoryId(strSubCategoryId);

            item.setStrCategoryTypeName(PlannerTabActivity.strCategoryTypeName);
            item.setStrCategoryName(PlannerTabActivity.strCategoryName);
            item.setStrDefaultSubName(PlannerTabActivity.strSubCategoryName);

            item.setBlnPinNote(isPinned);
            item.setIntPlannerId(planer_ID);
            item.setIntProjectId(mUserDetails.getArryselectedProjectAllItems().get(0).getId());
            item.setIntOrganisationId(mUserDetails.getArryselectedOrganisationAllItems().get(0).getId());
            item.setObjRemainder(objRemainder);
//            item.setStrDuration(tvAudio.getText().toString());
            noteUpdatedList.clear();//when collbrate is called
            noteUpdatedList.add(item);

            Gson gson = new Gson();
            String val = gson.toJson(noteUpdatedList.get(0));

            if (isNetworkAvailable()) {
                Intent returnIntent = new Intent();
                if (val.length() != 0) {
                    returnIntent.putExtra("item", val);
                }
                setResult(3, returnIntent); //By not passing the intent in the result, the calling activity will get null data.
                super.finish();
            }

        }
    }

    @SuppressLint("ResourceType")
    public void convertDatas() {

        if (isCheckBox.equals("Checkboxes")) { //convert textformat to checkboxes
            isCheckBox = "TextFormat";
            isType = "TextFormat";
            convertToCheckBoxes();
            ivConvert.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_checkbox_colorprimary));
        } else {
            isCheckBox = "Checkboxes";
            isType = "Checkboxes";
            convertToTextFormat();
            ivConvert.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_checkbox_grey));
        }

    }

    @SuppressLint("ResourceType")
    public void convertToCheckBoxes() {

        rlCheckbox.removeAllViews();
        rlCheckbox.setVisibility(View.VISIBLE);
        tvText.setVisibility(View.GONE);
        ivAdd.setVisibility(View.GONE);//VISIBLE

        ArrayList<String> allStrings = new ArrayList<>();

        if (!alleditText.isEmpty()) {

            for (int j = 0; j < alleditText.size(); j++) {

                String nixSampleLine = alleditText.get(j).getText().toString();

                if (nixSampleLine.length() != 0) {
                    String[] lines = nixSampleLine.split("\\r?\\n");
                    for (String line : lines) {
                        System.out.println(line);
                        allStrings.add(line);
                    }
                }
            }
            alleditText.clear();
        }

        alleditTextCheckbox.clear();
        if (allStrings.size() != 0) {//toSingleStringList contains value
            for (int i = 0; i < allStrings.size(); i++) {
                mLinearLayout1 = findViewById(R.id.rlCheckbox);
                view1 = View.inflate(AddNewNoteActivity.this, R.layout.checkbox_layout, null);
                final RelativeLayout llMain = view1.findViewById(R.id.llMain);
                etTextCheckBox = view1.findViewById(R.id.etText);
                final ImageView ivCheckbox = view1.findViewById(R.id.ivCheckbox);
                final ImageView ivClose = view1.findViewById(R.id.ivClose);
//                alleditText.add(etTextCheckBox);
                textStyle(etTextCheckBox);
//                etTextCheckBox.setTextColor(getResources().getColor(R.color.text_note));
//                etTextCheckBox.applyCustomFont(this, getResources().getString(R.string.roboto_light));
//                etTextCheckBox.setBackgroundResource(getResources().getColor(android.R.color.transparent));
                etTextCheckBox.requestFocus();
                etTextCheckBox.setText(allStrings.get(i));
                etTextCheckBox.setSelection(etTextCheckBox.getText().length());
                etTextCheckBox.setCursorVisible(true);
                etTextCheckBox.setTag(false);
                etTextCheckBox.setId(i);
//                etTextCheckBox.setTextSize(14);
                final int finalI = i;
                todoListItem.setId("0");
                todoListItem.setStrNoteListValue(allStrings.get(i));
                alleditTextCheckbox.add(etTextCheckBox);
                mLinearLayout1.addView(view1, i);

                etTextCheckBox.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        llBottom.setVisibility(View.VISIBLE);
                        if (undoCheckboxesArraylist.size() == 0) {
                            ivUndo.setEnabled(false);
                            ivUndo.setColorFilter(colorGrey);
                        } else {
                            ivUndo.setEnabled(true);
                            ivUndo.setColorFilter(colorBlack);

                        }
                        isChangedCheckbox = "Yes";
                        String _text = etTextCheckBox.getText().toString();
                        if (_text.isEmpty()) {
                        } else {
                            UndoalleditText = alleditTextCheckbox;
                        }

                    }
                });
                int finalI1 = i;

//                etTextCheckBox.setOnKeyListener((v, keyCode, event) -> {
//                    if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
//                        addEnterKeyPressed(finalI1 + 1, "");
//                        return true;
//                    } else if (keyCode == KeyEvent.KEYCODE_DEL) {
//
//                        if (alleditTextCheckbox.size() > 1) {
//
//                            (alleditTextCheckbox.get(alleditTextCheckbox.size() - 1)).setCursorVisible(true);
//                            (alleditTextCheckbox.get(alleditTextCheckbox.size() - 1)).setFocusable(true);
//                            (alleditTextCheckbox.get(alleditTextCheckbox.size() - 1)).setSelection(alleditTextCheckbox.get(alleditTextCheckbox.size() - 1).getText().length());
//                            String val = alleditTextCheckbox.get(alleditTextCheckbox.size() - 1).getText().toString();
//
//                            if (val.length() == 0) {
//                                rlCheckbox.removeView((View) v.getParent());
//                                alleditTextCheckbox.remove(alleditTextCheckbox.size() - 1);
//                                (alleditTextCheckbox.get(alleditTextCheckbox.size() - 1)).setSelection(alleditTextCheckbox.get(alleditTextCheckbox.size() - 1).getText().length());
//                                (alleditTextCheckbox.get(alleditTextCheckbox.size() - 1)).requestFocus();
//                                (alleditTextCheckbox.get(alleditTextCheckbox.size() - 1)).setCursorVisible(true);
//                                (alleditTextCheckbox.get(alleditTextCheckbox.size() - 1)).setFocusable(true);
//                            }
//
//                        }
//                    }
//                    return false;
//                });

                llMain.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        ivClose.setVisibility(View.GONE);//VISIBLE
                        etTextCheckBox.setCursorVisible(true);
                        return false;
                    }
                });

                etTextCheckBox.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View view, MotionEvent motionEvent) {
                        ivClose.setVisibility(View.GONE);//VISIBLE
                        etTextCheckBox.setCursorVisible(true);
                        return false;
                    }
                });

                ivClose.setOnClickListener(view -> rlCheckbox.removeView((View) view.getParent()));

            }

            for (int i = 0; i < mLinearLayout1.getChildCount(); i++) {
                View nextChild = (mLinearLayout1).getChildAt(i);
                String val = ((CustomEditText) ((RelativeLayout) nextChild).getChildAt(1)).getText().toString();

                ((RelativeLayout) nextChild).getChildAt(0).setOnTouchListener((view, motionEvent) -> {

                    ((CustomEditText) ((RelativeLayout) nextChild).getChildAt(1)).setCursorVisible(true);
                    String isCheck = String.valueOf(((RelativeLayout) nextChild).getChildAt(1).getTag());

                    if (isCheck.equals("true")) {
                        ((RelativeLayout) nextChild).getChildAt(1).setTag(false);
                        ((ImageView) ((RelativeLayout) nextChild).getChildAt(0)).setImageDrawable(getResources().getDrawable(R.drawable.ic_unchecked));
                        ((CustomEditText) ((RelativeLayout) nextChild).getChildAt(1)).setPaintFlags(etTextCheckBox.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                    } else {
                        ((RelativeLayout) nextChild).getChildAt(1).setTag(true);
                        ((ImageView) ((RelativeLayout) nextChild).getChildAt(0)).setImageDrawable(getResources().getDrawable(R.drawable.ic_checked));
                        ((CustomEditText) ((RelativeLayout) nextChild).getChildAt(1)).setPaintFlags(etTextCheckBox.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    }

                    return false;

                });
            }
            loopViewsCheckbox(mLinearLayout1);

        } else {//toSingleStringList contains no value ie todoCheckboxItems contains value

            rlCheckbox.setVisibility(View.VISIBLE);

            mLinearLayout1 = findViewById(R.id.rlCheckbox);
            view1 = View.inflate(AddNewNoteActivity.this, R.layout.checkbox_layout, null);
            final RelativeLayout llMain = view1.findViewById(R.id.llMain);
            etTextCheckBox = view1.findViewById(R.id.etText);
            final ImageView ivCheckbox = view1.findViewById(R.id.ivCheckbox);
            final ImageView ivClose = view1.findViewById(R.id.ivClose);
//            alleditText.add(etTextCheckBox);
//            etTextCheckBox.setTextColor(getResources().getColor(R.color.text_note));
//            etTextCheckBox.applyCustomFont(this, getResources().getString(R.string.roboto_light));
//            etTextCheckBox.setBackgroundResource(getResources().getColor(android.R.color.transparent));
            etTextCheckBox.requestFocus();
            etTextCheckBox.setCursorVisible(true);
            etTextCheckBox.setId(1);
//            etTextCheckBox.setTextSize(14);
            etTextCheckBox.setSelection(etTextCheckBox.getText().length());

            ivClose.setOnClickListener(view -> rlCheckbox.removeView((View) view.getParent()));
            alleditTextCheckbox.add(etTextCheckBox);
            etTextCheckBox.setTypeface(font_roboto_regular);
            mLinearLayout1.addView(view1);

//            etTextCheckBox.setOnKeyListener(new View.OnKeyListener() {
//                public boolean onKey(View v, int keyCode, KeyEvent event) {
//                    if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
//                        addEnterKeyPressed(1, "");
//                        return true;
//                    }
//                    return false;
//                }
//            });

        }

    }

    @SuppressLint("ResourceType")
    public void convertToTextFormat() {

        rlCheckbox.removeAllViews();
        rlCheckbox.setVisibility(View.VISIBLE);
        tvText.setVisibility(View.GONE);
        ivAdd.setVisibility(View.GONE);
        ArrayList<TodoListItem> todoCheckboxItem = new ArrayList<>();
        if (!alleditTextCheckbox.isEmpty()) {

            for (int j = 0; j < alleditTextCheckbox.size(); j++) {

                String val = alleditTextCheckbox.get(j).getText().toString();
                if (val.length() != 0) {//only add checkbox which contains values and remove if it is null
                    TodoListItem todoListItem = new TodoListItem();
                    todoListItem.setStrNoteListFlage(false);
                    todoListItem.setStrNoteListValue(val);
                    todoCheckboxItem.add(todoListItem);
                }

            }
        }

        if (todoCheckboxItem.size() != 0) {

            alleditText.clear();
            for (int i = 0; i < todoCheckboxItem.size(); i++) {

                editText = new CustomEditText(AddNewNoteActivity.this);
                editText.setText(todoCheckboxItem.get(i).getStrNoteListValue());
//                alleditText.add(editText);
                textStyle(editText);
                editText.setId(i);
                alleditText.add(editText);
                editText.setLeft(10);
                editText.requestFocus();
                editText.setSelection(editText.getText().length());
                editText.setCursorVisible(true);
                editText.setPadding(16, 16, 16, 16);
                editText.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                rlCheckbox.addView(editText, i);

                int finalI = i;
//                editText.setOnKeyListener((v, keyCode, event) -> {
//                    // If the event is a key-down event on the "enter" button
//                    if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
//                            (keyCode == KeyEvent.KEYCODE_ENTER)) {
//                        // Perform action on key press
//                        addEditTextEnterKeyPressed(finalI, "");
//                        return true;
//                    }
//                    return false;
//                });

                editText.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        redoTextsArraylist.clear();
                        ivRedo.setEnabled(false);
                        ivRedo.setColorFilter(colorGrey);
                        llBottom.setVisibility(View.VISIBLE);
                        if (undoTextsArraylist.size() == 0) {
                            ivUndo.setEnabled(false);
                            ivUndo.setColorFilter(colorGrey);
                        } else {
                            ivUndo.setEnabled(true);
                            ivUndo.setColorFilter(colorBlack);
                        }
                        isChanged = "Yes";
                        isActiveText = "No";
                        UndoalleditText = alleditText;
                    }

                });

            }
            loopViews(rlCheckbox);
        } else {//toSingleStringList contains no value ie todoCheckboxItems contains value

            editText = new CustomEditText(AddNewNoteActivity.this);
//            alleditText.add(editText);
//            editText.setTextSize(14);
            editText.requestFocus();
            editText.setCursorVisible(true);
            alleditText.add(editText);
            editText.setId(1);
            textStyle(editText);
//            editText.setBackgroundResource(getResources().getColor(android.R.color.transparent));
//            editText.setTextColor(getResources().getColor(R.color.text_note));
//            editText.setTypeface(font_roboto_light);
            editText.setLeft(10);
            editText.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

            rlCheckbox.addView(editText);
            toSingleString = new StringBuffer(1);
            toSingleString.append(editText.getText().toString());

            rlMain.setOnTouchListener((view, motionEvent) -> {
                editText.setSelection(editText.getText().length());
                editText.requestFocus();
                editText.setCursorVisible(true);
                return false;
            });

//            editText.setOnKeyListener((v, keyCode, event) -> {
//                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
//                    addEditTextEnterKeyPressed(1, "");
//                    return true;
//                }
//                return false;
//            });

        }
    }

    public void addEnterKeyPressed(int position, String newVal) {

        final int[] checked = {1};
        checkboxCount = checkboxCount + 1;

        mLinearLayout1 = findViewById(R.id.rlCheckbox);
        view1 = View.inflate(AddNewNoteActivity.this, R.layout.checkbox_layout, null);

        final RelativeLayout llMain1 = view1.findViewById(R.id.llMain);
        etTextCheckBox = view1.findViewById(R.id.etText);
        final ImageView ivCheckbox1 = view1.findViewById(R.id.ivCheckbox);
        final ImageView ivClose1 = view1.findViewById(R.id.ivClose);


        textStyle(etTextCheckBox);
        etTextCheckBox.requestFocus();
        etTextCheckBox.setText(newVal);
        etTextCheckBox.setId(position);
        etTextCheckBox.setTag(false);
        etTextCheckBox.setCursorVisible(true);

        todoListItem.setId(String.valueOf(checkboxCount));
        todoListItem.setStrNoteListValue(etTextCheckBox.getText().toString());

        alleditTextCheckbox.add(position, etTextCheckBox);
        mLinearLayout1.addView(view1, position);

        etTextCheckBox.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                redoCheckboxesArraylist.clear();
                ivRedo.setEnabled(false);
                ivRedo.setColorFilter(colorGrey);
                llBottom.setVisibility(View.VISIBLE);
                if (undoCheckboxesArraylist.size() == 0) {
                    ivUndo.setEnabled(false);
                    ivUndo.setColorFilter(colorGrey);
                } else {
                    ivUndo.setEnabled(true);
                    ivUndo.setColorFilter(colorBlack);
                }
                isChangedCheckbox = "Yes";
                isActiveCheckbox = "No";
                UndoalleditText = alleditTextCheckbox;

            }
        });

        loopViewsCheckbox(mLinearLayout1);

        rlMain.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                etTextCheckBox.setSelection(etTextCheckBox.getText().length());
                etTextCheckBox.requestFocus();
                etTextCheckBox.setCursorVisible(true);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(etTextCheckBox, InputMethodManager.SHOW_IMPLICIT);
                return false;
            }
        });

        Boolean[] isChecked = {false};
        llMain1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                ivClose1.setVisibility(View.GONE);//VISIBLE
                etTextCheckBox.setCursorVisible(true);
                return false;
            }
        });

        etTextCheckBox.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                ivClose1.setVisibility(View.GONE);//VISIBLE
                etTextCheckBox.setCursorVisible(true);
                return false;
            }
        });

        for (int i = 0; i < mLinearLayout1.getChildCount(); i++) {
            View nextChild = (mLinearLayout1).getChildAt(i);
            String val = ((CustomEditText) ((RelativeLayout) nextChild).getChildAt(1)).getText().toString();

            ((RelativeLayout) nextChild).getChildAt(0).setOnTouchListener((view, motionEvent) -> {

                ((CustomEditText) ((RelativeLayout) nextChild).getChildAt(1)).setCursorVisible(true);
                String isCheck = String.valueOf(((RelativeLayout) nextChild).getChildAt(1).getTag());

                if (isCheck.equals("true")) {
                    ((RelativeLayout) nextChild).getChildAt(1).setTag(false);
                    ((ImageView) ((RelativeLayout) nextChild).getChildAt(0)).setImageDrawable(getResources().getDrawable(R.drawable.ic_unchecked));
                    ((CustomEditText) ((RelativeLayout) nextChild).getChildAt(1)).setPaintFlags(etTextCheckBox.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
                } else {
                    ((RelativeLayout) nextChild).getChildAt(1).setTag(true);
                    ((ImageView) ((RelativeLayout) nextChild).getChildAt(0)).setImageDrawable(getResources().getDrawable(R.drawable.ic_checked));
                    ((CustomEditText) ((RelativeLayout) nextChild).getChildAt(1)).setPaintFlags(etTextCheckBox.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                }

                return false;

            });
        }

        ivClose1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                ivClose1.setAlpha(0f);
                return false;
            }
        });

        if (checkboxCount == 0) {
        } else {

            for (int j = 0; j < checkboxCount; j++) {

                String isChk = String.valueOf(etTextCheckBox.getTag());
                if (isChk.equals("true")) {
                    todoListItem.setStrNoteListFlage(true);
                } else {
                    todoListItem.setStrNoteListFlage(false);
                }

                todoListItem.setId(String.valueOf(j));
                todoListItem.setStrNoteListValue(etTextCheckBox.getText().toString());
                todoCheckboxItems.add(todoListItem);
                alleditText.add(etTextCheckBox);

            }
        }
    }

    @SuppressLint("ResourceType")
    public void addEditTextEnterKeyPressed(int position, String newVal) {
        editText = new CustomEditText(AddNewNoteActivity.this);
        editText.setHint("");
        editText.setId(position);
        editText.setText(newVal);
        textStyle(editText);
        editText.setLeft(10);
        editText.requestFocus();
        isActiveText = "Yes";
        editText.setCursorVisible(true);
        editText.setPadding(16, 16, 16, 16);
        editText.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        rlCheckbox.addView(editText, position);
        alleditText.add(position, editText);

        loopViews(rlCheckbox);

        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                redoTextsArraylist.clear();
                ivRedo.setEnabled(false);
                ivRedo.setColorFilter(colorGrey);
                llBottom.setVisibility(View.VISIBLE);
                if (undoTextsArraylist.size() == 0) {
                    ivUndo.setEnabled(false);
                    ivUndo.setColorFilter(colorGrey);
                } else {
                    ivUndo.setEnabled(true);
                    ivUndo.setColorFilter(colorBlack);
                }
                isChanged = "Yes";
                isActiveText = "No";
                UndoalleditText = alleditText;
            }

        });

        rlMain.setOnTouchListener((view, motionEvent) -> {
            editText.setSelection(editText.getText().length());
            editText.requestFocus();
            editText.setCursorVisible(true);
            return false;
        });
    }

    private void loopViewsCheckbox(ViewGroup parent) {

        for (int i = 0; i < parent.getChildCount(); i++) {
            View child = parent.getChildAt(i);

            if (child instanceof RelativeLayout) {
                int finalI = i;

                ((RelativeLayout) child).getChildAt(1).setOnKeyListener((v, keyCode, event) -> {
                    if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        // Perform action on key press

                        String val = ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getText().toString();
                        String newVal = val.substring((((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getSelectionStart()));
                        ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).setText(val.substring(0, (((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getSelectionStart())));
                        ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).setSelection((((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getText().length()));
                        addEnterKeyPressed(finalI + 1, newVal);
                        return true;
                    } else if (keyCode == KeyEvent.KEYCODE_DEL) {

                        String modifyText = null;
                        modifyText = ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getText().toString();
                        int curPos = ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getSelectionStart();

                        if (curPos == 0) {
                            if (finalI == 0) {
                                parent.getChildAt(0).requestFocus();
                            } else {
                                rlCheckbox.removeView(parent.getChildAt(finalI));
                                parent.getChildAt(finalI - 1).requestFocus();

                                String newTextVal = ((CustomEditText) ((RelativeLayout) parent.getChildAt(finalI - 1)).getChildAt(1)).getText().toString();
                                modifyText = newTextVal + modifyText;

                                ((CustomEditText) ((RelativeLayout) parent.getChildAt(finalI - 1)).getChildAt(1)).setText(modifyText);
                                ((CustomEditText) ((RelativeLayout) parent.getChildAt(finalI - 1)).getChildAt(1)).setSelection(modifyText.length());
                            }
                        }
                        loopViewsCheckbox(rlCheckbox);

                    }
                    return false;
                });

            }
        }

    }

    private void loopViews(ViewGroup parent) {

        for (int i = 0; i < parent.getChildCount(); i++) {
            View child = parent.getChildAt(i);
            if (child instanceof CustomEditText) {
                int finalI = i;

                child.setOnKeyListener(new View.OnKeyListener() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {

                        Toast.makeText(AddNewNoteActivity.this, "keyCode.." + keyCode, Toast.LENGTH_LONG).show();
                        if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                            // Perform action on key press

                            // Perform action on key press
                            String val = ((CustomEditText) child).getText().toString();
                            String newVal = val.substring(((CustomEditText) child).getSelectionStart());

                            ((CustomEditText) child).setText(val.substring(0, ((CustomEditText) child).getSelectionStart()));
                            ((CustomEditText) child).setSelection(((CustomEditText) child).getText().length());
                            addEditTextEnterKeyPressed(finalI + 1, newVal);

                            return true;
                        } else if (keyCode == KeyEvent.KEYCODE_DEL) {

                            String modifyText = null;
                            modifyText = ((CustomEditText) child).getText().toString();
                            int curPos = ((CustomEditText) child).getSelectionStart();

                            if (curPos == 0) {
                                if (finalI == 0) {
                                    parent.getChildAt(0).requestFocus();
                                } else {

                                    rlCheckbox.removeView(parent.getChildAt(finalI));
                                    parent.getChildAt(finalI - 1).requestFocus();
                                    alleditText.remove(finalI);
                                    String newTextVal = ((CustomEditText) parent.getChildAt(finalI - 1)).getText().toString();
                                    modifyText = newTextVal + modifyText;

                                    ((CustomEditText) parent.getChildAt(finalI - 1)).setText(modifyText);
                                    ((CustomEditText) parent.getChildAt(finalI - 1)).setSelection(modifyText.length());

                                }
                            }

                            loopViews(rlCheckbox);

                        }
                        return false;
                    }
                });
            }
        }

    }

    public void addNote() {

        RequestParams params = new RequestParams();
        params.put("strTitle", tvTitle.getText().toString());
        params.put("strColor", "#ffffff");
        params.put("strCreateUserId", strUserId);
//        params.put("strPlannerMainId", catID);
        params.put("strCategoryTypeId", strCategoryTypeId);
        params.put("strCategoryId", strCategoryId);
        params.put("strSubCategoryId", strSubCategoryId);
        params.put("intPlannerId", String.valueOf(planer_ID));

        params.put("blnPinNote", isPinned);
        params.put("intProjectId", mUserDetails.getArryselectedProjectAllItems().get(0).getId());
        params.put("intOrganisationId", mUserDetails.getArryselectedOrganisationAllItems().get(0).getId());

        params.put("objRemainder", new Gson().toJson(objRemainder));//'{"strRemainderDate":"","strRemainderTime":"","strType":"","blnFlage":false}',

        params.put("strValue", toSingleString.toString());
        params.put("arryObjstrTodoList", new Gson().toJson(todoCheckboxItems));

        showProgressDialog(this, "Please Wait...");
        ApiCallWithToken(url_addNotess, ApiCall.ADDPLANNER, params);

    }

    public void updateData() {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

        RequestParams params = new RequestParams();
        params.put("strTitle", tvTitle.getText().toString());
        params.put("strColor", "#ffffff");
        params.put("strCreateUserId", strUserId);

        params.put("strCategoryTypeId", strCategoryTypeId);
        params.put("strCategoryId", strCategoryId);
        params.put("strSubCategoryId", strSubCategoryId);
        params.put("intPlannerId", String.valueOf(planer_ID));
        params.put("blnPinNote", isPinned);

        params.put("intProjectId", mUserDetails.getArryselectedProjectAllItems().get(0).getId());
        params.put("intOrganisationId", mUserDetails.getArryselectedOrganisationAllItems().get(0).getId());

        params.put("strModifiedUserId", strUserId);//strModifiedUserId
        params.put("datCreateDateAndTime", sdf.format(new Date()));//initial note created date and time
        params.put("objRemainder", new Gson().toJson(objRemainder));//'{"strRemainderDate":"","strRemainderTime":"","strType":"","blnFlage":false}',

        params.put("strValue", toSingleString.toString());
        params.put("arryObjstrTodoList", new Gson().toJson(todoCheckboxItems));

        ApiCallWithToken(urlSaveAllNotes, ApiCall.UPDATEPLANNER, params);

    }

    public void loadImage() {

        int SPLASH_TIME_OUT = 0;//3000

        if (fileArrayList.size() > 0) {

            new Handler().postDelayed(() -> {
                saveImage(fileArrayList.get(0).getFile(), fileArrayList.get(0).getHeight(), fileArrayList.get(0).getWeight());
                if (fileArrayList.get(0).getFile().length() != 0) {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    bitmap = BitmapFactory.decodeFile(fileArrayList.get(0).getFile().getAbsolutePath(), options);
                    SaveImageToFolder(fileArrayList.get(0).getFile());
                } else {
                    ivImageOne.setVisibility(View.GONE);
                }
                fileArrayList.remove(0);
            }, SPLASH_TIME_OUT);

        }
    }

    public void saveImage(File fileProfile, int imageHeight, int imageWidth) {

        RequestParams params = new RequestParams();
        try {
            params.put("file", fileProfile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        params.put("intPlannerId", String.valueOf(planer_ID));

        params.put("strCategoryTypeId", strCategoryTypeId);
        params.put("strCategoryId", strCategoryId);
        params.put("strSubCategoryId", strSubCategoryId);
        if (planer_ID == 0) {
            params.put("strCreateUserId", strUserId);//strModifiedUserId if planner id == 0
        } else {
            params.put("strModifiedUserId", strUserId);//strModifiedUserId if planner id != 0
        }

        params.put("intHeight", String.valueOf(imageHeight));
        params.put("intWeight", String.valueOf(imageWidth));
        params.put("intProjectId", mUserDetails.getArryselectedProjectAllItems().get(0).getId());
        params.put("intOrganisationId", mUserDetails.getArryselectedOrganisationAllItems().get(0).getId());
//        showProgressDialog(this, "Uploading...");

        ApiCallWithToken(urlImage, ApiCall.IMAGE, params);


    }

    public void saveAudio(File audFile, String time) {

        RequestParams params = new RequestParams();
        try {
            params.put("file", audFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        if (planer_ID != 0) {
            params.put("strModifiedUserId", strUserId);
        } else {
            params.put("strCreateUserId", strUserId);
        }

        params.put("intPlannerId", String.valueOf(planer_ID));
        params.put("strCategoryTypeId", strCategoryTypeId);
        params.put("strCategoryId", strCategoryId);
        params.put("strSubCategoryId", strSubCategoryId);
        params.put("strDuration", time);

        Log.e("saveAudi_file", urlAudio + "..." + params);
//        params.put("intProjectId", mUserDetails.getArryselectedProjectAllItems().get(0).getId());
//        params.put("intOrganisationId", mUserDetails.getArryselectedOrganisationAllItems().get(0).getId());

        showProgressDialog(this, "Uploading...");
        ApiCallWithToken(urlAudio, ApiCall.AUDIO, params);

    }

    public void deleteAudio(AudioDetails arrAudioDetail) {

        RequestParams params = new RequestParams();
//        try {
//            params.put("file", new File(""));
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
        params.put("strAudioId", arrAudioDetail.getIntAudioId());
        params.put("intPlannerId", String.valueOf(planer_ID));
        params.put("strCategoryTypeId", strCategoryTypeId);
        params.put("strCategoryId", strCategoryId);
        params.put("strSubCategoryId", strSubCategoryId);
        params.put("strModifiedUserId", strUserId);

        Log.e("deleteAudi", urldeleteNoteAud + "..." + params);

        showProgressDialog(this, "Removing...");
        ApiCallWithToken(urldeleteNoteAud, ApiCall.DELETEAUDIO, params);
    }

    public void getColleborateUsers() {
        RequestParams params = new RequestParams();
        params.put("strUserId", strUserId);
        params.put("intPlannerId", String.valueOf(planer_ID));
        ApiCallWithToken(urlColUser, ApiCall.COLLABORATEUSERS, params);
    }

    @Override
    public void OnResponce(JSONObject data, ApiCall type) {
        JSONArray array = null;
        try {
            if (type == ApiCall.ADDPLANNER) {

                hideProgressDialog(AddNewNoteActivity.this);
                array = data.getJSONArray("data");
                if (array.length() != 0) {
                    Gson gson = new Gson();

                    for (int i = 0; i < array.length(); i++) {
                        try {
                            JSONObject object = array.getJSONObject(i);
                            Item item = gson.fromJson(object.toString(), Item.class);
                            planer_ID = item.getIntPlannerId();
                            finish();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

            } else if (type == ApiCall.UPDATEPLANNER) {

                array = data.getJSONArray("data");
                if (array.length() != 0) {
                    Gson gson = new Gson();
                    NoteUpdate item = gson.fromJson(data.toString(), NoteUpdate.class);
                }
            } else if (type == ApiCall.IMAGE) {
                ivLoader.setVisibility(View.GONE);
                hideProgressDialog(AddNewNoteActivity.this);
                imgDetailss.clear();
                array = data.getJSONArray("data");
                if (array.length() != 0) {
                    Gson gson = new Gson();
//                    ArrayList<ImgDetails> imgDetails = new ArrayList<>();
                    for (int i = 0; i < array.length(); i++) {
                        try {
                            JSONObject object = array.getJSONObject(i);
                            Item img = gson.fromJson(object.toString(), Item.class);
                            mRealm.beginTransaction();
                            mRealm.copyToRealmOrUpdate(img);
                            imgDetailss.addAll(img.getImgNoteList());
                            mRealm.commitTransaction();

                            planer_ID = img.getIntPlannerId();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }

                setImages(imgDetailss);
                if (fileArrayList.size() != 0) {
                    for (int j = 0; j < fileArrayList.size(); j++) {
                        saveImage(fileArrayList.get(j).getFile(), fileArrayList.get(j).getHeight(), fileArrayList.get(j).getWeight());
                        if (fileArrayList.get(j).getFile().length() != 0) {
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            bitmap = BitmapFactory.decodeFile(fileArrayList.get(j).getFile().getAbsolutePath(), options);
                            SaveImageToFolder(fileArrayList.get(j).getFile());
                        }
                        fileArrayList.remove(j);
                    }
                }

            } else if (type == ApiCall.AUDIO) {

                hideProgressDialog(AddNewNoteActivity.this);
                array = data.getJSONArray("data");
                mRealm.beginTransaction();
                arrAudioDetails.clear();
                audDeletUpdated.clear();
                mRealm.commitTransaction();
                if (array.length() != 0) {
                    showToast("Successfully uploaded");
                    Gson gson = new Gson();

                    for (int i = 0; i < array.length(); i++) {
                        try {
                            JSONObject object = array.getJSONObject(i);
                            Item img = gson.fromJson(object.toString(), Item.class);
                            mRealm.beginTransaction();
                            arrAudioDetails.addAll(img.getFileAudioList());
                            audDeletUpdated.addAll(img.getFileAudioList());
                            mRealm.commitTransaction();
                            planer_ID = img.getIntPlannerId();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    setAudioDatas();
                }
            } else if (type == ApiCall.COLLABORATEUSERS) {
                array = data.getJSONArray("data");
                if (data.length() != 0) {
                    Gson gson = new Gson();
                    final ArrayList<ColleborateDatum> colleborators = new ArrayList<>();
                    for (int i = 0; i < array.length(); i++) {
                        try {
                            JSONObject object = array.getJSONObject(i);
                            ColleborateDatum users = gson.fromJson(object.toString(), ColleborateDatum.class);
                            colleborators.add(users);


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    for (int j = 0; j < colleborators.size(); j++) {
                        collUsers.add(colleborators.get(j).getId());
                    }
                    if (colleborators.size() != 0) {

                        int color = Color.parseColor("#FFFFFF");
                        ivPin.setColorFilter(color);
                        isPinned = false;
//                        mRealm.beginTransaction();
//                        oneItemServer.get(0).setBlnPinNote(false);
//                        mRealm.copyToRealmOrUpdate(oneItemServer.get(0));
//                        mRealm.commitTransaction();

                        arryCollaboratUserData.clear();
                        arryCollaboratUserData.addAll(colleborators);
                        setColleborateUsers(arryCollaboratUserData);
                    }
                }
            } else if (type == ApiCall.DELETEAUDIO) {
                hideProgressDialog(AddNewNoteActivity.this);

                Log.e("deleteAduio_data", data + "");
                array = data.getJSONArray("data");


                mRealm.beginTransaction();
                arrAudioDetails.clear();
                mRealm.commitTransaction();

                if (array.length() != 0) {
                    showToast("Successfully removed");
                    Gson gson = new Gson();

                    for (int i = 0; i < array.length(); i++) {
                        try {
                            JSONObject object = array.getJSONObject(i);
                            Item img = gson.fromJson(object.toString(), Item.class);
//                            arrAudioDetails.addAll(img.getFileAudioList());

                            mRealm.beginTransaction();
                            arrAudioDetails.addAll(img.getFileAudioList());
                            mRealm.commitTransaction();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    if (isAudioExpanded) {
//                        tvMoreAudio.setVisibility(View.GONE);
                    } else {
                        if (arrAudioDetails.size() == 1) tvMoreAudio.setVisibility(View.GONE);
                        else if (arrAudioDetails.size() > 1) {
                            tvMoreAudio.setVisibility(View.VISIBLE);
                            tvMoreAudio.setText("+ (" + (arrAudioDetails.size() - 1) + " )");
                        }
                    }
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        super.OnResponce(data, type);
    }

    @Override
    public void OnError(JSONObject object, ApiCall type) {
        super.OnError(object, type);
        if (type == ApiCall.IMAGE) {
            flImage.setVisibility(View.GONE);
        }
    }

    public void setColleborateUsers(ArrayList<ColleborateDatum> colleborators) {

        if (isPinned) {
            int color = Color.parseColor("#FFFFFF"); //The color u want
            ivPin.setColorFilter(color);
            isPinned = false;
        }

        if (colleborators.size() > 0) {

            llColleborate.setVisibility(View.VISIBLE);
            int totCount = colleborators.size();

            final String lastName, firstName, fullName;
            RelativeLayout linearLayout = (RelativeLayout) View.inflate(AddNewNoteActivity.this, R.layout.list_item_colleborate_list_users, null);

            TextView text = linearLayout.findViewById(R.id.text);
            FrameLayout frMain = linearLayout.findViewById(R.id.frMain);
            LinearLayout llShare = linearLayout.findViewById(R.id.llShare);
            ImageView cvImage1 = linearLayout.findViewById(R.id.cvImage1);

            String name = colleborators.get(0).getItemName();
            final String[] split = name.split(":");
            firstName = split[0];
            if (split.length == 2) {
                lastName = split[1];
                fullName = firstName + " " + lastName;
            } else {
                fullName = firstName;
            }

            if (totCount == 1) {
                text.setText(fullName);
                cvImage1.setVisibility(View.VISIBLE);
                Glide.with(AddNewNoteActivity.this)
                        .load(R.drawable.ic_share)
                        .dontAnimate()
                        .into(cvImage1);
            } else {
                text.setText(fullName + " +others");
                cvImage1.setVisibility(View.VISIBLE);
                Glide.with(AddNewNoteActivity.this)
                        .load(R.drawable.ic_share)
                        .dontAnimate()
                        .into(cvImage1);
            }

            llShare.setOnClickListener(view -> {

                Intent intent = new Intent(AddNewNoteActivity.this, ColleborateActivity.class);
                intent.putExtra("Type", "NoteDetail");
                intent.putParcelableArrayListExtra("Colleborators", colleborators);
                intent.putExtra("strCategoryTypeId", PlannerFragment.strCategoryTypeId);
                intent.putExtra("strCategoryId", PlannerFragment.strCategoryId);
                intent.putExtra("strSubCategoryId", PlannerFragment.strSubCategoryId);
                intent.putExtra("intPlannerId", String.valueOf(planer_ID));
                intent.putExtra("createdBy", strUserId);
                startActivityForResult(intent, 3);
                linearLayout.removeAllViews();
            });

            llColleborate.addView(linearLayout);
        }


    }

    private void SaveImageToFolder(File fileProfile) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(fileProfile.getAbsolutePath(), options);
        File folder = new File(Environment.getExternalStorageDirectory() + File.separator + "WorFlow");
        File newfolder = new File(folder, "WorFlow Images");
        boolean success = true;
        if (!newfolder.exists()) {
            success = newfolder.mkdirs();
        }
        if (success) {
            // Do something on success
        } else {
            // Do something else on failure
        }
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "Planner_" + n + ".jpg";
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        File f = new File(newfolder + File.separator + "IMG_" + timestamp + ".jpg");
        try {

            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint({"ResourceType", "ClickableViewAccessibility"})
    @OnClick({R.id.ivBack, R.id.tvMoreAudio, R.id.ivAudioDeleteOne, R.id.ivAudioDeleteTwo, R.id.ivAudioDeleteThree, R.id.ivAdd, R.id.llPin, R.id.ivArchieve, R.id.llRemainder, R.id.tvTimer, R.id.llCamera, R.id.llGallery, R.id.llColleborateIcon, R.id.llRecord, R.id.llCheckbox, R.id.llGmail, R.id.ivUndo, R.id.ivRedo, R.id.ivImageOne, R.id.ivImageTwo, R.id.ivImageThree})
    public void onViewClicked(View view) {

        switch (view.getId()) {

            case R.id.ivBack:
                onBackPressed();
                break;


            case R.id.tvMoreAudio:

                if (kCount == 0) {
                    isAudioExpanded = true;
                    llAudioOne.setVisibility(View.VISIBLE);
                    if (arrAudioDetails.size() == 3) {
                        llAudioTwo.setVisibility(View.VISIBLE);
                        llAudioThree.setVisibility(View.VISIBLE);
                    } else if (arrAudioDetails.size() == 2) {
                        llAudioTwo.setVisibility(View.VISIBLE);
                    }

                    tvMoreAudio.setText("+ less");
                    kCount = 1;
                } else {
                    isAudioExpanded = false;
                    llAudioOne.setVisibility(View.VISIBLE);
                    if (arrAudioDetails.size() == 3) {
                        llAudioTwo.setVisibility(View.GONE);
                        llAudioThree.setVisibility(View.GONE);
                    } else if (arrAudioDetails.size() == 2) {
                        llAudioTwo.setVisibility(View.GONE);
                    }
                    tvMoreAudio.setText("+ (" + (arrAudioDetails.size() - 1) + " )");
                    kCount = 0;
                }
                break;
            case R.id.ivAudioDeleteOne:
                if (isNetworkAvailable()) {
                    llAudioOne.setVisibility(View.GONE);
                    deleteAudio(audDeletUpdated.get(0));
                } else {
                    String status = Application.getConnectivityStatusString(this);
                    setSnackbarMessage(status, false);
                }
                break;

            case R.id.ivAudioDeleteTwo:
                if (isNetworkAvailable()) {
                    llAudioTwo.setVisibility(View.GONE);
                    deleteAudio(audDeletUpdated.get(1));
                } else {
                    String status = Application.getConnectivityStatusString(this);
                    setSnackbarMessage(status, false);
                }
                break;
            case R.id.ivAudioDeleteThree:

                if (isNetworkAvailable()) {
                    llAudioThree.setVisibility(View.GONE);
                    deleteAudio(audDeletUpdated.get(2));
                } else {
                    String status = Application.getConnectivityStatusString(this);
                    setSnackbarMessage(status, false);
                }

                break;

            case R.id.ivAdd:

//                addEnterKeyPressed(finalI1,"ZERO);
                break;

            case R.id.llPin:

                if (arryCollaboratUserData.size() > 0) {
                    showToast("Shared Note cannot be pinned");
                } else {
                    if (isPinned) {
                        int color = Color.parseColor("#FFFFFF"); //The color u want
                        ivPin.setColorFilter(color);
                        isPinned = false;
                    } else {
                        int color = Color.parseColor("#F1C40F"); //The color u want
                        ivPin.setColorFilter(color);
                        isPinned = true;
                    }

                }

                break;

            case R.id.ivArchieve:
                break;

            case R.id.llRemainder:
            case R.id.tvTimer:

                String isThere = tvTimer.getText().toString();
                if (isThere.length() != 0) {

                    String oldType = objRemainder.getStrType();
                    String oldDate = objRemainder.getStrRemainderDate();
                    String oldTime = objRemainder.getStrRemainderTime();

                    ReminderDialog.newIntance(oldType, oldDate, oldTime, new ReminderDialog.Callback() {
                        @Override
                        public void edit(ReminderDialog dialog, String newDate, String time, String type) {
                            if (newDate.length() != 0) {
                                objRemainder.setBlnFlage(true);
                                objRemainder.setStrRemainderDate(newDate);
                                objRemainder.setStrRemainderTime(time);
                                objRemainder.setStrType(type);

                                tvTimer.setVisibility(View.VISIBLE);
                                Date date1 = null;
                                String date = Functions.formatDate("yyyy-MM-dd", "MMMM dd", newDate);
                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

                                try {
                                    date1 = format.parse(newDate);
                                    System.out.println(date1);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                Boolean check_today = DateUtils.isToday(date1);
                                Boolean check_yesterday = DateUtils.isYesterday(date1);
                                Boolean check_tomorrow = DateUtils.isTomorrow(date1);

                                if (check_today) {
                                    tvTimer.setText("Today " + time);
                                } else if (check_yesterday) {
                                    tvTimer.setText("Yesterday " + time);
                                } else if (check_tomorrow) {
                                    tvTimer.setText("Tomorrow " + time);
                                } else {
                                    tvTimer.setText(date + ", " + time);
                                }
                            } else {
                                ObjRemainder obj = new ObjRemainder();
                                objRemainder = obj;
                                tvTimer.setVisibility(View.GONE);
                                tvTimer.setText("");

                            }
                        }
                    }).show(getSupportFragmentManager(), "ReminderDialog");
                } else {
                    ReminderDialog.newIntance("null", "null", "null", (dialog, newDate, time, type) -> {

                        objRemainder.setBlnFlage(true);
                        objRemainder.setStrRemainderDate(newDate);
                        objRemainder.setStrRemainderTime(time);
                        objRemainder.setStrType(type);

                        tvTimer.setVisibility(View.VISIBLE);
                        Date date1 = null;
                        String date = Functions.formatDate("yyyy-MM-dd", "MMMM dd", newDate);
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

                        try {
                            date1 = format.parse(newDate);
                            System.out.println(date1);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        Boolean check_today = DateUtils.isToday(date1);
                        Boolean check_yesterday = DateUtils.isYesterday(date1);
                        Boolean check_tomorrow = DateUtils.isTomorrow(date1);

                        if (check_today) {
                            tvTimer.setText("Today " + time);
                        } else if (check_yesterday) {
                            tvTimer.setText("Yesterday " + time);
                        } else if (check_tomorrow) {
                            tvTimer.setText("Tomorrow " + time);
                        } else {
                            tvTimer.setText(date + ", " + time);
                        }
                    }).show(getSupportFragmentManager(), "ReminderDialog");
                }

                break;

            case R.id.llCamera:

                isCam = true;//true= camera,false=gallery
//                if (fileProfile.length() != 0) {

                if (imgDetailss.size() >= 3) {
                    final BottomSheetDialog dialog = new BottomSheetDialog(AddNewNoteActivity.this);
                    View sheetView = AddNewNoteActivity.this.getLayoutInflater().inflate(R.layout.new_layout, null);
                    dialog.setContentView(sheetView);
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                    TextView tvTitle = sheetView.findViewById(R.id.tvTitle);
                    tvTitle.setText("Cant't upload more than 3 images");
                    Button btnProceed = sheetView.findViewById(R.id.btnProceed);
                    Button btnCancel = sheetView.findViewById(R.id.btnCancel);
                    btnProceed.setText("Ok");
                    btnProceed.setOnClickListener(view2 -> {

                        ArrayList<String> perms = new ArrayList<String>();
                        perms.add(Manifest.permission.CAMERA);
                        perms.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            perms.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                        }
                        final String[] permissions = perms.toArray(new String[perms.size()]);
                        if (Nammu.hasPermission(AddNewNoteActivity.this, permissions)) {
                            if (mayRequest()) {
                                Intent intent5 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent5, IMAGE_PICKER_REQUEST_CODE);
                            }
                        } else {
                            if (Nammu.shouldShowRequestPermissionRationale(AddNewNoteActivity.this, permissions[0])
                                    || Nammu.shouldShowRequestPermissionRationale(AddNewNoteActivity.this, permissions[1])
                                    || Nammu.shouldShowRequestPermissionRationale(AddNewNoteActivity.this, permissions[2])
                            ) {
                                ActivityCompat.requestPermissions(AddNewNoteActivity.this, new String[]{Manifest.permission.CAMERA}, IMAGE_PICKER_REQUEST_CODE);
                            } else {
                                ActivityCompat.requestPermissions(AddNewNoteActivity.this, new String[]{Manifest.permission.CAMERA}, IMAGE_PICKER_REQUEST_CODE);
                            }
                        }
                        dialog.dismiss();
                    });
                    btnCancel.setOnClickListener(view22 -> dialog.dismiss());

                } else {

                    ArrayList<String> perms = new ArrayList<String>();
                    perms.add(Manifest.permission.CAMERA);
                    perms.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        perms.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                    }

                    final String[] permissions = perms.toArray(new String[perms.size()]);
                    if (Nammu.hasPermission(AddNewNoteActivity.this, permissions)) {
                        Intent intent5 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        startActivityForResult(intent5, IMAGE_PICKER_REQUEST_CODE);
                    } else {
                        if (Nammu.shouldShowRequestPermissionRationale(AddNewNoteActivity.this, permissions[0])
                                || Nammu.shouldShowRequestPermissionRationale(AddNewNoteActivity.this, permissions[1])
                                || Nammu.shouldShowRequestPermissionRationale(AddNewNoteActivity.this, permissions[2])
                        ) {
                            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, IMAGE_PICKER_REQUEST_CODE);

                        } else {
                            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, IMAGE_PICKER_REQUEST_CODE);
                        }
                    }
                }
                break;
            case R.id.llGallery:

                isCam = false;//true= camera,false=gallery
                if (imgDetailss.size() >= 3) {
                    final BottomSheetDialog dialog = new BottomSheetDialog(AddNewNoteActivity.this);
                    View sheetView = AddNewNoteActivity.this.getLayoutInflater().inflate(R.layout.new_layout, null);
                    dialog.setContentView(sheetView);
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();
                    TextView tvTitle = sheetView.findViewById(R.id.tvTitle);
                    tvTitle.setText("Cant't upload more than 3 images");
                    Button btnProceed = sheetView.findViewById(R.id.btnProceed);
                    Button btnCancel = sheetView.findViewById(R.id.btnCancel);
                    btnProceed.setText("Ok");
                    btnProceed.setOnClickListener(view2 -> {
                        ArrayList<String> perms = new ArrayList<String>();
                        perms.add(Manifest.permission.CAMERA);
                        perms.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            perms.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                        }
                        final String[] permissions = perms.toArray(new String[perms.size()]);
                        if (Nammu.hasPermission(AddNewNoteActivity.this, permissions)) {
                            if (mayRequest()) {
//                                Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                                i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//                                startActivityForResult(i, IMAGE_PICKER_REQUEST_CODE);
                                Intent intent = new Intent();
                                intent.setType("image/*");
                                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                                intent.setAction(Intent.ACTION_GET_CONTENT);
                                startActivityForResult(Intent.createChooser(intent, "Select Picture"), IMAGE_PICKER_REQUEST_CODE);

                            }
                        } else {
                            if (Nammu.shouldShowRequestPermissionRationale(AddNewNoteActivity.this, permissions[0])
                                    || Nammu.shouldShowRequestPermissionRationale(AddNewNoteActivity.this, permissions[1])
                                    || Nammu.shouldShowRequestPermissionRationale(AddNewNoteActivity.this, permissions[2])
                            ) {
                                ActivityCompat.requestPermissions(AddNewNoteActivity.this,
                                        new String[]{Manifest.permission.CAMERA},
                                        IMAGE_PICKER_REQUEST_CODE);
                            } else {
                                ActivityCompat.requestPermissions(AddNewNoteActivity.this, new String[]{Manifest.permission.CAMERA}, IMAGE_PICKER_REQUEST_CODE);
                            }
                        }
                        dialog.dismiss();
                    });
                    btnCancel.setOnClickListener(view22 -> dialog.dismiss());

                } else {

                    ArrayList<String> perms = new ArrayList<String>();
                    perms.add(Manifest.permission.CAMERA);
                    perms.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        perms.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                    }
                    final String[] permissions = perms.toArray(new String[perms.size()]);
                    if (Nammu.hasPermission(AddNewNoteActivity.this, permissions)) {
//                        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                        i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
//                        startActivityForResult(i, IMAGE_PICKER_REQUEST_CODE);

                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.putExtra("3", true);
                        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), IMAGE_PICKER_REQUEST_CODE);

//                        ImagePicker.create(AddNewNoteActivity.this)
//                                .showCamera(false)
//                                .limit(3)
//                                .imageTitle("Upload")
//                                .folderTitle("WOr Flow")
////                                .theme(R.style.ImagePickerTheme)
//                                .start(RC_CODE_PICKER);

                    } else {

                        if (Nammu.shouldShowRequestPermissionRationale(AddNewNoteActivity.this, permissions[0])
                                || Nammu.shouldShowRequestPermissionRationale(AddNewNoteActivity.this, permissions[1])
                                || Nammu.shouldShowRequestPermissionRationale(AddNewNoteActivity.this, permissions[2])
                        ) {
                            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, IMAGE_PICKER_REQUEST_CODE);

                        } else {
                            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, IMAGE_PICKER_REQUEST_CODE);
                        }
                    }
                }
                break;
            case R.id.llColleborateIcon:

                Intent intent = new Intent(AddNewNoteActivity.this, ColleborateActivity.class);
                intent.putExtra("intPlannerId", String.valueOf(planer_ID));
                intent.putParcelableArrayListExtra("Colleborators", arryCollaboratUserData);
                intent.putExtra("strCategoryTypeId", PlannerFragment.strCategoryTypeId);
                intent.putExtra("strCategoryId", PlannerFragment.strCategoryId);
                intent.putExtra("strSubCategoryId", PlannerFragment.strSubCategoryId);
                intent.putExtra("createdBy", strUserId);
                if (arryCollaboratUserData.size() != 0) {
                    intent.putExtra("Type", "NoteDetail");
                } else {
                    intent.putExtra("Type", "AddNewNote");
                }
                startActivityForResult(intent, 3);

                break;

            case R.id.llRecord:

                if (arrAudioDetails.size() >= 3) {
                    final BottomSheetDialog dialog = new BottomSheetDialog(AddNewNoteActivity.this);
                    View sheetView = AddNewNoteActivity.this.getLayoutInflater().inflate(R.layout.new_layout, null);
                    dialog.setContentView(sheetView);
                    dialog.setCanceledOnTouchOutside(true);
                    dialog.show();

                    TextView tvTitle = sheetView.findViewById(R.id.tvTitle);
                    tvTitle.setText("Cant't upload more than 3 audios");
                    Button btnProceed = sheetView.findViewById(R.id.btnProceed);
                    Button btnCancel = sheetView.findViewById(R.id.btnCancel);
                    btnProceed.setVisibility(View.GONE);
                    btnCancel.setText("Ok");
                    btnProceed.setOnClickListener(view23 -> {
                        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
                        int res = getApplicationContext().checkCallingOrSelfPermission(permission);
                        if (res == PackageManager.PERMISSION_GRANTED) {
                            requestAudioPermissions();
                        } else {
                            Snackbar snackbar = Snackbar.make(llBottom, "Allow permission for recording", Snackbar.LENGTH_LONG);
                            snackbar.show();
                            PermissionsManager.get().intentToAppSettings(AddNewNoteActivity.this);
                        }
                        dialog.dismiss();
                    });
                    btnCancel.setOnClickListener(view24 -> dialog.dismiss());

                } else {
                    String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
                    int res = getApplicationContext().checkCallingOrSelfPermission(permission);
                    if (res == PackageManager.PERMISSION_GRANTED) {
                        requestAudioPermissions();
                    } else {
                        Snackbar snackbar = Snackbar.make(llBottom, "Allow permission for recording", Snackbar.LENGTH_LONG);
                        snackbar.show();
                        PermissionsManager.get().intentToAppSettings(AddNewNoteActivity.this);
                    }
                }
                break;

            case R.id.llCheckbox:

                if (isType.equals("Checkboxes")) {
                    undoCheckboxesArraylist.clear();
                    redoCheckboxesArraylist.clear();

                    if (!alleditTextCheckbox.isEmpty()) {
                        UndoalleditText = alleditTextCheckbox;
                        ArrayList<UndoCheckboxModel> NewundoCheckboxesFinal = new ArrayList<>();
                        ArrayList<UndoCheckboxModel> undoCheckboxesFinal = new ArrayList<>();
                        for (int i = 0; i < UndoalleditText.size(); i++) {
                            UndoCheckboxModel undoCheckboxes1 = new UndoCheckboxModel();
                            undoCheckboxes1.setId(String.valueOf(i));
                            undoCheckboxes1.setStrNoteListValue(UndoalleditText.get(i).getText().toString());
                            undoCheckboxes1.setStrNoteListFlage(false);
                            undoCheckboxesFinal.add(undoCheckboxes1);
                            NewundoCheckboxesFinal.add(i, undoCheckboxesFinal.get(i));
                        }
                        UndoCheckboxes undos1 = new UndoCheckboxes();
                        undos1.setId(String.valueOf(0));
                        undos1.setTitle(tvTitle.getText().toString());
                        undos1.setUndoCheckboxModel(NewundoCheckboxesFinal);
                        undoCheckboxesArraylist.add(0, undos1);
                    }
                } else {
                    undoTextsArraylist.clear();
                    redoTextsArraylist.clear();
                    ArrayList<UndoTextsModel> NewundoTextsFinal = new ArrayList<>();
                    ArrayList<UndoTextsModel> undoTextsFinal = new ArrayList<>();
                    if (!alleditTextCheckbox.isEmpty()) {
                        UndoalleditText = alleditTextCheckbox;//issue
                        for (int i = 0; i < UndoalleditText.size(); i++) {
                            UndoTextsModel undoCheckboxes1 = new UndoTextsModel();
                            undoCheckboxes1.setId(String.valueOf(i));
                            undoCheckboxes1.setStrNoteListValue(UndoalleditText.get(i).getText().toString());
                            undoTextsFinal.add(undoCheckboxes1);
                            NewundoTextsFinal.add(i, undoTextsFinal.get(i));
                        }
                        UndoTexts undos1 = new UndoTexts();
                        undos1.setId(String.valueOf(0));
                        undos1.setTitle(tvTitle.getText().toString());
                        undos1.setUndoTextsModel(NewundoTextsFinal);
                        undoTextsArraylist.add(0, undos1);
                    }
                }

                ivUndo.setEnabled(false);
                ivUndo.setColorFilter(colorGrey);
                ivRedo.setColorFilter(colorGrey);
                ivRedo.setEnabled(false);
                convertDatas();

                break;

            case R.id.llGmail:

                String list_text = "";
                for (int i = 0; i < rlCheckbox.getChildCount(); i++) {
                    View child = rlCheckbox.getChildAt(i);

                    if (child instanceof CustomEditText) {
                        String val = ((CustomEditText) child).getText().toString();
                        if (i == 0) {
                            list_text = list_text + val;
                        } else {
                            list_text = list_text + "\n" + val;
                        }
                    } else if (child instanceof RelativeLayout) {
                        String val = ((CustomEditText) ((RelativeLayout) child).getChildAt(1)).getText().toString();
                        if (i == 0) {
                            list_text = list_text + val;
                        } else {
                            list_text = list_text + "\n" + val;
                        }
                    }
                }

                Intent i = new Intent(Intent.ACTION_SENDTO);
                i.setType("message/rfc822");
                i.setData(Uri.parse("mailto:"));
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{""});//recipient@example.com
                i.putExtra(Intent.EXTRA_SUBJECT, "Planner: " + tvTitle.getText().toString());
                i.putExtra(Intent.EXTRA_TEXT, list_text);

                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(AddNewNoteActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.ivUndo:

                if (isType.equals("Checkboxes")) { //convert textformat to checkboxes

                    if (undoTextsArraylist.size() == 0) {
                        ivUndo.setColorFilter(colorGrey);
                        ivUndo.setEnabled(false);
                    } else {
                        ivUndo.setColorFilter(colorBlack);
                        ivUndo.setEnabled(true);
                    }

                    isActiveText = "Yes";
                    int k;
                    if (undoTextsArraylist.size() - 2 == -1) {
                        k = 0;
                    } else {
                        k = undoTextsArraylist.size() - 2;
                    }

                    isChanged = "No";
                    tvTitle.setText(undoTextsArraylist.get(k).getTitle());

                    ArrayList<UndoTextsModel> undoTexts = new ArrayList<>(undoTextsArraylist.get(k).getUndoTextsModel());
                    textFieldValue(undoTexts);

                    redoTextsArraylist.add(undoTextsArraylist.get(undoTextsArraylist.size() - 1));
                    undoTextsArraylist.remove(undoTextsArraylist.size() - 1);
                    if (k == 0) {
                        ivUndo.setColorFilter(colorGrey);
                        ivUndo.setEnabled(false);
                    }
                    if (redoTextsArraylist.size() == 0) {
                        ivRedo.setColorFilter(colorGrey);
                        ivRedo.setEnabled(false);
                    } else {
                        ivRedo.setColorFilter(colorBlack);
                        ivRedo.setEnabled(true);
                    }

                } else {

                    if (undoCheckboxesArraylist.size() == 0) {
                        ivUndo.setColorFilter(colorGrey);
                        ivUndo.setEnabled(false);
                    } else {
                        ivUndo.setColorFilter(colorBlack);
                        ivUndo.setEnabled(true);
                    }

                    isActiveCheckbox = "Yes";
                    int k;
                    if (undoCheckboxesArraylist.size() - 2 == -1) {
                        k = 0;
                    } else {
                        k = undoCheckboxesArraylist.size() - 2;
                    }

                    isChangedCheckbox = "No";
                    tvTitle.setText(undoCheckboxesArraylist.get(k).getTitle());
                    if (k == 0) {
                        ivUndo.setColorFilter(colorGrey);
                        ivUndo.setEnabled(false);
                    } else {
                        ivUndo.setColorFilter(colorBlack);
                        ivUndo.setEnabled(true);
                    }

                    ArrayList<UndoCheckboxModel> undoCheckboxes = new ArrayList<>(undoCheckboxesArraylist.get(k).getUndoCheckboxModel());
                    checkBoxeValue(undoCheckboxes);

                    redoCheckboxesArraylist.add(undoCheckboxesArraylist.get(undoCheckboxesArraylist.size() - 1));
                    undoCheckboxesArraylist.remove(undoCheckboxesArraylist.size() - 1);
                    if (redoCheckboxesArraylist.size() == 0) {
                        ivRedo.setColorFilter(colorGrey);
                        ivRedo.setEnabled(false);
                    } else {
                        ivRedo.setColorFilter(colorBlack);
                        ivRedo.setEnabled(true);
                    }
                }

                break;

            case R.id.ivRedo:

                if (isType.equals("Checkboxes")) { //convert textformat to checkboxes

                    if (redoTextsArraylist.size() == 0) {
                        ivRedo.setColorFilter(colorGrey);
                        ivRedo.setEnabled(false);
                    } else {
                        ivRedo.setColorFilter(colorBlack);
                        ivRedo.setEnabled(true);
                    }

                    int k1 = redoTextsArraylist.size() - 1;
                    tvTitle.setText(redoTextsArraylist.get(k1).getTitle());
                    ArrayList<UndoTextsModel> undoTextsModels = new ArrayList<>(redoTextsArraylist.get(k1).getUndoTextsModel());
                    textFieldValue(undoTextsModels);

                    undoTextsArraylist.add(redoTextsArraylist.get(redoTextsArraylist.size() - 1));
                    redoTextsArraylist.remove(redoTextsArraylist.size() - 1);

                    if (k1 == 0) {
                        ivRedo.setColorFilter(colorGrey);
                        ivRedo.setEnabled(false);
                    }

                    if (undoTextsArraylist.size() != 0) {
                        ivUndo.setColorFilter(colorBlack);
                        ivUndo.setEnabled(true);
                    }

                } else {

                    if (redoCheckboxesArraylist.size() == 0) {
                        ivRedo.setColorFilter(colorGrey);
                        ivRedo.setEnabled(false);
                    } else {
                        ivRedo.setColorFilter(colorBlack);
                        ivRedo.setEnabled(true);
                    }

                    int k1 = redoCheckboxesArraylist.size() - 1;
                    tvTitle.setText(redoCheckboxesArraylist.get(k1).getTitle());
                    ArrayList<UndoCheckboxModel> undoCheckboxes = new ArrayList<>(redoCheckboxesArraylist.get(k1).getUndoCheckboxModel());
                    checkBoxeValue(undoCheckboxes);

                    undoCheckboxesArraylist.add(redoCheckboxesArraylist.get(redoCheckboxesArraylist.size() - 1));
                    redoCheckboxesArraylist.remove(redoCheckboxesArraylist.size() - 1);

                    if (k1 == 0) {
                        ivRedo.setColorFilter(colorGrey);
                        ivRedo.setEnabled(false);
                    }

                    if (undoCheckboxesArraylist.size() != 0) {
                        ivUndo.setColorFilter(colorBlack);
                        ivUndo.setEnabled(true);
                    }

                }

                break;
            case R.id.ivImageOne:
                ArrayList<ImgDetails> listOne = new ArrayList();
                listOne.addAll(imgDetailss);
                Intent img = new Intent(AddNewNoteActivity.this, ImageShowActivity.class);
                img.putExtra("type", "PLANNER");
                img.putExtra("img_pos", 0);

                img.putExtra("strImageId", imgDetailss.get(0).getIntImageId());
                img.putExtra("intPlannerId", planer_ID);
                img.putExtra("strCategoryTypeId", strCategoryTypeId);
                img.putExtra("strCategoryId", strCategoryId);
                img.putExtra("strSubCategoryId", strSubCategoryId);


                img.putParcelableArrayListExtra("img_list", listOne);
                startActivityForResult(img, 1001);
                break;
            case R.id.ivImageTwo:
                Log.e("ivImageTwo", imgDetailss.size() + "...");
                ArrayList<ImgDetails> list = new ArrayList();
                list.addAll(imgDetailss);
                Intent img2 = new Intent(AddNewNoteActivity.this, ImageShowActivity.class);
                img2.putExtra("type", "PLANNER");
                img2.putExtra("img_pos", 1);

                img2.putExtra("strImageId", imgDetailss.get(1).getIntImageId());
                img2.putExtra("intPlannerId", planer_ID);
                img2.putExtra("strCategoryTypeId", strCategoryTypeId);
                img2.putExtra("strCategoryId", strCategoryId);
                img2.putExtra("strSubCategoryId", strSubCategoryId);

                img2.putParcelableArrayListExtra("img_list", list);
                startActivityForResult(img2, 1001);
                break;
            case R.id.ivImageThree:
                ArrayList<ImgDetails> list3 = new ArrayList();
                list3.addAll(imgDetailss);
                Intent img3 = new Intent(AddNewNoteActivity.this, ImageShowActivity.class);
                img3.putExtra("type", "PLANNER");
                img3.putExtra("img_pos", 2);

                img3.putExtra("strImageId", imgDetailss.get(2).getIntImageId());
                img3.putExtra("intPlannerId", planer_ID);
                img3.putExtra("strCategoryTypeId", strCategoryTypeId);
                img3.putExtra("strCategoryId", strCategoryId);
                img3.putExtra("strSubCategoryId", strSubCategoryId);


                img3.putParcelableArrayListExtra("img_list", list3);
                startActivityForResult(img3, 1001);
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerInternetCheckReceiver();
    }

    private void registerInternetCheckReceiver() {
        IntentFilter internetFilter = new IntentFilter();
        internetFilter.addAction("android.net.wifi.STATE_CHANGE");
        internetFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        registerReceiver(broadcastReceiver, internetFilter);
    }

    private void setSnackbarMessage(String status, boolean showBar) {
        String internetStatus = "";
        Snackbar snackbar;
        if (status.equalsIgnoreCase("Wifi enabled") || status.equalsIgnoreCase("Mobile data enabled")) {
            internetStatus = "Connecting..";
        } else {
            internetStatus = "No Internet Connection";
        }
        if (internetStatus.equalsIgnoreCase("No Internet Connection")) {
            if (internetConnected) {
                snackbar = Snackbar.make(findViewById(R.id.llBottom), internetStatus, Snackbar.LENGTH_INDEFINITE);

                snackbar.setActionTextColor(Color.WHITE);
                View sbView = snackbar.getView();
                TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);
                snackbar.show();
                internetConnected = false;
            }
        } else {
            if (!internetConnected) {
                internetConnected = true;
                snackbar = Snackbar.make(findViewById(R.id.llBottom), internetStatus, Snackbar.LENGTH_LONG);
                View sbView = snackbar.getView();
                TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.WHITE);

                snackbar.setActionTextColor(Color.WHITE);
                snackbar.show();
            }
        }
    }
}
