package com.example.user.communicator.Model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Filter extends RealmObject {

    ////nameIDSelected , statusSelected ,prioritySelected , sortBySelected

    @PrimaryKey
    String id;
    String nameIDSelected, nameSelected, statusSelected, prioritySelected, sortBySelected;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNameSelected() {
        return nameSelected;
    }

    public void setNameSelected(String nameSelected) {
        this.nameSelected = nameSelected;
    }

    public String getNameIDSelected() {
        return nameIDSelected;
    }

    public void setNameIDSelected(String nameIDSelected) {
        this.nameIDSelected = nameIDSelected;
    }

    public String getStatusSelected() {
        return statusSelected;
    }

    public void setStatusSelected(String statusSelected) {
        this.statusSelected = statusSelected;
    }

    public String getPrioritySelected() {
        return prioritySelected;
    }

    public void setPrioritySelected(String prioritySelected) {
        this.prioritySelected = prioritySelected;
    }

    public String getSortBySelected() {
        return sortBySelected;
    }

    public void setSortBySelected(String sortBySelected) {
        this.sortBySelected = sortBySelected;
    }
}
