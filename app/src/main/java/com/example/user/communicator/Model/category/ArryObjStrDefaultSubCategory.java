
package com.example.user.communicator.Model.category;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ArryObjStrDefaultSubCategory extends RealmObject implements Parcelable {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("strDefaultSubName")
    @Expose
    private String strDefaultSubName;

    public String getStrDefaultSubName() {
        return strDefaultSubName;
    }

    public void setStrDefaultSubName(String strDefaultSubName) {
        this.strDefaultSubName = strDefaultSubName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public static final Parcelable.Creator<ArryObjStrDefaultSubCategory> CREATOR = new Parcelable.Creator<ArryObjStrDefaultSubCategory>() {
        @Override
        public ArryObjStrDefaultSubCategory createFromParcel(Parcel in) {
            return new ArryObjStrDefaultSubCategory(in);
        }
        @Override
        public ArryObjStrDefaultSubCategory[] newArray(int size) {
            return new ArryObjStrDefaultSubCategory[size];
        }
    };

    protected ArryObjStrDefaultSubCategory(Parcel in) {
        id = in.readString();
        strDefaultSubName = in.readString();
    }

    public ArryObjStrDefaultSubCategory() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {

        parcel.writeString(id);
        parcel.writeString(strDefaultSubName);
    }


}
