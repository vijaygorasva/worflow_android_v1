
package com.example.user.communicator.Model.NoteUpdate;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import com.example.user.communicator.Model.Notes.AudioDetails;
import com.example.user.communicator.Model.Notes.ImgDetails;
import com.example.user.communicator.Model.Notes.TodoListItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum implements Parcelable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("intPlannerId")
    @Expose
    private Integer intPlannerId;
    @SerializedName("strStatus")
    @Expose
    private String strStatus;
    @SerializedName("fileAudio")
    @Expose
    private AudioDetails fileAudio;
    @SerializedName("imgNote")
    @Expose
    private ImgDetails imgNote;
    @SerializedName("arryObjstrTodoList")
    @Expose
    private List<TodoListItem> arryObjstrTodoList = null;
    @SerializedName("blnPinNote")
    @Expose
    private String blnPinNote;
    @SerializedName("datCreateDateAndTime")
    @Expose
    private String datCreateDateAndTime;
    @SerializedName("datModifiedDateAndTime")
    @Expose
    private String datModifiedDateAndTime;
    @SerializedName("intCreateUserId")
    @Expose
    private String intCreateUserId;
    @SerializedName("intModifiedUserId")
    @Expose
    private String intModifiedUserId;
    @SerializedName("intOrganisationId")
    @Expose
    private String intOrganisationId;
    @SerializedName("intPlannerMainId")
    @Expose
    private String intPlannerMainId;
    @SerializedName("intProjectId")
    @Expose
    private String intProjectId;
    @SerializedName("objRemainder")
    @Expose
    private ObjRemainder objRemainder;
    @SerializedName("strColor")
    @Expose
    private String strColor;
    @SerializedName("strTitle")
    @Expose
    private String strTitle;
    @SerializedName("strValue")
    @Expose
    private String strValue;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getIntPlannerId() {
        return intPlannerId;
    }

    public void setIntPlannerId(Integer intPlannerId) {
        this.intPlannerId = intPlannerId;
    }

    public String getStrStatus() {
        return strStatus;
    }

    public void setStrStatus(String strStatus) {
        this.strStatus = strStatus;
    }

    public AudioDetails getFileAudio() {
        return fileAudio;
    }

    public void setFileAudio(AudioDetails fileAudio) {
        this.fileAudio = fileAudio;
    }

    public ImgDetails getImgNote() {
        return imgNote;
    }

    public void setImgNote(ImgDetails imgNote) {
        this.imgNote = imgNote;
    }

    public List<TodoListItem> getArryObjstrTodoList() {
        return arryObjstrTodoList;
    }

    public void setArryObjstrTodoList(List<TodoListItem> arryObjstrTodoList) {
        this.arryObjstrTodoList = arryObjstrTodoList;
    }

    public String getBlnPinNote() {
        return blnPinNote;
    }

    public void setBlnPinNote(String blnPinNote) {
        this.blnPinNote = blnPinNote;
    }

    public String getDatCreateDateAndTime() {
        return datCreateDateAndTime;
    }

    public void setDatCreateDateAndTime(String datCreateDateAndTime) {
        this.datCreateDateAndTime = datCreateDateAndTime;
    }

    public String getDatModifiedDateAndTime() {
        return datModifiedDateAndTime;
    }

    public void setDatModifiedDateAndTime(String datModifiedDateAndTime) {
        this.datModifiedDateAndTime = datModifiedDateAndTime;
    }

    public String getIntCreateUserId() {
        return intCreateUserId;
    }

    public void setIntCreateUserId(String intCreateUserId) {
        this.intCreateUserId = intCreateUserId;
    }

    public String getIntModifiedUserId() {
        return intModifiedUserId;
    }

    public void setIntModifiedUserId(String intModifiedUserId) {
        this.intModifiedUserId = intModifiedUserId;
    }

    public String getIntOrganisationId() {
        return intOrganisationId;
    }

    public void setIntOrganisationId(String intOrganisationId) {
        this.intOrganisationId = intOrganisationId;
    }

    public String getIntPlannerMainId() {
        return intPlannerMainId;
    }

    public void setIntPlannerMainId(String intPlannerMainId) {
        this.intPlannerMainId = intPlannerMainId;
    }

    public String getIntProjectId() {
        return intProjectId;
    }

    public void setIntProjectId(String intProjectId) {
        this.intProjectId = intProjectId;
    }

    public ObjRemainder getObjRemainder() {
        return objRemainder;
    }

    public void setObjRemainder(ObjRemainder objRemainder) {
        this.objRemainder = objRemainder;
    }

    public String getStrColor() {
        return strColor;
    }

    public void setStrColor(String strColor) {
        this.strColor = strColor;
    }

    public String getStrTitle() {
        return strTitle;
    }

    public void setStrTitle(String strTitle) {
        this.strTitle = strTitle;
    }

    public String getStrValue() {
        return strValue;
    }

    public void setStrValue(String strValue) {
        this.strValue = strValue;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Datum> CREATOR = new Creator<Datum>() {
        @Override
        public Datum createFromParcel(Parcel in) {
            return new Datum(in);
        }

        @Override
        public Datum[] newArray(int size) {
            return new Datum[size];
        }
    };

    protected Datum(Parcel in) {
        id = in.readString();
        strTitle = in.readString();
        strValue = in.readString();
        strColor = in.readString();
        intCreateUserId = in.readString();
        datCreateDateAndTime = in.readString();
        intPlannerId = in.readInt();
        strStatus = in.readString();
        intPlannerMainId = in.readString();
        datModifiedDateAndTime=in.readString();

    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(id);
        parcel.writeString(strTitle);
        parcel.writeString(strValue);
        parcel.writeString(strColor);
        parcel.writeString(intCreateUserId);
        parcel.writeString(datCreateDateAndTime);
        parcel.writeInt(intPlannerId);
        parcel.writeString(strStatus);
        parcel.writeString(intPlannerMainId);
        parcel.writeString(datModifiedDateAndTime);

        for (TodoListItem array : arryObjstrTodoList) {
            parcel.writeArray(new TodoListItem[]{array});
        }

    }
}
