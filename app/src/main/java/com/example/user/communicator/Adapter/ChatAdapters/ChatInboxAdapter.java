package com.example.user.communicator.Adapter.ChatAdapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.example.user.communicator.CustomViews.CustomTextView;
import com.example.user.communicator.Model.ChatModels.ChatItem;
import com.example.user.communicator.R;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatInboxAdapter extends RecyclerView.Adapter<ChatInboxAdapter.ViewHolder> {

    private final String BASE_URL = "http://52.66.249.75:9999/";
    private OnItemClickList mItemClickList;
    private ArrayList<ChatItem> mItems;
    private Context mContext;
    public int pos;

    public ChatInboxAdapter(Context context, ArrayList<ChatItem> Items) {
        mContext = context;
        mItems = Items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View inflatedView = LayoutInflater.from(mContext).inflate(R.layout.layout_chat_user_item, parent, false);
        return new ViewHolder(inflatedView);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        ChatItem data = mItems.get(position);

        if (!TextUtils.isEmpty(data.getName())) {
            holder.mName.setText(data.getName());
        }
        if (data.getLastMessageData() != null) {
            if (data.getLastMessageData().getSendTime() != 0) {
                holder.mTime.setText(LongToTimeWithAmPm(data.getLastMessageData().getSendTime()));
            }
            if (!TextUtils.isEmpty(data.getLastMessageData().getMessage())) {
                holder.mMsg.setText(data.getLastMessageData().getMessage());
            } else {
                holder.mMsg.setText("Media");
            }
        }
//        if (data.getUnreadCount() != 0) {
//            holder.mMsg.setTextColor(ContextCompat.getColor(mContext, R.color.black));
//        } else {
//            holder.mMsg.setTextColor(ContextCompat.getColor(mContext, R.color.light_black));
//        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RelativeLayout mainHolder;
        CircleImageView mImg;
        CustomTextView mName, mMsg, mTime;

        public ViewHolder(final View view) {
            super(view);
            mainHolder = itemView.findViewById(R.id.mainH);
            mImg = view.findViewById(R.id.icon);
            mName = view.findViewById(R.id.name);
            mMsg = view.findViewById(R.id.msgText);
            mTime = view.findViewById(R.id.time);
            mainHolder.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v == mainHolder) {

                if (mItemClickList != null) {
                    mItemClickList.onItemClick(itemView, getAdapterPosition());
                }
            }
        }
    }

    public interface OnItemClickList {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final OnItemClickList mItemClickListener) {
        this.mItemClickList = mItemClickListener;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public String getDate() {

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat date = new SimpleDateFormat("dd MMM yyyy", Locale.US);

        return date.format(calendar.getTime());
    }

    private String LongToTimeWithAmPm(Long Data) {


        Date time = new Date(Data);

        SimpleDateFormat dateFormat = new SimpleDateFormat("hh:mm a", Locale.US);
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        dateFormat.setTimeZone(tz);
        return dateFormat.format(time);
    }

    public String LongToDate(Long Data) {
        Date time = new Date(Data);

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy", Locale.US);
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        dateFormat.setTimeZone(tz);
        return dateFormat.format(time);
    }
}