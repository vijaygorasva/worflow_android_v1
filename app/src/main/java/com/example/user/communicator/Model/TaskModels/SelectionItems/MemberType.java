package com.example.user.communicator.Model.TaskModels.SelectionItems;


public class MemberType {

    private String _id;
    private String strMemberTypeName;
    private String strMemberTypeCode;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getStrMemberTypeName() {
        return strMemberTypeName;
    }

    public void setStrMemberTypeName(String strMemberTypeName) {
        this.strMemberTypeName = strMemberTypeName;
    }

    public String getStrMemberTypeCode() {
        return strMemberTypeCode;
    }

    public void setStrMemberTypeCode(String strMemberTypeCode) {
        this.strMemberTypeCode = strMemberTypeCode;
    }
}
