package com.example.user.communicator.Utility;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;


public class FilePicker {
    public static final int File_PICKER_REQUEST_CODE = 10001;

    public static Uri generateFileUri(Context context) {
//        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "MyDir" + File.separator);
        final File root = context.getCacheDir();
        if (!root.exists() || !root.isDirectory()) {
            root.mkdirs();
        }

        final String name = Calendar.getInstance().getTimeInMillis() + ".pdf";
        final File sdFileMainDirectory = new File(root, name);
        Uri uri = Uri.fromFile(sdFileMainDirectory);
        return uri;
    }

    public static Intent createFileIntent(Context context) {

        // Filesystem.
        final Intent fileIntent = new Intent();
        fileIntent.setType(".pdf");
        fileIntent.setAction(Intent.ACTION_GET_CONTENT);

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        photoPickerIntent.setType(".pdf");

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(photoPickerIntent, "Files and Document");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, fileIntent);

        return chooserIntent;
    }

    public static void openFileIntent(Activity activity) {
        Intent chooserIntent = createFileIntent(activity);
        activity.startActivityForResult(chooserIntent, File_PICKER_REQUEST_CODE);
    }

//    public static void openFileIntent(Context context, Fragment fragment) {
//        Intent chooserIntent = createFileIntent(context);
//        fragment.startActivityForResult(chooserIntent, File_PICKER_REQUEST_CODE);
//    }

    public static void openFileIntent(Context context, android.app.Fragment fragment) {
        Intent chooserIntent = createFileIntent(context);
        fragment.startActivityForResult(chooserIntent, File_PICKER_REQUEST_CODE);
    }

    public static boolean saveBitmap(Bitmap bmp, Uri uri) {
        File file = new File(uri.getPath());
        if (file.exists()) {
            file.delete();
        }

        FileOutputStream out = null;
        boolean result = false;
        try {
            out = new FileOutputStream(uri.getPath());
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public static Uri getFileUri(Activity activity, Intent data) {
        if (data != null) {
            String action = data.getAction();
            Uri uri = data.getData();

            if (uri != null) {
                return uri;
            } else if ("inline-data".equals(action)) {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                if (bitmap != null) {
                    Uri outputFileUri = generateFileUri(activity);
                    if (saveBitmap(bitmap, outputFileUri)) {
                        return outputFileUri;
                    } else {
                        File file = new File(outputFileUri.getPath());
                        if (file.exists()) {
                            file.delete();
                        }
                    }
                }
            }
        }

        return null;
    }

}
