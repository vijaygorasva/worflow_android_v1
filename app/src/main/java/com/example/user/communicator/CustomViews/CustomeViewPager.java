package com.example.user.communicator.CustomViews;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

import java.io.Serializable;

public class CustomeViewPager extends ViewPager implements Serializable {

    public CustomeViewPager(@NonNull Context context) {
        super(context);
    }

    public CustomeViewPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }
}
