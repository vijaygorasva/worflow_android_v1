package com.example.user.communicator.Model.ChatModels;

public class Messages {

    private String senderId;
    private String groupId;
    private String mediaPath;
    private String message;
    private String time;
    private String mediaOriginalName;
    private String _id;
    private String mediaType;
    private boolean isHaveMedia;
    private double __v;
    private double status;
    private long readTime;
    private long sendTime;
    private String date;

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getMediaPath() {
        return mediaPath;
    }

    public void setMediaPath(String mediaPath) {
        this.mediaPath = mediaPath;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public String getMediaOriginalName() {
        return mediaOriginalName;
    }

    public void setMediaOriginalName(String mediaOriginalName) {
        this.mediaOriginalName = mediaOriginalName;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public boolean isHaveMedia() {
        return isHaveMedia;
    }

    public void setHaveMedia(boolean haveMedia) {
        isHaveMedia = haveMedia;
    }

    public double get__v() {
        return __v;
    }

    public void set__v(double __v) {
        this.__v = __v;
    }

    public double getStatus() {
        return status;
    }

    public void setStatus(double status) {
        this.status = status;
    }

    public long getReadTime() {
        return readTime;
    }

    public void setReadTime(long readTime) {
        this.readTime = readTime;
    }

    public long getSendTime() {
        return sendTime;
    }

    public void setSendTime(long sendTime) {
        this.sendTime = sendTime;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}