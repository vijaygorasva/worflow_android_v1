package com.example.user.communicator.Dialog;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.user.communicator.Adapter.TaskAdapters.FilterTypeAdapter;
import com.example.user.communicator.Model.TaskModels.FilterType;
import com.example.user.communicator.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TaskFilterDialog extends DialogFragment {

    @BindView(R.id.rvRadioButtons)
    RecyclerView rvRadioButtons;

    Callback callback;

    ArrayList<FilterType> filterTypes = new ArrayList<>();

    public TaskFilterDialog() {

    }

    public static TaskFilterDialog newIntance(ArrayList<FilterType> filterType, String oldDate, String oldTime, Callback callback) {
        Bundle args = new Bundle();
        args.putSerializable("oldType", filterType);
        args.putString("oldDate", oldDate);
        args.putString("oldTime", oldTime);

        TaskFilterDialog dialog = new TaskFilterDialog();
        dialog.setArguments(args);
        dialog.setCallback(callback);
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, getTheme());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_task_filter, container, false);
        ButterKnife.bind(this, view);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.border_roundeed);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();
        filterTypes = (ArrayList<FilterType>) args.getSerializable("oldType");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        rvRadioButtons.setLayoutManager(linearLayoutManager);

        FilterTypeAdapter adapter = new FilterTypeAdapter(getContext(), filterTypes);
        rvRadioButtons.setAdapter(adapter);
//        adapter.setOnClickListner(position -> {
//            for (int i = 0; i < filterTypes.size(); i++) {
//                filterTypes.get(i).setSelected(false);
//            }
//            filterTypes.get(position).setSelected(true);
//            mFilterType = filterTypes.get(position).getKey();
//            filterType.setText(filterTypes.get(position).getName());
//            adapter.notifyDataSetChanged();
//            dialog.dismiss();
//            if (isSearch) {
//                performSearch();
//            } else {
//                if (isMyTaskItems) {
//                    setListOfTasks("My Tasks");
//                } else {
//                    setListOfTasks("Assigned Tasks");
//                }
//            }
//        });

//        callback.edit(TaskFilterDialog.this, date, time, memType);
//        dismiss();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {

        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onDestroyView() {

        super.onDestroyView();
//        ButterKnife.un(this);
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public interface Callback {
        void edit(TaskFilterDialog dialog, String date, String time, String name);
    }

}