package com.example.user.communicator.Dialog;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.communicator.Activity.PlannerModule.BaseActivity;
import com.example.user.communicator.Adapter.PlannerAdapter.PlannerCategorylistListAdapter;
import com.example.user.communicator.ConnectivityReceiver;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.Login.Login;
import com.example.user.communicator.R;
import com.example.user.communicator.Utility.EELViewUtils;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import io.realm.RealmResults;

public class CategoryDialog extends DialogFragment implements PlannerCategorylistListAdapter.Callback {

    @BindView(R.id.rvCategoryList)
    RecyclerView rvCategoryList;
    @BindView(R.id.flMainContent)
    LinearLayout flMainContent;
    @BindView(R.id.ivAdd)
    ImageView ivAdd;
    @BindView(R.id.etCategory)
    EditText etCategory;
    @BindView(R.id.llAddNew)
    LinearLayout llAddNew;
    int k = 0;
    EELViewUtils mEelViewUtils;
    Callback callback;
    String strUserId;
    ArrayList<com.example.user.communicator.Model.NoteCategory.Datum> categoryArrayList = new ArrayList<>();
    Realm mRealm;
    PlannerCategorylistListAdapter plannerCategorylistListAdapter;
    com.example.user.communicator.Model.Login.Datum mUserDetails;
    public ProgressDialog pDialog;
//    public final String URL_ROOT = "http://52.66.249.75:9999/api/";//52.66.249.75

    public final String URL_ImageUpload_SERVER = "http://52.66.249.75:9999/uploads/";
    public final String URL_ROOT_SERVER = "http://52.66.249.75:9999/api/";//52.66.249.75
    public final String BASE_URL_SEVER = "http://52.66.249.75:9999/";

    public final String URL_ImageUpload_LOCAL = "http://52.66.249.75:9999/uploads/";
    public final String URL_ROOT_local = "http://52.66.249.75:9999/api/";//52.66.249.75
    public final String BASE_URL_local = "http://52.66.249.75:9999/";

    public final String URL_ROOT_TEST = "http://13.232.37.104:9999/api/";//http://13.232.37.104:9999
    public final String URL_ImageUpload_ROOT_TEST = "http://52.66.249.75:9999/uploads/";
    public final String BASE_URL_TEST = "http://192.168.50.100:9999/";

    public final String URL_ROOT = URL_ROOT_TEST;
    public final String URL_ImageUpload_ROOT = URL_ImageUpload_ROOT_TEST;
    public final String BASE_URL = BASE_URL_TEST;

    public final String urlCategorySave = URL_ROOT + "planner/savemaincategory";
    public final String urlNoteCategory = URL_ROOT + "planner/getplnnercategoryviewAll";

    public static CategoryDialog newIntance(Callback callback) {
        Bundle args = new Bundle();
//        args.putString("oldTime", oldTime);
        CategoryDialog dialog = new CategoryDialog();
        dialog.setArguments(args);
        dialog.setCallback(callback);
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_category, container, false);
        ButterKnife.bind(this, view);

        mEelViewUtils = new EELViewUtils(flMainContent, rvCategoryList);
//        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.border_roundeed);
        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SharedPreferences sharedpreferences = getContext().getApplicationContext().getSharedPreferences("Login", 0);
        String data = sharedpreferences.getString("UserData", null);
        Gson gson = new Gson();
        mUserDetails = gson.fromJson(data, com.example.user.communicator.Model.Login.Datum.class);
        strUserId = mUserDetails.getIntUserId();
        mRealm = Realm.getDefaultInstance();
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvCategoryList.setLayoutManager(layoutManager);
        boolean isConnected = ConnectivityReceiver.isConnected();
        if (isConnected) {
            loadCategory();
        } else {
            loadCategory();
        }

        ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (k == 0) {
                    llAddNew.setVisibility(View.VISIBLE);
                    k = 1;
                } else {
                    llAddNew.setVisibility(View.GONE);
                    k = 0;
                }
            }
        });

        etCategory.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                // Perform action on key press
                String val = etCategory.getText().toString();
                if (val.length() > 15) {
                    showToast("Category name should be within 15 characters");
                } else {
                    if (isNetworkAvailable()) {
                        saveCategory();
                        etCategory.setText("");
                    } else {
                        showToast(getString(R.string.noConnection));
                    }
                }
                return true;
            }
            return false;
        });
    }


    public void saveCategory() {

        String catName = etCategory.getText().toString();
        RequestParams params = new RequestParams();
        params.put("strMainCategoryName", catName);
        params.put("strCreateUserId", strUserId);
        params.put("intProjectId", mUserDetails.getArryselectedProjectAllItems().get(0).getId());
        params.put("intOrganisationId", mUserDetails.getArryselectedOrganisationAllItems().get(0).getId());

        ApiCallWithToken(urlCategorySave, BaseActivity.ApiCall.CATEGORYSAVE, params);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {

        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    public void loadCategory() {

        if (isNetworkAvailable()) {
            RequestParams params = new RequestParams();
            params.put("strCreateUserId", strUserId);
            mEelViewUtils.showLoadingView();
            ApiCallWithToken(urlNoteCategory, BaseActivity.ApiCall.CATEGORYLIST, params);
        } else {
            categoryArrayList.clear();
            RealmResults<com.example.user.communicator.Model.NoteCategory.Datum> itemsRealmResults = mRealm.where(com.example.user.communicator.Model.NoteCategory.Datum.class).findAll();
            Log.e("loadCategory", itemsRealmResults.size() + "...");
            if (itemsRealmResults != null && itemsRealmResults.size() != 0) {
                categoryArrayList.addAll(itemsRealmResults);
                plannerCategorylistListAdapter = new PlannerCategorylistListAdapter(getActivity(), categoryArrayList, (PlannerCategorylistListAdapter.Callback) this, rvCategoryList);//, (PlannerCategorylistListAdapter.Callback) callback
                rvCategoryList.setAdapter(plannerCategorylistListAdapter);
            }
        }
    }

    public void ApiCallWithToken(String url, final BaseActivity.ApiCall type, final RequestParams requestParams) {

        try {
            SharedPreferences sharedpreferences = getActivity().getSharedPreferences("Login", 0);
            String data = sharedpreferences.getString("UserData", null);
            String token = sharedpreferences.getString("Token", null);
            Gson gson = new Gson();
            Login login = gson.fromJson(data, Login.class);
            com.example.user.communicator.Model.Login.Datum userData = gson.fromJson(data, Datum.class);
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(30 * 1000);
            if (userData != null) {
                client.addHeader("Authorization", "Bearer " + token);
            }

            client.setURLEncodingEnabled(true);
            client.post(getActivity(), url, requestParams, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    try {
                        if (response.getBoolean("success")) {
                            OnResponce(response, type);
                        } else {
                            OnError(response, type);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    hideProgressDialog(getActivity());
                    String errorType = throwable.getMessage();
                    if (errorType.equals("Read timed out")) {
                        showToast("Oops!!! Server Time-out.Please try again");
                    } else {
                        showToast("Oops!!! Please try again");
                        OnError(errorResponse, type);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    hideProgressDialog(getActivity());
                    showToast("Oops!!! Please try again");
                }

            });

        } catch (Exception e) {
            hideProgressDialog(getActivity());
            e.printStackTrace();
        }

    }

    public void OnResponce(JSONObject data, BaseActivity.ApiCall type) {
        String s = data.toString();
        JSONArray array = null;
        try {
            if (type == BaseActivity.ApiCall.CATEGORYLIST) {
                array = data.getJSONArray("data");
                hideProgressDialog(getActivity());
                mEelViewUtils.showContentViewColorPrimary();
                if (array.length() != 0) {

                    Gson gson = new Gson();
                    categoryArrayList.clear();
                    com.example.user.communicator.Model.NoteCategory.Datum datum = new com.example.user.communicator.Model.NoteCategory.Datum();
                    datum.setId("0");
                    datum.setStrMainCategoryName("All");
                    categoryArrayList.add(0, datum);
                    mRealm.beginTransaction();
                    mRealm.copyToRealmOrUpdate(datum);
                    for (int i = 0; i < array.length(); i++) {
                        try {

                            JSONObject object = array.getJSONObject(i);
                            com.example.user.communicator.Model.NoteCategory.Datum item = gson.fromJson(object.toString(), com.example.user.communicator.Model.NoteCategory.Datum.class);
                            mRealm.copyToRealmOrUpdate(item);
                            categoryArrayList.add(item);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    mRealm.commitTransaction();
                    if (categoryArrayList.size() != 0) {
                        plannerCategorylistListAdapter = new PlannerCategorylistListAdapter(getActivity(), categoryArrayList, (PlannerCategorylistListAdapter.Callback) this, rvCategoryList);//,(PlannerCategorylistListAdapter.Callack) callback
                        rvCategoryList.setAdapter(plannerCategorylistListAdapter);
                    }

                }

            } else if (type == BaseActivity.ApiCall.CATEGORYSAVE) {
                array = data.getJSONArray("data");
                hideProgressDialog(getActivity());
                if (array.length() != 0) {
                    Gson gson = new Gson();
                    for (int i = 0; i < array.length(); i++) {
                        try {

                            JSONObject object = array.getJSONObject(i);
                            com.example.user.communicator.Model.NoteCategory.Datum item = gson.fromJson(object.toString(), com.example.user.communicator.Model.NoteCategory.Datum.class);

                            if (plannerCategorylistListAdapter.getItemCount() > 0) {
                                plannerCategorylistListAdapter.insertItem(item, 2);

                                plannerCategorylistListAdapter.notifyItemInserted(2);
                            } else {
                                ArrayList<com.example.user.communicator.Model.NoteCategory.Datum> categoryArrayList = new ArrayList<>();
                                categoryArrayList.add(item);
                                plannerCategorylistListAdapter = new PlannerCategorylistListAdapter(getActivity(), categoryArrayList, (PlannerCategorylistListAdapter.Callback) this, rvCategoryList);//,(PlannerCategorylistListAdapter.Callack) callback
                                rvCategoryList.setAdapter(plannerCategorylistListAdapter);
                            }

                            etCategory.setText("");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void OnError(JSONObject object, BaseActivity.ApiCall type) {

        hideProgressDialog(getActivity());

        try {
            showToast(object.getString("message"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void showToast(String message) {
        LayoutInflater inflater = getLayoutInflater();
        View layouttoast = inflater.inflate(R.layout.toastcustom, (ViewGroup) getActivity().findViewById(R.id.toastcustom));
        ((TextView) layouttoast.findViewById(R.id.texttoast)).setText(message);

        Toast mytoast = new Toast(getActivity());
        mytoast.setView(layouttoast);
        mytoast.setDuration(Toast.LENGTH_LONG);
        mytoast.setGravity(Gravity.CENTER, 0, 0);
        mytoast.show();
    }

    public void hideProgressDialog(Context ct) {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    public void showProgressDialog(Context ct, String message) {
        if (pDialog == null || !pDialog.isShowing()) {
            pDialog = new ProgressDialog(ct);
            pDialog.setMessage(message);
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void getCategorySelceted(PlannerCategorylistListAdapter dialog, String id, String catName) {
        callback.edit(this, id, catName);
        dismiss();
    }

    public interface Callback {
        void edit(CategoryDialog dialog, String id, String catName);
    }

}