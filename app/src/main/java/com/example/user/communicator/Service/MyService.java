package com.example.user.communicator.Service;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.example.user.communicator.Activity.PlannerModule.AddNewNoteActivity;
import com.example.user.communicator.Activity.PlannerModule.NoteDetailActivity;

public class MyService extends Service {

    Handler handler;
    Runnable test;

    @Override
    public void onDestroy() {
        handler.removeCallbacks(test);
        super.onDestroy();
        Log.d("ClearFromRecentService", "Service Destroyed");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        handler.removeCallbacks(test);
        stopSelf();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {

        handler = new Handler();
        if (intent != null) {
            Boolean stop = intent.getBooleanExtra("close", false);
            String type = intent.getStringExtra("activity");
            Log.e("SERvice", stop + "...." + type);

            if (stop) {
                stopSelf();
                handler.removeCallbacks(test);
            } else {
                Log.e("SERvicety", "else...." + type);
                if (type.equals("AddNewNoteActivity")) {
                    test = () -> {
                        handler.postDelayed(test, 3000); //100 ms you should do it 4000
                        AddNewNoteActivity.storeData();
                    };
                } else if (type.equals("NoteDetailActivity")) {
                    test = () -> {
                        handler.postDelayed(test, 3000); //100 ms you should do it 4000
                        NoteDetailActivity.storeData();
                    };
                } else {
                    test = () -> {
//                    handler.postDelayed(test, 3000); //100 ms you should do it 4000
//                    DetailActivtiy.storeData();
                    };
                }

                handler.postDelayed(test, 0);

            }
        }
        return START_STICKY;
    }

}
