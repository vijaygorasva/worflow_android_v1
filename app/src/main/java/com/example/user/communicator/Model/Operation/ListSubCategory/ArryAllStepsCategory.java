package com.example.user.communicator.Model.Operation.ListSubCategory;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class ArryAllStepsCategory extends RealmObject implements Parcelable {

    public static final Parcelable.Creator<ArryAllStepsCategory> CREATOR = new Parcelable.Creator<ArryAllStepsCategory>() {
        @Override
        public ArryAllStepsCategory createFromParcel(Parcel in) {
            return new ArryAllStepsCategory(in);
        }

        @Override
        public ArryAllStepsCategory[] newArray(int size) {
            return new ArryAllStepsCategory[size];
        }
    };
    @PrimaryKey
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("fkintSubOperationId")
    @Expose
    private String fkintSubOperationId;
    @SerializedName("strStepDiscretion")
    @Expose
    private String strStepDiscretion;
    @SerializedName("intCreateUserId")
    @Expose
    private String intCreateUserId;
    @SerializedName("datCreateDateAndTime")
    @Expose
    private String datCreateDateAndTime;
    @SerializedName("blnAddStep")
    @Expose
    private String blnAddStep;
    @SerializedName("datActionDateAndTime")
    @Expose
    private String datActionDateAndTime;
    @SerializedName("datLastModifiedDateAndTime")
    @Expose
    private String datLastModifiedDateAndTime;
    @SerializedName("intLastModifiedUserId")
    @Expose
    private String intLastModifiedUserId;

    public ArryAllStepsCategory() {
    }

    protected ArryAllStepsCategory(Parcel in) {
        id = in.readString();
        fkintSubOperationId = in.readString();
        strStepDiscretion = in.readString();
        intCreateUserId = in.readString();
        datCreateDateAndTime = in.readString();
        blnAddStep = in.readString();
        datActionDateAndTime = in.readString();
        datLastModifiedDateAndTime = in.readString();
        intLastModifiedUserId = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFkintSubOperationId() {
        return fkintSubOperationId;
    }

    public void setFkintSubOperationId(String fkintSubOperationId) {
        this.fkintSubOperationId = fkintSubOperationId;
    }

    public String getStrStepDiscretion() {
        return strStepDiscretion;
    }

    public void setStrStepDiscretion(String strStepDiscretion) {
        this.strStepDiscretion = strStepDiscretion;
    }

    public String getIntCreateUserId() {
        return intCreateUserId;
    }

    public void setIntCreateUserId(String intCreateUserId) {
        this.intCreateUserId = intCreateUserId;
    }

    public String getDatCreateDateAndTime() {
        return datCreateDateAndTime;
    }

    public void setDatCreateDateAndTime(String datCreateDateAndTime) {
        this.datCreateDateAndTime = datCreateDateAndTime;
    }

    public String getBlnAddStep() {
        return blnAddStep;
    }

    public void setBlnAddStep(String blnAddStep) {
        this.blnAddStep = blnAddStep;
    }

    public String getDatActionDateAndTime() {
        return datActionDateAndTime;
    }

    public void setDatActionDateAndTime(String datActionDateAndTime) {
        this.datActionDateAndTime = datActionDateAndTime;
    }

    public String getDatLastModifiedDateAndTime() {
        return datLastModifiedDateAndTime;
    }

    public void setDatLastModifiedDateAndTime(String datLastModifiedDateAndTime) {
        this.datLastModifiedDateAndTime = datLastModifiedDateAndTime;
    }

    public String getIntLastModifiedUserId() {
        return intLastModifiedUserId;
    }

    public void setIntLastModifiedUserId(String intLastModifiedUserId) {
        this.intLastModifiedUserId = intLastModifiedUserId;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(id);
        parcel.writeString(fkintSubOperationId);
        parcel.writeString(strStepDiscretion);
        parcel.writeString(intCreateUserId);
        parcel.writeString(datCreateDateAndTime);
        parcel.writeString(blnAddStep);
        parcel.writeString(datActionDateAndTime);
        parcel.writeString(datLastModifiedDateAndTime);
        parcel.writeString(intLastModifiedUserId);
    }
}
