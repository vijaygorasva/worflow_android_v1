package com.example.user.communicator.Model.Notes;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import io.realm.annotations.PrimaryKey;

public class Undo {

    @PrimaryKey
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("note")
    @Expose
    private String note;

//    @SerializedName("checkboxdummy")
//    @Expose
//    private UndoCheckboxModel undoCheckboxModelDummy;


//    public UndoCheckboxModel getUndoCheckboxModel() {
//        return undoCheckboxModelDummy;
//    }

//    public void setUndoCheckboxModel(UndoCheckboxModel undoCheckboxModel) {
//        this.undoCheckboxModelDummy = undoCheckboxModel;
//    }


    @SerializedName("checkbox")
    @Expose
    private ArrayList<UndoCheckboxModel> undoCheckboxModel;

    public ArrayList<UndoCheckboxModel> getUndoCheckboxModel() {
        return undoCheckboxModel;
    }

    public void setUndoCheckboxModel(ArrayList<UndoCheckboxModel> undoCheckboxModel) {
        this.undoCheckboxModel = undoCheckboxModel;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
