package com.example.user.communicator.Dialog;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.communicator.Activity.PlannerModule.BaseActivity;
import com.example.user.communicator.ConnectivityReceiver;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.Login.Login;
import com.example.user.communicator.Model.Operation.ListCategory.DatumOper;
import com.example.user.communicator.R;
import com.example.user.communicator.Utility.EELViewUtils;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import io.realm.RealmResults;

public class OperationCategoryDialog extends DialogFragment {

    @BindView(R.id.rvCategoryList)
    RecyclerView rvCategoryList;

    @BindView(R.id.title)
    TextView titleTxt;

    @BindView(R.id.etCategory)
    EditText etCategory;
    @BindView(R.id.llAddNew)
    LinearLayout llAddNew;
    @BindView(R.id.ivAdd)
    ImageView ivAdd;
    @BindView(R.id.flMainContent)
    LinearLayout flMainContent;
    EELViewUtils mEelViewUtils;
    Callback callback;
    String strUserId;
    int k = 0;
    OperationCategoryAdapter categoryAdapter;
    public ProgressDialog pDialog;
    ArrayList<DatumOper> categoryArrayList = new ArrayList<>();

    Realm mRealm;
    String mTitle;
    Datum mUserDetails;
//    public final String URL_ROOT = "http://52.66.249.75:9999/api/";//52.66.249.75

    public final String URL_ImageUpload_SERVER = "http://52.66.249.75:9999/uploads/";
    public final String URL_ROOT_SERVER = "http://52.66.249.75:9999/api/";//52.66.249.75
    public final String BASE_URL_SEVER = "http://52.66.249.75:9999/";

    public final String URL_ImageUpload_LOCAL = "http://52.66.249.75:9999/uploads/";
    public final String URL_ROOT_local = "http://52.66.249.75:9999/api/";//52.66.249.75
    public final String BASE_URL_local = "http://52.66.249.75:9999/";

    public final String URL_ROOT_TEST = "http://13.232.37.104:9999/api/";//http://13.232.37.104:9999
    public final String URL_ImageUpload_ROOT_TEST = "http://52.66.249.75:9999/uploads/";
    public final String BASE_URL_TEST = "http://192.168.50.100:9999/";

    public final String URL_ROOT = URL_ROOT_TEST;
    public final String URL_ImageUpload_ROOT = URL_ImageUpload_ROOT_TEST;
    public final String BASE_URL = BASE_URL_TEST;

    public final String url_operationsList = URL_ROOT + "operation/getoperationcategoryviewAll";
    public final String url_saveMainCat = URL_ROOT + "operation/savemaincategoryoperation";

    public static OperationCategoryDialog newIntance(String oldTime, String title, Callback callback) {
        Bundle args = new Bundle();
        args.putString("oldTime", oldTime);
        args.putString("title", title);
        OperationCategoryDialog dialog = new OperationCategoryDialog();
        dialog.setArguments(args);
        dialog.setCallback(callback);
        return dialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        categoryAdapter = new OperationCategoryAdapter(categoryArrayList, getActivity());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.dialog_operation_category, container, false);
        ButterKnife.bind(this, view);
        mEelViewUtils = new EELViewUtils(flMainContent, rvCategoryList);
        return view;

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();
//        oldType = args.getString("oldType");

        if (args != null) {
            String title = args.getString("title");
            if (!TextUtils.isEmpty(title)) {
                titleTxt.setText(title);
            }
        }

        SharedPreferences sharedpreferences = getContext().getApplicationContext().getSharedPreferences("Login", 0);
        String data = sharedpreferences.getString("UserData", null);
        Gson gson = new Gson();
        mUserDetails = gson.fromJson(data, Datum.class);
        strUserId = mUserDetails.getIntUserId();
        mRealm = Realm.getDefaultInstance();

        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvCategoryList.setLayoutManager(layoutManager);
        boolean isConnected = ConnectivityReceiver.isConnected();
        getAllCategory();

        etCategory.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                // Perform action on key press
                String val = etCategory.getText().toString();
                if (val.length() > 30) {
                    showToast("Category name should be within 15 characters");
                } else {
                    if (isNetworkAvailable()) {
                        addMainCategory(val);
                        etCategory.setText("");
                    } else {
                        showToast(getString(R.string.noConnection));
                    }
                }
                return true;
            }
            return false;
        });

        ivAdd.setOnClickListener(v -> {
            if (k == 0) {
                llAddNew.setVisibility(View.VISIBLE);
                k = 1;
            } else {
                llAddNew.setVisibility(View.GONE);
                k = 0;
            }
        });

//        rvCategoryList.addOnItemTouchListener(new RecyclerItemLongClickListener(getActivity(), rvCategoryList, new RecyclerItemLongClickListener.OnItemClickListener() {
//            @Override
//            public void onItemClick(View view, int pos) {
//                callback.selectopertaionCategory(String.valueOf(pos), categoryArrayList.get(pos).getStrOperationMainCategoryName(), categoryArrayList.get(pos).getId());//Itempos=pos  ,  listPos=position
//                dismiss();
//            }
//
//            @Override
//            public void onItemLongClick(View view, int pos) {
//                callback.selectopertaionCategory(String.valueOf(pos), categoryArrayList.get(pos).getStrOperationMainCategoryName(), categoryArrayList.get(pos).getId());//Itempos=pos  ,  listPos=position
//                dismiss();
//            }
//
//        }));

    }


    @Override
    public void onAttach(Activity activity) {
//        CategoryAdapter categoryAdapter = new CategoryAdapter(categoryArrayList, getActivity());
        super.onAttach(activity);
        try {

        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    public void showToast(String message) {
        LayoutInflater inflater = getLayoutInflater();
        View layouttoast = inflater.inflate(R.layout.toastcustom, (ViewGroup) getActivity().findViewById(R.id.toastcustom));
        ((TextView) layouttoast.findViewById(R.id.texttoast)).setText(message);

        Toast mytoast = new Toast(getActivity());
        mytoast.setView(layouttoast);
        mytoast.setDuration(Toast.LENGTH_LONG);
        mytoast.setGravity(Gravity.CENTER, 0, 0);
        mytoast.show();
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public interface Callback {
        void selectopertaionCategory(String position, String catName, String id);
    }


    public void addMainCategory(String val) {

        String catName = etCategory.getText().toString();
        RequestParams params = new RequestParams();
        params.put("strCreateUserId", strUserId);
        params.put("strOperationMainCategoryName", val);
        params.put("intProjectId", mUserDetails.getArryselectedProjectAllItems().get(0).getId());
        params.put("intOrganisationId", mUserDetails.getArryselectedOrganisationAllItems().get(0).getId());
//        showProgressDialog(getActivity(), "Please Wait...");
        ApiCallWithToken(url_saveMainCat, BaseActivity.ApiCall.ADDMAINCAT, params);

    }

    public void getAllCategory() {
        if (isNetworkAvailable()) {
            RequestParams params = new RequestParams();
            params.put("strCreateUserId", strUserId);
            Log.e("getAllCategory", params + "....." + strUserId);
            ApiCallWithToken(url_operationsList, BaseActivity.ApiCall.OPERATIONSLIST, params);

        } else {

            categoryArrayList.clear();
            RealmResults<DatumOper> itemsRealmResults = mRealm.where(DatumOper.class).findAll();
            if (itemsRealmResults != null && itemsRealmResults.size() != 0) {
                categoryArrayList.addAll(itemsRealmResults);
                categoryAdapter = new OperationCategoryAdapter(categoryArrayList, getActivity());
                rvCategoryList.setAdapter(categoryAdapter);
            }
        }
    }

    public void ApiCallWithToken(String url, final BaseActivity.ApiCall type, final RequestParams requestParams) {

        try {
            SharedPreferences sharedpreferences = getActivity().getSharedPreferences("Login", 0);
            String data = sharedpreferences.getString("UserData", null);
            String token = sharedpreferences.getString("Token", null);
            Gson gson = new Gson();
            Login login = gson.fromJson(data, Login.class);
            com.example.user.communicator.Model.Login.Datum userData = gson.fromJson(data, Datum.class);
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(30 * 1000);
            if (userData != null) {
                client.addHeader("Authorization", "Bearer " + token);
            }

//            ByteArrayEntity entity = new ByteArrayEntity(requestParams.toString().getBytes("UTF-8"));
//            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            client.setURLEncodingEnabled(true);
            client.post(getActivity(), url, requestParams, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    try {
                        if (response.getBoolean("success")) {
                            OnResponce(response, type);
                        } else {
                            OnError(response, type);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    hideProgressDialog(getActivity());
                    String errorType = throwable.getMessage();
                    if (errorType.equals("Read timed out")) {
                        showToast("Oops!!! Server Time-out.Please try again");
                    } else {
                        showToast("Oops!!! Please try again");
                        OnError(errorResponse, type);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    hideProgressDialog(getActivity());
                    showToast("Oops!!! Please try again");
                }

            });

        } catch (Exception e) {
            hideProgressDialog(getActivity());
            e.printStackTrace();
        }

    }

    public void OnResponce(JSONObject data, BaseActivity.ApiCall type) {
        String s = data.toString();
        JSONArray array = null;
        try {
            if (type == BaseActivity.ApiCall.OPERATIONSLIST) {
                array = data.getJSONArray("data");
                hideProgressDialog(getActivity());
                mEelViewUtils.showContentViewColorPrimary();
                if (array.length() != 0) {

                    Gson gson = new Gson();
                    categoryArrayList.clear();
                    mRealm.beginTransaction();
                    DatumOper newdata = new DatumOper();
                    newdata.setStrOperationMainCategoryName("All");
                    categoryArrayList.add(0, newdata);
                    newdata.setId("0");
                    mRealm.copyToRealmOrUpdate(newdata);
                    for (int i = 0; i < array.length(); i++) {
                        try {

                            JSONObject object = array.getJSONObject(i);
                            DatumOper item = gson.fromJson(object.toString(), DatumOper.class);
                            mRealm.copyToRealmOrUpdate(item);
                            categoryArrayList.add(item);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                    mRealm.commitTransaction();
                    if (categoryArrayList.size() != 0) {
                        categoryAdapter = new OperationCategoryAdapter(categoryArrayList, getActivity());
                        rvCategoryList.setAdapter(categoryAdapter);
                    }
                }

            } else if (type == BaseActivity.ApiCall.ADDMAINCAT) {
                array = data.getJSONArray("data");
                hideProgressDialog(getActivity());
                mEelViewUtils.showContentViewColorPrimary();
                if (array.length() != 0) {
                    Gson gson = new Gson();
                    for (int i = 0; i < array.length(); i++) {
                        try {
                            JSONObject object = array.getJSONObject(i);
                            DatumOper item = gson.fromJson(object.toString(), DatumOper.class);
                            if (categoryAdapter.getItemCount() > 0) {
                                categoryAdapter.insertItem(1, item);
                                categoryAdapter.notifyDataSetChanged();
                            } else {
                                ArrayList<DatumOper> categoryArrayList = new ArrayList<>();
                                categoryArrayList.add(item);
                                categoryAdapter = new OperationCategoryAdapter(categoryArrayList, getActivity());
                                rvCategoryList.setAdapter(categoryAdapter);
                            }

                            etCategory.setText("");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void OnError(JSONObject object, BaseActivity.ApiCall type) {

        if (type == BaseActivity.ApiCall.OPERATIONSLIST) {
            hideProgressDialog(getActivity());

            try {
                showToast(object.getString("message"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void hideProgressDialog(Context ct) {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    public void showProgressDialog(Context ct, String message) {
        if (pDialog == null || !pDialog.isShowing()) {
            pDialog = new ProgressDialog(ct);
            pDialog.setMessage(message);
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }


    public class OperationCategoryAdapter extends RecyclerView.Adapter<OperationCategoryAdapter.CategoryViewHolder> {

        int btnVisible = 0;
        private int expandPos = 9999;
        Boolean isExpandedSave = false;
        ArrayList<DatumOper> categoryLists;
        Context context;
        public ProgressDialog pDialog;
        String strUserId;
        Realm mRealm;
        Datum mUserDetails;

        public final String URL_ROOT = "http://52.66.249.75:9999/api/";//52.66.249.75
        public final String url_updateMainCat = URL_ROOT + "operation/updatemaincategoryoperation";
        public final String url_deleteMainCat = URL_ROOT + "operation/deletemaincategoryoperation";

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        public OperationCategoryAdapter(ArrayList<DatumOper> categoryLists, Context context) {
            this.categoryLists = categoryLists;
            this.context = context;
        }

        public void insertItem(int position, DatumOper item) {
            categoryLists.add(position, item);
        }

        @Override
        public OperationCategoryAdapter.CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_operation_category, parent, false);
            OperationCategoryAdapter.CategoryViewHolder viewHolder = new OperationCategoryAdapter.CategoryViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(OperationCategoryAdapter.CategoryViewHolder holder, int position) {
            holder.bind(categoryLists.get(position), holder, position);
        }

        @Override
        public int getItemCount() {
            return categoryLists.size();
        }

        public class CategoryViewHolder extends RecyclerView.ViewHolder {

            @BindView(R.id.tvCategory)
            TextView tvCategory;
            @BindView(R.id.etCategory)
            EditText etCategory;
            @BindView(R.id.ivSave)
            ImageView ivSave;
            @BindView(R.id.ivDelete)
            ImageView ivDelete;
            @BindView(R.id.llMain)
            LinearLayout llMain;
            @BindView(R.id.flIcons)
            FrameLayout flIcons;

            public CategoryViewHolder(View itemView) {

                super(itemView);
                ButterKnife.bind(this, itemView);

            }

            public void bind(final DatumOper items, final OperationCategoryAdapter.CategoryViewHolder holder, int position) {

                final boolean isExpanded = holder.getAdapterPosition() == expandPos;
                tvCategory.setText(items.getStrOperationMainCategoryName());
                etCategory.setText(tvCategory.getText().toString());

                SharedPreferences sharedpreferences = context.getApplicationContext().getSharedPreferences("Login", 0);
                String data = sharedpreferences.getString("UserData", null);
                Gson gson = new Gson();
                mUserDetails = gson.fromJson(data, Datum.class);
                strUserId = mUserDetails.getIntUserId();
                mRealm = Realm.getDefaultInstance();

                llMain.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (btnVisible == 0) {
                            callback.selectopertaionCategory(String.valueOf(position), categoryLists.get(position).getStrOperationMainCategoryName(), categoryLists.get(position).getId());//Itempos=pos  ,  listPos=position
                            dismiss();
                        } else {
                            etCategory.setCursorVisible(true);
                            etCategory.requestFocus();
                            etCategory.setSelection(etCategory.getText().toString().length());
                        }
                    }
                });

                llMain.setActivated(isExpanded);
                etCategory.setCursorVisible(isExpanded);
                flIcons.setVisibility(View.GONE);
                if ((isExpanded) && (!isExpandedSave)) {
                    flIcons.setVisibility(View.VISIBLE);
                    tvCategory.setVisibility(View.GONE);
                    etCategory.setVisibility(View.VISIBLE);
                    isExpandedSave = false;
                } else {
                    flIcons.setVisibility(View.GONE);
                    tvCategory.setVisibility(View.VISIBLE);
                    etCategory.setVisibility(View.GONE);
                }

                if (position != 0) {
                    llMain.setOnLongClickListener(new View.OnLongClickListener() {
                        @Override
                        public boolean onLongClick(View v) {

                            if (isNetworkAvailable()) {
                                expandPos = isExpanded ? -1 : holder.getAdapterPosition();
                                isExpandedSave = false;

//                            if ((isExpanded) && (isExpandedSave)) {
//                                notifyItemChanged(holder.getAdapterPosition());
//                                etCategory.setCursorVisible(false);
//                            } else {
//                                TransitionManager.beginDelayedTransition(recyclerView);
//                            }
                                btnVisible = 1;


                                tvCategory.setVisibility(View.GONE);
                                etCategory.setVisibility(View.VISIBLE);
                                etCategory.setCursorVisible(true);
                                etCategory.requestFocus();
                                etCategory.setSelection(etCategory.getText().toString().length());
                                notifyItemChanged(getItemViewType());
                                notifyItemRangeChanged(0, categoryLists.size());
                                notifyDataSetChanged();

                            } else {
                                showToast(context.getString(R.string.noConnection));
                            }
                            return false;

                        }
                    });
                }
                ivSave.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (isNetworkAvailable()) {
                            String catID = items.getId();
                            String Name = items.getStrOperationMainCategoryName();
                            int position = getItemViewType();
                            btnVisible = 0;
                            ivSave.setVisibility(View.GONE);
                            String catName = etCategory.getText().toString();
                            if (catName.length() > 15) {
                                showToast("Category name should be within 15 characters");
                            } else {

                                updateMainCategory(catID, catName);
                            }
//                        isExpanded=false;
                        } else {
                            showToast(context.getString(R.string.noConnection));
                        }
                    }
                });

                ivDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (isNetworkAvailable()) {
                            int pos = getItemViewType();
                            categoryLists.remove(pos);
                            notifyItemRemoved(pos);
                            deleteMainCategory(items.getId());
                            ivDelete.setVisibility(View.GONE);
                            ivSave.setVisibility(View.GONE);
                        } else {
                            showToast(context.getString(R.string.noConnection));
                        }

                    }
                });


            }


            public boolean isNetworkAvailable() {
                ConnectivityManager connectivityManager
                        = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                return activeNetworkInfo != null && activeNetworkInfo.isConnected();
            }


            public void updateMainCategory(String mainCatId, String mainCatName) {

                RequestParams params = new RequestParams();
                params.put("strOperationMainCategoryId", mainCatId);
                params.put("strModifiedUserId", strUserId);
                params.put("strOperationMainCategoryName", mainCatName);
                params.put("intProjectId", mUserDetails.getArryselectedProjectAllItems().get(0).getId());
                params.put("intOrganisationId", mUserDetails.getArryselectedOrganisationAllItems().get(0).getId());
//        showProgressDialog(getActivity(), "Please Wait...");
                ApiCallWithToken(url_updateMainCat, BaseActivity.ApiCall.UPDATEMAINCAT, params);

            }

            public void deleteMainCategory(String mainCatId) {

                RequestParams params = new RequestParams();
                params.put("strModifiedUserId", strUserId);
                params.put("strOperationMainCategoryId", mainCatId);
//        showProgressDialog(getActivity(), "Please Wait...");
                ApiCallWithToken(url_deleteMainCat, BaseActivity.ApiCall.DELETEMAINCAT, params);

            }


            public void ApiCallWithToken(String url, final BaseActivity.ApiCall type, final RequestParams requestParams) {

                try {
                    SharedPreferences sharedpreferences = context.getSharedPreferences("Login", 0);
                    String data = sharedpreferences.getString("UserData", null);
                    String token = sharedpreferences.getString("Token", null);
                    Gson gson = new Gson();
                    Login login = gson.fromJson(data, Login.class);
                    com.example.user.communicator.Model.Login.Datum userData = gson.fromJson(data, Datum.class);
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.setTimeout(30 * 1000);
                    if (userData != null) {
                        client.addHeader("Authorization", "Bearer " + token);
                    }

//            ByteArrayEntity entity = new ByteArrayEntity(requestParams.toString().getBytes("UTF-8"));
//            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    client.setURLEncodingEnabled(true);
                    client.post(context, url, requestParams, new JsonHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                            try {
                                if (response.getBoolean("success")) {
                                    OnResponce(response, type);
                                } else {
                                    OnError(response, type);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                            hideProgressDialog(context);
                            String errorType = throwable.getMessage();
                            if (errorType.equals("Read timed out")) {
                                showToast("Oops!!! Server Time-out.Please try again");
                            } else {
                                showToast("Oops!!! Please try again");
                                OnError(errorResponse, type);
                            }
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                            hideProgressDialog(context);
                            showToast("Oops!!! Please try again");
                        }

                    });

                } catch (Exception e) {
                    hideProgressDialog(context);
                    e.printStackTrace();
                }

            }

            public void OnResponce(JSONObject data, BaseActivity.ApiCall type) {
                String s = data.toString();
                JSONArray array = null;
                try {
                    if (type == BaseActivity.ApiCall.OPERATIONSLIST) {
                        array = data.getJSONArray("data");
                        hideProgressDialog(context);
                        if (array.length() != 0) {
                            Gson gson = new Gson();
                            for (int i = 0; i < array.length(); i++) {
                                try {
                                    JSONObject object = array.getJSONObject(i);
//                            DatumOper item = gson.fromJson(object.toString(), DatumOper.class);
//                            categoryAdapter.insertItem(0, item);
//                            categoryAdapter.notifyDataSetChanged();
//                            etCategory.setText("");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    } else if (type == BaseActivity.ApiCall.ADDMAINCAT) {
                        array = data.getJSONArray("data");
                        hideProgressDialog(context);
                        if (array.length() != 0) {
                            Gson gson = new Gson();
                            for (int i = 0; i < array.length(); i++) {
                                try {
                                    JSONObject object = array.getJSONObject(i);
//                            DatumOper item = gson.fromJson(object.toString(), DatumOper.class);
//                            categoryAdapter.insertItem(0, item);
//                            categoryAdapter.notifyDataSetChanged();
//                            etCategory.setText("");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    } else if (type == BaseActivity.ApiCall.UPDATEMAINCAT) {

                        hideProgressDialog(context);
                        try {
                            if (data.getBoolean("success")) {
                                tvCategory.setText(etCategory.getText().toString());
                                etCategory.setVisibility(View.GONE);
                                tvCategory.setVisibility(View.VISIBLE);
                                isExpandedSave = true;
                                flIcons.setVisibility(View.GONE);
                            } else {
                                showToast(data.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else if (type == BaseActivity.ApiCall.DELETEMAINCAT) {
                        array = data.getJSONArray("data");
                        hideProgressDialog(context);

                        isExpandedSave = true;
                        flIcons.setVisibility(View.GONE);
                        notifyDataSetChanged();
                        showToast(data.getString("message"));

                    } else {
                        showToast(data.getString("message"));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            public void OnError(JSONObject object, BaseActivity.ApiCall type) {

                if (type == BaseActivity.ApiCall.OPERATIONSLIST) {
                    hideProgressDialog(context);

                    try {
                        showToast(object.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }


            public void hideProgressDialog(Context ct) {
                if (pDialog != null && pDialog.isShowing()) {
                    pDialog.dismiss();
                    pDialog = null;
                }
            }

            public void showProgressDialog(Context ct, String message) {
                if (pDialog == null || !pDialog.isShowing()) {
                    pDialog = new ProgressDialog(ct);
                    pDialog.setMessage(message);
                    pDialog.setCancelable(false);
                    pDialog.show();
                }
            }


        }
    }


}