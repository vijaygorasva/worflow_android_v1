
package com.example.user.communicator.Model.Operation.SubCategory;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class DatumSub extends RealmObject {

    @PrimaryKey
    @SerializedName("fkintMainOperationId")
    @Expose
    private String fkintMainOperationId;
    @SerializedName("strTitle")
    @Expose
    private String strTitle;
    @SerializedName("strOperationSubCategoryName")
    @Expose
    private String strOperationSubCategoryName;
    @SerializedName("intCreateUserId")
    @Expose
    private String intCreateUserId;
    @SerializedName("datCreateDateAndTime")
    @Expose
    private String datCreateDateAndTime;
    @SerializedName("strGoalAchive")
    @Expose
    private String strGoalAchive;
    @SerializedName("_id")
    @Expose
    private String id;

    public String getFkintMainOperationId() {
        return fkintMainOperationId;
    }

    public void setFkintMainOperationId(String fkintMainOperationId) {
        this.fkintMainOperationId = fkintMainOperationId;
    }

    public String getStrTitle() {
        return strTitle;
    }

    public void setStrTitle(String strTitle) {
        this.strTitle = strTitle;
    }

    public String getStrOperationSubCategoryName() {
        return strOperationSubCategoryName;
    }

    public void setStrOperationSubCategoryName(String strOperationSubCategoryName) {
        this.strOperationSubCategoryName = strOperationSubCategoryName;
    }

    public String getIntCreateUserId() {
        return intCreateUserId;
    }

    public void setIntCreateUserId(String intCreateUserId) {
        this.intCreateUserId = intCreateUserId;
    }

    public String getDatCreateDateAndTime() {
        return datCreateDateAndTime;
    }

    public void setDatCreateDateAndTime(String datCreateDateAndTime) {
        this.datCreateDateAndTime = datCreateDateAndTime;
    }

    public String getStrGoalAchive() {
        return strGoalAchive;
    }

    public void setStrGoalAchive(String strGoalAchive) {
        this.strGoalAchive = strGoalAchive;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
