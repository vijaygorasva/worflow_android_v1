package com.example.user.communicator.Utility;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.common.primitives.Chars;

import java.util.Locale;


public class Utils {

    public static int getScreenWidth(@NonNull Context context) {
        Point size = new Point();
        ((Activity) context).getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    }

    public static int getScreenHeight(@NonNull Context context) {
        Point size = new Point();
        ((Activity) context).getWindowManager().getDefaultDisplay().getSize(size);
        return size.y;
    }

    public static boolean isInLandscapeMode(@NonNull Context context) {
        boolean isLandscape = false;
        if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            isLandscape = true;
        }
        return isLandscape;
    }

    public static int dp2px(Context context, int dp) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        DisplayMetrics displaymetrics = new DisplayMetrics();
        display.getMetrics(displaymetrics);

        return (int) (dp * displaymetrics.density + 0.5f);
    }

    public static boolean isNetworkAvailable(Context context, boolean alert) {
        boolean available = isNetworkAvailable(context);
        if (!available && alert) {
            showNetworkAlert(context);
        }
        return available;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void showNetworkAlert(Context context) {
        alert(context, "Network Error", "No network connected");
    }

    public static void alert(Context context, String title, String message) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    /*  public static String getToken(Context context) {
          SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
          String token = sp.getString("token", null);
          token = "3295c76acbf4caaed33c36b1b5fc2cb1";
          token = "0f49c89d1e7298bb9930789c8ed59d48";
          token = "92c8c96e4c37100777c7190b76d28233";

          if (TextUtils.isEmpty(token)) {
              MainActivity.start(context);
          }

          return token;
      }

  */
    public static String trim(String string) {
        if (TextUtils.isEmpty(string)) return string;

        int start = 0, last = string.length() - 1;
        int end = last;

        while ((start <= end) && (string.charAt(start) == Chars.checkedCast(160) || string.charAt(start) == ' ' || string.charAt(start) <= '\n' || string.charAt(start) <= '\t' || string.charAt(start) <= '\r')) {
            start++;
        }

        while ((end >= start) && (string.charAt(end) == Chars.checkedCast(160) || string.charAt(end) == ' ' || string.charAt(end) == '\n' || string.charAt(end) == '\t' || string.charAt(end) <= '\r')) {
            end--;
        }

        if (start == 0 && end == last) {
            return string;
        }


        return string.substring(start, end - start + 1);
    }


    public static String format(String format, Object... args) {
        return String.format(Locale.getDefault(), format, args);
    }


    public static String replaceAll(String string, String pattern, String replacement) {
        if (TextUtils.isEmpty(string)) return string;

        return string.replaceAll(pattern, replacement);
    }

    @SuppressLint("NewApi")
    public static void loadImage(final Activity context, ImageView imageView, String url, int placeHolderUrl, int errorImageUrl) {
        if (context == null || context.isDestroyed()) return;

        //placeHolderUrl=R.drawable.ic_user;
        //errorImageUrl=R.drawable.ic_error;
        Glide.with(context) //passing context
                .load(url) //passing your url to load image.
                .placeholder(placeHolderUrl) //this would be your default image (like default profile or logo etc). it would be loaded at initial time and it will replace with your loaded image once glide successfully load image using url.
                .error(errorImageUrl)//in case of any glide exception or not able to download then this image will be appear . if you won't mention this error() then nothing to worry placeHolder image would be remain as it is.
                .diskCacheStrategy(DiskCacheStrategy.ALL) //using to load into cache then second time it will load fast.
//                .animate(R.anim.fade_in) // when image (url) will be loaded by glide then this face in animation help to replace url image in the place of placeHolder (default) image.
                .fitCenter()//this method help to fit image into center of your ImageView
                .into(imageView); //pass imageView reference to appear the image.
    }
}
