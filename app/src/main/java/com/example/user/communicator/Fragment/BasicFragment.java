package com.example.user.communicator.Fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.communicator.Activity.PlannerModule.PlannerTabActivity;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.Login.Login;
import com.example.user.communicator.Model.Notes.Item;
import com.example.user.communicator.R;
import com.example.user.communicator.Utility.Utils;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;
import io.realm.RealmResults;

public abstract class BasicFragment extends Fragment implements SharedPreferences.OnSharedPreferenceChangeListener {

    public ProgressDialog pDialog;

    public final String URL_ImageUpload_SERVER = "http://52.66.249.75:9999/uploads/";
    public final String URL_ROOT_SERVER = "http://52.66.249.75:9999/api/";//52.66.249.75

    public final String URL_ImageUpload_LOCAL = "http://52.66.249.75:9999/uploads/";
    public final String URL_ROOT_local = "http://192.168.50.100:9999/api/";//52.66.249.75

    public final String URL_ROOT_TEST = "http://13.232.37.104:9999/api/";//http://13.232.37.104:9999
    public final String URL_ImageUpload_ROOT_TEST = "http://13.232.37.104:9999/uploads/";

    public final String URL_ROOT = URL_ROOT_TEST;
    public final String URL_ImageUpload_ROOT = URL_ImageUpload_ROOT_TEST;

    public final String url_allNotess = URL_ROOT + "planner/getplnnernotesviewAll";
    public final String url_allNotessByTime = URL_ROOT + "planner/getTimeBasePlannerNotesViewAll";
    public final String url_noteDelete = URL_ROOT + "planner/deleteplannerdata";

    public final String url_allSubCatList = URL_ROOT + "operation/getOperationlist";//getsuboperationcategoryviewAll
    public final String url_saveTitleProject = URL_ROOT + "operation/saveOperation";//savesubcategoryoperation replace saveOperation
    public final String url_subCatList = URL_ROOT + "operation/getOperationlist"; //getsuboperationcategorylist
    public final String url_deleteList = URL_ROOT + "operation/deleteOperation";//deletesubcategoryoperation
    public final String url_saveCategory = URL_ROOT + "category/SaveCategory";
    public final int IMAGE_PICKER_REQUEST_CODE = 10001;
    private String TAG = "BaseActivity";

    public final String ANDROID = "ANDROID";

    Toolbar vToolbar;

    DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;

    View vNavProfileBlock;
    ImageView ivNavLogo;
    ImageView ivNavProfilePicture;
    TextView tvNavProfileName;
    TextView tvNavProfileEmail;

    boolean hasNavigationDrawer = false;

    ProgressDialog mProgressDialog;

    protected Toolbar getToolbar() {
//        if (vToolbar == null) {
//            vToolbar = (Toolbar) findViewById(R.id.toolbar);
//
//            if (vToolbar != null) {
//                // ivNavLogo = (ImageView) vToolbar.findViewById(R.id.ivCompanyLogo);
//                setSupportActionBar(vToolbar);
//            }
//
//            // setCompanyLogo();
//
//        }
//
        return vToolbar;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        PreferenceManager.getDefaultSharedPreferences(getActivity()).registerOnSharedPreferenceChangeListener(this.getActivity());
//        if (TextUtils.isEmpty(Constant.DEVICE_TOKEN)) {
//            FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(getActivity(), instanceIdResult -> Constant.DEVICE_TOKEN = instanceIdResult.getToken());
//        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanseState) {
        View view = provideYourFragmentView(inflater, parent, savedInstanseState);

        return view;
    }

    public abstract View provideYourFragmentView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState);


    protected void showCompanyLogo(boolean show) {
        if (ivNavLogo != null) ivNavLogo.setVisibility(show ? View.VISIBLE : View.GONE);
    }


    public void showProgressDialog(Context ct, String message) {
        if (pDialog == null || !pDialog.isShowing()) {
            pDialog = new ProgressDialog(ct);
            pDialog.setMessage(message);
            pDialog.setCancelable(false);
            pDialog.show();
        }
    }


    public void hideProgressDialog(Context ct) {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
            pDialog = null;
        }
    }


    protected void enableHomeUp(Drawable navigationIcon, View.OnClickListener listener) {
        if (vToolbar == null) {
            return;
        }

        vToolbar.setNavigationIcon(navigationIcon);
        vToolbar.setNavigationOnClickListener(listener);
    }

    OnBackPressedListener mOnBackPressedListener;


    public void setMyPlannerItems(ArrayList<Item> appInfo) {
        SharedPreferences.Editor editor1 = getSharedPreferences().edit();
        Gson gson = new Gson();
        String data = gson.toJson(appInfo);
        editor1.putString("MyPlannerItems", data);
        editor1.apply();
    }

//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                if (vNavProfileBlock != null){
//                    int statusBarHeight = getStatusBarHeight();
//
//                    vNavProfileBlock.setPadding(vNavProfileBlock.getPaddingLeft(),
//                            vNavProfileBlock.getPaddingTop()+statusBarHeight,
//                            vNavProfileBlock.getPaddingRight(),
//                            vNavProfileBlock.getPaddingBottom()
//                    );
//
//                    ViewGroup.LayoutParams lp = vNavProfileBlock.getLayoutParams();
//                    lp.height = lp.height + statusBarHeight;
//                    vNavProfileBlock.requestLayout();
//                } else {
//                    mNavigationView.setPadding(0, getStatusBarHeight(), 0, 0);
//                }
//            }

//            vNavProfileBlock.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    onNavigationProfileClicked();
//                }
//            });


    protected void setNavigationItemSelected(int id) {
        if (mNavigationView == null) {
            return;
        }

        Menu menu = mNavigationView.getMenu();
        MenuItem menuItem = menu.findItem(id);
        onNavigationItemSelected(menuItem);
    }

    protected void onNavigationProfileClicked() {

    }

    protected boolean onNavigationItemSelected(MenuItem menuItem) {

        return false;
    }

    protected void setNavigationItemChecked(int id) {
        if (mNavigationView == null) {
            return;
        }

        Menu menu = mNavigationView.getMenu();
        MenuItem menuItem = menu.findItem(id);
        menuItem.setChecked(true);
    }

    public void closeNavigationDrawer() {
        if (mDrawerLayout != null && mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        }
    }

    public void openNavigationDrawer() {
        if (mDrawerLayout != null && !mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }
    }

    protected void removeNavigationIcon() {
        if (vToolbar == null) {
            return;
        }

        vToolbar.setNavigationIcon(null);
        vToolbar.setNavigationOnClickListener(null);
    }

//    public void setUserDataOnDrawer() {
//        if(tvNavProfileName == null || tvNavProfileEmail == null) {
//            return;
//        }
//
//        User user = PreferenceUtils.getActiveUser(getActivity());
//        if (user == null) {
//            return;
//        }
//
//        String logoUrl = user.getCompanyLogoFullUrl();
//        Log.d("BaseActivity", "logoUrl -> " + logoUrl);
//
//        if (ivNavLogo != null) {
//            Picasso.with(getActivity())
//                    .load(logoUrl)
//                    .resize(0, Utils.dp(90, getActivity()))
//                    .into(ivNavLogo);
//        }
//        tvNavProfileName.setText(user.getDriverName());
//        tvNavProfileEmail.setText(user.getDriverEmail());
//    }

//    public void setCompanyLogo() {
//        User user = PreferenceUtils.getActiveUser(getActivity());
//        if (user == null) {
//            return;
//        }

//        String logoUrl = user.getCompanyLogoFullUrl();
//        Log.d("BaseActivity", "logoUrl -> " + logoUrl);
//
//        if (ivNavLogo != null) {
//            Picasso.with(getActivity())
//                    .load(logoUrl)
//                    .resize(0, Utils.dp(90, getActivity()))
//                    .into(ivNavLogo);
//        }
//    }

    public NavigationView getNavigationView() {
        return mNavigationView;
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public void showProgress() {
        showProgress("Loading..");
    }

    public void showProgress(String message) {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            return;
        }

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public boolean isNavigationDrawerOpen() {
        return (mDrawerLayout != null && mDrawerLayout.isDrawerOpen(GravityCompat.START));
    }

    public boolean isValidEmail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    public boolean isValidPhone(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    public String getCurruntDate() {
        SimpleDateFormat df = new SimpleDateFormat("dd,MMM yyyy", Locale.US);
        return df.format(Calendar.getInstance().getTime());

    }

    public String getMillsToDate(long mills) {
        SimpleDateFormat df = new SimpleDateFormat("dd,MMM yyyy", Locale.US);
        return df.format(new Date(mills));
    }


    public long stringToMills(String mDate) {
        SimpleDateFormat df = new SimpleDateFormat("dd,MMM yyyy", Locale.US);
        Date date = null;
        try {
            date = df.parse(mDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long millis = 0;
        if (date != null) {
            millis = date.getTime();
        }
        return millis;
    }

    public String getCurruntTime() {
        SimpleDateFormat df = new SimpleDateFormat("hh:mm a", Locale.US);
        return df.format(Calendar.getInstance().getTime());
    }

    public String millsToDate(long mills) {
        Date date = new Date(mills);
        SimpleDateFormat df = new SimpleDateFormat("hh:mm a", Locale.US);
        return df.format(date);
    }


    public SharedPreferences getSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(getActivity());
    }

//    private void loadLang() {
//        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
//        int langPos = sp.getInt("lang", 0);
//        String[] languages =  getResources().getStringArray(R.array.language_codes);
//        Resources res = getResources();
//        // Change locale settings in the app.
//        DisplayMetrics dm = res.getDisplayMetrics();
//        android.content.res.Configuration conf = res.getConfiguration();
//        conf.locale = new Locale(languages[langPos].toLowerCase());
//        res.updateConfiguration(conf, dm);
//    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        //if (PreferenceUtils.PREF_USER.equals(key)) {
        //setUserDataOnDrawer();
        //} else if("lang".equals(key)){
//            recreate();
        //}
    }


    public void dismissProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }


    public Datum getLoginData() {
        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("Login", 0);
        String data = sharedpreferences.getString("UserData", null);
        if (!TextUtils.isEmpty(data)) {
            Gson gson = new Gson();
//            return gson.fromJson(data, Login.class);
            return gson.fromJson(data, Datum.class);
        }
        return null;
    }

    public boolean isOnline(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
//        NetworkInfo netInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        //should check null because in airplane mode it will be null
        return (netInfo != null && netInfo.isConnected());
    }

    public void setLoginData(Datum response, String Token) {

        Gson gson = new Gson();
        String Data = gson.toJson(response);

        SharedPreferences sharedpreferences = getActivity().getSharedPreferences("Login", 0);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString("UserData", Data);
        editor.putString("Token", Token);
        editor.apply();
    }

    public interface OnBackPressedListener {
        boolean onBackPressed();
    }


    //------------------------------  For Image Pick -----------------------------------------------//

    public Uri generateImageUri(Context context) {
        final File root = context.getCacheDir();
        if (!root.exists() || !root.isDirectory()) {
            root.mkdirs();
        }
        final String name = Calendar.getInstance().getTimeInMillis() + ".jpg";
        final File sdImageMainDirectory = new File(root, name);
        return Uri.fromFile(sdImageMainDirectory);
    }

    public Intent createImageIntent(Context context) {
        // Camera.
        final List<Intent> cameraIntents = new ArrayList<>();
        final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        final PackageManager packageManager = context.getPackageManager();
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            cameraIntents.add(intent);
        }

        // Filesystem.
        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        photoPickerIntent.setType("image/*");

        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(photoPickerIntent, "Profile photo");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));

        return chooserIntent;
    }

    public void openImageIntent(Activity activity) {
        Intent chooserIntent = createImageIntent(activity);
        activity.startActivityForResult(chooserIntent, IMAGE_PICKER_REQUEST_CODE);
    }

    public Bitmap compressedBitmap(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] BYTE;
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        BYTE = byteArrayOutputStream.toByteArray();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 2;
        return BitmapFactory.decodeByteArray(BYTE, 0, BYTE.length, options);
    }

    public boolean saveBitmap(Bitmap bmp, Uri uri) {
        File file = new File(uri.getPath());
        if (file.exists()) {
            file.delete();
        }

        FileOutputStream out = null;
        boolean result = false;
        try {
            out = new FileOutputStream(uri.getPath());
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, out); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public Uri getImageUri(Activity activity, Intent data) {
        if (data != null) {
            String action = data.getAction();
            Uri uri = data.getData();

            if (uri != null) {
                return uri;
            } else if ("inline-data".equals(action)) {
                Bitmap bitmap = (Bitmap) data.getExtras().get("data");
                if (bitmap != null) {
                    Uri outputFileUri = generateImageUri(activity);
                    if (saveBitmap(bitmap, outputFileUri)) {
                        return outputFileUri;
                    } else {
                        File file = new File(outputFileUri.getPath());
                        if (file.exists()) {
                            file.delete();
                        }
                    }
                }
            }
        }

        return null;
    }


    public String storeImage(Bitmap image) {

        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.d(TAG,
                    "Error creating media file, check storage permissions: ");// e.getMessage());
            return null;
        }
        try {
            String s = getSize(String.valueOf(image.getWidth()), String.valueOf(image.getHeight()));
            String size = getSize(s.substring(0, s.indexOf(",")), s.substring(s.indexOf(",") + 1));
            image = Bitmap.createScaledBitmap(image, Integer.parseInt(size.substring(0, size.indexOf(","))), Integer.parseInt(size.substring(size.indexOf(",") + 1)), true);
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.JPEG, 70, fos);
            fos.close();
            return pictureFile.getAbsolutePath();
        } catch (FileNotFoundException e) {
            Log.d(TAG, "File not found: " + e.getMessage());
            return null;
        } catch (IOException e) {
            Log.d(TAG, "Error accessing file: " + e.getMessage());
            return null;
        }
    }

    public File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getActivity().getPackageName()
                + "/Files");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss", Locale.US).format(new Date());
        File mediaFile;
        String mImageName = "MI_" + timeStamp + ".jpeg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public String getSize(String _width, String _height) {
        int width = Integer.parseInt(_width);
        int height = Integer.parseInt(_height);
        if (width > height) {
            if (width > 720) {
                height = (height * 720) / width;
                width = 720;
            }
            return width + "," + height;
        } else {
            if (height > 720) {
                width = (width * 720) / height;
                height = 720;
            }
            return width + "," + height;
        }
    }

    public double getImageHeight(Context context, int imageWidth, int imageHeight) {

        Log.e("original", imageWidth + "..." + imageHeight);//276  183

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        int phnWidth = Utils.getScreenWidth(context);
//        int phnHeight = Utils.getScreenHeight(context);
        double height = ((imageHeight * phnWidth) / imageWidth);//716
//        double height = ((imageHeight/imageWidth) * phnWidth);//1065
        int phnHeight = ((display.getHeight() * 70) / 100);

        Log.e("getImageHeight", height + "");//716
        if (height >= phnHeight) {
            height = ((display.getHeight() * 70) / 100); //80% of the screan
        } else if (height == 0) {
            height = ((imageHeight * 40) / 100); //80% of the screan
        }
        return height;

    }

    public void ApiCall(String url, final ApiCall type, RequestParams requestParams) {

        requestParams.put("strCategoryTypeId", PlannerTabActivity.strCategoryTypeId);
        requestParams.put("strCategoryId", PlannerTabActivity.strCategoryId);
        requestParams.put("strSubCategoryId", PlannerTabActivity.strSubCategoryId);

        try {
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(60 * 1000);
            client.setURLEncodingEnabled(true);
            client.post(getActivity(), url, requestParams, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        if (response.getBoolean("success")) {
                            OnResponce(response, type);
                        } else {
                            OnError(response, type);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    hideProgressDialog(getActivity());
                    try {
                        showToast(errorResponse.getString("message"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    OnError(errorResponse, type);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    hideProgressDialog(getActivity());
                    showToast("Oops!!! Please try again");
                }

            });

        } catch (Exception e) {
            hideProgressDialog(getActivity());
            e.printStackTrace();
        }
    }

    public void ApiCallWithToken(String url, final ApiCall type, final RequestParams requestParams) {
//        requestParams.put("strCategoryTypeId", PlannerTabActivity.strCategoryTypeId);
//        requestParams.put("strCategoryId", PlannerTabActivity.strCategoryId);
//        requestParams.put("strSubCategoryId", PlannerFragment.strSubCategoryId);
        try {
            SharedPreferences sharedpreferences = getActivity().getSharedPreferences("Login", 0);
            String data = sharedpreferences.getString("UserData", null);
            String token = sharedpreferences.getString("Token", null);
            Gson gson = new Gson();
            Login login = gson.fromJson(data, Login.class);
            Datum userData = gson.fromJson(data, Datum.class);
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(30 * 1000);
            if (userData != null) {
                client.addHeader("Authorization", "Bearer " + token);

                requestParams.put("intProjectId", userData.getArryselectedProjectAllItems().get(0).getId());
                requestParams.put("intOrganisationId", userData.getArryselectedOrganisationAllItems().get(0).getId());
            }

//            ByteArrayEntity entity = new ByteArrayEntity(requestParams.toString().getBytes("UTF-8"));
//            entity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
            client.setURLEncodingEnabled(true);
            Log.e("ApiCallWithToken", url + "..." + requestParams);
            client.post(getActivity(), url, requestParams, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        Log.e("token_fragmt", type + "..." + response + "..../" + response.getString("message") + "/.....");
                        if (response.getBoolean("success")) {
                            OnResponce(response, type);
                        } else {
                            if (response.getString("message").equals(" Delete  failed.")) {

                                Log.e("APi_delete", type + "..." + requestParams.has("intPlannerId"));
                                if (type.equals("DELETEPLANNER")) {
                                    Realm realm = Realm.getDefaultInstance();
                                    RealmResults<Item> deleteFromRealm = realm.where(Item.class).equalTo("intPlannerId", requestParams.has("intPlannerId")).findAll();
                                    Log.e("APi_delete_INN", type + "..." + deleteFromRealm.size());
                                }
                            }
//                            OnError(response, type);
//                            Log.e("token_fra11", type + "..."+response);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    hideProgressDialog(getActivity());
                    String errorType = throwable.getMessage();
                    if (TextUtils.isEmpty(errorType)) {

                        showToast("Oops!!! Please try again");
                        OnError(errorResponse, type);
                    } else {

                        if (errorType.equals("Read timed out")) {
                            showToast("Oops!!! Server Time-out.Please try again");
                        } else {
                            showToast("Oops!!! Please try again");
                            OnError(errorResponse, type);
                        }
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    hideProgressDialog(getActivity());
                    showToast("Oops!!! Please try again");
                }

            });

        } catch (Exception e) {
            hideProgressDialog(getActivity());
            e.printStackTrace();
        }

    }

    public void OnResponce(JSONObject object, ApiCall type) {
        String s = object.toString();
    }

    public void OnError(JSONObject object, ApiCall type) {

//        String s1 = object.toString();
        hideProgressDialog(getActivity());


        try {
            showToast(object.getString("message"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void showToast(String message) {
        LayoutInflater inflater = getLayoutInflater();
        View layouttoast = inflater.inflate(R.layout.toastcustom, (ViewGroup) getActivity().findViewById(R.id.toastcustom));
        ((TextView) layouttoast.findViewById(R.id.texttoast)).setText(message);

        Toast mytoast = new Toast(getActivity());
        mytoast.setView(layouttoast);
        mytoast.setDuration(Toast.LENGTH_LONG);
        mytoast.setGravity(Gravity.CENTER, 0, 0);
        mytoast.show();
//        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public void realmUpdate(Item item) {


    }

    public void realmDelete(Item item) {


    }

    public void realmInsert(Item item) {


    }

    public void realmList(Item item) {


    }

    public enum ApiCall {
        LOGIN, PLANNERLIST, NEWPLANNERLIST, ADDPLANNER, PLANNERDETAILS, COLLEBORATELIST, COLLABORATESAVE,
        UPDATEPLANNER, DELETEPLANNER, CATEGORYLIST, CATEGORYSAVE, DELETECATEGORY, IMAGE,
        AUDIO, COLLABORATEUSERS, DELETEAUDIO, PINNED, REMINDERLIST, OPERATIONSLIST, GETSUBCATlIST,
        SAVEPROJECTTITLE, GOALDELETE, SAVEMAINCATEGORY
    }

//    public RealmList<Item> toRealmList(Realm realm, ArrayList<String> arrayList) {
//        RealmList mRealmList = new RealmList<Item>();
//        for (int i = 0; i < arrayList.size(); i++){
//            // Create a IncidentPhoto object which is managed by Realm.
//            Item incidentPhoto = realm.createObject(Item.class);
//            incidentPhoto.setPhotoPath(arrayList.get(i));
//            mRealmList.add(incidentPhoto);
//        }
//        return mRealmList;
//    }

}
