
package com.example.user.communicator.Model.Operation.AllStepsList;

import com.example.user.communicator.Model.Operation.ListSubCategory.DatumSubCat;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AllStepsList {

    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<DatumSubCat> data = null;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DatumSubCat> getData() {
        return data;
    }

    public void setData(List<DatumSubCat> data) {
        this.data = data;
    }

}
