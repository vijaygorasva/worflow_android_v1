package com.example.user.communicator.Model.ChatModels;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MessagesData {
    private String status;
    private String msg;
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("messagesData")
        public List<Messages> messagesList;

        public List<Messages> getMessagesList() {
            return messagesList;
        }

        public void setMessagesList(List<Messages> messagesList) {
            this.messagesList = messagesList;
        }
    }
}
