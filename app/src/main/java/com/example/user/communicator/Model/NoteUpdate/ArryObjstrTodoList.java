
package com.example.user.communicator.Model.NoteUpdate;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ArryObjstrTodoList {

    @SerializedName("strNoteListFlage")
    @Expose
    private Boolean strNoteListFlage;
    @SerializedName("strNoteListValue")
    @Expose
    private String strNoteListValue;

    public Boolean getStrNoteListFlage() {
        return strNoteListFlage;
    }

    public void setStrNoteListFlage(Boolean strNoteListFlage) {
        this.strNoteListFlage = strNoteListFlage;
    }

    public String getStrNoteListValue() {
        return strNoteListValue;
    }

    public void setStrNoteListValue(String strNoteListValue) {
        this.strNoteListValue = strNoteListValue;
    }

}
