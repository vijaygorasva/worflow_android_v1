package com.example.user.communicator.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.user.communicator.Activity.PlannerModule.BaseActivity;
import com.example.user.communicator.CustomViews.CustomButton;
import com.example.user.communicator.Model.Login.Datum;
import com.example.user.communicator.Model.Login.ObjModuledetailLevel;
import com.example.user.communicator.Model.Login.ObjModuledetailUser;
import com.example.user.communicator.R;
import com.example.user.communicator.Utility.Constant;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.loopj.android.http.RequestParams;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.etEmail)
    MaterialEditText etEmail;
    @BindView(R.id.etPassword)
    MaterialEditText etPassword;
    @BindView(R.id.ivShowPassowrd)
    ImageView ivShowPassowrd;
    @BindView(R.id.llMain)
    LinearLayout llMain;

    int i = 1;
    @BindView(R.id.btnLogin)
    CustomButton btnLogin;
    Datum response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        etPassword.setText("user1234");
    }

    @OnClick({R.id.ivShowPassowrd, R.id.btnLogin})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivShowPassowrd:

                if (i == 0) {
                    Glide.with(LoginActivity.this)
                            .load(R.drawable.ic_hidepassword)
                            .placeholder(R.drawable.ic_showpassword)
                            .dontAnimate()
                            .into(ivShowPassowrd);
                    etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    i = 1;
                } else {
                    Glide.with(LoginActivity.this)
                            .load(R.drawable.ic_showpassword)
                            .placeholder(R.drawable.ic_showpassword)
                            .dontAnimate()
                            .into(ivShowPassowrd);
                    etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    i = 0;
                }

                break;
            case R.id.btnLogin:
                String email = etEmail.getText().toString().trim();
                String password = etPassword.getText().toString().trim();

                if (TextUtils.isEmpty(email)) {
                    showToast("Please enter email!");
                    break;
                } else if (!isValidEmail(email)) {
                    showToast("Please enter valid email!");
                    break;
                }
                if (TextUtils.isEmpty(password)) {
                    showToast("Please enter password!");
                    break;
                }
                if (isNetworkAvailable()) {
                    loginUser(email, password);
                } else {
                    int color = Color.RED;
                    String message = "Sorry! Not internet connection";
                    Snackbar snackbar = Snackbar.make(findViewById(R.id.llMain), message, Snackbar.LENGTH_LONG);
                    View sbView = snackbar.getView();
                    TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                    textView.setTextColor(color);
                    snackbar.show();
                }
                break;
        }
    }

    public void loginUser(String email, String password) {

        RequestParams params = new RequestParams();
//        params.put("strEmail", "jjj@gmail.com");
//        params.put("strPassword", "PIl2MDbrmBpo6tfXmmqDPmWtGAjR+Cw5lual4RuWyyZigqwJs98guQzFspECKFJCaDGh52MGf2U+ZPx+iT/RK/wj+YzigMNEQHD5IXvsdfzrWUzqrEZY7LMQMu/C0bNBCLoHNuleawI7miSNWxGh/STVecdHuVB86MPLG08/P3s=");

        params.put("strEmail", email);
        params.put("strPassword", password);
        params.put("deviceType", ANDROID);
        params.put("deviceToken", Constant.DEVICE_TOKEN);

        showProgressDialog(LoginActivity.this, "Verifying...");
        Log.e("url", urlLogin + "...." + params);
        ApiCall(urlLogin, ApiCall.LOGIN, params);

    }

//    public void loginUser() {
//
////        DatabaseHelper db = new DatabaseHelper(getApplicationContext());
////        db.removeAll();
//
//        RequestParams params = new RequestParams();
//        params.put("strEmail", "mohith@gmail.com");
//        params.put("strPassword", "user1234");
//
//        showProgressDialog(this, "Verifying...");
//        ApiCall(urlLogin, ApiCall.LOGIN, params);
//
//    }


    @Override
    public void OnResponce(JSONObject data, ApiCall type) {
        JSONArray array = null;

        String token;
        try {
            array = data.getJSONArray("data");

            if (type == ApiCall.LOGIN) {
                hideProgressDialog(LoginActivity.this);
                if (array.length() != 0) {
                    Realm realm = Realm.getDefaultInstance();
//                    Real
                    realm.beginTransaction();
                    realm.deleteAll();
                    realm.commitTransaction();
                    Gson gson = new Gson();

//                    Login commentData = gson.fromJson(object.getJSONObject("data").getJSONObject("commentData").toString(), CommentData.class);

                    for (int i = 0; i < array.length(); i++) {

                        try {
                            token = data.getString("token");
                            JSONObject object = array.getJSONObject(i);
                            response = gson.fromJson(object.toString(), Datum.class);
                            setLoginData(response, token);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    Dexter.withActivity(LoginActivity.this)
                            .withPermissions(
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                            ).withListener(new MultiplePermissionsListener() {
                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                            if (report.areAllPermissionsGranted()) {


                                File folder = new File(Environment.getExternalStorageDirectory() + File.separator + "WorFlow");

                                File presfolder = new File(folder, "WorFlow Images");
                                File appfolder = new File(folder, "WorFlow Audios");

                                if (!folder.exists()) {

                                    presfolder.mkdirs();
                                    appfolder.mkdirs();
                                }

                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);

                                finish();
                                showToast("Welcome to worflow");

                            } else {

                                List<PermissionDeniedResponse> isDenied = report.getDeniedPermissionResponses();

                                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                startActivity(intent);

                                finish();
                                showToast("Welcome to worflow");

                            }

                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                            Log.e("report_token", token + "..." + permissions);

                        }

                    }).check();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        super.OnResponce(data, type);
    }

    @Override
    public void OnError(JSONObject object, ApiCall type) {
        super.OnError(object, type);
        if (type == ApiCall.LOGIN) {
            try {
                showToast(object.getString("message"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void saveArrayList(ArrayList<ObjModuledetailUser> list, String key) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!

//        ArrayList<ObjModuleUserdetail> jsonValue=getArrayList(key);
//        Log.e("json_jsonValue", new Gson().toJson(jsonValue)+"....");

    }

    public ArrayList<ObjModuledetailLevel> getArrayList(String key) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        return gson.fromJson(json, type);
    }
}
